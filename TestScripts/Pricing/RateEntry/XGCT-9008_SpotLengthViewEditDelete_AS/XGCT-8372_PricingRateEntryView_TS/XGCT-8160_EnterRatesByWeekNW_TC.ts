/*
***********************************************************************************************************
* @Script Name :  XGCT-8160
* @Description :  xG Campaign - Enter Rates by Week (Network)
* @Page Object Name(s) : Rate Entry
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 2-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { element, by, protractor } from 'protractor';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { Reporter } from '../../../../../Utility/htmlResult';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { AppCommonFunctions } from '../../../../../Libs/ApplicationLibs/CommAppLib';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { RateEntry } from '../../../../../Pages/Pricing/RateEntry';
import { globalTestData } from '../../../../../TestData/globalTestData';
import { RateCardPage } from '../../../../../Pages/Pricing/Reference/RateCard';
import { Gutils } from '../../../../../Utility/gutils';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let homePage = new HomePageFunction();
let appCommFunction = new AppCommonFunctions();
let rateEntry = new RateEntry();
let rateCard = new RateCardPage();
let gutil = new Gutils();
//Variables Declaration
let TestCase_ID = 'XGCT-8160'
let TestCase_Title = 'xG Campaign - Enter Rates by Week (Network)'
let Rate1: number
let Rate2: number
let netpercentage: number
let rateCardName;
let startDate = gutil.getcurrentDate();
let endDate = gutil.getcurrentDateAfterSomeDays(10);
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - ENTER RATES BY WEEK (NETWORK)', () => {


    // --------------Test Step 1------------
    it("Navigate to the application URL", async () => {

        let stepAction = "Navigate to the application URL";
        let stepData = "";
        let stepExpResult = "User should be navigated to the application";
        let stepActualResult = "User is navigated to the application";
        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    // --------------Test Step 2------------
    it("Navigate to  Pricing>Network Rate entry", async () => {

        let stepAction = "Navigate to  Pricing>Network Rate entry";
        let stepData = "";
        let stepExpResult = "Check the  rate entry View page is displayed";
        let stepActualResult = "Rate entry View page is displayed";
        try {
            await homePage.appclickOnNavigationList(globalTestData.pricingTabName);
            await homePage.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await homePage.appclickOnMenuListLeftPanel(globalTestData.networkRateCardleftMenuName);
            await verify.verifyProgressBarNotPresent();
            await appCommFunction.selectDropDownValue("Network",rateEntry.networkName[0]);
            await verify.verifyProgressBarNotPresent();
            await action.SetText(appCommFunction.globalFilter,"True","");
            await verify.verifyProgressBarNotPresent();
            let noMatchStatus:boolean = await verify.verifyElementVisible(appCommFunction.noDataMatchFound);
            if(noMatchStatus)
            {
                let cardDetails = {"Name":"AT_"+new Date().getTime()}
                rateCardName = await cardDetails["Name"];
                await appCommFunction.clickOnAddButton();
                await rateCard.addRateCard(cardDetails);
                await verify.verifyProgressBarNotPresent(); 
            }
            else
            {
            let columnIndex = await appCommFunction.getTableColumnIndex("Rate Card");
            rateCardName = await action.GetText(by.xpath(appCommFunction.gridCellData.replace("rowdynamic","1").replace("columndynamic",String(columnIndex))),"");
            rateCardName = String(rateCardName).trim();   
        } 
            await homePage.appclickOnMenuListLeftPanel(globalTestData.leftMenuNetworkRateEntry);
            await verify.verifyElementIsDisplayed(appCommFunction.pageHeader, " Rate Entry");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    // --------------Test Step 3------------
    it("Select Network", async () => {

        let stepAction = "Select Network";
        let stepData = "'Market as 'FOX'";
        let stepExpResult = "FOX network should be selected";
        let stepActualResult = "FOX network is selected";

        try {
            await appCommFunction.deSelectAllMultiDropdownOptions("network");
            await appCommFunction.selectMultiOptionsFromDropDown("network",[rateEntry.networkName[0]]);
            // await rateEntry.VerifyDropDown(rateEntry.NetworkMultiSelectDropdwon, rateEntry.DropDownOptions, rateEntry.networkName, "Network")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 4------------
    it("Select Rates by dropdown", async () => {

        let stepAction = "Select Rates by dropdown";
        let stepData = "Rates by as Week";
        let stepExpResult = "Week drop should be selected";
        let stepActualResult = "Week drop is selected";
        let week = "Week";
        let rateBy = "Rates By";
        try {
            await appCommFunction.selectDropDownValue(rateBy,week);
            // await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-ratesby", week, rateBy);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step 5------------
    it("Select Rate Card", async () => {

        let stepAction = "Select Rate Card";
        let stepData = "";
        let stepExpResult = "Rate Card should be selected";
        let stepActualResult = "Rate Card is selected";

        try {
            await appCommFunction.selectDropDownValue("Rate Card",rateCardName);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 6------------
    it("Select Base length drop down as 30", async () => {

        let stepAction = "Select Base length drop down as 30";
        let stepData = "";
        let stepExpResult = "Base length should be selected";
        let stepActualResult = "Base length is selected";
        let baselength = "30";
        try {
            await appCommFunction.selectDropDownValue("Base Length (seconds)",baselength);
                        // await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-baselength", baselength, "Base Length (seconds)")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 7------------
    it("Select Start Date and End date (2 weeks)", async () => {

        let stepAction = "Select Start Date and End date (2 weeks)";
        let stepData = "";
        let stepExpResult = "User should be selected 2 weeks dates";
        let stepActualResult = "User selected 2 weeks dates";

        try {
            await action.ClearText(rateEntry.startDate_Textbox, "startDate");
            await action.SetText(rateEntry.startDate_Textbox, startDate, "startDate");
            await action.ClearText(rateEntry.endDate_Textbox, "endDate")
            await action.SetText(rateEntry.endDate_Textbox, endDate, "endDate_Textbox");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 8------------
    it("Check Gross from calculation metrics", async () => {

        let stepAction = "Check Gross from calculation metrics";
        let stepData = "";
        let stepExpResult = "Gross should be selected";
        let stepActualResult = "Gross is selected";

        try {
            await action.Click(rateEntry.gross_RadioButton, "gross_RadioButton")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 9------------
    it("Click on search", async () => {

        let stepAction = "Click on search";
        let stepData = "";
        let stepExpResult = "User able to view local programs in the grid.(Based on selected criteria)";
        let stepActualResult = "User able to view local programs in the grid.(Based on selected criteria)";

        try {
            await action.Click(rateEntry.button_Search, "SearchButton");
            await verify.verifyProgressBarNotPresent();
            await verify.verifyElement(rateEntry.loadedGrid, "Grid with Programs");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 10------------
    it("Check the user can enter a 'Base Length for entering rates for local programs", async () => {

        let stepAction = "Check the user can enter a 'Base' Length for entering rates for local programs";
        let stepData = "";
        let stepExpResult = "user can enter a 'Base' Length for entering rates for local programs";
        let stepActualResult = "user can enter a 'Base' Length for entering rates for local programs";

        try {
            await verify.verifyElement(rateEntry.baseLengthDropdown, "baseLengthDropdown")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 11------------
    it("Check the user can able to change the 'Base' Length", async () => {

        let stepAction = "Check the user can able to change the 'Base' Length";
        let stepData = "";
        let stepExpResult = "User can able to change the Base Length";
        let stepActualResult = "User can able to change the Base Length";

        try {
            await action.Click(rateEntry.baseLengthDropdown, "baseLengthDropdown")
            await verify.verifyElement(rateEntry.baseLengthDropdownOptions, "baseLengthDropdownOptions")
            await action.Click(rateEntry.baseLengthDropdown, "baseLengthDropdown")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 12------------
    it("Change the Start and end date (4 weeks)", async () => {

        let stepAction = "Change the Start and end date (4 weeks)";
        let stepData = "";
        let stepExpResult = "User should be select 4 weeks dates";
        let stepActualResult = "User selects 4 weeks dates";
        endDate = await gutil.getcurrentDateAfterSomeDays(27);
        try {
            await action.ClearText(rateEntry.endDate_Textbox, "End Date")
            await action.SetText(rateEntry.endDate_Textbox, endDate, "endDate_Textbox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 13------------
    it("Click on search", async () => {

        let stepAction = "Click on search";
        let stepData = "";
        let stepExpResult = "User able to view local program in the grid.(Based on selected criteria)";
        let stepActualResult = "User able to view local program in the grid.(Based on selected criteria)";

        try {
            await action.Click(rateEntry.button_Search, "SearchButton");
            await verify.verifyProgressBarNotPresent();
            await verify.verifyElement(rateEntry.loadedGrid, "Grid with Programs");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 14------------
    it("Check the user is provided a field for updating the rate of the base length.", async () => {

        let stepAction = "Check the user is provided a field for updating the rate of the base length.";
        let stepData = "";
        let stepExpResult = "User should be able to see a field for updating the rate of the base length in the grid.";
        let stepActualResult = "User should be able to see a field for updating the rate of the base length in the grid.";

        try {
            Rate1 = Number(await rateEntry.makeNumericId(4))
            Rate2 = Number(await rateEntry.makeNumericId(4))
            await action.ClearText(element.all(rateEntry.ratesInput_TextBox).get(0), "ratesInput_TextBox week 1")
            await action.ClearText(element.all(rateEntry.ratesInput_TextBox).get(1), "ratesInput_TextBox week 2")
            await action.SetText(element.all(rateEntry.ratesInput_TextBox).get(0), String(Rate1), "ratesInput_TextBox week 1")
            await action.SetText(element.all(rateEntry.ratesInput_TextBox).get(1), String(Rate2), "ratesInput_TextBox week 2")
            await action.Click(rateEntry.button_Update, "button_Update")
            await action.Click(rateEntry.button_Confirm, "button_Confirm")
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 15------------
    it("Update 30 seconds base rates with a value in completely and navigate away from a rate entry window.", async () => {

        let stepAction = "Update 30 seconds base rates with a value in completely and navigate away from a rate entry window.";
        let stepData = "";
        let stepExpResult = "User will get a validation message that incomplete rate entry's in the grid.";
        let stepActualResult = "User got a validation message that incomplete rate entry's in the grid.";

        try {
            Rate1 = Number(await rateEntry.makeNumericId(4))
            await action.ClearText(element.all(rateEntry.ratesInput_TextBox).get(2), "ratesInput_TextBox week 1")
            await action.SetText(element.all(rateEntry.ratesInput_TextBox).get(2), String(Rate1), "ratesInput_TextBox week 1")
            await element.all(rateEntry.ratesInput_TextBox).get(2).sendKeys(protractor.Key.TAB)
            await action.Click(rateEntry.button_Search, "SearchButton")
            await verify.verifyProgressBarNotPresent();
            await verify.verifyElement(rateEntry.cancelUpdatesPopup, "cancelUpdatesPopup")
            await action.Click(rateEntry.button_No, "button_No")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });



    // --------------Test Step 17-----------
    it("Click on net checkbox in Calcualtion Metrics", async () => {

        let stepAction = "Click on net checkbox in Calcualtion Metrics";
        let stepData = "";
        let stepExpResult = "Net checkbox should checked in Calcualtion Metrics";
        let stepActualResult = "Net checkbox checked in Calcualtion Metrics";

        try {
            netpercentage = 50
            await verify.verifyElement(rateEntry.netOption, "netOption")
            await action.Click(rateEntry.netOption, "netOption")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 18------------
    it("Enter Net % and click on search", async () => {

        let stepAction = "Enter Net % and click on search";
        let stepData = "";
        let stepExpResult = "Net% should be take and should show selected date range releated program list in the rate entry view grid";
        let stepActualResult = "Net% is taken and showed selected date range releated program list in the rate entry view grid";

        try {
            await action.SetText(rateEntry.enterNetpercentage, String(netpercentage), "enterNetpercentage")
            await action.Click(rateEntry.calculate, "calculate")
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 19------------
    it("Check the data points rate values are changed in the rate entry view grid based on Net%  value in Net% field", async () => {

        let stepAction = "Check the data points rate values are changed in the rate entry view grid based on Net%  value in Net% field";
        let stepData = "";
        let stepExpResult = "User should be able to view changed values in rate entry view grid based on net % value";
        let stepActualResult = "User is able to view changed values in rate entry view grid based on net % value";

        try {
            let RevisedRate1 = ((Rate1 * netpercentage) / 100);
            console.log("revised:" + String(RevisedRate1.toFixed(2)));
            let actualValue = await action.GetText(rateEntry.weekprogram2rateInGrid,"");
            if(String(actualValue).replace('$','').replace(',','') != String(RevisedRate1.toFixed(2))) throw "Failed-Expected Value '"+String(RevisedRate1.toFixed(2))+"' and Actual Value '"+String(actualValue).replace('$','').replace(',','')+"' is mismatched"
            // await verify.verifyTextOfElement(rateEntry.weekprogram2rateInGrid, String(RevisedRate1.toFixed(2)), "ratesInput_TextBox week 1")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 16------------
    it("Check that the Round To column is moved all the way to the right", async () => {

        let stepAction = "Check that the Round To column is moved all the way to the right";
        let stepData = "";
        let stepExpResult = "User able to see 'Round To' column is moved to the right in the grid.'";
        let stepActualResult = "User able to see 'Round To' column is moved to the right in the grid.'";

        try {
            await action.Click(rateEntry.gross_RadioButton, "gross_RadioButton")
            await rateEntry.ColumnNameValidations(["Round To"]);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 20------------
    it("Check all column headers for rate card grids are frozen.", async () => {

        let stepAction = "Check all column headers for rate card grids are frozen.";
        let stepData = "";
        let stepExpResult = "User able to see all the column headers for rate card grids are frozen.";
        let stepActualResult = "User able to see all the column headers for rate card grids are frozen.";
        try {

            report.ReportStatus(stepAction, stepData, 'SKIP', stepExpResult, "Cannot be automated");
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'SKIP', stepExpResult, err);
        }

    });

});
