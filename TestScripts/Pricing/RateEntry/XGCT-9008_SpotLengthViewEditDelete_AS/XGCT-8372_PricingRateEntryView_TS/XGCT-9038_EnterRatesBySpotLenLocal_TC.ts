/*
***********************************************************************************************************
* @Script Name :  XGCT-9038
* @Description :  Enter Rates by Rate Card - Spot Length (Local)
* @Page Object Name(s) : Rate Entry
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 2-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, protractor } from 'protractor';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { Reporter } from '../../../../../Utility/htmlResult';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { AppCommonFunctions } from '../../../../../Libs/ApplicationLibs/CommAppLib';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { RateEntry } from '../../../../../Pages/Pricing/RateEntry';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let homePage = new HomePageFunction();
let appCommFunction = new AppCommonFunctions();
let rateEntry = new RateEntry();

//Variables Declaration
let TestCase_ID = 'XGCT-9038'
let TestCase_Title = 'Enter Rates by Rate Card - Spot Length (Local)'
let pricing = "Pricing";
let localRateEntry = "Local Rate Entry";
let Rate1: number
let Rate2: number
let netpercentage: number

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - ENTER RATES BY RATE CARD - SPOT LENGTH (LOCAL)', () => {


    // --------------Test Step 1------------
    it("Navigate to the application URL", async () => {

        let stepAction = "Navigate to the application URL";
        let stepData = "";
        let stepExpResult = "User should be navigated to the application";
        let stepActualResult = "User is navigated to the application";
        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    // --------------Test Step 2------------
    it("Navigate to  Pricing>Local Rate entry", async () => {

        let stepAction = "Navigate to  Pricing>Local Rate entry";
        let stepData = "";
        let stepExpResult = "Check the  rate entry View page is displayed";
        let stepActualResult = "Check the  rate entry View page is displayed";
        try {
            await homePage.appclickOnNavigationList(pricing);
            await homePage.appclickOnMenuListLeftPanel(localRateEntry);
            await verify.verifyElementIsDisplayed(appCommFunction.pageHeader, " Rate Entry");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    // --------------Test Step 4------------
    it("Select Market", async () => {

        let stepAction = "Select Market";
        let stepData = "Pittsburgh";
        let stepExpResult = "Market should be selected and should display selected market related channel list";
        let stepActualResult = "Market is selected and is displayed selected market related channel list";

        try {
            await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-markets", rateEntry.marketName, "Market")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 5------------
    it("Select Channel", async () => {
        let stepAction = "Select Market";
        let stepData = "WPGH";
        let stepExpResult = "Channel should be selected";
        let stepActualResult = "Channel is selected";
        try {
            await rateEntry.VerifyDropDown(rateEntry.Channel, rateEntry.DropDownOptions, rateEntry.multipleChnnels, "Channel")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step 6------------
    it("Select Rates By as spot length", async () => {

        let stepAction = "Select Rates By as spot length";
        let stepData = "";
        let stepExpResult = "Rates By spot length should be selected";
        let stepActualResult = "Rates By spot length is selected";

        try {
            await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-ratesby", "Spot Length", "Rates By");
            await browser.sleep(1000);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 7------------
    it("Select Spot Length (seconds)", async () => {

        let stepAction = "Select Spot Length (seconds)";
        let stepData = "30";
        let stepExpResult = "Base length(seconds) should be selected";
        let stepActualResult = "Base length(seconds) is selected";

        try {
            await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-ratesby", "Spot Length", "Rates By");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 8------------
    it("Select Start Date and End date . Click on search", async () => {

        let stepAction = "Select Start Date and End date . Click on search";
        let stepData = "";
        let stepExpResult = "Start date and end date should be selected  and should show selected date range releated program list in the rate entry view grid";
        let stepActualResult = "Start date and end date are selected  and showed selected date range releated program list in the rate entry view grid";

        try {
            await action.ClearText(rateEntry.startDate_Textbox, "startDate")
            await action.SetText(rateEntry.startDate_Textbox, rateEntry.startDateRentryView, "startDate")
            await action.SetText(rateEntry.endDate_Textbox, rateEntry.endDateRentryView1, "endDate_Textbox")
            await browser.sleep(3000);
            await action.Click(rateEntry.button_Search, "SearchButton");
            await rateEntry.GridValidationsUpdated("Channel", rateEntry.channelsToBeVerifiedInGrid)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });



    // --------------Test Step 9------------
    it("Check user can enter a 'Base' length  rate for the selected programs in data field", async () => {

        let stepAction = "Check user can enter a 'Base' length  rate for the selected programs in data field";
        let stepData = "";
        let stepExpResult = "User should have option to enter rate for base length's";
        let stepActualResult = "User have option to enter rate for base length's";

        try {
            await verify.verifyElement(rateEntry.spotLengthDropdown, "spotLengthDropdown")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 11------------
    it("Check the user is ability to change the 'Base' length", async () => {

        let stepAction = "Check the user is ability to change the 'Base' length";
        let stepData = "";
        let stepExpResult = "User should have option to change 'Base' length";
        let stepActualResult = "User have an option to change 'Base' length";

        try {
            await action.Click(rateEntry.spotLengthDropdown, "spotLengthDropdown")
            await verify.verifyElement(rateEntry.baseLengthDropdownOptions, "spotLengthDropdownOptions")
            await action.Click(rateEntry.spotLengthDropdown, "spotLengthDropdown")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 12------------
    it("Check the user is  provided a field for updating the rate of the base length for the selected date range", async () => {

        let stepAction = "Check the user is  provided a field for updating the rate of the base length for the selected date range";
        let stepData = "";
        let stepExpResult = "User should have field to update the rate of base length for the selected date range";
        let stepActualResult = "User have field to update the rate of base length for the selected date range";

        try {
            Rate1 = 100;
            Rate2 = 200;
            await action.ClearText(element.all(rateEntry.ratesInput_TextBox).get(0), "ratesInput_TextBox week 1")
            await action.ClearText(element.all(rateEntry.ratesInput_TextBox).get(1), "ratesInput_TextBox week 2")
            await action.SetText(element.all(rateEntry.ratesInput_TextBox).get(0), String(Rate1), "ratesInput_TextBox week 1")
            await action.SetText(element.all(rateEntry.ratesInput_TextBox).get(1), String(Rate2), "ratesInput_TextBox week 2")
            await action.Click(rateEntry.button_Update, "button_Update");
            await action.Click(rateEntry.button_Confirm, "button_Confirm");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 13------------
    it("Select a record , enter rates in data point for selected spot  length(30S)  and click on update", async () => {

        let stepAction = "Select a record , enter rates in data point for selected spot  length(30S)  and click on update";
        let stepData = "";
        let stepExpResult = "Record should be selected and rate should be updated sucessfully for 30s spot length";
        let stepActualResult = "Record is selected and rate is updated sucessfully for 30s spot length";

        try {
            Rate1 = 100;
            await action.ClearText(element.all(rateEntry.ratesInput_TextBox).get(0), "ratesInput_TextBox week 1")
            await action.SetText(element.all(rateEntry.ratesInput_TextBox).get(0), String(Rate1), "ratesInput_TextBox week 1")
            await action.Click(rateEntry.button_Update, "button_Update")
            await action.Click(rateEntry.button_Confirm, "button_Confirm");
            browser.call(async function () {
                await browser.sleep(3000);
                await verify.verifyElementAttribute(element.all(rateEntry.ratesInput_TextBox).get(0), "value", String(Rate1), "ratesInput_TextBox week 1")
            });

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step 14------------
    it('Select a record , enter rates in data points(rate card input fields in the grid)', async () => {
        let stepAction = 'Select a record , enter rates in data points(rate card input fields in the grid)';
        let stepData = '';
        let stepExpResult = 'Record should be selected and  rates should be taken in data points';
        let stepActualResult = 'Record is selected and rates are taken in data points';
        try {
            Rate1 = 100;
            await action.ClearText(element.all(rateEntry.ratesInput_TextBox).get(2), "ratesInput_TextBox week 1")
            await action.SetText(element.all(rateEntry.ratesInput_TextBox).get(2), String(Rate1), "ratesInput_TextBox week 1")
            await element.all(rateEntry.ratesInput_TextBox).get(2).sendKeys(protractor.Key.TAB)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 15------------
    it('Check the user is warned when they attempt to navigate away from a rate entry window after changes have been made in the window', async () => {

        let stepAction = 'Check the user is warned when they attempt to navigate away from a rate entry window after changes have been made in the window';
        let stepData = '';
        let stepExpResult = 'User should get a confirmation pop-up window when try to away from a rate entry window after changes have been made in rate entry view grid';
        let stepActualResult = 'User gets a confirmation pop-up window when try to away from a rate entry window after changes have been made in rate entry view grid';

        try {
            await action.Click(rateEntry.button_Search, "button_Search")
            await verify.verifyElement(rateEntry.cancelUpdatesPopup, "cancelUpdatesPopup")
            await action.Click(rateEntry.button_No, "button_No")
            await action.SwitchToDefaultFrame()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 16------------
    it('Select a record , enter rates in data points', async () => {
        let stepAction = 'Select a record , enter rates in data points';
        let stepData = '';
        let stepExpResult = 'Record should be selected and  rates should be taken in data points';
        let stepActualResult = 'Record is selected and rates are taken in data points';
        try {
            Rate1 = 100;
            await action.ClearText(element.all(rateEntry.ratesInput_TextBox).get(2), "ratesInput_TextBox week 1")
            await action.SetText(element.all(rateEntry.ratesInput_TextBox).get(2), String(Rate1), "ratesInput_TextBox week 1")
            await element.all(rateEntry.ratesInput_TextBox).get(2).sendKeys(protractor.Key.TAB)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step 17------------
    it("Check the save/update button is available the rate entry grid to save the rates", async () => {

        let stepAction = "Check the save/update button is available the rate entry grid to save the rates";
        let stepData = "";
        let stepExpResult = "User should be able to view Save/Update button in the grid to save the rate's";
        let stepActualResult = "User is able to view Save/Update button in the grid to save the rate's";

        try {
            netpercentage = 50
            await verify.verifyElement(rateEntry.button_Update, "Update Button verification");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step 18------------
    it("Select Start date and End date", async () => {

        let stepAction = "Select Start date and End date";
        let stepData = "";
        let stepExpResult = "Start date and end date should be selected  and should show selected date range releated program list in the rate entry view grid";
        let stepActualResult = "Start date and end date are selected  and showed selected date range releated program list in the rate entry view grid";

        try {
            await action.ClearText(rateEntry.startDate_Textbox, "startDate")
            await action.ClearText(rateEntry.endDate_Textbox, "EndDate")
            await action.SetText(rateEntry.startDate_Textbox, rateEntry.startDateRentryView, "startDate")
            await action.SetText(rateEntry.endDate_Textbox, rateEntry.endDateRentryView1, "endDate_Textbox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step 19------------
    it("click on Gross checkbox in Calculation Metrics", async () => {

        let stepAction = "click on Gross checkbox in Calculation Metrics";
        let stepData = "";
        let stepExpResult = "Gross checkbox should checked in Calculation Metrics";
        let stepActualResult = "Gross checkbox is checked in Calculation Metrics";

        try {
            await action.Click(rateEntry.gross_RadioButton, "gross_RadioButton")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
    // --------------Test Step 20------------
    it("Select a record , enter rates in data point", async () => {

        let stepAction = "Select a record , enter rates in data point";
        let stepData = "";
        let stepExpResult = "Record should be selected and  rate's should be taken in data point's";
        let stepActualResult = "Record is selected and  rate's is taken in data point's";

        try {
            Rate1 = 100;
            await action.ClearText(element.all(rateEntry.ratesInput_TextBox).get(0), "ratesInput_TextBox week 1")
            await action.SetText(element.all(rateEntry.ratesInput_TextBox).get(0), String(Rate1), "ratesInput_TextBox week 1")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
    // --------------Test Step 21------------
    it("Click on update button in the rate entry view grid", async () => {

        let stepAction = "Click on update button in the rate entry view grid";
        let stepData = "";
        let stepExpResult = "Rates should be updated/saved for the selected record";
        let stepActualResult = "Rates is updated/saved for the selected record";

        try {
            await action.Click(rateEntry.button_Update, "button_Update")
            await action.Click(rateEntry.button_Confirm, "button_Confirm");
            await browser.sleep(3000);
            await verify.verifyElementAttribute(element.all(rateEntry.ratesInput_TextBox).get(0), "value", String(Rate1), "ratesInput_TextBox week 1")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
    // --------------Test Step 22------------
    it("Check the rates for the non-base lengths (other than 30s spot lengths) are calculated correctly from the appropriate length factor group and rounding factor", async () => {

        let stepAction = "Check the rates for the non-base lengths (other than 30s spot lengths) are calculated correctly from the appropriate length factor group and rounding factor";
        let stepData = "";
        let stepExpResult = "Rate should be calculated correctly for the non-base lengths (other than 30s spot lengths) from the  appropriate length factor group and rounding factor";
        let stepActualResult = "Rate is calculated correctly for the non-base lengths (other than 30s spot lengths) from the  appropriate length factor group and rounding factor";

        try {
            browser.sleep(3000);
            await action.MouseMoveToElement(rateEntry.Channel, "");
            await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-spotlength", "5", "spot Length (seconds)");
            await action.Click(rateEntry.button_Search, "SearchButton");
            await action.MouseMoveToElement(element.all(rateEntry.ratesInput_TextBox).get(2), "rate card");
            await browser.sleep(3000);
            await verify.verifyElementAttribute(element.all(rateEntry.ratesInput_TextBox).get(2), 'value', "1237700", 'Lenth factor 5');
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
    // --------------Test Step 23------------
    it("Select Start date and End date", async () => {

        let stepAction = "Select Start date and End date";
        let stepData = "";
        let stepExpResult = "Start date and end date should be selected";
        let stepActualResult = "Start date and end date is selected";

        try {
            await action.ClearText(rateEntry.startDate_Textbox, "startDate")
            await action.ClearText(rateEntry.endDate_Textbox, "startDate")
            await action.SetText(rateEntry.startDate_Textbox, rateEntry.startDateRentryView, "startDate")
            await action.SetText(rateEntry.endDate_Textbox, rateEntry.endDateRentryView1, "endDate_Textbox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step 24------------
    it('"click on net checkbox in Calculation Metrics"', async () => {

        let stepAction = 'click on net checkbox in Calculation Metrics';
        let stepData = '';
        let stepExpResult = 'Net checkbox should checked in Calculation Metrics';
        let stepActualResult = 'Net checkbox is checked in Calculation Metrics';

        try {
            netpercentage = 50
            Rate1 = 100;
            await browser.sleep(2000);
            await action.ClearText(element.all(rateEntry.ratesInput_TextBox).get(0), "ratesInput_TextBox week 1");
            await browser.sleep(2000);
            await action.SetText(element.all(rateEntry.ratesInput_TextBox).get(0), String(Rate1), "ratesInput_TextBox week 1")
            await console.log(String(Rate1) + " Test")
            await browser.sleep(2000);

            browser.call(async function () {
                await browser.sleep(2000);
                await element.all(rateEntry.ratesInput_TextBox).get(0).sendKeys(protractor.Key.TAB)
                await action.Click(rateEntry.button_Update, "button_Update");
                await action.Click(rateEntry.button_Confirm, "button_Confirm");
                await browser.sleep(3000);
            });

            await verify.verifyElementAttribute(element.all(rateEntry.ratesInput_TextBox).get(0), "value", "1237700", "ratesInput_TextBox week 1")
            await verify.verifyElement(rateEntry.netOption, "netOption")
            browser.sleep(1000);
            await action.Click(rateEntry.netOption, "netOption")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });



    // --------------Test Step 25------------
    it('Enter Net % and click on search', async () => {

        let stepAction = 'Enter Net % and click on search';
        let stepData = '';
        let stepExpResult = 'Net% should be take and should show selected date range releated program list in the rate entry view grid';
        let stepActualResult = 'Net% is taken and showed selected date range releated program list in the rate entry view grid';

        try {
            await action.SetText(rateEntry.enterNetpercentage, String(netpercentage), "enterNetpercentage")
            await action.Click(rateEntry.calculate, "calculate")
            await browser.sleep(1500);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 26------------
    it('Check the data points rate values are changed in the rate entry view grid based on Net%  value in Net% field', async () => {

        let stepAction = 'Check the data points rate values are changed in the rate entry view grid based on Net%  value in Net% field';
        let stepData = '';
        let stepExpResult = 'User should be able to view changed values in rate entry view grid based on net % value';
        let stepActualResult = 'User is able to view changed values in rate entry view grid based on net % value';

        try {
            let rate3 = 1237700;
            let RevisedRate1 = ((rate3 * netpercentage) / 100)
            await console.log(RevisedRate1 + " RevisedRate1");
            await verify.verifyTextOfElement(rateEntry.weekprogram1rateInGrid, String(RevisedRate1.toFixed(2)), "ratesInput_TextBox week 1")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
});
