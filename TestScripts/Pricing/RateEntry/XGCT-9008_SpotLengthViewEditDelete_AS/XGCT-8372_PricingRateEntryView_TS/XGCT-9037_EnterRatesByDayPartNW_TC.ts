/*
***********************************************************************************************************
* @Script Name :  XGCT-9037
* @Description :  Enter Rates by Rate Card - Daypart (Network)
* @Page Object Name(s) : Rate Entry
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 6-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, protractor } from 'protractor';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { Reporter } from '../../../../../Utility/htmlResult';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { AppCommonFunctions } from '../../../../../Libs/ApplicationLibs/CommAppLib';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { RateEntry } from '../../../../../Pages/Pricing/RateEntry';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let homePage = new HomePageFunction();
let appCommFunction = new AppCommonFunctions();
let rateEntry = new RateEntry();

//Variables Declaration
let TestCase_ID = 'XGCT-9037'
let TestCase_Title = 'Enter Rates by Rate Card - Daypart (Network)'
let pricing = "Pricing";
let NetworkRateEntry = "Network Rate Entry";
let Rate1: number
let Rate2: number
let netpercentage: number

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - ENTER RATES BY RATE CARD - DAYPART (Network)', () => {


    // --------------Test Step 1------------
    it("Navigate to the application URL", async () => {

        let stepAction = "Navigate to the application URL";
        let stepData = "";
        let stepExpResult = "User should be navigated to the application";
        let stepActualResult = "User is navigated to the application";
        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    // --------------Test Step 2------------
    it("Navigate to  Pricing > Network Rate entry", async () => {

        let stepAction = "Navigate to  Pricing > Network Rate entry";
        let stepData = "";
        let stepExpResult = "Check the  rate entry View page is displayed";
        let stepActualResult = "Rate entry View page is displayed";
        try {
            await homePage.appclickOnNavigationList(pricing);
            await homePage.appclickOnMenuListLeftPanel(NetworkRateEntry);
            await verify.verifyElementIsDisplayed(appCommFunction.pageHeader, " Rate Entry");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    // --------------Test Step 4------------
    it("Select Network", async () => {

        let stepAction = "Select Network";
        let stepData = "Fox";
        let stepExpResult = "Fox Network should be selected and should display selected market related channel list";
        let stepActualResult = "Fox Network should be selected and should display selected market related channel list";

        try {
            await rateEntry.VerifyDropDown(rateEntry.NetworkMultiSelectDropdwon, rateEntry.DropDownOptions, rateEntry.networkName, "Network");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 6------------
    it("Select Rates By as Daypart", async () => {

        let stepAction = "Select Rates By as Daypart";
        let stepData = "";
        let stepExpResult = "Rates By Daypart should be selected";
        let stepActualResult = "Rates By Daypart is selected";

        try {
            await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-ratesby", "Daypart", "Rates By");
            await browser.sleep(1000);
            await rateEntry.selectAllInMultiselectDropdown(rateEntry.Daypart, "rateEntry DayParts");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 7------------
    it("Select Base Length (seconds)", async () => {

        let stepAction = "Select Base Length (seconds)";
        let stepData = "30";
        let stepExpResult = "Base length(seconds) should be selected";
        let stepActualResult = "Base length(seconds) is selected";

        try {
            await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-baselength", "30", "Base Length");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 8------------
    it("Select Start Date and End date . Click on search", async () => {

        let stepAction = "Select Start Date and End date . Click on search";
        let stepData = "";
        let stepExpResult = "Start date and end date should be selected  and should show selected date range releated program list in the rate entry view grid";
        let stepActualResult = "Start date and end date are selected  and showed selected date range releated program list in the rate entry view grid";

        try {
            await action.ClearText(rateEntry.startDate_Textbox, "startDate")
            await action.SetText(rateEntry.startDate_Textbox, rateEntry.startDateRentryView, "startDate")
            await action.SetText(rateEntry.endDate_Textbox, rateEntry.endDateRentryView1, "endDate_Textbox")
            await browser.sleep(3000);
            await action.Click(rateEntry.button_Search, "SearchButton");
            await rateEntry.GridValidationsUpdated("Channel", rateEntry.channelsToBeVerifiedInGrid)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });



    // --------------Test Step 9------------
    it("Check user can enter a 'Base' length  rate for the selected programs in data field", async () => {

        let stepAction = "Check user can enter a 'Base' length  rate for the selected programs in data field";
        let stepData = "";
        let stepExpResult = "User should have option to enter rate for base length's";
        let stepActualResult = "User have option to enter rate for base length's";

        try {
            await verify.verifyElement(rateEntry.baseLengthDropdown, "baselengthDropdown")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 10------------
    it("Check the user is ability to change the 'Base' length", async () => {

        let stepAction = "Check the user is ability to change the 'Base' length";
        let stepData = "";
        let stepExpResult = "User should have option to change 'Base' length";
        let stepActualResult = "User have an option to change 'Base' length";

        try {
            await action.Click(rateEntry.baseLengthDropdown, "baseLengthDropdown")
            await verify.verifyElement(rateEntry.baseLengthDropdownOptions, "baseLengthDropdownOptions")
            await action.Click(rateEntry.baseLengthDropdown, "baseLengthDropdown")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 11------------
    it("Check the user is  provided a field for updating the rate of the base length for the selected date range", async () => {

        let stepAction = "Check the user is  provided a field for updating the rate of the base length for the selected date range";
        let stepData = "";
        let stepExpResult = "User should have field to update the rate of base length for the selected date range";
        let stepActualResult = "User have field to update the rate of base length for the selected date range";

        try {
            Rate1 = 100;
            Rate2 = 200;
            await action.ClearText(element.all(rateEntry.ratesInput_TextBox).get(0), "ratesInput_TextBox week 1")
            await action.ClearText(element.all(rateEntry.ratesInput_TextBox).get(1), "ratesInput_TextBox week 2")
            await action.SetText(element.all(rateEntry.ratesInput_TextBox).get(0), String(Rate1), "ratesInput_TextBox week 1")
            await action.SetText(element.all(rateEntry.ratesInput_TextBox).get(1), String(Rate2), "ratesInput_TextBox week 2")
            await action.Click(rateEntry.button_Update, "button_Update");
            await action.Click(rateEntry.button_Confirm, "button_Confirm");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 12------------
    it("Select a record , enter rates in data point for selected spot  length(30S)  and click on update", async () => {

        let stepAction = "Select a record , enter rates in data point for selected spot  length(30S)  and click on update";
        let stepData = "";
        let stepExpResult = "Record should be selected and rate should be updated sucessfully for 30s spot length";
        let stepActualResult = "Record is selected and rate is updated sucessfully for 30s spot length";

        try {
            Rate1 = 100;
            await action.ClearText(element.all(rateEntry.ratesInput_TextBox).get(0), "ratesInput_TextBox week 1")
            await action.SetText(element.all(rateEntry.ratesInput_TextBox).get(0), String(Rate1), "ratesInput_TextBox week 1")
            await action.Click(rateEntry.button_Update, "button_Update")
            await action.Click(rateEntry.button_Confirm, "button_Confirm");
            browser.call(async function () {
                await browser.sleep(3000);
                await verify.verifyElementAttribute(element.all(rateEntry.ratesInput_TextBox).get(0), "value", String(Rate1), "ratesInput_TextBox week 1")
            });

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 13------------
    it('Check the user is warned when they attempt to navigate away from a rate entry window after changes have been made in the window', async () => {

        let stepAction = 'Check the user is warned when they attempt to navigate away from a rate entry window after changes have been made in the window';
        let stepData = '';
        let stepExpResult = 'User should get a confirmation pop-up window when try to away from a rate entry window after changes have been made in rate entry view grid';
        let stepActualResult = 'User gets a confirmation pop-up window when try to away from a rate entry window after changes have been made in rate entry view grid';

        try {
            await action.ClearText(element.all(rateEntry.ratesInput_TextBox).get(0), "ratesInput_TextBox week 1")
            await action.SetText(element.all(rateEntry.ratesInput_TextBox).get(0), String(Rate1), "ratesInput_TextBox week 1")
            await action.Click(rateEntry.button_Search, "button_Search")
            await verify.verifyElement(rateEntry.cancelUpdatesPopup, "cancelUpdatesPopup")
            await action.Click(rateEntry.button_No, "button_No")
            await action.SwitchToDefaultFrame()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step 14------------
    it('Select Base Length (seconds)', async () => {

        let stepAction = 'Select Base Length (seconds)';
        let stepData = '';
        let stepExpResult = 'Base length(seconds) should be selected';
        let stepActualResult = 'Base length(seconds) should be selected';

        try {
            await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-baselength", "30", "Base Length (seconds)")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 15------------
    it("Select Start date and End date", async () => {

        let stepAction = "Select Start date and End date";
        let stepData = "";
        let stepExpResult = "Start date and end date should be selected  and should show selected date range releated program list in the rate entry view grid";
        let stepActualResult = "Start date and end date are selected  and showed selected date range releated program list in the rate entry view grid";

        try {
            await action.ClearText(rateEntry.startDate_Textbox, "startDate")
            await action.ClearText(rateEntry.endDate_Textbox, "EndDate")
            await action.SetText(rateEntry.startDate_Textbox, rateEntry.startDateRentryView, "startDate")
            await action.SetText(rateEntry.endDate_Textbox, rateEntry.endDateRentryView1, "endDate_Textbox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 17------------
    it("click on Gross checkbox in Calculation Metrics", async () => {

        let stepAction = "click on Gross checkbox in Calculation Metrics";
        let stepData = "";
        let stepExpResult = "Gross checkbox should checked in Calculation Metrics";
        let stepActualResult = "Gross checkbox is checked in Calculation Metrics";

        try {
            await action.Click(rateEntry.gross_RadioButton, "gross_RadioButton")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
    // --------------Test Step 18------------
    it("Check the rates for the non-base lengths (other than 30s spot lengths) are calculated correctly from the appropriate length factor group and rounding factor", async () => {

        let stepAction = "Check the rates for the non-base lengths (other than 30s spot lengths) are calculated correctly from the appropriate length factor group and rounding factor";
        let stepData = "";
        let stepExpResult = "Rate should be calculated correctly for the non-base lengths (other than 30s spot lengths) from the  appropriate length factor group and rounding factor";
        let stepActualResult = "Rate is calculated correctly for the non-base lengths (other than 30s spot lengths) from the  appropriate length factor group and rounding factor";

        try {
            await action.MouseMoveToElement(rateEntry.NetworkMultiSelectDropdwon, "");
            await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-baselength", "30", "Base Length (seconds)");
            await action.Click(rateEntry.button_Search, "SearchButton");
            await verify.verifyElement(rateEntry.cancelUpdatesPopup, "cancelUpdatesPopup")
            await action.Click(rateEntry.button_No, "button_No")
            await action.MouseMoveToElement(element.all(rateEntry.ratesInput_TextBox).get(0), "rate card");
            await browser.sleep(3000);
            await verify.verifyElementAttribute(element.all(rateEntry.ratesInput_TextBox).get(0), 'value', "100", 'Lenth factor 5');
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
    // --------------Test Step 16/19------------
    it('"click on net checkbox in Calculation Metrics"', async () => {

        let stepAction = 'click on net checkbox in Calculation Metrics';
        let stepData = '';
        let stepExpResult = 'Net checkbox should checked in Calculation Metrics';
        let stepActualResult = 'Net checkbox is checked in Calculation Metrics';

        try {
            netpercentage = 50
            Rate1 = 100;
            await browser.sleep(2000);
            await action.ClearText(element.all(rateEntry.ratesInput_TextBox).get(0), "ratesInput_TextBox week 1");
            await browser.sleep(2000);
            await action.SetText(element.all(rateEntry.ratesInput_TextBox).get(0), String(Rate1), "ratesInput_TextBox week 1")
            await console.log(String(Rate1) + " Test")
            await browser.sleep(2000);

            browser.call(async function () {
                await browser.sleep(2000);
                await element.all(rateEntry.ratesInput_TextBox).get(0).sendKeys(protractor.Key.TAB)
                await action.Click(rateEntry.button_Update, "button_Update");
                await action.Click(rateEntry.button_Confirm, "button_Confirm");
                await browser.sleep(3000);
            });

            await verify.verifyElementAttribute(element.all(rateEntry.ratesInput_TextBox).get(0), "value", String(Rate1), "ratesInput_TextBox week 1")
            await verify.verifyElement(rateEntry.netOption, "netOption")
            await action.Click(rateEntry.netOption, "netOption")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });



    // --------------Test Step 20-----------
    it('Enter Net % and click on search', async () => {

        let stepAction = 'Enter Net % and click on search';
        let stepData = '';
        let stepExpResult = 'Net% should be take and should show selected date range releated program list in the rate entry view grid';
        let stepActualResult = 'Net% is taken and showed selected date range releated program list in the rate entry view grid';

        try {
            await action.SetText(rateEntry.enterNetpercentage, String(netpercentage), "enterNetpercentage")
            await action.Click(rateEntry.calculate, "calculate")
            await browser.sleep(1500);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 21------------
    it('Check the data points rate values are changed in the rate entry view grid based on Net%  value in Net% field', async () => {

        let stepAction = 'Check the data points rate values are changed in the rate entry view grid based on Net%  value in Net% field';
        let stepData = '';
        let stepExpResult = 'User should be able to view changed values in rate entry view grid based on net % value';
        let stepActualResult = 'User is able to view changed values in rate entry view grid based on net % value';

        try {
            let rate1 = 100;
            let RevisedRate1 = ((rate1 * netpercentage) / 100)
            await console.log(RevisedRate1 + " RevisedRate1");
            await verify.verifyTextOfElement(rateEntry.weekprogram1rateInGrid, String(RevisedRate1.toFixed(2)), "ratesInput_TextBox week 1")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
});
