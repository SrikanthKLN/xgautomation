/*
***********************************************************************************************************
* @Script Name :  XGCT-8447
* @Description :  xG Campaign - Spot length - Edit9-8-2019
* @Page Object Name(s) : LocalLengthFactors,RateEntry
* @Dependencies/Libs : ActionLib,VerifyLib
* @Pre-Conditions : 
* @Creation Date : 9-8-2019
* @Author : Sivaraj
* @Modified By & Date:12-8-2019
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { RateEntry } from '../../../../../Pages/Pricing/RateEntry';
import { LocalLengthFactorsPricing } from '../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../TestData/globalTestData';
import { AppCommonFunctions } from '../../../../../Libs/ApplicationLibs/CommAppLib';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let homePage = new HomePageFunction();
let rateEntry = new RateEntry();
let localLF = new LocalLengthFactorsPricing();
let commonLib = new AppCommonFunctions();

//Variables Declaration
let TestCase_ID = 'XGCT-8447'
let TestCase_Title = 'xG Campaign - Spot length - Edit'
let spotlength = "13" + Math.floor(Math.random() * 2);

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - SPOT LENGTH - EDIT', () => {
        // --------------Test Step------------
        it("Navigate to the application https://xgplatform-dev.myimagine.com/#/", async () => {

                let stepAction = "Navigate to the application https://xgplatform-dev.myimagine.com/#/";
                let stepData = "";
                let stepExpResult = "User should be navigated to the application";
                let stepActualResult = "User able to navigated to the application";

                try {
                        await global.LaunchStoryBook();
                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });


        // --------------Test Step------------
        it("Login as a local admin user", async () => {

                let stepAction = "Login as a local admin user";
                let stepData = "";
                let stepExpResult = "Local admin user should be able to login sucessfully";
                let stepActualResult = "Local admin user page is opened";

                try {
                        await homePage.appclickOnNavigationList(globalTestData.pricingTabName);
                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });


        // --------------Test Step------------
        it("Navigate to  Pricing>Spot Length View", async () => {

                let stepAction = "Navigate to  Pricing>Spot Length View";
                let stepData = "";
                let stepExpResult = "Check the Spot Length View page is displayed";
                let stepActualResult = "Spot Length View page is displayed";

                try {
                        await homePage.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
                        await homePage.appclickOnMenuListLeftPanel(globalTestData.leftMenuLocalSpotLengths);
                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });


        // --------------Test Step------------
        it("Select Market and Channel", async () => {

                let stepAction = "Select Market and Channel";
                let stepData = "'Market as ''Pittsburgh'' Channel as ''WPGH'''";
                let stepExpResult = "'User able to select Market as ''Pittsburgh'' and channel as ''WPGH''.User should be able to see Spot length and Spot length factor values in the grid based on the selected market.'";
                let stepActualResult = "'User able to select Market as ''Pittsburgh'' and channel as ''WPGH''.User able to see Spot length and Spot length factor values in the grid based on the selected market.'";

                try {
                        await commonLib.selectDropDownValue("Market", globalTestData.marketName);
                        await commonLib.selectMultiOptionsFromDropDown("channel", [globalTestData.multipleChnnels[0]]);
                        await verify.verifyProgressBarNotPresent();
                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });


        // --------------Test Step------------
        it("Check the user is provided with a view to add any user specified spot length.", async () => {

                let stepAction = "Check the user is provided with a view to add any user specified spot length.";
                let stepData = "";
                let stepExpResult = "User should be provided with a view to add spot length.";
                let stepActualResult = "User able to provided with a view to add spot length.";

                try {
                        await verify.verifyElement(localLF.spotLenthadd, "");
                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });


        // --------------Test Step------------
        it("Click on Add spot length. Select one channel and give a Name and click on SAVE button.", async () => {

                let stepAction = "Click on Add spot length. Select one channel and give a Name and click on SAVE button.";
                let stepData = "";
                let stepExpResult = "User should be able to add Spot length successful and able to view in the grid.";
                let stepActualResult = "User able to add Spot length successful and able to view in the grid.";

                try {
                        await action.Click(localLF.spotLenthadd, "");
                        await action.SetText(localLF.addSpot, spotlength, "")
                        await action.SetText(localLF.addLength, "123", "")
                        await action.Click(localLF.save, "");
                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });


        // --------------Test Step------------
        it("Select  any spot length and click on Edit", async () => {

                let stepAction = "Select  any spot length and click on Edit";
                let stepData = "";
                let stepExpResult = "Edit Spot Length pop up should be open.";
                let stepActualResult = "Edit Spot Length pop up is open.";

                try {
                        await commonLib.verifyPopupMessage("Spot Length Added Successfully");
                        await action.waitForElementInvisible(localLF.addSpot,60);
                        await commonLib.selectDropDownValue("Market", globalTestData.marketName);
                        await commonLib.selectMultiOptionsFromDropDown("channel", [globalTestData.multipleChnnels[0]]);
                        await verify.verifyProgressBarNotPresent();
                        await action.SetText(localLF.searchspotlength, spotlength, "");
                        await browser.sleep(2000)
                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });

        // --------------Test Step------------
        it("Change the value of spot length to 99999", async () => {

                let stepAction = "Change the value of spot length to 99999";
                let stepData = "";
                let stepExpResult = "'User should be able to view validation message as ''Number should be between 1 - 86399'''";
                let stepActualResult = "'User able to view validation message as ''Number should be between 1 - 86399'''";

                try {
                        await action.ClearText(localLF.editSpotLength, "")
                        await action.SetText(localLF.editSpotLength, "99999", "")
                        await browser.sleep(5000)
                        await element(localLF.editSpotLength).getAttribute('value').then(async function (text) {
                                if (text == '9999') {
                                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                                }
                                else {
                                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, 'Validation failed');
                                }
                        })
                }
                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });


        // --------------Test Step------------
        it("Change the value of spot length to 0", async () => {

                let stepAction = "Change the value of spot length to 0";
                let stepData = "";
                let stepExpResult = "'User should be able to view validation message as ''Number should be between 1 - 86399'''";
                let stepActualResult = "'User able to view validation message as ''Number should be between 1 - 86399'''";
                try {
                        await action.ClearText(localLF.editSpotLength, "")
                        await action.SetText(localLF.editSpotLength, "0", "")
                        await browser.sleep(5000)
                        await element(localLF.editSpotLength).getAttribute('value').then(async function (text) {
                                if (text == '') {
                                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                                }
                                else {
                                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, 'Validation failed');
                                }
                        })
                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }
        });


        // --------------Test Step------------
        it("Change the value of spot length to 77", async () => {

                let stepAction = "Change the value of spot length to 77";
                let stepData = "";
                let stepExpResult = "User should be able to save the record successfully and changed value should be displayed in the grid.";
                let stepActualResult = "User able to save the record successfully and changed value should be displayed in the grid.";

                try {
                        await action.ClearText(localLF.editSpotLength, "")
                        await action.SetText(localLF.editSpotLength, "77", "")
                        await action.Click(localLF.selectSpotLength, "")
                        await action.Click(localLF.update, '')
                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });
        // --------------Test Step------------
        it("Login as a network  admin user", async () => {

                let stepAction = "Login as a network  admin user";
                let stepData = "";
                let stepExpResult = "Network admin user should be able to login sucessfully";
                let stepActualResult = "Network admin user able to login sucessfully";

                try {
                        await homePage.appclickOnMenuListLeftPanel(globalTestData.leftMenuNetworkSpotLengths);
                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });


        // --------------Test Step------------
        it("Navigate to  Pricing>Spot Length View", async () => {

                let stepAction = "Navigate to  Pricing>Spot Length View";
                let stepData = "";
                let stepExpResult = "Check the Spot Length View page is displayed";
                let stepActualResult = "Spot Length View page is displayed";

                try {

                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });


        // --------------Test Step------------
        it("Select Network", async () => {

                let stepAction = "Select Network";
                let stepData = "'Network as ''Fox'''";
                let stepExpResult = "User should be able to select Fox Network. User should be able to see Spot length and Spot length factor values in the grid based on the selected market.";
                let stepActualResult = "User able to select Fox Network. User should be able to see Spot length and Spot length factor values in the grid based on the selected market.";

                try {
                        await commonLib.selectDropDownValue("Network", globalTestData.networkName[0]);
                        await verify.verifyProgressBarNotPresent();
                        // await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, String(globalTestData.networkName[0]), "Network")
                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });


        // --------------Test Step------------
        it("Check the user is provided with a view to add any user specified spot length.", async () => {

                let stepAction = "Check the user is provided with a view to add any user specified spot length.";
                let stepData = "";
                let stepExpResult = "User should be provided with a view to add spot length.";
                let stepActualResult = "User provided with a view to add spot length.";

                try {
                        await verify.verifyElement(localLF.spotLenthadd, "");
                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });


        // --------------Test Step------------
        it("Click on Add spot length. Add any value in Spot length and Spot length factor fields", async () => {

                let stepAction = "Click on Add spot length. Add any value in Spot length and Spot length factor fields";
                let stepData = "";
                let stepExpResult = "User should be able to add Spot length successful and able to view in the grid.";
                let stepActualResult = "User able to add Spot length successful and able to view in the grid.";

                try {
                        await action.Click(localLF.spotLenthadd, "");
                        await action.SetText(localLF.addSpot, spotlength, "")
                        await action.SetText(localLF.addLength, "123", "")
                        await action.Click(localLF.save, "");
                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });


        // --------------Test Step------------
        it("Select  any spot length and click on Edit", async () => {

                let stepAction = "Select  any spot length and click on Edit";
                let stepData = "";
                let stepExpResult = "Edit Spot Length pop up should be open.";
                let stepActualResult = "Spot Length pop up is open.";

                try {
                        await action.SetText(localLF.searchspotlength, spotlength, "");
                        await browser.sleep(2000)
                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });


        // --------------Test Step------------
        it("Change the value of spot length to 99999", async () => {

                let stepAction = "Change the value of spot length to 99999";
                let stepData = "";
                let stepExpResult = "'User should be able to view validation message as ''Number should be between 1 - 86399'''";
                let stepActualResult = "'User able to view validation message as ''Number should be between 1 - 86399'''";

                try {

                        await action.ClearText(localLF.editSpotLength, "")
                        await action.SetText(localLF.editSpotLength, "99999", "")
                        await browser.sleep(5000)
                        await element(localLF.editSpotLength).getAttribute('value').then(async function (text) {
                                //await console.log("*************" + text)
                                // expect(9999).toBe(text)
                                if (text == '9999') {
                                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                                }
                                else {
                                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, 'Validation failed');
                                }
                                report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                        })
                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });


        // --------------Test Step------------
        it("Change the value of spot length to 0", async () => {

                let stepAction = "Change the value of spot length to 0";
                let stepData = "";
                let stepExpResult = "'User should be able to view validation message as ''Number should be between 1 - 86399'''";
                let stepActualResult = "'User able to view validation message as ''Number should be between 1 - 86399'''";

                try {
                        await action.ClearText(localLF.editSpotLength, "")
                        await action.SetText(localLF.editSpotLength, "0", "")
                        await browser.sleep(5000)
                        await element(localLF.editSpotLength).getAttribute('value').then(async function (text) {
                                //await console.log("*************" + text)
                                // expect('').toBe(text)
                                if (text == '') {
                                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                                }
                                else {
                                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, 'Validation failed');
                                }
                        })

                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });


        // --------------Test Step------------
        it("Change the value of spot length to 77", async () => {

                let stepAction = "Change the value of spot length to 77";
                let stepData = "";
                let stepExpResult = "User should be able to save the record successfully and changed value should be displayed in the grid.";
                let stepActualResult = "User  able to save the record successfully and changed value should be displayed in the grid.";

                try {
                        await action.ClearText(localLF.editSpotLength, "")
                        await action.SetText(localLF.editSpotLength, "77", "")
                        await action.Click(localLF.selectSpotLength, "")
                        await action.Click(localLF.update, '')
                        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
                }

                catch (err) {
                        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
                }

        });

});
