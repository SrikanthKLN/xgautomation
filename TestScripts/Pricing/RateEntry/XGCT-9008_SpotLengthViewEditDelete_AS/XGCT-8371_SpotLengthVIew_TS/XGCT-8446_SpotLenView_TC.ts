/*
***********************************************************************************************************
* @Script Name :  XGCT-8446
* @Description :  xG Campaign - Spot length - View9-8-2019
* @Page Object Name(s) : LocalLengthFactors,RateEntry
* @Dependencies/Libs : ActionLib,VerifyLib
* @Pre-Conditions : 
* @Creation Date : 9-8-2019
* @Author : Sivaraj
* @Modified By & Date:12-8-2019
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { RateEntry } from '../../../../../Pages/Pricing/RateEntry';
import { LocalLengthFactorsPricing } from '../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../TestData/globalTestData';
import { AppCommonFunctions } from '../../../../../Libs/ApplicationLibs/CommAppLib';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let homePage = new HomePageFunction();
let rateEntry = new RateEntry();
let localLF = new LocalLengthFactorsPricing();
let commonLib = new AppCommonFunctions();
//Variables Declaration
let TestCase_ID = 'XGCT-8446'
let TestCase_Title = 'xG Campaign - Spot length - View'

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - SPOT LENGTH - VIEW', () => {

    // --------------Test Step------------
    it("Navigate to the application https://xgplatform-dev.myimagine.com/#/", async () => {

        let stepAction = "Navigate to the application https://xgplatform-dev.myimagine.com/#";
        let stepData = "";
        let stepExpResult = "User should be navigated to the application";
        let stepActualResult = "User able to navigated to the application";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step------------
    it("Login as a local admin user", async () => {

        let stepAction = "Login as a local admin user";
        let stepData = "";
        let stepExpResult = "Local admin user should be able to login sucessfully";
        let stepActualResult = "Local admin user able to login sucessfully";

        try {
            await homePage.appclickOnNavigationList(globalTestData.pricingTabName);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step------------
    it("Navigate to  Pricing>Spot Length View", async () => {

        let stepAction = "Navigate to  Pricing>Spot Length View";
        let stepData = "";
        let stepExpResult = "Check the Spot Length View page is displayed";
        let stepActualResult = "Spot Length View page is displayed";

        try {
            await homePage.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await homePage.appclickOnMenuListLeftPanel(globalTestData.leftMenuLocalSpotLengths);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Market and Channel", async () => {

        let stepAction = "Select Market and Channel";
        let stepData = "'Market as ''Pittsburgh'' Channel as ''WPGH'''";
        let stepExpResult = "'User able to select Market as ''Pittsburgh'' and channel as ''WPGH''.User should be able to see Spot length and Spot length factor values in the grid based on the selected market, channel.'";
        let stepActualResult = "'User able to select Market as ''Pittsburgh'' and channel as ''WPGH''.User able to see Spot length and Spot length factor values in the grid based on the selected market, channel.'";

        try {
            await commonLib.selectDropDownValue("Market", globalTestData.marketName)
            await verify.verifyProgressBarNotPresent();

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step------------
    it("Check the spot lengths are sorted by base length (30s) first then in sequential order", async () => {

        let stepAction = "Check the spot lengths are sorted by base length (30s) first then in sequential order";
        let stepData = "";
        let stepExpResult = "User should be able to see spot lengths are sorted by base length (30s) first then in sequential order.";
        let stepActualResult = "User able to see spot lengths are sorted by base length (30s) first then in sequential order.";

        try {
            await rateEntry.ColumnContainGivenData("Spot Length", "30")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Select Market & more than one channel click on search. Then check channel header display in the grid.", async () => {

        let stepAction = "Select Market & more than one channel click on search. Then check channel header display in the grid.";
        let stepData = "'Market as ''Pittsburgh'' & Channel ''WPGH'' & ''WPNT'''";
        let stepExpResult = "User should be able to see channel header in the grid.";
        let stepActualResult = "User able to see channel header in the grid.";

        try {
            await commonLib.deSelectAllMultiDropdownOptions("channel");
            await commonLib.selectMultiOptionsFromDropDown("channel", globalTestData.multipleChnnels);
            await verify.verifyProgressBarNotPresent();
            await rateEntry.ColumnNameValidations(["Channel"])
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step------------
    it("Select Market & only one channel and click on search. Then check the channel header display in the grid.", async () => {

        let stepAction = "Select Market & only one channel and click on search. Then check the channel header display in the grid.";
        let stepData = "'Market as ''Pittsburgh'' & Channel as ''WPGH'''";
        let stepExpResult = "User should not be able to see channel header in the grid.";
        let stepActualResult = "User not able to see channel header in the grid.";

        try {
            await browser.executeScript("window.scrollTo(0,0)")
            await commonLib.unSelectMultiOptionsFromDropDown("channel", globalTestData.singleChannel1);
            await verify.verifyProgressBarNotPresent();
            await rateEntry.ColumnNameNotDisplayedValidations(["Channel"])
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the user is able to filter the records", async () => {

        let stepAction = "Check the user is able to filter the records";
        let stepData = "Select Filters - Start with, Contains, Ends with, Does not contain,  Does not contain, Equal, Not equal";
        let stepExpResult = "User should be able to filter the records.";
        let stepActualResult = "User able to filter the records.";

        try {

            await action.Click(rateEntry.pDropdown, "")
            await action.Click(rateEntry.filterWithGreaterthan, "")
            await action.SetText(rateEntry.searchText, "30", "")
            await verify.verifyProgressBarNotPresent();
            await rateEntry.GridValidationsUpdated2("Spot Length", "30", "GT")
            await action.Click(rateEntry.closeSearch, "")
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the user able to search for the records", async () => {

        let stepAction = "Check the user able to search for the records";
        let stepData = "";
        let stepExpResult = "User should be able to search the records.";
        let stepActualResult = "User able to search the records.";

        try {
            await action.SetText(rateEntry.searchText, "30", "");
            await verify.verifyProgressBarNotPresent();
            await rateEntry.GridValidationsUpdated2("Spot Length", "30", "EQ");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Check the user can view spot length factor", async () => {

        let stepAction = "Check the user can view spot length factor";
        let stepData = "";
        let stepExpResult = "User should be able to view spot length factor in the grid.";
        let stepActualResult = "User able to view spot length factor in the grid.";

        try {
            await rateEntry.ColumnNameValidations(["Spot Length Factor"]);
            await action.Click(rateEntry.closeSearch, "");
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Check the user is able to sort the records in the grid.", async () => {

        let stepAction = "Check the user is able to sort the records in the grid.";
        let stepData = "";
        let stepExpResult = "User should be able to sort the records in the grid.";
        let stepActualResult = "User able to sort the records in the grid.";

        try {
            await action.Click(rateEntry.clickSort, "");
            await rateEntry.verifysortorderforspotlengths();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Login as a network  admin user", async () => {

        let stepAction = "Login as a network  admin user";
        let stepData = "";
        let stepExpResult = "Network admin user should be able to login sucessfully";
        let stepActualResult = "Network admin user able to login sucessfully";

        try {
            await homePage.appclickOnMenuListLeftPanel(globalTestData.leftMenuNetworkSpotLengths);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Review the grid.", async () => {

        let stepAction = "Review the grid.";
        let stepData = "";
        let stepExpResult = "'User should be able to see ''Spot length and Spot length factor'' values in the grid based on the selected market.'";
        let stepActualResult = "'User able to see ''Spot length and Spot length factor'' values in the grid based on the selected market.'";

        try {

            await rateEntry.ColumnContainGivenData("Spot Length", "30")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("'Review ''Spot Length'' and ''Spot Length Factor'' columns.'", async () => {

        let stepAction = "'Review ''Spot Length'' and ''Spot Length Factor'' columns.'";
        let stepData = "";
        let stepExpResult = "'Expected values should be displayed on ''Spot Length'' &  Spot Length Factor'' columns.'";
        let stepActualResult = "'Expected values displayed on ''Spot Length'' &  Spot Length Factor'' columns.'";

        try {
            await rateEntry.ColumnNameValidations(["Spot Length"])
            await rateEntry.ColumnNameValidations(["Spot Length Factor"])
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click column header[EX: Spot Length] and perform sorting.", async () => {

        let stepAction = "Click column header[EX: Spot Length] and perform sorting.";
        let stepData = "";
        let stepExpResult = "'Desire column values should be sorted correctly. ";
        let stepActualResult = "'Desire column values sorted correctly. ";

        try {
            await action.Click(rateEntry.clickSort, "")
            await rateEntry.verifysortorderforspotlengths();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Review and verify column headers are freezed.", async () => {

        let stepAction = "Review and verify column headers are freezed.";
        let stepData = "";
        let stepExpResult = "User is able to see freezed column headers while scrolling down the list.";
        let stepActualResult = "User is able to see freezed column headers while scrolling down the list.";

        try {

            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, '');
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Switch to another network and Review the grid data.", async () => {

        let stepAction = "Switch to another network and Review the grid data.";
        let stepData = "'Network as ''Fox'', ''NBC'''";
        let stepExpResult = "'User should be able to see expected desire Network data on the grid";
        let stepActualResult = "'User able to see expected desire Network data on the grid";

        try {
            await browser.executeScript("window.scrollTo(0,0)")
            await commonLib.selectDropDownValue("Network", globalTestData.networkName[1]);
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Review filter menu lists.", async () => {

        let stepAction = "Review filter menu lists.";
        let stepData = "";
        let stepExpResult = "'Following Filters should be displayed:";
        let stepActualResult = "'Following Filters  displayed:";

        try {
            await action.Click(rateEntry.pDropdown, "")
            await verify.verifyElement(rateEntry.filterWithGreaterthan, "")
            await verify.verifyElement(rateEntry.filterWithLessthan, "")
            await action.Click(rateEntry.pDropdown, "")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step------------
    it("Perform filter on the column and Review the grid data.", async () => {

        let stepAction = "Perform filter on the column and Review the grid data.";
        let stepData = "Desire filter data search: 100";
        let stepExpResult = "' User should be able to search the records.";
        let stepActualResult = "' User able to search the records.";

        try {
            await action.SetText(rateEntry.searchText, "30", "")
            await verify.verifyProgressBarNotPresent();
            await rateEntry.GridValidationsUpdated2("Spot Length", "30", "EQ")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
