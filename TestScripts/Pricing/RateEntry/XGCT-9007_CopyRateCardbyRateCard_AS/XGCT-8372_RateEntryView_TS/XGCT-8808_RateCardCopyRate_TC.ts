/*
***********************************************************************************************************
* @Script Name :  XGCT-8808
* @Description :  xG Campaign - Rate Card - Copy Rate2-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 2-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

 //Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { RateEntry } from '../../../../../Pages/Pricing/RateEntry';
import { LocalLengthFactorsPricing } from '../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../TestData/globalTestData';



//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let home=new HomePageFunction()
let global=new globalvalues();
let rateEntry=new RateEntry()
let localLF=new LocalLengthFactorsPricing();
new globalTestData()



//Variables Declaration
let TestCase_ID = 'XGCT-8808'
let TestCase_Title = 'xG Campaign - Rate Card - Copy Rate'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID,TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - RATE CARD - COPY RATE', () => {


// --------------Test Step------------
it("Navigate to the application http://xgcampaignwebapp.azurewebsites.net/", async () => {

let stepAction = "Navigate to the application http://xgcampaignwebapp.azurewebsites.net/";
let stepData = "";
let stepExpResult = "User should be navigated to the application";
let stepActualResult = "User should be navigated to the application";

try {
 await global.LaunchStoryBook();
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Navigate to  Pricing>Rate card >Rate Entry", async () => {

let stepAction = "Navigate to  Pricing>Rate card >Rate Entry";
let stepData = "";
let stepExpResult = "Check the  rate entry View page is displayed";
let stepActualResult = "Check the  rate entry View page is displayed";

try {
    await home.appclickOnNavigationList("Pricing");
    await home.appclickOnMenuListLeftPanel("Local Rate Entry");
    
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("login as local admin user", async () => {

let stepAction = "login as local admin user";
let stepData = "";
let stepExpResult = "Local admin should be login sucessfully";
let stepActualResult = "Local admin should be login sucessfully";

try {

report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select market", async () => {

let stepAction = "Select market";
let stepData = "Pittsburgh";
let stepExpResult = "Market should be selected and should display selected market releated channel list ";
let stepActualResult = "Market should be selected and should display selected market releated channel list ";

try {
    await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-markets", rateEntry.marketName, "Market")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select channel", async () => {

let stepAction = "Select channel";
let stepData = "WPGH";
let stepExpResult = "Channel should be selected ";
let stepActualResult = "Channel should be selected ";

try {
    await browser.sleep(1000)
    await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,globalTestData.multipleChnnels,"Channels")
     report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Rates By as spot length", async () => {

let stepAction = "Select Rates By as spot length";
let stepData = "";
let stepExpResult = "Rates By spot length should be selected a";
let stepActualResult = "Rates By spot length should be selected a";

try {
    await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-ratesby", "Spot Length", "Rates By")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select spot  Length(seconds) ", async () => {

let stepAction = "Select spot  Length(seconds) ";
let stepData = "30";
let stepExpResult = "Spot length(seconds) should be selected";
let stepActualResult = "Spot length(seconds) should be selected";

try {
    await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-spotlength", "30", "Spot Length (seconds)")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Start date and End date and click on search", async () => {

let stepAction = "Select Start date and End date and click on search";
let stepData = "";
let stepExpResult = "Start date and end date should be selected  and should show selected date range releated program list in the rate entry view grid";
let stepActualResult = "Start date and end date should be selected  and should show selected date range releated program list in the rate entry view grid";

try {
    await action.ClearText(rateEntry.startDate_Textbox, "startDate")
    await action.SetText(rateEntry.startDate_Textbox, rateEntry.startDateRentryView, "startDate")
    await action.SetText(rateEntry.endDate_Textbox, rateEntry.endDateRentryView1, "endDate_Textbox")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Check copy rates icon(Arrow icon) was avalible in rate entry view grid", async () => {

let stepAction = "Check copy rates icon(Arrow icon) was avalible in rate entry view grid";
let stepData = "";
let stepExpResult = "Copy  rates icon(Arrow icon) should be avalible in the rate entry view grid";
let stepActualResult = "Copy  rates icon(Arrow icon) should be avalible in the rate entry view grid";

try {
    await browser.waitForAngularEnabled(false)
    await action.Click(rateEntry.button_Search, "SearchButton")
    // await verify.verifyElement(element.all(rateEntry.ratesInput_TextBox).get(0), '10')
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Navigate to  Pricing>Rate card >Rate Entry", async () => {

let stepAction = "Navigate to  Pricing>Rate card >Rate Entry";
let stepData = "";
let stepExpResult = "Check the  rate entry View page is displayed";
let stepActualResult = "Check the  rate entry View page is displayed";

try {
  
    await home.appclickOnMenuListLeftPanel("Network Rate Entry");
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select  network admin in knobs", async () => {

let stepAction = "Select  network admin in knobs";
let stepData = "";
let stepExpResult = "Network admin should be login sucessfully";
let stepActualResult = "Network admin should be login sucessfully";

try {

report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select network", async () => {

let stepAction = "Select network";
let stepData = "FOX";
let stepExpResult = "Network should be selectd ";
let stepActualResult = "Network should be selectd ";

try {
    await browser.sleep(1000)
    await localLF.selectMultiSelectDropDownOptions(localLF.networkMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,globalTestData.networkName,"Channels")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Rates By as spot length", async () => {

let stepAction = "Select Rates By as spot length";
let stepData = "";
let stepExpResult = "Rates By spot length should be selected ";
let stepActualResult = "Rates By spot length should be selected ";

try {
    await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-ratesby", "Spot Length", "Rates By")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select spot  Length(seconds) ", async () => {

let stepAction = "Select spot  Length(seconds) ";
let stepData = "30";
let stepExpResult = "Spot length(seconds) should be selected";
let stepActualResult = "Spot length(seconds) should be selected";

try {
    await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-spotlength", "30", "Spot Length (seconds)")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Start date and End date and click on search", async () => {

let stepAction = "Select Start date and End date and click on search";
let stepData = "";
let stepExpResult = "Start date and end date should be selected  and should show selected date range releated program list in the rate entry view grid";
let stepActualResult = "Start date and end date should be selected  and should show selected date range releated program list in the rate entry view grid";

try {
    await action.ClearText(rateEntry.startDate_Textbox, "startDate")
    await action.SetText(rateEntry.startDate_Textbox, rateEntry.startDateRentryView, "startDate")
    await action.SetText(rateEntry.endDate_Textbox, rateEntry.endDateRentryView1, "endDate_Textbox")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Check copy rates icon(Arrow icon) was avalible in rate entry view grid", async () => {

let stepAction = "Check copy rates icon(Arrow icon) was avalible in rate entry view grid";
let stepData = "";
let stepExpResult = "Copy  rates icon(Arrow icon) should be avalible in the rate entry view grid";
let stepActualResult = "Copy  rates icon(Arrow icon) should be avalible in the rate entry view grid";

try {
    await browser.sleep(3000)
    await action.Click(rateEntry.button_Search, "SearchButton")
    // await verify.verifyElement(element.all(rateEntry.ratesInput_TextBox).get(0), '10')
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Navigate to  Pricing>Rate card >Rate Entry", async () => {

let stepAction = "Navigate to  Pricing>Rate card >Rate Entry";
let stepData = "";
let stepExpResult = "Check the  rate entry View page is displayed";
let stepActualResult = "Check the  rate entry View page is displayed";

try {
    await home.appclickOnNavigationList("Pricing");
    
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("login as local admin user", async () => {

let stepAction = "login as local admin user";
let stepData = "";
let stepExpResult = "Local admin should be login sucessfully";
let stepActualResult = "Local admin should be login sucessfully";

try {
    await home.appclickOnMenuListLeftPanel("Local Rate Entry");
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select market", async () => {

let stepAction = "Select market";
let stepData = "Pittsburgh";
let stepExpResult = "Market should be selected and should display selected market releated channel list ";
let stepActualResult = "Market should be selected and should display selected market releated channel list ";

try {
    await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-markets", rateEntry.marketName, "Market")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select channel", async () => {

let stepAction = "Select channel";
let stepData = "WPGH";
let stepExpResult = "Channel should be selected ";
let stepActualResult = "Channel should be selected ";

try {
    await browser.sleep(1000)
    await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,globalTestData.multipleChnnels,"Channels")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Rates By as Daypart", async () => {

let stepAction = "Select Rates By as Daypart";
let stepData = "";
let stepExpResult = "Rates By Daypart should be selected and should display selected daypart releated dayparts and tags list";
let stepActualResult = "Rates By Daypart should be selected and should display selected daypart releated dayparts and tags list";

try {
    await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-ratesby", "Daypart", "Rates By")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Dayparts", async () => {

let stepAction = "Select Dayparts";
let stepData = "";
let stepExpResult = "Dayparts should be selected and should display selected dayparts releated tags list ";
let stepActualResult = "Dayparts should be selected and should display selected dayparts releated tags list ";

try {
await localLF.selectAllInMultiselectDropdown(element(by.css(localLF.mutliSelectDropdownLocator.replace("dynamic","daypart"))),"Dayparts")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Tags", async () => {

let stepAction = "Select Tags";
let stepData = "";
let stepExpResult = "Tags should be selected";
let stepActualResult = "Tags should be selected";

try {
    // await localLF.selectAllInMultiselectDropdown(element(by.css(localLF.mutliSelectDropdownLocator.replace("dynamic","tag"))),"Tags")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Base  Length(seconds) ", async () => {

let stepAction = "Select Base  Length(seconds) ";
let stepData = "30";
let stepExpResult = "Base length(seconds) should be selected";
let stepActualResult = "Base length(seconds) should be selected";

try {
    await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-baselength", "30", "Base Length (seconds)")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Start date and End date and click on search", async () => {

let stepAction = "Select Start date and End date and click on search";
let stepData = "";
let stepExpResult = "Start date and end date should be selected  and should show selected date range releated program list in the rate entry view grid";
let stepActualResult = "Start date and end date should be selected  and should show selected date range releated program list in the rate entry view grid";

try {
    await action.ClearText(rateEntry.startDate_Textbox, "startDate")
    await action.SetText(rateEntry.startDate_Textbox, rateEntry.startDateRentryView, "startDate")
    await action.SetText(rateEntry.endDate_Textbox, rateEntry.endDateRentryView1, "endDate_Textbox")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Check copy rates icon(Arrow icon) was avalible in rate entry view grid", async () => {

let stepAction = "Check copy rates icon(Arrow icon) was avalible in rate entry view grid";
let stepData = "";
let stepExpResult = "Copy  rates icon(Arrow icon) should be avalible in the rate entry view grid";
let stepActualResult = "Copy  rates icon(Arrow icon) should be avalible in the rate entry view grid";

try {
    await browser.sleep(3000)
    // await action.Click(rateEntry.button_Search, "SearchButton")
    // await verify.verifyElement(element.all(rateEntry.ratesInput_TextBox).get(0), '10')
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Navigate to  Pricing>Rate card >Rate Entry", async () => {

let stepAction = "Navigate to  Pricing>Rate card >Rate Entry";
let stepData = "";
let stepExpResult = "Check the  rate entry View page is displayed";
let stepActualResult = "Check the  rate entry View page is displayed";

try {
    await home.appclickOnNavigationList("Pricing");
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select  network admin in knobs", async () => {

let stepAction = "Select  network admin in knobs";
let stepData = "";
let stepExpResult = "Network admin should be login sucessfully";
let stepActualResult = "Network admin should be login sucessfully";

try {
    await home.appclickOnMenuListLeftPanel("Network Rate Entry");
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select network", async () => {

let stepAction = "Select network";
let stepData = "FOX";
let stepExpResult = "Network should be selectd ";
let stepActualResult = "Network should be selectd ";

try {
    await browser.sleep(1000)
    await localLF.selectMultiSelectDropDownOptions(localLF.networkMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,globalTestData.networkName,"Channels")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Rates By as Daypart", async () => {

let stepAction = "Select Rates By as Daypart";
let stepData = "";
let stepExpResult = "Rates By Daypart should be selected and should display selected daypart releated dayparts and tags list";
let stepActualResult = "Rates By Daypart should be selected and should display selected daypart releated dayparts and tags list";

try {
    await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-ratesby", "Daypart", "Rates By")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Dayparts", async () => {

let stepAction = "Select Dayparts";
let stepData = "";
let stepExpResult = "Dayparts should be selected and should display selected dayparts releated tags list ";
let stepActualResult = "Dayparts should be selected and should display selected dayparts releated tags list ";

try {
    await localLF.selectAllInMultiselectDropdown(element(by.css(localLF.mutliSelectDropdownLocator.replace("dynamic","daypart"))),"Dayparts")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Tags", async () => {

let stepAction = "Select Tags";
let stepData = "";
let stepExpResult = "Tags should be selected";
let stepActualResult = "Tags should be selected";

try {

    // await localLF.selectAllInMultiselectDropdown(element(by.css(localLF.mutliSelectDropdownLocator.replace("dynamic","tag"))),"Tags")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Base  Length(seconds) ", async () => {

let stepAction = "Select Base  Length(seconds) ";
let stepData = "30";
let stepExpResult = "Base length(seconds) should be selected";
let stepActualResult = "Base length(seconds) should be selected";

try {
    await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-baselength", "30", "Base Length (seconds)")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Start date and End date and click on search", async () => {

let stepAction = "Select Start date and End date and click on search";
let stepData = "";
let stepExpResult = "Start date and end date should be selected  and should show selected date range releated program list in the rate entry view grid";
let stepActualResult = "Start date and end date should be selected  and should show selected date range releated program list in the rate entry view grid";

try {
    await action.ClearText(rateEntry.startDate_Textbox, "startDate")
    await action.SetText(rateEntry.startDate_Textbox, rateEntry.startDateRentryView, "startDate")
    await action.SetText(rateEntry.endDate_Textbox, rateEntry.endDateRentryView1, "endDate_Textbox")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Check copy rates icon(Arrow icon) was avalible in rate entry view grid", async () => {

let stepAction = "Check copy rates icon(Arrow icon) was avalible in rate entry view grid";
let stepData = "";
let stepExpResult = "Copy  rates icon(Arrow icon) should be avalible in the rate entry view grid";
let stepActualResult = "Copy  rates icon(Arrow icon) should be avalible in the rate entry view grid";

try {
    await browser.sleep(3000)
    // await action.Click(rateEntry.button_Search, "SearchButton")
    // await verify.verifyElement(element.all(rateEntry.ratesInput_TextBox).get(0), '10')
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Navigate to  Pricing>Rate card >Rate Entry", async () => {

let stepAction = "Navigate to  Pricing>Rate card >Rate Entry";
let stepData = "";
let stepExpResult = "Check the  rate entry View page is displayed";
let stepActualResult = "Check the  rate entry View page is displayed";

try {
    await home.appclickOnNavigationList("Pricing");
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("login as local admin user", async () => {

let stepAction = "login as local admin user";
let stepData = "";
let stepExpResult = "Local admin should be login sucessfully";
let stepActualResult = "Local admin should be login sucessfully";

try {
    await home.appclickOnMenuListLeftPanel("Local Rate Entry");
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select market", async () => {

let stepAction = "Select market";
let stepData = "Pittsburgh";
let stepExpResult = "Market should be selected and should display selected market releated channel list ";
let stepActualResult = "Market should be selected and should display selected market releated channel list ";

try {
    await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-markets", rateEntry.marketName, "Market")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select channel", async () => {

let stepAction = "Select channel";
let stepData = "WPGH";
let stepExpResult = "Channel should be selected ";
let stepActualResult = "Channel should be selected ";

try {
    await browser.sleep(1000)
    await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,globalTestData.multipleChnnels,"Channels")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Rates By as Week", async () => {

let stepAction = "Select Rates By as Week";
let stepData = "";
let stepExpResult = "Rates By Week should be selected and should display  Rate card list in rate card drop down";
let stepActualResult = "Rates By Week should be selected and should display  Rate card list in rate card drop down";

try {
    await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-ratesby", "Week", "Rates By")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Rate card", async () => {

let stepAction = "Select Rate card";
let stepData = "";
let stepExpResult = "Rate card should be selected";
let stepActualResult = "Rate card should be selected";

try {
    await action.Click(rateEntry.RateCard, "RateCard")
    await browser.sleep(1000)
    await action.Click(element.all(rateEntry.RateCardOptions).get(1), "Rate Card")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Base  Length(seconds) ", async () => {

let stepAction = "Select Base  Length(seconds) ";
let stepData = "30";
let stepExpResult = "Base length(seconds) should be selected";
let stepActualResult = "Base length(seconds) should be selected";

try {
    await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-baselength", "30", "Base Length (seconds)")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Start date and End date and click on search", async () => {

let stepAction = "Select Start date and End date and click on search";
let stepData = "";
let stepExpResult = "Start date and end date should be selected  and should show selected date range releated program list in the rate entry view grid";
let stepActualResult = "Start date and end date should be selected  and should show selected date range releated program list in the rate entry view grid";

try {
    await action.ClearText(rateEntry.startDate_Textbox, "startDate")
    await action.SetText(rateEntry.startDate_Textbox, rateEntry.startDateRentryView, "startDate")
    await action.SetText(rateEntry.endDate_Textbox, rateEntry.endDateRentryView1, "endDate_Textbox")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Check copy rates icon(Arrow icon) was avalible in rate entry view grid", async () => {

let stepAction = "Check copy rates icon(Arrow icon) was avalible in rate entry view grid";
let stepData = "";
let stepExpResult = "Copy  rates icon(Arrow icon) should be avalible in the rate entry view grid";
let stepActualResult = "Copy  rates icon(Arrow icon) should be avalible in the rate entry view grid";

try {
    await action.Click(rateEntry.button_Search, "SearchButton")
    // await verify.verifyElement(element.all(rateEntry.ratesInput_TextBox).get(0), '10')
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Navigate to  Pricing>Rate card >Rate Entry", async () => {

let stepAction = "Navigate to  Pricing>Rate card >Rate Entry";
let stepData = "";
let stepExpResult = "Check the  rate entry View page is displayed";
let stepActualResult = "Check the  rate entry View page is displayed";

try {
    await home.appclickOnNavigationList("Pricing");
    await home.appclickOnMenuListLeftPanel("Network Rate Entry");
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select  network admin in knobs", async () => {

let stepAction = "Select  network admin in knobs";
let stepData = "";
let stepExpResult = "Network admin should be login sucessfully";
let stepActualResult = "Network admin should be login sucessfully";

try {
   
  
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select network", async () => {

let stepAction = "Select network";
let stepData = "FOX";
let stepExpResult = "Network should be selectd ";
let stepActualResult = "Network should be selectd ";

try {
    await browser.sleep(1000)
    await localLF.selectMultiSelectDropDownOptions(localLF.networkMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,globalTestData.networkName,"Channels")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Rates By as Week", async () => {

let stepAction = "Select Rates By as Week";
let stepData = "";
let stepExpResult = "Rates By Week should be selected and should display  Rate card list in rate card drop down";
let stepActualResult = "Rates By Week should be selected and should display  Rate card list in rate card drop down";

try {
    await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-ratesby", "Week", "Rates By")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Rate card", async () => {

let stepAction = "Select Rate card";
let stepData = "";
let stepExpResult = "Rate card should be selected";
let stepActualResult = "Rate card should be selected";

try {
    await action.Click(rateEntry.RateCard, "RateCard")
    await browser.sleep(1000)
   // await action.Click(element.all(rateEntry.RateCardOptions).get(1), "Rate Card")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Base  Length(seconds) ", async () => {

let stepAction = "Select Base  Length(seconds) ";
let stepData = "30";
let stepExpResult = "Base length(seconds) should be selected";
let stepActualResult = "Base length(seconds) should be selected";

try {
    await rateEntry.ClickOnSingleSelectDropDownUsingLabel("xgc-rate-entry-baselength", "30", "Base Length (seconds)")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Start date and End date and click on search", async () => {

let stepAction = "Select Start date and End date and click on search";
let stepData = "";
let stepExpResult = "Start date and end date should be selected  and should show selected date range releated program list in the rate entry view grid";
let stepActualResult = "Start date and end date should be selected  and should show selected date range releated program list in the rate entry view grid";

try {
    await action.ClearText(rateEntry.startDate_Textbox, "startDate")
    await action.SetText(rateEntry.startDate_Textbox, rateEntry.startDateRentryView, "startDate")
    await action.SetText(rateEntry.endDate_Textbox, rateEntry.endDateRentryView1, "endDate_Textbox")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Check copy rates icon(Arrow icon) was avalible in rate entry view grid", async () => {

let stepAction = "Check copy rates icon(Arrow icon) was avalible in rate entry view grid";
let stepData = "";
let stepExpResult = "Copy  rates icon(Arrow icon) should be avalible in the rate entry view grid";
let stepActualResult = "Copy  rates icon(Arrow icon) should be avalible in the rate entry view grid";

try {
    await action.Click(rateEntry.button_Search, "SearchButton")
    // await verify.verifyElement(element.all(rateEntry.ratesInput_TextBox).get(0), '10')
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


});
