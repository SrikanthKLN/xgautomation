/*
***********************************************************************************************************
* @Script Name :  XGCT-9032
* @Description :  xG Campaign - Pricing - Spot Length Addition to Length Factor View (Network)16-8-2019
* @Page Object Name(s) : LocalLengthFactorsPricing
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib,globalvalues,HomePageFunction
* @Pre-Conditions : 
* @Creation Date : 16-8-2019
* @Author : Sivaraj
* @Modified By & Date:26-8-2019
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../TestData/globalTestData';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let home = new HomePageFunction();
let localLF = new LocalLengthFactorsPricing();

let id: string
let testdata_LFGroupName: string
let testdata_SpotLength: number
let testdata_SpotLengthFactor: string

//Variables Declaration
let TestCase_ID = 'XGCT-9032'
let TestCase_Title = 'xG Campaign - Pricing - Spot Length Addition to Length Factor View (Network)'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - PRICING - SPOT LENGTH ADDITION TO LENGTH FACTOR VIEW (NETWORK)', () => {

    // --------------Test Step------------
    it("Navigate to the application", async () => {

        let stepAction = "Navigate to the application";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "User should be able to open the application";
        let stepActualResult = "User able to open the application";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to pricing> Network spot length>add", async () => {

        let stepAction = "Navigate to pricing> Network spot length>add";
        let stepData = "";
        let stepExpResult = "User should be able to  view add spot length page";
        let stepActualResult = "User able to  view add spot length page";

        try {
            await home.appclickOnNavigationList("Pricing");
            await home.appclickOnMenuListLeftPanel("Reference");
            await home.appclickOnMenuListLeftPanel("Network Spot Lengths");
            await localLF.verifyPageHeader('Spot Length View')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Fill details in Spot length,length factors and click on save", async () => {

        let stepAction = "Fill details in Spot length,length factors and click on save";
        let stepData = "";
        let stepExpResult = "Spot length should be added sucessfully";
        let stepActualResult = "Spot length  added sucessfully";

        try {
            testdata_SpotLength = await localLF.makeNumericId(4)
            testdata_SpotLengthFactor = "0." + testdata_SpotLength
            await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, String(globalTestData.networkName[0]), "Network")
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            await action.SetText(element.all(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length"))).get(0), String(testdata_SpotLength), "SpotLength")
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length-factor")), (testdata_SpotLengthFactor), "SpotLengthFactor")
            await localLF.buttonActionsVerify(["Save"], "Click", "Save Button")
            await localLF.verifyToastNotification("Add Spot Length,Spot Length Added Successfully.")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await verify.verifyProgressBarNotPresent()
            await browser.sleep(5000)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Pricing>  Network Length Factor View", async () => {

        let stepAction = "Navigate to Pricing>  Network Length Factor View";
        let stepData = "";
        let stepExpResult = "User should be able to see the length factor view";
        let stepActualResult = "User able to see the length factor view";

        try {
            await home.appclickOnMenuListLeftPanel("Network Length Factors");
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select the Network for which spot length was added", async () => {

        let stepAction = "Select the Network for which spot length was added";
        let stepData = "";
        let stepExpResult = "User should be able to view selected network releated spot lengths in grid";
        let stepActualResult = "User able to view selected network releated spot lengths in grid";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, String(globalTestData.networkName[0]), "Network")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Pricing>  Network Length Factor View > Add", async () => {

        let stepAction = "Navigate to Pricing>  Network Length Factor View > Add";
        let stepData = "";
        let stepExpResult = "User should be able to view add length factor view screen";
        let stepActualResult = "User able to view add length factor view screen";

        try {

            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await browser.sleep(3000)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the newly created spot length is  showing in add length factor spot length fields", async () => {

        let stepAction = "Check the newly created spot length is  showing in add length factor spot length fields";
        let stepData = "";
        let stepExpResult = "User should be able to see newly created spot length in add length factor screen";
        let stepActualResult = "User able to see newly created spot length in add length factor screen";

        try {
            let AddedspotLength = await action.makeDynamicLocatorContainsText(localLF.lengthFactorViewspotlengths, String(testdata_SpotLength) + "s", "Equal")
            await verify.verifyElement(AddedspotLength, "AddedspotLength")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Fill details in Group name,description, length factors and click on save", async () => {

        let stepAction = "Fill details in Group name,description, length factors and click on save";
        let stepData = "";
        let stepExpResult = "User should be able to add new length factor sucessfully";
        let stepActualResult = "User able to add new length factor sucessfully";

        try {
            id = await localLF.makeAlphaNumericId(4)
            testdata_LFGroupName = id + "Name"
            await localLF.selectSingleSelectDropDownOptions(element.all(localLF.networksingleSelectDropDown).get(1), localLF.singleSelectDropdownOptionsLocator, String(globalTestData.networkName[0]), "Network")
            await browser.sleep(3000)
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic", "groupname")), testdata_LFGroupName, "LFGroupName")
            await localLF.buttonActionsVerify(["Save"], "Click", "Save Button")
            await localLF.verifyToastNotification("Length Factor Group,Length Factor Group added succesfully")
            await verify.verifyProgressSpinnerCircleNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the newly created length factor is showing in the grid", async () => {

        let stepAction = "Check the newly created length factor is showing in the grid";
        let stepData = "";
        let stepExpResult = "User should be able to see newly created length factor in the grid";
        let stepActualResult = "User able to see newly created length factor in the grid";

        try {
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName, "equal")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
