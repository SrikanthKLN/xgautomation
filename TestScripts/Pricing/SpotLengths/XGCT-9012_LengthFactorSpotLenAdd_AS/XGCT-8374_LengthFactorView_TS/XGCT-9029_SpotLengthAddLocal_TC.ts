/*
***********************************************************************************************************
* @Script Name :  XGCT-9029
* @Description :  xG Campaign - Pricing - Spot Length Addition to Length Factor View (Local)27-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 27-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { Reporter } from '../../../../../Utility/htmlResult';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../TestData/globalTestData';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let home = new HomePageFunction();
let localLF = new LocalLengthFactorsPricing();

let testdata_SpotLength: number
let testdata_SpotLengthFactor: string
//Variables Declaration
let TestCase_ID = 'XGCT-9029'
let TestCase_Title = 'xG Campaign - Pricing - Spot Length Addition to Length Factor View (Local)'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - PRICING - SPOT LENGTH ADDITION TO LENGTH FACTOR VIEW (LOCAL)', () => {

    // --------------Test Step------------
    it("Navigate to the application", async () => {

        let stepAction = "Navigate to the application";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "User should be able to open the application";
        let stepActualResult = "User should be able to open the application";

        try {
            await browser.waitForAngularEnabled(false);
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to pricing> Local spot length>add", async () => {

        let stepAction = "Navigate to pricing> Local spot length>add";
        let stepData = "";
        let stepExpResult = "User should be able to  view add spot length page";
        let stepActualResult = "User should be able to  view add spot length page";

        try {
            await home.appclickOnNavigationList("Pricing");
            await home.appclickOnMenuListLeftPanel("Reference");
            await home.appclickOnMenuListLeftPanel("Local Spot Lengths");
            await localLF.verifyPageHeader('Spot Length View')
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await localLF.eraseMultiselectDropdownSelection(localLF.channelMultiSelectDropDown, "Channels")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, globalTestData.singleChannel, "Channels")
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            await localLF.verifyPopUpHeader('Add Spot Length')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Fill details in Spot length,length factors and click on save", async () => {

        let stepAction = "Fill details in Spot length,length factors and click on save";
        let stepData = "";
        let stepExpResult = "Spot length should be added sucessfully";
        let stepActualResult = "Spot length should be added sucessfully";

        try {
            testdata_SpotLength = await localLF.makeNumericId(4)
            testdata_SpotLengthFactor = "0." + testdata_SpotLength
            await action.SetText(element.all(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length"))).get(0), String(testdata_SpotLength), "SpotLength")
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length-factor")), (testdata_SpotLengthFactor), "SpotLengthFactor")
            await localLF.buttonActionsVerify(["Save"], "Click", "Save Button")
            await localLF.verifyToastNotification("Add Spot Length,Spot Length Added Successfully.")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await verify.verifyProgressBarNotPresent()
            await browser.sleep(5000)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Pricing>  Local Length Factor View", async () => {

        let stepAction = "Navigate to Pricing>  Local Length Factor View";
        let stepData = "";
        let stepExpResult = "User should be able to see the length factor view";
        let stepActualResult = "User should be able to see the length factor view";

        try {
            await home.appclickOnMenuListLeftPanel("Local Length Factors");
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select the market and channel for which spot length was added", async () => {

        let stepAction = "Select the market and channel for which spot length was added";
        let stepData = "Pre-Condition: There should be a length factor added to spot length specific to a market - channel";
        let stepExpResult = "User should be able to the length spot length associated with the market selected.";
        let stepActualResult = "User should be able to the length spot length associated with the market selected.";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await verify.verifyProgressBarNotPresent()
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            await localLF.eraseMultiselectDropdownSelection(localLF.channelMultiSelectDropDown, "Channels")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, globalTestData.singleChannel, "Channels")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await browser.sleep(3000)
            let AddedspotLength = await action.makeDynamicLocatorContainsText(localLF.lengthFactorViewspotlengths, String(testdata_SpotLength) + "s", "Equal")
            await verify.verifyElement(AddedspotLength, "AddedspotLength")
            await localLF.buttonActionsVerify(["cancel"], "Click", "cancel Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to pricing> Local spot length>add", async () => {

        let stepAction = "Navigate to pricing> Local spot length>add";
        let stepData = "";
        let stepExpResult = "User should be able to  view add spot length page";
        let stepActualResult = "User should be able to  view add spot length page";

        try {
            await home.appclickOnMenuListLeftPanel("Local Spot Lengths");
            await localLF.verifyPageHeader('Spot Length View')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select the another market (other than above one)  Fill details in Spot length,length factors and click on save", async () => {

        let stepAction = "Select the another market (other than above one)  Fill details in Spot length,length factors and click on save";
        let stepData = "";
        let stepExpResult = "Spot length should be added sucessfully";
        let stepActualResult = "Spot length should be added sucessfully";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName2, "Market")
            await localLF.eraseMultiselectDropdownSelection(localLF.channelMultiSelectDropDown, "Channels")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, globalTestData.singleChannelElPaso, "Channels")
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            await localLF.verifyPopUpHeader('Add Spot Length')
            testdata_SpotLength = await localLF.makeNumericId(4)
            testdata_SpotLengthFactor = "0." + testdata_SpotLength
            await action.SetText(element.all(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length"))).get(0), String(testdata_SpotLength), "SpotLength")
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length-factor")), (testdata_SpotLengthFactor), "SpotLengthFactor")
            await localLF.buttonActionsVerify(["Save"], "Click", "Save Button")
            await localLF.verifyToastNotification("Add Spot Length,Spot Length Added Successfully.")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await verify.verifyProgressBarNotPresent()
            await browser.sleep(5000)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select the another market (other than above one)for which spot length was added", async () => {

        let stepAction = "Select the another market (other than above one)for which spot length was added";
        let stepData = "Pre-Condition: There should be a spot length which is added/linked to more than one market(multiplemarkets)";
        let stepExpResult = "User should be able to the length spot length associated with the market selected.";
        let stepActualResult = "User should be able to the length spot length associated with the market selected.";

        try {
            await home.appclickOnMenuListLeftPanel("Local Length Factors");
            await localLF.verifyPageHeader('Length Factors')
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName2, "Market")
            await verify.verifyProgressBarNotPresent()
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            await localLF.eraseMultiselectDropdownSelection(localLF.channelMultiSelectDropDown, "Channels")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, globalTestData.singleChannelElPaso, "Channels")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await browser.sleep(3000)
            let AddedspotLength = await action.makeDynamicLocatorContainsText(localLF.lengthFactorViewspotlengths, String(testdata_SpotLength) + "s", "Equal")
            await verify.verifyElement(AddedspotLength, "AddedspotLength")
            await localLF.buttonActionsVerify(["cancel"], "Click", "cancel Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select one market and one channel from market and channel dropdowns", async () => {

        let stepAction = "Select one market and one channel from market and channel dropdowns";
        let stepData = "";
        let stepExpResult = "User should be able to view the spot lengths added to the market and channel";
        let stepActualResult = "This step is already covered in step no 2 & 3";

        try {

            report.ReportStatus(stepAction, stepData, 'SKIP', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Select one market and more than one channel from market and channel dropdowns", async () => {

        let stepAction = "Select one market and more than one channel from market and channel dropdowns";
        let stepData = "Pre-condition:There should be a spot length added to the market and multiple channels associated with it";
        let stepExpResult = "User should be able to view the spot lengths added to the market and channel";
        let stepActualResult = "User should be able to view the spot lengths added to the market and channel";

        try {
            await home.appclickOnMenuListLeftPanel("Local Spot Lengths");
            await localLF.verifyPageHeader('Spot Length View')
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await localLF.eraseMultiselectDropdownSelection(localLF.channelMultiSelectDropDown, "Channels")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, globalTestData.multipleChnnels, "Channels")
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            await localLF.verifyPopUpHeader('Add Spot Length')
            testdata_SpotLength = await localLF.makeNumericId(4)
            testdata_SpotLengthFactor = "0." + testdata_SpotLength
            await action.SetText(element.all(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length"))).get(0), String(testdata_SpotLength), "SpotLength")
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length-factor")), (testdata_SpotLengthFactor), "SpotLengthFactor")
            await localLF.buttonActionsVerify(["Save"], "Click", "Save Button")
            await localLF.verifyToastNotification("Add Spot Length,Spot Length Added Successfully.")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await verify.verifyProgressBarNotPresent()
            await browser.sleep(5000)            
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to pricing> Local Length factor view", async () => {

        let stepAction = "Navigate to pricing> Local Length factor view";
        let stepData = "";
        let stepExpResult = "User should be able to view local length factor view";
        let stepActualResult = "User should be able to view local length factor view";

        try {
            await home.appclickOnMenuListLeftPanel("Local Length Factors");
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Check that newly added spot lengths are in the sequence in the length factor view", async () => {

        let stepAction = "Check that newly added spot lengths are in the sequence in the length factor view";
        let stepData = "Eg: Existing spot length - 5s, 10s, 20s Added 2s length Final display of the spots should be as below..2s,5s,10s,20s";
        let stepExpResult = "newly added spot length should be in the sequence of increase in the spot length value";
        let stepActualResult = "newly added spot length should be in the sequence of increase in the spot length value";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await verify.verifyProgressBarNotPresent()
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            await localLF.eraseMultiselectDropdownSelection(localLF.channelMultiSelectDropDown, "Channels")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, globalTestData.multipleChnnels, "Channels")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await browser.sleep(3000)
            let AddedspotLength = await action.makeDynamicLocatorContainsText(localLF.lengthFactorViewspotlengths, String(testdata_SpotLength) + "s", "Equal")
            await action.MouseMoveJavaScript(AddedspotLength, "AddedspotLength")
            await verify.verifyElement(AddedspotLength, "AddedspotLength")
            await localLF.verifySortOrderSpotLengthslenthfactorView()
            await localLF.buttonActionsVerify(["cancel"], "Click", "cancel Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
