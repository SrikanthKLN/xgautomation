/*
***********************************************************************************************************
* @Script Name :  XGCT-9025
* @Description :  xG Campaign - Pricing - Length Factor View (Network)1-8-2019
* @Page Object Name(s) : LocalLengthFactorsPricing
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib,globalvalues,HomePageFunction
* @Pre-Conditions : 
* @Creation Date : 1-8-2019
* @Author : Adithya K
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../../TestData/globalTestData';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let home = new HomePageFunction();
let localLF = new LocalLengthFactorsPricing();

//Variables Declaration
let TestCase_ID = 'XGCT-9025'
let TestCase_Title = 'xG Campaign - Pricing - Length Factor View (Network)'

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - PRICING - LENGTH FACTOR VIEW (NETWORK)', () => {

    // --------------Test Step------------
    it("Navigate to the application", async () => {

        let stepAction = "Navigate to the application";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "User should be navigated to Login page of the application";
        let stepActualResult = "User is navigated to Login page of the application";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to the pricing > Network Length Factors", async () => {

        let stepAction = "Navigate to the pricing > Network Length Factors";
        let stepData = "";
        let stepExpResult = "User should be able to view network length factor view grid";
        let stepActualResult = "User is able to view network length factor view grid";

        try {
            await home.appclickOnNavigationList(globalTestData.pricingTabName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuNetworkLengthFactors);
            await localLF.verifyPageHeader('Length Factors')
            await verify.verifyElement(localLF.gridLocator,"LengthFactor Grid");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check that user is able to view Network with affiliation labeled in the Length Factor view", async () => {

        let stepAction = "Check that user is able to view Network with affiliation labeled in the Length Factor view";
        let stepData = "";
        let stepExpResult = "User should be able to view Network with affiliation labeled in the Length Factor view";
        let stepActualResult = "User is able to view Network with affiliation labeled in the Length Factor view";

        try {
            await verify.verifyElement(localLF.networksingleSelectDropDown,"Network");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check that user is able to select a network for the related Length Factor View", async () => {

        let stepAction = "Check that user is able to select a network for the related Length Factor View";
        let stepData = "";
        let stepExpResult = "User should be able to view associated Network name/affiliation on the Length factor view for the selected network";
        let stepActualResult = "User is able to view associated Network name/affiliation on the Length factor view for the selected network";

        try {
            await action.Click(localLF.networksingleSelectDropDown,"Network");
            await verify.verifyElement(localLF.singleSelectDropdownOptionsLocator,"Network Names")
            await action.Click(localLF.networksingleSelectDropDown,"Network");
            await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown,localLF.singleSelectDropdownOptionsLocator,String(globalTestData.networkName[0]),"Network")
            await verify.verifyElement(localLF.gridLocator,"LengthFactor Grid");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check that user is able to view the length factor group as defined for Netowrk", async () => {

        let stepAction = "Check that user is able to view the length factor group as defined for Netowrk";
        let stepData = "";
        let stepExpResult = "User should be able to view the group linked to the Length Factor view as defined for Netowrk";
        let stepActualResult = "User is able to view the group linked to the Length Factor view as defined for Netowrk";

        try {
            await localLF.VerifyColumnNameDisplayedInGrid(["Group Name"])
            await localLF.buttonActionsVerify(["Add"],"Click","Add Button")
            await verify.verifyProgressSpinnerCircleNotPresent();
            await browser.sleep(3000)
            await localLF.verifyDefaultSpotLengths()
            await localLF.buttonActionsVerify(["Cancel"],"Click","Cancel Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the user is able to view search option in the grid", async () => {

        let stepAction = "Check the user is able to view search option in the grid";
        let stepData = "";
        let stepExpResult = "User should be able to view search option in the grid";
        let stepActualResult = "User is able to view search option in the grid";

        try {
            await verify.verifyElement(localLF.globalFilterInput,"globalFilterInput")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
