/*
***********************************************************************************************************
* @Script Name :  XGCT-9034
* @Description :  xG Campaign - Pricing -  Add Length Factor View 2-8-2019
* @Page Object Name(s) : LocalLengthFactorsPricing
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib,globalvalues,HomePageFunction
* @Pre-Conditions : 
* @Creation Date : 2-8-2019
* @Author : Adithya K
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../../TestData/globalTestData';
import { AppCommonFunctions } from '../../../../../../Libs/ApplicationLibs/CommAppLib';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let home = new HomePageFunction();
let localLF = new LocalLengthFactorsPricing();
let commonLib = new AppCommonFunctions();
let id: string
let testdata_LFGroupName: string
// let testdata_Description: string
let roundToOptions: string = "Exact"

//Variables Declaration
let TestCase_ID = 'XGCT-9034'
let TestCase_Title = 'xG Campaign - Pricing -  Add Length Factor View '

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - PRICING -  ADD LENGTH FACTOR VIEW ', () => {

    // --------------Test Step------------
    it("Navigate to the application", async () => {

        let stepAction = "Navigate to the application";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "User should be able to open the application";
        let stepActualResult = "User is able to open the application";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Pricing>  Local  Length Factor View > Add", async () => {

        let stepAction = "Navigate to Pricing>  Local  Length Factor View > Add";
        let stepData = "";
        let stepExpResult = "User should be able to view add length factor  screen";
        let stepActualResult = "User is able to view add length factor  screen";

        try {
            await home.appclickOnNavigationList(globalTestData.pricingTabName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuLocalLengthFactors);
            await verify.verifyElement(localLF.gridLocator, "LengthFactor Grid");
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select channel ", async () => {

        let stepAction = "Select channel ";
        let stepData = "";
        let stepExpResult = "Channel should be selected";
        let stepActualResult = "Channel is selected";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            await localLF.eraseMultiselectDropdownSelection(localLF.channelMultiSelectDropDown, "Channels")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, globalTestData.multipleChnnels, "Channels")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter Group name and Descripiton", async () => {

        let stepAction = "Enter Group name and Descripiton";
        let stepData = "";
        let stepExpResult = "Group name and Description  should be taken";
        let stepActualResult = "Group name and Description are taken";

        try {
            id = await localLF.makeAlphaNumericId(5)
            testdata_LFGroupName = id + "Name"
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic", "groupname")), testdata_LFGroupName, "LFGroupName")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select length factor group as default and enter/update length factor ", async () => {

        let stepAction = "Select length factor group as default and enter/update length factor ";
        let stepData = "";
        let stepExpResult = "Default should be selected and length factor value should be taken";
        let stepActualResult = "Default is selected and length factor value is taken";

        try {
            await action.Click(localLF.defalutLinkLF, "Default")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select round to", async () => {

        let stepAction = "Select round to";
        let stepData = "";
        let stepExpResult = "Round to should be selected ";
        let stepActualResult = "Round to is selected ";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.roundToDropdown, localLF.singleSelectDropdownOptionsLocator, roundToOptions, "roundToOptions")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on save ", async () => {

        let stepAction = "Click on save ";
        let stepData = "";
        let stepExpResult = "Length factor should be created and system should show 'Length Factor Group added succesfully' message";
        let stepActualResult = "'Length factor is created and 'Length Factor Group added succesfully' message is displayed";

        try {
            await localLF.buttonActionsVerify(["Save"], "Click", "Save Button")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await commonLib.verifyPopupMessage("Length Factor Group added succesfully");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the newly created length factor is avaliable In the grid", async () => {

        let stepAction = "Check the newly created length factor is avaliable In the grid";
        let stepData = "";
        let stepExpResult = "User should be able to see newly created length factor in the grid";
        let stepActualResult = "User is able to see newly created length factor in the grid";

        try {
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName, "equal")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Pricing>  Network  Length Factor View > Add", async () => {

        let stepAction = "Navigate to Pricing>  Network  Length Factor View > Add";
        let stepData = "";
        let stepExpResult = "User should be able to view add length factor screen";
        let stepActualResult = "User is able to view add length factor screen";

        try {
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuNetworkLengthFactors);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Network ", async () => {

        let stepAction = "Select Network ";
        let stepData = "";
        let stepExpResult = "Network should be selected";
        let stepActualResult = "Network is selected";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, String(globalTestData.networkName[0]), "Network")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter Group name and Descripiton", async () => {

        let stepAction = "Enter Group name and Descripiton";
        let stepData = "";
        let stepExpResult = "Group name and Description should be taken";
        let stepActualResult = "Group name and Description are taken";

        try {
            id = await localLF.makeAlphaNumericId(5)
            testdata_LFGroupName = id + "Name"
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            await browser.sleep(3000)
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic", "groupname")), testdata_LFGroupName, "LFGroupName")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select length factor group as default and enter/update length factor ", async () => {

        let stepAction = "Select length factor group as default and enter/update length factor ";
        let stepData = "";
        let stepExpResult = "Default should be selected and length factor value should be taken";
        let stepActualResult = "Default is selected and length factor value is taken";

        try {
            await action.Click(localLF.defalutLinkLF, "Default")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select round to", async () => {

        let stepAction = "Select round to";
        let stepData = "";
        let stepExpResult = "Round to should be selected ";
        let stepActualResult = "Round to is selected ";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.roundToDropdown, localLF.singleSelectDropdownOptionsLocator, roundToOptions, "roundToOptions")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on save ", async () => {

        let stepAction = "Click on save ";
        let stepData = "";
        let stepExpResult = "Length factor should be created and system should show Length Factor Group added succesfully' message";
        let stepActualResult = "Length factor is created and 'Length Factor Group added succesfully' message is displayed";

        try {
            await localLF.buttonActionsVerify(["Save"], "Click", "Save Button");
            await verify.verifyProgressSpinnerCircleNotPresent();
            await commonLib.verifyPopupMessage("Length Factor Group added succesfully");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the newly created length factor is avaliable In the grid", async () => {

        let stepAction = "Check the newly created length factor is avaliable In the grid";
        let stepData = "";
        let stepExpResult = "User should be able to see newly created length factor in the grid";
        let stepActualResult = "User should be able to see newly created length factor in the grid";

        try {
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName, "equal")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
