/*
***********************************************************************************************************
* @Script Name :  XGCT-9024
* @Description :  xG Campaign - Pricing - Length Factor View (Local)1-8-2019
* @Page Object Name(s) : LocalLengthFactorsPricing
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib,globalvalues,HomePageFunction
* @Pre-Conditions : 
* @Creation Date : 1-8-2019
* @Author : Adithya K
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../../TestData/globalTestData';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let home = new HomePageFunction();
let localLF = new LocalLengthFactorsPricing();


//Variables Declaration
let TestCase_ID = 'XGCT-9024'
let TestCase_Title = 'xG Campaign - Pricing - Length Factor View (Local)'

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - PRICING - LENGTH FACTOR VIEW (LOCAL)', () => {

    // --------------Test Step------------
    it("Navigate to the application", async () => {

        let stepAction = "Navigate to the application";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "User should be navigated to Login page of the application";
        let stepActualResult = "User is navigated to Login page of the application";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to pricing > Local Length Factors", async () => {

        let stepAction = "Navigate to pricing > Local Length Factors";
        let stepData = "";
        let stepExpResult = "User should be able to view local length factor view grid";
        let stepActualResult = "User is able to view local length factor view grid";

        try {
            await home.appclickOnNavigationList(globalTestData.pricingTabName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuLocalLengthFactors);
            await localLF.verifyPageHeader('Length Factors')
            await verify.verifyElement(localLF.gridLocator, "LengthFactor Grid");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check that user is able to view the market label in the Length Factor view", async () => {

        let stepAction = "Check that user is able to view the market label in the Length Factor view";
        let stepData = "";
        let stepExpResult = "User should be able to view the market name labeled on Length Factor View";
        let stepActualResult = "User is able to view the market name labeled on Length Factor View";

        try {
            await verify.verifyElement(localLF.marketsingleSelectDropDown, "Market");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check that user is able to select the Length Factor view based on the market to which he has access to", async () => {

        let stepAction = "Check that user is able to select the Length Factor view based on the market to which he has access to";
        let stepData = "";
        let stepExpResult = "User should be able to select the Length Factor view based on the market";
        let stepActualResult = "User is able to select the Length Factor view based on the market";

        try {
            await action.Click(localLF.marketsingleSelectDropDown, "Market");
            await verify.verifyElement(localLF.singleSelectDropdownOptionsLocator, "Market Names")
            await action.Click(localLF.marketsingleSelectDropDown, "Market");
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await verify.verifyElement(localLF.gridLocator, "LengthFactor Grid");
            browser.sleep(1000)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check that user is able to access all the channels from the market, that he is access to", async () => {

        let stepAction = "Check that user is able to access all the channels from the market, that he is access to";
        let stepData = "";
        let stepExpResult = "User should be able to access all the channels from the market";
        let stepActualResult = "User is able to access all the channels from the market";

        try {
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            browser.sleep(1000)
            await localLF.verifyDropDownOptionAvailable(localLF.channelMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, globalTestData.multipleChnnels, "Channels")
            await action.Click(localLF.channelMultiSelectDropDown, "Channel")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check that user is able to view the default spot lengths in the Length Factor View for each channel", async () => {

        let stepAction = "Check that user is able to view the default spot lengths in the Length Factor View for each channel";
        let stepData = "";
        let stepExpResult = "User should be able to view the default spot lengths in the Length Factor View for each channel";
        let stepActualResult = "User is able to view the default spot lengths in the Length Factor View for each channel";

        try {
            await verify.verifyElement(localLF.lengthFactorViewspotlengths, "lengthFactorViewspotlengths")
            await verify.verifyElement(localLF.defaultLengthFactors, "defaultLengthFactors")
            await localLF.verifyDefaultSpotLengths()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the user is able to view search option in length factor view grid", async () => {

        let stepAction = "Check the user is able to view search option in length factor view grid";
        let stepData = "";
        let stepExpResult = "User should be able to view search option grid";
        let stepActualResult = "User is able to view search option grid";

        try {
            await localLF.buttonActionsVerify(["Cancel"], "Click", "Cancel Button")
            await verify.verifyElement(localLF.globalFilterInput, "globalFilterInput")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
