/*
***********************************************************************************************************
* @Script Name :  XGCT-9434
* @Description :  xG Campaign - Length Factor Group - Edit (Network)14-8-2019
* @Page Object Name(s) : LocalLengthFactorsPricing
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib,globalvalues,HomePageFunction
* @Pre-Conditions : 
* @Creation Date : 14-8-2019
* @Author : Adithya K
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../../TestData/globalTestData';
import { AppCommonFunctions } from '../../../../../../Libs/ApplicationLibs/CommAppLib';
import { NetworkTagsPage } from '../../../../../../Pages/Inventory/Reference/Tags/NetworkTags';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let home = new HomePageFunction();
let localLF = new LocalLengthFactorsPricing();
let commonLib = new AppCommonFunctions();
let networkTags = new NetworkTagsPage(); 

let id: string
let testdata_LFGroupName: string
let daypartname: string = "Morning"
let daypartname_afterChange: string = "Daytime"
let roundToOptions: string = "Exact"
let roundToOptions_afterChange: string = "Nearest $0.10"
let tagName

let deviationLF: string = "1"
let Deviation_afterChange

//Variables Declaration
let TestCase_ID = 'XGCT-9434'
let TestCase_Title = 'xG Campaign - Length Factor Group - Edit (Network)'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - LENGTH FACTOR GROUP - EDIT (NETWORK)', () => {

    // --------------Test Step------------
    it("Navigate to the application", async () => {

        let stepAction = "Navigate to the application";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "User should be navigated to the application";
        let stepActualResult = "User should be navigated to the application";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to  Pricing> Network Length Factors", async () => {

        let stepAction = "Navigate to  Pricing> Network Length Factors";
        let stepData = "";
        let stepExpResult = "Check the Network Length Factor View page is displayed";
        let stepActualResult = "Check the Network Length Factor View page is displayed";

        try {
            await home.appclickOnNavigationList(globalTestData.programmingTabName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuTagsName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuNetworkTagsName);
            await browser.sleep(5000);
            await verify.verifyProgressBarNotPresent();
            await commonLib.selectMultiOptionsFromDropDownUsingSearch(networkTags.networkMultiSelectDropDown,[String(globalTestData.networkName[0])]);
            await verify.verifyProgressBarNotPresent();
            await action.SetText(commonLib.globalFilter,"EM,DT","");
            await verify.verifyProgressBarNotPresent();
            let status = await verify.verifyElementVisible(commonLib.noDataMatchFound);
            if(status)
            {
                await localLF.buttonActionsVerify(["tag"], "Click", "Add Button");
                tagName = "AT_"+new Date().getTime();
                await action.ClearText(networkTags.nameInPopUp,"");
                await action.SetText(networkTags.nameInPopUp,tagName,"");
                await commonLib.selectMultiOptionsFromDropDownUsingSearch(networkTags.networksMultiSelectDropDownInPopUp,[String(globalTestData.networkName[0])]);
                await action.Click(networkTags.createButtonInPopUp,"Click On Create Button");
                await verify.verifyProgressBarNotPresent();
                await commonLib.verifyPopupMessage("Successfully Tag added");
                await verify.verifyProgressBarNotPresent();
                await commonLib.selectMultiOptionsFromDropDownUsingSearch(networkTags.networkMultiSelectDropDown,[String(globalTestData.networkName[0])]);
                await verify.verifyProgressBarNotPresent();
                await action.SetText(commonLib.globalFilter,tagName,"");
                await verify.verifyProgressBarNotPresent();
                
                let status = await verify.verifyElementVisible(commonLib.noDataMatchFound);
                if(status) throw "Failed- Created tag "+tagName+" is not displayed in the grid"
            }
            else
            {
                let columnIndex = await commonLib.getTableColumnIndex("Name");
                tagName = await action.GetText(by.xpath(commonLib.gridCellData.replace("rowdynamic","1").replace("columndynamic",String(columnIndex))),"");
                tagName = String(tagName).trim();
                await console.log("tagName:"+tagName);
            }
            await home.appclickOnNavigationList(globalTestData.pricingTabName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuNetworkLengthFactors);
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Network", async () => {

        let stepAction = "Select Network";
        let stepData = "";
        let stepExpResult = "Network should be selected and should display channels list";
        let stepActualResult = "Network should be selected and should display channels list";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown,localLF.singleSelectDropdownOptionsLocator,String(globalTestData.networkName[0]),"Network")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on add button", async () => {

        let stepAction = "Click on add button";
        let stepData = "";
        let stepExpResult = "Add length factor group window should be appear.";
        let stepActualResult = "Add length factor group window should be appear.";

        try {
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter Length factor group name", async () => {

        let stepAction = "Enter Length factor group name";
        let stepData = "";
        let stepExpResult = "Length factor group name should be taken";
        let stepActualResult = "Length factor group name should be taken";

        try {
            id = await localLF.makeAlphaNumericId(5)
            testdata_LFGroupName = id + "Name"
            await localLF.selectSingleSelectDropDownOptions(element.all(localLF.networksingleSelectDropDown).get(1),localLF.singleSelectDropdownOptionsLocator,String(globalTestData.networkName[0]),"Network")
            await browser.sleep(3000)
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic","groupname")),testdata_LFGroupName,"LFGroupName")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Round To", async () => {

        let stepAction = "Select Round To";
        let stepData = "";
        let stepExpResult = "Round To should be selected";
        let stepActualResult = "Round To should be selected";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.roundToDropdown, localLF.singleSelectDropdownOptionsLocator, roundToOptions, "roundToOptions")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Deviation (+) icon and Select Dayparts and Tags", async () => {

        let stepAction = "Click on Deviation (+) icon and Select Dayparts and Tags";
        let stepData = "";
        let stepExpResult = "Dayparts and Tags should be selected";
        let stepActualResult = "Dayparts and Tags should be selected";

        try {
            await action.Click(localLF.addDeviation, "addDeviation")
            await localLF.selectSingleSelectDropDownOptions(element(by.css(localLF.singleSelectDropdownLocator.replace("dynamic", "daypartname"))), localLF.singleSelectDropdownOptionsLocator, daypartname, "RateCard")
            await commonLib.selectMultiOptionsFromDropDownUsingSearch(localLF.tagsDropDown,[tagName]);
            await localLF.buttonActionsVerify(["xgc-add-edit-length-factor-save"], "Click", "Save Button")
            await action.Click(localLF.addedDaypartTagDeviation, "addedDaypartTagDeviation")
            let EleCount = await element.all(localLF.deviationDefaultLFInputBox).count()
            await console.log(EleCount + " EleCount")
            for (let i = 0; i < EleCount - 1; i++) {
                await action.MouseMoveToElement(element.all(localLF.deviationDefaultLFInputBox).get(i), "deviationLF")
                await action.SetText(element.all(localLF.deviationDefaultLFInputBox).get(i), deviationLF, "deviationLF")
                await element.all(localLF.deviationDefaultLFInputBox).get(i).sendKeys(protractor.Key.TAB)
            }
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Save", async () => {

        let stepAction = "Click on Save";
        let stepData = "";
        let stepExpResult = "Length factor group should created and should show successfully created message";
        let stepActualResult = "Length factor group should created and should show successfully created message";

        try {
            await localLF.buttonActionsVerify(["save"], "Click", "Save Button")
            await localLF.verifyToastNotification("Length Factor Group,Length Factor Group added succesfully")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName, "equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Network", async () => {

        let stepAction = "Select Network";
        let stepData = "";
        let stepExpResult = "Network should be selected and should display selected market,channel releated length factor group  list in the grid";
        let stepActualResult = "Network should be selected and should display selected market,channel releated length factor group  list in the grid";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown,localLF.singleSelectDropdownOptionsLocator,String(globalTestData.networkName[0]),"Network")
            await verify.verifyElement(localLF.gridLocator,"LengthFactor Grid");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Verify the edit option is available in length factors view", async () => {

        let stepAction = "Verify the edit option is available in length factors view";
        let stepData = "";
        let stepExpResult = "Edit  button should be available in length factor view";
        let stepActualResult = "Edit  button should be available in length factor view";

        try {
            await localLF.buttonActionsVerify(["Edit"], "Displayed", "Edit Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a record from Length factors view grid", async () => {

        let stepAction = "Select a record from Length factors view grid";
        let stepData = "";
        let stepExpResult = "Record should be selected from length factor view grid and  should have option to edit  the selected record";
        let stepActualResult = "Record should be selected from length factor view grid and  should have option to edit  the selected record";

        try {
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Edit"], "Enabled", "Edit Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select more than one record from  Length factors view grid", async () => {

        let stepAction = "Select more than one record from  Length factors view grid";
        let stepData = "";
        let stepExpResult = "Edit button should be disable";
        let stepActualResult = "Edit button should be disable";

        try {
            await action.Click(localLF.secondRecordCheckbox, "secondRecordCheckbox")
            await localLF.buttonActionsVerify(["Edit"], "Disabled", "Edit Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a record from  Length factors view grid and click on Edit button", async () => {

        let stepAction = "Select a record from  Length factors view grid and click on Edit button";
        let stepData = "";
        let stepExpResult = "Record should be selected from length factor view grid and user should have option to edit length factor details";
        let stepActualResult = "Record should be selected from length factor view grid and user should have option to edit length factor details";

        try {
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await action.Click(localLF.secondRecordCheckbox, "secondRecordCheckbox")
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Edit"], "Click", "Edit Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("select round to value from the Round To drop down list", async () => {

        let stepAction = "select round to value from the Round To drop down list";
        let stepData = "";
        let stepExpResult = "Round To value  should be selected";
        let stepActualResult = "Round To value  should be selected";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.roundToDropdown, localLF.singleSelectDropdownOptionsLocator, roundToOptions_afterChange, "roundToOptions")
            await browser.sleep(1500)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Save", async () => {

        let stepAction = "Click on Save";
        let stepData = "";
        let stepExpResult = "Round To drop down Changes should be updated";
        let stepActualResult = "Round To drop down Changes should be updated";

        try {
            await localLF.buttonActionsVerify(["save"], "Click", "update Button")
            await localLF.verifyToastNotification("Length Factor Group,Length Factor Group updated succesfully")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName, "equal")
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Edit"], "Click", "Edit Button")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await browser.sleep(3000)
            await verify.verifyTextOfElement(localLF.roundToDropdown, roundToOptions_afterChange, "roundToOptions_afterChange")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Change/Modify the Length Factor Group details for the selected record and click on save", async () => {

        let stepAction = "Change/Modify the Length Factor Group details for the selected record and click on save";
        let stepData = "";
        let stepExpResult = "System should display sucessfully details updated message and  Length factor group name should be updated";
        let stepActualResult = "System should display sucessfully details updated message and  Length factor group name should be updated";

        try {
            id = await localLF.makeAlphaNumericId(5)
            testdata_LFGroupName = id + "Name"
            await action.ClearText(by.css(localLF.textBox_locator.replace("dynamic", "groupname")), "LFGroupName")
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic", "groupname")), testdata_LFGroupName, "LFGroupName")
            await localLF.buttonActionsVerify(["save"], "Click", "update Button")
            await localLF.verifyToastNotification("Length Factor Group,Length Factor Group updated succesfully")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName, "equal")
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Edit"], "Click", "Edit Button")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await browser.sleep(1000)
            await verify.verifyElementAttribute(by.css(localLF.textBox_locator.replace("dynamic", "groupname")), "value", testdata_LFGroupName, "LFGroupName")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Add/Modify the dayparts,tags  details for the selected record and click on save", async () => {

        let stepAction = "Add/Modify the dayparts,tags  details for the selected record and click on save";
        let stepData = "";
        let stepExpResult = "System should display sucessfully details updated message and dayparts, tags details should be updated";
        let stepActualResult = "System should display sucessfully details updated message and dayparts, tags details should be updated";

        try {
            await action.Click(localLF.addDeviation, "addDeviation")
            await localLF.selectSingleSelectDropDownOptions(element(by.css(localLF.singleSelectDropdownLocator.replace("dynamic", "daypartname"))), localLF.singleSelectDropdownOptionsLocator, daypartname_afterChange, "RateCard")
            await commonLib.selectMultiOptionsFromDropDownUsingSearch(localLF.tagsDropDown,[tagName]);
            await localLF.buttonActionsVerify(["xgc-add-edit-length-factor-save"], "Click", "Save Button")
            Deviation_afterChange = await action.makeDynamicLocatorContainsText(localLF.addedDaypartTagDeviation, daypartname_afterChange + " (1 Tags)", "equal")//Daytime (1 Tags)
            await action.Click(Deviation_afterChange, "addedDaypartTagDeviation")
            let EleCount = await element.all(localLF.deviationDefaultLFInputBox).count()
            await console.log(EleCount + " EleCount")
            for (let i = 0; i < EleCount - 1; i++) {
                await action.MouseMoveToElement(element.all(localLF.deviationDefaultLFInputBox).get(i), "deviationLF")
                await action.SetText(element.all(localLF.deviationDefaultLFInputBox).get(i), deviationLF, "deviationLF")
                await element.all(localLF.deviationDefaultLFInputBox).get(i).sendKeys(protractor.Key.TAB)
            }
            await localLF.buttonActionsVerify(["save"], "Click", "update Button")
            await localLF.verifyToastNotification("Length Factor Group,Length Factor Group updated succesfully")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName, "equal")
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Edit"], "Click", "Edit Button")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await browser.sleep(1000)
            await element.all(localLF.addedDaypartTagDeviation).each(function (item) {
                item.getText().then(function (text) {
                    console.log(text)
                    if (text.includes(daypartname) || text.includes(daypartname_afterChange)) {
                        action.ReportSubStep("verifyDropDownOptionAvailable", " options available", "Pass")

                    }
                    else {
                        action.ReportSubStep("verifyDropDownOptionAvailable", " options not available", "Fail")
                    }
                })
            })
            await localLF.buttonActionsVerify(["cancel"], "Click", "cancel Button")
            await browser.sleep(3000)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a record and Edit", async () => {

        let stepAction = "Select a record and Edit";
        let stepData = "";
        let stepExpResult = "User able to edit the record";
        let stepActualResult = "User able to edit the record";

        try {
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Edit"], "Click", "Edit Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on cancel in Edit length factor  group details", async () => {

        let stepAction = "Click on cancel in Edit length factor  group details";
        let stepData = "";
        let stepExpResult = "User should navigate to length factor  grid and details should not be changed/updated";
        let stepActualResult = "User should navigate to length factor  grid and details should not be changed/updated";

        try {
            await localLF.buttonActionsVerify(["cancel"], "Click", "cancel Button")
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to qaxgStorybook>xG Pricing>NetworkLength Factor View", async () => {

        let stepAction = "Navigate to qaxgStorybook>xG Pricing>NetworkLength Factor View";
        let stepData = "";
        let stepExpResult = "Check the Network Length Factor View page is displayed";
        let stepActualResult = "Check the Network Length Factor View page is displayed";

        try {
            await home.appclickOnMenuListLeftPanel("Network Length Factors");
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Edit  button", async () => {

        let stepAction = "Click on Edit  button";
        let stepData = "";
        let stepExpResult = "Check Edit button should not perform any action without  selecting any record from length factor grid list.";
        let stepActualResult = "Check Edit button should not perform any action without  selecting any record from length factor grid list.";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown,localLF.singleSelectDropdownOptionsLocator,String(globalTestData.networkName[0]),"Network")
            await localLF.buttonActionsVerify(["Edit"], "Disabled", "Edit Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
