/*
***********************************************************************************************************
* @Script Name :  XGCT-9435
* @Description :  xG Campaign - Length Factor Group - Clone (Network)9-8-2019
* @Page Object Name(s) : LocalLengthFactorsPricing
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib,globalvalues,HomePageFunction
* @Pre-Conditions : 
* @Creation Date : 9-8-2019
* @Author : Adithya K
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, by } from 'protractor';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../../TestData/globalTestData';
import { AppCommonFunctions } from '../../../../../../Libs/ApplicationLibs/CommAppLib';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let home = new HomePageFunction();
let localLF = new LocalLengthFactorsPricing();
let id: string
let testdata_LFGroupName: string
// let testdata_Description: string
let testdata_CloneName: string
let cloneLengthFactorName_Textbox
// let testdata_RetrievedDescription

//Variables Declaration
let TestCase_ID = 'XGCT-9435'
let TestCase_Title = 'xG Campaign - Length Factor Group - Clone (Network)'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - LENGTH FACTOR GROUP - CLONE (NETWORK)', () => {

    // --------------Test Step------------
    it("Navigate to the application", async () => {

        let stepAction = "Navigate to the application";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "Should display  QAxGstorybook page.";
        let stepActualResult = "Displayed QAxGstorybook page.";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Pricing>  Network Length Factor View.", async () => {

        let stepAction = "Navigate to Pricing>  Network Length Factor View.";
        let stepData = "";
        let stepExpResult = "Check the Network Length Factor View page is displayed";
        let stepActualResult = "Network Length Factor View page is displayed";

        try {
            await home.appclickOnNavigationList(globalTestData.pricingTabName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuNetworkLengthFactors);
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a record from Length Factor group grid list.", async () => {

        let stepAction = "Select a record from Length Factor group grid list.";
        let stepData = "";
        let stepExpResult = "Record should be selected.";
        let stepActualResult = "Record is selected.";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown,localLF.singleSelectDropdownOptionsLocator,String(globalTestData.networkName[0]),"Network")
            await browser.sleep(5000);
            await action.MouseMoveToElement(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Clone button.", async () => {

        let stepAction = "Click on Clone button.";
        let stepData = "";
        let stepExpResult = "System should show option to enter title name of the  Length Factor clone";
        let stepActualResult = "Able to enter title name of the Length Factor clone";

        try {
            await localLF.buttonActionsVerify(["Clone"],"Click","Clone Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter the Title.Check the user is presented with an option to update a unique name of the new Length Factor group.", async () => {

        let stepAction = "Enter the Title.Check the user is presented with an option to update a unique name of the new Length Factor group.";
        let stepData = "";
        let stepExpResult = "Clone button should be enabled and user is able to enter the unique name in Title box.";
        let stepActualResult = "Clone button is enabled and user is able to enter the unique name in Title box.";

        try {
            id = await localLF.makeAlphaNumericId(5)
            testdata_CloneName = id + "Clone"
            cloneLengthFactorName_Textbox= by.css(localLF.textBox_locator.replace("dynamic","name"))
            await action.SetText(cloneLengthFactorName_Textbox, testdata_CloneName, "Clone_Name")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Save button.Check the user can perform a clone operation for a existing length factor group.", async () => {

        let stepAction = "Click on Save button.Check the user can perform a clone operation for a existing length factor group.";
        let stepData = "";
        let stepExpResult = "Should be able to perform a clone operation for the existing record from Length Factor Group grid displayed list.";
        let stepActualResult = "Able to perform a clone operation for the existing record from Length Factor Group grid displayed list.";

        try {
            await localLF.buttonActionsVerify(["Save"],"Click","save Button")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await browser.sleep(5000)
            await localLF.gridFilter("Group Name", testdata_CloneName)
            await localLF.gridDataValidation("Group Name", testdata_CloneName,"equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            await browser.sleep(500)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a record from existing Length Factor Group and click on Clone.", async () => {

        let stepAction = "Select a record from existing Length Factor Group and click on Clone.";
        let stepData = "";
        let stepExpResult = "Record should be selected and should show an option to enter the title name of the cloned record.";
        let stepActualResult = "Record is selected and able to enter the title name of the cloned record.";

        try {
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Clone"],"Click","Clone Button")
            await verify.verifyElement(cloneLengthFactorName_Textbox, "cloneLengthFactorName_Textbox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter Length Factor Title name as existing Length Factor Group name  and Click on Save button.", async () => {

        let stepAction = "Enter Length Factor Title name as existing Length Factor Group name  and Click on Save button.";
        let stepData = "";
        let stepExpResult = "'System should display an error message.''Length Factor Group Name already Exists.'''";
        let stepActualResult = "An error message is displayed as 'Length Factor Group Name already Exists'";

        try {
            await action.SetText(cloneLengthFactorName_Textbox, testdata_CloneName, "Clone_Name")
            await localLF.buttonActionsVerify(["Save"],"Click","save Button")
            await localLF.verifyToastNotification("Length Factor Group,Length Factor Group Name already Exists")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.buttonActionsVerify(["Cancel"],"Click","Cancel Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a record from  Length Factor Group displayed grid list and click on Clone.", async () => {

        let stepAction = "Select a record from  Length Factor Group displayed grid list and click on Clone.";
        let stepData = "";
        let stepExpResult = "Record should be selected and should show an option to enter the title name of the cloned record.";
        let stepActualResult = "Record is selected and able to enter the title name of the cloned record.";

        try {
            await localLF.buttonActionsVerify(["Clone"],"Click","Clone Button")
            await verify.verifyElement(cloneLengthFactorName_Textbox, "cloneLengthFactorName_Textbox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Cancel button on Title box.", async () => {

        let stepAction = "Click on Cancel button on Title box.";
        let stepData = "";
        let stepExpResult = "User should be navigated to Length Factor View Grid.";
        let stepActualResult = "User is navigated to Length Factor View Grid.";

        try {
            await localLF.buttonActionsVerify(["Cancel"],"Click","Cancel Button")
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a record from  Length Factor Group displayed grid list and click on Clone.", async () => {

        let stepAction = "Select a record from  Length Factor Group displayed grid list and click on Clone.";
        let stepData = "";
        let stepExpResult = "Record should be selected and should show an option to enter the title name of the cloned record.";
        let stepActualResult = "Record is selected and able to enter the title name of the cloned record.";

        try {
            await localLF.buttonActionsVerify(["Clone"],"Click","Clone Button")
            await verify.verifyElement(cloneLengthFactorName_Textbox, "cloneLengthFactorName_Textbox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Provide unique name and  click on Save in Title box.", async () => {

        let stepAction = "Provide unique name and  click on Save in Title box.";
        let stepData = "";
        let stepExpResult = "'Should display message''Successfully cloned  Length Factor Group''.'";
        let stepActualResult = "'Successfully cloned  Length Factor Group' message is displayed";

        try {
            id = await localLF.makeAlphaNumericId(5)
            testdata_CloneName = id + "Clone"
            await action.SetText(cloneLengthFactorName_Textbox, testdata_CloneName, "Clone_Name")
            await localLF.buttonActionsVerify(["Save"],"Click","save Button")
            await browser.sleep(500)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("After Confirmation message check newly cloned record  is visible on top of the Length Factor Grid.", async () => {

        let stepAction = "After Confirmation message check newly cloned record  is visible on top of the Length Factor Grid.";
        let stepData = "";
        let stepExpResult = "System should display newly cloned records on the top of the grid.";
        let stepActualResult = "Newly cloned record is displayed on the top of the grid.";

        try {
            await localLF.gridFilter("Group Name", testdata_CloneName)
            await localLF.gridDataValidation("Group Name", testdata_CloneName,"equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Elect to continue the clone operation. Check the cloned Record has captured all details of the original Length Factor.", async () => {

        let stepAction = "Elect to continue the clone operation. Check the cloned Record has captured all details of the original Length Factor.";
        let stepData = "";
        let stepExpResult = "Cloned record should have all details of the original.";
        let stepActualResult = "Cloned record is having all details of the original.";

        try {
            id = await localLF.makeAlphaNumericId(5)
            testdata_CloneName = id + "Clone"
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Clone"],"Click","Clone Button")
            await verify.verifyElement(cloneLengthFactorName_Textbox, "cloneLengthFactorName_Textbox")
            await action.SetText(cloneLengthFactorName_Textbox, testdata_CloneName, "Clone_Name")
            await localLF.buttonActionsVerify(["Save"],"Click","save Button")
            await browser.sleep(500)
            await localLF.verifyToastNotification("Length Factor Group,Successfully cloned Length Factor Group")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_CloneName)
            await localLF.gridDataValidation("Group Name", testdata_CloneName,"equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Clone button without selecting Length Factor in Grid.", async () => {

        let stepAction = "Click on Clone button without selecting Length Factor in Grid.";
        let stepData = "";
        let stepExpResult = "Clone button should be disabled.";
        let stepActualResult = "Clone button is disabled.";

        try {
            await localLF.buttonActionsVerify(["Clone"],"Disabled","Clone Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Add button.Select Channels,Length Factor Group,Description,Round To.Click on Save.", async () => {

        let stepAction = "Click on Add button.Select Channels,Length Factor Group,Description,Round To.Click on Save.";
        let stepData = "";
        let stepExpResult = "New Length Factor Group should be added successfully.";
        let stepActualResult = "New Length Factor Group is added successfully.";

        try {
            id = await localLF.makeAlphaNumericId(5)
            testdata_LFGroupName = id + "Name"
            await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown,localLF.singleSelectDropdownOptionsLocator,String(globalTestData.networkName[0]),"Network")
            await localLF.buttonActionsVerify(["Add"],"Click","Add Button")
            await browser.sleep(3000)
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic","groupname")),testdata_LFGroupName,"LFGroupName")
            await localLF.buttonActionsVerify(["Save"],"Click","Save Button")
            await verify.verifyProgressSpinnerCircleNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check New Length Factor Group is available in the Grid or not.", async () => {

        let stepAction = "Check New Length Factor Group is available in the Grid or not.";
        let stepData = "";
        let stepExpResult = "Length Factor Group grid should  display newly created record.";
        let stepActualResult = "newly created record is displayed in Length Factor Group grid .";

        try {
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName,"equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select more than 1 record from Length Factor Group grid. ", async () => {

        let stepAction = "Select more than 1 record from Length Factor Group grid. ";
        let stepData = "";
        let stepExpResult = "Clone button should be disabled.";
        let stepActualResult = "Clone button is disabled.";

        try {
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await action.Click(localLF.secondRecordCheckbox, "secondRecordCheckbox")
            await localLF.buttonActionsVerify(["Clone"],"Disabled","Clone Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a record from  Length Factor Group displayed grid list and click on Clone.", async () => {

        let stepAction = "Select a record from  Length Factor Group displayed grid list and click on Clone.";
        let stepData = "";
        let stepExpResult = "Record should be selected and should show an option to enter the title name of the cloned record.";
        let stepActualResult = "Record is selected and should able to enter the title name of the cloned record.";

        try {
            await action.Click(localLF.secondRecordCheckbox, "secondRecordCheckbox")
            await localLF.buttonActionsVerify(["Clone"],"Click","Clone Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Save button in title box without entering Length Factor Group title.", async () => {

        let stepAction = "Click on Save button in title box without entering Length Factor Group title.";
        let stepData = "";
        let stepExpResult = "'System should show valid error message.''Length Factor Group should not be blank'''";
        let stepActualResult = "An error message is displayed as 'Length Factor Group should not be blank'";

        try {
            await localLF.buttonActionsVerify(["Save"],"Click","save Button")
            await localLF.verifyToastNotification("Length Factor Group,Length Factor Group should not be blank.")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
