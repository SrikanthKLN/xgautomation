/*
***********************************************************************************************************
* @Script Name :  XGCT-9031
* @Description :  xG Campaign - Pricing - Verify the user has selected the channel / network, verify that there's a populated length factor list with length factor group name for these channels.6-8-2019
* @Page Object Name(s) : LocalLengthFactorsPricing
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib,globalvalues,HomePageFunction
* @Pre-Conditions : 
* @Creation Date : 6-8-2019
* @Author : Adithya K
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../../TestData/globalTestData';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let home = new HomePageFunction();
let localLF = new LocalLengthFactorsPricing();

let id: string
let testdata_LFGroupName: string
// let testdata_Description: string
let roundToOptions: string="Exact"

//Variables Declaration
let TestCase_ID = 'XGCT-9031'
let TestCase_Title = 'xG Campaign - Pricing - Verify the user has selected the channel / network verify that theres a populated length factor list with length factor group name for these channels.'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - PRICING - VERIFY THE USER HAS SELECTED THE CHANNEL / NETWORK VERIFY THAT THERES A POPULATED LENGTH FACTOR LIST WITH LENGTH FACTOR GROUP NAME FOR THESE CHANNELS.', () => {

    // --------------Test Step------------
    it("Navigate to the application", async () => {

        let stepAction = "Navigate to the application";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "User should be able to open the application";
        let stepActualResult = "User is able to open the application";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to  Pricing>Local/network Length Factor view", async () => {

        let stepAction = "Navigate to  Pricing>Local/network Length Factor view";
        let stepData = "";
        let stepExpResult = "Check the Length Factor view page is displayed";
        let stepActualResult = "Check the Length Factor view page is displayed";

        try {
            await home.appclickOnNavigationList(globalTestData.pricingTabName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuLocalLengthFactors);
            await verify.verifyElement(localLF.gridLocator,"LengthFactor Grid");
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("'Select a Market as ''Pittsburgh'' and click Add button.'", async () => {

        let stepAction = "'Select a Market as ''Pittsburgh'' and click Add button.'";
        let stepData = "";
        let stepExpResult = "'User should be get ''Add Length Factor Group'' Popup.'";
        let stepActualResult = "Add Length Factor Group Popup is displayed";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown,localLF.singleSelectDropdownOptionsLocator,globalTestData.marketName,"Market")
            await localLF.buttonActionsVerify(["Add"],"Click","Add Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("'Select Channel as ''WPCB'', Length Factor Group as ''Test'', Description as ''Test'', Round to Value as ''Exact''.'", async () => {

        let stepAction = "'Select Channel as ''WPCB'', Length Factor Group as ''Test'', Description as ''Test'', Round to Value as ''Exact''.'";
        let stepData = "";
        let stepExpResult = "User should be able to enter the data successful.";
        let stepActualResult = "User is to enter the data successful.";

        try {
            id = await localLF.makeAlphaNumericId(5)
            testdata_LFGroupName = id + "Name"
            await localLF.eraseMultiselectDropdownSelection(localLF.channelMultiSelectDropDown,"Channels")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,globalTestData.singleChannel,"Channels")
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic","groupname")),testdata_LFGroupName,"LFGroupName")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on the save button", async () => {

        let stepAction = "Click on the save button";
        let stepData = "";
        let stepExpResult = "'User should be able to save. and get the ''Test'' Group name record in the grid.'";
        let stepActualResult = "User is able to save. and created Group name record is displayed in the grid.'";

        try {
            await localLF.buttonActionsVerify(["Save"],"Click","Save Button")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName,"equal")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Pricing> Local/Network rate card view> Add", async () => {

        let stepAction = "Navigate to Pricing> Local/Network rate card view> Add";
        let stepData = "";
        let stepExpResult = "Add rate card page should be displayed.";
        let stepActualResult = "Add rate card page is displayed.";

        try {
            await home.appclickOnMenuListLeftPanel(globalTestData.localRateCardleftMenuName);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("'Select Market as ''Pittsburgh'' and Channel as ''WPCB'', Then click on Length factor group drop down.'", async () => {

        let stepAction = "'Select Market as ''Pittsburgh'' and Channel as ''WPCB'', Then click on Length factor group drop down.'";
        let stepData = "";
        let stepExpResult = "'Check that Length factor group name added in the drop down.'";
        let stepActualResult = "Created Length factor group name is added in the drop down";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown,localLF.singleSelectDropdownOptionsLocator,globalTestData.marketName,"Market")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,globalTestData.singleChannel,"Channels")
            await localLF.buttonActionsVerify(["Add"],"Click","Add Button")
            await localLF.selectMultiSelectDropDownOptions(element.all(localLF.channelMultiSelectDropDown).get(1),localLF.multiSelectDropdownOptionsLocator,globalTestData.singleChannel,"Channels")
            await browser.sleep(3000)
            await localLF.verifyDropDownOptionAvailable(element(by.css(localLF.singleSelectDropdownLocator.replace("dynamic","lengthFactors"))),localLF.singleSelectDropdownOptionsLocator,[testdata_LFGroupName],"Length Factor Dropdown")
            await localLF.buttonActionsVerify(["Cancel"],"Click","Cancel Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate toxG Pricing>Local/Network Length Factor view", async () => {

        let stepAction = "Navigate toxG Pricing>Local/Network Length Factor view";
        let stepData = "";
        let stepExpResult = "Check the Length Factor view page is displayed";
        let stepActualResult = " Length Factor view page is displayed";

        try {
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuLocalLengthFactors);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("'Select Channel as ''WPCB'' & ''WPGH'', Length Factor Group as ''Test1'', Description as ''Test'', Round to Value as ''Exact''.'", async () => {

        let stepAction = "'Select Channel as ''WPCB'' & ''WPGH'', Length Factor Group as ''Test1'', Description as ''Test'', Round to Value as ''Exact''.'";
        let stepData = "";
        let stepExpResult = "User should be able to enter the data successful.";
        let stepActualResult = "User is able to enter the data successful.";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown,localLF.singleSelectDropdownOptionsLocator,globalTestData.marketName,"Market")
            await localLF.buttonActionsVerify(["Add"],"Click","Add Button")
            id = await localLF.makeAlphaNumericId(5)
            testdata_LFGroupName = id + "Name"
            await localLF.eraseMultiselectDropdownSelection(localLF.channelMultiSelectDropDown,"Channels")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,globalTestData.multipleChnnels,"Channels")
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic","groupname")),testdata_LFGroupName,"LFGroupName")
            await localLF.buttonActionsVerify(["Save"],"Click","Save Button")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName,"equal")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Pricing> Local/Network rate card view> Add", async () => {

        let stepAction = "Navigate to Pricing> Local/Network rate card view> Add";
        let stepData = "";
        let stepExpResult = "Add rate card page should be displayed.";
        let stepActualResult = "Add rate card page is displayed.";

        try {
            await home.appclickOnMenuListLeftPanel(globalTestData.localRateCardleftMenuName);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("'Select Market as ''Pittsburgh'' and Channel as ''WPCB'' & ''WPGH'', Then click on Length factor group drop down.'", async () => {

        let stepAction = "'Select Market as ''Pittsburgh'' and Channel as ''WPCB'' & ''WPGH'', Then click on Length factor group drop down.'";
        let stepData = "";
        let stepExpResult = "'Check that Length factor group name added in the drop down.'";
        let stepActualResult = "Newly created Length factor group name is added in the drop down.'";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown,localLF.singleSelectDropdownOptionsLocator,globalTestData.marketName,"Market")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,globalTestData.multipleChnnels,"Channels")
            await localLF.buttonActionsVerify(["Add"],"Click","Add Button")
            await localLF.selectMultiSelectDropDownOptions(element.all(localLF.channelMultiSelectDropDown).get(1),localLF.multiSelectDropdownOptionsLocator,globalTestData.multipleChnnels,"Channels")
            await browser.sleep(3000)
            await localLF.verifyDropDownOptionAvailable(element(by.css(localLF.singleSelectDropdownLocator.replace("dynamic","lengthFactors"))),localLF.singleSelectDropdownOptionsLocator,[testdata_LFGroupName],"Length Factor Dropdown")
            await localLF.buttonActionsVerify(["Cancel"],"Click","Cancel Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("'Select role as ''Network admin'' in Knobs section'", async () => {

        let stepAction = "'Select role as ''Network admin'' in Knobs section'";
        let stepData = "";
        let stepExpResult = "Network admin should be selected";
        let stepActualResult = "Network admin is selected";

        try {
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuNetworkLengthFactors);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("'Select ''CBS'' network, Length Factor Group as ''Test'', Description as ''Test'', Round to as ''Exact'''", async () => {

        let stepAction = "'Select ''CBS'' network, Length Factor Group as ''Test'', Description as ''Test'', Round to as ''Exact'''";
        let stepData = "";
        let stepExpResult = "User should be able to enter the data successful.";
        let stepActualResult = "User is able to enter the data successful.";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown,localLF.singleSelectDropdownOptionsLocator,String(globalTestData.networkName[0]),"Network")
            id = await localLF.makeAlphaNumericId(5)
            testdata_LFGroupName = id + "Name"
            await localLF.buttonActionsVerify(["Add"],"Click","Add Button")
            await browser.sleep(3000)
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic","groupname")),testdata_LFGroupName,"LFGroupName")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on the save button", async () => {

        let stepAction = "Click on the save button";
        let stepData = "";
        let stepExpResult = "'User should be able to save. and get the ''Test'' Group name record in the grid.'";
        let stepActualResult = "'User is able to save. and Created Group name record is displayed in the grid.'";

        try {
            await localLF.buttonActionsVerify(["Save"],"Click","Save Button")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName,"equal")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Pricing> Local/Network rate card view> Add", async () => {

        let stepAction = "Navigate to Pricing> Local/Network rate card view> Add";
        let stepData = "";
        let stepExpResult = "Add rate card page should be displayed.";
        let stepActualResult = "Add rate card page is displayed.";

        try {
            await home.appclickOnMenuListLeftPanel(globalTestData.networkRateCardleftMenuName);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("'Select Network as ''CBS'' then click on Length Factor drop down.'", async () => {

        let stepAction = "'Select Network as ''CBS'' then click on Length Factor drop down.'";
        let stepData = "";
        let stepExpResult = "'Check that Created Length factor group name added in the drop down.'";
        let stepActualResult = "Created Length factor group name is added in the drop down.'";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown,localLF.singleSelectDropdownOptionsLocator,globalTestData.networkName[0],"Market")
            await localLF.buttonActionsVerify(["Add"],"Click","Add Button")
            await localLF.verifyDropDownOptionAvailable(element(by.css(localLF.singleSelectDropdownLocator.replace("dynamic","lengthFactors"))),localLF.singleSelectDropdownOptionsLocator,[testdata_LFGroupName],"Length Factor Dropdown")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
