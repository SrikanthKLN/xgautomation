/*
***********************************************************************************************************
* @Script Name :  XGCT-9026
* @Description :  xG Campaign - Length Factor Group - Edit7-8-2019
* @Page Object Name(s) : LocalLengthFactorsPricing
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib,globalvalues,HomePageFunction
* @Pre-Conditions : 
* @Creation Date : 7-8-2019
* @Author : Adithya K
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../../TestData/globalTestData';
import { AppCommonFunctions } from '../../../../../../Libs/ApplicationLibs/CommAppLib';
import { LocalTagsPage } from '../../../../../../Pages/Inventory/Reference/Tags/LocalTags';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let home = new HomePageFunction();
let localLF = new LocalLengthFactorsPricing();
let commonLib = new AppCommonFunctions();
let localTags = new LocalTagsPage(); 
let id: string
let testdata_LFGroupName: string
// let testdata_Description: string
let daypartname: string = "Morning"
let daypartname_afterChange: string = "Daytime"
let roundToOptions: string = "Exact"
let roundToOptions_afterChange: string = "Nearest $0.10"

// let testdata_RetrievedDescription
let deviationLF: string = "1"
let Deviation_afterChange
let tagName

//Variables Declaration
let TestCase_ID = 'XGCT-9026'
let TestCase_Title = 'xG Campaign - Length Factor Group - Edit'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - LENGTH FACTOR GROUP - EDIT', () => {

    // --------------Test Step------------
    it("Login to the application", async () => {

        let stepAction = "Login to the application";
        let stepData = globalvalues.urlname;
        let stepExpResult = "Login should be successfull";
        let stepActualResult = "Successfully Logged on";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Pricing>  Length Factor View.", async () => {

        let stepAction = "Navigate to Pricing>  Length Factor View.";
        let stepData = "";
        let stepExpResult = "Check the Length Factor View page should be displayed";
        let stepActualResult = "Check the Length Factor View page is displayed";
       
        try {
            await home.appclickOnNavigationList(globalTestData.programmingTabName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuTagsName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuLocalTagsName);
            await browser.sleep(5000);
            await verify.verifyProgressBarNotPresent();
            await commonLib.selectDropDownValue("Market",globalTestData.marketName);
            await commonLib.selectMultiOptionsFromDropDownUsingSearch(localTags.channelDropDown,globalTestData.singleChannel);
            await verify.verifyProgressBarNotPresent();
            await action.SetText(commonLib.globalFilter,"EM,DT","");
            await verify.verifyProgressBarNotPresent();
            let status = await verify.verifyElementVisible(commonLib.noDataMatchFound);
            if(status)
            {
                await localLF.buttonActionsVerify(["tag"], "Click", "Add Button");
                tagName = "AT_"+new Date().getTime();
                await commonLib.selectMultiOptionsFromDropDownUsingSearch(localTags.channelDropDownInPopup,globalTestData.singleChannel);
                await action.ClearText(localTags.nameInPopUp,"");
                await action.SetText(localTags.nameInPopUp,tagName,"");
                await action.Click(localTags.createButtonInPopUp,"Click On Create Button");
                await verify.verifyProgressBarNotPresent();
                await commonLib.verifyPopupMessage("Successfully Tag added");
                await verify.verifyProgressBarNotPresent();
                await commonLib.selectDropDownValue("Market",globalTestData.marketName);
                await commonLib.selectMultiOptionsFromDropDownUsingSearch(localTags.channelDropDown,globalTestData.singleChannel);
                await verify.verifyProgressBarNotPresent();
                await action.SetText(commonLib.globalFilter,tagName,"");
                await verify.verifyProgressBarNotPresent();
                let status = await verify.verifyElementVisible(commonLib.noDataMatchFound);
                if(status) throw "Failed- Created tag "+tagName+" is not displayed in the grid"
            }
            else
            {
                let columnIndex = await commonLib.getTableColumnIndex("Name");
                tagName = await action.GetText(by.xpath(commonLib.gridCellData.replace("rowdynamic","1").replace("columndynamic",String(columnIndex))),"");
                tagName = String(tagName).trim();
            }
            await home.appclickOnNavigationList(globalTestData.pricingTabName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuLocalLengthFactors);
            await verify.verifyProgressBarNotPresent();
            await verify.verifyElement(localLF.gridLocator, "LengthFactor Grid");
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Select Market or network", async () => {

        let stepAction = "Select Market or network";
        let stepData = "";
        let stepExpResult = "Market or network should be selected and should display channels list";
        let stepActualResult = "Market or network is selected and displayed channels list";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Click on add button", async () => {

        let stepAction = "Click on add button";
        let stepData = "";
        let stepExpResult = "Add length factor group window should be appear.";
        let stepActualResult = "Add length factor group window is appeared.";

        try {
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });



    // --------------Test Step------------
    it("Select Channel", async () => {

        let stepAction = "Select Channel";
        let stepData = "";
        let stepExpResult = "Channel should be selected";
        let stepActualResult = "Channel is selected";

        try {
            await localLF.eraseMultiselectDropdownSelection(localLF.channelMultiSelectDropDown, "Channels")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, globalTestData.singleChannel, "Channels")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Enter Length factor group name", async () => {

        let stepAction = "Enter Length factor group name";
        let stepData = "";
        let stepExpResult = "Length factor group name should be taken";
        let stepActualResult = "Length factor group name is entered";

        try {
            id = await localLF.makeAlphaNumericId(5)
            testdata_LFGroupName = id + "Name"
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic", "groupname")), testdata_LFGroupName, "LFGroupName")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Round To", async () => {

        let stepAction = "Select Round To";
        let stepData = "";
        let stepExpResult = "Round To should be selected";
        let stepActualResult = "Round To is selected";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.roundToDropdown, localLF.singleSelectDropdownOptionsLocator, roundToOptions, "roundToOptions")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Deviation (+) icon and Select Dayparts and Tags", async () => {

        let stepAction = "Click on Deviation (+) icon and Select Dayparts and Tags";
        let stepData = "";
        let stepExpResult = "Dayparts and Tags should be selected";
        let stepActualResult = "Dayparts and Tags is selected";

        try {
            await action.Click(localLF.addDeviation, "addDeviation")
            await localLF.selectSingleSelectDropDownOptions(element(by.css(localLF.singleSelectDropdownLocator.replace("dynamic", "daypartname"))), localLF.singleSelectDropdownOptionsLocator, daypartname, "RateCard")
            await commonLib.selectMultiOptionsFromDropDownUsingSearch(localLF.tagsDropDown,[tagName]);
            await localLF.buttonActionsVerify(["xgc-add-edit-length-factor-save"], "Click", "Save Button")
            await action.Click(localLF.addedDaypartTagDeviation, "addedDaypartTagDeviation")
            let EleCount = await element.all(localLF.deviationDefaultLFInputBox).count()
            await console.log(EleCount + " EleCount")
            for (let i = 0; i < EleCount - 1; i++) {
                await action.MouseMoveToElement(element.all(localLF.deviationDefaultLFInputBox).get(i), "deviationLF")
                await action.SetText(element.all(localLF.deviationDefaultLFInputBox).get(i), deviationLF, "deviationLF")
                await element.all(localLF.deviationDefaultLFInputBox).get(i).sendKeys(protractor.Key.TAB)
            }

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Save", async () => {

        let stepAction = "Click on Save";
        let stepData = "";
        let stepExpResult = "Length factor group should created and should show successfully created message";
        let stepActualResult = "Length factor group is created and successfully created message is displayed";

        try {
            await localLF.buttonActionsVerify(["save"], "Click", "Save Button")
            await localLF.verifyToastNotification("Length Factor Group,Length Factor Group added succesfully")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName, "equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Market", async () => {

        let stepAction = "Select Channel";
        let stepData = "";
        let stepExpResult = "Channel should be selected and should display selected market,channel releated length factor group list in the grid";
        let stepActualResult = "Channel is selected and displayed selected market,channel releated length factor group list in the grid";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Verify the edit option is available in length factors view", async () => {

        let stepAction = "Verify the edit option is available in length factors view";
        let stepData = "";
        let stepExpResult = "Edit  button should be available in length factor view";
        let stepActualResult = "Edit button is available in length factor view";

        try {
            await localLF.buttonActionsVerify(["Edit"], "Displayed", "Edit Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a record from Length factors view grid", async () => {

        let stepAction = "Select a record from Length factors view grid";
        let stepData = "";
        let stepExpResult = "Record should be selected from length factor view grid and  should have option to edit  the selected record";
        let stepActualResult = "Record is selected from length factor view grid and have option to edit  the selected record";

        try {
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Edit"], "Enabled", "Edit Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select more than one record from  Length factors view grid", async () => {

        let stepAction = "Select more than one record from  Length factors view grid";
        let stepData = "";
        let stepExpResult = "Edit button should be disabled";
        let stepActualResult = "Edit button is disabled";

        try {
            await action.Click(localLF.secondRecordCheckbox, "secondRecordCheckbox")
            await localLF.buttonActionsVerify(["Edit"], "Disabled", "Edit Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a record from  Length factors view grid and click on Edit button", async () => {

        let stepAction = "Select a record from  Length factors view grid and click on Edit button";
        let stepData = "";
        let stepExpResult = "Record should be selected from length factor view grid and user should have option to edit length factor details";
        let stepActualResult = "Record is selected from length factor view grid and user has option to edit length factor details";

        try {
            await action.Click(localLF.secondRecordCheckbox, "secondRecordCheckbox")
            await localLF.buttonActionsVerify(["Edit"], "Click", "Edit Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("select round to value from the Round To drop down list", async () => {

        let stepAction = "select round to value from the Round To drop down list";
        let stepData = "";
        let stepExpResult = "Round To value should be selected";
        let stepActualResult = "Round To value is selected";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.roundToDropdown, localLF.singleSelectDropdownOptionsLocator, roundToOptions_afterChange, "roundToOptions")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Save", async () => {

        let stepAction = "Click on Save";
        let stepData = "";
        let stepExpResult = "Round To drop down Changes should be updated";
        let stepActualResult = "Round To drop down Changes is updated";

        try {
            await localLF.buttonActionsVerify(["save"], "Click", "update Button")
            await localLF.verifyToastNotification("Length Factor Group,Length Factor Group updated succesfully")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName, "equal")
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Edit"], "Click", "Edit Button")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await browser.sleep(1000)
            await verify.verifyTextOfElement(localLF.roundToDropdown, roundToOptions_afterChange, "roundToOptions_afterChange")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Change/Modify the Length Factor Group details for the selected record and click on save", async () => {

        let stepAction = "Change/Modify the Length Factor Group details for the selected record and click on save";
        let stepData = "";
        let stepExpResult = "System should display sucessfully details updated message and  Length factor group name should be updated";
        let stepActualResult = "System is displayed sucessfully details updated message and  Length factor group name  is updated";

        try {
            id = await localLF.makeAlphaNumericId(5)
            testdata_LFGroupName = id + "Name"
            await action.ClearText(by.css(localLF.textBox_locator.replace("dynamic", "groupname")), "LFGroupName")
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic", "groupname")), testdata_LFGroupName, "LFGroupName")
            await localLF.buttonActionsVerify(["save"], "Click", "update Button")
            await localLF.verifyToastNotification("Length Factor Group,Length Factor Group updated succesfully")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName, "equal")
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Edit"], "Click", "Edit Button")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await browser.sleep(1000)
            await verify.verifyElementAttribute(by.css(localLF.textBox_locator.replace("dynamic", "groupname")), "value", testdata_LFGroupName, "LFGroupName")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Add/Modify the dayparts,tags  details for the selected record and click on save", async () => {

        let stepAction = "Add/Modify the dayparts,tags  details for the selected record and click on save";
        let stepData = "";
        let stepExpResult = "System should display sucessfully details updated message and dayparts, tags details should be updated";
        let stepActualResult = "System is displayed sucessfully details updated message and dayparts, tags details is updated";

        try {
            await action.Click(localLF.addDeviation, "addDeviation")
            await localLF.selectSingleSelectDropDownOptions(element(by.css(localLF.singleSelectDropdownLocator.replace("dynamic", "daypartname"))), localLF.singleSelectDropdownOptionsLocator, daypartname_afterChange, "RateCard")
            await commonLib.selectMultiOptionsFromDropDownUsingSearch(localLF.tagsDropDown,[tagName]);
            await localLF.buttonActionsVerify(["xgc-add-edit-length-factor-save"], "Click", "Save Button")
            Deviation_afterChange = await action.makeDynamicLocatorContainsText(localLF.addedDaypartTagDeviation, daypartname_afterChange + " (1 Tags)", "equal")//Daytime (1 Tags)
            await action.Click(Deviation_afterChange, "addedDaypartTagDeviation")
            let EleCount = await element.all(localLF.deviationDefaultLFInputBox).count()
            await console.log(EleCount + " EleCount")
            for (let i = 0; i < EleCount - 1; i++) {
                await action.MouseMoveToElement(element.all(localLF.deviationDefaultLFInputBox).get(i), "deviationLF")
                await action.SetText(element.all(localLF.deviationDefaultLFInputBox).get(i), deviationLF, "deviationLF")
                await element.all(localLF.deviationDefaultLFInputBox).get(i).sendKeys(protractor.Key.TAB)
            }
            await localLF.buttonActionsVerify(["save"], "Click", "update Button")
            await localLF.verifyToastNotification("Length Factor Group,Length Factor Group updated succesfully")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName, "equal")
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Edit"], "Click", "Edit Button")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await browser.sleep(1000)
            await element.all(localLF.addedDaypartTagDeviation).each(function (item) {
                item.getText().then(function (text) {
                    console.log(text)
                    if (text.includes(daypartname) || text.includes(daypartname_afterChange)) {
                        action.ReportSubStep("verifyDropDownOptionAvailable", " options available", "Pass")

                    }
                    else {
                        action.ReportSubStep("verifyDropDownOptionAvailable", " options not available", "Fail")
                    }
                })
            })
            await localLF.buttonActionsVerify(["cancel"], "Click", "cancel Button")
            await browser.sleep(3000)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Select a record and Edit", async () => {

        let stepAction = "Select a record and Edit";
        let stepData = "";
        let stepExpResult = "User able to edit the record";
        let stepActualResult = "User is able to edit the record";

        try {
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Edit"], "Click", "Edit Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Click on cancel in Edit length factor  group details", async () => {

        let stepAction = "Click on cancel in Edit length factor  group details";
        let stepData = "";
        let stepExpResult = "User should navigate to length factor  grid and details should not be changed/updated";
        let stepActualResult = "User is navigated to length factor grid and details is not changed/updated";

        try {
            await localLF.buttonActionsVerify(["cancel"], "Click", "cancel Button")
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to qaxgStorybook>xG Pricing>Local/Network Length Factor View", async () => {

        let stepAction = "Navigate to qaxgStorybook>xG Pricing>Local/Network Length Factor View";
        let stepData = "";
        let stepExpResult = "Check the Length Factor View page is displayed";
        let stepActualResult = "Check the Length Factor View page is displayed";

        try {
            await home.appclickOnMenuListLeftPanel("Local Length Factors");
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Edit  button", async () => {

        let stepAction = "Click on Edit  button";
        let stepData = "";
        let stepExpResult = "Check Edit button should not perform any action without  selecting any record from length factor grid list.";
        let stepActualResult = "Edit button is in disabled without selecting any record from length factor grid list.";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await localLF.buttonActionsVerify(["Edit"], "Disabled", "Edit Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
});
