/*
***********************************************************************************************************
* @Script Name :  XGCT-9028
* @Description :  xG Campaign - Length Factor Group - Delete (Local)8-8-2019
* @Page Object Name(s) : LocalLengthFactorsPricing
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib,globalvalues,HomePageFunction
* @Pre-Conditions : 
* @Creation Date : 8-8-2019
* @Author : Adithya K
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../../TestData/globalTestData';
import { AppCommonFunctions } from '../../../../../../Libs/ApplicationLibs/CommAppLib';
import { LocalTagsPage } from '../../../../../../Pages/Inventory/Reference/Tags/LocalTags';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let home = new HomePageFunction();
let localLF = new LocalLengthFactorsPricing();
let commonLib = new AppCommonFunctions();
let localTags = new LocalTagsPage(); 
let id: string
let testdata_LFGroupName: string
// let testdata_Description: string
let daypartname: string = "Morning"
let roundToOptions: string = "Exact"
let deviationLF: string = "1"
let testdata_RetrievedLFName1: string
let testdata_RetrievedLFName2: string
let tagName;
//Variables Declaration
let TestCase_ID = 'XGCT-9028'
let TestCase_Title = 'xG Campaign - Length Factor Group - Delete (Local)'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - LENGTH FACTOR GROUP - DELETE (LOCAL)', () => {


    // --------------Test Step------------
    it("Navigate to the application", async () => {

        let stepAction = "Navigate to the application";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "User should be navigated to the application";
        let stepActualResult = "User is navigated to the application";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Pricing>Local/Network Length Factor Group>add", async () => {

        let stepAction = "Navigate to Pricing>Local/Network Length Factor Group>add";
        let stepData = "";
        let stepExpResult = "Check the Length Factor View page is displayed";
        let stepActualResult = " Length Factor View page is displayed";

        try {
            await home.appclickOnNavigationList(globalTestData.programmingTabName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuTagsName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuLocalTagsName);
            await browser.sleep(5000);
            await verify.verifyProgressBarNotPresent();
            await commonLib.selectDropDownValue("Market",globalTestData.marketName);
            await commonLib.selectMultiOptionsFromDropDownUsingSearch(localTags.channelDropDown,globalTestData.singleChannel);
            await verify.verifyProgressBarNotPresent();
            await action.SetText(commonLib.globalFilter,"EM","");
            await verify.verifyProgressBarNotPresent();
            let status = await verify.verifyElementVisible(commonLib.noDataMatchFound);
            if(status)
            {
                await localLF.buttonActionsVerify(["tag"], "Click", "Add Button");
                tagName = "AT_"+new Date().getTime();
                await commonLib.selectMultiOptionsFromDropDownUsingSearch(localTags.channelDropDownInPopup,globalTestData.singleChannel);
                await action.ClearText(localTags.nameInPopUp,"");
                await action.SetText(localTags.nameInPopUp,tagName,"");
                await action.Click(localTags.createButtonInPopUp,"Click On Create Button");
                await verify.verifyProgressBarNotPresent();
                await commonLib.verifyPopupMessage("Successfully Tag added");
                await verify.verifyProgressBarNotPresent();
                await commonLib.selectDropDownValue("Market",globalTestData.marketName);
                await commonLib.selectMultiOptionsFromDropDownUsingSearch(localTags.channelDropDown,globalTestData.singleChannel);
                await verify.verifyProgressBarNotPresent();
                await action.SetText(commonLib.globalFilter,tagName,"");
                await verify.verifyProgressBarNotPresent();
                let status = await verify.verifyElementVisible(commonLib.noDataMatchFound);
                if(status) throw "Failed- Created tag "+tagName+" is not displayed in the grid"
            }
            else
            {
                let columnIndex = await commonLib.getTableColumnIndex("Name");
                tagName = await action.GetText(by.xpath(commonLib.gridCellData.replace("rowdynamic","1").replace("columndynamic",String(columnIndex))),"");
                tagName = String(tagName).trim();
            }
            await home.appclickOnNavigationList(globalTestData.pricingTabName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuLocalLengthFactors);
            await verify.verifyProgressBarNotPresent();
            
            await verify.verifyElement(localLF.gridLocator, "LengthFactor Grid");
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Market", async () => {

        let stepAction = "Select Market";
        let stepData = "";
        let stepExpResult = "Market should be selected and should display channels list";
        let stepActualResult = "Market is selected and displayed channels list";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter Length factor group name", async () => {

        let stepAction = "Enter Length factor group name";
        let stepData = "";
        let stepExpResult = "Length factor group name should be taken";
        let stepActualResult = "Length factor group name is taken";

        try {
            id = await localLF.makeAlphaNumericId(5)
            testdata_LFGroupName = id + "Name"
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            await localLF.eraseMultiselectDropdownSelection(localLF.channelMultiSelectDropDown, "Channels")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, globalTestData.singleChannel, "Channels")
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic", "groupname")), testdata_LFGroupName, "LFGroupName")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Round To", async () => {

        let stepAction = "Select Round To";
        let stepData = "";
        let stepExpResult = "Round To should be selected";
        let stepActualResult = "Round To is selected";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.roundToDropdown, localLF.singleSelectDropdownOptionsLocator, roundToOptions, "roundToOptions")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Dayparts and Tags", async () => {

        let stepAction = "Select Dayparts and Tags";
        let stepData = "";
        let stepExpResult = "Dayparts and Tags should be selected";
        let stepActualResult = "Dayparts and Tags are selected";

        try {
            await action.Click(localLF.addDeviation, "addDeviation")
            await localLF.selectSingleSelectDropDownOptions(element(by.css(localLF.singleSelectDropdownLocator.replace("dynamic", "daypartname"))), localLF.singleSelectDropdownOptionsLocator, daypartname, "RateCard")
            await commonLib.selectMultiOptionsFromDropDownUsingSearch(localLF.tagsDropDown,[tagName]);
            await localLF.buttonActionsVerify(["xgc-add-edit-length-factor-save"], "Click", "Save Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on dayparts in deviation and enter deviation length factor", async () => {

        let stepAction = "Click on dayparts in deviation and enter deviation length factor";
        let stepData = "";
        let stepExpResult = "daypart should be clicked and deviation length factor should be taken";
        let stepActualResult = "daypart is clicked and deviation length factor is taken";

        try {
            await action.Click(localLF.addedDaypartTagDeviation, "addedDaypartTagDeviation")
            let EleCount = await element.all(localLF.deviationDefaultLFInputBox).count()
            await console.log(EleCount + " EleCount")
            for (let i = 0; i < EleCount - 1; i++) {
                await action.MouseMoveToElement(element.all(localLF.deviationDefaultLFInputBox).get(i), "deviationLF")
                await action.SetText(element.all(localLF.deviationDefaultLFInputBox).get(i), deviationLF, "deviationLF")
                await element.all(localLF.deviationDefaultLFInputBox).get(i).sendKeys(protractor.Key.TAB)
            }
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Save", async () => {

        let stepAction = "Click on Save";
        let stepData = "";
        let stepExpResult = "Length factor group should be  created  successfully and Created length factor should show in length factor view grid list";
        let stepActualResult = "Length factor group is created  successfully and Created length factor is displayed in length factor view grid list";

        try {
            await localLF.buttonActionsVerify(["save"], "Click", "Save Button")
            await localLF.verifyToastNotification("Length Factor Group,Length Factor Group added succesfully")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName, "equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Market", async () => {

        let stepAction = "Select Market";
        let stepData = "";
        let stepExpResult = "Market should be selected and should display selected market,channel releated length factor group  list in the grid";
        let stepActualResult = "Market is selected and is displayed selected market,channel releated length factor group  list in the grid";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Verify the delete option was avalible in length factors view", async () => {

        let stepAction = "Verify the delete option was avalible in length factors view";
        let stepData = "";
        let stepExpResult = "Delete button should be avaliable in length factor view";
        let stepActualResult = "Delete button is avaliable in length factor view";

        try {
            await localLF.buttonActionsVerify(["Delete"], "Displayed", "Delete Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Delete button", async () => {

        let stepAction = "Click on Delete button";
        let stepData = "";
        let stepExpResult = "Check delete button should not perform any action without selecting any record from length factor grid list.";
        let stepActualResult = "delete button is in disabled without selecting any record from length factor grid list.";

        try {
            await localLF.buttonActionsVerify(["Delete"], "Disabled", "Delete Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a record from Length factors view grid", async () => {

        let stepAction = "Select a record from Length factors view grid";
        let stepData = "";
        let stepExpResult = "Record should be selected from length factor view grid and  should have option to delete the selected record";
        let stepActualResult = "Record is selected from length factor view grid and is an option to delete the selected record";

        try {
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Delete"], "Enabled", "Delete Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a record from  Length factors view grid and click on Yes button", async () => {

        let stepAction = "Select a record from  Length factors view grid and click on Yes button";
        let stepData = "";
        let stepExpResult = "Record should be selected from length factor view grid and  user should be  able to delete selected record successfully";
        let stepActualResult = "Record is selected from length factor view grid and  user is able to delete selected record successfully";

        try {
            testdata_RetrievedLFName1 = String(await action.GetText(localLF.latestGroupName, "latestGroupName"))
            await localLF.buttonActionsVerify(["Delete"], "Click", "Delete Button")
            await localLF.buttonActionsVerify(["Yes"], "Click", "Yes Button")
            await localLF.verifyToastNotification("Length Factor Group,Successfully deleted Length Factor Group")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_RetrievedLFName1)
            await localLF.verifyNodataFoundInGrid()
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select more than one record from  Length factors view grid and click on yes button", async () => {

        let stepAction = "Select more than one record from  Length factors view grid and click on yes button";
        let stepData = "";
        let stepExpResult = "Record should be selected from  length factor view grid and  user should be  able to delete selected record successfully";
        let stepActualResult = "Record is selected from  length factor view grid and user is able to delete selected record successfully";

        try {
            testdata_RetrievedLFName1 = String(await action.GetText(localLF.latestGroupName, "latestGroupName"))
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            testdata_RetrievedLFName2 = String(await action.GetText(localLF.secondGroupName, "secondGroupName"))
            await action.Click(localLF.secondRecordCheckbox, "secondRecordCheckbox")
            await localLF.buttonActionsVerify(["Delete"], "Click", "Delete Button")
            await localLF.buttonActionsVerify(["Yes"], "Click", "Yes Button")
            await localLF.verifyToastNotification("Length Factor Group,Successfully deleted Length Factor Group")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_RetrievedLFName1)
            await localLF.verifyNodataFoundInGrid()
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            await localLF.gridFilter("Group Name", testdata_RetrievedLFName2)
            await localLF.verifyNodataFoundInGrid()
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a record from  Length factors view grid  and click on yes button", async () => {

        let stepAction = "Select a record from  Length factors view grid  and click on yes button";
        let stepData = "";
        let stepExpResult = "Record should be selected from length factor view grid  and should show delete confirmation pop-up message with yes and no buttons";
        let stepActualResult = "Record is selected from length factor view grid and delete confirmation pop-up message with yes and no buttons";

        try {
            testdata_RetrievedLFName1 = String(await action.GetText(localLF.latestGroupName, "latestGroupName"))
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Delete"], "Click", "Delete Button")
            await localLF.buttonActionsVerify(["Yes"], "Displayed", "Yes Button")
            await localLF.buttonActionsVerify(["No"], "Displayed", "No Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a record from  Length factors view grid  and click on yes", async () => {

        let stepAction = "Select a record from  Length factors view grid  and click on yes";
        let stepData = "";
        let stepExpResult = "Record should be selected from length factor view grid  and should show delete confirmation pop-up message with yes and no buttons";
        let stepActualResult = "Record is selected from length factor view grid and delete confirmation pop-up message with yes and no buttons";

        try {
            await localLF.buttonActionsVerify(["Yes"], "Click", "Yes Button")
            await localLF.verifyToastNotification("Length Factor Group,Successfully deleted Length Factor Group")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_RetrievedLFName1)
            await localLF.verifyNodataFoundInGrid()
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on yes option in confirmation pop-up message", async () => {

        let stepAction = "Click on yes option in confirmation pop-up message";
        let stepData = "";
        let stepExpResult = "Selected record should be deleted and system should display successfully deleted confirmation message";
        let stepActualResult = "Selected record is deleted and system displayed successfully deleted confirmation message";

        try {
            testdata_RetrievedLFName1 = String(await action.GetText(localLF.latestGroupName, "latestGroupName"))
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Delete"], "Click", "Delete Button")
            await localLF.buttonActionsVerify(["Yes"], "Displayed", "Yes Button")
            await localLF.buttonActionsVerify(["No"], "Displayed", "No Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on no option in confirmation pop-up message", async () => {

        let stepAction = "Click on no option in confirmation pop-up message";
        let stepData = "";
        let stepExpResult = "User should be navigate to length factor view grid and record should not be deleted";
        let stepActualResult = "User is navigated to length factor view grid and record is not deleted";

        try {
            await localLF.buttonActionsVerify(["No"], "Click", "No Button")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Verify the selected records was available in Length factor view grid", async () => {

        let stepAction = "Verify the selected records was available in Length factor view grid";
        let stepData = "";
        let stepExpResult = "Selected record should be available in length factor view";
        let stepActualResult = "Selected record is available in length factor view";

        try {
            await localLF.gridFilter("Group Name", testdata_RetrievedLFName1)
            await localLF.gridDataValidation("Group Name", testdata_RetrievedLFName1, "equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
