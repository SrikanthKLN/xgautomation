/*
***********************************************************************************************************
* @Script Name :  XGCT-9027
* @Description :  xG Campaign - Length Factor Group - Clone7-8-2019
* @Page Object Name(s) : LocalLengthFactorsPricing
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib,globalvalues,HomePageFunction
* @Pre-Conditions : 
* @Creation Date : 7-8-2019
* @Author : Adithya K
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/
//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../../TestData/globalTestData';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let home = new HomePageFunction();
let localLF = new LocalLengthFactorsPricing();

let id: string
let testdata_LFGroupName: string
// let testdata_Description: string
let testdata_CloneName: string
let cloneLengthFactorName_Textbox
let testdata_RetrievedChannelName
// let testdata_RetrievedDescription

//Variables Declaration
let TestCase_ID = 'XGCT-9027'
let TestCase_Title = 'xG Campaign - Length Factor Group - Clone'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - LENGTH FACTOR GROUP - CLONE', () => {

    // --------------Test Step------------
    it("Login to the application", async () => {

        let stepAction = "Login to the application";
        let stepData = globalvalues.urlname;
        let stepExpResult = "Login should be successfull";
        let stepActualResult = "Successfully logged in";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Pricing>  Length Factor View.", async () => {

        let stepAction = "Navigate to Pricing>  Length Factor View.";
        let stepData = "";
        let stepExpResult = "Check the Length Factor View page is displayed";
        let stepActualResult = " Length Factor View page is displayed";

        try {
            await home.appclickOnNavigationList("Pricing");
            await home.appclickOnMenuListLeftPanel("Reference");
            await home.appclickOnMenuListLeftPanel("Local Length Factors");
            await verify.verifyElement(localLF.gridLocator,"LengthFactor Grid");
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('Select a record from Length Factor group grid list.', async () => {

        let stepAction = 'Select a record from Length Factor group grid list.';
        let stepData = '';
        let stepExpResult = 'Record should be selected.';
        let stepActualResult = 'Record is selected.';

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown,localLF.singleSelectDropdownOptionsLocator,globalTestData.marketName,"Market")
            await action.MouseMoveToElement(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('Click on Clone button.', async () => {

        let stepAction = 'Click on Clone button.';
        let stepData = '';
        let stepExpResult = 'System should show option to enter title name of the  Length Factor clone';
        let stepActualResult = 'Clicked on Clone button';

        try {
            await localLF.buttonActionsVerify(["Clone"],"Click","Clone Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('Enter the Title.Check the user is presented with an option to update a unique name of the new Length Factor group.', async () => {

        let stepAction = 'Enter the Title.Check the user is presented with an option to update a unique name of the new Length Factor group.';
        let stepData = '';
        let stepExpResult = 'Clone button should be enabled and user is able to enter the unique name in Title box.';
        let stepActualResult = 'Clone button is enabled and user is able to enter the unique name in Title box.';

        try {
            id = await localLF.makeAlphaNumericId(5)
            testdata_CloneName = id + "Clone"
            cloneLengthFactorName_Textbox= by.css(localLF.textBox_locator.replace("dynamic","name"))
            await action.SetText(cloneLengthFactorName_Textbox, testdata_CloneName, "Clone_Name")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('Click on Save button.Check the user can perform a clone operation for a existing length factor group.', async () => {

        let stepAction = 'Click on Save button.Check the user can perform a clone operation for a existing length factor group.';
        let stepData = '';
        let stepExpResult = 'Should be able to perform a clone operation for the existing record from Length Factor Group grid displayed list.';
        let stepActualResult = 'Able to perform a clone operation for the existing record from Length Factor Group grid displayed list.';

        try {
            await localLF.buttonActionsVerify(["Save"],"Click","save Button")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await browser.sleep(5000)
            await localLF.gridFilter("Group Name", testdata_CloneName)
            await localLF.gridDataValidation("Group Name", testdata_CloneName,"equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            await browser.sleep(500)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('Select a record from existing Length Factor Group and click on Clone.', async () => {

        let stepAction = 'Select a record from existing Length Factor Group and click on Clone.';
        let stepData = '';
        let stepExpResult = 'Record should be selected and should show an option to enter the title name of the cloned record.';
        let stepActualResult = 'Record is selected and option is available to enter the title name of the cloned record.';

        try {
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Clone"],"Click","Clone Button")
            await verify.verifyElement(cloneLengthFactorName_Textbox, "cloneLengthFactorName_Textbox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('Enter Length Factor Title name as existing Length Factor Group name  and Click on Save button.', async () => {

        let stepAction = 'Enter Length Factor Title name as existing Length Factor Group name  and Click on Save button.';
        let stepData = '';
        let stepExpResult = '"System should display an error message.""Length Factor Group Name already Exists."""';
        let stepActualResult = 'An error message is displayed as "Length Factor Group Name already Exists."';

        try {
            await action.SetText(cloneLengthFactorName_Textbox, testdata_CloneName, "Clone_Name")
            await localLF.buttonActionsVerify(["Save"],"Click","save Button")
            await localLF.verifyToastNotification("Length Factor Group,Length Factor Group Name already Exists")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.buttonActionsVerify(["Cancel"],"Click","Cancel Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('Select a record from  Length Factor Group displayed grid list and click on Clone.', async () => {

        let stepAction = 'Select a record from  Length Factor Group displayed grid list and click on Clone.';
        let stepData = '';
        let stepExpResult = 'Record should be selected and should show an option to enter the title name of the cloned record.';
        let stepActualResult = 'Record is selected and should have an option to enter the title name of the cloned record.';

        try {
            await localLF.buttonActionsVerify(["Clone"],"Click","Clone Button")
            await verify.verifyElement(cloneLengthFactorName_Textbox, "cloneLengthFactorName_Textbox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('Click on Cancel button on Title box.', async () => {

        let stepAction = 'Click on Cancel button on Title box.';
        let stepData = '';
        let stepExpResult = 'User should be navigated to Length Factor View Grid.';
        let stepActualResult = 'User is navigated to Length Factor View Grid.';

        try {
            await localLF.buttonActionsVerify(["Cancel"],"Click","Cancel Button")
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('Select a record from  Length Factor Group displayed grid list and click on Clone.', async () => {

        let stepAction = 'Select a record from  Length Factor Group displayed grid list and click on Clone.';
        let stepData = '';
        let stepExpResult = 'Record should be selected and should show an option to enter the title name of the cloned record.';
        let stepActualResult = 'Record is selected and have an option to enter the title name of the cloned record.';

        try {
            await localLF.buttonActionsVerify(["Clone"],"Click","Clone Button")
            await verify.verifyElement(cloneLengthFactorName_Textbox, "cloneLengthFactorName_Textbox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('Provide unique name and  click on Save in Title box.', async () => {

        let stepAction = 'Provide unique name and  click on Save in Title box.';
        let stepData = '';
        let stepExpResult = '"Should display message""Successfully cloned  Length Factor Group""."';
        let stepActualResult = 'An success message is displayed as "Successfully cloned Length Factor Group"';

        try {
            id = await localLF.makeAlphaNumericId(5)
            testdata_CloneName = id + "Clone"
            await action.SetText(cloneLengthFactorName_Textbox, testdata_CloneName, "Clone_Name")
            await localLF.buttonActionsVerify(["Save"],"Click","save Button")
            await browser.sleep(500)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('After Confirmation message check newly cloned record  is visible on top of the Length Factor Grid.', async () => {

        let stepAction = 'After Confirmation message check newly cloned record  is visible on top of the Length Factor Grid.';
        let stepData = '';
        let stepExpResult = 'System should display newly cloned records on the top of the grid.';
        let stepActualResult = 'System is displayed newly cloned records on the top of the grid.';

        try {
            await localLF.gridFilter("Group Name", testdata_CloneName)
            await localLF.gridDataValidation("Group Name", testdata_CloneName,"equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('Elect to continue the clone operation. Check the cloned Record has captured all details of the original Length Factor.', async () => {

        let stepAction = 'Elect to continue the clone operation. Check the cloned Record has captured all details of the original Length Factor.';
        let stepData = '';
        let stepExpResult = 'Cloned record should have all details of the original.';
        let stepActualResult = 'Cloned record is have all details of the original.';

        try {
            id = await localLF.makeAlphaNumericId(5)
            testdata_CloneName = id + "Clone"
            testdata_RetrievedChannelName = String(await action.GetText(localLF.latestChannelNames, "latestChannelNames"))
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Clone"],"Click","Clone Button")
            await verify.verifyElement(cloneLengthFactorName_Textbox, "cloneLengthFactorName_Textbox")
            await action.SetText(cloneLengthFactorName_Textbox, testdata_CloneName, "Clone_Name")
            await localLF.buttonActionsVerify(["Save"],"Click","save Button")
            await browser.sleep(500)
            await localLF.verifyToastNotification("Length Factor Group,Successfully cloned Length Factor Group")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_CloneName)
            await localLF.gridDataValidation("Group Name", testdata_CloneName,"equal")
            await verify.verifyTextOfElement(localLF.latestChannelNames, testdata_RetrievedChannelName, "latestChannelNames")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('Click on Clone button without selecting Length Factor in Grid.', async () => {

        let stepAction = 'Click on Clone button without selecting Length Factor in Grid.';
        let stepData = '';
        let stepExpResult = 'Clone button should be disabled.';
        let stepActualResult = 'Clone button is disabled.';

        try {
            await localLF.buttonActionsVerify(["Clone"],"Disabled","Clone Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('Click on Add button.Select Channels,Length Factor Group,Description,Round To.Click on Save.', async () => {

        let stepAction = 'Click on Add button.Select Channels,Length Factor Group,Description,Round To.Click on Save.';
        let stepData = '';
        let stepExpResult = 'New Length Factor Group should be added successfully.';
        let stepActualResult = 'This step will be covered in Add Length Factor Group cases ';

        try {
            id = await localLF.makeAlphaNumericId(5)
            testdata_LFGroupName = id + "Name"
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown,localLF.singleSelectDropdownOptionsLocator,globalTestData.marketName,"Market")
            await localLF.buttonActionsVerify(["Add"],"Click","Add Button")
            await localLF.eraseMultiselectDropdownSelection(localLF.channelMultiSelectDropDown,"Channels")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,globalTestData.multipleChnnels,"Channels")
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic","groupname")),testdata_LFGroupName,"LFGroupName")
            await localLF.buttonActionsVerify(["Save"],"Click","Save Button")
            await verify.verifyProgressSpinnerCircleNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('Check New Length Factor Group is available in the Grid or not.', async () => {

        let stepAction = 'Check New Length Factor Group is available in the Grid or not.';
        let stepData = '';
        let stepExpResult = 'Length Factor Group grid should  display newly created record.';
        let stepActualResult = 'This step will be covered in Add Length Factor Group cases ';

        try {
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName,"equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('Select more than 1 record from Length Factor Group grid. ', async () => {

        let stepAction = 'Select more than 1 record from Length Factor Group grid. ';
        let stepData = '';
        let stepExpResult = 'Clone button should be disabled.';
        let stepActualResult = 'Clone button is disabled.';

        try {
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await action.Click(localLF.secondRecordCheckbox, "secondRecordCheckbox")
            await localLF.buttonActionsVerify(["Clone"],"Disabled","Clone Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('Select a record from  Length Factor Group displayed grid list and click on Clone.', async () => {

        let stepAction = 'Select a record from  Length Factor Group displayed grid list and click on Clone.';
        let stepData = '';
        let stepExpResult = 'Record should be selected and should show an option to enter the title name of the cloned record.';
        let stepActualResult = 'Record is selected and an option to enter the title name of the cloned record.';

        try {
            await action.Click(localLF.secondRecordCheckbox, "secondRecordCheckbox")
            await localLF.buttonActionsVerify(["Clone"],"Click","Clone Button")
            await verify.verifyElement(cloneLengthFactorName_Textbox, "cloneLengthFactorName_Textbox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it('Click on Save button in title box without entering Length Factor Group title.', async () => {

        let stepAction = 'Click on Save button in title box without entering Length Factor Group title.';
        let stepData = '';
        let stepExpResult = '"System should show valid error message.""Length Factor Group should not be blank"""';
        let stepActualResult = 'An error message is displayed as "Length Factor Group should not be blank"';

        try {
            await localLF.buttonActionsVerify(["Save"],"Click","save Button")
            await localLF.verifyToastNotification("Length Factor Group,Length Factor Group should not be blank.")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

});
