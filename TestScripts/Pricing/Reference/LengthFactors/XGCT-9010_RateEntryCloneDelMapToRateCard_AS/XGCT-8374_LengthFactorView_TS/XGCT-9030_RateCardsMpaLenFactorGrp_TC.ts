/*
***********************************************************************************************************
* @Script Name :  XGCT-9030
* @Description :  xG Campaign - Pricing - Verify that the rate card rate entry view rate cards reflect the correct length factor group mapped6-8-2019
* @Page Object Name(s) : LocalLengthFactorsPricing
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib,globalvalues,HomePageFunction
* @Pre-Conditions : 
* @Creation Date : 6-8-2019
* @Author : Adithya K
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../../TestData/globalTestData';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let home = new HomePageFunction();
let localLF = new LocalLengthFactorsPricing();


let endDateRentryView: string = "11/29/2020"
let id: string
let testdata_LFGroupName: string
// let testdata_Description: string
let testdata_RCName: string
let testdata_RCDescription: string
let lengthfactorSpotlengths: Array<string>
let rateEntryViewSpotlengths: Array<string>

//Variables Declaration
let TestCase_ID = 'XGCT-9030'
let TestCase_Title = 'xG Campaign - Pricing - Verify that the rate card rate entry view rate cards reflect the correct length factor group mapped'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - PRICING - VERIFY THAT THE RATE CARD RATE ENTRY VIEW RATE CARDS REFLECT THE CORRECT LENGTH FACTOR GROUP MAPPED', () => {

    // --------------Test Step------------
    it("Navigate to the application http://xgcampaignwebapp.azurewebsites.net/", async () => {

        let stepAction = "Navigate to the application http://xgcampaignwebapp.azurewebsites.net/";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "User should be navigated to the application";
        let stepActualResult = "User is navigated to the application";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Pricing>Local/Network Length Factor Group>Add", async () => {

        let stepAction = "Navigate to Pricing>Local/Network Length Factor Group>Add";
        let stepData = "";
        let stepExpResult = "Check the Length Factor View page should be displayed";
        let stepActualResult = "Length Factor View page is displayed";

        try {
            await home.appclickOnNavigationList(globalTestData.pricingTabName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuLocalLengthFactors);
            await verify.verifyElement(localLF.gridLocator,"LengthFactor Grid");
            await localLF.verifyPageHeader('Length Factors')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Create length factor group with default length factors", async () => {

        let stepAction = "Create length factor group with default length factors";
        let stepData = "";
        let stepExpResult = "Length factor group should be created sucessfully";
        let stepActualResult = "Length factor group is created sucessfully";

        try {
            lengthfactorSpotlengths = []
            let lengthfactordatatoberemoved: Array<String> = ["Spot Length", "Round To"]
            id = await localLF.makeAlphaNumericId(5)
            testdata_LFGroupName = id + "Name"
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown,localLF.singleSelectDropdownOptionsLocator,globalTestData.marketName,"Market")
            await localLF.buttonActionsVerify(["Add"],"Click","Add Button")
            await localLF.eraseMultiselectDropdownSelection(localLF.channelMultiSelectDropDown,"Channels")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,globalTestData.multipleChnnels,"Channels")
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic","groupname")),testdata_LFGroupName,"LFGroupName")
            await localLF.returnspotlengthsArray(localLF.lengthFactorViewspotlengths, lengthfactorSpotlengths, lengthfactordatatoberemoved)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the newly created length factor group is avalible in the grid", async () => {

        let stepAction = "Check the newly created length factor group is avalible in the grid";
        let stepData = "";
        let stepExpResult = "Newly created length factor group should display top of the grid";
        let stepActualResult = "Newly created length factor group is displayed top of the grid";

        try {
            await localLF.buttonActionsVerify(["Save"],"Click","Save Button")
            await localLF.verifyToastNotification("Length Factor Group,Length Factor Group added succesfully")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter("Group Name", testdata_LFGroupName)
            await localLF.gridDataValidation("Group Name", testdata_LFGroupName,"equal")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to  Pricing>Local/Network Add Rate Card", async () => {

        let stepAction = "Navigate to  Pricing>Local/Network Add Rate Card";
        let stepData = "";
        let stepExpResult = "Check the add rate card page is displayed";
        let stepActualResult = "Add rate card page is displayed";

        try {
            await home.appclickOnMenuListLeftPanel(globalTestData.localRateCardleftMenuName);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Create a rate card with newly created length factor group", async () => {

        let stepAction = "Create a rate card with newly created length factor group";
        let stepData = "";
        let stepExpResult = "Rate card should be created sucessfully";
        let stepActualResult = "Rate card is created sucessfully";

        try {
            id = await localLF.makeAlphaNumericId(5)
            testdata_RCName = id + "Name"
            testdata_RCDescription = id + "Desc"
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown,localLF.singleSelectDropdownOptionsLocator,globalTestData.marketName,"Market")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,globalTestData.multipleChnnels,"Channels")
            await localLF.buttonActionsVerify(["Add"],"Click","Add Button")
            await localLF.selectMultiSelectDropDownOptions(element.all(localLF.channelMultiSelectDropDown).get(1),localLF.multiSelectDropdownOptionsLocator,globalTestData.multipleChnnels,"Channels")
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic","ratecardName")),testdata_RCName,"RCName")
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic","ratecardDescription")),testdata_RCDescription,"RCDescription")
            await localLF.selectSingleSelectDropDownOptions(element(by.css(localLF.singleSelectDropdownLocator.replace("dynamic","lengthFactors"))),localLF.singleSelectDropdownOptionsLocator,testdata_LFGroupName,"LFName")
            await action.Click(element(by.css(localLF.singleSelectDropdownLocator.replace("dynamic","sections"))),"Section level")
            await action.Click(element.all(localLF.singleSelectDropdownOptionsLocator).get(1),"Section Level")
            await localLF.buttonActionsVerify(["Save"],"Click","Save Button")
            await browser.sleep(3000)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Pricing>Local/Network Rate Entry View", async () => {

        let stepAction = "Navigate to Pricing>Local/Network Rate Entry View";
        let stepData = "";
        let stepExpResult = "Check the  Rate Entry view page should be displayed";
        let stepActualResult = "Rate Entry view page is displayed";

        try {
            await home.appclickOnMenuListLeftPanel(globalTestData.leftMenuLocalRateEntry);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Market or Network", async () => {

        let stepAction = "Select Market or Network";
        let stepData = "";
        let stepExpResult = "Market or network should be selected";
        let stepActualResult = "Market or network is selected";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown,localLF.singleSelectDropdownOptionsLocator,globalTestData.marketName,"Market")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select channel", async () => {

        let stepAction = "Select channel";
        let stepData = "";
        let stepExpResult = "Channel should be selected";
        let stepActualResult = "Channel is selected";

        try {
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,globalTestData.multipleChnnels,"Channels")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Rates By as Rate Card", async () => {

        let stepAction = "Select Rates By as Rate Card";
        let stepData = "";
        let stepExpResult = "Rates By should be selected";
        let stepActualResult = "Rates By is selected";

        try {
            await localLF.selectSingleSelectDropDownOptions(element(by.css(localLF.singleSelectDropdownLocator.replace("dynamic","ratesby"))),localLF.singleSelectDropdownOptionsLocator,"Rate Card","Rates By")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select rate card which is newly created", async () => {

        let stepAction = "Select rate card which is newly created";
        let stepData = "";
        let stepExpResult = "Newly created rate card should be selected";
        let stepActualResult = "Newly created rate card is selected";

        try {
            await browser.sleep(3000)
            await localLF.selectSingleSelectDropDownOptions(element(by.css(localLF.singleSelectDropdownLocator.replace("dynamic","ratecard"))),localLF.singleSelectDropdownOptionsLocator,testdata_RCName,"RateCard")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select start date,end date  and click on search", async () => {

        let stepAction = "Select start date,end date  and click on search";
        let stepData = "";
        let stepExpResult = "Start date, end date should be selected and should display selected date range releated programs in the list";
        let stepActualResult = "Start date, end date are selected and displayed selected date range releated programs in the list";

        try {
            await localLF.selectDateFromCalender("xgc-rate-entry-enddate",20,"Dec","2021")
            await localLF.buttonActionsVerify(["Search"],"Click","Search Button")
            await verify.verifyProgressBarNotPresent()
            await localLF.gridDataValidation("Channel",globalTestData.channelsToBeVerifiedInGrid,"Contains")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the newly created Length factor group related spot lengths are visiable in rate entry view grid", async () => {

        let stepAction = "Check the newly created Length factor group related spot lengths are visiable in rate entry view grid";
        let stepData = "";
        let stepExpResult = " Newly created length factor group related spot lengths should display  in rate entry view grid";
        let stepActualResult = " Newly created length factor group related spot lengths is displayed  in rate entry view grid";

        try {
            let rateEntryViewdatatoberemoved: Array<String> = ["Dayparts", "Days", "Hiatus Dates", "Tags", "Genres"]
            rateEntryViewSpotlengths = []
            await action.Click(localLF.columnPicker, "columnPicker")
            await localLF.returnspotlengthsArray(localLF.rateEntryViewspotlengths, rateEntryViewSpotlengths, rateEntryViewdatatoberemoved)
            for (let i = 0; i < lengthfactorSpotlengths.length; i++) {
                if (rateEntryViewSpotlengths.includes(lengthfactorSpotlengths[i])) {
                    action.ReportSubStep("StepName", lengthfactorSpotlengths[i] + " Length Factor Matched ", "Pass");
                }
                else {
                    action.ReportSubStep("StepName", lengthfactorSpotlengths[i] + " Length Factor not Matched ", "Fail");
                }
            }
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
