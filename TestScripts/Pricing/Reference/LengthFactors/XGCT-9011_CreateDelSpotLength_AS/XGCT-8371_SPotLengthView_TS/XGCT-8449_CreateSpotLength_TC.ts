/*
***********************************************************************************************************
* @Script Name :  XGCT-8449
* @Description :  xG Campaign - Spot length - Add28-8-2019
* @Page Object Name(s) : LocalLengthFactorsPricing
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib,globalvalues,HomePageFunction
* @Pre-Conditions : 
* @Creation Date : 16-8-2019
* @Author : Adithya K
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements

import { Reporter } from "../../../../../../Utility/htmlResult";
import { VerifyLib } from "../../../../../../Libs/GeneralLibs/VerificationLib";
import { ActionLib } from "../../../../../../Libs/GeneralLibs/ActionLib";
import { globalvalues } from "../../../../../../Utility/globalvalue";
import { HomePageFunction } from "../../../../../../Pages/Home/HomePage";
import { LocalLengthFactorsPricing } from "../../../../../../Pages/Pricing/Reference/LocalLengthFactors";
import { browser, element, by } from "protractor";
import { globalTestData } from "../../../../../../TestData/globalTestData";


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let home = new HomePageFunction();
let localLF = new LocalLengthFactorsPricing();

let id: string
let testdata_SpotLength: number
let testdata_SpotLengthFactor: string
let spotLengthColumnName: string = "Spot Length"
let channelsColumnName: string = "Channel"

//Variables Declaration
let TestCase_ID = 'XGCT-8449'
let TestCase_Title = 'xG Campaign - Spot length - Add'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - SPOT LENGTH - ADD', () => {

    // --------------Test Step------------
    it("Navigate to the application http://xgcampaignwebapp.azurewebsites.net/", async () => {

        let stepAction = "Navigate to the application http://xgcampaignwebapp.azurewebsites.net/";
        let stepData = "";
        let stepExpResult = "User should be navigated to the application";
        let stepActualResult = "User should be navigated to the application";

        try {
            await browser.waitForAngularEnabled(false);
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to  Pricing>Local Spot Lengths> Add", async () => {

        let stepAction = "Navigate to  Pricing>Local Spot Lengths> Add";
        let stepData = "";
        let stepExpResult = "Check the add spot length view page is displayed";
        let stepActualResult = "Check the add spot length view page is displayed";

        try {
            await home.appclickOnNavigationList("Pricing");
            await home.appclickOnMenuListLeftPanel("Reference");
            await home.appclickOnMenuListLeftPanel("Local Spot Lengths");
            await localLF.verifyPageHeader('Spot Length View')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Market and Channel", async () => {

        let stepAction = "Select Market and Channel";
        let stepData = "'Market as ''Pittsburgh'' Channel as ''WPGH'''";
        let stepExpResult = "'User able to select Market as ''Pittsburgh'' and channel as ''WPGH''..'";
        let stepActualResult = "'User able to select Market as ''Pittsburgh'' and channel as ''WPGH''..'";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            // await localLF.eraseMultiselectDropdownSelection(localLF.channelMultiSelectDropDown, "Channels")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, globalTestData.singleChannel, "Channels")
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            await localLF.verifyPopUpHeader('Add Spot Length')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter spot lengths in Spot Length (seconds) field", async () => {

        let stepAction = "Enter spot lengths in Spot Length (seconds) field";
        let stepData = "";
        let stepExpResult = "Spot Length should be taken";
        let stepActualResult = "Spot Length should be taken";

        try {
            testdata_SpotLength = await localLF.makeNumericId(4)
            testdata_SpotLengthFactor = "0." + testdata_SpotLength
            await action.SetText(element.all(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length"))).get(0), String(testdata_SpotLength), "SpotLength")
            await verify.verifyElementAttribute(element.all(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length"))).get(0),"value", String(testdata_SpotLength), "SpotLength")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter spot length factor in Spot Length Factor field", async () => {

        let stepAction = "Enter spot length factor in Spot Length Factor field";
        let stepData = "";
        let stepExpResult = "Spot Length Factor should be taken ";
        let stepActualResult = "Spot Length Factor should be taken ";

        try {
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length-factor")), (testdata_SpotLengthFactor), "SpotLengthFactor")
            await verify.verifyElementAttribute(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length-factor")),"value", (testdata_SpotLengthFactor), "SpotLengthFactor")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on save ", async () => {

        let stepAction = "Click on save ";
        let stepData = "";
        let stepExpResult = "Spot length should be created sucessfully ";
        let stepActualResult = "Spot length should be created sucessfully ";

        try {
            await localLF.buttonActionsVerify(["Save"], "Click", "Save Button")
            await localLF.verifyToastNotification("Add Spot Length,Spot Length Added Successfully.")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the newly create spot length is avalible in the spot length view grid", async () => {

        let stepAction = "Check the newly create spot length is avalible in the spot length view grid";
        let stepData = "";
        let stepExpResult = "Newly created spot length should be avalible in the grid";
        let stepActualResult = "Newly created spot length should be avalible in the grid";

        try {
            await browser.sleep(2000)
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            // await localLF.eraseMultiselectDropdownSelection(localLF.channelMultiSelectDropDown, "Channels")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, globalTestData.singleChannel, "Channels")
            await verify.verifyProgressBarNotPresent()
            await localLF.gridFilter(spotLengthColumnName, String(testdata_SpotLength))
            await localLF.gridDataValidation(spotLengthColumnName, String(testdata_SpotLength), "equal")           
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to  Pricing>Local Spot Lengths> Add", async () => {

        let stepAction = "Navigate to  Pricing>Local Spot Lengths> Add";
        let stepData = "";
        let stepExpResult = "Check the add spot length view page is displayed";
        let stepActualResult = "Check the add spot length view page is displayed";

        try {
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            await localLF.verifyPopUpHeader('Add Spot Length')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on cancel button in add spot length window", async () => {

        let stepAction = "Click on cancel button in add spot length window";
        let stepData = "";
        let stepExpResult = "User should be navigated to the spot length view";
        let stepActualResult = "User should be navigated to the spot length view";

        try {
            await action.Click(localLF.cancelBtn_SpotLengths,"cancelBtn_SpotLengths")
            await localLF.verifyPageHeader('Spot Length View')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to  Pricing>Local Spot Lengths> Add", async () => {

        let stepAction = "Navigate to  Pricing>Local Spot Lengths> Add";
        let stepData = "";
        let stepExpResult = "Check the add spot length view page is displayed";
        let stepActualResult = "Check the add spot length view page is displayed";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            // await localLF.eraseMultiselectDropdownSelection(localLF.channelMultiSelectDropDown, "Channels")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, globalTestData.multipleChnnels, "Channels")
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            await localLF.verifyPopUpHeader('Add Spot Length')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Create a spot length with more than one channel", async () => {

        let stepAction = "Create a spot length with more than one channel";
        let stepData = "";
        let stepExpResult = "Spot length should be created sucessfully ";
        let stepActualResult = "Spot length should be created sucessfully ";

        try {
            testdata_SpotLength = await localLF.makeNumericId(4)
            testdata_SpotLengthFactor = "0." + testdata_SpotLength
            await action.SetText(element.all(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length"))).get(0), String(testdata_SpotLength), "SpotLength")
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length-factor")), (testdata_SpotLengthFactor), "SpotLengthFactor")
            await localLF.buttonActionsVerify(["Save"], "Click", "Save Button")
            await localLF.verifyToastNotification("Add Spot Length,Spot Length Added Successfully.")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await verify.verifyProgressBarNotPresent()
            await browser.sleep(5000)            
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the newly create spot length is showing in both channel's", async () => {

        let stepAction = "Check the newly create spot length is showing in both channel's";
        let stepData = "";
        let stepExpResult = "Newly created spot length should show in both channel's";
        let stepActualResult = "Newly created spot length should show in both channel's";

        try {
            await browser.sleep(2000)
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            // await localLF.eraseMultiselectDropdownSelection(localLF.channelMultiSelectDropDown, "Channels")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, globalTestData.multipleChnnels, "Channels")
            await verify.verifyProgressBarNotPresent()
            await localLF.gridFilter(spotLengthColumnName, String(testdata_SpotLength))
            await localLF.gridDataValidation(spotLengthColumnName, String(testdata_SpotLength), "equal")           
            await localLF.gridDataValidation(channelsColumnName, globalTestData.channelsToBeVerifiedInGrid, "Contains")           
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to  Pricing>Network Spot Length View > Add", async () => {

        let stepAction = "Navigate to  Pricing>Network Spot Length View > Add";
        let stepData = "";
        let stepExpResult = "Check the add spot length view page is displayed";
        let stepActualResult = "Check the add spot length view page is displayed";

        try {
            await home.appclickOnMenuListLeftPanel("Network Spot Lengths");
            await localLF.verifyPageHeader('Spot Length View')
            await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, String(globalTestData.networkName[0]), "Network")
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.verifyPopUpHeader('Add Spot Length')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter spot lengths in Spot Length (seconds) field", async () => {

        let stepAction = "Enter spot lengths in Spot Length (seconds) field";
        let stepData = "";
        let stepExpResult = "Spot Length should be taken";
        let stepActualResult = "Spot Length should be taken";

        try {
            testdata_SpotLength = await localLF.makeNumericId(4)
            testdata_SpotLengthFactor = "0." + testdata_SpotLength
            await action.SetText(element.all(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length"))).get(0), String(testdata_SpotLength), "SpotLength")
            await verify.verifyElementAttribute(element.all(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length"))).get(0),"value", String(testdata_SpotLength), "SpotLength")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter spot length factor in Spot Length Factor field", async () => {

        let stepAction = "Enter spot length factor in Spot Length Factor field";
        let stepData = "";
        let stepExpResult = "Spot Length Factor should be taken ";
        let stepActualResult = "Spot Length Factor should be taken ";

        try {
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length-factor")), (testdata_SpotLengthFactor), "SpotLengthFactor")
            await verify.verifyElementAttribute(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length-factor")),"value", (testdata_SpotLengthFactor), "SpotLengthFactor")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on save ", async () => {

        let stepAction = "Click on save ";
        let stepData = "";
        let stepExpResult = "Spot length should be created sucessfully ";
        let stepActualResult = "Spot length should be created sucessfully ";

        try {
            await localLF.buttonActionsVerify(["Save"], "Click", "Save Button")
            await localLF.verifyToastNotification("Add Spot Length,Spot Length Added Successfully.")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the newly create spot length is avalible in the spot length view grid", async () => {

        let stepAction = "Check the newly create spot length is avalible in the spot length view grid";
        let stepData = "";
        let stepExpResult = "Newly created spot length should be avalible in the grid";
        let stepActualResult = "Newly created spot length should be avalible in the grid";

        try {
            await browser.sleep(2000)
            await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, String(globalTestData.networkName[0]), "Network")
            await verify.verifyProgressBarNotPresent()
            await localLF.gridFilter(spotLengthColumnName, String(testdata_SpotLength))
            await localLF.gridDataValidation(spotLengthColumnName, String(testdata_SpotLength), "equal")           
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to  Pricing>Network Spot Length View > Add", async () => {

        let stepAction = "Navigate to  Pricing>Network Spot Length View > Add";
        let stepData = "";
        let stepExpResult = "Check the add spot length view page is displayed";
        let stepActualResult = "Check the add spot length view page is displayed";

        try {
            await localLF.buttonActionsVerify(["Add"], "Click", "Add Button")
            await localLF.verifyPopUpHeader('Add Spot Length')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on cancel button in add spot length window", async () => {

        let stepAction = "Click on cancel button in add spot length window";
        let stepData = "";
        let stepExpResult = "User should be navigated to the spot length view";
        let stepActualResult = "User should be navigated to the spot length view";

        try {
            await action.Click(localLF.cancelBtn_SpotLengths,"cancelBtn_SpotLengths")
            await localLF.verifyPageHeader('Spot Length View')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
