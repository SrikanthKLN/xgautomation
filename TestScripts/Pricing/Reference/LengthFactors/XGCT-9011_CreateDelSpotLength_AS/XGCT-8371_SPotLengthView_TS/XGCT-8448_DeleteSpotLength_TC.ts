/*
***********************************************************************************************************
* @Script Name :  XGCT-8448
* @Description :  xG Campaign - Spot length - Delete6-9-2019
* @Page Object Name(s) : LocalLengthFactorsPricing
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib,globalvalues,HomePageFunction
* @Pre-Conditions : 
* @Creation Date : 6-9-2019
* @Author : Adithya K
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { Reporter } from "../../../../../../Utility/htmlResult";
import { VerifyLib } from "../../../../../../Libs/GeneralLibs/VerificationLib";
import { ActionLib } from "../../../../../../Libs/GeneralLibs/ActionLib";
import { globalvalues } from "../../../../../../Utility/globalvalue";
import { HomePageFunction } from "../../../../../../Pages/Home/HomePage";
import { LocalLengthFactorsPricing } from "../../../../../../Pages/Pricing/Reference/LocalLengthFactors";
import { browser, element, by } from "protractor";
import { globalTestData } from "../../../../../../TestData/globalTestData";
import { SpotLengths} from "../../../../../../Pages/Pricing/Reference/SpotLengths";


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let home = new HomePageFunction();
let localLF = new LocalLengthFactorsPricing();
let spotLength = new SpotLengths();

let testdata_SpotLength: number
let spotLengthColumnName: string = "Spot Length"
let spotLengthTypeColumnName: string = "Spot Length Type"
let spotLengthType: string = "Custom"
let firstSpotLength
let secondSpotLength

//Variables Declaration
let TestCase_ID = 'XGCT-8448'
let TestCase_Title = 'xG Campaign - Spot length - Delete'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - SPOT LENGTH - DELETE', () => {

    // --------------Test Step------------
    it("Navigate to the application http://xgcampaignwebapp.azurewebsites.net/", async () => {

        let stepAction = "Navigate to the application http://xgcampaignwebapp.azurewebsites.net/";
        let stepData = "";
        let stepExpResult = "User should be navigated to the application";
        let stepActualResult = "User should be navigated to the application";

        try {
            await browser.waitForAngularEnabled(false);
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Spot Length View'", async () => {

        let stepAction = "Spot Length View'";
        let stepData = "";
        let stepExpResult = "Check the Spot Length View page is displayed";
        let stepActualResult = "Check the Spot Length View page is displayed";

        try {
            await home.appclickOnNavigationList("Pricing");
            await home.appclickOnMenuListLeftPanel("Reference");
            await home.appclickOnMenuListLeftPanel("Local Spot Lengths");
            await localLF.verifyPageHeader('Spot Length View')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Market and Channel", async () => {

        let stepAction = "Select Market and Channel";
        let stepData = "'Market as ''Pittsburgh'' Channel as ''WPGH'''";
        let stepExpResult = "'User able to select Market as ''Pittsburgh'' and channel as ''WPGH''.User should be able to see Spot length and Spot length factor values in the grid based on the selected market.'";
        let stepActualResult = "'User able to select Market as ''Pittsburgh'' and channel as ''WPGH''.User should be able to see Spot length and Spot length factor values in the grid based on the selected market.'";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.marketsingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await localLF.selectMultiSelectDropDownOptions(localLF.channelMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, globalTestData.singleChannel, "Channels")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a existing record from the spot length factor view grid", async () => {

        let stepAction = "Select a existing record from the spot length factor view grid";
        let stepData = "";
        let stepExpResult = "Record should be selected and delete button should be enabled on the spot length view grid ";
        let stepActualResult = "Record should be selected and delete button should be enabled on the spot length view grid ";

        try {
            await localLF.gridFilter(spotLengthTypeColumnName, spotLengthType)
            await browser.sleep(500)
            let elementStatus = await verify.verifyElementVisible(spotLength.noDataFound);
            if (elementStatus) {
                await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
                await console.log("TEst 1")
                testdata_SpotLength = await spotLength.addSpotLength()
                await localLF.gridFilter(spotLengthColumnName, String(testdata_SpotLength))
                await localLF.gridDataValidation(spotLengthColumnName, String(testdata_SpotLength), "equal")
                await action.Click(spotLength.latestRecordCheckbox, "latestRecordCheckbox")
            }
            else {
                await action.Click(spotLength.latestRecordCheckbox, "latestRecordCheckbox")
                testdata_SpotLength = Number(await action.GetText(spotLength.latestSpotLength, "latestSpotLength"))
            }
            await localLF.buttonActionsVerify(["Delete"], "Enabled", "Delete Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the user has an option to delete an existing spot length", async () => {

        let stepAction = "Check the user has an option to delete an existing spot length";
        let stepData = "";
        let stepExpResult = "User should have option to delete recrods for existing spot length";
        let stepActualResult = "User should have option to delete recrods for existing spot length";

        try {
            await localLF.buttonActionsVerify(["Delete"], "Displayed", "Delete Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Click on delete button", async () => {

        let stepAction = "Click on delete button";
        let stepData = "";
        let stepExpResult = "System should show confirmation pop-up window with delete and cancel buttons";
        let stepActualResult = "System should show confirmation pop-up window with delete and cancel buttons";

        try {
            await localLF.buttonActionsVerify(["Delete"], "Click", "Delete Button")
            await localLF.buttonActionsVerify(["confirmation"], "Displayed", "Yes Button")
            await localLF.buttonActionsVerify(["xgc-cancel-delete"], "Displayed", "No Button")
            await verify.verifyTextOfElement(spotLength.deleteConfirmationMsg, spotLength.deleteConfirmationText, "deleteConfirmationText")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on delete in confirmation pop-up window", async () => {

        let stepAction = "Click on delete in confirmation pop-up window";
        let stepData = "";
        let stepExpResult = "Record should be deleted and system should show sucessfully delete confirmation message ";
        let stepActualResult = "Record should be deleted and system should show sucessfully delete confirmation message ";

        try {
            await localLF.buttonActionsVerify(["confirmation"], "Click", "Yes Button")
            await localLF.verifyToastNotification(spotLength.deletedNotificationMsg)
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter(spotLength.spotLengthColumnName, String(testdata_SpotLength))
            await spotLength.verifyNodataFoundInSpotLengthGrid()
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select more than one record from spot length view grid list and click on delete", async () => {

        let stepAction = "Select more than one record from spot length view grid list and click on delete";
        let stepData = "";
        let stepExpResult = "Records should be selected and system  should show confirmation pop-up window with delete and cancel buttons";
        let stepActualResult = "Records should be selected and system  should show confirmation pop-up window with delete and cancel buttons";

        try {
            await localLF.gridFilter(spotLengthTypeColumnName, spotLengthType)
            await browser.sleep(500)
            let elementStatus = await verify.verifyElementVisible(spotLength.noDataFound);
            if (elementStatus) {
                await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
                await console.log("TEst 1")
                await spotLength.addSpotLength()
                await spotLength.addSpotLength()
                await localLF.gridFilter(spotLengthTypeColumnName, spotLengthType)
                await action.Click(spotLength.latestRecordCheckbox, "latestRecordCheckbox")
                firstSpotLength = await action.GetText(spotLength.latestSpotLength, "latestSpotLength")
                await action.Click(spotLength.secondRecordCheckbox, "secondRecordCheckbox")
                secondSpotLength = await action.GetText(spotLength.secondSpotLength, "secondSpotLength")

            }
            else {
                let spotLengthCount = await element.all(spotLength.customSpotLength).count();
                if (spotLengthCount > 1) {
                    await console.log("TEst 2")
                    await action.Click(spotLength.latestRecordCheckbox, "latestRecordCheckbox")
                    firstSpotLength = await action.GetText(spotLength.latestSpotLength, "latestSpotLength")
                    await action.Click(spotLength.secondRecordCheckbox, "secondRecordCheckbox")
                    secondSpotLength = await action.GetText(spotLength.secondSpotLength, "secondSpotLength")

                }
                else if (spotLengthCount == 1) {
                    await console.log("TEst 3")
                    await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
                    await spotLength.addSpotLength()
                    await localLF.gridFilter(spotLengthTypeColumnName, spotLengthType)
                    await action.Click(spotLength.latestRecordCheckbox, "latestRecordCheckbox")
                    firstSpotLength = await action.GetText(spotLength.latestSpotLength, "latestSpotLength")
                    await action.Click(spotLength.secondRecordCheckbox, "secondRecordCheckbox")
                    secondSpotLength = await action.GetText(spotLength.secondSpotLength, "secondSpotLength")

                }
            }
            await localLF.buttonActionsVerify(["Delete"], "Click", "Delete Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on delete in confirmation pop-up window", async () => {

        let stepAction = "Click on delete in confirmation pop-up window";
        let stepData = "";
        let stepExpResult = "Record should be deleted and system should show sucessfully delete confirmation message ";
        let stepActualResult = "Record should be deleted and system should show sucessfully delete confirmation message ";

        try {
            await localLF.buttonActionsVerify(["confirmation"], "Click", "Yes Button")
            await localLF.verifyToastNotification(spotLength.deletedNotificationMsg)
            await verify.verifyProgressSpinnerCircleNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click the deleted records is avalible in the spot length view grid", async () => {

        let stepAction = "Click the deleted records is avalible in the spot length view grid";
        let stepData = "";
        let stepExpResult = "Deleted record should not be avaliable in the spot length view grid";
        let stepActualResult = "Deleted record should not be avaliable in the spot length view grid";

        try {
            await localLF.gridFilter(spotLength.spotLengthColumnName, String(firstSpotLength))
            await spotLength.verifyNodataFoundInSpotLengthGrid()
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            await localLF.gridFilter(spotLength.spotLengthColumnName, String(secondSpotLength))
            await spotLength.verifyNodataFoundInSpotLengthGrid()
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on cancel in confirmation pop-up window", async () => {

        let stepAction = "Click on cancel in confirmation pop-up window";
        let stepData = "";
        let stepExpResult = "User should navigate to spot length view grid and selected Record should not be deleted ";
        let stepActualResult = "User should navigate to spot length view grid and selected Record should not be deleted ";

        try {
            await localLF.gridFilter(spotLengthTypeColumnName, spotLengthType)
            await browser.sleep(500)
            let elementStatus = await verify.verifyElementVisible(spotLength.noDataFound);
            if (elementStatus) {
                await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
                await console.log("TEst 1")
                testdata_SpotLength = await spotLength.addSpotLength()
                await localLF.gridFilter(spotLengthColumnName, String(testdata_SpotLength))
                await localLF.gridDataValidation(spotLengthColumnName, String(testdata_SpotLength), "equal")
                await action.Click(spotLength.latestRecordCheckbox, "latestRecordCheckbox")
            }
            else {
                await action.Click(spotLength.latestRecordCheckbox, "latestRecordCheckbox")
                testdata_SpotLength = Number(await action.GetText(spotLength.latestSpotLength, "latestSpotLength"))
            }
            await localLF.buttonActionsVerify(["Delete"], "Click", "Delete Button")
            await localLF.buttonActionsVerify(["xgc-cancel-delete"], "Click", "No Button")
            await localLF.gridFilter(spotLength.spotLengthColumnName, String(testdata_SpotLength))
            await localLF.gridDataValidation(spotLengthColumnName, String(testdata_SpotLength), "equal")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on delete button in spot length view grid", async () => {

        let stepAction = "Click on delete button in spot length view grid";
        let stepData = "";
        let stepExpResult = "Default delete button should not perform  any particular action ";
        let stepActualResult = "Default delete button should not perform  any particular action ";

        try {
            await action.Click(spotLength.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Delete"], "Disabled", "Delete Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Spot Length View'", async () => {

        let stepAction = "Spot Length View'";
        let stepData = "";
        let stepExpResult = "Check the Spot Length View page is displayed";
        let stepActualResult = "Check the Spot Length View page is displayed";

        try {
            await home.appclickOnMenuListLeftPanel("Network Spot Lengths");
            await localLF.verifyPageHeader('Spot Length View')
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select network", async () => {

        let stepAction = "Select network";
        let stepData = "FOX";
        let stepExpResult = "Network  should be selected and should display selected network releated spot lengths in the view grid";
        let stepActualResult = "Network  should be selected and should display selected network releated spot lengths in the view grid";

        try {
            await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, String(globalTestData.networkName[0]), "Network")
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a existing record from the spot length factor view grid", async () => {

        let stepAction = "Select a existing record from the spot length factor view grid";
        let stepData = "";
        let stepExpResult = "Record should be selected and delete button should be enabled on the spot length view grid ";
        let stepActualResult = "Record should be selected and delete button should be enabled on the spot length view grid ";

        try {
            await localLF.gridFilter(spotLengthTypeColumnName, spotLengthType)
            await browser.sleep(500)
            let elementStatus = await verify.verifyElementVisible(spotLength.noDataFound);
            if (elementStatus) {
                await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
                await console.log("TEst 1")
                testdata_SpotLength = await spotLength.addSpotLength()
                await browser.sleep(1000)
                await verify.verifyProgressBarNotPresent()
                await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, String(globalTestData.networkName[0]), "Network")
                await verify.verifyProgressBarNotPresent()
                await localLF.gridFilter(spotLengthColumnName, String(testdata_SpotLength))
                await localLF.gridDataValidation(spotLengthColumnName, String(testdata_SpotLength), "equal")
                await action.Click(spotLength.latestRecordCheckbox, "latestRecordCheckbox")
            }
            else {
                await action.Click(spotLength.latestRecordCheckbox, "latestRecordCheckbox")
                testdata_SpotLength = Number(await action.GetText(spotLength.latestSpotLength, "latestSpotLength"))
            }
            await localLF.buttonActionsVerify(["Delete"], "Enabled", "Delete Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the user has an option to delete an existing spot length", async () => {

        let stepAction = "Check the user has an option to delete an existing spot length";
        let stepData = "";
        let stepExpResult = "User should have option to delete recrods for existing spot length";
        let stepActualResult = "User should have option to delete recrods for existing spot length";

        try {
            await localLF.buttonActionsVerify(["Delete"], "Displayed", "Delete Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Click on delete button", async () => {

        let stepAction = "Click on delete button";
        let stepData = "";
        let stepExpResult = "System should show confirmation pop-up window with delete and cancel buttons";
        let stepActualResult = "System should show confirmation pop-up window with delete and cancel buttons";

        try {
            await localLF.buttonActionsVerify(["Delete"], "Click", "Delete Button")
            await localLF.buttonActionsVerify(["confirmation"], "Displayed", "Yes Button")
            await localLF.buttonActionsVerify(["xgc-cancel-delete"], "Displayed", "No Button")
            await verify.verifyTextOfElement(spotLength.deleteConfirmationMsg, spotLength.deleteConfirmationText, "deleteConfirmationText")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on delete in confirmation pop-up window", async () => {

        let stepAction = "Click on delete in confirmation pop-up window";
        let stepData = "";
        let stepExpResult = "Record should be deleted and system should show sucessfully delete confirmation message ";
        let stepActualResult = "Record should be deleted and system should show sucessfully delete confirmation message ";

        try {
            await localLF.buttonActionsVerify(["confirmation"], "Click", "Yes Button")
            await localLF.verifyToastNotification(spotLength.deletedNotificationMsg)
            await verify.verifyProgressSpinnerCircleNotPresent()
            await localLF.gridFilter(spotLength.spotLengthColumnName, String(testdata_SpotLength))
            await spotLength.verifyNodataFoundInSpotLengthGrid()
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select more than one record from spot length view grid list and click on delete", async () => {

        let stepAction = "Select more than one record from spot length view grid list and click on delete";
        let stepData = "";
        let stepExpResult = "Records should be selected and system  should show confirmation pop-up window with delete and cancel buttons";
        let stepActualResult = "Records should be selected and system  should show confirmation pop-up window with delete and cancel buttons";

        try {
            await localLF.gridFilter(spotLengthTypeColumnName, spotLengthType)
            await browser.sleep(500)
            let elementStatus = await verify.verifyElementVisible(spotLength.noDataFound);
            if (elementStatus) {
                await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
                await console.log("TEst 1")
                await spotLength.addSpotLength()
                await browser.sleep(1000)
                await verify.verifyProgressBarNotPresent()
                await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, String(globalTestData.networkName[0]), "Network")
                await verify.verifyProgressBarNotPresent()
                await spotLength.addSpotLength()
                await browser.sleep(1000)
                await verify.verifyProgressBarNotPresent()
                await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, String(globalTestData.networkName[0]), "Network")
                await verify.verifyProgressBarNotPresent()
                await localLF.gridFilter(spotLengthTypeColumnName, spotLengthType)
                await action.Click(spotLength.latestRecordCheckbox, "latestRecordCheckbox")
                firstSpotLength = await action.GetText(spotLength.latestSpotLength, "latestSpotLength")
                await action.Click(spotLength.secondRecordCheckbox, "secondRecordCheckbox")
                secondSpotLength = await action.GetText(spotLength.secondSpotLength, "secondSpotLength")

            }
            else {
                let spotLengthCount = await element.all(spotLength.customSpotLength).count();
                if (spotLengthCount > 1) {
                    await console.log("TEst 2")
                    await action.Click(spotLength.latestRecordCheckbox, "latestRecordCheckbox")
                    firstSpotLength = await action.GetText(spotLength.latestSpotLength, "latestSpotLength")
                    await action.Click(spotLength.secondRecordCheckbox, "secondRecordCheckbox")
                    secondSpotLength = await action.GetText(spotLength.secondSpotLength, "secondSpotLength")

                }
                else if (spotLengthCount == 1) {
                    await console.log("TEst 3")
                    await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
                    await spotLength.addSpotLength()
                    await browser.sleep(1000)
                    await verify.verifyProgressBarNotPresent()
                    await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, String(globalTestData.networkName[0]), "Network")
                    await verify.verifyProgressBarNotPresent()
                    await localLF.gridFilter(spotLengthTypeColumnName, spotLengthType)
                    await action.Click(spotLength.latestRecordCheckbox, "latestRecordCheckbox")
                    firstSpotLength = await action.GetText(spotLength.latestSpotLength, "latestSpotLength")
                    await action.Click(spotLength.secondRecordCheckbox, "secondRecordCheckbox")
                    secondSpotLength = await action.GetText(spotLength.secondSpotLength, "secondSpotLength")
                }
            }
            await localLF.buttonActionsVerify(["Delete"], "Click", "Delete Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on delete in confirmation pop-up window", async () => {

        let stepAction = "Click on delete in confirmation pop-up window";
        let stepData = "";
        let stepExpResult = "Record should be deleted and system should show sucessfully delete confirmation message ";
        let stepActualResult = "Record should be deleted and system should show sucessfully delete confirmation message ";

        try {
            await localLF.buttonActionsVerify(["confirmation"], "Click", "Yes Button")
            await localLF.verifyToastNotification(spotLength.deletedNotificationMsg)
            await verify.verifyProgressSpinnerCircleNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click the deleted records is avalible in the spot length view grid", async () => {

        let stepAction = "Click the deleted records is avalible in the spot length view grid";
        let stepData = "";
        let stepExpResult = "Deleted record should not be avaliable in the spot length view grid";
        let stepActualResult = "Deleted record should not be avaliable in the spot length view grid";

        try {
            await localLF.gridFilter(spotLength.spotLengthColumnName, String(firstSpotLength))
            await spotLength.verifyNodataFoundInSpotLengthGrid()
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            await localLF.gridFilter(spotLength.spotLengthColumnName, String(secondSpotLength))
            await spotLength.verifyNodataFoundInSpotLengthGrid()
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on cancel in confirmation pop-up window", async () => {

        let stepAction = "Click on cancel in confirmation pop-up window";
        let stepData = "";
        let stepExpResult = "User should navigate to spot length view grid and selected Record should not be deleted ";
        let stepActualResult = "User should navigate to spot length view grid and selected Record should not be deleted ";

        try {
            await localLF.gridFilter(spotLengthTypeColumnName, spotLengthType)
            await browser.sleep(500)
            let elementStatus = await verify.verifyElementVisible(spotLength.noDataFound);
            if (elementStatus) {
                await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
                await console.log("TEst 1")
                testdata_SpotLength = await spotLength.addSpotLength()
                await browser.sleep(1000)
                await verify.verifyProgressBarNotPresent()
                await localLF.selectSingleSelectDropDownOptions(localLF.networksingleSelectDropDown, localLF.singleSelectDropdownOptionsLocator, String(globalTestData.networkName[0]), "Network")
                await verify.verifyProgressBarNotPresent()
                await localLF.gridFilter(spotLengthColumnName, String(testdata_SpotLength))
                await localLF.gridDataValidation(spotLengthColumnName, String(testdata_SpotLength), "equal")
                await action.Click(spotLength.latestRecordCheckbox, "latestRecordCheckbox")
            }
            else {
                await action.Click(spotLength.latestRecordCheckbox, "latestRecordCheckbox")
                testdata_SpotLength = Number(await action.GetText(spotLength.latestSpotLength, "latestSpotLength"))
            }
            await localLF.buttonActionsVerify(["Delete"], "Click", "Delete Button")
            await localLF.buttonActionsVerify(["xgc-cancel-delete"], "Click", "No Button")
            await localLF.gridFilter(spotLength.spotLengthColumnName, String(testdata_SpotLength))
            await localLF.gridDataValidation(spotLengthColumnName, String(testdata_SpotLength), "equal")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on delete button in spot length view grid", async () => {

        let stepAction = "Click on delete button in spot length view grid";
        let stepData = "";
        let stepExpResult = "Default delete button should not perform  any particular action ";
        let stepActualResult = "Default delete button should not perform  any particular action ";

        try {
            await action.Click(spotLength.latestRecordCheckbox, "latestRecordCheckbox")
            await localLF.buttonActionsVerify(["Delete"], "Disabled", "Delete Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
