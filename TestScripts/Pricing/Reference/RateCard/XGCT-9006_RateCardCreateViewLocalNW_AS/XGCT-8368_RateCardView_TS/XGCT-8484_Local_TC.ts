/*
***********************************************************************************************************
* @Script Name :  XGCT-8484
* @Description :  xG Campaign - Pricing - Rate Card View (Local)6-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 6-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { LocalProgramming } from '../../../../../../Pages/Inventory/LocalProgramming';
import { AppCommonFunctions } from '../../../../../../Libs/ApplicationLibs/CommAppLib';
import { RateCardPage } from '../../../../../../Pages/Pricing/Reference/RateCard';
import { globalTestData } from '../../../../../../TestData/globalTestData';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let globalFunction = new globalvalues();
let homePage = new HomePageFunction();
let localProgramming = new LocalProgramming();
let commonLib = new AppCommonFunctions();
let rateCardPage = new RateCardPage();
let rateCardName;
let cloneRateCardName;
//Variables Declaration
let TestCase_ID = 'XGCT-8484'
let TestCase_Title = 'xG Campaign - Pricing - Rate Card View (Local)'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - PRICING - RATE CARD VIEW (LOCAL)', () => {

    // --------------Test Step------------
    it("Click on the Add new rate card button'", async () => {

        let stepAction = "Click on the Add new rate card button'";
        let stepData = "";
        let stepExpResult = "New window for creating a rate card should be opened";
        let stepActualResult = "New window for creating a rate card is opened";
        try {
            await globalFunction.LaunchStoryBook();
            await homePage.appclickOnNavigationList(globalTestData.pricingTabName);
            await homePage.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await homePage.appclickOnMenuListLeftPanel(globalTestData.localRateCardleftMenuName);
            await commonLib.selectDropDownValue("Network", "Pittsburgh");
            await commonLib.selectMultiOptionsFromDropDown("channel", ["WPGH"]);
            await commonLib.clickOnAddButton();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Enter all the required details and click save", async () => {

        let stepAction = "Enter all the required details and click save";
        let stepData = "";
        let stepExpResult = "User should be successfully save/create the rate card";
        let stepActualResult = "Successfully saved/created the rate card";
        rateCardName = "ATA" + new Date().getTime();
        let description = "ATA" + new Date().getTime();
        let cardDetails = { "Channels": ["WPGH"], "Name": rateCardName, "Description": description }
        try {
            await rateCardPage.addRateCard(cardDetails);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Select an existing rate card and click on clone button", async () => {

        let stepAction = "Select an existing rate card and click on clone button";
        let stepData = "";
        let stepExpResult = "User should be able to clone the rate card successfully";
        let stepActualResult = "User is able to clone the rate card successfully";
        cloneRateCardName = "ATA" + new Date().getTime();
        try {
            await action.SetText(commonLib.globalFilter, rateCardName, "Enter " + rateCardName + " in Global Filter");
            await browser.sleep(1000);
            await action.Click(by.xpath(rateCardPage.gridCheckBox.replace("dynamic", String(rateCardName))), "Click On Checkbox");
            await verify.verifyElementIsDisplayed(by.xpath(rateCardPage.gridActiveCheckBox.replace("dynamic", String(rateCardName))), "Verify Checkbox is checked")
            await rateCardPage.clickOnCopyOrCloneButton("clone");
            await action.ClearText(rateCardPage.cloneRateCardName,"Clear");
            await action.SetText(rateCardPage.cloneRateCardName, cloneRateCardName, "");
            await action.Click(rateCardPage.cloneDialogConfirmActiveButton, "Click On Confirm Button");
            await browser.sleep(1000);
            await commonLib.verifyPopupMessage(rateCardPage.cloneRateCardMessage);
            await action.ClearText(commonLib.globalFilter, "");
            await action.SetText(commonLib.globalFilter, cloneRateCardName, "");
            await browser.sleep(1000);
            let status: boolean = await verify.verifyElementVisible(commonLib.noDataMatchFound);
            if (status) throw "failed - No data match is found is displayed after filtering the cloned record";
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select an existing rate card and click on delete button", async () => {

        let stepAction = "Select an existing rate card and click on delete button";
        let stepData = "";
        let stepExpResult = "User should be able to delete the rate card successfully";
        let stepActualResult = "User is able to delete the rate card successfully";

        try {
            await action.Click(by.xpath(rateCardPage.gridCheckBox.replace("dynamic", String(cloneRateCardName))), "Click On Check Box");
            await verify.verifyElementIsDisplayed(by.xpath(rateCardPage.gridActiveCheckBox.replace("dynamic", String(cloneRateCardName))), "Verify Check Box is checked");
            await action.Click(rateCardPage.deleteRateCardButton, "Click On Delete Button");
            await verify.verifyElementIsDisplayed(rateCardPage.deleteRateCardPopupYesButton, "Verify Yes button is displayed");
            await action.Click(rateCardPage.deleteRateCardPopupYesButton, "Click On Yes button");
            await commonLib.verifyPopupMessage(rateCardPage.deleteRateCardMessage);
            await action.ClearText(commonLib.globalFilter, "");
            await action.SetText(commonLib.globalFilter, cloneRateCardName, "");
            await browser.sleep(1000);
            let status: boolean = await verify.verifyElementVisible(commonLib.noDataMatchFound);
            if (!status) throw "failed - No data match is found is not displayed after filtering the deleted cloned record";
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Select a Rate Card from the View and click on the Active button", async () => {

        let stepAction = "Select a Rate Card from the View and click on the Active button";
        let stepData = "Selected Rate Card is Active- true(Active)";
        let stepExpResult = "'Status of the Rate Card should be changed to ''Inactive''(Active- false)'";
        let stepActualResult = "'Status of the Rate Card is changed to ''Inactive''(Active- false)'";
        try {
            await action.ClearText(commonLib.globalFilter, "");
            await action.SetText(commonLib.globalFilter, rateCardName, "");
            await verify.verifyElementIsDisplayed(rateCardPage.gridActiveSlider, "Active Slider is displayed");
            await action.Click(rateCardPage.gridActiveSlider, "");
            await verify.verifyElementIsDisplayed(rateCardPage.gridInActiveSlider, "");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a Rate Card from the View and click on the Active button", async () => {

        let stepAction = "Select a Rate Card from the View and click on the Active button";
        let stepData = "Selected Rate Card is Active- false(Inactive)";
        let stepExpResult = "'Status of the Rate Card should be changed to ''Active''(Active- true)'";
        let stepActualResult = "'Status of the Rate Card is changed to ''Active''(Active- true)'";

        try {
            await action.Click(rateCardPage.gridInActiveSlider, "");
            await verify.verifyElementIsDisplayed(rateCardPage.gridActiveSlider, "");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Check that User can see the Last Updated date/time for each Rate Card", async () => {

        let stepAction = "Check that User can see the Last Updated date/time for each Rate Card";
        let stepData = "";
        let stepExpResult = "Each Rate Cards should be displayed with Last Updated date/time in the Rate Card View";
        let stepActualResult = "Each Rate Cards are displayed with Last Updated date/time in the Rate Card View";
        try {
            await action.ClearText(commonLib.globalFilter, "");
            await browser.sleep(2000);
            let values = await commonLib.getTableColumnValues("Last Updated");
            for (let index = 0; index < values.length; index++) {
                if (values[index] == "") throw "Failed-Last Updated Date is empty in the grid";
            }
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

});
