/*
***********************************************************************************************************
* @Script Name :  XGCT-8536
* @Description :  Add Rate Card Creation - Network12-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 12-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { AppCommonFunctions } from '../../../../../../Libs/ApplicationLibs/CommAppLib';
import { RateCardPage } from '../../../../../../Pages/Pricing/Reference/RateCard';
import { globalTestData } from '../../../../../../TestData/globalTestData';



//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let homePage = new HomePageFunction();
let globalFunc = new globalvalues();
let commonLib = new AppCommonFunctions();
let rateCardPage = new RateCardPage();
//Variables Declaration
let TestCase_ID = 'XGCT-8536'
let TestCase_Title = 'Add Rate Card Creation - Network'
let rateCardName;
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('ADD RATE CARD CREATION - NETWORK', () => {

   // --------------Test Step------------
    it("Navigate to the application", async () => {

        let stepAction = "Navigate to the application";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "User should be able to open the application";
        let stepActualResult = "User should be able to open the application";
        try {
            await globalFunc.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("'Click on ''Pricing'' Tab'", async () => {

        let stepAction = "'Click on ''Pricing'' Tab'";
        let stepData = "";
        let stepExpResult = "'User should be able to navigate to ''Pricing'' Tab.'";
        let stepActualResult = "'User should be able to navigate to ''Pricing'' Tab.'";

        try {
            await homePage.appclickOnNavigationList(globalTestData.pricingTabName);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("'Select 'Local Rate Card' from left panel.'", async () => {

        let stepAction = "'Select Local Rate Card from left panel.'";
        let stepData = "";
        let stepExpResult = " 'Local Rate Card'' page should be displayed '";
        let stepActualResult = " 'Local Rate Card'' page is displayed '";

        try {
            await homePage.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await homePage.appclickOnMenuListLeftPanel(globalTestData.networkRateCardleftMenuName);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Enter all the required details and click save", async () => {

        let stepAction = "Enter all the required details and click save";
        let stepData = "";
        let stepExpResult = "User should be successfully save/create the rate card.Verify success message should be displayed";
        let stepActualResult = "Rate card saved successfully and success message is displayed";
        rateCardName = "ATA"+new Date().getTime();
        let description = "ATA"+new Date().getTime();
        let cardDetails = {"Name":rateCardName,"Description":description};
        try {
            await commonLib.selectDropDownValue("Network",globalTestData.networkName[0]);
            await commonLib.clickOnAddButton();
            await rateCardPage.addRateCard(cardDetails);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Create another Rate card by giving the same name for it as of above", async () => {

        let stepAction = "Create another Rate card by giving the same name for it as of above";
        let stepData = "";
        let stepExpResult = "User should be alerted that Rate Card can not be added(since there is a rate card already with the same name)";
        let stepActualResult = "Rate card already exits error message is displayed";
        let cardDetails = {"Name":rateCardName};
        try {
            await commonLib.selectDropDownValue("Network",globalTestData.networkName[0]);
            await commonLib.clickOnAddButton();
            await rateCardPage.addRateCard(cardDetails,true);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Create another Rate card by entering more than 20 chars for Rate Card Name and click save", async () => {

        let stepAction = "Create another Rate card by entering more than 20 chars for Rate Card Name and click save";
        let stepData = "";
        let stepExpResult = "User should not be able to save the rate card since the rate card field only accepts 20 chars";
        let stepActualResult = "Rated Card field accepted 20 characters only";
        rateCardName = "ATATest12"+new Date().getTime();
        try {
            await verify.verifyElementIsDisplayed(rateCardPage.name,"Verify Name field is displayed");
            await action.ClearText(rateCardPage.name,"");
            await action.SetText(rateCardPage.name,rateCardName,"");
            let actualValue = await action.GetTextFromInput(rateCardPage.name,"");
            if(String(actualValue).length < 20 || String(actualValue).length > 20) throw "Failed-Name field is accepting more than 20 characters";
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult); 
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Make changes in rate card as mentioned in Data", async () => {

        let stepAction = "Make changes in rate card as mentioned in Data";
        let stepData = "";
        let stepExpResult = "User should be able to save the rate card since the rate card field only accepts 20 chars";
        let stepActualResult = "Rate card is saved successfully";
        rateCardName = "ATA&@"+new Date().getTime();
        let cardDetails = {"Name":rateCardName};
        try {
            await rateCardPage.addRateCard(cardDetails);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Create another Rate card by entering 20 chars for Rate Card Name and click save", async () => {

        let stepAction = "Create another Rate card by entering 20 chars for Rate Card Name and click save";
        let stepData = "";
        let stepExpResult = "User should be able to save the rate card since the rate card field only accepts 20 chars";
        let stepActualResult = "Network Rate Card saved succesfully";
        rateCardName = "ATA&@12"+new Date().getTime();
        let cardDetails = {"Name":rateCardName};
        try {
            await commonLib.selectDropDownValue("Network",globalTestData.networkName[0]);
            await commonLib.clickOnAddButton();
            await rateCardPage.addRateCard(cardDetails);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Create another Rate card by entering alpha numerics with special charaters for Description and click save.", async () => {

        let stepAction = "Create another Rate card by entering alpha numerics with special charaters for Description and click save.";
        let stepData = "";
        let stepExpResult = "User should be able to save the rate card";
        let stepActualResult = "Network rate card saved successfully";
        rateCardName = "ATA"+new Date().getTime();
        let description = "ATA&$Description"+new Date().getTime();
        let cardDetails = {"Name":rateCardName,"Description":description};
        try {
            await commonLib.selectDropDownValue("Network",globalTestData.networkName[0]);
            await commonLib.clickOnAddButton();
            await rateCardPage.addRateCard(cardDetails);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Create another Rate card by entering more than 100 characters for Description and click save.", async () => {

        let stepAction = "Create another Rate card by entering more than 100 characters for Description and click save.";
        let stepData = "";
        let stepExpResult = "User should not be able to save the rate card since the description field only accepts 100 chars";
        let stepActualResult = "Description field is accepted 100 characters only";
        let description = "ATADescription"+new Date().getTime()+"ATADescription"+new Date().getTime()+"ATADescription"+new Date().getTime()+"ATADescription"+new Date().getTime();
        try {
            await commonLib.selectDropDownValue("Network",globalTestData.networkName[0]);
            await commonLib.clickOnAddButton();
            await verify.verifyElementIsDisplayed(rateCardPage.name,"Verify Name field is displayed");
            await action.SetText(rateCardPage.description,description,"");
            let actualValue = await action.GetTextFromInput(rateCardPage.description,"");
            if(String(actualValue).length < 100 || String(actualValue).length > 100) throw "Failed-Description field is accepting more than 100 characters";
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Make changes in rate card as mentioned in Data.", async () => {

        let stepAction = "Make changes in rate card as mentioned in Data.";
        let stepData = "";
        let stepExpResult = "User should be able to save the rate card";
        let stepActualResult = "Network Rate Card is saved successfully";
        let description = "ATA$&Description"+new Date().getTime()+"ATADescription"+new Date().getTime()+"ATADescription"+new Date().getTime();
        rateCardName = "ATA"+new Date().getTime();
        let cardDetails = {"Name":rateCardName,"Description":description};
        try {
            await rateCardPage.addRateCard(cardDetails);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Create another Rate card by entering exactly 100 characters for Description and click save.", async () => {

        let stepAction = "Create another Rate card by entering exactly 100 characters for Description and click save.";
        let stepData = "";
        let stepExpResult = "User should be able to save the rate card since the description field only accepts 100 chars";
        let stepActualResult = "Network Rate Card saved successfully";
        rateCardName = "ATA"+new Date().getTime();
        let description = "ATA$&Description"+new Date().getTime()+"ATADescription"+new Date().getTime()+"ATADescription"+new Date().getTime()+"ATA1"+new Date().getTime();
        let cardDetails = {"Name":rateCardName,"Description":description};
        try {
            await commonLib.selectDropDownValue("Network",globalTestData.networkName[0]);
            await commonLib.clickOnAddButton();
            await rateCardPage.addRateCard(cardDetails);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Create another Rate card by entering all mandatory fields and set the rate card as private(enable toggle)", async () => {

        let stepAction = "Create another Rate card by entering all mandatory fields and set the rate card as private(enable toggle)";
        let stepData = "";
        let stepExpResult = "User should be allowed to enter the details";
        let stepActualResult = "User is able to enter the details and Network Rate Card saved successfully";
        rateCardName = "ATA"+new Date().getTime();
        let description = "ATADescription"+new Date().getTime();
        let cardDetails = {"Name":rateCardName,"Description":description,"Public":"No","Users":["Testuser2","Testuser3"]};
        try {
            await commonLib.selectDropDownValue("Network",globalTestData.networkName[0]);
            await commonLib.clickOnAddButton();
            await rateCardPage.addRateCard(cardDetails);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select the specific users from Users drop down and click save", async () => {

        let stepAction = "Select the specific users from Users drop down and click save";
        let stepData = "";
        let stepExpResult = "User should be able to save the rate card";
        let stepActualResult = "Covered in previous spec";

        try {
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Create another Rate card by entering all mandatory fields and set the rate card as public(disable toggle) and click save.", async () => {

        let stepAction = "Create another Rate card by entering all mandatory fields and set the rate card as public(disable toggle) and click save.";
        let stepData = "";
        let stepExpResult = "User should be successfully save/create the rate card";
        let stepActualResult = "Network Rate Card saved successfully";
        rateCardName = "ATA"+new Date().getTime();
        let description = "ATADescription"+new Date().getTime();
        let cardDetails = {"Name":rateCardName,"Description":description,"Public":"Yes"};
        try {
            await commonLib.selectDropDownValue("Network",globalTestData.networkName[0]);
            await commonLib.clickOnAddButton();
            await rateCardPage.addRateCard(cardDetails);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Create another Rate card by entering all mandatory fields and set the rate card as private(enable toggle)", async () => {

        let stepAction = "Create another Rate card by entering all mandatory fields and set the rate card as private(enable toggle)";
        let stepData = "";
        let stepExpResult = "User should be allowed to enter the details";
        let stepActualResult = "User is able to enter the details and Network Rate Card saved successfully";
        rateCardName = "ATA"+new Date().getTime();
        let description = "ATADescription"+new Date().getTime();
        let cardDetails = {"Name":rateCardName,"Description":description,"Public":"No"};
        try {
            await commonLib.selectDropDownValue("Network",globalTestData.networkName[0]);
            await commonLib.clickOnAddButton();
            await rateCardPage.addRateCard(cardDetails);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Do not select any users from Users drop down and click save", async () => {

        let stepAction = "Do not select any users from Users drop down and click save";
        let stepData = "";
        let stepExpResult = "User should be able to save the rate card";
        let stepActualResult = "Covered in previous spec";
        try {
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
});
