/*
***********************************************************************************************************
* @Script Name :  XGCT-8483
* @Description :  xG Campaign - Pricing - Validation: Section view (Network)
* @Page Object Name(s) : Pricing
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 01-08-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { SectionLevels } from '../../../../../../Pages/Pricing/Reference/SectionLevels';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { globalTestData } from '../../../../../../TestData/globalTestData';
import { AppCommonFunctions } from '../../../../../../Libs/ApplicationLibs/CommAppLib';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let homePage = new HomePageFunction();
let section = new SectionLevels();
let appCommFunction = new AppCommonFunctions();

//Variables Declaration
let TestCase_ID = 'XGCT-8483'
let TestCase_Title = 'xG Campaign - Pricing - Validation: Section view (Network)';
var sectionName = "Sec" + new Date().getTime();
var trafficName = "Traffic" + new Date().getTime();
var pricing = "Pricing";
let network = "Network";
let name = "Name";
let trafficTranslator = "Traffic Translator";
let edit = "Edit";
let deleteName = "Delete";

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - PRICING - VALIDATION: SECTION VIEW (NETWORK)', () => {

    // --------------Test Step 1------------
    it("Navigate to the application URL", async () => {

        let stepAction = "Navigate to the application URL";
        let stepData = "";
        let stepExpResult = "User should be navigated to the application";
        let stepActualResult = "User is navigated to the application";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step 2------------
    it("Navigate to  Pricing>Sections", async () => {

        let stepAction = "Navigate to  Pricing>Sections";
        let stepData = "";
        let stepExpResult = "Check the Section Level  View page is displayed";
        let stepActualResult = "The Section Level  View page is displayed";

        try {
            await homePage.appclickOnNavigationList(pricing);
            await homePage.appclickOnMenuListLeftPanel("Reference");
            await homePage.appclickOnMenuListLeftPanel("Network Section Levels");
            await verify.verifyElementIsDisplayed(appCommFunction.pageHeader, " Section Page");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 3------------
    it("Navigate to  Pricing>Sections> add", async () => {

        let stepAction = "Navigate to  Pricing>Sections> add";
        let stepData = "";
        let stepExpResult = "Check the add section level view page is displayed";
        let stepActualResult = "The add section level view page is displayed";

        try {
            await action.Click(by.xpath(section.sectionButtons.replace('dynamic', 'Add')), "Add Section Button");
            await verify.verifyElementIsDisplayed(by.xpath(section.popUpDialog.replace('dynamic', 'Add Section')), "Add Section Pop Up Dialog");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 4------------
    it("Select Network", async () => {

        let stepAction = "Select Network";
        let stepData = globalTestData.networksToBeVerifiedInGrid;
        let stepExpResult = "Network should be selected";
        let stepActualResult = "Network is selected";

        try {
            await action.Click(by.xpath(section.MarketChannelDropdown.replace('dynamic', network)), "Network dropdown");
            await appCommFunction.clickOnComponentLink(globalTestData.networksToBeVerifiedInGrid);
            await section.verifyDropdownSelectedValue(network, globalTestData.networksToBeVerifiedInGrid);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 5------------
    it("Enter section name and Traffic Translator", async () => {

        let stepAction = "Enter section name and Traffic Translator";
        let stepData = "";
        let stepExpResult = "section name and Traffic Translator should be taken";
        let stepActualResult = "section name and Traffic Translator are taken";

        try {
            await action.SetText(by.xpath(section.sectionNameTraffic.replace('dynamic', name)), sectionName, "");
            await action.SetText(by.xpath(section.sectionNameTraffic.replace('dynamic', trafficTranslator)), trafficName, "");
            await section.verifyTextIninputAndTextArea(by.xpath(section.sectionNameTraffic.replace('dynamic', name)), sectionName);
            await section.verifyTextIninputAndTextArea(by.xpath(section.sectionNameTraffic.replace('dynamic', trafficTranslator)), trafficName);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    // --------------Test Step 6------------
    it("Enter description ", async () => {
        let stepAction = "Enter description ";
        let stepData = "";
        let stepExpResult = "Description should be taken";
        let stepActualResult = "Description is taken";

        try {
            await action.SetText(section.SectionDescription, "Description added", "Section Description");
            await section.verifyTextIninputAndTextArea(section.SectionDescription, "Description added");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step 8------------
    it("Click on cancel on add section level", async () => {

        let stepAction = "Click on cancel on add section level";
        let stepData = "";
        let stepExpResult = "User should be navigated to the application and record should not be created";
        let stepActualResult = "User is navigated to the application and record is not created";

        try {
            await action.Click(section.clickCancel, " Cancel Section");
            await browser.sleep(1000);
            await action.SetText(section.serachsectionname, sectionName, "");
            await browser.sleep(1000);
            await action.GetText(section.tableFirstRow, "").then(async function (_txt: string) {
                if(_txt!="No data match is found")  throw 'failed - '+ _txt 
            });
            report.ReportStatus(stepAction, stepData, 'PASS', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 7------------
    it("Click on save ", async () => {
        let stepAction = "Click on save ";
        let stepData = "";
        let stepExpResult = "Section level should be created and it should be avalible in the section level grid";
        let stepActualResult = "Section level is created and it is avalible in the section level grid";

        try {
            await action.Click(by.xpath(section.sectionButtons.replace('dynamic', 'Add')), "Add Section Button");
            await action.Click(by.xpath(section.MarketChannelDropdown.replace('dynamic', network)), "Network dropdown");
            await appCommFunction.clickOnComponentLink(globalTestData.networksToBeVerifiedInGrid);
            await action.SetText(by.xpath(section.sectionNameTraffic.replace('dynamic', name)), sectionName, "");
            await action.SetText(by.xpath(section.sectionNameTraffic.replace('dynamic', trafficTranslator)), trafficName, "");
            await action.SetText(section.SectionDescription, "Description added", "Section Description");
            await action.Click(section.clickSave, "Save Section");
            browser.call(async function () {
                await browser.sleep(6000);
                await action.ClearText(section.serachsectionname, sectionName);
                await action.SetText(section.serachsectionname, sectionName, "");
                await browser.sleep(4000);
            });
            await section.verifyGridDataPresent("Section", sectionName);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step 9------------
    it("Check the active and in-active toggle is avaliable in the grid", async () => {
        let stepAction = "Check the active and in-active toggle is avaliable in the grid";
        let stepData = "";
        let stepExpResult = "Active and in-active should be avaliable in the grid";
        let stepActualResult = "Active and in-active are avaliable in the grid";

        try {
            verify.verifyElementIsDisplayed(section.toggleSwitch, " Toggle Button");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    // --------------Test Step 10------------
    it("Check edit button is avaliable in the section level grid", async () => {

        let stepAction = "Check edit button is avaliable in the section level grid";
        let stepData = "";
        let stepExpResult = "Edit button should be avalible in the grid";
        let stepActualResult = "Edit button is avalible in the grid";

        try {
            verify.verifyElementIsDisplayed(by.xpath(section.sectionButtons.replace('dynamic', edit)), " Edit Button");
            verify.verifyElementIsDisabled(by.xpath(section.sectionButtons.replace('dynamic', edit)), " Edit Button");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 11------------
    it("Check the delete button is avaliable in the section level grid", async () => {

        let stepAction = "Check the delete button is avaliable in the section level grid";
        let stepData = "";
        let stepExpResult = "Delete button should be avalible in the grid";
        let stepActualResult = "Delete button is avalible in the grid";

        try {
            verify.verifyElementIsDisplayed(by.xpath(section.sectionButtons.replace('dynamic', deleteName)), " Delete Button");
            verify.verifyElementIsDisabled(by.xpath(section.sectionButtons.replace('dynamic', deleteName)), " Delete Button");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 12------------
    it("Check the user is able to the source for the records", async () => {

        let stepAction = "Check the user is able to the source for the records";
        let stepData = "";
        let stepExpResult = "User should be able to view source for the records";
        let stepActualResult = "User is able to view source for the records";

        try {
            await section.verifyGridDataPresent("Source", "Custom");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 14------------
    it("Check the  edit, delete button is  perform any operations by default", async () => {

        let stepAction = "Check the  edit, delete button is  perform any operations by default";
        let stepData = "";
        let stepExpResult = "Default edit, delete button should not perform any operations";
        let stepActualResult = "Default edit, delete button are not able perform any operations";

        try {
            verify.verifyElementIsDisplayed(by.xpath(section.sectionButtons.replace('dynamic', deleteName)), " Delete Button");
            verify.verifyElementIsDisabled(by.xpath(section.sectionButtons.replace('dynamic', deleteName)), " Delete Button");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 13------------
    it("Select a record and check the edit and delete button should be enable", async () => {

        let stepAction = "Select a record and check the edit and delete button should be enable";
        let stepData = "";
        let stepExpResult = "Edit, delete button should be enable when user select a record";
        let stepActualResult = "Edit, delete button are enable when user select a record";

        try {
            action.Click(section.tableFirstRow, " Select Row");
            verify.verifyElementIsEnabled(by.xpath(section.sectionButtons.replace('dynamic', edit)), " Edit Button");
            verify.verifyElementIsEnabled(by.xpath(section.sectionButtons.replace('dynamic', deleteName)), " Delete Button");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step 15------------
    it("Select a record and  click on edit", async () => {

        let stepAction = "Select a record and  click on edit";
        let stepData = "";
        let stepExpResult = "User should have option to edit section level details";
        let stepActualResult = "User should have option to edit section level details";

        try {
            await action.Click(section.tableFirstRow, " Select Row");
            await action.Click(by.xpath(section.sectionButtons.replace('dynamic', edit)), "Edit Button");
            report.ReportStatus(stepAction, stepData, 'SKIP', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 16 ------------
    it("Select a record and click on delete", async () => {

        let stepAction = "Select a record and click on delete";
        let stepData = "";
        let stepExpResult = "System should show confirmation pop-up window witn delete and cancel buttons";
        let stepActualResult = "System should show confirmation pop-up window witn delete and cancel buttons";

        try {
            await action.Click(section.tableFirstRow, " Select Row");
            await action.Click(by.xpath(section.sectionButtons.replace('dynamic', deleteName)), "Delete Button");
            report.ReportStatus(stepAction, stepData, 'SKIP', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 17------------
    it("Click on Delete in confirmation pop-up window", async () => {

        let stepAction = "Click on Delete in confirmation pop-up window";
        let stepData = "";
        let stepExpResult = "Record should be deleted sucessfully";
        let stepActualResult = "Record should be deleted sucessfully";

        try {
            report.ReportStatus(stepAction, stepData, 'SKIP', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 18------------
    it("Check the last updated date is updating when we change  exist section level details", async () => {

        let stepAction = "Check the last updated date is updating when we change  exist section level details";
        let stepData = "";
        let stepExpResult = "Last update date should be updated , when we change exist section levels";
        let stepActualResult = "Last update date should be updated , when we change exist section levels";

        try {
            //await section.verifyGridDataPresent("Last Updated", global.getcurrentDate);
            report.ReportStatus(stepAction, stepData, 'SKIP', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

});
