/*
***********************************************************************************************************
* @Script Name :  XGCT-8644
* @Description :  xG Campaign - Reporting – Demographic Reference : Verify the columns displayed for Demographic Reference Grid when Source is selected as Nielsen2-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 2-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { Demographics } from '../../../../../Pages/Reporting/ReferenceData/Demographics';



//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let globalvalue = new globalvalues();
let demoref = new Demographics();
let home = new HomePageFunction()
let locallength = new LocalLengthFactorsPricing()
let columnnames: Array<string> = ["Demo Target", "Description", "Category", "Type", "First Reported"]


//Variables Declaration
let TestCase_ID = 'XGCT-8644'
let TestCase_Title = 'xG Campaign - Reporting – Demographic Reference : Verify the columns displayed for Demographic Reference Grid when Source is selected as Nielsen'

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - REPORTING – DEMOGRAPHIC REFERENCE : VERIFY THE COLUMNS DISPLAYED FOR DEMOGRAPHIC REFERENCE GRID WHEN SOURCE IS SELECTED AS NIELSEN', () => {




    // --------------Test Step------------
    it("Login to xGCampaign web app", async () => {

        let stepAction = "Login to xGCampaign web app";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net";
        let stepExpResult = "User should be able to open the application";
        let stepActualResult = "User is able to open the application";

        try {
            await browser.waitForAngularEnabled(false)
            await globalvalue.LaunchStoryBook();


            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Reporting > Reference > Demographic > Demographic Reference page", async () => {

        let stepAction = "Navigate to Reporting > Reference > Demographic > Demographic Reference page";
        let stepData = "";
        let stepExpResult = "User should be navigated to Demographic Reference page";
        let stepActualResult = "User is able to navigated to Demographic Reference page";

        try {
            await home.appclickOnNavigationList("Reference");
            await browser.sleep(3000)
            // await home.appclickOnMenuListLeftPanel("Reference Data");
            // await browser.sleep(1000)
            await home.appclickOnMenuListLeftPanel("Demographics")

            // await emailDistributionlist.clickOnReporting('Demographics')


            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select the Source as Nielsen", async () => {

        let stepAction = "Select the Source as Nielsen";
        let stepData = "";
        let stepExpResult = "User should be able to select the Source as Nielsen";
        let stepActualResult = "User is able to select the Source as Nielsen";

        try {
            //  await action.SwitchToFrame(0)

            await demoref.selectSingleSelectDropDownOptions(demoref.sourceDropdown, locallength.singleSelectDropdownOptionsLocator, "Nielsen", "Source")

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Select 'Category'  from the Category dropdown", async () => {

        let stepAction = "Select the Category ";
        let stepData = "";
        let stepExpResult = "User should be able to select the Category";
        let stepActualResult = "User is  able to select the Source as category";

        try {
            await browser.sleep(5000)
            await demoref.selectSingleSelectDropDownOptions(demoref.categorydropdown, locallength.singleSelectDropdownOptionsLocator, "Females", "category")


            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Select 'Market' from the Market dropdown", async () => {

        let stepAction = "Select the Market from dropdown";
        let stepData = "";
        let stepExpResult = "User should be able to select the Market";
        let stepActualResult = "User is able to select the market";

        try {
            await demoref.selectSingleSelectDropDownOptions(demoref.marketdropdown, locallength.singleSelectDropdownOptionsLocator, "Pittsburgh", "Market")

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step------------
    it("Check column name on the grid", async () => {

        let stepAction = "Check column name on the grid";
        let stepData = "";
        let stepExpResult = "Following should be displayed. Demo Target Description Category Type First Reported";
        let stepActualResult = "Demo Target Description Category Type First Reported are displayed on the grid";

        try {

            await browser.sleep(3000)
            await demoref.VerifyColumnNameDisplayedInGrid(columnnames)
            await demoref.VerifyColumnNameDisplayedInGrid(["Demo Target"])



            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check values under Demo Target column.", async () => {

        let stepAction = "'Check values under ''Demo Target'' column.'";
        let stepData = "";
        let stepExpResult = "'Following data should get displayed:";
        let stepActualResult = "'Following data is displayed:";

        try {

            await demoref.GridValidationsUpdated("Demo Target", "W1")

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check values under Description column.", async () => {

        let stepAction = "'Check values under ''Description'' column.'";
        let stepData = "";
        let stepExpResult = "'Following  test data should get displayed:";
        let stepActualResult = "'Following  test data is displayed:";

        try {
            await demoref.GridValidationsUpdated("Description", "Nielsen")

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check values under Category column.", async () => {

        let stepAction = "'Check values under ''Category'' column.'";
        let stepData = "";
        let stepExpResult = "'It should be displayed expected data";
        let stepActualResult = " expected data is displayed";

        try {
            await demoref.GridValidationsUpdated("Category", "Females")

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check values under Type column.", async () => {

        let stepAction = "'Check values under ''Type'' column.'";
        let stepData = "";
        let stepExpResult = "'It should be displayed either ''Reportable or Building Block''.'";
        let stepActualResult = "Reportable or Building Block is displayed";

        try {
            await element(demoref.type).sendKeys("Reportable")
            await demoref.GridValidationsUpdated("Type", "Reportable")

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check values under First Reported column.", async () => {

        let stepAction = "'Check values under ''First Reported'' column.'";
        let stepData = "";
        let stepExpResult = "'[It should be displayed date [EX(format) ''01/01/2000 12:00 AM'']'";
        let stepActualResult = "'[ date [EX(format) ''01/01/2000 12:00 AM'']' is displayed";

        try {

            await demoref.GridValidationsUpdated("First Reported", "")

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
