/*
***********************************************************************************************************
* @Script Name :  XGCT-8646
* @Description :  xG Campaign - Reporting – Demographic Reference : Verify the filter functionality for the columns displayed for Demographic Reference Grid when Source is selected as Nielsen6-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 6-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

 //Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { Demographics } from '../../../../../Pages/Reporting/ReferenceData/Demographics';



//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let globalvalue =new globalvalues();
let demoref = new Demographics();
let home = new HomePageFunction()
let locallength = new LocalLengthFactorsPricing() 
let columnnames: Array<string> = ["Demo Target","Description","Category","Type","First Reported"]


//Variables Declaration
let TestCase_ID = 'XGCT-8646'
let TestCase_Title = 'xG Campaign - Reporting – Demographic Reference : Verify the filter functionality for the columns displayed for Demographic Reference Grid when Source is selected as Nielsen'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID,TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - REPORTING – DEMOGRAPHIC REFERENCE : VERIFY THE FILTER FUNCTIONALITY FOR THE COLUMNS DISPLAYED FOR DEMOGRAPHIC REFERENCE GRID WHEN SOURCE IS SELECTED AS NIELSEN', () => {



// --------------Test Step------------
it("Login to xGCampaign web app", async () => {

let stepAction = "Login to xGCampaign web app";
let stepData = "http://xgcampaignwebapp.azurewebsites.net";
let stepExpResult = "User should be able to open the application";
let stepActualResult = "User is able to open the application";

try { 
    await  browser.waitForAngularEnabled(false)
    await globalvalue.LaunchStoryBook();

report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Navigate to Reporting > Reference > Demographic > Demographic Reference page", async () => {

let stepAction = "Navigate to Reporting > Reference > Demographic > Demographic Reference page";
let stepData = "";
let stepExpResult = "User should be navigated to Demographic Reference page";
let stepActualResult = "User is navigated to Demographic Reference page";

try { 
    await home.appclickOnNavigationList("Reference");
    await browser.sleep(3000)
    // await home.appclickOnMenuListLeftPanel("Reference Data");
    // await browser.sleep(1000)
    await home.appclickOnMenuListLeftPanel("Demographics")

report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select the Source as Nielsen", async () => {

let stepAction = "Select the Source as Nielsen";
let stepData = "";
let stepExpResult = "User should be able to select the Source as Nielsen";
let stepActualResult = "User is able to select the Source as Nielsen";

try { 
    await demoref.selectSingleSelectDropDownOptions(demoref.sourceDropdown,locallength.singleSelectDropdownOptionsLocator,"Nielsen","Source")


report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

}); 
// --------------Test Step------------
it("Select 'Category'  from the Category dropdown", async () => {

    let stepAction = "Select 'Category'  from the Category dropdown";
    let stepData = "";
    let stepExpResult = "User should be able to select the Category";
    let stepActualResult = "User is able to select the Category";
    
    try { 
        await browser.sleep(5000)
        await demoref.selectSingleSelectDropDownOptions(demoref.categorydropdown,locallength.singleSelectDropdownOptionsLocator,"All","category")

    
    report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
     }
    
    catch (err) {
    report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
     }
    
    });
   
// --------------Test Step------------
it("Select 'Market' from the Market dropdown", async () => {

    let stepAction = "Select 'Market' from the Market dropdown";
    let stepData = "";
    let stepExpResult = "User should be able to select the Market";
    let stepActualResult = "User is able to select the Source as Market";
    
    try {
        await demoref.selectSingleSelectDropDownOptions(demoref.marketdropdown,locallength.singleSelectDropdownOptionsLocator,"Pittsburgh","Market")

    report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
     }
    
    catch (err) {
    report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
     }
    
    });
       // --------------Test Step------------
it("Check column name on the grid", async () => {

    let stepAction = "Check column name on the grid";
    let stepData = "";
    let stepExpResult = "Following should be displayed. Demo Target Description Category Type First Reported";
    let stepActualResult = "Demo Target Description Category Type First Reported is displayed";
    
    try { 
        
        await browser.sleep(3000)
        await demoref.VerifyColumnNameDisplayedInGrid(columnnames)
        await demoref.VerifyColumnNameDisplayedInGrid(["Demo Target"])
        
   
    
    report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
     }
    
    catch (err) {
    report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
     }
    
    });
       // --------------Test Step------------

     // --------------Test Step------------
     it("Filter values under Demo Target column.", async () => {

        let stepAction = "Filter values under Demo Target column.";
        let stepData = "";
        let stepExpResult = "User should be able to Filter the Column values using the selected Filter Criteria";
        let stepActualResult = "User is able to Filter the Column values using the selected Filter Criteria";
        
        try { 
            await demoref.searchAndValidateValueInGridTable("Demo Target",demoref.Startswith,"W12+","Startswith")
            await demoref.searchAndValidateValueInGridTable("Demo Target",demoref.Endswith,"W12+","Endswith")
            await demoref.searchAndValidateValueInGridTable("Demo Target",demoref.Contains,"W12+","contains")
            await demoref.searchAndValidateValueInGridTable("Demo Target",demoref.Equals,"W12+","equals")
            await demoref.searchAndValidateValueInGridTable("Demo Target",demoref.DoesnotEqual,"W12+","doesnotequal")
    
            
       
        
        report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
         }
        
        catch (err) {
        report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
         }
        
        });
    
    // --------------Test Step------------
    it("Filter values under Description column.", async () => {

        let stepAction = "Filter values under Description column.";
        let stepData = "";
        let stepExpResult = "User should be able to Filter the Column values using the selected Filter Criteria";
        let stepActualResult = "User is able to Filter the Column values using the selected Filter Criteria";
        
        try { 
            await demoref.searchAndValidateValueInGridTable("Description",demoref.Startswith,"Nielsen","Startswith")
            await demoref.searchAndValidateValueInGridTable("Description",demoref.Endswith,"18-20","Endswith")
             await demoref.searchAndValidateValueInGridTable("Description",demoref.Contains,"Nielsen","contains")
             await demoref.searchAndValidateValueInGridTable("Description",demoref.Equals,"Nielsen - Females 12-14","Equals")
             await demoref.searchAndValidateValueInGridTable("Description",demoref.DoesnotEqual,"Nielsen","DoesnotEqual")
    
            
       
        
        report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
         }
        
        catch (err) {
        report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
         }
        });
           // --------------Test Step------------
    it("Filter values under Category column.", async () => {

        let stepAction = "Filter values under Category column";
        let stepData = "";
        let stepExpResult = "User should be able to Filter the Column values using the selected Filter Criteria";
        let stepActualResult = "User is able to Filter the Column values using the selected Filter Criteria";
        
        try { 
            await demoref.searchAndValidateValueInGridTable("Category",demoref.Startswith,"Females","Startswith")
            await demoref.searchAndValidateValueInGridTable("Category",demoref.Endswith,"Females","Endswith")
             await demoref.searchAndValidateValueInGridTable("Category",demoref.Contains,"Females","contains")
             await demoref.searchAndValidateValueInGridTable("Category",demoref.Equals,"Females","Equals")
             await demoref.searchAndValidateValueInGridTable("Category",demoref.DoesnotEqual,"Females","DoesnotEqual")
    
            
       
        
        report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
         }
        
        catch (err) {
        report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
        } 
    });
                   // --------------Test Step------------
    it("Filter values under Type column.", async () => {

        let stepAction = "Filter values under Type column";
        let stepData = "";
        let stepExpResult = "User should be able to Filter the Column values using the selected Filter Criteria";
        let stepActualResult = "User is able to Filter the Column values using the selected Filter Criteria";
        
        try { 
            await demoref.searchAndValidateValueInGridTable("Type",demoref.Startswith,"Reportable","Startswith")
            await demoref.searchAndValidateValueInGridTable("Type",demoref.Endswith,"Reportable","Endswith")
             await demoref.searchAndValidateValueInGridTable("Type",demoref.Contains,"Reportable","contains")
             await demoref.searchAndValidateValueInGridTable("Type",demoref.Equals,"Reportable","Equals")
             await demoref.searchAndValidateValueInGridTable("Type",demoref.DoesnotEqual,"Reportable","DoesnotEqual")
    
            
       
        
        report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
         }
        
        catch (err) {
        report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
        } 
    });

    //                        // --------------Test Step------------
    // it("Filter values under First Reported column.", async () => {

    //     let stepAction = "Select the Source as Nielsen";
    //     let stepData = "";
    //     let stepExpResult = "User should be able to Filter the Column values using the selected Filter Criteria";
    //     let stepActualResult = "User is able to Filter the Column values using the selected Filter Criteria";
        
    //     try { 
    //         await demoref.searchAndValidateValueInGridTable("First Reported",locallength.Startswith,"","Startswith")
    //         await demoref.searchAndValidateValueInGridTable("First Reported",locallength.Endswith,"","Endswith")
    //          await demoref.searchAndValidateValueInGridTable("First Reported",locallength.Contains,"","contains")
    //          await demoref.searchAndValidateValueInGridTable("First Reported",locallength.Equals,"","Equals")
    //          await demoref.searchAndValidateValueInGridTable("First Reported",locallength.DoesnotEqual,"","DoesnotEqual")
    
            
       
        
    //     report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
    //      }
        
    //     catch (err) {
    //     report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
    //     } 
    
        
    //     });
    });
    
    
    
    



    
    






