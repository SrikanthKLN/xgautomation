/*
***********************************************************************************************************
* @Script Name :  XGCT-8646
* @Description :  xG Campaign - Reporting – Demographic Reference : Verify the filter functionality for the columns displayed for Demographic Reference Grid when Source is selected as Nielsen6-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 6-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

 //Import Statements
 import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { Demographics } from '../../../../../Pages/Reporting/ReferenceData/Demographics';

 //Import Class Objects Instantiation
 let report = new Reporter();
 let verify = new VerifyLib();
 let action = new ActionLib();
 let globalvalue =new globalvalues();
 let demoref = new Demographics();
 
 
 let home = new HomePageFunction()
 let locallength = new LocalLengthFactorsPricing() 
 let columnnames: Array<string> = ["Tag","Demo Target","Description","Category","Tier","First Reported"]
 
 
 //Variables Declaration
 let TestCase_ID = 'XGCT-8646'
 let TestCase_Title = 'xG Campaign - Reporting – Demographic Reference : Verify the filter functionality for the columns displayed for Demographic Reference Grid when Source is selected as Nielsen'
 //HTML Report generate intiation
 report.InitializeSigleHtmlReport(TestCase_ID,TestCase_Title);
 globalvalues.reportInstance = report;
 
 //**********************************  TEST CASE Implementation ***************************
 describe('XG CAMPAIGN - REPORTING – DEMOGRAPHIC REFERENCE : VERIFY THE FILTER FUNCTIONALITY FOR THE COLUMNS DISPLAYED FOR DEMOGRAPHIC REFERENCE GRID WHEN SOURCE IS SELECTED AS NIELSEN', () => {
 
 
 
 // --------------Test Step------------
 it("Login to xGCampaign web app", async () => {
 
 let stepAction = "Login to xGCampaign web app";
 let stepData = "http://xgcampaignwebapp.azurewebsites.net";
 let stepExpResult = "User should be able to open the application";
 let stepActualResult = "User is able to open the application";
 
 try { 
     await  browser.waitForAngularEnabled(false)
     await globalvalue.LaunchStoryBook();
 
 report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
  }
 
 catch (err) {
 report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
  }
 
 });
 
 
 // --------------Test Step------------
 it("Navigate to Reporting > Reference > Demographic > Demographic Reference page", async () => {
 
 let stepAction = "Navigate to Reporting > Reference > Demographic > Demographic Reference page";
 let stepData = "";
 let stepExpResult = "User should be navigated to Demographic Reference page";
 let stepActualResult = "User is able to  navigated to Demographic Reference page";
 
 try { 
     await home.appclickOnNavigationList("Reference");
     await home.appclickOnMenuListLeftPanel("Reference Data");
     await home.appclickOnMenuListLeftPanel("Reference Data");
     await home.appclickOnMenuListLeftPanel("Demographics")
     await browser.sleep(5000)
 
 report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
  }
 
 catch (err) {
 report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
  }
 
 });
 
 
 // --------------Test Step------------
 it("Select the Source as Comscore", async () => {
 
 let stepAction = "Select the Source as Comscore";
 let stepData = "";
 let stepExpResult = "User should be able to select the Source as Comscore";
 let stepActualResult = "User is able to select the Source as Comscore";
 
 try { 
     await demoref.selectSingleSelectDropDownOptions(demoref.sourceDropdown,locallength.singleSelectDropdownOptionsLocator,"Comscore","Source")
     await browser.sleep(5000)
 
 
 report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
  }
 
 catch (err) {
 report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
  }
 
 }); 
 
 // --------------Test Step------------
 it("Select 'Category'  from the Category dropdown", async () => {
 
     let stepAction = "Select 'Category'  from the Category dropdown";
     let stepData = "";
     let stepExpResult = "User should be able to select the Category";
     let stepActualResult = "User is  able to select the Source as Nielsen";
     
     try { 
         await browser.sleep(5000)
         await demoref.selectSingleSelectDropDownOptions(demoref.categorydropdown,locallength.singleSelectDropdownOptionsLocator,"All","category")
 
     
     report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
      }
     
     catch (err) {
     report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
      }
     
     });
    
 // --------------Test Step------------
 it("Select 'Market' from the Market dropdown", async () => {
 
     let stepAction = "Select 'Market' from the Market dropdown";
     let stepData = "";
     let stepExpResult = "User should be able to select the Market";
     let stepActualResult = "User is able to select the market from dropdown";
     
     try {
         await demoref.selectSingleSelectDropDownOptions(demoref.marketdropdown,locallength.singleSelectDropdownOptionsLocator,"Pittsburgh","Market")
 
     report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
      }
     
     catch (err) {
     report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
      }
     
     });
        // --------------Test Step------------
 it("Check column name on the grid", async () => {
 
     let stepAction = "Check column name on the grid";
     let stepData = "";
     let stepExpResult = "Following should be displayed. Demo Target Description Category Type First Reported";
     let stepActualResult = "Demo Target Description Category Type First Reported is displayed";
     
     try { 
         
 
         await demoref.VerifyColumnNameDisplayedInGrid(columnnames)
         
         
    
     
     report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
      }
     
     catch (err) {
     report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
      }
     
     });
        // --------------Test Step------------
      // --------------Test Step------------
      it("Filter values under Demo Target column.", async () => {
 
         let stepAction = "Filter values under Demo Target column.";
         let stepData = "";
         let stepExpResult = "User should be able to Filter the Column values using the selected Filter Criteria";
         let stepActualResult = "User is able to Filter the Column values using the selected Filter Criteria";
         
         try { 
             await demoref.searchAndValidateValueInGridTable("Demo Target",demoref.Startswith,"$","Startswith")
             await demoref.searchAndValidateValueInGridTable("Demo Target",demoref.Endswith,"0","Endswith")
             await demoref.searchAndValidateValueInGridTable("Demo Target",demoref.Contains,"$","contains")
             await demoref.searchAndValidateValueInGridTable("Demo Target",demoref.Equals,"$0-20","equals")
             await demoref.searchAndValidateValueInGridTable("Demo Target",demoref.DoesnotEqual,"$0-20","doesnotequal")
     
             
        
         
         report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
          }
         
         catch (err) {
         report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
          }
         
         });
     
     // --------------Test Step------------
     it("Filter values under Description column.", async () => {
 
         let stepAction = "Filter values under Description column";
         let stepData = "";
         let stepExpResult = "User should be able to Filter the Column values using the selected Filter Criteria";
         let stepActualResult = "User is able to Filter the Column values using the selected Filter Criteria";
         
         try { 
             await demoref.searchAndValidateValueInGridTable("Description",demoref.Startswith,"Comscore","Startswith")
             await demoref.searchAndValidateValueInGridTable("Description",demoref.Endswith,"9","Endswith")
              await demoref.searchAndValidateValueInGridTable("Description",demoref.Contains,"Comscore","contains")
              await demoref.searchAndValidateValueInGridTable("Description",demoref.Equals,"Comscore - $0 - $19,999","Equals")
              await demoref.searchAndValidateValueInGridTable("Description",demoref.DoesnotEqual,"Comscore","Endswith")
     
             
        
         
         report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
          }
         
         catch (err) {
         report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
          }
         });
            // --------------Test Step------------
     it("Filter values under Category column.", async () => {
 
         let stepAction = "Filter values under Category column";
         let stepData = "";
         let stepExpResult = "User should be able to Filter the Column values using the selected Filter Criteria";
         let stepActualResult = "User is able to Filter the Column values using the selected Filter Criteria";
         
         try { 
             await demoref.searchAndValidateValueInGridTable("Category",demoref.Startswith,"HH Income","Startswith")
             await demoref.searchAndValidateValueInGridTable("Category",demoref.Endswith,"HH Income","Endswith")
              await demoref.searchAndValidateValueInGridTable("Category",demoref.Contains,"HH Income","contains")
              await demoref.searchAndValidateValueInGridTable("Category",demoref.Equals,"HH Income","Equals")
              await demoref.searchAndValidateValueInGridTable("Category",demoref.DoesnotEqual,"HH Income","DoesnotEqual")
     
             
        
         
         report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
          }
         
         catch (err) {
         report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
         } 
     });
                    // --------------Test Step------------
     it("Filter values under Tier column.", async () => {
 
         let stepAction = "Filter values under Tier column";
         let stepData = "";
         let stepExpResult = "User should be able to Filter the Column values using the selected Filter Criteria";
         let stepActualResult = "User is able to Filter the Column values using the selected Filter Criteria";
         
         try { 
             await demoref.searchAndValidateValueInGridTable("Tier",demoref.Startswith,"E","Startswith")
             await demoref.searchAndValidateValueInGridTable("Tier",demoref.Endswith,"E","Endswith")
              await demoref.searchAndValidateValueInGridTable("Tier",demoref.Contains,"E","contains")
              await demoref.searchAndValidateValueInGridTable("Tier",demoref.Equals,"E","Equals")
              await demoref.searchAndValidateValueInGridTable("Tier",demoref.DoesnotEqual,"F","DoesnotEqual")
     
             
        
         
         report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
          }
         
         catch (err) {
         report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
         } 
     });
 
    //                         // --------------Test Step------------
    //  it("Filter values under First Reported column.", async () => {
 
    //      let stepAction = "Select the Source as Nielsen";
    //      let stepData = "";
    //      let stepExpResult = "User should be able to Filter the Column values using the selected Filter Criteria";
    //      let stepActualResult = "User is able to Filter the Column values using the selected Filter Criteria";
         
    //      try { 
    //          await demoref.searchAndValidateValueInGridTable("First Reported",demoref.Startswith,"","Startswith")
    //          await demoref.searchAndValidateValueInGridTable("First Reported",demoref.Endswith,"","Endswith")
    //           await demoref.searchAndValidateValueInGridTable("First Reported",demoref.Contains,"","contains")
    //           await demoref.searchAndValidateValueInGridTable("First Reported",demoref.Equals,"","Equals")
    //           await demoref.searchAndValidateValueInGridTable("First Reported",demoref.DoesnotEqual,"","DoesnotEqual")
     
             
        
         
    //      report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
    //       }
         
    //      catch (err) {
    //      report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
    //      } 
     
         
    //      });
    
     });
     
     
     
     
 
 
 
     
     
 
 
 
 
 
 
 