/*
***********************************************************************************************************
* @Script Name :  XGCT-8552
* @Description :  Rating Market Reference Grid - Verify Filter functionality (Nielsen)12-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 12-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { ProgramRatingTracks } from '../../../../../Pages/Inventory/ProgramRatingTracks';
import { RateEntry } from '../../../../../Pages/Pricing/RateEntry';
import { LocalLengthFactorsPricing } from '../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { Markets } from '../../../../../Pages/Reporting/ReferenceData/Markets';




//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let home = new HomePageFunction()
let global = new globalvalues();
let programTracksPage=new ProgramRatingTracks();
let rateEntry = new RateEntry()
let localLF = new LocalLengthFactorsPricing();

let channel = new Markets();
//Variables Declaration
let TestCase_ID = 'XGCT-8552'
let TestCase_Title = 'Rating Market Reference Grid - Verify Filter functionality (Comscore)'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('Rating Market Reference Grid - Verify Filter functionality (Comscore)', () => {

     // --------------Test Step------------
     it("Open browser and enter CHASSIS URL", async () => {

          let stepAction = "Open browser and enter CHASSIS URL";
          let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
          let stepExpResult = "Page is loaded successfully.";
          let stepActualResult = "Page is loaded successfully.";

          try {
               await global.LaunchStoryBook();
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Click Reporting menu item", async () => {

          let stepAction = "Click Reporting menu item";
          let stepData = "";
          let stepExpResult = "Left navigation should display Reference and Rating Data Search menus";
          let stepActualResult = "Left navigation should display Reference and Rating Data Search menus";

          try {
               await home.appclickOnNavigationList("Reference");

               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Click Reference", async () => {

          let stepAction = "Click Reference";
          let stepData = "";
          let stepExpResult = "Menu item should expand and display Channels, Markets, Demographics, Universe Estimate";
          let stepActualResult = "Menu item should expand and display Channels, Markets, Demographics, Universe Estimate";

          try {
               //await home.appclickOnMenuListLeftPanel("Reference Data");
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Click Markets", async () => {

          let stepAction = "Click Markets";
          let stepData = "";
          let stepExpResult = "Page should redirect market reference page and display Source drop down with Nielsen by Default";
          let stepActualResult = "Page should redirect market reference page and display Source drop down with Nielsen by Default";

          try {
               // await home.appclickOnMenuListLeftPanel("Markets");
               // await home.appclickOnMenuListLeftPanel("Reference Data");
                await home.appclickOnMenuListLeftPanel("Markets");
              // await home.appclickOnMenuListLeftPanel("Channels");
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });

      // --------------Test Step------------
      it("Select Comscore from the dropdown", async () => {

        let stepAction = "Select Comscore from the dropdown";
        let stepData = "";
        let stepExpResult = "1. Should display  Comscore related market reference data based on Authorization/access to market 2. Data should be shown in Grid view";
        let stepActualResult = "1. Should display  Comscore related market reference data based on Authorization/access to market 2. Data should be shown in Grid view";

        try {
            await channel.clickComponentPdropdown("Source");
            await channel.selectOptionsFromComponentPdropdown("Comscore");
            //await programTracksPage.clickComponentPdropdown("xgc-channel-market");
            //await programTracksPage.selectOptionsFromComponentPdropdown("Pittsburgh");
             report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
             report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

   });

     // --------------Test Step------------
     it("Check the availability of filter to the market code.", async () => {

          let stepAction = "Check the availability of filter to the market code.";
          let stepData = "";
          let stepExpResult = "Filter option should be available for the market code column";
          let stepActualResult = "Filter option should be available for the market code column";

          try {
               await action.Click(channel.SearchIcon, 'Click on channel')
               await channel.sendtext(120);


               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displaying in the record.", async () => {

          let stepAction = "Check the data displaying in the record.";
          let stepData = "Enter data that is part of/ Matching to market code";
          let stepExpResult = "Matching data to the market code should be displayed.";
          let stepActualResult = "Matching data to the market code should be displayed.";

          try {

               await action.Click(channel.cancelIcon, 'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the filter with multiple variables", async () => {

          let stepAction = "Check the filter with multiple variables";
          let stepData = "Enter 2 strings in the filter and search";
          let stepExpResult = "Data matching to the both strings should be displayed.";
          let stepActualResult = "Data matching to the both strings should be displayed.";

          try {

               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the user can filter a data range for variable using X and Y parameters:", async () => {

          let stepAction = "Check the user can filter a data range for variable using X and Y parameters:";
          let stepData = "Enter variables Greater than X";
          let stepExpResult = "Values greater than x should be displayed in the grid";
          let stepActualResult = "Values greater than x should be displayed in the grid";

          try {

               await channel.clickongreaterthan();
               await channel.sendtext(120);
               await channel.verifygreaterthenText();
               await action.Click(channel.cancelIcon, 'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Less than X", async () => {

          let stepAction = "Less than X";
          let stepData = "Enter variables Less Than X";
          let stepExpResult = "Values less than X should be displayed in the output.";
          let stepActualResult = "Values less than X should be displayed in the output.";

          try {
               await channel.clickonequalto();
               await channel.sendtext(120);
               await channel.verifyLessthenText();
               await action.Click(channel.cancelIcon, 'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Equal to X", async () => {

          let stepAction = "Equal to X";
          let stepData = "Enter the value equal to X";
          let stepExpResult = "Output equal to x values should be displayed.";
          let stepActualResult = "Output equal to x values should be displayed.";

          try {
               await channel.sendtext(120)

               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Between X and Y", async () => {

          let stepAction = "Between X and Y";
          let stepData = "Enter the variables between X and Y";
          let stepExpResult = "'The values that contains between X and Y should be displayed.";
          let stepActualResult = "'The values that contains between X and Y should be displayed.";

          try {
               await action.Click(channel.cancelIcon, 'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Not Equal to X", async () => {

          let stepAction = "Not Equal to X";
          let stepData = "Enter the variable do not equal to X";
          let stepExpResult = "Values other than the entered variable should be displayed in the output.";
          let stepActualResult = "Values other than the entered variable should be displayed in the output.";

          try {
               await channel.clickonNotequalto();

               // await channel.verifyNOTthenText();

               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displayed in the grid", async () => {

          let stepAction = "Check the data displayed in the grid";
          let stepData = "";
          let stepExpResult = "Data displayed in the grid should be displayed read only.";
          let stepActualResult = "Data displayed in the grid should be displayed read only.";

          try {
               await channel.sendtext(120);

               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the notification of records for user", async () => {

          let stepAction = "Check the notification of records for user";
          let stepData = "";
          let stepExpResult = "'User should be provided with a notification in the grid, ";
          let stepActualResult = "'User should be provided with a notification in the grid, ";

          try {
               await action.Click(channel.cancelIcon, 'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check reset option", async () => {

          let stepAction = "Check reset option";
          let stepData = "Reset the data after applying filter";
          let stepExpResult = "After reset, data from the filter should be removed default data should be displayed";
          let stepActualResult = "After reset, data from the filter should be removed default data should be displayed";

          try {
               await action.Click(channel.resetButton, 'click reset')
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the availability of filter to the market rank.", async () => {

          let stepAction = "Check the availability of filter to the market rank.";
          let stepData = "";
          let stepExpResult = "Filter option should be available for the market rank column";
          let stepActualResult = "Filter option should be available for the market rank column";

          try {
               await action.Click(channel.mKRankIcon, 'Click on channel')
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displaying in the record.", async () => {

          let stepAction = "Check the data displaying in the record.";
          let stepData = "Enter data that is part of/ Matching to market rank";
          let stepExpResult = "Matching data to the market rank should be displayed.";
          let stepActualResult = "Matching data to the market rank should be displayed.";

          try {
               await action.SetText(channel.mKRanksendID, '120', 'clickOntext')

               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the filter with multiple variables", async () => {

          let stepAction = "Check the filter with multiple variables";
          let stepData = "Enter 2 strings in the filter and search";
          let stepExpResult = "Data matching to the both strings should be displayed.";
          let stepActualResult = "Data matching to the both strings should be displayed.";

          try {
               await action.Click(channel.mKRankIcon, 'click on it');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the user can filter a data range for variable using X and Y parameters:", async () => {

          let stepAction = "Check the user can filter a data range for variable using X and Y parameters:";
          let stepData = "Enter variables Greater than X";
          let stepExpResult = "Values greater than x should be displayed in the grid";
          let stepActualResult = "Values greater than x should be displayed in the grid";

          try {
               await channel.clickonmKgreaterthan();
               await action.SetText(channel.mKRanksendID, '120', '')
               //  await channel.verifygreaterthenText();
               await action.Click(channel.mKCancelIcon, 'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Less than X", async () => {

          let stepAction = "Less than X";
          let stepData = "Enter variables Less Than X";
          let stepExpResult = "Values less than X should be displayed in the output.";
          let stepActualResult = "Values less than X should be displayed in the output.";

          try {
               await channel.clickonMklessthan();
               await action.SetText(channel.mKRanksendID, '120', '')
               //  await channel.verifyLessthenText();
               await action.Click(channel.mKCancelIcon, 'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Equal to X", async () => {

          let stepAction = "Equal to X";
          let stepData = "Enter the value equal to X";
          let stepExpResult = "Output equal to x values should be displayed.";
          let stepActualResult = "Output equal to x values should be displayed.";

          try {
               await action.SetText(channel.mKRanksendID, '120', '')

               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Between X and Y", async () => {

          let stepAction = "Between X and Y";
          let stepData = "Enter the variables between X and Y";
          let stepExpResult = "'The values that contains between X and Y should be displayed.";
          let stepActualResult = "'The values that contains between X and Y should be displayed.";

          try {
               await action.Click(channel.mKCancelIcon, 'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Not Equal to X", async () => {

          let stepAction = "Not Equal to X";
          let stepData = "Enter the variable do not equal to X";
          let stepExpResult = "Values other than the entered variable should be displayed in the output.";
          let stepActualResult = "Values other than the entered variable should be displayed in the output.";

          try {
               await channel.clickonMkNotequalto()
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displayed in the grid", async () => {

          let stepAction = "Check the data displayed in the grid";
          let stepData = "";
          let stepExpResult = "Data displayed in the grid should be displayed read only.";
          let stepActualResult = "Data displayed in the grid should be displayed read only.";

          try {

               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the notification of records for user", async () => {

          let stepAction = "Check the notification of records for user";
          let stepData = "";
          let stepExpResult = "'User should be provided with a notification in the grid, ";
          let stepActualResult = "'User should be provided with a notification in the grid, ";

          try {

               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check reset option", async () => {

          let stepAction = "Check reset option";
          let stepData = "Reset the data after applying filter";
          let stepExpResult = "After reset, data from the filter should be removed default data should be displayed";
          let stepActualResult = "After reset, data from the filter should be removed default data should be displayed";

          try {
               await action.Click(channel.resetButton, 'click reset')
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the availability of filter to the market name.", async () => {

          let stepAction = "Check the availability of filter to the market name.";
          let stepData = "";
          let stepExpResult = "Filter option should be available for the market name";
          let stepActualResult = "Filter option should be available for the market name";

          try {
               await action.Click(channel.SearchIcon, 'Click on channel')
               await channel.sendtext(120)
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displaying in the record.", async () => {

          let stepAction = "Check the data displaying in the record.";
          let stepData = "Enter data that is part of/ Matching to market code";
          let stepExpResult = "Matching data to the market code should be displayed.";
          let stepActualResult = "Matching data to the market code should be displayed.";

          try {
               await action.Click(channel.cancelIcon, 'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the filter with multiple variables", async () => {

          let stepAction = "Check the filter with multiple variables";
          let stepData = "Enter 2 strings in the filter and search";
          let stepExpResult = "Data matching to the both strings should be displayed.";
          let stepActualResult = "Data matching to the both strings should be displayed.";

          try {
               await channel.clickongreaterthan();
               await channel.sendtext(120);
               await channel.verifygreaterthenText();
               await action.Click(channel.cancelIcon, 'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displayed in the grid", async () => {

          let stepAction = "Check the data displayed in the grid";
          let stepData = "";
          let stepExpResult = "Data displayed in the grid should be displayed read only.";
          let stepActualResult = "Data displayed in the grid should be displayed read only.";

          try {
               await channel.clickonequalto();
               await channel.sendtext(120);
               await channel.verifyLessthenText();
               await action.Click(channel.cancelIcon, 'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the notification of records for user", async () => {

          let stepAction = "Check the notification of records for user";
          let stepData = "";
          let stepExpResult = "'User should be provided with a notification in the grid, ";
          let stepActualResult = "'User should be provided with a notification in the grid, ";

          try {
               await channel.sendtext(120)
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check reset option", async () => {

          let stepAction = "Check reset option";
          let stepData = "Reset the data after applying filter";
          let stepExpResult = "After reset, data from the filter should be removed default data should be displayed";
          let stepActualResult = "After reset, data from the filter should be removed default data should be displayed";

          try {
               await action.Click(channel.resetButton, 'click reset')
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the availability of filter to the market indicator", async () => {

          let stepAction = "Check the availability of filter to the market indicator";
          let stepData = "";
          let stepExpResult = "Filter option should be available for the market indicator";
          let stepActualResult = "Filter option should be available for the market indicator";

          try {
               await action.Click(channel.mKRankIcon, 'Click on channel')
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displaying in the record.", async () => {

          let stepAction = "Check the data displaying in the record.";
          let stepData = "Enter data that is part of/ Matching to market indicator";
          let stepExpResult = "Matching data to the market indicator should be displayed in the grid";
          let stepActualResult = "Matching data to the market indicator should be displayed in the grid";

          try {
               await action.SetText(channel.mKRanksendID, '120', 'clickOntext')
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the filter with multiple variables", async () => {

          let stepAction = "Check the filter with multiple variables";
          let stepData = "Enter 2 strings in the filter and search";
          let stepExpResult = "Data matching to the both strings should be displayed.";
          let stepActualResult = "Data matching to the both strings should be displayed.";

          try {
               await action.Click(channel.mKRankIcon, 'click on it');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displayed in the grid", async () => {

          let stepAction = "Check the data displayed in the grid";
          let stepData = "";
          let stepExpResult = "Data displayed in the grid should be displayed read only.";
          let stepActualResult = "Data displayed in the grid should be displayed read only.";

          try {
               await channel.clickonmKgreaterthan();
               await action.SetText(channel.mKRanksendID, '120', '')
               //  await channel.verifygreaterthenText();
               await action.Click(channel.mKCancelIcon, 'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the notification of records for user", async () => {

          let stepAction = "Check the notification of records for user";
          let stepData = "";
          let stepExpResult = "'User should be provided with a notification in the grid, ";
          let stepActualResult = "'User should be provided with a notification in the grid, ";

          try {

               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check reset option", async () => {

          let stepAction = "Check reset option";
          let stepData = "Reset the data after applying filter";
          let stepExpResult = "After reset, data from the filter should be removed default data should be displayed";
          let stepActualResult = "After reset, data from the filter should be removed default data should be displayed";

          try {
               await action.Click(channel.resetButton, 'click reset')
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the availability of filter to the collection method data variable", async () => {

          let stepAction = "Check the availability of filter to the collection method data variable";
          let stepData = "";
          let stepExpResult = "Filter option should be available for the Collection method data variable";
          let stepActualResult = "Filter option should be available for the Collection method data variable";

          try {
               await action.Click(channel.SearchIcon, 'Click on channel')
               await channel.sendtext(120);


               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displaying in the record.", async () => {

          let stepAction = "Check the data displaying in the record.";
          let stepData = "Enter data that is part of/ Matching to Collection method data variable";
          let stepExpResult = "Matching data to the collection method data variable should be displayed.";
          let stepActualResult = "Matching data to the collection method data variable should be displayed.";

          try {
               await action.Click(channel.cancelIcon, 'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the filter with multiple variables", async () => {

          let stepAction = "Check the filter with multiple variables";
          let stepData = "Enter 2 strings in the filter and search";
          let stepExpResult = "Data matching to the both strings should be displayed.";
          let stepActualResult = "Data matching to the both strings should be displayed.";

          try {
               await channel.clickongreaterthan();
               await channel.sendtext(120);
               await channel.verifygreaterthenText();
               await action.Click(channel.cancelIcon, 'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displayed in the grid", async () => {

          let stepAction = "Check the data displayed in the grid";
          let stepData = "";
          let stepExpResult = "Data displayed in the grid should be displayed read only.";
          let stepActualResult = "Data displayed in the grid should be displayed read only.";

          try {
               await channel.clickonequalto();
               await channel.sendtext(120);
               // await channel.verifyLessthenText();
               await action.Click(channel.cancelIcon, 'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the notification of records for user", async () => {

          let stepAction = "Check the notification of records for user";
          let stepData = "";
          let stepExpResult = "'User should be provided with a notification in the grid, ";
          let stepActualResult = "'User should be provided with a notification in the grid, ";

          try {
               await channel.sendtext(120)
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check reset option", async () => {

          let stepAction = "Check reset option";
          let stepData = "Reset the data after applying filter";
          let stepExpResult = "After reset, data from the filter should be removed default data should be displayed";
          let stepActualResult = "After reset, data from the filter should be removed default data should be displayed";

          try {
               await action.Click(channel.resetButton, 'click reset')

               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the availability of filter to the Time zone data variable", async () => {

          let stepAction = "Check the availability of filter to the Time zone data variable";
          let stepData = "";
          let stepExpResult = "Filter option should be available for the Time Zone data variable";
          let stepActualResult = "Filter option should be available for the Time Zone data variable";

          try {
               //await action.Click(channel.cancelIcon,'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displaying in the record.", async () => {

          let stepAction = "Check the data displaying in the record.";
          let stepData = "Enter data that is part of/ Matching to Time zone data variable";
          let stepExpResult = "Matching data to the Time zone data variable should be displayed.";
          let stepActualResult = "Matching data to the Time zone data variable should be displayed.";

          try {
               await channel.clickonNotequalto();
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the filter with multiple variables", async () => {

          let stepAction = "Check the filter with multiple variables";
          let stepData = "Enter 2 strings in the filter and search";
          let stepExpResult = "Data matching to the both strings should be displayed.";
          let stepActualResult = "Data matching to the both strings should be displayed.";

          try {
               await channel.sendtext(120);
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displayed in the grid", async () => {

          let stepAction = "Check the data displayed in the grid";
          let stepData = "";
          let stepExpResult = "Data displayed in the grid should be displayed read only.";
          let stepActualResult = "Data displayed in the grid should be displayed read only.";

          try {
               await action.Click(channel.cancelIcon, 'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the notification of records for user", async () => {

          let stepAction = "Check the notification of records for user";
          let stepData = "";
          let stepExpResult = "'User should be provided with a notification in the grid, ";
          let stepActualResult = "'User should be provided with a notification in the grid, ";

          try {

               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check reset option", async () => {

          let stepAction = "Check reset option";
          let stepData = "Reset the data after applying filter";
          let stepExpResult = "After reset, data from the filter should be removed default data should be displayed";
          let stepActualResult = "After reset, data from the filter should be removed default data should be displayed";

          try {
               await action.Click(channel.resetButton, 'click reset')

               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the availability of filter to the Daylight Saving Indicator.", async () => {

          let stepAction = "Check the availability of filter to the Daylight Saving Indicator.";
          let stepData = "";
          let stepExpResult = "Filter option should be available for the Daylight Saving Indicator";
          let stepActualResult = "Filter option should be available for the Daylight Saving Indicator";

          try {
               await action.Click(channel.SearchIcon, 'Click on channel')
               await channel.sendtext(120);
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displaying in the record.", async () => {

          let stepAction = "Check the data displaying in the record.";
          let stepData = "Enter data that is part of/ Matching to market code";
          let stepExpResult = "Matching data to the market code should be displayed.";
          let stepActualResult = "Matching data to the market code should be displayed.";

          try {
               await action.Click(channel.cancelIcon, 'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displayed in the grid", async () => {

          let stepAction = "Check the data displayed in the grid";
          let stepData = "";
          let stepExpResult = "Data displayed in the grid should be displayed read only.";
          let stepActualResult = "Data displayed in the grid should be displayed read only.";

          try {
               // await channel.clickongreaterthan();
               // await channel.sendtext(120);
               // await channel.verifygreaterthenText();
               // await action.Click(channel.cancelIcon,'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the notification of records for user", async () => {

          let stepAction = "Check the notification of records for user";
          let stepData = "";
          let stepExpResult = "'User should be provided with a notification in the grid, ";
          let stepActualResult = "'User should be provided with a notification in the grid, ";

          try {
               // await channel.clickonequalto();
               // await channel.sendtext(120);
               // await channel.verifyLessthenText();
               // await action.Click(channel.cancelIcon,'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check reset option", async () => {

          let stepAction = "Check reset option";
          let stepData = "Reset the data after applying filter";
          let stepExpResult = "After reset, data from the filter should be removed default data should be displayed";
          let stepActualResult = "After reset, data from the filter should be removed default data should be displayed";

          try {
               await action.Click(channel.resetButton, 'click reset')
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the availability of filter to the Class code", async () => {

          let stepAction = "Check the availability of filter to the Class code";
          let stepData = "";
          let stepExpResult = "Filter option should be available for the Class Code";
          let stepActualResult = "Filter option should be available for the Class Code";

          try {

               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displaying in the record.", async () => {

          let stepAction = "Check the data displaying in the record.";
          let stepData = "Enter data that is part of/ Matching to Class code";
          let stepExpResult = "Matching data to the Class code should be displayed.";
          let stepActualResult = "Matching data to the Class code should be displayed.";

          try {
               // await action.Click(channel.mKRankIcon,'Click on channel')
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the filter with multiple variables", async () => {

          let stepAction = "Check the filter with multiple variables";
          let stepData = "Enter 2 strings in the filter and search";
          let stepExpResult = "Data matching to the both strings should be displayed.";
          let stepActualResult = "Data matching to the both strings should be displayed.";

          try {
               //     await action.SetText(channel.mKRanksendID,'120','clickOntext')
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displayed in the grid", async () => {

          let stepAction = "Check the data displayed in the grid";
          let stepData = "";
          let stepExpResult = "Data displayed in the grid should be displayed read only.";
          let stepActualResult = "Data displayed in the grid should be displayed read only.";

          try {
               // await action.Click(channel.mKRankIcon,'click on it');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the notification of records for user", async () => {

          let stepAction = "Check the notification of records for user";
          let stepData = "";
          let stepExpResult = "'User should be provided with a notification in the grid, ";
          let stepActualResult = "'User should be provided with a notification in the grid, ";

          try {

               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check reset option", async () => {

          let stepAction = "Check reset option";
          let stepData = "Reset the data after applying filter";
          let stepExpResult = "After reset, data from the filter should be removed default data should be displayed";
          let stepActualResult = "After reset, data from the filter should be removed default data should be displayed";

          try {
               // await action.Click(channel.resetButton,'click reset')
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the availability of filter to the Last Updated", async () => {

          let stepAction = "Check the availability of filter to the Last Updated";
          let stepData = "";
          let stepExpResult = "Filter option should be available for the Last Updated";
          let stepActualResult = "Filter option should be available for the Last Updated";

          try {
               // await action.Click(channel.SearchIcon,'Click on channel')
               // await channel.sendtext(120)
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displaying in the record.", async () => {

          let stepAction = "Check the data displaying in the record.";
          let stepData = "Enter data that is part of/ Matching to Last Updated";
          let stepExpResult = "Matching data to the Last Updated should be displayed.";
          let stepActualResult = "Matching data to the Last Updated should be displayed.";

          try {
               // await action.Click(channel.cancelIcon,'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the filter with multiple variables", async () => {

          let stepAction = "Check the filter with multiple variables";
          let stepData = "Enter 2 strings in the filter and search";
          let stepExpResult = "Data matching to the both strings should be displayed.";
          let stepActualResult = "Data matching to the both strings should be displayed.";

          try {
               // await channel.clickongreaterthan();
               // await channel.sendtext(120);
               // await channel.verifygreaterthenText();
               // await action.Click(channel.cancelIcon,'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the data displayed in the grid", async () => {

          let stepAction = "Check the data displayed in the grid";
          let stepData = "";
          let stepExpResult = "Data displayed in the grid should be displayed read only.";
          let stepActualResult = "Data displayed in the grid should be displayed read only.";

          try {
               // await channel.clickonequalto();
               // await channel.sendtext(120);
               // await channel.verifyLessthenText();
               // await action.Click(channel.cancelIcon,'cancel button');
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check the notification of records for user", async () => {

          let stepAction = "Check the notification of records for user";
          let stepData = "";
          let stepExpResult = "'User should be provided with a notification in the grid, ";
          let stepActualResult = "'User should be provided with a notification in the grid, ";

          try {

               await channel.sendtext(120)
               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


     // --------------Test Step------------
     it("Check reset option", async () => {

          let stepAction = "Check reset option";
          let stepData = "Reset the data after applying filter";
          let stepExpResult = "After reset, data from the filter should be removed default data should be displayed";
          let stepActualResult = "After reset, data from the filter should be removed default data should be displayed";

          try {
               await action.Click(channel.resetButton, 'click reset')

               report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
          }

          catch (err) {
               report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
          }

     });


});
