/*
***********************************************************************************************************
* @Script Name :  XGCT-8555
* @Description :  Rating Market Reference Grid - Verify Search functionality (Nielsen)12-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 12-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { AppCommonFunctions } from '../../../../../Libs/ApplicationLibs/CommAppLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { Markets } from '../../../../../Pages/Reporting/ReferenceData/Markets'

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let appCommFunction = new AppCommonFunctions();
let homePage = new HomePageFunction();
let marketRef = new Markets();
let localLength = new LocalLengthFactorsPricing();

//Variables Declaration
let TestCase_ID = 'XGCT-8555'
let TestCase_Title = 'Rating Market Reference Grid - Verify Search functionality (Nielsen)'
let contains = "Contains";
let Market = "Markets", Channels = "Channels", Demograp = "Demographics", universe = "Universe Estimate";
let columnGrid = ["Market Code", "Market Rank", "Market Name", "Market Indicator", "Collection Method", "Time Zone", "Daylight Saving Indicator", "Class Code", "Last Updated"];
let equals = "EQUAL";
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('RATING MARKET REFERENCE GRID - VERIFY SEARCH FUNCTIONALITY (NIELSEN)', () => {

    // --------------Test Step 1------------
    it("Open browser and enter CHASSIS URL", async () => {

        let stepAction = "Open browser and enter CHASSIS URL";
        let stepData = "https://xgplatform-dev.myimagine.com/";
        let stepExpResult = "Page is loaded successfully.";
        let stepActualResult = "Page is loaded successfully.";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    // --------------Test Step 2 ------------
    it("Click Reference menu item", async () => {

        let stepAction = "Click Reference menu item";
        let stepData = "";
        let stepExpResult = "Left navigation should display Reference menus";
        let stepActualResult = "Left navigation displayed Reference menus";
        let refernceMenu = "Reference";
        try {
            await homePage.appclickOnNavigationList(refernceMenu);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 3------------
    it("Click Reference", async () => {

        let stepAction = "Click Reference";
        let stepData = "";
        let stepExpResult = "Menu item should expand and display Channels, Markets, Demographics, Universe Estimate";
        let stepActualResult = "Menu item should expand and display Channels, Markets, Demographics, Universe Estimate";
        let referenceData = "Reference Data";


        try {
            // await homePage.appclickOnMenuListLeftPanel(referenceData);
            await homePage.verifyMenuListonLeftPanel(Market);
            await homePage.verifyMenuListonLeftPanel(Channels);
            await homePage.verifyMenuListonLeftPanel(Demograp);
            await homePage.verifyMenuListonLeftPanel(universe);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    // --------------Test Step 4 ------------
    it("Click Markets", async () => {

        let stepAction = "Click Markets";
        let stepData = "";
        let stepExpResult = "Page should redirect to market reference page and display Source drop down with Nielsen by Default";
        let stepActualResult = "Page redirected to market reference page and displayed Source drop down with Nielsen by Default";
        let Nielsen = "Nielsen";
        try {
            await homePage.appclickOnMenuListLeftPanel(Market);
            await verify.verifyElementIsDisplayed(appCommFunction.pageHeader, " Market Reference");
            await browser.sleep(1000);
            verify.verifyTextOfElement(marketRef.sourceDropDownSelectedValue, Nielsen, " Source Default Value");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 5------------
    it("Review column header row in the grid", async () => {

        let stepAction = "Review column header row in the grid";
        let stepData = "";
        let stepExpResult = "Should have below columns :Market Code,Market Rank, Market Name,Market Indicator, Collection Method,Time Zone, Daylight Saving Indicator,Class Code, Last Updated";
        let stepActualResult = "Should had below columns :Market Code,Market Rank, Market Name,Market Indicator, Collection Method,Time Zone, Daylight Saving Indicator,Class Code, Last Updated";

        try {
            appCommFunction.validateHeaders(columnGrid);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    //--------------Test Step 6------------
    it("Review search box under all the column headers", async () => {

        let stepAction = "Review search box under all the column headers";
        let stepData = "";
        let stepExpResult = "Search box should be displayed under each column header name.";
        let stepActualResult = "Search box are displayed under each column header name.";

        try {
            appCommFunction.validateSearchBoxForTableHeaders();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 7------------
    it("Review total records count message display", async () => {

        let stepAction = "Review total records count message display";
        let stepData = "";
        let stepExpResult = "Following messages should be displayed as 'There are X records in this dataset.' & 'X records selected'''";
        let stepActualResult = "Following messages are displayed as 'There are X records in this dataset.' & 'X records selected'''";

        try {
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    // --------------Test Step 8------------
    // it("Keep cursor focus on search box under column Market Code'.", async () => {

    //     let stepAction = "Keep cursor focus on search box under column Market Code'.";
    //     let stepData = "";
    //     let stepExpResult = "Cursor should be focused.";
    //     let stepActualResult = "Cursor should be focused.";
    //     try {
    //         report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    //     }
    //     catch (err) {
    //         report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    //     }
    // });


    // --------------Test Step 8 & 9------------
    it("Enter valid full market code in search field", async () => {

        let stepAction = "Enter valid full market code in search field";
        let stepData = "EX:  115";
        let stepExpResult = "Matching record should be fetched successfully.";
        let stepActualResult = "Matching record are fetched successfully.";
        let actual = "100"
        try {
            await browser.sleep(3000);
            await marketRef.mouseMoveToHeader(columnGrid[0]);
            await localLength.gridFilter(columnGrid[0], actual);
            await localLength.gridDataValidation(columnGrid[0], actual, equals);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    // --------------Test Step 10------------
    it("Review the table filter record count message", async () => {

        let stepAction = "Review the table filter record count message";
        let stepData = "";
        let stepExpResult = "Message should be displayed as follows There are X records in this dataset.' & 'X records selected' & 'X records filtered from X records'";
        let stepActualResult = "Message is displayed as follows There are X records in this dataset.' & 'X records selected' & 'X records filtered from X records'";

        try {
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 11------------
    it("Clear search field and enter non-existent market code # in the search field.", async () => {

        let stepAction = "Clear search field and enter non-existent market code # in the search field.";
        let stepData = "Test data: [EX] 9999999";
        let stepExpResult = "Following message 'No data match is found' should be displayed.'";
        let stepActualResult = "Following message 'No data match is found' is displayed.'";
        let roughValue = "99999999";
        try {
            await localLength.gridFilter(columnGrid[0], roughValue);
            await localLength.verifyNodataFoundInGrid();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 12------------
    it("Click grid reset option", async () => {

        let stepAction = "Click grid reset option";
        let stepData = "";
        let stepExpResult = "Grid data should be refreshed.";
        let stepActualResult = "Grid data is refreshed.";
        try {
            await action.Click(marketRef.buttonReset, " Grid Reset Button");
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    // // --------------Test Step 13------------
    // it("Keep cursor focus on search box under column Market Rank'.", async () => {

    //     let stepAction = "Keep cursor focus on search box under column Market Rank'.";
    //     let stepData = "";
    //     let stepExpResult = "Cursor should be focused.";
    //     let stepActualResult = "Cursor should be focused.";
    //     try {

    //         report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    //     }
    //     catch (err) {
    //         report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    //     }
    // });


    // --------------Test Step 14 & 15------------
    it("Enter valid full market rank in search field", async () => {

        let stepAction = "Enter valid full market rank in search field";
        let stepData = "EX:  154";
        let stepExpResult = "Matching record should be fetched successfully.";
        let stepActualResult = "Matching record are fetched successfully.";
        let actualValue = "79";
        try {
            await browser.sleep(3000);
            await marketRef.mouseMoveToHeader(columnGrid[1]);
            await localLength.gridFilter(columnGrid[1], actualValue);
            await localLength.gridDataValidation(columnGrid[1], actualValue, equals);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 16------------
    it("Review the table filter record count message", async () => {

        let stepAction = "Review the table filter record count message";
        let stepData = "";
        let stepExpResult = "Message should be displayed as follows 'There are X records in this dataset.' & 'X records selected'' & 'X records filtered from X records'";
        let stepActualResult = "Message is displayed as follows 'There are X records in this dataset.' & 'X records selected'' & 'X records filtered from X records'";

        try {
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 17------------
    it("Clear search field and enter non-existent market rank in the search field.", async () => {

        let stepAction = "Clear search field and enter non-existent market rank in the search field.";
        let stepData = "Test data: [EX] 12531";
        let stepExpResult = "Following message 'No data match is found' should be displayed.'";
        let stepActualResult = "Following message 'No data match is found' is displayed.'";
        let roughValue = "99999999"
        try {
            await localLength.gridFilter(columnGrid[1], roughValue);
            await localLength.verifyNodataFoundInGrid();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 18------------
    it("Click grid reset option", async () => {

        let stepAction = "Click grid reset option";
        let stepData = "";
        let stepExpResult = "Grid data should be refreshed.";
        let stepActualResult = "Grid data is refreshed.";

        try {
            await action.Click(marketRef.buttonReset, " Grid Reset Button");
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // // --------------Test Step 19------------
    // it("Keep cursor focus on search box under column Market Name'.", async () => {

    //     let stepAction = "Keep cursor focus on search box under column Market Name'.";
    //     let stepData = "";
    //     let stepExpResult = "Cursor should be focused.";
    //     let stepActualResult = "Cursor should be focused.";

    //     try {

    //         report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    //     }

    //     catch (err) {
    //         report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    //     }

    // });


    // --------------Test Step 19 & 20------------
    it("Enter valid full market name in search field", async () => {

        let stepAction = "Enter valid full market name in search field";
        let stepData = "EX: PITTSBURGH";
        let stepExpResult = "Matching record should be fetched successfully.";
        let stepActualResult = "Matching record are fetched successfully.";
        let actualVal = "Portland-Auburn"
        try {
            await browser.sleep(3000);
            await marketRef.mouseMoveToHeader(columnGrid[2]);
            await localLength.gridFilter(columnGrid[2], actualVal);
            await localLength.gridDataValidation(columnGrid[2], actualVal, contains);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 21------------
    it("Review the table filter record count message", async () => {

        let stepAction = "Review the table filter record count message";
        let stepData = "";
        let stepExpResult = "Message should be displayed as follows 'There are X records in this dataset.' & 'X records selected'' & 'X records filtered from X records'";
        let stepActualResult = "Message is displayed as follows 'There are X records in this dataset.' & 'X records selected'' & 'X records filtered from X records'";

        try {
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 22------------
    it("Clear search field and enter partial market name in the search field.", async () => {

        let stepAction = "Clear search field and enter partial market name in the search field.";
        let stepData = "EX: PITTS";
        let stepExpResult = "Matching record should be fetched successfully.";
        let stepActualResult = "Matching record are fetched successfully.";
        let val = "Portland-Auburn"
        let roughValue = "Auburn"
        try {
            await localLength.gridFilter(columnGrid[2], roughValue);
            await localLength.gridDataValidation(columnGrid[2], val, contains);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 23------------
    it("Review the table filter record count message", async () => {

        let stepAction = "Review the table filter record count message";
        let stepData = "";
        let stepExpResult = "Message should be displayed as follows 'There are X records in this dataset.' & 'X records selected'' & 'X records filtered from X records'";
        let stepActualResult = "Message is displayed as follows 'There are X records in this dataset.' & 'X records selected'' & 'X records filtered from X records'";

        try {
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 24------------
    it("Clear search field and enter non-existent market name in the search field.", async () => {

        let stepAction = "Clear search field and enter non-existent market name in the search field.";
        let stepData = "Test data: [EX] GREENWOOD";
        let stepExpResult = "Following message 'No data match is found' should be displayed.'";
        let stepActualResult = "Following message 'No data match is found' is displayed.'";
        let dup = "Aburn"
        try {
            await localLength.gridFilter(columnGrid[2], dup);
            await localLength.verifyNodataFoundInGrid();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 25------------
    it("Click grid reset option", async () => {

        let stepAction = "Click grid reset option";
        let stepData = "";
        let stepExpResult = "Grid data should be refreshed.";
        let stepActualResult = "Grid data is refreshed.";

        try {
            await action.Click(marketRef.buttonReset, " Grid Reset Button");
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // // --------------Test Step 26------------
    // it("Keep cursor focus on search box under column Market Indicator'.", async () => {

    //     let stepAction = "Keep cursor focus on search box under column Market Indicator'.";
    //     let stepData = "";
    //     let stepExpResult = "Cursor should be focused.";
    //     let stepActualResult = "Cursor should be focused.";

    //     try {

    //         report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    //     }

    //     catch (err) {
    //         report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    //     }

    // });


    // --------------Test Step 26 & 27------------
    it("Enter valid full market indicator in search field", async () => {

        let stepAction = "Enter valid full market indicator in search field";
        let stepData = "EX: DMA";
        let stepExpResult = "Matching record should be fetched successfully.";
        let stepActualResult = "Matching record should be fetched successfully.";
        let val = "DMA";
        try {
            await browser.sleep(3000);
            await marketRef.mouseMoveToHeader(columnGrid[3]);
            await localLength.gridFilter(columnGrid[3], val);
            await localLength.gridDataValidation(columnGrid[3], val, contains);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 28------------
    it("Review the table filter record count message", async () => {

        let stepAction = "Review the table filter record count message";
        let stepData = "";
        let stepExpResult = "Message should be displayed as follows 'There are X records in this dataset.' & 'X records selected'' & 'X records filtered from X records'";
        let stepActualResult = "Message is displayed as follows 'There are X records in this dataset.' & 'X records selected'' & 'X records filtered from X records'";

        try {
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 29------------
    it("Clear search field and enter partial market indicator in the search field.", async () => {

        let stepAction = "Clear search field and enter partial market indicator in the search field.";
        let stepData = "EX: D";
        let stepExpResult = "Matching record should be fetched successfully.";
        let stepActualResult = "Matching record are fetched successfully.";
        let dup = "DM"
        try {
            await localLength.gridFilter(columnGrid[3], dup);
            await localLength.gridDataValidation(columnGrid[3], dup, contains);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 30------------
    it("Review the table filter record count message", async () => {

        let stepAction = "Review the table filter record count message";
        let stepData = "";
        let stepExpResult = "Message should be displayed as follows 'There are X records in this dataset.' & 'X records selected'' & 'X records filtered from X records'";
        let stepActualResult = "Message is displayed as follows 'There are X records in this dataset.' & 'X records selected'' & 'X records filtered from X records'";

        try {
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 31------------
    it("Clear search field and enter non-existent market indicator in the search field.", async () => {

        let stepAction = "Clear search field and enter non-existent market indicator in the search field.";
        let stepData = "Test data: [EX] HWC";
        let stepExpResult = "Following message 'No data match is found' should be displayed.'";
        let stepActualResult = "Following message 'No data match is found' is displayed.'";
        let roughValue = "HWC"
        try {
            await localLength.gridFilter(columnGrid[3], roughValue);
            await localLength.verifyNodataFoundInGrid();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 32------------
    it("Click grid reset option", async () => {

        let stepAction = "Click grid reset option";
        let stepData = "";
        let stepExpResult = "Grid data should be refreshed.";
        let stepActualResult = "Grid data is refreshed.";

        try {
            await action.Click(marketRef.buttonReset, " Grid Reset Button");
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // // --------------Test Step 33------------
    // it("Keep cursor focus on search box under column 'Collection Method'.", async () => {

    //     let stepAction = "Keep cursor focus on search box under column 'Collection Method'.";
    //     let stepData = "";
    //     let stepExpResult = "Cursor should be focused.";
    //     let stepActualResult = "Cursor should be focused.";

    //     try {

    //         report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    //     }

    //     catch (err) {
    //         report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    //     }

    // });


    // --------------Test Step 33 & 34------------
    it("Enter valid full Collection Method in search field", async () => {

        let stepAction = "Enter valid full Collection Method in search field";
        let stepData = "EX: LPM + PPM";
        let stepExpResult = "Matching record should be fetched successfully.";
        let stepActualResult = "Matching record are fetched successfully.";
        let val = "RPD Plus";
        try {
            await browser.sleep(3000);
            await marketRef.mouseMoveToHeader(columnGrid[4]);
            await localLength.gridFilter(columnGrid[4], val);
            await localLength.gridDataValidation(columnGrid[4], val, contains);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 35------------
    it("Review the table filter record count message", async () => {

        let stepAction = "Review the table filter record count message";
        let stepData = "";
        let stepExpResult = "Message should be displayed as follows 'There are X records in this dataset.' & 'X records selected'' & 'X records filtered from X records'";
        let stepActualResult = "Message is displayed as follows 'There are X records in this dataset.' & 'X records selected'' & 'X records filtered from X records'";

        try {
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 36------------
    it("Clear search field and enter partial Collection Method in the search field.", async () => {

        let stepAction = "Clear search field and enter partial Collection Method in the search field.";
        let stepData = "EX: LPM";
        let stepExpResult = "Matching record should be fetched successfully.";
        let stepActualResult = "Matching record should be fetched successfully.";

        try {
            await localLength.gridFilter(columnGrid[4], "RPD");
            await localLength.gridDataValidation(columnGrid[4], "RPD Plus", contains);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 37------------
    it("Review the table filter record count message", async () => {

        let stepAction = "Review the table filter record count message";
        let stepData = "";
        let stepExpResult = "Message should be displayed as follows 'There are X records in this dataset.' & 'X records selected'' & 'X records filtered from X records'";
        let stepActualResult = "Message should be displayed as follows 'There are X records in this dataset.' & 'X records selected'' & 'X records filtered from X records'";

        try {
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 38------------
    it("Clear search field and enter non-existent Collection Method in the search field.", async () => {

        let stepAction = "Clear search field and enter non-existent Collection Method in the search field.";
        let stepData = "Test data: [EX] CMLCMP";
        let stepExpResult = "Following message 'No data match is found' should be displayed.";
        let stepActualResult = "Following message 'No data match is found' is displayed.";
        let dup = "RDDD";
        try {
            await localLength.gridFilter(columnGrid[4], dup);
            await localLength.verifyNodataFoundInGrid();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 39------------
    it("Click grid reset option", async () => {

        let stepAction = "Click grid reset option";
        let stepData = "";
        let stepExpResult = "Grid data should be refreshed.";
        let stepActualResult = "Grid data is refreshed.";

        try {
            await action.Click(marketRef.buttonReset, " Grid Reset Button");
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // // --------------Test Step 40 & 41------------
    // it("Keep cursor focus on search box under column 'Time Zone'.", async () => {

    //     let stepAction = "Keep cursor focus on search box under column 'Time Zone'.";
    //     let stepData = "";
    //     let stepExpResult = "Cursor should be focused.";
    //     let stepActualResult = "Cursor should be focused.";

    //     try {

    //         report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    //     }

    //     catch (err) {
    //         report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    //     }

    // });


    // --------------Test Step 42------------
    it("Enter valid full Time Zone in search field", async () => {

        let stepAction = "Enter valid full Time Zone in search field";
        let stepData = "EX: Mountain";
        let stepExpResult = "Matching record should be fetched successfully.";
        let stepActualResult = "Matching record should be fetched successfully.";
        let val = "Eastern"
        try {
            await browser.sleep(3000);
            await marketRef.mouseMoveToHeader(columnGrid[6]);
            await localLength.gridFilter(columnGrid[5], val);
            await localLength.gridDataValidation(columnGrid[5], val, contains);

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 43------------
    it("Review the table filter record count message", async () => {

        let stepAction = "Review the table filter record count message";
        let stepData = "";
        let stepExpResult = "Message should be displayed as follows There are X records in this dataset.' & 'X records selected' & 'X records filtered from X records'";
        let stepActualResult = "Message is displayed as follows There are X records in this dataset.' & 'X records selected' & 'X records filtered from X records'";

        try {
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 44------------
    it("Clear search field and enter partial Time Zone in the search field.", async () => {

        let stepAction = "Clear search field and enter partial Time Zone in the search field.";
        let stepData = "EX: Mount";
        let stepExpResult = "Matching record should be fetched successfully.";
        let stepActualResult = "Matching record are fetched successfully.";
        let actual = "tern";
        try {
            await localLength.gridFilter(columnGrid[5], actual);
            await localLength.gridDataValidation(columnGrid[5], "Eastern", contains);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 45------------
    it("Review the table filter record count message", async () => {

        let stepAction = "Review the table filter record count message";
        let stepData = "";
        let stepExpResult = "Message should be displayed as follows There are X records in this dataset.' & 'X records selected' & 'X records filtered from X records'";
        let stepActualResult = "Message is displayed as follows There are X records in this dataset.' & 'X records selected' & 'X records filtered from X records'";

        try {
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 46------------
    it("Clear search field and enter non-existent Time Zone in the search field.", async () => {

        let stepAction = "Clear search field and enter non-existent Time Zone in the search field.";
        let stepData = "Test data: [EX:] Indian";
        let stepExpResult = "Following message 'No data match is found' should be displayed.'";
        let stepActualResult = "Following message 'No data match is found' is displayed.'";
        let dup = "Indian";
        try {
            await localLength.gridFilter(columnGrid[5], dup);
            await localLength.verifyNodataFoundInGrid();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 47------------
    it("Click grid reset option", async () => {

        let stepAction = "Click grid reset option";
        let stepData = "";
        let stepExpResult = "Grid data should be refreshed.";
        let stepActualResult = "Grid data is refreshed.";

        try {
            await action.Click(marketRef.buttonReset, " Grid Reset Button");
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // // --------------Test Step 48------------
    // it("Keep cursor focus on search box under column 'Daylight Saving Indicator'.", async () => {

    //     let stepAction = "Keep cursor focus on search box under column 'Daylight Saving Indicator'.";
    //     let stepData = "";
    //     let stepExpResult = "Cursor should be focused.";
    //     let stepActualResult = "Cursor should be focused.";

    //     try {

    //         report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    //     }

    //     catch (err) {
    //         report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    //     }

    // });


    // --------------Test Step 48 & 49------------
    it("Enter valid full Daylight Saving Indicator in search field", async () => {

        let stepAction = "Enter valid full Daylight Saving Indicator in search field";
        let stepData = "EX: Y or N";
        let stepExpResult = "Matching record should be fetched successfully.";
        let stepActualResult = "Matching record are fetched successfully.";
        try {
            await browser.sleep(3000);
            await marketRef.mouseMoveToHeader(columnGrid[6]);
            await localLength.gridFilter(columnGrid[6], "Y");
            await localLength.gridDataValidation(columnGrid[6], "Y", contains);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 50------------
    it("Review the table filter record count message", async () => {

        let stepAction = "Review the table filter record count message";
        let stepData = "";
        let stepExpResult = "Message should be displayed as follows There are X records in this dataset.' & 'X records selected' & 'X records filtered from X records'";
        let stepActualResult = "Message is displayed as follows There are X records in this dataset.' & 'X records selected' & 'X records filtered from X records'";

        try {
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 51------------
    it("Clear search field and enter non-existent Daylight Saving Indicator in the search field.", async () => {

        let stepAction = "Clear search field and enter non-existent Daylight Saving Indicator in the search field.";
        let stepData = "Test data: [EX:] UT";
        let stepExpResult = "Following message 'No data match is found' should be displayed.";
        let stepActualResult = "Following message 'No data match is found' is displayed.";

        try {
            await localLength.gridFilter(columnGrid[6], "UT");
            await localLength.verifyNodataFoundInGrid();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 52------------
    it("Click grid reset option", async () => {

        let stepAction = "Click grid reset option";
        let stepData = "";
        let stepExpResult = "Grid data should be refreshed.";
        let stepActualResult = "Grid data is refreshed.";

        try {
            await action.Click(marketRef.buttonReset, " Grid Reset Button");
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // // --------------Test Step 53------------
    // it("Keep cursor focus on search box under column 'Class Code'.", async () => {

    //     let stepAction = "Keep cursor focus on search box under column 'Class Code'.";
    //     let stepData = "";
    //     let stepExpResult = "Cursor should be focused.";
    //     let stepActualResult = "Cursor should be focused.";

    //     try {

    //         report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    //     }

    //     catch (err) {
    //         report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    //     }

    // });


    // --------------Test Step 53 & 54------------
    it("Enter valid full Class Code in search field", async () => {

        let stepAction = "Enter valid full Class Code in search field";
        let stepData = "EX: A or D";
        let stepExpResult = "Matching record should be fetched successfully.";
        let stepActualResult = "Matching record is fetched successfully.";
        let headerName = "Class Code";
        let valueData = "A";
        try {
            await browser.sleep(3000);
            await marketRef.mouseMoveToHeader(headerName);
            await localLength.gridFilter(headerName, valueData);
            await localLength.gridDataValidation(headerName, valueData, contains);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 55------------
    it("Review the table filter record count message", async () => {

        let stepAction = "Review the table filter record count message";
        let stepData = "";
        let stepExpResult = "Message should be displayed as follows There are X records in this dataset.' & 'X records selected'' & 'X records filtered from X records'";
        let stepActualResult = "Message is displayed as follows There are X records in this dataset.' & 'X records selected'' & 'X records filtered from X records'";

        try {
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 56------------
    it("Clear search field and enter non-existent Class Code in the search field.", async () => {

        let stepAction = "Clear search field and enter non-existent Class Code in the search field.";
        let stepData = "Test data: [EX:] ABC";
        let stepExpResult = "Following message 'No data match is found' should be displayed.";
        let stepActualResult = "Following message 'No data match is found' is displayed.";
        let headerName = "Class Code";
        let value = "ABC";
        try {
            await localLength.gridFilter(headerName, value);
            await localLength.verifyNodataFoundInGrid();

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 57------------
    it("Click grid reset option", async () => {

        let stepAction = "Click grid reset option";
        let stepData = "";
        let stepExpResult = "Grid data should be refreshed.";
        let stepActualResult = "Grid data is refreshed.";

        try {
            await action.Click(marketRef.buttonReset, " Grid Reset Button");
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // // --------------Test Step 58------------
    // it("Keep cursor focus on search box under column 'Last Updated'.", async () => {

    //     let stepAction = "Keep cursor focus on search box under column 'Last Updated'.";
    //     let stepData = "";
    //     let stepExpResult = "Cursor should be focused.";
    //     let stepActualResult = "Cursor should be focused.";

    //     try {

    //         report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    //     }

    //     catch (err) {
    //         report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    //     }

    // });


    // --------------Test Step 58 & 59------------
    it("Enter valid full Last Updated in search field", async () => {

        let stepAction = "Enter valid full Last Updated in search field";
        let stepData = "EX: 05/31/2019 3:00 AM";
        let stepExpResult = "Matching record should be fetched successfully.";
        let stepActualResult = "Matching record should be fetched successfully.";
        let dateValue = "07/09/2019";
        let headerValue = "Last Updated";
        try {
            await browser.sleep(3000);
            await marketRef.mouseMoveToHeader(headerValue);
            await marketRef.enterDateIntoDateField(dateValue);
            await marketRef.verifyDateInDateField(dateValue);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 60------------
    it("Review the table filter record count message", async () => {

        let stepAction = "Review the table filter record count message";
        let stepData = "";
        let stepExpResult = "Message should be displayed as follows There are X records in this dataset. & 'X records selected' & 'X records filtered from X records'";
        let stepActualResult = "Message is displayed as follows There are X records in this dataset.' & 'X records selected' & 'X records filtered from X records'";

        try {
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 61------------
    it("Note: While doing this step, review filter record count message for each search'", async () => {

        let stepAction = "Note: While doing this step, review filter record count message for each search'";
        let stepData = "[Perform all below wildcard search:]";
        let stepExpResult = "Expected records should be filtered correctly based on the filter set and appropriate filter record count message should be displayed.";
        let stepActualResult = "Expected records are filtered correctly based on the filter set and appropriate filter record count message is displayed.";
        let dateValue = "07/09/2019";
        try {
            await marketRef.enterDateIntoDateField(dateValue);
            await marketRef.verifyDateInDateField(dateValue);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 62------------
    it("Clear search field and enter non-existent Last Updated in the search field.", async () => {

        let stepAction = "Clear search field and enter non-existent Last Updated in the search field.";
        let stepData = "Test data: [EX:] 6/4/2020 3:45 PM";
        let stepExpResult = "Following message 'No data match is found' should be displayed.";
        let stepActualResult = "Following message 'No data match is found' is displayed.";
        let dateValue = "07/09/2222";
        try {
            await marketRef.enterDateIntoDateField(dateValue);
            await localLength.verifyNodataFoundInGrid();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 63------------
    it("Click grid reset option", async () => {

        let stepAction = "Click grid reset option";
        let stepData = "";
        let stepExpResult = "Grid data should be refreshed.";
        let stepActualResult = "Grid data is refreshed.";

        try {
            await action.Click(marketRef.buttonReset, " Grid Reset Button");
            await marketRef.verifyTotalRecordsMessage();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
});
