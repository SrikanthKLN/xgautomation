/*
***********************************************************************************************************
* @Script Name : XGCT-9935_NetworkProgCreate_Comscore_TC
* @Description :  Test for Network Program flow - Smoke testing comscore 19-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 19-8-2019
* @Author : Vikas
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../Pages/Home/HomePage';
import { globalTestData } from '../../../../TestData/globalTestData';
import { localProgrammingSmoke } from '../../../../Pages/SMOKE-TESTS-PAGES/LocalProgrammingSmoke';
import { NetworkProgrammingSmoke } from '../../../../Pages/SMOKE-TESTS-PAGES/NetworkProgrammingSmoke';
import { Gutils } from '../../../../Utility/gutils';


//Import Class Objects Instantiation
let gUtils = new Gutils();
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let globalFunc = new globalvalues();
let HomePage = new HomePageFunction();


let localProgPage = new localProgrammingSmoke();
let networkProgram = new NetworkProgrammingSmoke();

//Variables Declaration
let currentDate = gUtils.getcurrentDate();
let endDate = gUtils.getcurrentDateAfterSomeDays(10);
let recordChild = {};
let pavAllRec = {};
let _rowcont: number;
let reqAPI = require("request");
let elementStatus: boolean;
let demoValue: string;
let sourceValue = "Comscore";
let distributorCodes = "5426";
let marketSource = "508";
let weeks = "Weeks=Week1&Weeks=Week2&Weeks=Week3&Weeks=Week4";
let averageBy = "ReportableDemographic";
let interval = "Hour";
let collectionMethodValue = "ComscoreSTB";
let playBackType = "LO";
let surveyName, metricsRatings, uiRTGValue, uiSHRValue, uiHPValue, responseStatusBody, statusCode, daysOfWeek, actualUpdatedStartTime, actualUpdatedEndTime, prgramData//let sellingTitle;
let ramdonValue = new Date().toLocaleString().split('/').join('').split(' ').join('').split(',').join('').split(':').join('').split('AM').join('').split('PM').join('');

let TestCase_ID = 'XGCT-9935_NetworkProgCreate_Comscore_TC'
let TestCase_Title = 'Test for Network Program flow - Smoke testing  comscore'

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('TEST FOR NETWORK PROGRAM FLOW - SMOKE TESTING comscore ', () => {


    // --------------Test Step------------
    it("Navigate to xGcampaign website.", async () => {

        let stepAction = "Navigate to xGcampaign website.";
        let stepData = "https://xgplatform-dev.myimagine.com/";
        let stepExpResult = "Website should get loaded.";
        let stepActualResult = "Website should get loaded.";

        prgramData = {
            "SellingTitle": "AT_" + ramdonValue + "CXGCT9935",
            "Dayparts": "EM,DT,EF,EN,PA,PT,LN,LF,WK,ON,ST,SP",
            "Days": "M-Su",
            "StartTime": "7:00 AM",
            "EndTime": "11:00 AM",
            "STime": "07:00",
            "ETime": "11:00",
            "TimeZone": "ET",
            "MF": "M-F",
            "SSU": "Sa-Su",
            "ALL": "All",
            "Desc": "Network Program Test",
            "demoValue": "A18-20"
        }

        try {
            await globalFunc.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Programming from Top menu", async () => {

        let stepAction = "Select Programming from Top menu";
        let stepData = "";
        let stepExpResult = "'Programming menu should get selected.";
        let stepActualResult = "'Programming menu should get selected.";

        try {
            await HomePage.appclickOnNavigationList("Programming");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Network programming from left menu", async () => {

        let stepAction = "Select Network programming from left menu";
        let stepData = "";
        let stepExpResult = "Network Programs page should get loaded.";
        let stepActualResult = "Network Programs page should get loaded.";

        try {
            await HomePage.appclickOnMenuListLeftPanel("Network Programming");
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the Add options display for Network programming", async () => {

        let stepAction = "Check the Add options display for Network programming";
        let stepData = "";
        let stepExpResult = "'User should have 2 options.";
        let stepActualResult = "'User should have 2 options.";

        try {
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Add Network program from the menu", async () => {

        let stepAction = "Select Add Network program from the menu";
        let stepData = "";
        let stepExpResult = "Should get a popup opened for Network program view.";
        let stepActualResult = "Should get a popup opened for Network program view.";

        try {
            await localProgPage.clickonButtonsRatecardPage1("xgc-network-program-add", "Add");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Network program from the menu", async () => {

        let stepAction = "Select Network program from the menu";
        let stepData = "";
        let stepExpResult = "Should get a popup opened for local program view.";
        let stepActualResult = "Should get a popup opened for local program view.";

        try {
            await localProgPage.clickDropdownOptionInventory1("Network Program");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Add all the mandatory fields and save the Network program", async () => {

        let stepAction = "Add all the mandatory fields and save the Network program";
        let stepData = "";
        let stepExpResult = "Network program should get saved to the Network program list with program type as Network and orbit as Network Orbit";
        let stepActualResult = "Network program should get saved to the Network program list with program type as Network and orbit as Network Orbit";
        daysOfWeek = "DaysOfWeek=Sat&DaysOfWeek=Sun";
        try {

            await networkProgram.clickComponentPdropdownInPopup("xgc-network");
            await networkProgram.selectOptionsFromComponentPdropdown("FOX");
            await localProgPage.enterTextInInputInventoryAddProgram1("xgc-network-program-selling_title", prgramData["SellingTitle"]);
            await networkProgram.enterTextInDescriptionNetwork(prgramData["Desc"]);
         
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("xgc-dropdown-daypart");
            await networkProgram.selectAllOptionsFromMultiSelectDropDown();
            //await browser.sleep(2000);
            //await localProgPage.clickMultiSelectOptionInventoryAddProgram('Prime');
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("xgc-dropdown-daypart");
           
            await networkProgram.clickDaysOptionShortCutActiveInactive("days", "INACTIVE", prgramData["ALL"]);
            await localProgPage.clickClockIconTimeOptionInventorydialog1("xgc-time-start");
            await localProgPage.clickTimeInInventorydialogTimetable(prgramData["STime"]);
            await localProgPage.clickTimeInInventorydialogTimetable(prgramData["STime"]);
            await localProgPage.clickClockIconTimeOptionInventorydialog1("xgc-time-end");
            await localProgPage.clickTimeInInventorydialogTimetable(prgramData["ETime"]);
            await localProgPage.clickTimeInInventorydialogTimetable(prgramData["ETime"]);
            await localProgPage.enterDateInInputInventoryAddProgram1("xgc-date-start", currentDate);
            await localProgPage.enterDateInInputInventoryAddProgram1("xgc-date-end", endDate);
            await localProgPage.clickonButtonsRatecardPage1("xgc-save-program", "Save");
            await verify.verifyNotPresent(networkProgram.descriptionLocator,"Description");
            

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on collapsible button", async () => {

        let stepAction = "Click on collapsible button";
        let stepData = "";
        let stepExpResult = "User should be able to see the child records displayed";
        let stepActualResult = "User should be able to see the child records displayed";

        try {
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await networkProgram.clickComponentPdropdown("xgc-networks");
            await networkProgram.selectOptionsFromComponentPdropdown("FOX");
            await networkProgram.clickFieldWithComboboxMultiselect("xgc-dayparts");
            await networkProgram.clickMultiSelectAllOptions();
            await networkProgram.clickCloseButtonMultiSelectListDropdown();
            await action.scrollToTopOfPage();
           
            await localProgPage.clickonButtonsRatecardPage1("xgc-network-program-search", "Search");
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await networkProgram.enterTextInGlobalFilterNetworkProgram(prgramData["SellingTitle"]);
            await networkProgram.clickButtonXgDetailsToggleExpendedCollepsedNeworkChildRecord(2, "Selling Title", prgramData["SellingTitle"], "EXPAND");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check that the following details are displayed for the child records", async () => {

        let stepAction = "Check that the following details are displayed for the child records";
        let stepData = "";
        let stepExpResult = "'The following details need to be captured for the child recordsa.	Market b.	Channel c.	Day-Parts d.	Selling Title e.	Time f.	Days g.	Start time h.	End time i.	FTC j.	LTC k.	Hiatus dates l.	Keyword tags m.	Genre";
        let stepActualResult = "'The following details need to be captured for the child records";

        try {
            _rowcont = Number(await networkProgram.returnRowCountInChildTableNetworkPrograms());
            if (_rowcont < 1 || typeof (_rowcont) == undefined) {
                throw new Error("Fail :No Child Record is present");

            }
            console.log("Row Count" + _rowcont);
            for (let i = 1; i <= _rowcont; i++) {
                let recordOne = Object(await networkProgram.returnDataInChildTableNetworkPrograms(i));
                recordChild[i] = recordOne;
                // console.log(Object.keys(recordOne));
                await networkProgram.verifyDataInChildTableNetworkPrograms("Market", recordOne["Market"]);
                await networkProgram.verifyDataInChildTableNetworkPrograms("Channel", recordOne["Channel"]);
                await networkProgram.verifyDataInChildTableNetworkPrograms("Selling Title", prgramData["SellingTitle"]);
                await networkProgram.verifyDataInChildTableNetworkPrograms("Dayparts", prgramData["Dayparts"]);
                await networkProgram.verifyDataInChildTableNetworkPrograms("Days", prgramData["Days"]);
                await networkProgram.verifyDataInChildTableNetworkPrograms("Time Zone", prgramData["TimeZone"]);
                //await networkProgram.verifyDataInChildTableNetworkPrograms("Start Time", prgramData["StartTime"]);
               // await networkProgram.verifyDataInChildTableNetworkPrograms("End Time", prgramData["EndTime"]);
                await networkProgram.verifyDataInChildTableNetworkPrograms("FTC", currentDate);
                await networkProgram.verifyDataInChildTableNetworkPrograms("LTC", endDate);
            }


            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check that the child records are having active/inactive after a parent program is collapsed", async () => {

        let stepAction = "Check that the child records are having active/inactive after a parent program is collapsed";
        let stepData = "";
        let stepExpResult = "User should be able to enable child records active/inactive";
        let stepActualResult = "User should be able to enable child records active/inactive";

        try {

            for (let i = 1; i <= _rowcont; i++) {
                let recordOne = recordChild[i];
                await networkProgram.verifyActiveInactiveRecordInChildTableNetworkPrograms("Market", recordOne["Market"], "ACTIVE")

            }
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("To check the Inactive functionality disable the toggle button associated to the child records", async () => {

        let stepAction = "To check the Inactive functionality disable the toggle button associated to the child records";
        let stepData = "";
        let stepExpResult = "User should not be able to see the child record in the Local program view";
        let stepActualResult = "User should not be able to see the child record in the Local program view";

        try {
            await networkProgram.makeActiveInactiveRecordInChildTableNetworkPrograms(1, "INACTIVE");
         
            await HomePage.appclickOnMenuListLeftPanel("Local Programming");
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            for (let i = 1; i <= _rowcont; i++) {
                let recordOne = recordChild[i];
                await networkProgram.clickComponentPdropdown("xgc-markets");
                await networkProgram.selectOptionsFromComponentPdropdown(recordOne["Market"]);
                await networkProgram.clickFieldWithComboboxMultiselect("xgc-channels");
                await networkProgram.clickMultiSelectAllOptions();
                await networkProgram.clickCloseButtonMultiSelectListDropdown();
                await networkProgram.clickFieldWithComboboxMultiselect("xgc-dayparts");
                await networkProgram.clickMultiSelectAllOptions();
                await networkProgram.clickCloseButtonMultiSelectListDropdown();
                await localProgPage.clickonButtonsRatecardPage1("xgc-search-button", "Search");
                await verify.verifyProgressBarNotPresent();
                await verify.verifyProgressBarNotPresent();
                await verify.verifyProgressBarNotPresent();
                await localProgPage.enterTextInGlobalFilter(prgramData["SellingTitle"]);
                await browser.sleep(1000);
                await verify.verifyNotPresent(by.xpath(localProgPage.localProgram.replace("dynamic", prgramData["SellingTitle"])), "Program Name" + prgramData["SellingTitle"]);
            }


            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("To check the Active functionality disable the toggle button associated to the child records", async () => {

        let stepAction = "To check the Active functionality disable the toggle button associated to the child records";
        let stepData = "";
        let stepExpResult = "User should be able to see the child record in the Local program view";
        let stepActualResult = "User should be able to see the child record in the Local program view";

        try {
            await HomePage.appclickOnMenuListLeftPanel("Network Programming");
            
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await networkProgram.clickComponentPdropdown("xgc-networks");
            await networkProgram.selectOptionsFromComponentPdropdown("FOX");
            await networkProgram.clickFieldWithComboboxMultiselect("xgc-dayparts");
            await networkProgram.clickMultiSelectAllOptions();
            await networkProgram.clickCloseButtonMultiSelectListDropdown();
            await action.scrollToTopOfPage();
            
            await localProgPage.clickonButtonsRatecardPage1("xgc-network-program-search", "Search");
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
           
            await networkProgram.enterTextInGlobalFilterNetworkProgram(prgramData["SellingTitle"]);
            await networkProgram.clickButtonXgDetailsToggleExpendedCollepsedNeworkChildRecord(2, "Selling Title", prgramData["SellingTitle"], "EXPAND");
          
            await networkProgram.makeActiveInactiveRecordInChildTableNetworkPrograms(1, "ACTIVE");
           
            await HomePage.appclickOnMenuListLeftPanel("Local Programming");
          
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            for (let i = 1; i <= _rowcont; i++) {
                let recordOne = recordChild[i];
  
                await networkProgram.clickComponentPdropdown("xgc-markets");
                await networkProgram.selectOptionsFromComponentPdropdown(recordOne["Market"]);
                await networkProgram.clickFieldWithComboboxMultiselect("xgc-channels");
                await networkProgram.clickMultiSelectAllOptions();
                await networkProgram.clickCloseButtonMultiSelectListDropdown();
                await networkProgram.clickFieldWithComboboxMultiselect("xgc-dayparts");
                await networkProgram.clickMultiSelectAllOptions();
                await networkProgram.clickCloseButtonMultiSelectListDropdown();
                await localProgPage.clickonButtonsRatecardPage1("xgc-search-button", "Search");
                await verify.verifyProgressBarNotPresent();
                await verify.verifyProgressBarNotPresent();
                await verify.verifyProgressBarNotPresent();
                await localProgPage.enterTextInGlobalFilter(prgramData["SellingTitle"]);
                await verify.verifyElement(by.xpath(localProgPage.localProgram.replace("dynamic", prgramData["SellingTitle"])), "Program Name" + prgramData["SellingTitle"]);
            }

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check that user has a option to lock/unlock the child records", async () => {

        let stepAction = "Check that user has a option to lock/unlock the child records";
        let stepData = "";
        let stepExpResult = "User should be able to lock and unlock child records";
        let stepActualResult = "User should be able to lock and unlock child records";

        try {
            await HomePage.appclickOnMenuListLeftPanel("Network Programming");
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await networkProgram.clickComponentPdropdown("xgc-networks");
            await networkProgram.selectOptionsFromComponentPdropdown("FOX");
            await networkProgram.clickFieldWithComboboxMultiselect("xgc-dayparts");
            await networkProgram.clickMultiSelectAllOptions();
            await networkProgram.clickCloseButtonMultiSelectListDropdown();
            await action.scrollToTopOfPage();
            
            await localProgPage.clickonButtonsRatecardPage1("xgc-network-program-search", "Search");
            
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
           
            await networkProgram.enterTextInGlobalFilterNetworkProgram(prgramData["SellingTitle"]);
            await networkProgram.clickButtonXgDetailsToggleExpendedCollepsedNeworkChildRecord(2, "Selling Title", prgramData["SellingTitle"], "EXPAND");
          
            await networkProgram.makeActiveInactiveRecordInChildTableNetworkPrograms(2, "ACTIVE");
            
            await networkProgram.clickButtonXgDetailsToggleExpendedCollepsed(1, 2, "COLLAPSE");
            await action.Click(by.xpath(networkProgram.networkProgram.replace("dynamic", prgramData["SellingTitle"])), "Click Nework Program" + prgramData["SellingTitle"]);
         
            await networkProgram.selectCheckBox("Mo");
            await networkProgram.selectCheckBox("Tu");
            await networkProgram.selectCheckBox("We");
            await networkProgram.selectCheckBox("Th");
            await networkProgram.selectCheckBox("Fr");
            await localProgPage.clickonButtonsRatecardPage1("xgc-save-program", "Update");
            await verify.verifyNotPresent(networkProgram.descriptionLocator,"Description");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("To check the lock functionality enable the toggle button associated to the child records", async () => {

        let stepAction = "To check the lock functionality enable the toggle button associated to the child records";
        let stepData = "To test this a Network program should be edited";
        let stepExpResult = "User should not be able to see all the changes updated in the child records that has made in the network parent program";
        let stepActualResult = "User should not be able to see all the changes updated in the child records that has made in the network parent program";

        try {
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await networkProgram.clickButtonXgDetailsToggleExpendedCollepsedNeworkChildRecord(2, "Selling Title", prgramData["SellingTitle"], "EXPAND");
            await networkProgram.verifyDataNotPresentInChildTableNetworkPrograms("Days", prgramData["SSU"]);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("To check the un-lock functionality disable the toggle button associated to the child records", async () => {

        let stepAction = "To check the un-lock functionality disable the toggle button associated to the child records";
        let stepData = "To test this a Network program should be edited";
        let stepExpResult = "User should not be able to see all the changes updated in the child records that has made in the network parent program";
        let stepActualResult = "User should not be able to see all the changes updated in the child records that has made in the network parent program";

        try {
          
            await networkProgram.makeActiveInactiveRecordInChildTableNetworkPrograms(2, "INACTIVE");
        
            await networkProgram.clickButtonXgDetailsToggleExpendedCollepsed(1, 2, "COLLAPSE");
            await action.Click(by.xpath(networkProgram.networkProgram.replace("dynamic", prgramData["SellingTitle"])), "Click Nework Program" + prgramData["SellingTitle"]);
        
           
            await localProgPage.clickonButtonsRatecardPage1("xgc-save-program", "Update");
            await verify.verifyNotPresent(networkProgram.descriptionLocator,"Description");
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await browser.sleep(2000);
            await networkProgram.clickButtonXgDetailsToggleExpendedCollepsed(1, 2, "EXPAND");
            await networkProgram.verifyDataInChildTableNetworkPrograms("Days", prgramData["SSU"]);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to  Local programming  to check that the child records are displayed in the Local program view", async () => {

        let stepAction = "Navigate to  Local programming  to check that the child records are displayed in the Local program view";
        let stepData = "";
        let stepExpResult = "User should be able to see all the child records in the Local program view as per the respective markets,channels and dayparts";
        let stepActualResult = "User should be able to see all the child records in the Local program view as per the respective markets,channels and dayparts";

        try {

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step------------
    it('Navigate to Programming-->Local Programmig', async () => {

        let stepAction = 'Navigate to Programming-->Local Programmig';
        let stepData = 'Market- Pittsburgh Channels-WPCB Dayparts-Morning Active- check it. After clicking on search data should display for the above selection';
        let stepExpResult = '"User should select as below:';
        let stepActualResult = '"User able to select as below:';

        try {

            await localProgPage.clickOnSettings();
            await localProgPage.clickOnSettingsSubMenuLink("Daypart Demo");
            await localProgPage.clickOnDayPartDemoSubMenuLink("Local Daypart Demo");
            let recordOne = recordChild[1];
          
            await action.scrollToTopOfPage();
        
            await localProgPage.clickonButtonsRatecardPage1("xgc-button-comscore", "Comscore");
        
            await localProgPage.SelectMarketOptions(recordOne["Market"]);
            await localProgPage.clickDataInRatingSourceTableTarcksPage("Dayparts", "Morning");
         
            elementStatus = await verify.verifyElementVisible(localProgPage.noDataMatchFound);
            if (elementStatus) {
                await localProgPage.addDayPartsDemo(recordOne["Market"], "Morning");
                demoValue = prgramData["demoValue"];
            }
            else {
                demoValue = String(await action.GetText(localProgPage.firstRowDemos, "")).trim();
            }

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    // --------------Test Step------------
    it("Navigate to Local programming to select the Link hyperlink for the Network child records", async () => {

        let stepAction = "Navigate to Local programming to select the Link hyperlink for the Network child records";
        let stepData = "";
        let stepExpResult = "User should be navigated to the program tracks";
        let stepActualResult = "User should be navigated to the program tracks";

        try {
            await HomePage.appclickOnMenuListLeftPanel("Local Programming");
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            let recordOne = recordChild[1];
            await networkProgram.clickComponentPdropdown("xgc-markets");
            await networkProgram.selectOptionsFromComponentPdropdown(recordOne["Market"]);
            await networkProgram.clickFieldWithComboboxMultiselect("xgc-channels");
            await networkProgram.clickMultiSelectAllOptions();
            await networkProgram.clickCloseButtonMultiSelectListDropdown();
            await networkProgram.clickFieldWithComboboxMultiselect("xgc-dayparts");
            await networkProgram.clickMultiSelectAllOptions();
            await networkProgram.clickCloseButtonMultiSelectListDropdown();
            await localProgPage.clickonButtonsRatecardPage1("xgc-search-button", "Search");
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await localProgPage.enterTextInGlobalFilter(prgramData["SellingTitle"]);

            // ******Wait Time to load ratings data*******
           // await browser.sleep(900000);

            //****************************************/
            await localProgPage.clickProgramLinkInLocalProgramGrid("Program Track", "Selling Title", prgramData["SellingTitle"]);


            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Check that for all the playbacks the survey books are attached", async () => {

        //Checking for only LS playback type
        let stepAction = "Check that for all the playbacks the survey books are attached <br> (Check for LS)";
        let stepData = "";
        let stepExpResult = "User should be able to see all the survey books attached";
        let stepActualResult = "User is able to see all the survey books attached for LS <br> Validation not done for other playback types due to lot of delay in data loading";

        try {
            let playbacktype = "Live Only (LO)";
            let sampleType = "DMA Universe";
            let sourceType = "Comscore";

            await localProgPage.clickProgramInRatingsTracksView(prgramData["SellingTitle"]);
            await localProgPage.clickDataInRatingSourceTableTarcksPage("xgc-list-source", sourceType);
            await localProgPage.clickDataInRatingSourceTableTarcksPage("xgc-list-sample", sampleType);
            await localProgPage.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", playbacktype);
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            let count:number=0;
            let statusGrid= Boolean(await verify.verifyElementVisible(networkProgram.gridTable));
            while ((statusGrid==false) && (count<=19)) {
                await console.log("Waiting for Ratings Data to Load...")
                await browser.sleep(60000);
                count=count+1;
                await localProgPage.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", playbacktype); 
                await verify.verifyProgressBarNotPresent();
                await console.log("Waiting for Ratings Data to Load..."+count+" min");
                statusGrid=Boolean(await verify.verifyElementVisible(networkProgram.gridTable));
            }
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    /*   
   // --------------Test Step------------
   it('Remove PAV Record', async () => {
   
       let stepAction = 'Remove PAV Record';
       let stepData = '';
       let stepExpResult = 'User should able to remove the PAV Record ';
       let stepActualResult = 'User is able to remove the PAV Record';
   
       try {
           await browser.sleep(2000);
           await action.scrollToTopOfPage();
           await browser.sleep(2000);
           await localProgPage.clickonButtonsInPage("xgc-btn-rating-build","Rating Build");
           await localProgPage.clickDropdownOptionInventory("xgc-btn-rating-build","Remove Program Averages");
           await networkProgram.clickFieldWithComboboxMultiselect("xgc-dropdown-program-average");
           await networkProgram.clickMultiSelectAllOptions();
           await networkProgram.clickCloseButtonMultiSelectListDropdown();
           await localProgPage.clickonButtonsInPage("xgc-btn-remove","Remove");
           await localProgPage.clickDropdownOptionInventory("xgc-btn-remove","Remove and Close");
           await verify.verifyProgressBarNotPresent();
   
   
           report.ReportStatus(stepAction, stepData, 'SKIP', stepExpResult, stepActualResult);
       }
   
       catch (err) {
           report.ReportStatus(stepAction, stepData, 'SKIP', stepExpResult, err);
       }
   
   });
   */
    // --------------Test Step------------
    it('Select the same Edited program from the programs list', async () => {

        let stepAction = 'Select the same Edited program from the programs list ';
        let stepData = prgramData["SellingTitle"];
        let stepExpResult = 'Days and time should change as edited in the local program page';
        let stepActualResult = 'Days and times are changed as edited in the local program page';
        try {
            await action.scrollToTopOfPage();
        
            await action.Click(localProgPage.displayOptions, "");
            await localProgPage.slideDecimalvalueInMatSlider("xgc-mat-slider-rtg", 3);
            await localProgPage.slideDecimalvalueInMatSlider("xgc-mat-slider-shr", 3);
            await localProgPage.slideDecimalvalueInMatSlider("xgc-mat-slider-siu", 3);
            await action.MouseMoveToElement(localProgPage.displayOptions, "");
            await action.Click(localProgPage.displayOptions, "");

           
            await verify.verifyElementIsDisplayed(networkProgram.gridTable, "verify grid is displayed.");
            surveyName = String(await action.GetText(networkProgram.firstRowName, "")).trim();
            await verify.verifyElementIsDisplayed(networkProgram.gridTable, "verify grid is displayed.");
            metricsRatings = await localProgPage.getFirstRowMetricsAndRatings();
            uiRTGValue = metricsRatings["RTG"];
            uiSHRValue = metricsRatings["SHR"];
            uiHPValue = metricsRatings["SIU"];
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
    it('Get Ratings from Rating Api', async (done) => {
        let stepAction = 'Get Ratings from Rating Api ';
        let stepExpResult = 'Server Response should be 200 and rating details should be displayed';
        let stepActResult = 'Server response is 200';

        actualUpdatedStartTime = prgramData["STime"] + ":00";
        actualUpdatedEndTime = prgramData["ETime"] + ":00";
        await console.log(globalvalues.ratingAPI + "Ratings/TimePeriod/Monthly?RatingSource=" + sourceValue + "&MarketSource=" + marketSource + "&CollectionMethod=" + collectionMethodValue + "&PlaybackType=" + playBackType + "&DistributorCodes=" + distributorCodes + "&ReportableDemographics=" + demoValue + "&SurveyName=" + surveyName + "&StartTime=" + String(actualUpdatedStartTime).replace(":", "%3A").replace(":", "%3A") + "&EndTime=" + String(actualUpdatedEndTime).replace(":", "%3A").replace(":", "%3A") + "&" + weeks + "&" + daysOfWeek + "&AverageBy=" + averageBy + "&Interval=" + interval);
        try {
            return new Promise(function (resolve, reject) {
                reqAPI.get({
                    "headers": { "content-type": "application/json" },
                    "url": globalvalues.ratingAPI + "Ratings/TimePeriod/Monthly?RatingSource=" + sourceValue + "&MarketSource=" + marketSource + "&CollectionMethod=" + collectionMethodValue + "&PlaybackType=" + playBackType + "&DistributorCodes=" + distributorCodes + "&ReportableDemographics=" + demoValue + "&SurveyName=" + surveyName + "&StartTime=" + String(actualUpdatedStartTime).replace(":", "%3A").replace(":", "%3A") + "&EndTime=" + String(actualUpdatedEndTime).replace(":", "%3A").replace(":", "%3A") + "&" + weeks + "&" + daysOfWeek + "&AverageBy=" + averageBy + "&Interval=" + interval,
                    "body": JSON.stringify({
                    })
                }, (error, response, body) => {
                    if (error) {
                        return console.dir(error);
                    }
                    responseStatusBody = response.body;
                    statusCode = response.statusCode;
                    if (statusCode != 200) {
                        done();
                        report.ReportStatus(stepAction, '', 'Fail', stepExpResult, stepActResult);
                    }
                    else {
                        done();
                        report.ReportStatus(stepAction, '', 'Pass', stepExpResult, stepActResult);
                    }
                    resolve();
                });
            });
        }
        catch (err) {
            report.ReportStatus(stepAction, '', 'Fail', stepExpResult, 'Server response is ' + statusCode);
        }
    });

    it('Validate "Share" "Rating" and "HutPut" Ratings values', async (done) => {
        let stepAction = 'Validate "Share" "Rating" and "HutPut" Ratings values ';
        let stepExpResult = '"Share" "Rating" and "HutPut" Ratings values Should be equal in API and UI';
        let stepActResult;
        let failedCount = 0;
        let errorMsg = "";
        let succesMsg = "";
        let apiRatings;
        try {
            let jsonData = JSON.parse(responseStatusBody);
            await console.log(jsonData);
            let dataDetails = jsonData.data;
            if (dataDetails == null || dataDetails.length == 0) {
                report.ReportStatus(stepAction, '', 'Fail', stepExpResult, 'Data details are not displayed.');
                done();
            }
            else {
                apiRatings = await localProgPage.getRatingsFromAPIAfterCalculation(dataDetails);
                let finalPUT = apiRatings["HP"];
                let finalShare = apiRatings["SHR"];
                let finalRating = apiRatings["RTG"];
                if (finalPUT.toFixed(3) != uiHPValue) {
                    errorMsg = errorMsg + "HutPut Value in API After Calculation:" + finalPUT.toFixed(3) + "\n HutPut value in UI:" + uiHPValue + "\n";
                    failedCount = failedCount + 1;
                }
                else {
                    succesMsg = succesMsg + "UI HutPut:" + uiHPValue + "\n API Calcualtion HutPut:" + finalPUT.toFixed(3) + "\n";
                }
                if (finalShare.toFixed(3) != uiSHRValue) {
                    failedCount = failedCount + 1;
                    errorMsg = errorMsg + "Share Value in API:" + finalShare.toFixed(3) + "\n Share value in UI:" + uiSHRValue + "\n";
                }
                else {
                    succesMsg = succesMsg + "Share in UI:" + uiSHRValue + "\n API Calcualtion Share:" + finalShare.toFixed(3) + "\n";
                }
                if (finalRating.toFixed(3) != uiRTGValue) {
                    failedCount = failedCount + 1;
                    errorMsg = errorMsg + "Rate Value in API After Calculation:" + finalRating.toFixed(3) + " \n Rate value in UI:" + uiRTGValue + "\n";
                }
                else {
                    succesMsg = succesMsg + "Rating in UI:" + uiRTGValue + "\n API Calcualtion Rating:" + finalRating.toFixed(3) + "\n";
                }
                if (failedCount != 0) {
                    done();
                    stepActResult = errorMsg;
                    report.ReportStatus(stepAction, '', 'SKIP', stepExpResult, stepActResult);
                }
                else {
                    stepActResult = succesMsg;
                    done();
                    report.ReportStatus(stepAction, '', 'Pass', stepExpResult, stepActResult);
                }
            }
        }
        catch (err) {
            done();
            report.ReportStatus(stepAction, '', 'SKIP', stepExpResult, ' not able to Get the advertiser details' + err);
        }
    });



    // --------------Test Step------------
    it("Click on the down arrow beside the book to select that Assign PAV button to assign PAV for the survey books", async () => {

        let stepAction = "Click on the down arrow beside the book to select that Assign PAV button to assign PAV for the survey books";
        let stepData = "";
        let stepExpResult = "Assign PAV window opens with the default selection as per program and PAV records displayed in the grid";
        let stepActualResult = "Assign PAV window opens with the default selection as per program and PAV records displayed in the grid";

        try {
           
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            await verify.verifyProgressBarNotPresent();
            pavAllRec = Object(await networkProgram.returnAssignPAVRatingsProgramsRecordsPAVTable("Name", "Value", "TP"));
            console.log("pavAllRec" + pavAllRec[1]);
            await localProgPage.clickAssignPAVRatingsProgramsTrakingPAVTable("Name", "Name", "Value", "TP", pavAllRec[1]);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("In Assign PAV window select the required records and select Assign PAV drop down and select Enable PAV only", async () => {

        let stepAction = "In Assign PAV window select the required records and select Assign PAV drop down and select Enable PAV only";
        let stepData = "";
        let stepExpResult = "In the tracks page PAV will be assigned and the TP will be disabled as per selection";
        let stepActualResult = "In the tracks page PAV will be assigned and the TP will be disabled as per selection";

        try {
           
            await localProgPage.clickRowInPAVPopupGrid(1, 1);
            await localProgPage.clickonButtonsInPage("xgc-build-pav-assign-button", "Assign");
            await localProgPage.clickDropdownOptionInventory("xgc-build-pav-assign-button", "Enable PAV only");
            await localProgPage.verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable("Rationale", "Value", "PAV", "Active", "true", 'PAV (1): ' + pavAllRec[1]);
            await localProgPage.verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable("Name", "Value", "TP", "Active", "false", pavAllRec[1]);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Repeat step 22 and in Assign PAV window select the required records and select Assign PAV drop down and select Enable both PAV and TP", async () => {

        let stepAction = "Repeat step 22 and in Assign PAV window select the required records and select Assign PAV drop down and select Enable both PAV and TP";
        let stepData = "";
        let stepExpResult = "In the tracks page PAV will be assigned and both PAV and TP will be enabled";
        let stepActualResult = "In the tracks page PAV will be assigned and both PAV and TP will be enabled";

        try {
            console.log("pavAllRec" + pavAllRec[2]);
            await localProgPage.clickAssignPAVRatingsProgramsTrakingPAVTable("Name", "Name", "Value", "TP", pavAllRec[2]);
            await localProgPage.clickRowInPAVPopupGrid(1, 1);
            await localProgPage.clickonButtonsInPage("xgc-build-pav-assign-button", "Assign");
            await localProgPage.clickDropdownOptionInventory("xgc-build-pav-assign-button", "Enable both PAV and TP records");
            await localProgPage.verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable("Rationale", "Value", "PAV", "Active", "true", 'PAV (1): ' + pavAllRec[2]);
            await localProgPage.verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable("Name", "Value", "TP", "Active", "true", pavAllRec[2]);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
