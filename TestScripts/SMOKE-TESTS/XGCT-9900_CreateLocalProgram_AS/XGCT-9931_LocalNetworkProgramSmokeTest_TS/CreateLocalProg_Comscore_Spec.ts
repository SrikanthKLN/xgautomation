/*
***********************************************************************************************************
* @Script Name :  XGCT-9934
* @Description :  Test for Local program flow - Smoke testing.16-8-2019
* @Page Object Name(s) : localProgrammingSmoke
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib
* @Pre-Conditions : 
* @Creation Date : 16-8-2019
* @Author : Adithya K
* @Modified By & Date:22-8-2019
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../Libs/GeneralLibs/ActionLib';
import { localProgrammingPage } from '../../../../Pages/SMOKE-TESTS-PAGES/LocalProgram_Page';
import { globalvalues } from '../../../../Utility/globalvalue';
import { globalTestData } from '../../../../TestData/globalTestData';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let localProgPage = new localProgrammingPage();
let global = new globalvalues();

//Variables Declaration

let programData = {
    "Channels": ["WPGH"],
    "SellingTitle": "AT_S" + new Date().getTime(),
    "Description": "AT_" + new Date().getTime(),
    "Dayparts": ["Prime"], //["Late Fringe"],
    "Days": ["Sa", "Su"],
    "StartTime": "06:00 PM", //"08:00 PM",
    "EndTime": "10:00 PM", //"10:00 PM",
    //"Start Time": ["08:00", "08:00"],
    // "End Time": ["09:00", "09:00"],
    // "FTC": "04/01/2020",
    // "LTC": "04/11/2020",
    // "Tag": "AT_DoNotDelete",
    // "Genre": "AT_DoNotDelete",
}

let sample = "DMA Universe" //"HWC Universe"
let uiPlaybacktype = "Live Only (LO)"
let demoValue = "A18-24";
let sourceValue = "Comscore";
let distributorCodes = "5426";
let marketSource = "508";
let weeks = "Week1|Week2|Week3|Week4|Week5" //"Weeks=Week1&Weeks=Week2&Weeks=Week3&Weeks=Week4";
let averageBy = "ReportableDemographic";
let interval = "Survey";
let collectionMethodValue = "ComscoreSTB";
let playBackType = "LO";
let sampleValue = "Total%20DMA" //"Hard-Wired%20Cable"
let daysOfWeek = "Sat|Sun"
let actualUpdatedStartTime = "18:00" //"20:00";
let actualUpdatedEndTime = "22:00" //"22:00";
let reqAPI = require("request");
let surveyName, uiRTG, uiSHR, uiSIU, responseStatusBody, statusCode;

let TestCase_ID = 'XGCT-9934_LocalProgCreate_Comscore'
let TestCase_Title = 'Test for Local program flow - Smoke testing.'

daysOfWeek = formatAPIFields(daysOfWeek, "DaysOfWeek");
weeks = formatAPIFields(weeks, "Weeks")

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

// const self = this;

//**********************************  TEST CASE Implementation ***************************
describe('TEST FOR LOCAL PROGRAM FLOW - SMOKE TESTING.', () => {


    // --------------Test Step 1------------
    it("Navigate to xGcampaign website.", async () => {

        let stepAction = "Navigate to xGcampaign website.";
        let stepData = "https://xgplatform-dev.myimagine.com/";
        let stepExpResult = "Website should get loaded.";
        let stepActualResult = "Website should get loaded.";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step 2------------
    it("Select Programming from Top menu", async () => {

        let stepAction = "Select Programming from Top menu";
        let stepData = "";
        let stepExpResult = "'Programming menu should get selected.";
        let stepActualResult = "'Programming menu should get selected.";

        try {
            await localProgPage.appclickOnNavigationList(localProgPage.programmingLinkText);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 7------------
    it("Check for the required demo value and add if not", async () => {

        let stepAction = "Add demo value in settings if not present";
        let stepData = "";
        let stepExpResult = "Demo value should be present and displayed";
        let stepActualResult = "Demo value is present.";
        let chkdemo
        try {

            // await localProgPage.appclickOnNavigationList(localProgPage.programmingLinkText);
            await localProgPage.clickOnSettings();
            await localProgPage.clickOnSettingsSubMenuLink("Daypart Demo");
            await localProgPage.clickOnDayPartDemoSubMenuLink("Local Daypart Demo");
            await localProgPage.clickOnComscore();
            await localProgPage.selectSingleSelectDropDownOptions(localProgPage.marketsingleSelectDropDown, localProgPage.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await browser.sleep(1000);
            console.log("***************" + programData["Dayparts"][0])
            await localProgPage.clickDataInRatingSourceTableTracksPage("Dayparts", programData["Dayparts"][0]);
            await browser.sleep(2000);

            let js = "return document.querySelector('xg-grid[attr-name=xgc-daypart-demo-grid] table').textContent.match(/" + demoValue + "/g)"
            console.log(js)
            await browser.executeScript(js).then(function (val) { chkdemo = val });
            console.log(chkdemo)
            if (chkdemo == null) {
                await localProgPage.addDayPartsDemo(globalTestData.marketName, programData["Dayparts"][0], demoValue);    // demoValue = "A18-20";
                report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
            }
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step 3-----------
    it("Select Local programming from left menu", async () => {

        let stepAction = "Select Local programming from left menu";
        let stepData = "";
        let stepExpResult = "Local Programs page should get loaded.";
        let stepActualResult = "Local Programs page is loaded.";

        try {
            await localProgPage.appclickOnMenuListLeftPanel(localProgPage.localprogrammingLinkText);
            await localProgPage.verifyPageHeader(localProgPage.localProgrammingPageHeader)
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 4------------
    it("Check the Add options display for local programming", async () => {

        let stepAction = "Check the Add options display for local programming";
        let stepData = "";
        let stepExpResult = "'User should have 2 options.";
        let stepActualResult = "'User has 2 options.";

        try {
            await browser.waitForAngular();
            await action.MouseMoveToElement(localProgPage.addlp, "Click")
            await action.Click(localProgPage.addlp, "Click")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 5------------
    it("Select Local program from the menu", async () => {

        let stepAction = "Select Local program from the menu";
        let stepData = "";
        let stepExpResult = "Should get a popup opened for local program view.";
        let stepActualResult = "Popup opened for local program view.";

        try {
            await localProgPage.clickonAddProgram(localProgPage.addProgramtypeLink_Local)
            // await localProgPage.verifyPopUpHeader(localProgPage.addProgramPopUpHeader_Local)// await action.Click(localProgPage.closePopUp, "closePopUp")// await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 7------------
    it("Add all the mandatory fields and save the local program", async () => {

        let stepAction = "Add all the mandatory fields and save the local program";
        let stepData = "";
        let stepExpResult = "Local program should get saved to the local program list.";
        let stepActualResult = "Local program ia saved to the local program list.";

        try {
            await localProgPage.selectMultiOptionsFromDropDown("channel", programData["Channels"], "dialog");
            await localProgPage.selectMultiOptionsFromDropDown("daypart", programData["Dayparts"], "dialog")
            await action.SetText(by.css("xg-input[attr-name='xgc-program-name'] input"), programData["SellingTitle"], "")
            await action.SetText(localProgPage.descriptionTextArea, programData["Description"], "")
            
            for (let i = 0; i < programData["Days"].length; i++) {
                let weekdaylocator = 'xg-checkbox[ng-reflect-label-name=' + programData["Days"][i] + ']'
                await action.Click(by.css(weekdaylocator), "Click")
            }
           
            await action.SetText(localProgPage.startTime, programData["StartTime"], "")
            await action.SetText(localProgPage.endTime, programData["EndTime"], "")
            await action.Click(localProgPage.saveLP, "save");
            await browser.sleep(10000)

            await localProgPage.selectSingleSelectDropDownOptions(localProgPage.marketsingleSelectDropDown, localProgPage.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await browser.sleep(2000)
            await localProgPage.selectMultiSelectDropDownOptions(localProgPage.channelMultiSelectDropDown, localProgPage.multiSelectDropdownOptionsLocator, programData["Channels"], "Channels")
            await localProgPage.selectMultiSelectDropDownOptions(localProgPage.daypartMultiSelectDropDown, localProgPage.multiSelectDropdownOptionsLocator, programData["Dayparts"], "Dayparts")
            await localProgPage.buttonActionsVerify(["Search"], "Click", "Search Button")
            await verify.verifyProgressBarNotPresent()

            let obj_SellingTitle = element.all(by.css("xg-grid[attr-name='xgc-program-grid'] input[class*='xg-text-right']")).get(1)
            await action.SetText(obj_SellingTitle, programData["SellingTitle"], "");
            await browser.sleep(3000)

            let js = 'return document.querySelectorAll("xg-grid-data-cell a")[0].outerText.trim()==' + '"' + programData["SellingTitle"] + '"'
            console.log(js)
            await browser.executeScript(js).then(function (flag) {
                console.log(flag)
                if (!flag) { throw new Error("Program not created") }

            })
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 10------------
    it("Click on 'Link' hyperlink", async () => {

        let stepAction = "Click on 'Link' hyperlink";
        let stepData = "";
        let stepExpResult = "User should be able to navigate to the Program rating tracks view";
        let stepActualResult = "User is able to navigate to the Program rating tracks view";

        try {
            let obj_Link = element(by.linkText("Link"))
            await action.Click(obj_Link, "Program Track Link")
            await browser.sleep(2000)
            await verify.verifyProgressBarNotPresent()
            await localProgPage.verifyPageHeader(localProgPage.programRatingTracksPageHeader)          // await browser.sleep(3000);
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 11------------
    it("Check that for all the playbacks the survey books are attached", async () => {

        let stepAction = "Check that for all the playbacks the survey books are attached";
        let stepData = "";
        let stepExpResult = "User should be able to see all the survey books attached";
        let stepActualResult = "User is able to see all the survey books attached";
        let rdata, flag;

        try {
            await localProgPage.clickProgramInRatingsTracksView(programData["SellingTitle"]) // await browser.sleep(60000);
            await verify.verifyProgressBarNotPresent()// await browser.sleep(20000);
            await localProgPage.clickDataInRatingSourceTableTracksPage("xgc-list-source", sourceValue);
            await localProgPage.clickDataInRatingSourceTableTracksPage("xgc-list-sample", sample);
            await localProgPage.clickDataInRatingSourceTableTracksPage("xgc-list-playback", uiPlaybacktype);//"Live + One (L1)","Live + Three (L3)","Live Only (LO)", "Live + Seven (L7)"
            await verify.verifyProgressBarNotPresent()

            for (let i = 0; i < 15; i++) {
                await console.log("Waiting from: " +i*10 + "secs")
                await browser.executeScript(CheckGridDataPresent).then(function (val) { rdata = val; })
                console.log(rdata)
                if (rdata > 0) { break; } else { await browser.sleep(10000) }
                await localProgPage.clickDataInRatingSourceTableTracksPage("xgc-list-sample", sample);
                await localProgPage.clickDataInRatingSourceTableTracksPage("xgc-list-playback", uiPlaybacktype);//"Live + One (L1)","Live + Three (L3)","Live Only (LO)", "Live + Seven (L7)"
            }
            await browser.sleep(2000)
            await console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")

            let GridRowsEl = '.ui-table-scrollable-body-table tr'
            await browser.executeScript(CheckGridDataCount, GridRowsEl).then(function (val) {
                flag = val;if (!flag) throw new console.error("Grid Rows -  No Ratings Data Present");
            })

            let GridCellsEl = '.ui-table-scrollable-body-table tr td'
            await browser.executeScript(CheckGridDataCount, GridCellsEl).then(function (val) {
                flag = val;if (!flag) throw new console.error("Grid Cell -  No Ratings Data Present");
            })

            let findText = 'Jan|Feb|Mar|Apr|May|Jun|July|Aug|Sep|Oct|Nov|Dec'
            await browser.executeScript(CheckGridContentMatch, GridCellsEl, findText).then(function (val) {
                flag = val;if (!flag) throw new console.error("No Survey books loaded");
            })

            let GridTableEl = '.ui-table-scrollable-body-table'
            let tableIndex = 1
            await browser.executeScript(CheckRatingsContent, GridTableEl, tableIndex).then(function (val) {
                flag = val;if (!flag) throw new console.error(" No Ratings Data Present");
            })

            await browser.executeScript(getFirstCellData, GridTableEl).then(function (val) {
                surveyName = val;
                console.log("Survey Name: " + surveyName)
            })

            await browser.sleep(1500)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });



    // --------------Test Step------------
    it("Click on display options button to set the decimal display of the ratings", async () => {

        let stepAction = "Click on display options button to set the decimal display of the ratings";
        let stepData = "";
        let stepExpResult = "'User should be able to select display option button and see the following options";
        let stepActualResult = "'User is able to select display option button and see the following options";
        try {
            let metrics;
            await browser.sleep(2000)
            await browser.executeScript(getDemoColIndex, demoValue).then(function (rntgs) { metrics = rntgs; });// console.log(rntgs[0] + "--" + rntgs[1] + "--" + rntgs[2]);

            uiRTG = metrics[0];
            uiSHR = metrics[1];
            uiSIU = metrics[2];

            console.log("*********** UI Ratings *********"); console.log(uiRTG + "--" + uiSHR + "--" + uiSIU);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }


    });

    // --------------API Step------------
    it('Get Ratings from Rating Api for playback type of  ' + playBackType + '', async (done) => {

        let stepAction = 'Get Ratings from Rating Api for playback type of  ' + playBackType + '';
        let stepExpResult = 'Server Response should be 200 and rating details should be displayed';
        let stepActResult = 'Server response is 200';

        try {
            await console.log("test")
            await console.log(globalvalues.ratingAPI + "Ratings/TimePeriod/Monthly?RatingSource=" + sourceValue + "&MarketSource=" + marketSource + "&CollectionMethod=" + collectionMethodValue + "&PlaybackType=" + playBackType + "&SurveySampleType=" + sampleValue + "&DistributorCodes=" + distributorCodes + "&ReportableDemographics=" + demoValue + "&SurveyName=" + surveyName + "&StartTime=" + String(actualUpdatedStartTime).replace(":", "%3A").replace(":", "%3A") + "&EndTime=" + String(actualUpdatedEndTime).replace(":", "%3A").replace(":", "%3A") + "&" + weeks + "&" + daysOfWeek + "&AverageBy=" + averageBy + "&PAPeriod=" + interval)

            reqAPI.get({
                "headers": { "content-type": "application/json" },
                "url": globalvalues.ratingAPI + "Ratings/TimePeriod/Monthly?RatingSource=" + sourceValue + "&MarketSource=" + marketSource + "&CollectionMethod=" + collectionMethodValue + "&PlaybackType=" + playBackType + "&DistributorCodes=" + distributorCodes + "&ReportableDemographics=" + demoValue + "&SurveyName=" + surveyName + "&StartTime=" + String(actualUpdatedStartTime).replace(":", "%3A").replace(":", "%3A") + "&EndTime=" + String(actualUpdatedEndTime).replace(":", "%3A").replace(":", "%3A") + "&" + weeks + "&" + daysOfWeek + "&AverageBy=" + averageBy + "&PAPeriod=" + interval,
                "body": JSON.stringify({
                })
            }, async (error, response, body) => {
                if (error) { return console.dir(error); }
                responseStatusBody = response.body;
                await console.log(response.body);
                statusCode = response.statusCode;
                if (statusCode != 200) {
                    done(); report.ReportStatus(stepAction, '', 'Fail', stepExpResult, stepActResult);
                }
                else {
                    done(); report.ReportStatus(stepAction, '', 'Pass', stepExpResult, stepActResult);
                }
            });
        }
        catch (err) {
            report.ReportStatus(stepAction, '', 'Fail', stepExpResult, 'Server response is ' + statusCode);
        }
    });

    // --------------API Step------------
    it('Validate UI "SHR/SIU/RTG" Ratings values with API ratings data for playback type of  ' + playBackType + '', async (done) => {

        let stepAction = 'Validate "Share" "Rating" and "HutPut" Ratings values for playback type of  ' + playBackType + ' ';
        let stepExpResult = '"Share" "Rating" and "HutPut" Ratings values Should be equal in API and UI';
        let stepActResult;
        let apiRatings;
        try {

            let jsonData = JSON.parse(responseStatusBody);   // await console.log(responseStatusBody);
            await console.log(jsonData);
            let dataDetails = jsonData.data;

            if (dataDetails == null || dataDetails.length == 0) {
                report.ReportStatus(stepAction, '', 'Fail', stepExpResult, 'Data details are not displayed.');
                done();
            }
            else {
                apiRatings = await localProgPage.getRatingsFromAPIAfterCalculation(dataDetails);

                let apiRTG = apiRatings["RTG"].toFixed(1);
                let apiSHR = Math.round(apiRatings["SHR"]);
                let apiSIU = apiRatings["SIU"].toFixed(1);

                let apiVals = "API Values: " + "RTG: " + apiRTG + ", SHR: " + apiSHR + ", SIU: " + apiSIU
                let uiVals = "API Values: " + "RTG: " + uiRTG + ", SHR: " + uiSHR + ", SIU: " + uiSIU
                console.log(apiVals + "\n" + uiVals)// await console.log("API:---rtg=" + apiRTG + "--shr = " + apiSHR + "-- siu=" + apiSIU)

                if (apiSIU != uiSIU || apiSHR != uiSHR || apiRTG != uiRTG) {
                    throw new Error("Ratings API & UI values are NOT Matching:<br>" + apiVals + "<br>" + uiVals)
                }
                else { stepActResult = "Ratings API & UI values are Matching:<br>" + apiVals + "<br>" + uiVals }
            }
            done();
            report.ReportStatus(stepAction, '', 'Pass', stepExpResult, stepActResult);
        }
        catch (err) {
            done()
            report.ReportStatus(stepAction, '', 'Fail', stepExpResult, err);
        }
    })

    // --------------Test Step------------
    it("Click on the down arrow beside the book to select that Assign PAV button to assign PAV for the survey books", async () => {

        let stepAction = "Click on the down arrow beside the book to select that Assign PAV button to assign PAV for the survey books";
        let stepData = "";
        let stepExpResult = "Assign PAV window opens with the default selection as per program and PAV records displayed in the grid";
        let stepActualResult = "Assign PAV window opened with the default selection as per program and PAV records displayed in the grid";

        try {
            let obj_suveryColFilter = element.all(by.css('xg-grid-column-filter input[class*="ui-inputtext"]')).get(0) // surveyName = "Jun19"await localProgPage.clickAssignPAVRatingsProgramsTrakingPAVTable("Name", "Name", "Value", "TP", surveyName);
            await action.SetText(obj_suveryColFilter, surveyName, "Entering Text in Survey Filter")
            await browser.sleep(1000)

            let js = 'return document.querySelector("td[title=' + surveyName + '] button[id=dropdownMenuButton]").click()'
            await browser.executeScript(js)
            js = 'return document.querySelector("li[title*=Assign] a").click()'
            await browser.executeScript(js)

            await verify.verifyProgressBarNotPresent()
            await browser.sleep(1000);

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("In Assign PAV window select the required records and select Assign PAV drop down and select Enable PAV only", async () => {

        let stepAction = "In Assign PAV window select the required records and select Assign PAV drop down and select Enable PAV only";
        let stepData = "";
        let stepExpResult = "In the tracks page PAV will be assigned and the TP will be disabled as per selection";
        let stepActualResult = "In the tracks page PAV is assigned and the TP will be disabled as per selection";
        let selectPavOption = "Enable PAV only"

        try {
            await localProgPage.clickRowInPAVPopupGrid(1, 1);
            await localProgPage.clickonButtonsInPage("xgc-build-pav-assign-button", "Assign");
            await localProgPage.clickDropdownOptionInventory("xgc-build-pav-assign-button", selectPavOption);
            await browser.sleep(2000);
            await localProgPage.verifyPopupMessageRatecard("Successfully assigned PAV's")
            await verify.verifyProgressBarNotPresent()
            await browser.executeScript('return document.querySelector("button[attr-name=xgc-btn-export]").click()')

            let obj_suveryColFilter = element.all(by.css('xg-grid-column-filter input[class*="ui-inputtext"]')).get(0)
            await action.SetText(obj_suveryColFilter, surveyName, "Entering Text in Survey Filter")
            await browser.sleep(3000)

            //Number of Records for Survey after PAV Assign
            let val = browser.executeScript('return document.querySelectorAll("table")[1].querySelectorAll("tr").length == 2')
            val.then(function (val) { if (!val) throw new Error("Number of Records after assign PAV is NOT 2") })

            //Value Col text for PAV Record
            val = browser.executeScript('return document.querySelectorAll("table")[1].querySelectorAll("tr")[1].querySelectorAll("td")[2].textContent.trim() == "PAV"')
            val.then(function (val) { if (!val) throw new Error("No TP Record") })

            //Rationale Value for PAV Record
            val = browser.executeScript('return document.querySelectorAll("table")[1].querySelectorAll("tr")[1].querySelectorAll("td")[3].getAttribute("title").split(":")[0].match(/\\d/) == 1')
            val.then(function (val) { if (!val) throw new Error("Rationale value not matching to PAV for PAV Record") })

            //Toggle Check for PAV
            val = browser.executeScript('return document.querySelectorAll("table")[1].querySelectorAll("tr")[1].querySelectorAll("td")[4].querySelector("mat-slide-toggle").getAttribute("ng-reflect-checked")')
            val.then(function (val) { if (!val) throw new Error("Toggle active check failed for PAV") })

            //Value Col text for TP
            val = browser.executeScript('return document.querySelectorAll("table")[1].querySelectorAll("tr")[0].querySelectorAll("td")[2].textContent.trim() == "TP"')
            val.then(function (val) { if (!val) throw new Error("No PAV Record") })

            //Rationale Value for TP
            val = browser.executeScript('return document.querySelectorAll("table")[1].querySelectorAll("tr")[0].querySelectorAll("td")[3].textContent.trim().match(/Time Period/g).length > 0')
            val.then(function (val) { if (!val) throw new Error("Rationale value not matching to Time Period for TP Record") })

            //Toggle Off for TP
            val = browser.executeScript('return document.querySelectorAll("table")[1].querySelectorAll("tr")[0].querySelectorAll("td")[4].querySelector("mat-slide-toggle").getAttribute("ng-reflect-checked")')
            val.then(function (val) { if (!val) throw new Error("Toggle active check failed for TP") })


            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});

//JS Script Function 
function getFirstCellData() {
    let txt = ''
    let el = document.querySelector(arguments[0] + ' tr td')
    if (el != undefined) { txt = el.textContent.trim() }
    return txt;

}
//JS Script Function 
function CheckRatingsContent() {
    let rtg = 0, shr = 0, siu = 0;
    let el = document.querySelectorAll(arguments[0])[arguments[1]]
    if (typeof el != undefined) {
        rtg = el.textContent.trim().split('\n')[0].toString().length
        shr = el.textContent.trim().split('\n')[1].toString().length
        siu = el.textContent.trim().split('\n')[2].toString().length
    }
    if (rtg == 0 || shr == 0 || siu == 0) { return false }
    else { return true }
}

//JS Script Function 
function CheckGridContentMatch() {
    let txt;
    let el = document.querySelector(arguments[0])
    if (typeof el != undefined) { txt = el.textContent.trim() }
    let re = new RegExp(arguments[1], 'g')
    return (re.test(txt))
}

//JS Script Function 
function CheckGridDataCount() {
    let totalRows = 0
    let el = document.querySelectorAll(arguments[0])
    console.log(el)
    if (typeof el != undefined) { totalRows = el.length }
    if (totalRows > 0) { return true }
    else { return false }
}

// JS Script - TO get the column index of the Demo header (e.g. M18-20)
function getDemoColIndex() {

    demoValue = arguments[0];
    console.log(demoValue)
    var tbl = document.querySelectorAll('table')[2]
    for (var i = 0; i < tbl.rows.length; i++) {
        for (var j = 0; j < tbl.rows[i].cells.length; j++) { //console.log(tbl.rows[i].cells[j].textContent.trim())
            if (tbl.rows[i].cells[j].textContent.trim() == demoValue) { var indx = j }
        }
    }
    console.log(indx)
    var ratingsVals = document.querySelectorAll('table')[3].rows[0].cells[indx].textContent.trim().split("\n")
    return ratingsVals.slice();

}

// JS Script - TO check for grid data present
function CheckGridDataPresent() {
    let len = 0
    let ele = document.querySelector('.ui-table-scrollable-body-table tr td')
    if (ele != null) { len = ele.textContent.trim().length }
    return len
}
//JS Script - Format API variable values
function formatAPIFields(oldVal, text) {
    let days = oldVal.split("|")
    let newVal = ""
    for (var i = 0; i < days.length; i++) {
        if (i == 0) { newVal = newVal + text + "=" + days[i] }
        else { newVal = newVal + "\&" + text + "=" + days[i] }
    }
    return newVal
}


// if (apiSIU != uiSIU) {
//     errorMsg = errorMsg + "SIU Value in API After Calculation:" + apiSIU + "\n SIU value in UI:" + uiSIU + "\n";
//     failedCount = failedCount + 1;
// }
// else {
//     succesMsg = succesMsg + "UI SIU:" + uiSIU + "\n API Calcualtion SIU:" + apiSIU + "\n";
// }
// if (apiSHR != uiSHR) {
//     failedCount = failedCount + 1;
//     errorMsg = errorMsg + "Share Value in API:" + apiSHR + "\n Share value in UI:" + uiSHR + "\n";
// }
// else {
//     succesMsg = succesMsg + "Share in UI:" + uiSHR + "\n API Calcualtion Share:" + apiSHR + "\n";
// }
// if (apiRTG != uiRTG) {
//     failedCount = failedCount + 1;
//     errorMsg = errorMsg + "Rate Value in API After Calculation:" + apiRTG + " \n Rate value in UI:" + uiRTG + "\n";
// }
// else {
//     succesMsg = succesMsg + "Rating in UI:" + uiRTG + "\n API Calcualtion Rating:" + apiRTG + "\n";
// }
// if (failedCount != 0) {
//     done();
//     stepActResult = errorMsg;
//     report.ReportStatus(stepAction, '', 'Fail', stepExpResult, stepActResult);
// }
// else {
//     stepActResult = succesMsg;
 // await browser.executeScript('return document.querySelector(".ui-table-scrollable-body-table tr td").textContent.trim().match(/Jan|Feb|Mar|Apr|May|Jun|July|Aug|Sep|Oct|Nov|Dec/g)').then(function (value) {
            // Counts = value; console.log("Matching Suvery Record: " + Counts)
            // })
// await browser.executeScript("return document.querySelectorAll('.ui-table-scrollable-body-table')[1].querySelector('td').textContent.trim().split('\\n')[0].toString().length").then(function (value) {
            //     Counts = value; console.log("Rating Data Present: " + Counts)
            // })

            // await browser.executeScript('return document.querySelectorAll(".ui-table-scrollable-body-table")[1].querySelector("td").textContent.trim().split("\\n")[1].toString().length').then(function (value) {
            //     Counts = value; console.log("Rating Data Present: " + Counts)
            // })

            // await browser.executeScript('return document.querySelectorAll(".ui-table-scrollable-body-table")[1].querySelector("td").innerText.trim().split("\\n")[2].toString().length').then(function (value) {
            //     Counts = value; console.log("Rating Data Present: " + Counts)
            // })

            // await browser.executeScript("return document.querySelectorAll('.ui-table-scrollable-body-table tr').length").then(function (val) {
            //     Counts = val; console.log("Ratingd Data Records Count: " + Counts)
            // })

            // await browser.executeScript("return document.querySelectorAll('.ui-table-scrollable-body-table tr td').length").then(function (val) {
            //     Counts = val; console.log("Number of Ratings data cells count: " + Counts)
            // })
            // await browser.executeScript('return document.querySelector(".ui-table-scrollable-body-table tr td").innerText.trim()').then(function (value) {
                // surveyName = value; console.log("Rating Data Present: " + surveyName)
            // })
/*
        // --------------Test Step 6----------------*********** SKIP ****************
        xit("Select Orbit program from the menu", async () => {

            let stepAction = "Select Orbit program from the menu";
            let stepData = "";
            let stepExpResult = "Should get a popup opened for local program view.";
            let stepActualResult = "Popup opened for local program view.";

            try {
                await localProgPage.buttonActionsVerify(["Add"], "Click", "Add Button")
                await localProgPage.clickonAddProgram(localProgPage.addProgramtypeLink_Orbit)
                await localProgPage.verifyPopUpHeader(localProgPage.addProgramPopUpHeader_Orbit)
                await action.Click(localProgPage.closePopUp, "closePopUp")
                await verify.verifyProgressBarNotPresent()
                report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
            }

            catch (err) {
                report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
            }

        });
        */

          // --------------Test Step 8------------
/*
it("Check the display of labels for local and orbit programs in the listing page.", async () => {

    let stepAction = "Check the display of labels for local and orbit programs in the listing page.";
    let stepData = "";
    let stepExpResult = "'For local program label should be displayed as LOCAL";
    let stepActualResult = "'For local program label is displayed as LOCAL";

    try {
        await localProgPage.addProgram(localProgPage.programDetailsForOrbitProgram, "localorbit")
        await browser.sleep(3000)
        await localProgPage.selectSingleSelectDropDownOptions(localProgPage.marketsingleSelectDropDown, localProgPage.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
        await browser.sleep(2000)
        await localProgPage.selectMultiSelectDropDownOptions(localProgPage.channelMultiSelectDropDown, localProgPage.multiSelectDropdownOptionsLocator, localProgPage.programData["Channels"], "Channels")
        await localProgPage.selectMultiSelectDropDownOptions(localProgPage.daypartMultiSelectDropDown, localProgPage.multiSelectDropdownOptionsLocator, localProgPage.programData["Dayparts"], "Dayparts")
        await localProgPage.buttonActionsVerify(["Search"], "Click", "Search Button")
        await verify.verifyProgressBarNotPresent()
        await localProgPage.gridFilter(localProgPage.sellingTitleColumnHeader, localProgPage.programDetailsForOrbitProgram["Selling Title"])
        await localProgPage.gridDataValidation(localProgPage.sellingTitleColumnHeader, localProgPage.programDetailsForOrbitProgram["Selling Title"], "Equal")
        await action.Click(localProgPage.clearAllColumTextFilter, "clearAllColumTextFilter")
        await verify.verifyProgressBarNotPresent()
        await localProgPage.gridFilter(localProgPage.programTypeColumnHeader, localProgPage.programTypeInGrid_Local)
        await localProgPage.gridDataValidation(localProgPage.programTypeColumnHeader, localProgPage.programTypeInGrid_Local, "Contains")
        await action.Click(localProgPage.sellingTitleEditLink, "sellingTitleEditLink")
        await localProgPage.verifyPopUpHeader(localProgPage.editProgramPopUpHeader_Local)
        await action.Click(localProgPage.closePopUp, "closePopUp")
        await action.Click(localProgPage.clearAllColumTextFilter, "clearAllColumTextFilter")
        await verify.verifyProgressBarNotPresent()
        await localProgPage.gridFilter(localProgPage.programTypeColumnHeader, localProgPage.programTypeInGrid_Orbit)
        await localProgPage.gridDataValidation(localProgPage.programTypeColumnHeader, localProgPage.programTypeInGrid_Orbit, "Contains")
        await action.Click(localProgPage.sellingTitleEditLink, "sellingTitleEditLink")
        await localProgPage.verifyPopUpHeader(localProgPage.editProgramPopUpHeader_Orbit)
        await action.Click(localProgPage.closePopUp, "closePopUp")
        await action.Click(localProgPage.clearAllColumTextFilter, "clearAllColumTextFilter")
        await verify.verifyProgressBarNotPresent()
        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

});
*/

    // --------------Test Step 9------------
/*
xit("Check by creating a single program by selecting multiple channels.", async () => {

    let stepAction = "Check by creating a single program by selecting multiple channels.";
    let stepData = "";
    let stepExpResult = "After the program is being created, 2 local program should get displayed in the gird for 1 channel each";
    let stepActualResult = "After the program is being created, 2 local program is displayed in the gird for 1 channel each";

    try {
        await localProgPage.appclickOnNavigationList(localProgPage.programmingLinkText);
        await localProgPage.clickOnSettings();
        await localProgPage.clickOnSettingsSubMenuLink("Daypart Demo");
        await localProgPage.clickOnDayPartDemoSubMenuLink("Local Daypart Demo");
        await localProgPage.selectSingleSelectDropDownOptions(localProgPage.marketsingleSelectDropDown, localProgPage.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
        await browser.sleep(1000);
        await localProgPage.clickDataInRatingSourceTableTarcksPage("Dayparts", localProgPage.programDetailsWithMultipleChannels["Dayparts"][0]);
        await browser.sleep(1000);
        elementStatus = await verify.verifyElementVisible(localProgPage.noDataMatchFound);
        if (elementStatus) {
            await localProgPage.addDayPartsDemo(globalTestData.marketName, localProgPage.programDetailsWithMultipleChannels["Dayparts"][0]);
            demoValue = "A18-20";
        }
        else {
            demoValue = String(await action.GetText(localProgPage.firstRowDemos, "")).trim();
        }
        await browser.sleep(2000);
        await action.scrollToTopOfPage();
        await localProgPage.appclickOnNavigationList(localProgPage.programmingLinkText);
        await localProgPage.appclickOnMenuListLeftPanel(localProgPage.localprogrammingLinkText);
        await localProgPage.addProgram(localProgPage.programDetailsWithMultipleChannels, localProgPage.addProgramtypeLink_Local)
        await browser.sleep(2000)
        await localProgPage.selectSingleSelectDropDownOptions(localProgPage.marketsingleSelectDropDown, localProgPage.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
        await browser.sleep(2000)
        await localProgPage.selectMultiSelectDropDownOptions(localProgPage.channelMultiSelectDropDown, localProgPage.multiSelectDropdownOptionsLocator, localProgPage.programDetailsWithMultipleChannels["Channels"], "Channels")
        await localProgPage.selectMultiSelectDropDownOptions(localProgPage.daypartMultiSelectDropDown, localProgPage.multiSelectDropdownOptionsLocator, localProgPage.programDetailsWithMultipleChannels["Dayparts"], "Dayparts")
        await localProgPage.buttonActionsVerify(["Search"], "Click", "Search Button")
        await verify.verifyProgressBarNotPresent()
        await localProgPage.gridFilter(localProgPage.sellingTitleColumnHeader, localProgPage.programDetailsWithMultipleChannels["Selling Title"])
        await localProgPage.gridDataValidation(localProgPage.sellingTitleColumnHeader, localProgPage.programDetailsWithMultipleChannels["Selling Title"], "Equal")
        await localProgPage.gridDataValidation(localProgPage.channelColumnHeader, globalTestData.channelsToBeVerifiedInGrid, "contains")
        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

});


// --------------Test Step 12------------
/*
it("Check that the rating are displayed for the assigned demographics", async () => {

    let stepAction = "Check that the rating are displayed for the assigned demographics";
    let stepData = "Max 10 demographics should be displayed in the tracks page";
    let stepExpResult = "User should be able to see the rating for the demographics";
    let stepActualResult = "User is able to see the rating for the demographics";

    try {
        await verify.verifyElementIsDisplayed(localProgPage.gridTable, "verify grid is displayed.");
        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

});


*/

/*
        // --------------Test Step------------
        it("Repeat step 16 and in Assign PAV window select the required records and select Assign PAV drop down and select Enable both PAV and TP", async () => {

            let stepAction = "Repeat step 16 and in Assign PAV window select the required records and select Assign PAV drop down and select Enable both PAV and TP";
            let stepData = "";
            let stepExpResult = "In the tracks page PAV will be assigned and both PAV and TP will be enabled";
            let stepActualResult = "In the tracks page PAV is assigned and both PAV and TP are enabled";

            try {
                await localProgPage.clickAssignPAVRatingsProgramsTrakingPAVTable("Name", "Name", "Value", "PAV", surveyName);
                await verify.verifyProgressBarNotPresent()
                await browser.sleep(1000);
                await localProgPage.clickonButtonsInPage("xgc-build-pav-assign-button", "Assign");
                await localProgPage.clickDropdownOptionInventory("xgc-build-pav-assign-button", "Enable both PAV and TP records");
                await browser.sleep(2000);
                await localProgPage.verifyPopupMessageRatecard("Successfully assigned PAV's")
                await verify.verifyProgressBarNotPresent()
                await localProgPage.verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable("Rationale", "Value", "PAV", "Active", "true", "PAV (1): " + surveyName);
                await localProgPage.verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable("Name", "Value", "TP", "Active", "true", surveyName);
                report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
            }

            catch (err) {
                report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
            }

        });

     // --------------Test Step 10------------
    xit("Check the display of Link hyperlink to navigate to the program tracks associated to the created program", async () => {

        let stepAction = "Check the display of Link hyperlink to navigate to the program tracks associated to the created program";
        let stepData = "";
        let stepExpResult = "User should be able to see the hyperlink Link to navigate to the Program Tracks";
        let stepActualResult = "User is able to see the hyperlink Link to navigate to the Program Tracks";

        try {
            await verify.verifyProgressBarNotPresent()
            await action.MouseMoveToElement(localProgPage.programTrackLink, "programTrackLink")
            await verify.verifyElementIsDisplayed(localProgPage.programTrackLink, "programTrackLink")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    */