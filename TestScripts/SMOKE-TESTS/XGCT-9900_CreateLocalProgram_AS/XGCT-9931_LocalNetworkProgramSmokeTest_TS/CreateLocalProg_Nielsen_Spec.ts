/*
***********************************************************************************************************
* @Script Name :  XGCT-9934
* @Description :  Test for Local program flow - Smoke testing.16-8-2019
* @Page Object Name(s) : localProgrammingSmoke
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib
* @Pre-Conditions : 
* @Creation Date : 16-8-2019
* @Author : Adithya K
* @Modified By & Date:22-8-2019
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../Libs/GeneralLibs/ActionLib';
import { localProgrammingPage } from '../../../../Pages/SMOKE-TESTS-PAGES/LocalProgram_Page';
import { globalvalues } from '../../../../Utility/globalvalue';
import { globalTestData } from '../../../../TestData/globalTestData';
import { AppCommonFunctions } from '../../../../Libs/ApplicationLibs/CommAppLib';
import { async } from 'q';
import { error } from 'util';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let localProgPage = new localProgrammingPage();
let global = new globalvalues();
let common = new AppCommonFunctions()
//Variables Declaration

let programData = {
    "Channels": ["WPGH"],
    "SellingTitle": "AT_S" + new Date().getTime(),
    "Description": "AT_" + new Date().getTime(),
    "Dayparts": ["Prime"], //["Late Fringe"],
    "Days": ["Sa", "Su"],
    "StartTime": "06:00 PM", //"08:00 PM",
    "EndTime": "10:00 PM", //"10:00 PM",
    //"Start Time": ["08:00", "08:00"],
    // "End Time": ["09:00", "09:00"],
    // "FTC": "04/01/2020",
    // "LTC": "04/11/2020",
    // "Tag": "AT_DoNotDelete",
    // "Genre": "AT_DoNotDelete",
}

let sample = "HWC Universe"
let uiPlaybacktype = "Live + Same Day (LS)"
let demoValue = "A18-24";
let sourceValue = "Nielsen";
let distributorCodes = "5979";
let marketSource = "108";
let weeks = "Week1|Week2|Week3|Week4" //"Weeks=Week1&Weeks=Week2&Weeks=Week3&Weeks=Week4";
let averageBy = "ReportableDemographic";
let interval = "Hour";
let collectionMethodValue = "LPM";
let playBackType = "LS";
let sampleValue = "Hard-Wired%20Cable"; //"Total%20DMA";
let daysOfWeek = "Sat|Sun"
let actualUpdatedStartTime = "18:00" //"20:00";
let actualUpdatedEndTime = "22:00" //"22:00";
let reqAPI = require("request");
let surveyName, uiRTG, uiSHR, uiHP, responseStatusBody, statusCode;

let TestCase_ID = 'XGCT-9934_LocalProgCreate_Nielsen'
let TestCase_Title = 'Test for Local program flow - Smoke testing.'

daysOfWeek = formatAPIFields(daysOfWeek, "DaysOfWeek");
weeks = formatAPIFields(weeks, "Weeks")

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

// const self = this;

//**********************************  TEST CASE Implementation ***************************
describe('TEST FOR LOCAL PROGRAM FLOW - SMOKE TESTING.', () => {


    // --------------Test Step 1------------
    it("Navigate to xGcampaign website.", async () => {

        let stepAction = "Navigate to xGcampaign website.";
        let stepData = "https://xgplatform-dev.myimagine.com/";
        let stepExpResult = "Website should get loaded.";
        let stepActualResult = "Website should get loaded.";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step 2------------
    it("Select Programming from Top menu", async () => {

        let stepAction = "Select Programming from Top menu";
        let stepData = "";
        let stepExpResult = "'Programming menu should get selected.";
        let stepActualResult = "'Programming menu should get selected.";

        try {
            await localProgPage.appclickOnNavigationList(localProgPage.programmingLinkText);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 7------------
    it("Check for the required demo value and add if not exists", async () => {

        let stepAction = "Add demo value in settings if not present";
        let stepData = "";
        let stepExpResult = "Demo value should be present and displayed";
        let stepActualResult = "Demo value is present.";
        let chkdemo
        try {

            await common.clickOnMenuLink("Settings", "Daypart Demo")
            await common.clickOnMenuLink("Daypart Demo", "Local Daypart Demo");
            await common.clickOnMenuLink("Local Daypart Demo")
            // await localProgPage.clickOnComscore();
            await localProgPage.selectSingleSelectDropDownOptions(localProgPage.marketsingleSelectDropDown, localProgPage.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await browser.sleep(1000);
            console.log("***************" + programData["Dayparts"][0])
            await localProgPage.clickDataInRatingSourceTableTracksPage("Dayparts", programData["Dayparts"][0]);
            await browser.sleep(2000);

            let js = "return document.querySelector('xg-grid[attr-name=xgc-daypart-demo-grid] table').textContent.match(/" + demoValue + "/g)"
            console.log(js)
            await browser.executeScript(js).then(function (val) { chkdemo = val });
            if (chkdemo == null) {
                await localProgPage.addDayPartsDemo(globalTestData.marketName, programData["Dayparts"][0], demoValue);    // demoValue = "A18-20";
                report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
            }
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step 3-----------
    it("Select Local programming from left menu", async () => {

        let stepAction = "Select Local programming from left menu";
        let stepData = "";
        let stepExpResult = "Local Programs page should get loaded.";
        let stepActualResult = "Local Programs page is loaded.";

        try {
            await localProgPage.appclickOnMenuListLeftPanel(localProgPage.localprogrammingLinkText);
            await localProgPage.verifyPageHeader(localProgPage.localProgrammingPageHeader)
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 4------------
    it("Check the Add options display for local programming", async () => {

        let stepAction = "Check the Add options display for local programming";
        let stepData = "";
        let stepExpResult = "'User should have 2 options.";
        let stepActualResult = "'User has 2 options.";

        try {
            await browser.waitForAngular();
            await action.MouseMoveToElement(localProgPage.addlp, "Click")
            await action.Click(localProgPage.addlp, "Click")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step 5------------
    it("Select Local program from the menu", async () => {

        let stepAction = "Select Local program from the menu";
        let stepData = "";
        let stepExpResult = "Should get a popup opened for local program view.";
        let stepActualResult = "Popup opened for local program view.";

        try {
            await localProgPage.clickonAddProgram(localProgPage.addProgramtypeLink_Local)
            // await localProgPage.verifyPopUpHeader(localProgPage.addProgramPopUpHeader_Local)// await action.Click(localProgPage.closePopUp, "closePopUp")// await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 7------------
    it("Add all the mandatory fields and save the local program", async () => {

        let stepAction = "Add all the mandatory fields and save the local program";
        let stepData = "";
        let stepExpResult = "Local program should get saved to the local program list.";
        let stepActualResult = "Local program ia saved to the local program list.";
        // const delay = ms => new Promise(res => setTimeout(res, ms));

        try {
            await localProgPage.selectMultiOptionsFromDropDown("channel", programData["Channels"], "dialog");
            // await localProgPage.selectMultiOptionsFromDropDown("daypart", programData["Dayparts"], "dialog")

            await selectItem()

            // await browser.executeScript(selectItem, programData["Dayparts"][0])


            await action.SetText(by.css("xg-input[attr-name='xgc-program-name'] input"), programData["SellingTitle"], "")
            await action.SetText(localProgPage.descriptionTextArea, programData["Description"], "")

            for (let i = 0; i < programData["Days"].length; i++) {
                let weekdaylocator = 'xg-checkbox[ng-reflect-label-name=' + programData["Days"][i] + ']'
                await action.Click(by.css(weekdaylocator), "Click")
            }

            await action.SetText(localProgPage.startTime, programData["StartTime"], "")
            await action.SetText(localProgPage.endTime, programData["EndTime"], "")
            await action.Click(localProgPage.saveLP, "save");
            await browser.sleep(10000)

            await localProgPage.selectSingleSelectDropDownOptions(localProgPage.marketsingleSelectDropDown, localProgPage.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await browser.sleep(2000)
            await localProgPage.selectMultiSelectDropDownOptions(localProgPage.channelMultiSelectDropDown, localProgPage.multiSelectDropdownOptionsLocator, programData["Channels"], "Channels")
            await localProgPage.selectMultiSelectDropDownOptions(localProgPage.daypartMultiSelectDropDown, localProgPage.multiSelectDropdownOptionsLocator, programData["Dayparts"], "Dayparts")
            await localProgPage.buttonActionsVerify(["Search"], "Click", "Search Button")
            await verify.verifyProgressBarNotPresent()

            // let obj_SellingTitle = element.all(by.css("xg-grid[attr-name='xgc-program-grid'] input[class*='xg-text-right']")).get(1)
            let obj_SellingTitle = element(by.css("input[placeholder='Global Filter'][type='text']"))
            await action.SetText(obj_SellingTitle, programData["SellingTitle"], "");
            await browser.sleep(2000)

            let js = 'return document.querySelectorAll("xg-grid-data-cell a")[0].outerText.trim()==' + '"' + programData["SellingTitle"] + '"'
            console.log(js)
            await browser.executeScript(js).then(function (flag) {
                console.log(flag)
                if (!flag) { throw new Error("Program not created") }

            })
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 10------------
    it("Click on 'Link' hyperlink", async () => {

        let stepAction = "Click on 'Link' hyperlink";
        let stepData = "";
        let stepExpResult = "User should be able to navigate to the Program rating tracks view";
        let stepActualResult = "User is able to navigate to the Program rating tracks view";

        try {
            let obj_Link = element(by.linkText("Link"))
            await action.Click(obj_Link, "Program Track Link")
            await browser.sleep(2000)
            // await localProgPage.clickProgramLinkInLocalProgramGrid(localProgPage.programTrackColumnHeader, localProgPage.sellingTitleColumnHeader, programData["SellingTitle"]);
            await verify.verifyProgressBarNotPresent()
            await localProgPage.verifyPageHeader(localProgPage.programRatingTracksPageHeader)          // await browser.sleep(3000);
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step 11------------
    it("Check that for 'LS' playback the survey books are attached", async () => {

        let stepAction = "Check that for 'LS'playback the survey books are attached";
        let stepData = "";
        let stepExpResult = "User should be able to see all the survey books attached";
        let stepActualResult = "User is able to see all the survey books attached";
        let rdata, flag;

        try {
            // await localProgPage.clickProgramInRatingsTracksView(programData["SellingTitle"]) // await browser.sleep(60000);
            await verify.verifyProgressBarNotPresent()
            await browser.sleep(2000);
            await localProgPage.clickDataInRatingSourceTableTracksPage("xgc-list-source", sourceValue);
            await localProgPage.clickDataInRatingSourceTableTracksPage("xgc-list-collection", collectionMethodValue);
            await localProgPage.clickDataInRatingSourceTableTracksPage("xgc-list-sample", sample);
            await localProgPage.clickDataInRatingSourceTableTracksPage("xgc-list-playback", uiPlaybacktype);//"Live + One (L1)","Live + Three (L3)","Live Only (LO)", "Live + Seven (L7)"
            await verify.verifyProgressBarNotPresent()

            for (let i = 0; i < 30; i++) {
                await console.log("Waiting from: " + i * 10 + "secs")
                await browser.executeScript(CheckGridDataPresent).then(function (val) { rdata = val; })
                console.log(rdata)
                if (rdata > 0) { break; }
                else { await browser.sleep(10000) }

                await localProgPage.clickDataInRatingSourceTableTracksPage("xgc-list-sample", sample);
                await localProgPage.clickDataInRatingSourceTableTracksPage("xgc-list-playback", uiPlaybacktype);//"Live + One (L1)","Live + Three (L3)","Live Only (LO)", "Live + Seven (L7)"
            }

            await browser.sleep(3000);
            await console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")

            let GridRowsEl = '.ui-table-scrollable-body-table tr'
            await browser.executeScript(CheckGridDataCount, GridRowsEl).then(function (val) {
                flag = val;if (!flag) throw new console.error("Grid Rows -  No Ratings Data Present");
            })

            let GridCellsEl = '.ui-table-scrollable-body-table tr td'
            await browser.executeScript(CheckGridDataCount, GridCellsEl).then(function (val) {
                flag = val; if (!flag) throw new console.error("Grid Cell -  No Ratings Data Present");
            })

            let findText = 'Jan|Feb|Mar|Apr|May|Jun|July|Aug|Sep|Oct|Nov|Dec'
            await browser.executeScript(CheckGridContentMatch, GridCellsEl, findText).then(function (val) {
                flag = val; if (!flag) throw new console.error("No Survey books loaded");
            })

            let GridTableEl = '.ui-table-scrollable-body-table'
            let tableIndex = 1
            await browser.executeScript(CheckRatingsContent, GridTableEl, tableIndex).then(function (val) {
                flag = val; if (!flag) throw new console.error(" No Ratings Data Present"); 
            })

            await browser.executeScript(getFirstCellData, GridTableEl).then(function (val) {
                if(!val){throw new console.error("Error - Unable to get Data")}
                surveyName = val;
                console.log("Survey Name: " + surveyName)
            })

            await browser.sleep(1500)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);

        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });



    // --------------Test Step------------
    it("Get the ratings data for the required survey record", async () => {

        let stepAction = "Get the ratings data for the reuired survey record";
        let stepData = "";
        let stepExpResult = "Should be able to retrieve ratings data for the required survey";
        let stepActualResult = "Ratings data retrieved for the required survey";
        try {
            let metrics;
            await browser.sleep(2000)
            await browser.executeScript(getFirRowRatings, demoValue).then(function (rntgs) { metrics = rntgs; });// console.log(rntgs[0] + "--" + rntgs[1] + "--" + rntgs[2]);

            uiRTG = metrics[0];
            uiSHR = metrics[1];
            uiHP = metrics[2];

            console.log("*********** UI Ratings *********"); console.log(uiRTG + "--" + uiSHR + "--" + uiHP);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }


    });

    // --------------API Step------------
    it('Get Ratings from Rating Api for playback type of  ' + playBackType + '', async (done) => {

        let stepAction = 'Get Ratings from Rating Api for playback type of  ' + playBackType + '';
        let stepExpResult = 'Server Response should be 200 and rating details should be displayed';
        let stepActResult = 'Server response is 200';

        try {
            await console.log("test")
            await console.log(globalvalues.ratingAPI + "Ratings/TimePeriod/Monthly?RatingSource=" + sourceValue + "&MarketSource=" + marketSource + "&CollectionMethod=" + collectionMethodValue + "&PlaybackType=" + playBackType + "&SurveySampleType=" + sampleValue + "&DistributorCodes=" + distributorCodes + "&ReportableDemographics=" + demoValue + "&SurveyName=" + surveyName + "&StartTime=" + String(actualUpdatedStartTime).replace(":", "%3A").replace(":", "%3A") + "&EndTime=" + String(actualUpdatedEndTime).replace(":", "%3A").replace(":", "%3A") + "&" + weeks + "&" + daysOfWeek + "&AverageBy=" + averageBy + "&Interval=" + interval)
            reqAPI.get({
                "headers": { "content-type": "application/json" },
                "url": globalvalues.ratingAPI + "Ratings/TimePeriod/Monthly?RatingSource=" + sourceValue + "&MarketSource=" + marketSource + "&CollectionMethod=" + collectionMethodValue + "&PlaybackType=" + playBackType + "&SurveySampleType=" + sampleValue + "&DistributorCodes=" + distributorCodes + "&ReportableDemographics=" + demoValue + "&SurveyName=" + surveyName + "&StartTime=" + String(actualUpdatedStartTime).replace(":", "%3A").replace(":", "%3A") + "&EndTime=" + String(actualUpdatedEndTime).replace(":", "%3A").replace(":", "%3A") + "&" + weeks + "&" + daysOfWeek + "&AverageBy=" + averageBy + "&Interval=" + interval,
                "body": JSON.stringify({
                })
            }, async (error, response, body) => {
                if (error) { return console.dir(error); }
                responseStatusBody = response.body;
                await console.log(response.body);
                statusCode = response.statusCode;
                if (statusCode != 200) {
                    done(); report.ReportStatus(stepAction, '', 'Fail', stepExpResult, stepActResult);
                }
                else {
                    done(); report.ReportStatus(stepAction, '', 'Pass', stepExpResult, stepActResult);
                }
            });
        }
        catch (err) {
            report.ReportStatus(stepAction, '', 'Fail', stepExpResult, 'Server response is ' + statusCode);
        }
    });

    // --------------API Step------------
    it('Validate UI "SHR/HP/RTG" Ratings values with API ratings data for playback type of  ' + playBackType + '', async (done) => {

        let stepAction = 'Validate UI "SHR/HP/RTG" Ratings values with API ratings data for playback type of  ' + playBackType + '';
        let stepExpResult = '"SHR/HP/RTG" Ratings values Should be equal in API and UI';
        let stepActResult;

        let apiRatings;

        try {

            let jsonData = JSON.parse(responseStatusBody);   // await console.log(responseStatusBody);
            await console.log(jsonData);
            let dataDetails = jsonData.data;

            if (dataDetails == null || dataDetails.length == 0) {
                report.ReportStatus(stepAction, '', 'Fail', stepExpResult, 'Data details are not displayed.');
                done();
            }
            else {
                apiRatings = await localProgPage.getRatingsFromAPIAfterCalculation(dataDetails);

                let apiRTG = apiRatings["RTG"].toFixed(1);
                let apiSHR = Math.round(apiRatings["SHR"]);
                let apiHP = apiRatings["HP"].toFixed(1);

                let apiVals = "API Values: " + "RTG: " + apiRTG + ", SHR: " + apiSHR + ", SIU: " + apiHP
                let uiVals = "UI Values: " + "RTG: " + uiRTG + ", SHR: " + uiSHR + ", SIU: " + uiHP
                console.log(apiVals + "\n" + uiVals)// await console.log("API:---rtg=" + apiRTG + "--shr = " + apiSHR + "-- siu=" + apiSIU)

                if (apiHP != uiHP || apiSHR != uiSHR || apiRTG != uiRTG) {
                    throw new Error("Ratings API & UI values are NOT Matching:<br>" + apiVals + "<br>" + uiVals)
                }
                else { stepActResult = "Ratings API & UI values are Matching:<br>" + apiVals + "<br>" + uiVals }
            }
            done();
            report.ReportStatus(stepAction, '', 'Pass', stepExpResult, stepActResult);
        }
        catch (err) {
            done()
            report.ReportStatus(stepAction, '', 'Fail', stepExpResult, err);
        }
    })

    // --------------Test Step------------
    it("Click on the down arrow beside the book to select that Assign PAV button to assign PAV for the survey books", async () => {

        let stepAction = "Click on the down arrow beside the book to select that Assign PAV button to assign PAV for the survey books";
        let stepData = "";
        let stepExpResult = "Assign PAV window opens with the default selection as per program and PAV records displayed in the grid";
        let stepActualResult = "Assign PAV window opened with the default selection as per program and PAV records displayed in the grid";

        try {

            let obj_suveryColFilter = element.all(by.css('xg-grid-column-filter input[class*="ui-inputtext"]')).get(0)
            await action.SetText(obj_suveryColFilter, surveyName, "Entering Text in Survey Filter")
            await browser.sleep(1000)

            let js = 'return document.querySelector("td[title=' + surveyName + '] button[id=dropdownMenuButton]").click()'
            await browser.executeScript(js)
            js = 'return document.querySelector("li[title*=Assign] a").click()'
            await browser.executeScript(js)

            await verify.verifyProgressBarNotPresent()
            await browser.sleep(1000);

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("In Assign PAV window select the required records and select Assign PAV drop down and select Enable PAV only", async () => {

        let stepAction = "In Assign PAV window select the required records and select Assign PAV drop down and select Enable PAV only";
        let stepData = "";
        let stepExpResult = "In the tracks page PAV will be assigned and the TP will be disabled as per selection";
        let stepActualResult = "In the tracks page PAV is assigned and the TP will be disabled as per selection";
        let selectPavOption = "Enable PAV only"

        try {
            await localProgPage.clickRowInPAVPopupGrid(1, 1);
            await localProgPage.clickonButtonsInPage("xgc-build-pav-assign-button", "Assign");
            await localProgPage.clickDropdownOptionInventory("xgc-build-pav-assign-button", selectPavOption);
            await browser.sleep(2000);
            await localProgPage.verifyPopupMessageRatecard("Successfully assigned PAV's")
            await verify.verifyProgressBarNotPresent()
            await browser.executeScript('return document.querySelector("button[attr-name=xgc-btn-export]").click()')

            let obj_suveryColFilter = element.all(by.css('xg-grid-column-filter input[class*="ui-inputtext"]')).get(0)
            await action.SetText(obj_suveryColFilter, surveyName, "Entering Text in Survey Filter")
            await browser.sleep(3000)

            //Number of Records for Survey after PAV Assign
            let val = browser.executeScript('return document.querySelectorAll("table")[1].querySelectorAll("tr").length == 2')
            val.then(function (val) { if (!val) throw new Error("Number of Records after assign PAV is NOT 2") })

            //Value Col text for PAV Record
            val = browser.executeScript('return document.querySelectorAll("table")[1].querySelectorAll("tr")[1].querySelectorAll("td")[2].textContent.trim() == "PAV"')
            val.then(function (val) { if (!val) throw new Error("reP Record") })

            //Rationale Value for PAV Record
            val = browser.executeScript('return document.querySelectorAll("table")[1].querySelectorAll("tr")[1].querySelectorAll("td")[3].getAttribute("title").split(":")[0].match(/\\d/) == 1')
            val.then(function (val) { if (!val) throw new Error("Rationale value not matching to PAV for PAV Record") })

            //Toggle Check for PAV
            val = browser.executeScript('return document.querySelectorAll("table")[1].querySelectorAll("tr")[1].querySelectorAll("td")[4].querySelector("mat-slide-toggle").getAttribute("ng-reflect-checked")')
            val.then(function (val) { if (!val) throw new Error("Toggle active check failed for PAV") })

            //Value Col text for TP
            val = browser.executeScript('return document.querySelectorAll("table")[1].querySelectorAll("tr")[0].querySelectorAll("td")[2].textContent.trim() == "TP"')
            val.then(function (val) { if (!val) throw new Error("No PAV Record") })

            //Rationale Value for TP
            val = browser.executeScript('return document.querySelectorAll("table")[1].querySelectorAll("tr")[0].querySelectorAll("td")[3].textContent.trim().match(/Time Period/g).length > 0')
            val.then(function (val) { if (!val) throw new Error("Rationale value not matching to Time Period for TP Record") })

            //Toggle Off for TP
            val = browser.executeScript('return document.querySelectorAll("table")[1].querySelectorAll("tr")[0].querySelectorAll("td")[4].querySelector("mat-slide-toggle").getAttribute("ng-reflect-checked")')
            val.then(function (val) { if (!val) throw new Error("Toggle active check failed for TP") })


            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});

async function selectItem() {

    let el = element(by.css('div[attr-name="xgc-local-program"] xg-multi-select[attr-name="xgc-dayparts"] p-multiselect label'))
    await action.Click(el, "click")
    // el = element.all(by.css('div[ng-reflect-klass*="ui-chkbox-box"]')).get(0)
    // await action.Click(el, "click")
    // await action.Click(el, "click")
    // let elements = await action.getWebElements(by.css('ul[class*=ui-multiselect-items] li'))
    let els = element.all(by.css('ul[class*=ui-multiselect-items] li'))
    let length = 0
    await els.count().then(function (val) { length = val })
    await console.log(length)
    for (let i = 0; i < length; i++) {
        let txt
        await els.get(i).getText().then(function (val) { txt = val })
        if (txt.trim() == programData["Dayparts"][0]) {
            action.Click(els.get(i), "click")
            break;
        }
    }
    await action.Click(localProgPage.multiSelectClose, "Click On Close symbol");
}


//JS Script Function 
function getFirstCellData() {
   
    try {
        let txt = ''
        let el = document.querySelector(arguments[0] + ' tr td')
        if (el != undefined || el!=null) { txt = el.textContent.trim(); return txt; }
        else throw false
        
    }
    catch (err) { return (err) }
   
}
//JS Script Function 
function CheckRatingsContent() {
    let rtg = 0, shr = 0, siu = 0;
    let el = document.querySelectorAll(arguments[0])[arguments[1]]
    if (typeof el != undefined) {
        rtg = el.textContent.trim().split('\n')[0].toString().length
        shr = el.textContent.trim().split('\n')[1].toString().length
        siu = el.textContent.trim().split('\n')[2].toString().length
    }
    if (rtg == 0 || shr == 0 || siu == 0) { return false }
    else { return true }
}

//JS Script Function 
function CheckGridContentMatch() {
    let txt;
    let el = document.querySelector(arguments[0])
    if (typeof el != undefined) { txt = el.textContent.trim() }
    let re = new RegExp(arguments[1], 'g')
    return (re.test(txt))
}

//JS Script Function 
function CheckGridDataCount() {
    let totalRows = 0
    let el = document.querySelectorAll(arguments[0])
    console.log(el)
    if (typeof el != undefined) { totalRows = el.length }
    if (totalRows > 0) { return true }
    else { return false }
}

// JS Script - TO get the column index of the Demo header (e.g. M18-20)
function getFirRowRatings() {

    demoValue = arguments[0];
    console.log(demoValue)
    var tbl = document.querySelectorAll('table')[2]
    for (var i = 0; i < tbl.rows.length; i++) {
        for (var j = 0; j < tbl.rows[i].cells.length; j++) { //console.log(tbl.rows[i].cells[j].textContent.trim())
            if (tbl.rows[i].cells[j].textContent.trim() == demoValue) { var indx = j }
        }
    }
    console.log(indx)
    var ratingsVals = document.querySelectorAll('table')[3].rows[0].cells[indx].textContent.trim().split("\n")
    return ratingsVals.slice();

}

// JS Script - TO check for grid data present
function CheckGridDataPresent() {
    let len = 0
    let ele = document.querySelector('.ui-table-scrollable-body-table tr td')
    if (ele != null) { len = ele.textContent.trim().length }
    return len
}
//JS Script - Format API variable values
function formatAPIFields(oldVal, text) {
    let days = oldVal.split("|")
    let newVal = ""
    for (var i = 0; i < days.length; i++) {
        if (i == 0) { newVal = newVal + text + "=" + days[i] }
        else { newVal = newVal + "\&" + text + "=" + days[i] }
    }
    return newVal
}
