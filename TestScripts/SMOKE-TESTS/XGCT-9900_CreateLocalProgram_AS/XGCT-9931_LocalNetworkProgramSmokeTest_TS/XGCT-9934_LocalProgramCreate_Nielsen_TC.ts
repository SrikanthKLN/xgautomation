/*
***********************************************************************************************************
* @Script Name :  XGCT-9934
* @Description :  Test for Local program flow - Smoke testing.16-8-2019
* @Page Object Name(s) : localProgrammingSmoke
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib
* @Pre-Conditions : 
* @Creation Date : 16-8-2019
* @Author : Adithya K
* @Modified By & Date:22-8-2019
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../Libs/GeneralLibs/ActionLib';
import { localProgrammingSmoke } from '../../../../Pages/SMOKE-TESTS-PAGES/localProgrammingSmoke';
import { globalvalues } from '../../../../Utility/globalvalue';
import { globalTestData } from '../../../../TestData/globalTestData';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let localProg = new localProgrammingSmoke();
let global = new globalvalues();

//Variables Declaration
let elementStatus;
let demoValue
let sourceValue = "Nielsen";
let distributorCodes = "5979";
let marketSource = "108";
let weeks = "Weeks=Week1&Weeks=Week2&Weeks=Week3&Weeks=Week4";
let averageBy = "ReportableDemographic";
let interval = "Hour";
let collectionMethodValue = "LPM";
let playBackType = "LS";
// let sampleValue = "Total%20DMA";
let sampleValue = "Hard-Wired%20Cable";
// let daysOfWeek = "DaysOfWeek=Mon&DaysOfWeek=Tue&DaysOfWeek=Wed&DaysOfWeek=Thu&DaysOfWeek=Fri";
let daysOfWeek = "DaysOfWeek=Sun&DaysOfWeek=Sat";
// let actualUpdatedStartTime = localProg.programDetailsWithSingleChannel["Start Time"][1] + ":00";
// let actualUpdatedEndTime = localProg.programDetailsWithSingleChannel["End Time"][1] + ":00";
let actualUpdatedStartTime = localProg.programDetailsWithSingleChannel["Start Time"][0] + ":00";
let actualUpdatedEndTime = localProg.programDetailsWithSingleChannel["End Time"][0] + ":00";
let reqAPI = require("request");
let surveyName, metricsRatings, uiRTGValue, uiSHRValue, uiHPValue, responseStatusBody, statusCode;

let TestCase_ID = 'XGCT-9934_LocalProgCreate_Nielsen'
let TestCase_Title = 'Test for Local program flow - Smoke testing.'

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('TEST FOR LOCAL PROGRAM FLOW - SMOKE TESTING.', () => {

    // --------------Test Step------------
    it("Navigate to xGcampaign website.", async () => {

        let stepAction = "Navigate to xGcampaign website.";
        let stepData = "https://xgplatform-dev.myimagine.com/";
        let stepExpResult = "Website should get loaded.";
        let stepActualResult = "Website should get loaded.";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });



    // --------------Test Step------------
    it("Select Programming from Top menu", async () => {

        let stepAction = "Select Programming from Top menu";
        let stepData = "";
        let stepExpResult = "'Programming menu should get selected.";
        let stepActualResult = "'Programming menu should get selected.";

        try {
            await localProg.appclickOnNavigationList(localProg.programmingLinkText);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Local programming from left menu", async () => {

        let stepAction = "Select Local programming from left menu";
        let stepData = "";
        let stepExpResult = "Local Programs page should get loaded.";
        let stepActualResult = "Local Programs page is loaded.";

        try {
            await localProg.appclickOnMenuListLeftPanel(localProg.localprogrammingLinkText);
            await localProg.verifyPageHeader(localProg.localProgrammingPageHeader)
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Check the Add options display for local programming", async () => {

        let stepAction = "Check the Add options display for local programming";
        let stepData = "";
        let stepExpResult = "'User should have 2 options.";
        let stepActualResult = "'User has 2 options.";

        try {
            await localProg.buttonActionsVerify(["Add"], "Click", "Add Button")
            await localProg.verifyAddProgramtype(localProg.addProgramtypeLink_Local)
            await localProg.verifyAddProgramtype(localProg.addProgramtypeLink_Orbit)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Local program from the menu", async () => {

        let stepAction = "Select Local program from the menu";
        let stepData = "";
        let stepExpResult = "Should get a popup opened for local program view.";
        let stepActualResult = "Popup opened for local program view.";

        try {
            await localProg.clickonAddProgram(localProg.addProgramtypeLink_Local)
            await localProg.verifyPopUpHeader(localProg.addProgramPopUpHeader_Local)
            await action.Click(localProg.closePopUp, "closePopUp")
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Orbit program from the menu", async () => {

        let stepAction = "Select Orbit program from the menu";
        let stepData = "";
        let stepExpResult = "Should get a popup opened for local program view.";
        let stepActualResult = "Popup opened for local program view.";

        try {
            await localProg.buttonActionsVerify(["Add"], "Click", "Add Button")
            await localProg.clickonAddProgram(localProg.addProgramtypeLink_Orbit)
            await localProg.verifyPopUpHeader(localProg.addProgramPopUpHeader_Orbit)
            await action.Click(localProg.closePopUp, "closePopUp")
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step------------
    it("Add all the mandatory fields and save the local program", async () => {

        let stepAction = "Add all the mandatory fields and save the local program";
        let stepData = "";
        let stepExpResult = "Local program should get saved to the local program list.";
        let stepActualResult = "Local program ia saved to the local program list.";

        try {
            await localProg.addProgram(localProg.programDetailsWithSingleChannel, localProg.addProgramtypeLink_Local)
            await browser.sleep(2000)
            await localProg.selectSingleSelectDropDownOptions(localProg.marketsingleSelectDropDown, localProg.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await browser.sleep(2000)
            await localProg.selectMultiSelectDropDownOptions(localProg.channelMultiSelectDropDown, localProg.multiSelectDropdownOptionsLocator, localProg.programDetailsWithSingleChannel["Channels"], "Channels")
            await localProg.selectMultiSelectDropDownOptions(localProg.daypartMultiSelectDropDown, localProg.multiSelectDropdownOptionsLocator, localProg.programDetailsWithSingleChannel["Dayparts"], "Dayparts")
            await localProg.buttonActionsVerify(["Search"], "Click", "Search Button")
            await verify.verifyProgressBarNotPresent()
            await localProg.gridFilter(localProg.sellingTitleColumnHeader, localProg.programDetailsWithSingleChannel["Selling Title"])
            await localProg.gridDataValidation(localProg.sellingTitleColumnHeader, localProg.programDetailsWithSingleChannel["Selling Title"], "Equal")
            await action.Click(localProg.clearAllColumTextFilter, "clearAllColumTextFilter")
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Check the display of labels for local and orbit programs in the listing page.", async () => {

        let stepAction = "Check the display of labels for local and orbit programs in the listing page.";
        let stepData = "";
        let stepExpResult = "'For local program label should be displayed as LOCAL";
        let stepActualResult = "'For local program label is displayed as LOCAL";

        try {
            await localProg.addProgram(localProg.programDetailsForOrbitProgram, "localorbit")
            await browser.sleep(2000)
            await localProg.selectSingleSelectDropDownOptions(localProg.marketsingleSelectDropDown, localProg.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await browser.sleep(2000)
            await localProg.selectMultiSelectDropDownOptions(localProg.channelMultiSelectDropDown, localProg.multiSelectDropdownOptionsLocator, localProg.programDetailsForOrbitProgram["Channels"], "Channels")
            await localProg.selectMultiSelectDropDownOptions(localProg.daypartMultiSelectDropDown, localProg.multiSelectDropdownOptionsLocator, localProg.programDetailsForOrbitProgram["Dayparts"], "Dayparts")
            await localProg.buttonActionsVerify(["Search"], "Click", "Search Button")
            await verify.verifyProgressBarNotPresent()
            await localProg.gridFilter(localProg.sellingTitleColumnHeader, localProg.programDetailsForOrbitProgram["Selling Title"])
            await localProg.gridDataValidation(localProg.sellingTitleColumnHeader, localProg.programDetailsForOrbitProgram["Selling Title"], "Equal")
            await action.Click(localProg.clearAllColumTextFilter, "clearAllColumTextFilter")
            await verify.verifyProgressBarNotPresent()
            await localProg.gridFilter(localProg.programTypeColumnHeader, localProg.programTypeInGrid_Local)
            await localProg.gridDataValidation(localProg.programTypeColumnHeader, localProg.programTypeInGrid_Local, "Contains")
            await action.Click(localProg.sellingTitleEditLink, "sellingTitleEditLink")
            await localProg.verifyPopUpHeader(localProg.editProgramPopUpHeader_Local)
            await action.Click(localProg.closePopUp, "closePopUp")
            await action.Click(localProg.clearAllColumTextFilter, "clearAllColumTextFilter")
            await verify.verifyProgressBarNotPresent()
            await localProg.gridFilter(localProg.programTypeColumnHeader, localProg.programTypeInGrid_Orbit)
            await localProg.gridDataValidation(localProg.programTypeColumnHeader, localProg.programTypeInGrid_Orbit, "Contains")
            await action.Click(localProg.sellingTitleEditLink, "sellingTitleEditLink")
            await localProg.verifyPopUpHeader(localProg.editProgramPopUpHeader_Orbit)
            await action.Click(localProg.closePopUp, "closePopUp")
            await action.Click(localProg.clearAllColumTextFilter, "clearAllColumTextFilter")
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check by creating a single program by selecting multiple channels.", async () => {

        let stepAction = "Check by creating a single program by selecting multiple channels.";
        let stepData = "";
        let stepExpResult = "After the program is being created, 2 local program should get displayed in the gird for 1 channel each";
        let stepActualResult = "After the program is being created, 2 local program is displayed in the gird for 1 channel each";

        try {
            await localProg.appclickOnNavigationList(localProg.programmingLinkText);
            await localProg.clickOnSettings();
            await localProg.clickOnSettingsSubMenuLink("Daypart Demo");
            await localProg.clickOnDayPartDemoSubMenuLink("Local Daypart Demo");
            await localProg.selectSingleSelectDropDownOptions(localProg.marketsingleSelectDropDown, localProg.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await browser.sleep(1000);
            await localProg.clickDataInRatingSourceTableTarcksPage("Dayparts", localProg.programDetailsWithMultipleChannels["Dayparts"][0]);
            await browser.sleep(1000);
            elementStatus = await verify.verifyElementVisible(localProg.noDataMatchFound);
            if (elementStatus) {
                await localProg.addDayPartsDemo(globalTestData.marketName, localProg.programDetailsWithMultipleChannels["Dayparts"][0]);
                demoValue = "A18-20";
            }
            else {
                demoValue = String(await action.GetText(localProg.firstRowDemos, "")).trim();
            }
            await browser.sleep(2000);
            await action.scrollToTopOfPage();
            await localProg.appclickOnNavigationList(localProg.programmingLinkText);
            await localProg.appclickOnMenuListLeftPanel(localProg.localprogrammingLinkText);
            await localProg.addProgram(localProg.programDetailsWithMultipleChannels, localProg.addProgramtypeLink_Local)
            await browser.sleep(2000)
            await localProg.selectSingleSelectDropDownOptions(localProg.marketsingleSelectDropDown, localProg.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await browser.sleep(2000)
            await localProg.selectMultiSelectDropDownOptions(localProg.channelMultiSelectDropDown, localProg.multiSelectDropdownOptionsLocator, localProg.programDetailsWithMultipleChannels["Channels"], "Channels")
            await localProg.selectMultiSelectDropDownOptions(localProg.daypartMultiSelectDropDown, localProg.multiSelectDropdownOptionsLocator, localProg.programDetailsWithMultipleChannels["Dayparts"], "Dayparts")
            await localProg.buttonActionsVerify(["Search"], "Click", "Search Button")
            await verify.verifyProgressBarNotPresent()
            await localProg.gridFilter(localProg.sellingTitleColumnHeader, localProg.programDetailsWithMultipleChannels["Selling Title"])
            await localProg.gridDataValidation(localProg.sellingTitleColumnHeader, localProg.programDetailsWithMultipleChannels["Selling Title"], "Equal")
            await browser.sleep(2000)
            await localProg.gridDataValidation(localProg.channelColumnHeader, globalTestData.channelsToBeVerifiedInGrid, "contains")
            await action.Click(localProg.clearAllColumTextFilter, "clearAllColumTextFilter")
            await verify.verifyProgressBarNotPresent()
            await localProg.selectMultiSelectDropDownOptions(localProg.daypartMultiSelectDropDown, localProg.multiSelectDropdownOptionsLocator, localProg.programDetailsWithSingleChannel["Dayparts"], "Dayparts")
            await localProg.buttonActionsVerify(["Search"], "Click", "Search Button")
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the display of Link hyperlink to navigate to the program tracks associated to the created program", async () => {

        let stepAction = "Check the display of Link hyperlink to navigate to the program tracks associated to the created program";
        let stepData = "";
        let stepExpResult = "User should be able to see the hyperlink Link to navigate to the Program Tracks";
        let stepActualResult = "User is able to see the hyperlink Link to navigate to the Program Tracks";

        try {
            await verify.verifyProgressBarNotPresent()
            await action.MouseMoveToElement(localProg.programTrackLink, "programTrackLink")
            await verify.verifyElementIsDisplayed(localProg.programTrackLink, "programTrackLink")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on 'Link' hyperlink", async () => {

        let stepAction = "Click on 'Link' hyperlink";
        let stepData = "";
        let stepExpResult = "User should be able to navigate to the Program rating tracks view";
        let stepActualResult = "User is able to navigate to the Program rating tracks view";

        try {
            await localProg.gridFilter(localProg.sellingTitleColumnHeader, localProg.programDetailsWithSingleChannel["Selling Title"])
            await localProg.gridDataValidation(localProg.sellingTitleColumnHeader, localProg.programDetailsWithSingleChannel["Selling Title"], "Equal")
            await localProg.clickProgramLinkInLocalProgramGrid(localProg.programTrackColumnHeader, localProg.sellingTitleColumnHeader, localProg.programDetailsWithSingleChannel["Selling Title"]);
            await verify.verifyProgressBarNotPresent()
            await localProg.verifyPageHeader(localProg.programRatingTracksPageHeader)
            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check that for all the playbacks the survey books are attached", async () => {

        let stepAction = "Check that for all the playbacks the survey books are attached";
        let stepData = "";
        let stepExpResult = "User should be able to see all the survey books attached";
        let stepActualResult = "User is able to see all the survey books attached";

        try {
            await localProg.clickProgramInRatingsTracksView(localProg.programDetailsWithSingleChannel["Selling Title"])
            let count: number = 0;
            let statusGrid = Boolean(await verify.verifyElementVisible(localProg.gridTable));
            while ((statusGrid == false) && (count <= 19)) {
                await console.log("Waiting for Ratings Data to Load...")
                await browser.sleep(60000);
                count = count + 1;
                await localProg.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + Same Day (LS)");
                await verify.verifyProgressBarNotPresent();
                statusGrid = Boolean(await verify.verifyElementVisible(localProg.gridTable));
            }
            await verify.verifyProgressBarNotPresent()
            await localProg.clickDataInRatingSourceTableTarcksPage("xgc-list-source", sourceValue);
            await localProg.clickDataInRatingSourceTableTarcksPage("xgc-list-collection", collectionMethodValue);
            await localProg.clickDataInRatingSourceTableTarcksPage("xgc-list-sample", "HWC Universe");
            await localProg.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + Three (L3)");
            await verify.verifyProgressBarNotPresent()
            surveyName = String(await action.GetText(localProg.firstRowName, "")).trim();
            await localProg.verifyDataInAssignPAVRatingsProgramsTraksPAVTable("Value", "Name", surveyName, 'TP');
            await localProg.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + Same Day (LS)");
            await verify.verifyProgressBarNotPresent()
            surveyName = String(await action.GetText(localProg.firstRowName, "")).trim();
            await localProg.verifyDataInAssignPAVRatingsProgramsTraksPAVTable("Value", "Name", surveyName, 'TP');
            await localProg.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + One (L1)");
            await verify.verifyProgressBarNotPresent()
            surveyName = String(await action.GetText(localProg.firstRowName, "")).trim();
            await localProg.verifyDataInAssignPAVRatingsProgramsTraksPAVTable("Value", "Name", surveyName, 'TP');
            // await localProg.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live Only (LO)");
            // await verify.verifyProgressBarNotPresent()
            // surveyName = String(await action.GetText(localProg.firstRowName, "")).trim();
            // await localProg.verifyDataInAssignPAVRatingsProgramsTraksPAVTable("Value", "Name", surveyName, 'TP');
            // await localProg.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + Seven (L7)");
            // await verify.verifyProgressBarNotPresent()
            // surveyName = String(await action.GetText(localProg.firstRowName, "")).trim();
            // await localProg.verifyDataInAssignPAVRatingsProgramsTraksPAVTable("Value", "Name", surveyName, 'TP');
            // await browser.sleep(1500)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check that the rating are displayed for the assigned demographics", async () => {

        let stepAction = "Check that the rating are displayed for the assigned demographics";
        let stepData = "Max 10 demographics should be displayed in the tracks page";
        let stepExpResult = "User should be able to see the rating for the demographics";
        let stepActualResult = "User is able to see the rating for the demographics";

        try {
            await verify.verifyElementIsDisplayed(localProg.gridTable, "verify grid is displayed.");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on display options button to set the decimal display of the ratings", async (done) => {

        let stepAction = "Click on display options button to set the decimal display of the ratings";
        let stepData = "";
        let stepExpResult = "'User should be able to select display option button and see the following options";
        let stepActualResult = "'User is able to select display option button and see the following options";
        try {
            await localProg.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + Same Day (LS)");
            await localProg.buttonActionsVerify(["xgc-btn-display"], "Click", "display Button")
            await localProg.metricsDisplayOptionsPropValues()
            await localProg.slideDecimalvalueInMatSlider("xgc-mat-slider-rtg", 3);
            await localProg.slideDecimalvalueInMatSlider("xgc-mat-slider-shr", 3);
            await localProg.slideDecimalvalueInMatSlider("xgc-mat-slider-hp", 3);
            await localProg.buttonActionsVerify(["xgc-btn-display"], "Click", "display Button")
            await verify.verifyElementIsDisplayed(localProg.gridTable, "verify grid is displayed.");
            // metricsRatings = await localProg.getFirstRowMetricsAndRatings();
            // uiRTGValue = metricsRatings["RTG"];
            // uiSHRValue = metricsRatings["SHR"];
            // uiHPValue = metricsRatings["HP"];
            let metrics;
            await browser.sleep(2000)
            await browser.executeScript(getDemoColIndex, demoValue).then(function (rntgs) { metrics = rntgs; });// console.log(rntgs[0] + "--" + rntgs[1] + "--" + rntgs[2]);

            uiRTGValue = metrics[0];
            uiSHRValue = metrics[1];
            uiHPValue = metrics[2];

            done();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }


    });

    // --------------API Step------------
    it('Get Ratings from Rating Api for playback type of  ' + playBackType + '', async (done) => {
        let stepAction = 'Get Ratings from Rating Api for playback type of  ' + playBackType + '';
        let stepExpResult = 'Server Response should be 200 and rating details should be displayed';
        let stepActResult = 'Server response is 200';
        try {
            await console.log("test")
            await console.log(globalvalues.ratingAPI + "Ratings/TimePeriod/Monthly?RatingSource=" + sourceValue + "&MarketSource=" + marketSource + "&CollectionMethod=" + collectionMethodValue + "&PlaybackType=" + playBackType + "&SurveySampleType=" + sampleValue + "&DistributorCodes=" + distributorCodes + "&ReportableDemographics=" + demoValue + "&SurveyName=" + surveyName + "&StartTime=" + String(actualUpdatedStartTime).replace(":", "%3A").replace(":", "%3A") + "&EndTime=" + String(actualUpdatedEndTime).replace(":", "%3A").replace(":", "%3A") + "&" + weeks + "&" + daysOfWeek + "&AverageBy=" + averageBy + "&Interval=" + interval)
            reqAPI.get({
                "headers": { "content-type": "application/json" },
                "url": globalvalues.ratingAPI + "Ratings/TimePeriod/Monthly?RatingSource=" + sourceValue + "&MarketSource=" + marketSource + "&CollectionMethod=" + collectionMethodValue + "&PlaybackType=" + playBackType + "&SurveySampleType=" + sampleValue + "&DistributorCodes=" + distributorCodes + "&ReportableDemographics=" + demoValue + "&SurveyName=" + surveyName + "&StartTime=" + String(actualUpdatedStartTime).replace(":", "%3A").replace(":", "%3A") + "&EndTime=" + String(actualUpdatedEndTime).replace(":", "%3A").replace(":", "%3A") + "&" + weeks + "&" + daysOfWeek + "&AverageBy=" + averageBy + "&Interval=" + interval,
                "body": JSON.stringify({
                })
            }, async (error, response, body) => {
                if (error) {
                    return console.dir(error);
                }
                responseStatusBody = response.body;
                await console.log(response.body);
                statusCode = response.statusCode;
                if (statusCode != 200) {
                    done();
                    report.ReportStatus(stepAction, '', 'Fail', stepExpResult, stepActResult);
                }
                else {
                    done();
                    report.ReportStatus(stepAction, '', 'Pass', stepExpResult, stepActResult);
                }

            });
        }
        catch (err) {
            report.ReportStatus(stepAction, '', 'Fail', stepExpResult, 'Server response is ' + statusCode);
        }
    });

    // --------------API Step------------
    it('Validate "Share" "Rating" and "HutPut" Ratings values for playback type of  ' + playBackType + '', async (done) => {

        let stepAction = 'Validate "Share" "Rating" and "HutPut" Ratings values for playback type of  ' + playBackType + ' ';
        let stepExpResult = '"Share" "Rating" and "HutPut" Ratings values Should be equal in API and UI';
        let stepActResult;
        let failedCount = 0;
        let errorMsg = "";
        let succesMsg = "";
        let apiRatings;
        await console.log(responseStatusBody);
        let jsonData = JSON.parse(responseStatusBody);
        await console.log(jsonData);
        let dataDetails = jsonData.data;
        if (dataDetails == null || dataDetails.length == 0) {
            report.ReportStatus(stepAction, '', 'Fail', stepExpResult, 'Data details are not displayed.');
            done();
        }
        else {
            apiRatings = await localProg.getRatingsFromAPIAfterCalculation(dataDetails);
            let finalPUT: number = apiRatings["HP"];
            let finalShare: number = apiRatings["SHR"];
            let finalRating: number = apiRatings["RTG"];
            if (finalPUT.toFixed(3) != uiHPValue) {
                errorMsg = errorMsg + "HutPut Value in API After Calculation:" + finalPUT.toFixed(3) + "\n HutPut value in UI:" + uiHPValue + "\n";
                failedCount = failedCount + 1;
            }
            else {
                succesMsg = succesMsg + "UI HutPut:" + uiHPValue + "\n API Calcualtion HutPut:" + finalPUT.toFixed(3) + "\n";
            }
            if (finalShare.toFixed(3) != uiSHRValue) {
                failedCount = failedCount + 1;
                errorMsg = errorMsg + "Share Value in API:" + finalShare.toFixed(3) + "\n Share value in UI:" + uiSHRValue + "\n";
            }
            else {
                succesMsg = succesMsg + "Share in UI:" + uiSHRValue + "\n API Calcualtion Share:" + finalShare.toFixed(3) + "\n";
            }
            if (finalRating.toFixed(3) != uiRTGValue) {
                failedCount = failedCount + 1;
                errorMsg = errorMsg + "Rate Value in API After Calculation:" + finalRating.toFixed(3) + " \n Rate value in UI:" + uiRTGValue + "\n";
            }
            else {
                succesMsg = succesMsg + "Rating in UI:" + uiRTGValue + "\n API Calcualtion Rating:" + finalRating.toFixed(3) + "\n";
            }
            if (failedCount != 0) {
                done();
                stepActResult = errorMsg + "----" + succesMsg;
                report.ReportStatus(stepAction, '', 'SKIP', stepExpResult, stepActResult);
            }
            else {
                stepActResult = succesMsg;
                done();
                report.ReportStatus(stepAction, '', 'Pass', stepExpResult, stepActResult);
            }
        }
    })

    // --------------Test Step------------
    it("Click on the down arrow beside the book to select that Assign PAV button to assign PAV for the survey books", async () => {

        let stepAction = "Click on the down arrow beside the book to select that Assign PAV button to assign PAV for the survey books";
        let stepData = "";
        let stepExpResult = "Assign PAV window opens with the default selection as per program and PAV records displayed in the grid";
        let stepActualResult = "Assign PAV window opened with the default selection as per program and PAV records displayed in the grid";

        try {
            surveyName = "May19"
            await localProg.clickAssignPAVRatingsProgramsTrakingPAVTable("Name", "Name", "Value", "TP", surveyName);
            await verify.verifyProgressBarNotPresent()
            await browser.sleep(1000);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("In Assign PAV window select the required records and select Assign PAV drop down and select Enable PAV only", async () => {

        let stepAction = "In Assign PAV window select the required records and select Assign PAV drop down and select Enable PAV only";
        let stepData = "";
        let stepExpResult = "In the tracks page PAV will be assigned and the TP will be disabled as per selection";
        let stepActualResult = "In the tracks page PAV is assigned and the TP will be disabled as per selection";

        try {
            await localProg.clickRowInPAVPopupGrid(1, 1);
            await localProg.clickonButtonsInPage("xgc-build-pav-assign-button", "Assign");
            await localProg.clickDropdownOptionInventory("xgc-build-pav-assign-button", "Enable PAV only");
            await localProg.verifyPopupMessageRatecard("Successfully assigned PAV's")
            await verify.verifyProgressBarNotPresent()
            await localProg.verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable("Rationale", "Value", "PAV", "Active", "true", "PAV (1): " + surveyName);
            await localProg.verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable("Name", "Value", "TP", "Active", "false", surveyName);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Repeat step 16 and in Assign PAV window select the required records and select Assign PAV drop down and select Enable both PAV and TP", async () => {

        let stepAction = "Repeat step 16 and in Assign PAV window select the required records and select Assign PAV drop down and select Enable both PAV and TP";
        let stepData = "";
        let stepExpResult = "In the tracks page PAV will be assigned and both PAV and TP will be enabled";
        let stepActualResult = "In the tracks page PAV is assigned and both PAV and TP are enabled";

        try {
            await localProg.clickAssignPAVRatingsProgramsTrakingPAVTable("Name", "Name", "Value", "PAV", surveyName);
            await verify.verifyProgressBarNotPresent()
            await browser.sleep(1000);
            await localProg.clickonButtonsInPage("xgc-build-pav-assign-button", "Assign");
            await localProg.clickDropdownOptionInventory("xgc-build-pav-assign-button", "Enable both PAV and TP records");
            await localProg.verifyPopupMessageRatecard("Successfully assigned PAV's")
            await verify.verifyProgressBarNotPresent()
            await localProg.verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable("Rationale", "Value", "PAV", "Active", "true", "PAV (1): " + surveyName);
            await localProg.verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable("Name", "Value", "TP", "Active", "true", surveyName);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});

// JS Script - TO get the column index of the Demo header (e.g. M18-20)
function getDemoColIndex() {

    demoValue = arguments[0];
    console.log(demoValue)
    var tbl = document.querySelectorAll('table')[2]
    for (var i = 0; i < tbl.rows.length; i++) {
        for (var j = 0; j < tbl.rows[i].cells.length; j++) { //console.log(tbl.rows[i].cells[j].textContent.trim())
            if (tbl.rows[i].cells[j].textContent.trim() == demoValue) { var indx = j }
        }
    }
    console.log(indx)
    var ratingsVals = document.querySelectorAll('table')[3].rows[0].cells[indx].textContent.trim().split("\n")
    return ratingsVals.slice();

}


// JS Script - TO check for grid data present
function CheckGridDataPresent() {
    let len = 0
    let ele = document.querySelector('.ui-table-scrollable-body-table tr td')
    if (ele != null) { len = ele.textContent.trim().length }
    return len
}
//JS Script - Format API variable values
function formatAPIFields(oldVal, text) {
    let days = oldVal.split("|")
    let newVal = ""
    for (var i = 0; i < days.length; i++) {
        if (i == 0) { newVal = newVal + text + "=" + days[i] }
        else { newVal = newVal + "\&" + text + "=" + days[i] }
    }
    return newVal
}