/*
***********************************************************************************************************
* @Script Name :  XGCT-9934
* @Description :  Test for Local program flow - Smoke testing.16-8-2019
* @Page Object Name(s) : localProgrammingSmoke
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib
* @Pre-Conditions : 
* @Creation Date : 16-8-2019
* @Author : Adithya K
* @Modified By & Date:22-8-2019
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { globalvalues } from '../../../../Utility/globalvalue';
import { Reporter } from '../../../../Utility/htmlResult';
import { ActionLib } from '../../../../Libs/GeneralLibs/ActionLib';
import { VerifyLib } from '../../../../Libs/GeneralLibs/VerificationLib';
import { globalTestData } from '../../../../TestData/globalTestData';
import { localProgrammingSmoke } from '../../../../Pages/SMOKE-TESTS-PAGES/LocalProgrammingSmoke';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let localProg = new localProgrammingSmoke();
let global = new globalvalues();

let elementStatus;
let demoValue;
demoValue = "A18-24";

let sourceValue = "Comscore";
let distributorCodes = "5426";
let marketSource = "508";
let weeks = "Weeks=Week1&Weeks=Week2&Weeks=Week3&Weeks=Week4&Weeks=Week5";
let averageBy = "ReportableDemographic";
let interval = "Hour";
let collectionMethodValue = "ComscoreSTB";
let playBackType = "LO";
let surveyName;
let metricsRatings;
let uiRTGValue;
let uiSHRValue;
let uiSIUValue;
let responseStatusBody;
let statusCode;
let daysOfWeek = "DaysOfWeek=Mon&DaysOfWeek=Tue&DaysOfWeek=Wed&DaysOfWeek=Thu&DaysOfWeek=Fri";
let actualUpdatedStartTime = localProg.programDetailsWithSingleChannel["Start Time"][1] + ":00";
let actualUpdatedEndTime = localProg.programDetailsWithSingleChannel["End Time"][1] + ":00";
let reqAPI = require("request");

//Variables Declaration
let TestCase_ID = 'XGCT-9934_CreateLocalProg_Comscore'
let TestCase_Title = 'Test for Local program flow - Smoke testing.'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('TEST FOR LOCAL PROGRAM FLOW - SMOKE TESTING.', () => {

    // --------------Test Step------------
    it("Navigate to xGcampaign website.", async () => {

        let stepAction = "Navigate to xGcampaign website.";
        let stepData = "https://xgplatform-dev.myimagine.com/";
        let stepExpResult = "Website should get loaded.";
        let stepActualResult = "Website should get loaded.";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });



    // --------------Test Step------------
    it("Select Programming from Top menu", async () => {

        let stepAction = "Select Programming from Top menu";
        let stepData = "";
        let stepExpResult = "'Programming menu should get selected.";
        let stepActualResult = "'Programming menu should get selected.";

        try {
            await localProg.appclickOnNavigationList(localProg.programmingLinkText);
            await localProg.clickOnSettings();
            await localProg.clickOnSettingsSubMenuLink("Daypart Demo");
            await localProg.clickOnDayPartDemoSubMenuLink("Local Daypart Demo");
            await browser.sleep(1000);
            await localProg.clickOnComscore();
            await localProg.selectSingleSelectDropDownOptions(localProg.marketsingleSelectDropDown, localProg.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await browser.sleep(1000);
            await localProg.clickDataInRatingSourceTableTarcksPage("Dayparts", "Morning");
            await browser.sleep(1000);
            elementStatus = await verify.verifyElementVisible(localProg.noDataMatchFound);
            if (elementStatus) {
                await localProg.addDayPartsDemo("Pittsburgh", "Morning");
                demoValue = "A18-20";
            }
            else {
                demoValue = String(await action.GetText(localProg.firstRowDemos, "")).trim();
            }
            await browser.sleep(2000);
            await action.scrollToTopOfPage();
            await localProg.appclickOnNavigationList(localProg.programmingLinkText);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Local programming from left menu", async () => {

        let stepAction = "Select Local programming from left menu";
        let stepData = "";
        let stepExpResult = "Local Programs page should get loaded.";
        let stepActualResult = "Local Programs page should get loaded.";

        try {
            await localProg.appclickOnMenuListLeftPanel(localProg.localprogrammingLinkText);
            await localProg.verifyPageHeader(localProg.localProgrammingPageHeader)
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Add all the mandatory fields and save the local program", async () => {

        let stepAction = "Add all the mandatory fields and save the local program";
        let stepData = "";
        let stepExpResult = "Local program should get saved to the local program list.";
        let stepActualResult = "Local program should get saved to the local program list.";

        try {
            await localProg.addProgram(localProg.programDetailsWithSingleChannel, localProg.addProgramtypeLink_Local)
            await browser.sleep(2000)
            await localProg.selectSingleSelectDropDownOptions(localProg.marketsingleSelectDropDown, localProg.singleSelectDropdownOptionsLocator, globalTestData.marketName, "Market")
            await browser.sleep(2000)
            await localProg.eraseMultiselectDropdownSelection(localProg.channelMultiSelectDropDown, "Channels")
            await localProg.selectMultiSelectDropDownOptions(localProg.channelMultiSelectDropDown, localProg.multiSelectDropdownOptionsLocator, localProg.programDetailsWithSingleChannel["Channels"], "Channels")
            await localProg.eraseMultiselectDropdownSelection(localProg.daypartMultiSelectDropDown, "Dayparts")
            await localProg.selectMultiSelectDropDownOptions(localProg.daypartMultiSelectDropDown, localProg.multiSelectDropdownOptionsLocator, localProg.programDetailsWithSingleChannel["Dayparts"], "Dayparts")
            await localProg.buttonActionsVerify(["Search"], "Click", "Search Button")
            await verify.verifyProgressBarNotPresent()
            await localProg.gridFilter(localProg.sellingTitleColumnHeader, localProg.programDetailsWithSingleChannel["Selling Title"])
            await localProg.gridDataValidation(localProg.sellingTitleColumnHeader, localProg.programDetailsWithSingleChannel["Selling Title"], "Equal")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Click on 'Link' hyperlink", async () => {

        let stepAction = "Click on 'Link' hyperlink";
        let stepData = "";
        let stepExpResult = "User should be able to navigate to the Program rating tracks view";
        let stepActualResult = "User should be able to navigate to the Program rating tracks view";

        try {
            await localProg.clickProgramLinkInLocalProgramGrid(localProg.programTrackColumnHeader, localProg.sellingTitleColumnHeader, localProg.programDetailsWithSingleChannel["Selling Title"]);
            await verify.verifyProgressBarNotPresent()
            await localProg.verifyPageHeader(localProg.programRatingTracksPageHeader)
            await browser.sleep(3000);
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Check that the rating are displayed for the assigned demographics", async () => {

        let stepAction = "Check that the rating are displayed for the assigned demographics";
        let stepData = "Max 10 demographics should be displayed in the tracks page";
        let stepExpResult = "User should be able to see the rating for the demographics";
        let stepActualResult = "User should be able to see the rating for the demographics";

        try {
            await localProg.clickProgramInRatingsTracksView(localProg.programDetailsWithSingleChannel["Selling Title"])
            await console.log("Waiting for Ratings Data to Load...")
            await browser.sleep(480000);
            await verify.verifyProgressBarNotPresent()
            await browser.sleep(2000);
            await localProg.clickDataInRatingSourceTableTarcksPage("xgc-list-source", sourceValue);
            await localProg.clickDataInRatingSourceTableTarcksPage("xgc-list-sample", "DMA Universe");
            await localProg.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live Only (LO)");
            await verify.verifyProgressBarNotPresent()
            surveyName = String(await action.GetText(localProg.firstRowName, "")).trim();
            await localProg.verifyDataInAssignPAVRatingsProgramsTraksPAVTable("Value", "Name", surveyName, 'TP');
            await verify.verifyElementIsDisplayed(localProg.gridTable, "verify grid is displayed.");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on display options button to set the decimal display of the ratings", async (done) => {

        let stepAction = "Click on display options button to set the decimal display of the ratings";
        let stepData = "";
        let stepExpResult = "'User should be able to select display option button and see the following options";
        let stepActualResult = "'User should be able to select display option button and see the following options";
        try {
            await localProg.buttonActionsVerify(["xgc-btn-display"], "Click", "display Button")
            await localProg.metricsDisplayOptionsPropValuesComScore()
            await localProg.slideDecimalvalueInMatSlider("xgc-mat-slider-rtg", 3);
            await localProg.slideDecimalvalueInMatSlider("xgc-mat-slider-shr", 3);
            await localProg.slideDecimalvalueInMatSlider("xgc-mat-slider-siu", 3);
            await localProg.buttonActionsVerify(["xgc-btn-display"], "Click", "display Button")
            await browser.sleep(3000);
            await verify.verifyElementIsDisplayed(localProg.gridTable, "verify grid is displayed.");
            metricsRatings = await localProg.getFirstRowMetricsAndRatings();
            uiRTGValue = metricsRatings["RTG"];
            uiSHRValue = metricsRatings["SHR"];
            uiSIUValue = metricsRatings["SIU"];
            done();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }


    });

    it('Get Ratings from Rating Api', async (done) => {
        let stepAction = 'Get Ratings from Rating Api ';
        let stepExpResult = 'Server Response should be 200 and rating details should be displayed';
        let stepActResult = 'Server response is 200';
        try {
            await console.log("test")
            await console.log(globalvalues.ratingAPI + "Ratings/TimePeriod/Monthly?RatingSource=" + sourceValue + "&MarketSource=" + marketSource + "&CollectionMethod=" + collectionMethodValue + "&PlaybackType=" + playBackType + "&DistributorCodes=" + distributorCodes + "&ReportableDemographics=" + demoValue + "&SurveyName=" + surveyName + "&StartTime=" + String(actualUpdatedStartTime).replace(":", "%3A").replace(":", "%3A") + "&EndTime=" + String(actualUpdatedEndTime).replace(":", "%3A").replace(":", "%3A") + "&" + weeks + "&" + daysOfWeek + "&AverageBy=" + averageBy + "&Interval=" + interval)
            reqAPI.get({
                "headers": { "content-type": "application/json" },
                "url": globalvalues.ratingAPI + "Ratings/TimePeriod/Monthly?RatingSource=" + sourceValue + "&MarketSource=" + marketSource + "&CollectionMethod=" + collectionMethodValue + "&PlaybackType=" + playBackType + "&DistributorCodes=" + distributorCodes + "&ReportableDemographics=" + demoValue + "&SurveyName=" + surveyName + "&StartTime=" + String(actualUpdatedStartTime).replace(":", "%3A").replace(":", "%3A") + "&EndTime=" + String(actualUpdatedEndTime).replace(":", "%3A").replace(":", "%3A") + "&" + weeks + "&" + daysOfWeek + "&AverageBy=" + averageBy + "&Interval=" + interval,
                "body": JSON.stringify({
                })
            }, async (error, response, body) => {
                if (error) {
                    return console.dir(error);
                }
                responseStatusBody = response.body;
                await console.log(response.body);
                statusCode = response.statusCode;
                if (statusCode != 200) {
                    done();
                    report.ReportStatus(stepAction, '', 'Fail', stepExpResult, stepActResult);
                }
                else {
                    done();
                    report.ReportStatus(stepAction, '', 'Pass', stepExpResult, stepActResult);
                }

            });
        }
        catch (err) {
            report.ReportStatus(stepAction, '', 'Fail', stepExpResult, 'Server response is ' + statusCode);
        }
    });

    it('Validate "Share" "Rating" and "HutPut" Ratings values', async (done) => {
        let stepAction = 'Validate "Share" "Rating" and "HutPut" Ratings values ';
        let stepExpResult = '"Share" "Rating" and "HutPut" Ratings values Should be equal in API and UI';
        let stepActResult;
        let failedCount = 0;
        let errorMsg = "";
        let succesMsg = "";
        let apiRatings;
        await console.log(responseStatusBody);
        let jsonData = JSON.parse(responseStatusBody);
        await console.log(jsonData);
        let dataDetails = jsonData.data;
        if (dataDetails == null || dataDetails.length == 0) {
            report.ReportStatus(stepAction, '', 'Fail', stepExpResult, 'Data details are not displayed.');
            done();
        }
        else {
            apiRatings = await localProg.getRatingsFromAPIAfterCalculation(dataDetails);
            let finalSIU = apiRatings["SIU"];
            let finalShare = apiRatings["SHR"];
            let finalRating = apiRatings["RTG"];
            if (finalSIU.toFixed(3) != uiSIUValue) {
                errorMsg = errorMsg + "HutPut Value in API After Calculation:" + finalSIU.toFixed(3) + "\n HutPut value in UI:" + uiSIUValue + "\n";
                failedCount = failedCount + 1;
            }
            else {
                succesMsg = succesMsg + "UI HutPut:" + uiSIUValue + "\n API Calcualtion HutPut:" + finalSIU.toFixed(3) + "\n";
            }
            if (finalShare.toFixed(3) != uiSHRValue) {
                failedCount = failedCount + 1;
                errorMsg = errorMsg + "Share Value in API:" + finalShare.toFixed(3) + "\n Share value in UI:" + uiSHRValue + "\n";
            }
            else {
                succesMsg = succesMsg + "Share in UI:" + uiSHRValue + "\n API Calcualtion Share:" + finalShare.toFixed(3) + "\n";
            }
            if (finalRating.toFixed(3) != uiRTGValue) {
                failedCount = failedCount + 1;
                errorMsg = errorMsg + "Rate Value in API After Calculation:" + finalRating.toFixed(3) + " \n Rate value in UI:" + uiRTGValue + "\n";
            }
            else {
                succesMsg = succesMsg + "Rating in UI:" + uiRTGValue + "\n API Calcualtion Rating:" + finalRating.toFixed(3) + "\n";
            }
            if (failedCount != 0) {
                done();
                stepActResult = errorMsg + "----" + succesMsg;;
                report.ReportStatus(stepAction, '', 'Fail', stepExpResult, stepActResult);
            }
            else {
                stepActResult = succesMsg;
                done();
                report.ReportStatus(stepAction, '', 'Pass', stepExpResult, stepActResult);
            }
        }
    })

    // --------------Test Step------------
    it("Click on the down arrow beside the book to select that Assign PAV button to assign PAV for the survey books", async () => {

        let stepAction = "Click on the down arrow beside the book to select that Assign PAV button to assign PAV for the survey books";
        let stepData = "";
        let stepExpResult = "Assign PAV window opens with the default selection as per program and PAV records displayed in the grid";
        let stepActualResult = "Assign PAV window opened with the default selection as per program and PAV records displayed in the grid";

        try {
            surveyName="May19"
            await localProg.clickAssignPAVRatingsProgramsTrakingPAVTable("Name", "Name", "Value", "TP", surveyName);
            await verify.verifyProgressBarNotPresent()
            await browser.sleep(1000);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("In Assign PAV window select the required records and select Assign PAV drop down and select Enable PAV only", async () => {

        let stepAction = "In Assign PAV window select the required records and select Assign PAV drop down and select Enable PAV only";
        let stepData = "";
        let stepExpResult = "In the tracks page PAV will be assigned and the TP will be disabled as per selection";
        let stepActualResult = "In the tracks page PAV is assigned and the TP will be disabled as per selection";

        try {
            await localProg.clickRowInPAVPopupGrid(1, 1);
            await localProg.clickonButtonsInPage("xgc-build-pav-assign-button", "Assign");
            await localProg.clickDropdownOptionInventory("xgc-build-pav-assign-button", "Enable PAV only");
            await browser.sleep(2000);
            await localProg.verifyPopupMessageRatecard("Successfully assigned PAV's")
            await verify.verifyProgressBarNotPresent()
            await localProg.verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable("Rationale", "Value", "PAV", "Active", "true", "PAV (1): "+surveyName);
            await localProg.verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable("Name", "Value", "TP", "Active", "false", surveyName);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Repeat above steps and in Assign PAV window select the required records and select Assign PAV drop down and select Enable both PAV and TP", async () => {

        let stepAction = "Repeat above steps and in Assign PAV window select the required records and select Assign PAV drop down and select Enable both PAV and TP";
        let stepData = "";
        let stepExpResult = "In the tracks page PAV will be assigned and both PAV and TP will be enabled";
        let stepActualResult = "In the tracks page PAV is assigned and both PAV and TP are enabled";

        try {
            await localProg.clickAssignPAVRatingsProgramsTrakingPAVTable("Name", "Name", "Value", "PAV", surveyName);
            await verify.verifyProgressBarNotPresent()
            await browser.sleep(1000);
            await localProg.clickonButtonsInPage("xgc-build-pav-assign-button", "Assign");
            await localProg.clickDropdownOptionInventory("xgc-build-pav-assign-button", "Enable both PAV and TP records");
            await browser.sleep(2000);
            await localProg.verifyPopupMessageRatecard("Successfully assigned PAV's")
            await verify.verifyProgressBarNotPresent()
            await localProg.verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable("Rationale", "Value", "PAV", "Active", "true", "PAV (1): "+surveyName);
            await localProg.verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable("Name", "Value", "TP", "Active", "true", surveyName);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

});
