/*
***********************************************************************************************************
* @Script Name :  XGCT-8570
* @Description :  xG Campaign - Inventory - Refactor:  Remove PAV Button Options for Program Average in Program Rating Tracks page2-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 2-8-2019
* @Author : Vikas
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/


//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { Gutils } from '../../../../../Utility/gutils';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { ProgramRatingTracks } from '../../../../../Pages/Inventory/ProgramRatingTracks';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let globalFunc = new globalvalues();
let gUtils = new Gutils();
let HomePage = new HomePageFunction();
let localProgPage = new LocalProgramming();
let programTracksPage = new ProgramRatingTracks();


//Variables Declaration
let currentDate = gUtils.getcurrentDate();
let endDate = gUtils.getcurrentDateAfterSomeDays(10);
let ramdonValue = new Date().toLocaleString().split('/').join('').split(' ').join('').split(',').join('').split(':').join('').split('AM').join('').split('PM').join('');
let TestCase_ID = 'XGCT-8570'
let TestCase_Title = 'xG Campaign - Inventory - Refactor:  Remove PAV Button Options for Program Average in Program Rating Tracks page'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - INVENTORY - REFACTOR:  REMOVE PAV BUTTON OPTIONS FOR PROGRAM AVERAGE IN PROGRAM RATING TRACKS PAGE', () => {


    // --------------Test Step------------
    it("Navigate to URL", async () => {

        let stepAction = "Navigate to URL";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "User should be able to navigate to xGcampaign webpage";
        let stepActualResult = "User should be able to navigate to xGcampaign webpage";

        try {
            await globalFunc.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Link ''Link'' against Selling Title ''Convos With My 2-Year-Old''.'", async () => {

        let stepAction = "Click on Link ''Link'' against Selling Title ''Convos With My 2-Year-Old''.'";
        let stepData = "";
        let stepExpResult = "'User should be able to navigate to ''Program Rating Tracks'' page'";
        let stepActualResult = "'User should be able to navigate to ''Program Rating Tracks'' page'";

        try {
            await HomePage.appclickOnNavigationList("Programming");
            await HomePage.appclickOnMenuListLeftPanel("Local Programming");
            await localProgPage.clickonButtonsRatecardPage("xgc-add-program-orbit-button","Add");
            await localProgPage.clickDropdownOptionInventory("Local Program");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("channel");
            await localProgPage.clickMultiSelectOptionInventoryAddProgram("WPGH");
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await localProgPage.enterTextInInputInventoryAddProgram("xgc-program-name", "ATA" + ramdonValue + "XGCT8570");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("daypart");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.clickDaysOptionShortCutActiveInactive("xgc-days-day-picker","INACTIVE","M-F");
            await localProgPage.clickClockIconTimeOptionInventorydialog("xgc-starttime-time-picker");
            await localProgPage.clickTimeInInventorydialogTimetable("05:00");
            await localProgPage.clickTimeInInventorydialogTimetable("05:00");
            await localProgPage.clickClockIconTimeOptionInventorydialog("xgc-endtime-time-picker");
            await localProgPage.clickTimeInInventorydialogTimetable("06:00");
            await localProgPage.clickTimeInInventorydialogTimetable("06:00");
            await localProgPage.enterDateInInputInventoryAddProgram("xgc-telecast-startdate-date-picker", currentDate);
            await localProgPage.enterDateInInputInventoryAddProgram("xgc-telecast-enddate-date-picker", endDate);
            await localProgPage.clickonButtonsRatecardPage("xgc-save-local-program","Save");
            await browser.sleep(600000);
            
            //await localProgPage.verifyPopupMessageRatecard("Successfully added");
            await browser.refresh();
            await browser.sleep(1000);
            await programTracksPage.clickComponentPdropdown("xgc-markets");
            await programTracksPage.selectOptionsFromComponentPdropdown("Pittsburgh");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-channels");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-dayparts");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await localProgPage.clickonButtonsRatecardPage("xgc-search-button","Search");
            await localProgPage.enterTextInGlobalFilter("ATA" + ramdonValue + "XGCT8570");
            await localProgPage.clickProgramLinkInLocalProgramGrid("Program Track", "Selling Title","ATA" + ramdonValue + "XGCT8570");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select any one program from the list.", async () => {

        let stepAction = "Select any one program from the list.";
        let stepData = "Ex: NBALive";
        let stepExpResult = "Filters should display";
        let stepActualResult = "Filters should display";

        try {
            await programTracksPage.clickProgramInRatingsTracksView("ATA" + ramdonValue + "XGCT8570");
             //await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-collection", "LPM");
            //await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-sample", "HWC Universe");
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + Same Day (LS)");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on 'Rating Build' option and select 'Delete Program Average' option from dropdown", async () => {

        let stepAction = "Click on 'Rating Build' option and select 'Delete Program Average' option from dropdown";
        let stepData = "";
        let stepExpResult = "'Remove Program Averages' Pop up should open";
        let stepActualResult = "'Remove Program Averages' Pop up should open";

        try {

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check 'Remove' button is presented in 'Delete PAV' page", async () => {

        let stepAction = "Check 'Remove' button is presented in 'Delete PAV' page";
        let stepData = "";
        let stepExpResult = "'Remove' button is available";
        let stepActualResult = "'Remove' button is available";

        try {

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on 'Remove' button and check 'Remove' and 'Remove and Close' options are presented for removing program average", async () => {

        let stepAction = "Click on 'Remove' button and check 'Remove' and 'Remove and Close' options are presented for removing program average";
        let stepData = "";
        let stepExpResult = "Both the options are used to remove program average";
        let stepActualResult = "Both the options are used to remove program average";

        try {

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a survey that has 'Value'-'TP' from the grid and click on 'Assign PAV' link'", async () => {

        let stepAction = "Select a survey that has 'Value'-'TP' from the grid and click on 'Assign PAV' link'";
        let stepData = "Ex: May19";
        let stepExpResult = "'Program Track-Build PAV-City of Dreams' (City of Dreams value changes as per the program selection) pop up page should open.";
        let stepActualResult = "'Program Track-Build PAV-City of Dreams' (City of Dreams value changes as per the program selection) pop up page should open.";

        try {
            //await programTracksPage.clickToggaleProgramsTracksPAVTable("Active", "Name", "Value", "TP", "May19", "CHECK");
            await programTracksPage.clickAssignPAVRatingsProgramsTrakingPAVTable("Name", "Name", "Value", "TP", "May19");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check all the required fields are loaded as per the program selection", async () => {

        let stepAction = "Check all the required fields are loaded as per the program selection";
        let stepData = "";
        let stepExpResult = "Books should display automatically as per the data in the fields";
        let stepActualResult = "Books should display automatically as per the data in the fields";

        try {
            await browser.sleep(1000);
            await programTracksPage.clickRowInPAVPopupGrid(1, 1);
            await programTracksPage.clickRowInPAVPopupGrid(2, 1);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select one record and click on 'Assign' button and select 'Enable PAV only' option", async () => {

        let stepAction = "Select one record and click on 'Assign' button and select 'Enable PAV only' option";
        let stepData = "Enable PAV only";
        let stepExpResult = "PAV should be assigned to the survey successfully";
        let stepActualResult = "PAV should be assigned to the survey successfully";

        try {
            await programTracksPage.clickonButtonsInPage("xgc-build-pav-assign-button","Assign");
            await programTracksPage.clickDropdownOptionInventory("xgc-build-pav-assign-button","Enable PAV only");
            await browser.sleep(2000);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Repeat the above step to assign PAV for multiple surevys", async () => {

        let stepAction = "Repeat the above step to assign PAV for multiple surevys";
        let stepData = "Ex: Nov19";
        let stepExpResult = "PAV should be assigned to the surveys successfully";
        let stepActualResult = "PAV should be assigned to the surveys successfully";

        try {
            await browser.refresh();
            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent();
            await programTracksPage.clickProgramInRatingsTracksView("ATA" + ramdonValue + "XGCT8570");
            //await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-collection", "LPM");
           //await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-sample", "HWC Universe");
           await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + One (L1)");
            
           
            await verify.verifyProgressBarNotPresent();
            //await programTracksPage.clickToggaleProgramsTracksPAVTable("Active", "Name", "Value", "TP", "May19", "CHECK");
            await programTracksPage.clickAssignPAVRatingsProgramsTrakingPAVTable("Name", "Name", "Value", "TP", "May19");
            await browser.sleep(1000);
            await programTracksPage.clickRowInPAVPopupGrid(1, 1);
            await programTracksPage.clickRowInPAVPopupGrid(2, 1);
            await programTracksPage.clickonButtonsInPage("xgc-build-pav-assign-button","Assign");
            await programTracksPage.clickDropdownOptionInventory("xgc-build-pav-assign-button","Enable PAV only");
          
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on 'Rating Build' option and select 'Delete Program Averages' option from dropdown", async () => {

        let stepAction = "Click on 'Rating Build' option and select 'Delete Program Averages' option from dropdown";
        let stepData = "";
        let stepExpResult = "'Remove Program Averages' Pop up should open";
        let stepActualResult = "'Remove Program Averages' Pop up should open";

        try {
            await programTracksPage.clickonButtonsInPage("xgc-btn-rating-build","Rating Build");
            await programTracksPage.clickDropdownOptionInventory("xgc-btn-rating-build","Remove Program Averages");
            
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select multiple surveys from 'Survey(s)' dropdown. 'Assign to all playback types' check box is optional according to the scenario. Click on 'Remove' button and select 'Remove' option from the dropdown", async () => {

        let stepAction = "Select multiple surveys from 'Survey(s)' dropdown. 'Assign to all playback types' check box is optional according to the scenario. Click on 'Remove' button and select 'Remove' option from the dropdown";
        let stepData = "Ex: May19, May19, Nov19";
        let stepExpResult = "PAV should be removed for all the selected surveys";
        let stepActualResult = "PAV should be removed for all the selected surveys";

        try {
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-dropdown-program-average");
            await programTracksPage.selectOptionFromMultiselect("May19", "Survey(s)");
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("'Check if user selects the ''Remove'' option, the program average window remains in the screen view for the user'", async () => {

        let stepAction = "'Check if user selects the ''Remove'' option, the program average window remains in the screen view for the user'";
        let stepData = "";
        let stepExpResult = "The pop up screen shouldn't get closed if user selects Remove option";
        let stepActualResult = "The pop up screen shouldn't get closed if user selects Remove option";

        try {
            await programTracksPage.clickonButtonsInPage("xgc-btn-remove","Remove");
            await programTracksPage.verifydropdownOptionInventory("xgc-btn-remove","Remove");
            await programTracksPage.verifydropdownOptionInventory("xgc-btn-remove","Remove and Close");
            await programTracksPage.clickDropdownOptionInventory("xgc-btn-remove","Remove");
            await programTracksPage.clickonButtonsInPage("xgc-btn-clear","Cancel");
            
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("In 'Program Rating Tracks' page, click on 'Rating Build' option and select 'Delete Program Average' option from dropdown", async () => {

        let stepAction = "In 'Program Rating Tracks' page, click on 'Rating Build' option and select 'Delete Program Average' option from dropdown";
        let stepData = "";
        let stepExpResult = "'Remove Program Averages' Pop up should open";
        let stepActualResult = "'Remove Program Averages' Pop up should open";

        try {
           // await programTracksPage.clickToggaleProgramsTracksPAVTable("Active", "Name", "Value", "TP", "May19", "CHECK");
            await programTracksPage.clickAssignPAVRatingsProgramsTrakingPAVTable("Name", "Name", "Value", "TP", "May19");
            await browser.sleep(1000);
            await programTracksPage.clickRowInPAVPopupGrid(1, 1);
            await programTracksPage.clickRowInPAVPopupGrid(2, 1);
            await programTracksPage.clickonButtonsInPage("xgc-build-pav-assign-button","Assign");
            await programTracksPage.clickDropdownOptionInventory("xgc-build-pav-assign-button","Enable PAV only");
            
            
           
           
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select surveys from 'Survey(s)' dropdown. 'Assign to all playback types' check box is optional according to the scenario. Click on 'Remove' button and Check 'Remove and Close' is presented as second option", async () => {

        let stepAction = "Select surveys from 'Survey(s)' dropdown. 'Assign to all playback types' check box is optional according to the scenario. Click on 'Remove' button and Check 'Remove and Close' is presented as second option";
        let stepData = "Ex: Dec18";
        let stepExpResult = "PAV should be removed for the surveys and the pop up should be closed automatically and returns to Program Rating Tracks page";
        let stepActualResult = "PAV should be removed for the surveys and the pop up should be closed automatically and returns to Program Rating Tracks page";

        try {
            await browser.refresh();
            await programTracksPage.clickProgramInRatingsTracksView("ATA" + ramdonValue + "XGCT8570");
             //await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-collection", "LPM");
            //await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-sample", "HWC Universe");
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + Same Day (LS)");
            await programTracksPage.clickonButtonsInPage("xgc-btn-rating-build","Rating Build");
            await programTracksPage.clickDropdownOptionInventory("xgc-btn-rating-build","Remove Program Averages");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-dropdown-program-average");
            await programTracksPage.selectOptionFromMultiselect("May19", "Survey(s)");
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.selectCheckBox("Assign to All Playback Types");
            await programTracksPage.clickonButtonsInPage("xgc-btn-remove","Remove");
            await programTracksPage.verifydropdownOptionInventory("xgc-btn-remove","Remove");
            await programTracksPage.verifydropdownOptionInventory("xgc-btn-remove","Remove and Close");
            await programTracksPage.clickDropdownOptionInventory("xgc-btn-remove","Remove");
            await programTracksPage.clickonButtonsInPage("xgc-btn-clear","Cancel");
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the PAV record for the survey is removed successfully", async () => {

        let stepAction = "Check the PAV record for the survey is removed successfully";
        let stepData = "";
        let stepExpResult = "Details of PAV record for the selected survey should not present in the grid";
        let stepActualResult = "Details of PAV record for the selected survey should not present in the grid";

        try {
            await programTracksPage.verifyDataIsNotPresentInAssignPAVRatingsProgramsTraksPAVTable("Name", "Value", 'PAV', "May19");
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + One (L1)");
            await verify.verifyProgressBarNotPresent();
            await programTracksPage.verifyDataIsNotPresentInAssignPAVRatingsProgramsTraksPAVTable("Name", "Value", 'PAV', "May19");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
