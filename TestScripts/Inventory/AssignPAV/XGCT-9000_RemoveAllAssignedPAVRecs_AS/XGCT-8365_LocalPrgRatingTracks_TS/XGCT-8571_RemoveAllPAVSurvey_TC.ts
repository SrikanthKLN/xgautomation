/*
***********************************************************************************************************
* @Script Name :  XGCT-8571
* @Description :  xG Campaign - Inventory - Refactor: erify the system removes all program average records for each survey by playback type for the program if 'Survey' option and 'Assign to All Playback Types' option is selected5-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 5-8-2019
* @Author : Vikas
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { Gutils } from '../../../../../Utility/gutils';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { ProgramRatingTracks } from '../../../../../Pages/Inventory/ProgramRatingTracks';



//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let gUtils = new Gutils();
let globalFunc = new globalvalues();
let HomePage = new HomePageFunction();
let localProgPage = new LocalProgramming();
let programTracksPage = new ProgramRatingTracks();


//Variables Declaration
let currentDate = gUtils.getcurrentDate();
let endDate = gUtils.getcurrentDateAfterSomeDays(10);
let ramdonValue = new Date().toLocaleString().split('/').join('').split(' ').join('').split(',').join('').split(':').join('').split('AM').join('').split('PM').join('');
let TestCase_ID = 'XGCT-8571'
let TestCase_Title = 'xG Campaign - Inventory - Refactor: erify the system removes all program average records for each survey by playback type for the program if Survey option and Assign to All Playback Types option is selected'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - INVENTORY - REFACTOR: ERIFY THE SYSTEM REMOVES ALL PROGRAM AVERAGE RECORDS FOR EACH SURVEY BY PLAYBACK TYPE FOR THE PROGRAM IF SURVEY OPTION AND ASSIGN TO ALL PLAYBACK TYPES OPTION IS SELECTED', () => {


    // --------------Test Step------------
    it("Open qaxgstorybook url", async () => {

        let stepAction = "Open 'qaxgstorybook' url";
        let stepData = "http://qaxgstorybook.azurewebsites.net/";
        let stepExpResult = "User should see the home page 'qaxgstorybook'";
        let stepActualResult = "User should see the home page 'qaxgstorybook'";

        try {
            await globalFunc.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click left menu 'Inventory'-->'Local Program Rating Tracks View'", async () => {

        let stepAction = "Click left menu 'Inventory'-->'Local Program Rating Tracks View'";
        let stepData = "";
        let stepExpResult = "User should see 'Program Rating Tracks' page.";
        let stepActualResult = "User should see 'Program Rating Tracks' page.";

        try {
            
            await HomePage.appclickOnNavigationList("Programming");
            await HomePage.appclickOnMenuListLeftPanel("Local Programming");
            await localProgPage.clickonButtonsRatecardPage("xgc-add-program-orbit-button","Add");
            await localProgPage.clickDropdownOptionInventory("Local Program");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("channel");
            await localProgPage.clickMultiSelectOptionInventoryAddProgram("WPGH");
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await localProgPage.enterTextInInputInventoryAddProgram("xgc-program-name", "ATA" + ramdonValue + "XGCT8571");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("daypart");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.clickDaysOptionShortCutActiveInactive("xgc-days-day-picker","INACTIVE","M-F");
            await localProgPage.clickClockIconTimeOptionInventorydialog("xgc-starttime-time-picker");
            await localProgPage.clickTimeInInventorydialogTimetable("05:00");
            await localProgPage.clickTimeInInventorydialogTimetable("05:00");
            await localProgPage.clickClockIconTimeOptionInventorydialog("xgc-endtime-time-picker");
            await localProgPage.clickTimeInInventorydialogTimetable("06:00");
            await localProgPage.clickTimeInInventorydialogTimetable("06:00");
            await localProgPage.enterDateInInputInventoryAddProgram("xgc-telecast-startdate-date-picker", currentDate);
            await localProgPage.enterDateInInputInventoryAddProgram("xgc-telecast-enddate-date-picker", endDate);
            await localProgPage.clickonButtonsRatecardPage("xgc-save-local-program","Save");
            await browser.sleep(600000);
            await browser.refresh();
            await browser.sleep(1000);
            await programTracksPage.clickComponentPdropdown("xgc-markets");
            await programTracksPage.selectOptionsFromComponentPdropdown("Pittsburgh");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-channels");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-dayparts");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await localProgPage.clickonButtonsRatecardPage("xgc-search-button","Search");
            await localProgPage.enterTextInGlobalFilter("ATA" + ramdonValue + "XGCT8571");
            await localProgPage.clickProgramLinkInLocalProgramGrid("Program Track", "Selling Title", "ATA" + ramdonValue + "XGCT8571");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select any one program from the list.", async () => {

        let stepAction = "Select any one program from the list.";
        let stepData = "Ex: Breaking Bad";
        let stepExpResult = "Filters should display";
        let stepActualResult = "Filters should display";

        try {
            await programTracksPage.clickProgramInRatingsTracksView("ATA" + ramdonValue + "XGCT8571");
             //await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-collection", "LPM");
            //await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-sample", "HWC Universe");
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + Same Day (LS)");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a survey that has 'Value'-'TP' from the grid and click on 'Assign PAV' link", async () => {

        let stepAction = "Select a survey that has 'Value'-'TP' from the grid and click on 'Assign PAV' link";
        let stepData = "Ex: Jan19";
        let stepExpResult = "'Program Track-Build PAV-City of Dreams' (City of Dreams value changes as per the program selection) pop up page should open.";
        let stepActualResult = "'Program Track-Build PAV-City of Dreams' (City of Dreams value changes as per the program selection) pop up page should open.";

        try {
            //await programTracksPage.clickToggaleProgramsTracksPAVTable("Active", "Name", "Value", "TP", "May19", "CHECK");
            await programTracksPage.clickAssignPAVRatingsProgramsTrakingPAVTable("Name", "Name", "Value", "TP", "May19");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check all the required fields are loaded as per the program selection", async () => {

        let stepAction = "Check all the required fields are loaded as per the program selection";
        let stepData = "";
        let stepExpResult = "Books should display automatically as per the data in the fields";
        let stepActualResult = "Books should display automatically as per the data in the fields";

        try {

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select one record and click on 'Assign' button and select 'Enable PAV only' option", async () => {

        let stepAction = "Select one record and click on 'Assign' button and select 'Enable PAV only' option";
        let stepData = "";
        let stepExpResult = "PAV should be assigned to the survey successfully";
        let stepActualResult = "PAV should be assigned to the survey successfully";

        try {
            await browser.sleep(1000);
            await programTracksPage.clickRowInPAVPopupGrid(1, 1);
            await programTracksPage.clickonButtonsInPage("xgc-build-pav-assign-button","Assign");
            await programTracksPage.clickDropdownOptionInventory("xgc-build-pav-assign-button","Enable PAV only");
            await browser.sleep(2000);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Repeat the above step to 'Assign PAV' for multiple records for the same playback type", async () => {

        let stepAction = "Repeat the above step to 'Assign PAV' for multiple records for the same playback type";
        let stepData = "Live+Same Day(LS)";
        let stepExpResult = "PAV should be assigned successfully";
        let stepActualResult = "PAV should be assigned successfully";

        try {
            //await programTracksPage.clickToggaleProgramsTracksPAVTable("Active", "Name", "Value", "TP", "Mar19", "CHECK");
            await programTracksPage.clickAssignPAVRatingsProgramsTrakingPAVTable("Name", "Name", "Value", "TP", "Mar19");
            await browser.sleep(1000);
            await programTracksPage.clickRowInPAVPopupGrid(1, 1);
            await programTracksPage.clickRowInPAVPopupGrid(2, 1);
            await programTracksPage.clickonButtonsInPage("xgc-build-pav-assign-button","Assign");
            await programTracksPage.clickDropdownOptionInventory("xgc-build-pav-assign-button","Enable PAV only");
            await browser.sleep(2000);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("In 'Program Rating Tracks' page, click on 'Rating Build' option and select 'Remove Program Average' option from dropdown", async () => {

        let stepAction = "In 'Program Rating Tracks' page, click on 'Rating Build' option and select 'Remove Program Average' option from dropdown";
        let stepData = "";
        let stepExpResult = "'Remove Program Averages' Pop up should open";
        let stepActualResult = "'Remove Program Averages' Pop up should open";

        try {
            await programTracksPage.clickonButtonsInPage("xgc-btn-rating-build","Rating Build");
            await programTracksPage.clickDropdownOptionInventory("xgc-btn-rating-build","Remove Program Averages");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-dropdown-program-average");
            await programTracksPage.selectOptionFromMultiselect("Mar19", "Survey(s)");
            await programTracksPage.selectOptionFromMultiselect("May19", "Survey(s)");
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("'Assign to all playback types'- do not select this option.Click on 'Remove' button and select 'Remove and Close' option'", async () => {

        let stepAction = "'Assign to all playback types'- do not select this option.Click on 'Remove' button and select 'Remove and Close' option'";
        let stepData = "";
        let stepExpResult = "System should remove PAV records for each survey for the program for that playback type only";
        let stepActualResult = "System should remove PAV records for each survey for the program for that playback type only";

        try {
            await programTracksPage.clickonButtonsInPage("xgc-btn-remove","Remove");
            await programTracksPage.verifydropdownOptionInventory("xgc-btn-remove","Remove");
            await programTracksPage.verifydropdownOptionInventory("xgc-btn-remove","Remove and Close");
            await programTracksPage.clickDropdownOptionInventory("xgc-btn-remove","Remove and Close");
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("In 'Program Rating Tracks' page check each survey does not have PAV record for the program for that playback type", async () => {

        let stepAction = "In 'Program Rating Tracks' page check each survey does not have PAV record for the program for that playback type";
        let stepData = "";
        let stepExpResult = "PAV records should be deleted successfully";
        let stepActualResult = "PAV records should be deleted successfully";

        try {
            await programTracksPage.verifyDataIsNotPresentInAssignPAVRatingsProgramsTraksPAVTable("Name", "Value", 'PAV', "Mar19");
            await programTracksPage.verifyDataIsNotPresentInAssignPAVRatingsProgramsTraksPAVTable("Name", "Value", 'PAV', "May19");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select one record and click on 'Assign' button and select 'Enable both PAV and TP records' option", async () => {

        let stepAction = "Select one record and click on 'Assign' button and select 'Enable both PAV and TP records' option";
        let stepData = "";
        let stepExpResult = "PAV should be assigned to the survey successfully";
        let stepActualResult = "PAV should be assigned to the survey successfully";

        try {
            //await programTracksPage.clickToggaleProgramsTracksPAVTable("Active", "Name", "Value", "TP", "Mar19", "CHECK");
            await programTracksPage.clickAssignPAVRatingsProgramsTrakingPAVTable("Name", "Name", "Value", "TP", "Mar19");
            await browser.sleep(1000);
            await programTracksPage.clickRowInPAVPopupGrid(1, 1);
            await programTracksPage.clickonButtonsInPage("xgc-build-pav-assign-button","Assign");
            await programTracksPage.clickDropdownOptionInventory("xgc-build-pav-assign-button","Enable both PAV and TP records");
            await browser.sleep(2000);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("In 'Program Rating Tracks' page, click on 'Rating Build' option and select 'Delete Program Average' option from dropdown", async () => {

        let stepAction = "In 'Program Rating Tracks' page, click on 'Rating Build' option and select 'Delete Program Average' option from dropdown";
        let stepData = "";
        let stepExpResult = "'Remove Program Averages' Pop up should open";
        let stepActualResult = "'Remove Program Averages' Pop up should open";

        try {
            await browser.refresh();
            await browser.sleep(3000);
            await programTracksPage.clickProgramInRatingsTracksView("ATA" + ramdonValue + "XGCT8571");
            //await programTracksPage.clickDataInRatingSourceTableTarcksPage("Collection Method", "LPM");
            //await programTracksPage.clickDataInRatingSourceTableTarcksPage("Sample", "HWC Universe");
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + Same Day (LS)");
            await programTracksPage.clickonButtonsInPage("xgc-btn-rating-build","Rating Build");
            await programTracksPage.clickDropdownOptionInventory("xgc-btn-rating-build","Remove Program Averages");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-dropdown-program-average");
            await programTracksPage.selectOptionFromMultiselect("Mar19", "Survey(s)");
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
           
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on 'Remove' button and select 'Remove and Close' option'", async () => {

        let stepAction = "Click on 'Remove' button and select 'Remove and Close' option'";
        let stepData = "";
        let stepExpResult = "System should remove PAV records for each survey by playback type for the program";
        let stepActualResult = "System should remove PAV records for each survey by playback type for the program";

        try {
            await programTracksPage.clickonButtonsInPage("xgc-btn-remove","Remove");
            await programTracksPage.clickDropdownOptionInventory("xgc-btn-remove","Remove and Close");
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("In 'Program Rating Tracks' page check each survey by playback type does not have PAV record for the program", async () => {

        let stepAction = "In 'Program Rating Tracks' page check each survey by playback type does not have PAV record for the program";
        let stepData = "";
        let stepExpResult = "PAV records should be deleted successfully for the program for all playback types";
        let stepActualResult = "PAV records should be deleted successfully for the program for all playback types";

        try {
            await programTracksPage.verifyDataIsNotPresentInAssignPAVRatingsProgramsTraksPAVTable("Name", "Value", 'PAV', "Mar19");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
