/*
***********************************************************************************************************
* @Script Name :  XGCT-8389
* @Description :  xG Campaign - Inventory - Refactor: Local Program Rating Tracks view - Assign PAV: Verify Search Option for various fields9-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 9-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { ProgramRatingTracks } from '../../../../../Pages/Inventory/ProgramRatingTracks';
import { Gutils } from '../../../../../Utility/gutils';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let globalFunc = new globalvalues();
let HomePage = new HomePageFunction();
let localProgPage = new LocalProgramming();
let programTracksPage = new ProgramRatingTracks();
let gUtils = new Gutils();



let currentDate = gUtils.getcurrentDate();
let endDate = gUtils.getcurrentDateAfterSomeDays(10);
let ramdonValue = new Date().toLocaleString().split('/').join('').split(' ').join('').split(',').join('').split(':').join('').split('AM').join('').split('PM').join('');
let channelValue;
let survey;
//Variables Declaration
let TestCase_ID = 'XGCT-8389'
let TestCase_Title = 'xG Campaign - Inventory - Refactor: Local Program Rating Tracks view - Assign PAV: Verify Search Option for various fields'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - INVENTORY - REFACTOR: LOCAL PROGRAM RATING TRACKS VIEW - ASSIGN PAV: VERIFY SEARCH OPTION FOR VARIOUS FIELDS', () => {



    // --------------Test Step------------
    it("Navigate to xgplatform url", async () => {

        let stepAction = "Navigate to xgplatform url";
        let stepData = "https://xgplatform-dev.myimagine.com/#/programming/localprogramming";
        let stepExpResult = "User should navigate to the Imagine communications page";
        let stepActualResult = "User should navigate to the Imagine communications page";

        try {
            await globalFunc.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Provide 'User Name' and 'Password' and click on Login button", async () => {

        let stepAction = "Provide 'User Name' and 'Password' and click on Login button";
        let stepData = "";
        let stepExpResult = "User should see the home page of the application";
        let stepActualResult = "User should see the home page of the application";

        try {

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on 'Programming' icon on top of the page. Click on 'Local Programming' option on the left", async () => {

        let stepAction = "Click on 'Programming' icon on top of the page. Click on 'Local Programming' option on the left";
        let stepData = "";
        let stepExpResult = "'Local Programs' page should open and all the necessary details should display";
        let stepActualResult = "'Local Programs' page should open and all the necessary details should display";

        try {
            await HomePage.appclickOnNavigationList("Programming");
            await HomePage.appclickOnMenuListLeftPanel("Local Programming");
            await localProgPage.clickonButtonsRatecardPage("xgc-add-program-orbit-button","Add");
            await localProgPage.clickDropdownOptionInventory("Local Program");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("channel");
            await localProgPage.clickMultiSelectOptionInventoryAddProgram("WPGH");
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await localProgPage.enterTextInInputInventoryAddProgram("xgc-program-name", "ATA" + ramdonValue + "XGCT8389");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("daypart");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.clickDaysOptionShortCutActiveInactive("xgc-days-day-picker","INACTIVE","M-F");
            await localProgPage.clickClockIconTimeOptionInventorydialog("xgc-starttime-time-picker");
            await localProgPage.clickTimeInInventorydialogTimetable("05:00");
            await localProgPage.clickTimeInInventorydialogTimetable("05:00");
            await localProgPage.clickClockIconTimeOptionInventorydialog("xgc-endtime-time-picker");
            await localProgPage.clickTimeInInventorydialogTimetable("06:00");
            await localProgPage.clickTimeInInventorydialogTimetable("06:00");
            await localProgPage.enterDateInInputInventoryAddProgram("xgc-telecast-startdate-date-picker", currentDate);
            await localProgPage.enterDateInInputInventoryAddProgram("xgc-telecast-enddate-date-picker", endDate);
            await localProgPage.clickonButtonsRatecardPage("xgc-save-local-program","Save");
            await browser.sleep(600000);
            
            //await localProgPage.verifyPopupMessageRatecard("Successfully added")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select any one program from the grid display and click on 'Link' in 'Link' column of that particular program", async () => {

        let stepAction = "Select any one program from the grid display and click on 'Link' in 'Link' column of that particular program";
        let stepData = "";
        let stepExpResult = "'Local Programs' page should open and all the necessary details should display";
        let stepActualResult = "'Local Programs' page should open and all the necessary details should display";

        try {
            await browser.refresh();
            await browser.sleep(1000);
            await programTracksPage.clickComponentPdropdown("xgc-markets");
            await programTracksPage.selectOptionsFromComponentPdropdown("Pittsburgh");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-channels");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-dayparts");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await localProgPage.clickonButtonsRatecardPage("xgc-search-button","Search");
            await localProgPage.enterTextInGlobalFilter("ATA" + ramdonValue + "XGCT8389");
            await browser.sleep(1000);
            await localProgPage.clickProgramLinkInLocalProgramGrid("Program Track", "Selling Title", "ATA" + ramdonValue + "XGCT8389");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select any one program from the grid display and click on 'Program Tracks' link in 'Rating Track' column of that particular program", async () => {

        let stepAction = "Select any one program from the grid display and click on 'Program Tracks' link in 'Rating Track' column of that particular program";
        let stepData = "";
        let stepExpResult = "'Program Rating Tracks' page should display.";
        let stepActualResult = "'Program Rating Tracks' page should display.";

        try {
            await programTracksPage.clickProgramInRatingsTracksView("ATA" + ramdonValue + "XGCT8389");
             //await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-collection", "LPM");
            //await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-sample", "HWC Universe");
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + Same Day (LS)");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the program is selected by default in 'Programs' list and all the filters are loaded according to the program selection", async () => {

        let stepAction = "Check the program is selected by default in 'Programs' list and all the filters are loaded according to the program selection";
        let stepData = "";
        let stepExpResult = "Survey books should be loaded in the grid according to the filter selection";
        let stepActualResult = "Survey books should be loaded in the grid according to the filter selection";

        try {

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select any one survey from 'Name' column and click on the dropdown icon and click on 'Assign PAV' option", async () => {

        let stepAction = "Select any one survey from 'Name' column and click on the dropdown icon and click on 'Assign PAV' option";
        let stepData = "Ex: Jun19";
        let stepExpResult = "'Assign Program Average(PAV)' pop up page should open";
        let stepActualResult = "'Assign Program Average(PAV)' pop up page should open";

        try {
            //await programTracksPage.clickToggaleProgramsTracksPAVTable("Active", "Name", "Value", "TP", "Mar19", "CHECK");
            await programTracksPage.clickAssignPAVRatingsProgramsTrakingPAVTable("Name", "Name", "Value", "TP", "Mar19");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the channel selection for Multi selection", async () => {

        let stepAction = "Check the channel selection for Multi selection";
        let stepData = "";
        let stepExpResult = "Channel should allow to multi-seelct";
        let stepActualResult = "Channel should allow to multi-seelct";

        try {
            await programTracksPage.verifyXgmultiselectSelectionValue("channel", "WPGH");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-build-pav-channels");
            channelValue = await programTracksPage.retunComboboxMultiselectOptionAtIndex("xgc-build-pav-channels", 2)
            await programTracksPage.selectOptionFromMultiselect(channelValue, "Channel(s)");
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.verifyXgmultiselectSelectionValue("channel", "WPGH, " + channelValue);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Multi-select Channel from drop options.", async () => {

        let stepAction = "Multi-select Channel from drop options.";
        let stepData = "";
        let stepExpResult = "When user selects multiple channels, grid should display Channel column with the display of multiple channels.";
        let stepActualResult = "When user selects multiple channels, grid should display Channel column with the display of multiple channels.";

        try {
            await verify.verifyProgressBarNotPresent();
            await programTracksPage.clickonButtonsInPage("xgc-build-pav-search","Search");
            await verify.verifyProgressBarNotPresent();
            await programTracksPage.verifyDataInXgcbuildPavRatingDataGrid("Channel", channelValue)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the survey dropdown for multi-select options", async () => {

        let stepAction = "Check the survey dropdown for multi-select options";
        let stepData = "";
        let stepExpResult = "Survey should allow for multi selection.";
        let stepActualResult = "Survey should allow for multi selection.";

        try {
            await programTracksPage.verifyXgmultiselectSelectionValue("programAveragePeriod", "Survey");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-build-pav-program-average-period");
            survey = await programTracksPage.retunComboboxMultiselectOptionAtIndex("xgc-build-pav-program-average-period", 2)
            await programTracksPage.selectOptionFromMultiselect(survey, "Program Average Period");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-build-pav-program-average-period");
            await programTracksPage.verifyXgmultiselectSelectionValue("programAveragePeriod", "Survey, " + survey);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Multi-select Survey from drop options.", async () => {

        let stepAction = "Multi-select Survey from drop options.";
        let stepData = "";
        let stepExpResult = "When user selects multiple surveys, grid should display Survey column with the display of multiple Surveys.";
        let stepActualResult = "When user selects multiple surveys, grid should display Survey column with the display of multiple Surveys.";

        try {
            await verify.verifyProgressBarNotPresent();
            await programTracksPage.clickonButtonsInPage("xgc-build-pav-search","Search");
            await verify.verifyProgressBarNotPresent();
            await programTracksPage.verifyDataInXgcbuildPavRatingDataGrid("Weeks", "1234");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check Start and End time option for searching records.", async () => {

        let stepAction = "Check Start and End time option for searching records.";
        let stepData = "";
        let stepExpResult = "By default, system should display program start and end time.";
        let stepActualResult = "By default, system should display program start and end time.";

        try {
            await programTracksPage.verifyInputFieldText("Start Time", "05:00 AM");
            await programTracksPage.verifyInputFieldText("End Time", "06:00 AM");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check by updating start and End time", async () => {

        let stepAction = "Check by updating start and End time";
        let stepData = "";
        let stepExpResult = "Start and end time should be allowed to update.";
        let stepActualResult = "Start and end time should be allowed to update.";

        try {
            await localProgPage.clickClockIconTimeOptionInventorydialog("xgc-build-pav-start-time");
            await localProgPage.clickTimeInInventorydialogTimetable("06:00");
            await localProgPage.clickTimeInInventorydialogTimetable("06:00");
            await localProgPage.clickClockIconTimeOptionInventorydialog("xgc-build-pav-end-time");
            await localProgPage.clickTimeInInventorydialogTimetable("07:00");
            await localProgPage.clickTimeInInventorydialogTimetable("07:00");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the days selection option in the search options", async () => {

        let stepAction = "Check the days selection option in the search options";
        let stepData = "";
        let stepExpResult = "By default program days should be marked as selected.";
        let stepActualResult = "By default program days should be marked as selected.";

        try {
            await programTracksPage.verifyCheckBoxIsSelected("Mo");
            await programTracksPage.verifyCheckBoxIsSelected("Tu");
            await programTracksPage.verifyCheckBoxIsSelected("We");
            await programTracksPage.verifyCheckBoxIsSelected("Th");
            await programTracksPage.verifyCheckBoxIsSelected("Fr");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Update days selection and check the results", async () => {

        let stepAction = "Update days selection and check the results";
        let stepData = "";
        let stepExpResult = "As per the updated days selection, results should get updated in the grid.";
        let stepActualResult = "As per the updated days selection, results should get updated in the grid.";

        try {
            await programTracksPage.clickDaysOptionShortCutActiveInactive("xgc-build-pav-days","INACTIVE", "All");
            await verify.verifyProgressBarNotPresent();
            await programTracksPage.clickonButtonsInPage("xgc-build-pav-search","Search");
            await verify.verifyProgressBarNotPresent();
            await programTracksPage.verifyDataInXgcbuildPavRatingDataGrid("Days", "Su");
            await programTracksPage.verifyDataInXgcbuildPavRatingDataGrid("Days", "M-Fr");
            await programTracksPage.verifyDataInXgcbuildPavRatingDataGrid("Start Time", "6:00");
            await programTracksPage.verifyDataInXgcbuildPavRatingDataGrid("End Time", "7:00 AM");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
