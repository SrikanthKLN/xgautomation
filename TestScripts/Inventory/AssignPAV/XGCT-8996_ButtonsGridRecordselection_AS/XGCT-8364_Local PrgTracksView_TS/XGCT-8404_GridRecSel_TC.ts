/*
***********************************************************************************************************
* @Script Name :  XGCT-8404
* @Description :  xG Campaign - Inventory - Refactor: Local Program Rating Tracks view - Assign PAV: Verify Grid record selection9-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 9-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/


//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { ProgramRatingTracks } from '../../../../../Pages/Inventory/ProgramRatingTracks';
import { Gutils } from '../../../../../Utility/gutils';



//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let globalFunc = new globalvalues();
let HomePage = new HomePageFunction();
let localProgPage = new LocalProgramming();
let programTracksPage = new ProgramRatingTracks();
let gUtils = new Gutils();



let currentDate = gUtils.getcurrentDate();
let endDate = gUtils.getcurrentDateAfterSomeDays(10);
let ramdonValue = new Date().toLocaleString().split('/').join('').split(' ').join('').split(',').join('').split(':').join('').split('AM').join('').split('PM').join('');


//Variables Declaration
let TestCase_ID = 'XGCT-8404'
let TestCase_Title = 'xG Campaign - Inventory - Refactor: Local Program Rating Tracks view - Assign PAV: Verify Grid record selection'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - INVENTORY - REFACTOR: LOCAL PROGRAM RATING TRACKS VIEW - ASSIGN PAV: VERIFY GRID RECORD SELECTION', () => {

    // --------------Test Step------------
    it("Navigate to xgcampaignwebapp url", async () => {

        let stepAction = "Navigate to xgcampaignwebapp url";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net";
        let stepExpResult = "User should navigate to the xgcampaignwebapp login page";
        let stepActualResult = "User should navigate to the xgcampaignwebapp login page";

        try {
            await globalFunc.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Provide 'User Name' and 'Password' and click on Login button", async () => {

        let stepAction = "Provide 'User Name' and 'Password' and click on Login button";
        let stepData = "";
        let stepExpResult = "User should see the home page of the application";
        let stepActualResult = "User should see the home page of the application";

        try {

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on 'Programs' icon on top of the page. Click on 'Inventory'-->'Local Programs' option from the dropdown", async () => {

        let stepAction = "Click on 'Programs' icon on top of the page. Click on 'Inventory'-->'Local Programs' option from the dropdown";
        let stepData = "";
        let stepExpResult = "'Local Programs' page should open and all the necessary details should display";
        let stepActualResult = "'Local Programs' page should open and all the necessary details should display";

        try {
            await HomePage.appclickOnNavigationList("Programming");
            await HomePage.appclickOnMenuListLeftPanel("Local Programming");
            await localProgPage.clickonButtonsRatecardPage("xgc-add-program-orbit-button","Add");
            await localProgPage.clickDropdownOptionInventory("Local Program");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("channel");
            await localProgPage.clickMultiSelectOptionInventoryAddProgram("WPGH");
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await localProgPage.enterTextInInputInventoryAddProgram("xgc-program-name", "ATA" + ramdonValue + "XGCT8404");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("daypart");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.clickDaysOptionShortCutActiveInactive("xgc-days-day-picker","INACTIVE","M-F");
            await localProgPage.clickClockIconTimeOptionInventorydialog("xgc-starttime-time-picker");
            await localProgPage.clickTimeInInventorydialogTimetable("05:00");
            await localProgPage.clickTimeInInventorydialogTimetable("05:00");
            await localProgPage.clickClockIconTimeOptionInventorydialog("xgc-endtime-time-picker");
            await localProgPage.clickTimeInInventorydialogTimetable("06:00");
            await localProgPage.clickTimeInInventorydialogTimetable("06:00");
            await localProgPage.enterDateInInputInventoryAddProgram("xgc-telecast-startdate-date-picker", currentDate);
            await localProgPage.enterDateInInputInventoryAddProgram("xgc-telecast-enddate-date-picker", endDate);
            await localProgPage.clickonButtonsRatecardPage("xgc-save-local-program","Save");
            await browser.sleep(600000);

            //await localProgPage.verifyPopupMessageRatecard("Successfully added")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select any one program from the grid display and click on 'Program Tracks' link in 'Rating Track' column of that particular program", async () => {

        let stepAction = "Select any one program from the grid display and click on 'Program Tracks' link in 'Rating Track' column of that particular program";
        let stepData = "";
        let stepExpResult = "'Program Rating Tracks' page should display.";
        let stepActualResult = "'Program Rating Tracks' page should display.";

        try {
            await browser.refresh();
            await browser.sleep(1000);
            await programTracksPage.clickComponentPdropdown("xgc-markets");
            await programTracksPage.selectOptionsFromComponentPdropdown("Pittsburgh");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-channels");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-dayparts");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await localProgPage.clickonButtonsRatecardPage("xgc-search-button","Search");
            await localProgPage.enterTextInGlobalFilter("ATA" + ramdonValue + "XGCT8404");
            await browser.sleep(1000);
            await localProgPage.clickProgramLinkInLocalProgramGrid("Program Track", "Selling Title", "ATA" + ramdonValue + "XGCT8404");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the program is selected by default in 'Programs' list and all the filters are loaded according to the program selection", async () => {

        let stepAction = "Check the program is selected by default in 'Programs' list and all the filters are loaded according to the program selection";
        let stepData = "";
        let stepExpResult = "Survey books should be loaded in the grid according to the filter selection";
        let stepActualResult = "Survey books should be loaded in the grid according to the filter selection";

        try {
            await programTracksPage.clickProgramInRatingsTracksView("ATA" + ramdonValue + "XGCT8404");
            //await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-collection", "LPM");
            //await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-sample", "HWC Universe");
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + Same Day (LS)");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select any one survey from 'Name' column and click on the dropdown icon and click on 'Assign PAV' option", async () => {

        let stepAction = "Select any one survey from 'Name' column and click on the dropdown icon and click on 'Assign PAV' option";
        let stepData = "Ex: Jun19";
        let stepExpResult = "'Assign Program Average(PAV)' pop up page should open";
        let stepActualResult = "'Assign Program Average(PAV)' pop up page should open";

        try {
            //await programTracksPage.clickToggaleProgramsTracksPAVTable("Active", "Name", "Value", "TP", "Mar19", "CHECK");
            await programTracksPage.clickAssignPAVRatingsProgramsTrakingPAVTable("Name", "Name", "Value", "TP", "Mar19");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check that check-boxes are available to select records", async () => {

        let stepAction = "Check that check-boxes are available to select records";
        let stepData = "";
        let stepExpResult = "User should be able to see check-boxes";
        let stepActualResult = "User should be able to see check-boxes";

        try {
            await programTracksPage.verifyRowInPAVPopupGridIsNotSelected(1, 1);
            await programTracksPage.verifyRowInPAVPopupGridIsNotSelected(2, 1);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("check that user can select one or more records", async () => {

        let stepAction = "check that user can select one or more records";
        let stepData = "";
        let stepExpResult = "User should be able to select one or more records";
        let stepActualResult = "User should be able to select one or more records";

        try {
            await programTracksPage.clickRowInPAVPopupGrid(1, 1);
            await programTracksPage.clickRowInPAVPopupGrid(2, 1);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check that the user can un-select one or more records", async () => {

        let stepAction = "Check that the user can un-select one or more records";
        let stepData = "";
        let stepExpResult = "User should be able to un-select one or more reocrds";
        let stepActualResult = "User should be able to un-select one or more reocrds";

        try {
            await programTracksPage.verifyRowInPAVPopupGridIsChecked(1, 1);
            await programTracksPage.verifyRowInPAVPopupGridIsChecked(2, 1);
            await programTracksPage.clickRowInPAVPopupGrid(1, 1);
            await programTracksPage.clickRowInPAVPopupGrid(2, 1);
            await programTracksPage.verifyRowInPAVPopupGridIsNotSelected(1, 1);
            await programTracksPage.verifyRowInPAVPopupGridIsNotSelected(2, 1);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a record  from the grid", async () => {

        let stepAction = "Select a record  from the grid";
        let stepData = "";
        let stepExpResult = "User should be able to see the record selected";
        let stepActualResult = "User should be able to see the record selected";

        try {
            await programTracksPage.clickRowInPAVPopupGrid(1, 1);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter some details in the search criteria and click on Search", async () => {

        let stepAction = "Enter some details in the search criteria and click on Search";
        let stepData = "";
        let stepExpResult = "User should be to able to search the records and the previously selected should be displayed as selected";
        let stepActualResult = "User should be to able to search the records and the previously selected should be displayed as selected";

        try {
            await verify.verifyProgressBarNotPresent();
            await programTracksPage.verifyXgmultiselectSelectionValue("channel", "WPGH");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-build-pav-channels");
            let channelValue: string = String(await programTracksPage.retunComboboxMultiselectOptionAtIndex("xgc-build-pav-channels", 2));
            await programTracksPage.selectOptionFromMultiselect(channelValue, "Channel(s)");
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.clickonButtonsInPage("xgc-build-pav-search","Search");
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Assign button", async () => {

        let stepAction = "Click on Assign button";
        let stepData = "";
        let stepExpResult = "User should be able to see the selected record in program tracks page";
        let stepActualResult = "User should be able to see the selected record in program tracks page";

        try {
            await programTracksPage.clickonButtonsInPage("xgc-build-pav-assign-button","Assign");
            await programTracksPage.clickDropdownOptionInventory("xgc-build-pav-assign-button","Enable PAV only");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
