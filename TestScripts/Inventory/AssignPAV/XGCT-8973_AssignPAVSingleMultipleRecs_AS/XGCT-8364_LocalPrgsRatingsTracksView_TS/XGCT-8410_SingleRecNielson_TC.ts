/*
***********************************************************************************************************
* @Script Name :  XGCT-8410
* @Description :  xG Campaign - Inventory - Refactor: Local Program Rating Tracks view - Assign PAV: Single Record (Nielsen)1-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 1-8-2019
* @Author : Vikas
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { Gutils } from '../../../../../Utility/gutils';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { ProgramRatingTracks } from '../../../../../Pages/Inventory/ProgramRatingTracks';




//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let globalFunc = new globalvalues();
let HomePage = new HomePageFunction();
let localProgPage = new LocalProgramming();
let programTracksPage = new ProgramRatingTracks();
let gUtils = new Gutils();


//Variables Declaration
let currentDate = gUtils.getcurrentDate();
let endDate = gUtils.getcurrentDateAfterSomeDays(10);
let ramdonValue = new Date().toLocaleString().split('/').join('').split(' ').join('').split(',').join('').split(':').join('').split('AM').join('').split('PM').join('');
let TestCase_ID = 'XGCT-8410'
let TestCase_Title = 'xG Campaign - Inventory - Refactor: Local Program Rating Tracks view - Assign PAV: Single Record (Nielsen)'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - INVENTORY - REFACTOR: LOCAL PROGRAM RATING TRACKS VIEW - ASSIGN PAV: SINGLE RECORD (NIELSEN)', () => {


    // --------------Test Step------------
    it("Navigate to xgcampaignwebapp url", async () => {

        let stepAction = "Navigate to xgcampaignwebapp url";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net";
        let stepExpResult = "User should navigate to the xgcampaignwebapp login page";
        let stepActualResult = "User should navigate to the xgcampaignwebapp login page";

        try {
            await globalFunc.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Provide 'User Name' and 'Password' and click on Login button", async () => {

        let stepAction = "Provide 'User Name' and 'Password' and click on Login button";
        let stepData = "";
        let stepExpResult = "User should see the home page of the application";
        let stepActualResult = "User should see the home page of the application";

        try {

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on 'Programs' icon on top of the page. Click on 'Inventory'-->'Local Programs' option from the dropdown", async () => {

        let stepAction = "Click on 'Programs' icon on top of the page. Click on 'Inventory'-->'Local Programs' option from the dropdown";
        let stepData = "Local Programming";
        let stepExpResult = "'Local Programs' page should open and all the necessary details should display";
        let stepActualResult = "'Local Programs' page should open and all the necessary details should display";

        try {
            await HomePage.appclickOnNavigationList("Programming");
            await HomePage.appclickOnMenuListLeftPanel("Local Programming");
            await localProgPage.clickonButtonsRatecardPage("xgc-add-program-orbit-button","Add");
            await localProgPage.clickDropdownOptionInventory("Local Program");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("channel");
            await localProgPage.clickMultiSelectOptionInventoryAddProgram("WPGH");
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await localProgPage.enterTextInInputInventoryAddProgram("xgc-program-name", "ATA" + ramdonValue + "XGCT8410");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("daypart");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.clickDaysOptionShortCutActiveInactive("xgc-days-day-picker","INACTIVE","M-F");
            await localProgPage.clickClockIconTimeOptionInventorydialog("xgc-starttime-time-picker");
            await localProgPage.clickTimeInInventorydialogTimetable("05:00");
            await localProgPage.clickTimeInInventorydialogTimetable("05:00");
            await localProgPage.clickClockIconTimeOptionInventorydialog("xgc-endtime-time-picker");
            await localProgPage.clickTimeInInventorydialogTimetable("06:00");
            await localProgPage.clickTimeInInventorydialogTimetable("06:00");
            await localProgPage.enterDateInInputInventoryAddProgram("xgc-telecast-startdate-date-picker", currentDate);
            await localProgPage.enterDateInInputInventoryAddProgram("xgc-telecast-enddate-date-picker", endDate);
            await localProgPage.clickonButtonsRatecardPage("xgc-save-local-program","Save");
            await browser.sleep(600000);
            //await localProgPage.verifyPopupMessageRatecard("Successfully added");
          
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select any one program from the grid display and click on 'Program Tracks' link in 'Rating Track' column of that particular program", async () => {

        let stepAction = "Select any one program from the grid display and click on 'Program Tracks' link in 'Rating Track' column of that particular program";
        let stepData = "ATA" + ramdonValue + "XGCT8410";
        let stepExpResult = "'Program Rating Tracks' page should display.";
        let stepActualResult = "'Program Rating Tracks' page should display.";

        try {
            await browser.refresh();
            await browser.sleep(1000);
            await programTracksPage.clickComponentPdropdown("xgc-markets");
            await programTracksPage.selectOptionsFromComponentPdropdown("Pittsburgh");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-channels");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-dayparts");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await localProgPage.clickonButtonsRatecardPage("xgc-search-button","Search");
            await localProgPage.enterTextInGlobalFilter("ATA" + ramdonValue + "XGCT8410");
            await browser.sleep(1000);
            await localProgPage.clickProgramLinkInLocalProgramGrid("Program Track", "Selling Title", "ATA" + ramdonValue + "XGCT8410");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the program is selected by default in 'Programs' list and all the filters are loaded according to the program selection", async () => {

        let stepAction = "Check the program is selected by default in 'Programs' list and all the filters are loaded according to the program selection";
        let stepData = "ATA" + ramdonValue + "XGCT8410";
        let stepExpResult = "Survey books should be loaded in the grid according to the filter selection";
        let stepActualResult = "Survey books should be loaded in the grid according to the filter selection";

        try {
            await programTracksPage.clickProgramInRatingsTracksView("ATA" + ramdonValue + "XGCT8410");
            //await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-collection", "LPM");
            //await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-sample", "HWC Universe");
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + Same Day (LS)");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });





    // --------------Test Step------------
    it("Select any one survey from 'Name' column and click on the dropdown icon and click on 'Assign PAV' option", async () => {

        let stepAction = "Select any one survey from 'Name' column and click on the dropdown icon and click on 'Assign PAV' option";
        let stepData = "Ex: Jun19";
        let stepExpResult = "'Assign Program Average(PAV)' pop up page should open";
        let stepActualResult = "'Assign Program Average(PAV)' pop up page should open";

        try {
           // await programTracksPage.clickToggaleProgramsTracksPAVTable("Active", "Name", "Value", "TP", "Mar19", "CHECK");
            await programTracksPage.clickAssignPAVRatingsProgramsTrakingPAVTable("Name", "Name", "Value", "TP", "Mar19");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a single record that has Type-TP and click on Assign PAV", async () => {

        let stepAction = "Select a single record that has Type-TP and click on Assign PAV";
        let stepData = "";
        let stepExpResult = "Program Track-Build PAV- Riverdale Pop-up Page should Open. User should select one record from the grid";
        let stepActualResult = "Program Track-Build PAV- Riverdale Pop-up Page should Open. User should select one record from the grid";

        try {
            await browser.sleep(1000);
            await programTracksPage.clickRowInPAVPopupGrid(1, 1);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Clcik on Assign PAV at the bottom of the page", async () => {

        let stepAction = "Clcik on Assign PAV at the bottom of the page";
        let stepData = "Enable PAV only";
        let stepExpResult = "'In the main page in Grid section additional record should be created for the same montly survey";
        let stepActualResult = "'In the main page in Grid section additional record should be created for the same montly survey ";

        try {
            await programTracksPage.clickonButtonsInPage("xgc-build-pav-assign-button","Assign");
            await programTracksPage.clickDropdownOptionInventory("xgc-build-pav-assign-button","Enable PAV only");
            await browser.sleep(2000);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the Name of the survey attached to the PAV record line is not altered", async () => {

        let stepAction = "Check the Name of the survey attached to the PAV record line is not altered";
        let stepData = "PAV (1)";
        let stepExpResult = "'Same Name should be displayed in the Name column  newly added record as in the ";
        let stepActualResult = "'Same Name should be displayed in the Name column  newly added record as in the ";

        try {

            await programTracksPage.verifyDataInAssignPAVRatingsProgramsTraksPAVTable("Rationale", "Name", "Mar19", 'PAV (1): Mar19');
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the Book Type field is not altered", async () => {

        let stepAction = "Check the Book Type field is not altered";
        let stepData = "";
        let stepExpResult = "Book Type column field should display the same name as in the original record";
        let stepActualResult = "Book Type column field should display the same name as in the original record";

        try {
            await programTracksPage.verifyDataInAssignPAVRatingsProgramsTraksPAVTable("Rationale", "Type", "Survey", 'PAV (1): Mar19');
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the Type field is changed to PAV", async () => {

        let stepAction = "Check the Type field is changed to PAV";
        let stepData = "";
        let stepExpResult = "Type column field should display PAV";
        let stepActualResult = "Type column field should display PAV";

        try {
            await programTracksPage.verifyDataInAssignPAVRatingsProgramsTraksPAVTable("Rationale", "Value", "PAV", 'PAV (1): Mar19');
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the Rationale column is updated to the following structure:PAV (#): [Survey]{Call Letters of PAV record][Day(s)](Start and End Time][Wks](#) = the number of PAV records(Wks) = the number of weeks the program average record ran during the sweep", async () => {

        let stepAction = "Check the Rationale column is updated to the following structure:PAV (#): [Survey]{Call Letters of PAV record][Day(s)](Start and End Time][Wks](#) = the number of PAV records(Wks) = the number of weeks the program average record ran during the sweep";
        let stepData = "";
        let stepExpResult = "The rationale column should display the below value:Example: PAV (1): Jun18 WPGH M-F 6:00-7:00am (Wks 1234)";
        let stepActualResult = "The rationale column should display the below value:Example: PAV (1): Jun18 WPGH M-F 6:00-7:00am (Wks 1234)";

        try {
            await programTracksPage.verifyDataInAssignPAVRatingsProgramsTraksPAVTable("Rationale", "Value", "PAV", 'PAV (1): Mar19');
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step------------
    it('Remove PAV Record', async () => {

        let stepAction = 'Remove PAV Record';
        let stepData = '';
        let stepExpResult = 'User should able to remove the PAV Record ';
        let stepActualResult = 'User is able to remove the PAV Record';

        try {

            await programTracksPage.clickonButtonsInPage("xgc-btn-rating-build","Rating Build");
            await programTracksPage.clickDropdownOptionInventory("xgc-btn-rating-build","Remove Program Averages");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-dropdown-program-average");
            await programTracksPage.selectOptionFromMultiselect("Mar19", "Survey(s)");
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.clickonButtonsInPage("xgc-btn-remove","Remove");
            await programTracksPage.clickDropdownOptionInventory("xgc-btn-remove","Remove and Close");
            await verify.verifyProgressBarNotPresent();



            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

});
