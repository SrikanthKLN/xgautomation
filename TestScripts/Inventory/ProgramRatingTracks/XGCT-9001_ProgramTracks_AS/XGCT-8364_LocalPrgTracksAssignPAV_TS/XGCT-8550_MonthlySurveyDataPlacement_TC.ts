/*
***********************************************************************************************************
* @Script Name :  XGCT-8550
* @Description :  xG Campaign - Inventory - Refactor: Placement of Monthly Survey Data in Program Rating Tracks & select the items for removal of program average in Program Rating Tracks View page2-8-2019
* @Page Object Name(s) : LocalProgramming,ProgramRatingTracks
* @Dependencies/Libs : ActionLib,VerifyLib
* @Pre-Conditions : 
* @Creation Date : 2-8-2019
* @Author : Sivaraj
* @Modified By & Date:05-8-2019
*************************************************************************************************************
*/
//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { ProgramRatingTracks } from '../../../../../Pages/Inventory/ProgramRatingTracks';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let homePage = new HomePageFunction();
let globalValue = new globalvalues();
let localProgrammingPage = new LocalProgramming();
let ProgramRatingTrack = new ProgramRatingTracks();

//Variables Declaration
let TestCase_ID = 'XGCT-8550'
let TestCase_Title = 'xG Campaign - Inventory - Refactor: Placement of Monthly Survey Data in Program Rating Tracks & select the items for removal of program average in Program Rating Tracks View page'

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - INVENTORY - REFACTOR: PLACEMENT OF MONTHLY SURVEY DATA IN PROGRAM RATING TRACKS & SELECT THE ITEMS FOR REMOVAL OF PROGRAM AVERAGE IN PROGRAM RATING TRACKS VIEW PAGE', () => {

    // --------------Test Step------------
    it("Navigate to the Storybook URL", async () => {

        let stepAction = "Navigate to the Storybook URL";
        let stepData = "https://xgplatform-qa.myimagine.com/";
        let stepExpResult = "User should navigate to the storybook";
        let stepActualResult = "User able to navigate to the storybook";

        try {
            await globalValue.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Programming >Local Program Rating Tracks view", async () => {

        let stepAction = "Navigate to Programming >Local Program Rating Tracks view";
        let stepData = "";
        let stepExpResult = "User should navigate to the Program Rating Tracks view with Program Details displayed";
        let stepActualResult = "User able to navigate to the Program Rating Tracks view with Program Details displayed";
        let menuName = 'Programming';
        let subMenuName = 'Local Programming'
        let market = "Pittsburgh"

        try {
            await homePage.appclickOnNavigationList(menuName);
            await homePage.appclickOnMenuListLeftPanel(subMenuName);
            //await localProgrammingPage.SelectMarketOptions(market)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Select any link for the program from the list of program from the left pane", async () => {

        let stepAction = "Select any link for the program from the list of program from the left pane";
        let stepData = "";
        let stepExpResult = "User should be able to see the program tracks data";
        let stepActualResult = "User able to see the program tracks data";

        try {
            await action.Click(localProgrammingPage.clickLink, "")
            await browser.sleep(2000)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it(">>Playback Type'", async () => {

        let stepAction = ">>Playback Type'";
        let stepData = "";
        let stepExpResult = "Report generates under the grid";
        let stepActualResult = "Report generates under the grid";

        try {
           // await ProgramRatingTrack.clickProgramInRatingsTracksView(" base ball live ")
            await ProgramRatingTrack.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + Same Day (LS)")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("From the data table generated below, Click in the arrow Under NAME column", async () => {

        let stepAction = "From the data table generated below, Click in the arrow Under NAME column";
        let stepData = "";
        let stepExpResult = "Assign PAV option should display";
        let stepActualResult = "Assign PAV option is displayed";

        try {
            await action.Click(localProgrammingPage.sortIcon, "clicked on sort icon")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Assign PAV option available for the survey in the grid", async () => {

        let stepAction = "Click on Assign PAV option available for the survey in the grid";
        let stepData = "";
        let stepExpResult = "User should be able to see Assign Program track -(PAV)";
        let stepActualResult = "User able to see Assign Program track -(PAV)";

        try {
            await action.Click(localProgrammingPage.clickProgram, "")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Check that after click on reset button the sort is cleared", async () => {

        let stepAction = "Check that after click on reset button the sort is cleared";
        let stepData = "";
        let stepExpResult = "The sort value should clear and the default sort details should display";
        let stepActualResult = "The sort value cleared and the default sort details are displayed";

        try {
            await action.Click(localProgrammingPage.reset, "reset")
            await verify.verifyNotPresent(localProgrammingPage.sortCleared, "Sort")

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step------------
    it("Click on 'Rating Build' option and select 'Remove Program Averages' option from drop-down and check a window is presented to select the items for removing program average", async () => {

        let stepAction = "Click on 'Rating Build' option and select 'Remove Program Averages' option from drop-down and check a window is presented to select the items for removing program average";
        let stepData = "";
        let stepExpResult = "'Remove Program Average' pop up window should open";
        let stepActualResult = "'Remove Program Average' pop up window opened";

        try {
            await browser.sleep(2000)
            await browser.executeScript('window.scrollTo(0,0)').then(async function () {
                await action.Click(localProgrammingPage.clickRatingBuild, "")
            })
            await verify.verifyElement(localProgrammingPage.removeProgram, "")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check 'Survey(s)' option is given as default", async () => {

        let stepAction = "Check 'Survey(s)' option is given as default";
        let stepData = "";
        let stepExpResult = "Surveys option should be present to select the records to remove PAV";
        let stepActualResult = "Surveys option is presented to select the records to remove PAV";

        try {
            await action.Click(localProgrammingPage.removeProgram, "")
            await verify.verifyElement(localProgrammingPage.checkSurveys, "")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Check 'Assign to all Playback Types' is given", async () => {

        let stepAction = "Check 'Assign to all Playback Types' is given";
        let stepData = "";
        let stepExpResult = "'Assign to all Playback Types' check box should be used to remove PAV from same or all playback types";
        let stepActualResult = "'Assign to all Playback Types' check box is used to remove PAV from same or all playback types";

        try {
            await verify.verifyElement(localProgrammingPage.assignPlaybacks, "")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Check 'Remove' and 'Cancel' buttons are given to perform delete operations", async () => {

        let stepAction = "Check 'Remove' and 'Cancel' buttons are given to perform delete operations";
        let stepData = "";
        let stepExpResult = "'Remove' and 'cancel' buttons should be available in the window to perform necessary operations";
        let stepActualResult = "'Remove' and 'cancel' buttons should be available in the window to perform necessary operations";

        try {
            await verify.verifyElement(localProgrammingPage.verifyCancel, "")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

});
