/*
***********************************************************************************************************
* @Script Name :  XGCT-7278
* @Description :  xG Campaign - Inventory - Program Tracks Rating Changes after Program edit-Comscore19-6-2019
* @Page Object Name(s) : CreateCloneLocalProgram,LocalProgramView,AddLocalProgram
* @Dependencies/Libs : ActionLib,VerifyLib
* @Pre-Conditions : 
* @Creation Date : 19-6-2019
* @Author : Sivaraj
* @Modified By & Date:Venkata Sivakumar & 08-8-2019
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { SettingsPage } from '../../../../../Pages/Inventory/Settings';
import { DayPartDemoPage } from '../../../../../Pages/Inventory/Settings/DayPartDemo';
import { ProgramRatingTracks } from '../../../../../Pages/Inventory/ProgramRatingTracks';
import { InventoryPage } from '../../../../../Pages/Inventory/InventoryCommon';


//Import Class Objects Instantiation

let report = new Reporter();
let globalFunc = new globalvalues();
let action = new ActionLib();
let homePage = new HomePageFunction();
let localProgramPage = new LocalProgramming();
let verify = new VerifyLib();
let settingsPage = new SettingsPage();
let programTracksPage = new ProgramRatingTracks();
let dayPartDemoPage = new DayPartDemoPage();
let inventoryPage = new InventoryPage();

//Variables Declaration
let TestCase_ID = 'XGCT-7278'
let TestCase_Title = 'xG Campaign - Inventory - Program Tracks Rating Changes after Program edit-Comscore'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;
let elementStatus: boolean;
let demoValue: string;
let updatedStartTime;
let updatedEndTime;
let sourceValue = "Comscore";
let distributorCodes = "5979";
let marketSource = "508";
let weeks = "Weeks=Week1&Weeks=Week2&Weeks=Week3&Weeks=Week4";
let averageBy = "ReportableDemographic";
let interval = "Hour";
let collectionMethodValue = "ComscoreSTB";
let playBackType = "LO";
let sampleValue = "Total%20DMA";
let sellingTitle;
let surveyName;
let metricsRatings;
let uiRTGValue;
let uiSHRValue;
let uiSIUValue;
let responseStatusBody;
let statusCode;
let daysOfWeek;
let actualUpdatedStartTime;
let actualUpdatedEndTime;
let reqAPI = require("request");
//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - INVENTORY - PROGRAM TRACKS RATING CHANGES AFTER PROGRAM EDIT-COMSCORE', () => {

        // --------------Test Step------------
        it('Login to Chasis Application', async () => {

            let stepAction = 'Login to Chasis Application';
            let stepData = globalvalues.urlname;
            let stepExpResult = 'User should see the home page of Chasis';
            let stepActualResult = 'User able to see the home page of Chasis';
            try {
                  await globalFunc.LaunchStoryBook();
                  report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
            }
            catch (err) {
                  report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
            }
      });

      // --------------Test Step------------
      it('Navigate to Programming-->Local Programmig', async () => {

            let stepAction = 'Navigate to Programming-->Local Programmig';
            let stepData = 'Market- Pittsburgh Channels-WPCB Dayparts-Morning Active- check it. After clicking on search data should display for the above selection';
            let stepExpResult = '"User should select as below:';
            let stepActualResult = '"User able to select as below:';
            let mainMenuName = "Programming";
            let subMenuName = "Local Programming";
            try {
                  await homePage.appclickOnNavigationList(mainMenuName);
                  await homePage.appclickOnMenuListLeftPanel(subMenuName);
                  await settingsPage.clickOnSettings();
                  await settingsPage.clickOnSettingsSubMenuLink("Daypart Demo");
                  await settingsPage.clickOnDayPartDemoSubMenuLink("Local Daypart Demo");
                  await browser.sleep(1000);
                  await dayPartDemoPage.clickOnComscore();
                  await localProgramPage.SelectMarketOptions("Pittsburgh");
                  await browser.sleep(1000);
                  await programTracksPage.clickDataInRatingSourceTableTarcksPage("Dayparts", "Morning");
                  await browser.sleep(1000);
                  elementStatus = await verify.verifyElementVisible(dayPartDemoPage.noDataMatchFound);
                  if (elementStatus) {
                        await dayPartDemoPage.addDayPartsDemo("Pittsburgh", "Morning");
                        demoValue = "A18-20";
                  }
                  else {
                        demoValue = String(await action.GetText(dayPartDemoPage.firstRowDemos, "")).trim();
                  }
                  await browser.sleep(2000);
                  await action.scrollToTopOfPage();
                  await homePage.appclickOnMenuListLeftPanel(subMenuName);
                  await verify.verifyElementIsDisplayed(inventoryPage.headerLocalProgram, "Verify Local Programs is displayed");
                  await localProgramPage.SelectMarketOptions("Pittsburgh");
                  await localProgramPage.SelectChannelsOptions(["WPGH"]);
                  await localProgramPage.SelectDaypartsOptions(["Morning"]);
                  await localProgramPage.clickOnActiveCheckBox();
                  report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
            }
            catch (err) {
                  report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
            }
      });

      //--------------Test Step------------
      it('Selct a Local Program from the grid and click on it', async () => {
            let stepAction = 'Selct a Local Program from the grid and click on it';
            let stepData;
            let stepExpResult = 'Pop up should display. In Edit Local Program page change Day(s) or Start Time and End Time .';
            let stepActualResult = ' In Edit,able to update Local Program page change Day(s) or Start Time and End Time .';
            let channels = ["WPXI"];
            let dayParts = ["Morning"];
            let days = ["Mo","Tu","We","Th"];
            daysOfWeek = "DaysOfWeek=Mon&DaysOfWeek=Tue&DaysOfWeek=Wed&DaysOfWeek=Thu";
            try {
                  sellingTitle = await localProgramPage.addLocalProgram(channels, dayParts, days);
                  updatedStartTime = "02:00";
                  updatedEndTime = "03:00";
                  await action.SetText(localProgramPage.globalFilter,sellingTitle,"");
                  await browser.sleep(3000);
                  await action.Click(localProgramPage.firstRowSellingTitle, "Click On First Row Selling Title");
                  await browser.sleep(2000);
                  await localProgramPage.clickClockIconTimeOptionInventorydialog("xgc-starttime-time-picker");
                  await localProgramPage.clickTimeInInventorydialogTimetable(updatedStartTime);
                  await localProgramPage.clickTimeInInventorydialogTimetable(updatedStartTime);
                  await localProgramPage.clickClockIconTimeOptionInventorydialog("xgc-endtime-time-picker");
                  await localProgramPage.clickTimeInInventorydialogTimetable(updatedEndTime);
                  await localProgramPage.clickTimeInInventorydialogTimetable(updatedEndTime);
                  report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
            }
            catch (err) {
                  report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
            }
      });

      // --------------Test Step------------
      it('Click on Update button to update the new changes', async () => {

            let stepAction = 'Click on Update button to update the new changes';
            let stepData = '';
            let stepExpResult = 'Ater updation,new  details of record should display in the grid';
            let stepActualResult = 'Ater updation,new  details of record are displayed in the grid';
            try {
                  await localProgramPage.clickonButtonsRatecardPage("xgc-save-local-program","Update");
                  await browser.sleep(1000);
                  await localProgramPage.verifyPopupMessageRatecard("Successfully updated");
                  report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
            }
            catch (err) {
                  report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
            }
      });

      // --------------Test Step------------
      it('Navigate to Inventory-->Local Program Ratings Track View page', async () => {

            let stepAction = 'Navigate to Inventory-->Local Program Ratings Track View page';
            let stepData = '';
            let stepExpResult = 'Program Rating Tracks page should display ';
            let stepActualResult = 'Program Rating Tracks page is displayed ';
            try {
                  await localProgramPage.clickProgramLinkInLocalProgramGrid("Program Track", "Selling Title", sellingTitle);
                  report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
            }
            catch (err) {
                  report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
            }
      });

      // --------------Test Step------------
      it('Select the same Edited program from the programs list', async () => {

            let stepAction = 'Select the same Edited program from the programs list ';
            let stepData = sellingTitle;
            let stepExpResult = 'Days and time should change as edited in the local program page';
            let stepActualResult = 'Days and times are changed as edited in the local program page';
            try {
                  await programTracksPage.clickProgramInRatingsTracksView(stepData);
                  await programTracksPage.verifySellingTitleInProgramsList(stepData);
                  await browser.sleep(20000);
                  await action.Click(localProgramPage.displayOptions, "");
                  await programTracksPage.slideDecimalvalueInMatSlider("xgc-mat-slider-rtg", 3);
                  await programTracksPage.slideDecimalvalueInMatSlider("xgc-mat-slider-shr", 3);
                  await programTracksPage.slideDecimalvalueInMatSlider("xgc-mat-slider-hp", 3);
                  await programTracksPage.clickDataInRatingSourceTableTarcksPage("Source", sourceValue);
                  await programTracksPage.clickDataInRatingSourceTableTarcksPage("Sample", "DMA Universe");
                  await programTracksPage.clickDataInRatingSourceTableTarcksPage("Playback Type", "Live Only (LO)");
                  await browser.sleep(3000);
                  await verify.verifyElementIsDisplayed(programTracksPage.gridTable, "verify grid is displayed.");
                  surveyName = String(await action.GetText(programTracksPage.firstRowName, "")).trim();
                  await verify.verifyElementIsDisplayed(programTracksPage.gridTable, "verify grid is displayed.");
                  metricsRatings = await programTracksPage.getFirstRowMetricsAndRatings();
                  uiRTGValue = metricsRatings["RTG"];
                  uiSHRValue = metricsRatings["SHR"];
                  uiSIUValue = metricsRatings["SIU"];
                  report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
            }
            catch (err) {
                  report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
            }
      });
      it('Get Ratings from Rating Api', async (done) => {
            let stepAction = 'Get Ratings from Rating Api ';
            let stepExpResult = 'Server Response should be 200 and rating details should be displayed';
            let stepActResult = 'Server response is 200';
            actualUpdatedStartTime = updatedStartTime+":00";
            actualUpdatedEndTime = updatedEndTime+":00";
            await console.log(globalvalues.ratingAPI + "Ratings/TimePeriod/Monthly?RatingSource=" + sourceValue + "&MarketSource=" + marketSource + "&CollectionMethod=" + collectionMethodValue + "&PlaybackType=" + playBackType + "&DistributorCodes=" + distributorCodes + "&ReportableDemographics=" + demoValue + "&SurveyName=" + surveyName + "&StartTime=" + String(actualUpdatedStartTime).replace(":","%3A").replace(":","%3A") + "&EndTime=" + String(actualUpdatedEndTime).replace(":","%3A").replace(":","%3A") + "&" + weeks + "&"+daysOfWeek+"&AverageBy="+averageBy+"&Interval="+interval);
            try {
                  reqAPI.get({
                        "headers": { "content-type": "application/json" },
                        "url": globalvalues.ratingAPI + "Ratings/TimePeriod/Monthly?RatingSource=" + sourceValue + "&MarketSource=" + marketSource + "&CollectionMethod=" + collectionMethodValue + "&PlaybackType=" + playBackType + "&DistributorCodes=" + distributorCodes + "&ReportableDemographics=" + demoValue + "&SurveyName=" + surveyName + "&StartTime=" + String(actualUpdatedStartTime).replace(":","%3A").replace(":","%3A") + "&EndTime=" + String(actualUpdatedEndTime).replace(":","%3A").replace(":","%3A") + "&" + weeks + "&"+daysOfWeek+"&AverageBy="+averageBy+"&Interval="+interval,
                        "body": JSON.stringify({
                        })
                  }, (error, response, body) => {
                        if (error) {
                              return console.dir(error);
                        }
                        responseStatusBody = response.body;
                        statusCode = response.statusCode;
                        if(statusCode != 200)
                        {
                              done();
                              report.ReportStatus(stepAction, '', 'Fail', stepExpResult, stepActResult);
                        }
                        else
                        {
                              done();
                              report.ReportStatus(stepAction, '', 'Pass', stepExpResult, stepActResult);
                        }                      
                        
                  });
            }
            catch (err) {
                  report.ReportStatus(stepAction, '', 'Fail', stepExpResult, 'Server response is ' + statusCode);
            }
      });

      it('Validate "Share" "Rating" and "SIU" Ratings values', async (done) => {
            let stepAction = 'Validate "Share" "Rating" and "SIU" Ratings values ';
            let stepExpResult = '"Share" "Rating" and "SIU" Ratings values Should be equal in API and UI';
            let stepActResult;
            try {
                  let jsonData = JSON.parse(responseStatusBody);
                  await console.log(jsonData);
                  let dataDetails = jsonData.data;
                  if (dataDetails == null || dataDetails.length == 0) {
                        report.ReportStatus(stepAction, '', 'Fail', stepExpResult, 'Data details are not displayed.');
                        done();
                  }
                  else {
                        let finalShare = 0;
                        let finalRating = 0;
                        let count = dataDetails.length;
                        let finalPeopleUsingTelevision = 0;
                        let finalPUT = 0;
                        for(let index = 0; index < dataDetails.length ; index ++)
                        {
                          let apiProgramName = dataDetails[index].programName;
                          await console.log("programName"+apiProgramName); 
                          let apiStartTime = dataDetails[index].startTime;
                          await console.log("apiStartTime"+apiStartTime);
                          let apiEndTime = dataDetails[index].endTime;
                          await console.log("apiEndTime"+apiEndTime);
                          
                              await console.log("enter in to api if block");
                              let apiShareValue = dataDetails[index].share;
                              let apiRating =  dataDetails[index].rating;
                              let apiTotalImpression = dataDetails[index].totalImpressions;
                              let apiTotalUniverse = dataDetails[index].totalUniverse;
                              let apiTotalHUTPUT = dataDetails[index].totalHUTPUT;
                              let apiRatingHUTPUT = dataDetails[index].ratingHUTPUT;
                              let apiRatingPeopleWatching = dataDetails[index].ratingPeopleWatching;
                              let apiRatingUniverse = dataDetails[index].ratingUniverse;
                              let apiUniverse = dataDetails[index].universe;
                              let apiPeopleUsingTelevision = dataDetails[index].peopleUsingTelevision;
                              await console.log("shareValue"+apiShareValue);
                              await console.log("rating"+apiRating);
                              await console.log("totalImpression"+apiTotalImpression);
                              await console.log("totalHUTPUT"+apiTotalHUTPUT);
                              await console.log("totalUniverse"+apiTotalUniverse);
                              await console.log("ratingHUTPUT"+apiRatingHUTPUT);
                              await console.log("ratingPeopleWatching"+apiRatingPeopleWatching);
                              await console.log("ratingUniverse"+apiRatingUniverse);
                              await console.log("universe"+apiUniverse);
                              await console.log("peopleUsingTelevision"+apiPeopleUsingTelevision);
                              finalShare = finalShare + apiShareValue;
                              finalRating = finalRating + apiRating;
                              finalPeopleUsingTelevision = finalPeopleUsingTelevision + apiPeopleUsingTelevision;
                              finalPUT = finalPUT + (apiRatingHUTPUT / apiRatingUniverse)*100;
                        }
                        finalShare = finalShare / count;
                        finalRating = finalRating / count;
                        finalPUT = finalPUT / count;
                        finalPeopleUsingTelevision = finalPeopleUsingTelevision / count;
                        await console.log("finalShare"+finalShare);
                        await console.log("finalRating"+finalRating);
                        await console.log("finalPeopleUsingTelevision"+finalPeopleUsingTelevision);
                        await console.log("finalPUT"+finalPUT);
                        await console.log("UIRating"+uiRTGValue);
                        await console.log("UIShare"+uiSHRValue);
                        await console.log("UISIU"+uiSIUValue);                        
                        done();
                        report.ReportStatus(stepAction, '', 'Pass', stepExpResult, stepActResult);
                  }
            }
            catch (err) {
                  report.ReportStatus(stepAction, '', 'Fail', stepExpResult,  ' not able to Get the advertiser details' + err);
            }
      });
});
