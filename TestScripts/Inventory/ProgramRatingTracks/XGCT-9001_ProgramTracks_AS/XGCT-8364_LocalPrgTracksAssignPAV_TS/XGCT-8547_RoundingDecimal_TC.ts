/*
***********************************************************************************************************
* @Script Name :  XGCT-8547
* @Description :  xG Campaign - Inventory - Refactor: Verify the system rounds appropriately when changing from a larger decimal value to a smaller one and vise-versa5-8-2019
* @Page Object Name(s) : LocalProgramming,ProgramRatingTracks
* @Dependencies/Libs :  ActionLib,VerifyLib
* @Pre-Conditions : 
* @Creation Date : 5-8-2019
* @Author : Sivaraj
* @Modified By & Date:5-8-2019
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { ProgramRatingTracks } from '../../../../../Pages/Inventory/ProgramRatingTracks';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let homePage = new HomePageFunction();
let globalValue = new globalvalues();
let localProgrammingPage = new LocalProgramming();
let ProgramRatingTrack = new ProgramRatingTracks();

//Variables Declaration
let TestCase_ID = 'XGCT-8547'
let TestCase_Title = 'xG Campaign - Inventory - Refactor: Verify the system rounds appropriately when changing from a larger decimal value to a smaller one and vise-versa'

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - INVENTORY - REFACTOR: VERIFY THE SYSTEM ROUNDS APPROPRIATELY WHEN CHANGING FROM A LARGER DECIMAL VALUE TO A SMALLER ONE AND VISE-VERSA', () => {

    // --------------Test Step------------
    it("Navigate to URL", async () => {

        let stepAction = "Navigate to URL";
        let stepData = "https://xgplatform-qa.myimagine.com/";
        let stepExpResult = "User should be able to navigate to xGcampaign webpage";
        let stepActualResult = "User able to navigate to xGcampaign webpage";
        let menuName = 'Programming';
        let subMenuName = 'Local Programming'

        try {
            await globalValue.LaunchStoryBook();
            await homePage.appclickOnNavigationList(menuName);
            await homePage.appclickOnMenuListLeftPanel(subMenuName);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Link ''Link'' against Selling Title ''Convos With My 2-Year-Old''.'", async () => {

        let stepAction = "Click on Link ''Link'' against Selling Title ''Convos With My 2-Year-Old''.'";
        let stepData = "";
        let stepExpResult = "'User should be able to navigate to ''Program Rating Tracks'' page'";
        let stepActualResult = "'User able to navigate to ''Program Rating Tracks'' page'";

        try {
            await action.Click(localProgrammingPage.clickLink, "")
            await browser.sleep(2000)
            //await ProgramRatingTrack.clickProgramInRatingsTracksView(" base ball live ")
            await ProgramRatingTrack.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + Same Day (LS)")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check that there is an option available for display option", async () => {

        let stepAction = "Check that there is an option available for display option";
        let stepData = "";
        let stepExpResult = "User should be able to see Display options on right top of the page";
        let stepActualResult = "User  able to see Display options on right top of the page";

        try {
            await browser.sleep(2000)
            await verify.verifyElement(localProgrammingPage.displayOptions, "")
            await action.MouseMoveToElement(localProgrammingPage.displayOptions,"mouse moved to element")
            await action.Click(localProgrammingPage.displayOptions, "")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the metrics available in display options", async () => {

        let stepAction = "Check the metrics available in display options";
        let stepData = "";
        let stepExpResult = "' User should be able to see the following metric with default options";
        let stepActualResult = "' User able to see the following metric with default options";

        try {
            await localProgrammingPage.verifyCheckBoxIsSelected("RTG");
            await localProgrammingPage.verifyCheckBoxIsNotSelected("IMP");
            await localProgrammingPage.verifyCheckBoxIsSelected("SHR");
            await localProgrammingPage.verifyCheckBoxIsSelected("H/P");
            await localProgrammingPage.verifyCheckBoxIsNotSelected("H/P (000)");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the default values for the metrics", async () => {

        let stepAction = "Check the default values for the metrics";
        let stepData = "";
        let stepExpResult = "'The following values should be the default values";
        let stepActualResult = "'The following values are the default values";

        try {
            await localProgrammingPage.verifyDecimalOption("rtgDecimal", "1");
            await localProgrammingPage.verifyDecimalOption("impDecimal", "1");
            await localProgrammingPage.verifyDecimalOption("SHRDecimal", "0");
            await localProgrammingPage.verifyDecimalOption("HPDecimal", "1");
            await localProgrammingPage.verifyDecimalOption("HPIMPDecimal", "1");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    it('Change the decimal options to the largest decimal value', async () => {

        let stepAction = 'Change the decimal options to the largest decimal value';
        let stepData = '';
        let stepExpResult = 'The data should round to the nearest largest value';
        let stepActualResult = ' data is round to the nearest largest value';

        try {

            await ProgramRatingTrack.slideDecimalvalueInMatSlider("xgc-mat-slider-rtg", 2);
            await ProgramRatingTrack.slideDecimalvalueInMatSlider("xgc-mat-slider-shr", 2);
            await ProgramRatingTrack.slideDecimalvalueInMatSlider("xgc-mat-slider-hp", 2);
            let allRTGValue = { RTG: "2", SHR: "2", HP: "2" };
            await ProgramRatingTrack.verifyDecimalValuePAVRatingsProgramsTrakingTable(allRTGValue, "Metrics");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    it('Change the decimal options to the Smallest decimal value', async () => {

        let stepAction = 'Change the decimal options to the Smallest decimal value';
        let stepData = '';
        let stepExpResult = 'The data should round to the nearest Smallest value';
        let stepActualResult = ' data is round to the nearest Smallest value';

        try {

            await ProgramRatingTrack.slideDecimalvalueInMatSlider("xgc-mat-slider-rtg", 1);
            await ProgramRatingTrack.slideDecimalvalueInMatSlider("xgc-mat-slider-shr", 1);
            await ProgramRatingTrack.slideDecimalvalueInMatSlider("xgc-mat-slider-hp", 1);
            let allRTGValue = { RTG: "1", SHR: "1", HP: "1" };
            await ProgramRatingTrack.verifyDecimalValuePAVRatingsProgramsTrakingTable(allRTGValue, "Metrics");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

});
