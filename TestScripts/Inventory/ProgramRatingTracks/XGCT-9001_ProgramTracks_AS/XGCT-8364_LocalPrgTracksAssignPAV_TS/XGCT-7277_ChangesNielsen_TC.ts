/*
***********************************************************************************************************
* @Script Name :  XGCT-7277
* @Description :  xG Campaign - Inventory - Program Tracks Rating Changes after Program edit-Nielsen19-6-2019
* @Page Object Name(s) : CreateCloneLocalProgram,LocalProgramView,AddLocalProgram
* @Dependencies/Libs : ActionLib
* @Pre-Conditions : 
* @Creation Date : 19-6-2019
* @Author : Sivaraj
* @Modified By & Date:Venkata Sivakumar & 08-8-2019
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { SettingsPage } from '../../../../../Pages/Inventory/Settings';
import { DayPartDemoPage } from '../../../../../Pages/Inventory/Settings/DayPartDemo';
import { ProgramRatingTracks } from '../../../../../Pages/Inventory/ProgramRatingTracks';
import { InventoryPage } from '../../../../../Pages/Inventory/InventoryCommon';


//Import Class Objects Instantiation
let report = new Reporter();
let globalFunc = new globalvalues();
let action = new ActionLib();
let homePage = new HomePageFunction();
let localProgramPage = new LocalProgramming();
let verify = new VerifyLib();
let settingsPage = new SettingsPage();
let programTracksPage = new ProgramRatingTracks();
let dayPartDemoPage = new DayPartDemoPage();
let inventoryPage = new InventoryPage();

//Variables Declaration
let TestCase_ID = 'XGCT-7277'
let TestCase_Title = 'xG Campaign - Inventory - Program Tracks Rating Changes after Program edit-Nielsen'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;
let elementStatus: boolean;
let demoValue: string;
let updatedStartTime;
let updatedEndTime;
let sourceValue = "Nielsen";
let distributorCodes = "5979";
let marketSource = "108";
let weeks = "Weeks=Week1&Weeks=Week2&Weeks=Week3&Weeks=Week4";
let averageBy = "ReportableDemographic";
let interval = "Hour";
let collectionMethodValue = "LPM";
let playBackType = "LS";
let sampleValue = "Total%20DMA";
let sellingTitle;
let surveyName;
let metricsRatings;
let uiRTGValue;
let uiSHRValue;
let uiHPValue;
let responseStatusBody;
let statusCode;
let daysOfWeek;
let actualUpdatedStartTime;
let actualUpdatedEndTime;
let reqAPI = require("request");
//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - INVENTORY - PROGRAM TRACKS RATING CHANGES AFTER PROGRAM EDIT-NIELSEN', () => {

      // --------------Test Step------------
      it('Login to Chasis Application', async () => {

            let stepAction = 'Login to Chasis Application';
            let stepData = globalvalues.urlname;
            let stepExpResult = 'User should see the home page of Chasis';
            let stepActualResult = 'User able to see the home page of Chasis';
            try {
                  await globalFunc.LaunchStoryBook();
                  report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
            }
            catch (err) {
                  report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
            }
      });

      // --------------Test Step------------
      it('Navigate to Programming-->Local Programmig', async () => {

            let stepAction = 'Navigate to Programming-->Local Programmig';
            let stepData = 'Market- Pittsburgh Channels-WPCB Dayparts-Morning Active- check it. After clicking on search data should display for the above selection';
            let stepExpResult = '"User should select as below:';
            let stepActualResult = '"User able to select as below:';
            let mainMenuName = "Programming";
            let subMenuName = "Local Programming";
            try {
                  await homePage.appclickOnNavigationList(mainMenuName);
                  await homePage.appclickOnMenuListLeftPanel(subMenuName);
                  await settingsPage.clickOnSettings();
                  await settingsPage.clickOnSettingsSubMenuLink("Daypart Demo");
                  await settingsPage.clickOnDayPartDemoSubMenuLink("Local Daypart Demo");
                  await localProgramPage.SelectMarketOptions("Pittsburgh");
                  await browser.sleep(1000);
                  await programTracksPage.clickDataInRatingSourceTableTarcksPage("Dayparts", "Morning");
                  await browser.sleep(1000);
                  elementStatus = await verify.verifyElementVisible(dayPartDemoPage.noDataMatchFound);
                  if (elementStatus) {
                        await dayPartDemoPage.addDayPartsDemo("Pittsburgh", "Morning");
                        demoValue = "A18-20";
                  }
                  else {
                        demoValue = String(await action.GetText(dayPartDemoPage.firstRowDemos, "")).trim();
                  }
                  await browser.sleep(2000);
                  await action.scrollToTopOfPage();
                  await homePage.appclickOnMenuListLeftPanel(subMenuName);
                  await verify.verifyElementIsDisplayed(inventoryPage.headerLocalProgram, "Verify Local Programs is displayed");
                  await localProgramPage.SelectMarketOptions("Pittsburgh");
                  await localProgramPage.SelectChannelsOptions(["WPGH"]);
                  await localProgramPage.SelectDaypartsOptions(["Morning"]);
                  await localProgramPage.clickOnActiveCheckBox();
                  report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
            }
            catch (err) {
                  report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
            }
      });

      //--------------Test Step------------
      it('Selct a Local Program from the grid and click on it', async () => {
            let stepAction = 'Selct a Local Program from the grid and click on it';
            let stepData;
            let stepExpResult = 'Pop up should display. In Edit Local Program page change Day(s) or Start Time and End Time .';
            let stepActualResult = ' In Edit,able to update Local Program page change Day(s) or Start Time and End Time .';
            let channels = ["WPGH"];
            let dayParts = ["Morning"];
            let days = ["Mo", "Tu", "We", "Th"];
            daysOfWeek = "DaysOfWeek=Mon&DaysOfWeek=Tue&DaysOfWeek=Wed&DaysOfWeek=Thu";
            try {
                  sellingTitle = await localProgramPage.addLocalProgram(channels, dayParts, days);
                  updatedStartTime = "02:00";
                  updatedEndTime = "03:00";
                  await action.SetText(localProgramPage.globalFilter, sellingTitle, "");
                  await browser.sleep(3000);
                  await action.Click(localProgramPage.firstRowSellingTitle, "Click On First Row Selling Title");
                  await browser.sleep(2000);
                  await localProgramPage.clickClockIconTimeOptionInventorydialog("xgc-starttime-time-picker");
                  await localProgramPage.clickTimeInInventorydialogTimetable(updatedStartTime);
                  await localProgramPage.clickTimeInInventorydialogTimetable(updatedStartTime);
                  await localProgramPage.clickClockIconTimeOptionInventorydialog("xgc-endtime-time-picker");
                  await localProgramPage.clickTimeInInventorydialogTimetable(updatedEndTime);
                  await localProgramPage.clickTimeInInventorydialogTimetable(updatedEndTime);
                  report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
            }
            catch (err) {
                  report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
            }
      });

      // --------------Test Step------------
      it('Click on Update button to update the new changes', async () => {

            let stepAction = 'Click on Update button to update the new changes';
            let stepData = '';
            let stepExpResult = 'Ater updation,new  details of record should display in the grid';
            let stepActualResult = 'Ater updation,new  details of record are displayed in the grid';
            try {
                  await localProgramPage.clickonButtonsRatecardPage("xgc-save-local-program","Update");
                  await browser.sleep(1000);
                  await localProgramPage.verifyPopupMessageRatecard("Successfully updated");
                  report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
            }
            catch (err) {
                  report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
            }
      });

      // --------------Test Step------------
      it('Navigate to Inventory-->Local Program Ratings Track View page', async () => {

            let stepAction = 'Navigate to Inventory-->Local Program Ratings Track View page';
            let stepData = '';
            let stepExpResult = 'Program Rating Tracks page should display ';
            let stepActualResult = 'Program Rating Tracks page is displayed ';
            try {
                  await localProgramPage.clickProgramLinkInLocalProgramGrid("Program Track", "Selling Title", sellingTitle);
                  report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
            }
            catch (err) {
                  report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
            }
      });


      // --------------Test Step------------
      it('Select the same Edited program from the programs list', async () => {

            let stepAction = 'Select the same Edited program from the programs list ';
            let stepData = sellingTitle;
            let stepExpResult = 'Days and time should change as edited in the local program page';
            let stepActualResult = 'Days and times are changed as edited in the local program page';
            try {
                  await programTracksPage.clickProgramInRatingsTracksView(stepData);
                  await programTracksPage.verifySellingTitleInProgramsList(stepData);
                  await browser.sleep(20000);
                  await action.Click(localProgramPage.displayOptions, "");
                  await programTracksPage.slideDecimalvalueInMatSlider("xgc-mat-slider-rtg", 3);
                  await programTracksPage.slideDecimalvalueInMatSlider("xgc-mat-slider-shr", 3);
                  await programTracksPage.slideDecimalvalueInMatSlider("xgc-mat-slider-hp", 3);
                  await programTracksPage.clickDataInRatingSourceTableTarcksPage("Source", sourceValue);
                  await programTracksPage.clickDataInRatingSourceTableTarcksPage("Collection Method", collectionMethodValue);
                  await programTracksPage.clickDataInRatingSourceTableTarcksPage("Sample", "DMA Universe");
                  await programTracksPage.clickDataInRatingSourceTableTarcksPage("Playback Type", "Live + Same Day (LS)");
                  await programTracksPage.clickDataInRatingSourceTableTarcksPage("Type", "Surveys");
                  await browser.sleep(3000);
                  await verify.verifyElementIsDisplayed(programTracksPage.gridTable, "verify grid is displayed.");
                  surveyName = String(await action.GetText(programTracksPage.firstRowName, "")).trim();
                  await verify.verifyElementIsDisplayed(programTracksPage.gridTable, "verify grid is displayed.");
                  metricsRatings = await programTracksPage.getFirstRowMetricsAndRatings();
                  uiRTGValue = metricsRatings["RTG"];
                  uiSHRValue = metricsRatings["SHR"];
                  uiHPValue = metricsRatings["HP"];
                  report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
            }
            catch (err) {
                  report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
            }
      });
      it('Get Ratings from Rating Api', async (done) => {
            let stepAction = 'Get Ratings from Rating Api ';
            let stepExpResult = 'Server Response should be 200 and rating details should be displayed';
            let stepActResult = 'Server response is 200';
            actualUpdatedStartTime = updatedStartTime + ":00";
            actualUpdatedEndTime = updatedEndTime + ":00";
            await console.log(globalvalues.ratingAPI + "Ratings/TimePeriod/Monthly?RatingSource=" + sourceValue + "&MarketSource=" + marketSource + "&CollectionMethod=" + collectionMethodValue + "&PlaybackType=" + playBackType + "&SurveySampleType=" + sampleValue + "&DistributorCodes=" + distributorCodes + "&ReportableDemographics=" + demoValue + "&SurveyName=" + surveyName + "&StartTime=" + String(actualUpdatedStartTime).replace(":", "%3A").replace(":", "%3A") + "&EndTime=" + String(actualUpdatedEndTime).replace(":", "%3A").replace(":", "%3A") + "&" + weeks + "&" + daysOfWeek + "&AverageBy=" + averageBy + "&Interval=" + interval);
            try {
                  reqAPI.get({
                        "headers": { "content-type": "application/json" },
                        "url": globalvalues.ratingAPI + "Ratings/TimePeriod/Monthly?RatingSource=" + sourceValue + "&MarketSource=" + marketSource + "&CollectionMethod=" + collectionMethodValue + "&PlaybackType=" + playBackType + "&SurveySampleType=" + sampleValue + "&DistributorCodes=" + distributorCodes + "&ReportableDemographics=" + demoValue + "&SurveyName=" + surveyName + "&StartTime=" + String(actualUpdatedStartTime).replace(":", "%3A").replace(":", "%3A") + "&EndTime=" + String(actualUpdatedEndTime).replace(":", "%3A").replace(":", "%3A") + "&" + weeks + "&" + daysOfWeek + "&AverageBy=" + averageBy + "&Interval=" + interval,
                        "body": JSON.stringify({
                        })
                  }, (error, response, body) => {
                        if (error) {
                              return console.dir(error);
                        }
                        responseStatusBody = response.body;
                        statusCode = response.statusCode;
                        if (statusCode != 200) {
                              done();
                              report.ReportStatus(stepAction, '', 'Fail', stepExpResult, stepActResult);
                        }
                        else {
                              done();
                              report.ReportStatus(stepAction, '', 'Pass', stepExpResult, stepActResult);
                        }

                  });
            }
            catch (err) {
                  report.ReportStatus(stepAction, '', 'Fail', stepExpResult, 'Server response is ' + statusCode);
            }
      });

      it('Validate "Share" "Rating" and "HutPut" Ratings values', async (done) => {
            let stepAction = 'Validate "Share" "Rating" and "HutPut" Ratings values ';
            let stepExpResult = '"Share" "Rating" and "HutPut" Ratings values Should be equal in API and UI';
            let stepActResult;
            let failedCount = 0;
            let errorMsg = "";
            let succesMsg = "";
            let apiRatings;
            try {
                  let jsonData = JSON.parse(responseStatusBody);
                  await console.log(jsonData);
                  let dataDetails = jsonData.data;
                  if (dataDetails == null || dataDetails.length == 0) {
                        report.ReportStatus(stepAction, '', 'Fail', stepExpResult, 'Data details are not displayed.');
                        done();
                  }
                  else {
                        apiRatings = await programTracksPage.getRatingsFromAPIAfterCalculation(dataDetails);
                        let finalPUT = apiRatings["HP"];
                        let finalShare = apiRatings["SHR"];
                        let finalRating = apiRatings["RTG"];
                        if (finalPUT.toFixed(3) != uiHPValue) {
                              errorMsg = errorMsg + "HutPut Value in API After Calculation:" + finalPUT.toFixed(3) + "\n HutPut value in UI:" + uiHPValue + "\n";
                              failedCount = failedCount + 1;
                        }
                        else {
                              succesMsg = succesMsg + "UI HutPut:" + uiHPValue + "\n API Calcualtion HutPut:" + finalPUT.toFixed(3) + "\n";
                        }
                        if (finalShare.toFixed(3) != uiSHRValue) {
                              failedCount = failedCount + 1;
                              errorMsg = errorMsg + "Share Value in API:" + finalShare.toFixed(3) + "\n Share value in UI:" + uiSHRValue + "\n";
                        }
                        else {
                              succesMsg = succesMsg + "Share in UI:" + uiSHRValue + "\n API Calcualtion Share:" + finalShare.toFixed(3) + "\n";
                        }
                        if (finalRating.toFixed(3) != uiRTGValue) {
                              failedCount = failedCount + 1;
                              errorMsg = errorMsg + "Rate Value in API After Calculation:" + finalRating.toFixed(3) + " \n Rate value in UI:" + uiRTGValue + "\n";
                        }
                        else {
                              succesMsg = succesMsg + "Rating in UI:" + uiRTGValue + "\n API Calcualtion Rating:" + finalRating.toFixed(3) + "\n";
                        }
                        if (failedCount != 0) {
                              done();
                              stepActResult = errorMsg;
                              report.ReportStatus(stepAction, '', 'Fail', stepExpResult, stepActResult);
                        }
                        else {
                              stepActResult = succesMsg;
                              done();
                              report.ReportStatus(stepAction, '', 'Pass', stepExpResult, stepActResult);
                        }
                  }
            }
            catch (err) {
                  done();
                  report.ReportStatus(stepAction, '', 'Fail', stepExpResult, ' not able to Get the advertiser details' + err);
            }
      });
      /**  // --------------Test Step------------
       it('Type-Surveys. Data should display in the grid acc to above selection', async () => {
 
             let stepAction = 'Type-Surveys. Data should display in the grid acc to above selection"';
             let stepData = '';
             let stepExpResult = 'Each Time Period Record for each survey should rebuild agains the new day or time change';
             let stepActualResult = 'Each Time Period Record for each survey is rebuild agains the new day or time change';
 
             try {
 
                   report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
             }
 
             catch (err) {
                   report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
             }
 
       });
 
 
       // --------------Test Step------------
       it('Check survey with program average record is removed', async () => {
 
             let stepAction = 'Check survey with program average record is removed';
             let stepData = '';
             let stepExpResult = 'There should not be any PAV record for a survey present after the new changes';
             let stepActualResult = 'There is no PAV record for a survey present after the new changes';
             try {
 
 
                   report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
             }
 
             catch (err) {
                   report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
             }
 
       });
       //--------------Test Step------------
       it('All the sample and Playback types columns are covered with changes', async () => {
 
             let stepAction = 'All the sample and Playback types columns are covered with changes';
             let stepData = '';
             let stepExpResult = 'Each and every sample and playback type available for the program should update with the changes';
             let stepActualResult = 'Each and every sample and playback type available for the program is updated with the changes';
 
             try {
 
                   report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
             }
 
             catch (err) {
                   report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
             }
 
       });
 
       // --------------Test Step------------
       it('All the demographics for Nielsen is updated against the new changes', async () => {
 
             let stepAction = 'All the demographics for Nielsen is updated against the new changes';
             let stepData = '';
             let stepExpResult = 'M12-17,M18-20,M25-34,M55-64 column should rebuild with new changes';
             let stepActualResult = 'M12-17,M18-20,M25-34,M55-64 column is rebuild with new changes';
 
             try {
 
                   report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
             }
 
             catch (err) {
                   report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
             }
 
       });*/

});
