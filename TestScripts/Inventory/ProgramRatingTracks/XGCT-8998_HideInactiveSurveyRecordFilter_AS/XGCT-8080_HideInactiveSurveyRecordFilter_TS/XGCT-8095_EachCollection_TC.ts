/*
***********************************************************************************************************
* @Script Name :  XGCT-8095
* @Description :  Inventory- Local Program Rating Tracks View - Given the user has disabled the filter object, verify the system returns the inactive records to the view for each collection method available for the user12-8-2019
* @Page Object Name(s) : LocalProgramming,ProgramRatingTracks
* @Dependencies/Libs : ActionLib,VerifyLib
* @Pre-Conditions : 
* @Creation Date : 12-8-2019
* @Author : Sivaraj
* @Modified By & Date:12-8-2019
*************************************************************************************************************
*/

 //Import Statements
 import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { ProgramRatingTracks } from '../../../../../Pages/Inventory/ProgramRatingTracks';
import { InventoryPage } from '../../../../../Pages/Inventory/InventoryCommon';
import { AppCommonFunctions } from '../../../../../Libs/ApplicationLibs/CommAppLib';

 
 //Import Class Objects Instantiation
 let report = new Reporter();
 let verify = new VerifyLib();
 let action = new ActionLib();
 let homePage = new HomePageFunction();
 let globalValue = new globalvalues();
 let localProgrammingPage = new LocalProgramming();
 let ProgramRatingTrack = new ProgramRatingTracks();
 let inventoryPage = new InventoryPage();
 let commonLib = new AppCommonFunctions();
//Variables Declaration
let TestCase_ID = 'XGCT-8095'
let programName;
let TestCase_Title = 'Inventory- Local Program Rating Tracks View - Given the user has disabled the filter object  verify the system returns the inactive records to the view for each collection method available for the user'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID,TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('INVENTORY- LOCAL PROGRAM RATING TRACKS VIEW - GIVEN THE USER HAS DISABLED THE FILTER OBJECT  VERIFY THE SYSTEM RETURNS THE INACTIVE RECORDS TO THE VIEW FOR EACH COLLECTION METHOD AVAILABLE FOR THE USER', () => {
// --------------Test Step------------
it("Navigate to QA story book.", async () => {

let stepAction = "Navigate to QA story book.";
let stepData = "http://qaxgstorybook.azurewebsites.net";
let stepExpResult = "QA story book should get loaded";
let stepActualResult = "User able to navigate to xGcampaign webpage";
let menuName = 'Programming';
let subMenuName = 'Local Programming'

try {
    await globalValue.LaunchStoryBook();
    await homePage.appclickOnNavigationList(menuName);
    await homePage.appclickOnMenuListLeftPanel(subMenuName);

report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select Inventory - Local Program Ratings Track View", async () => {

let stepAction = "Select Inventory - Local Program Ratings Track View";
let stepData = "";
let stepExpResult = "Local Program Ratings Track View should get loaded.";
let stepActualResult = "Local Program Ratings Track View should get loaded.";
let programDetails = {"Channels":["WPGH"],"Selling Title":"AT_"+new Date().getTime(),"Dayparts":["Morning"],"Days":["Mo","Tu","We","Th"],"Start Time":["09:00","09:30"],"End Time":["10:00","10:30"]};
programName = programDetails["Selling Title"];
try {
    await inventoryPage.addProgram(programDetails,"localprogram");
    await action.SetText(commonLib.globalFilter,programName,"");
    await verify.verifyProgressBarNotPresent();
    await localProgrammingPage.clickProgramLinkInLocalProgramGrid("Program Track","Selling Title",programDetails["Selling Title"]);
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select filter to display data in the grid", async () => {

let stepAction = "Select filter to display data in the grid";
let stepData = "Nielsen > LPM > DMA Universe > LS > Surveys";
let stepExpResult = "Page should load grid of monthly surveys.";
let stepActualResult = "Page load grid of monthly surveys.";

try {
    
    await ProgramRatingTrack.clickProgramInRatingsTracksView(programName)
    await ProgramRatingTrack.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live + Same Day (LS)");
    await browser.sleep(2000)
    // await action.SwitchToFrame(0)
    await verify.verifyProgressBarNotPresent()
    await verify.verifyElement(ProgramRatingTrack.SurveyElement,"GridSurveys")

report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("'Check the display of ''Show inactive surveys'''", async () => {

let stepAction = "'Check the display of ''Show inactive surveys'''";
let stepData = "";
let stepExpResult = "'Grid should have an option for ''Hide inactive surveys'' under Survey drop down.'";
let stepActualResult = "'Grid  have an option for ''Hide inactive surveys'' under Survey drop down.'";

try {
    let SurveyButton = await action.makeDynamicLocatorContainsText(ProgramRatingTrack.button_generic, "Survey", "Equal")
    await action.Click(SurveyButton, "SurveyButton")
    await browser.sleep(1000)
    await action.Click(ProgramRatingTrack.hideInactiveSurveysLink, "hideInactiveSurveysLink")
    await action.Click(SurveyButton, "SurveyButton")
    await browser.sleep(1000)
    await verify.verifyElement(ProgramRatingTrack.showInactiveSurveysLink, "showInactiveSurveysLink")
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("'Click on ''Show inactive surveys'' option.'", async () => {

let stepAction = "'Click on ''Show inactive surveys'' option.'";
let stepData = "";
let stepExpResult = "From the grid, inactive surveys should be enabled and displayed";
let stepActualResult = "From the grid, inactive surveys  enabled and displayed";

try {
    await action.Click(ProgramRatingTrack.showInactiveSurveysLink, "showInactiveSurveysLink")
    //await action.SwitchToDefaultFrame()
    await ProgramRatingTrack.verifyshowInActiveSurveysInSUBGrid()
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Check for the inactive surveys in the different Collection Methods", async () => {

let stepAction = "Check for the inactive surveys in the different Collection Methods";
let stepData = "Select Source as LPM+PPM";
let stepExpResult = "Inactive surveys should displayed for all available Collection Methods";
let stepActualResult = "Inactive surveys  displayed for all available Collection Methods";

try {
    await ProgramRatingTrack.clickDataInRatingSourceTableTarcksPage("xgc-list-source", "Comscore");
    await ProgramRatingTrack.clickDataInRatingSourceTableTarcksPage("xgc-list-sample", "DMA Universe");
    await ProgramRatingTrack.clickDataInRatingSourceTableTarcksPage("xgc-list-playback", "Live Only (LO)");
    await browser.sleep(2000)
    await ProgramRatingTrack.verifyshowInActiveSurveysInSUBGrid()
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


});
