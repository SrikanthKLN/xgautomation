/*
***********************************************************************************************************
* @Script Name :  XGCT-8535
* @Description :  xG Campaign - Inventory - Refactor:  Select Demographics Targets for View Rating Metrics in Program Tracks (Nielsen) : Verify the user can remove a previously-added demographic target to a specific/All daypart1-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 1-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, by } from 'protractor';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { Reporter } from '../../../../../Utility/htmlResult';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { InventoryPage } from '../../../../../Pages/Inventory/InventoryCommon';
import { SettingsPage } from '../../../../../Pages/Inventory/Settings';
import { DayPartDemoPage } from '../../../../../Pages/Inventory/Settings/DayPartDemo';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { ProgramRatingTracks } from '../../../../../Pages/Inventory/ProgramRatingTracks';
import { Gutils } from '../../../../../Utility/gutils';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let globalFunc = new globalvalues();
let homePage = new HomePageFunction();
let inventoryPage = new InventoryPage();
let settingsPage = new SettingsPage();
let dayPartDemoPage = new DayPartDemoPage();
let localProgramPage = new LocalProgramming();
let programTracksPage = new ProgramRatingTracks();
let  gUtils = new Gutils();

//Variables Declaration
let TestCase_ID = 'XGCT-8535'
let TestCase_Title = 'xG Campaign - Inventory - Refactor:  Select Demographics Targets for View Rating Metrics in Program Tracks (Nielsen) : Verify the user can remove a previously-added demographic target to a specific/All daypart'
let sellingTitleValue = "ATA"+new Date().getTime();
let currentDate = gUtils.getcurrentDate();
let endDate = gUtils.getcurrentDateAfterSomeDays(10);
let headers:Array<any> ;

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - INVENTORY - REFACTOR:  SELECT DEMOGRAPHICS TARGETS FOR VIEW RATING METRICS IN PROGRAM TRACKS (NIELSEN) : VERIFY THE USER CAN REMOVE A PREVIOUSLY-ADDED DEMOGRAPHIC TARGET TO A SPECIFIC/ALL DAYPART', () => {
    // --------------Test Step------------
    it("Navigate to the URL "+globalvalues.urlname, async () => {
        let stepAction = "Navigate to the  "+globalvalues.urlname+" URL";
        let stepData = globalvalues.urlname;
        let stepExpResult = "User should navigate to the application URL:"+globalvalues.urlname;
        let stepActualResult = "User is navigated to the application URL:"+globalvalues.urlname;
        try {
            
            await globalFunc.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Navigate to the Programming -->Local Programming", async () => {

        let stepAction = "Navigate to the Programming -->Local Programming";
        let stepData = "";
        let stepExpResult = "User should able to view Local Programming page";
        let stepActualResult = "User is able to view Local Programming page";
        let menuName  = "Programming";
        let subMenuName = "Local Programming";
        let elementStatus:boolean;
        try {
             await homePage.appclickOnNavigationList(menuName);
             await settingsPage.clickOnSettings();
            await settingsPage.clickOnSettingsSubMenuLink("Daypart Demo");
            await settingsPage.clickOnDayPartDemoSubMenuLink("Local Daypart Demo");
            await localProgramPage.SelectMarketOptions("Pittsburgh");
            await browser.sleep(1000);
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("Dayparts","Morning");
            await browser.sleep(1000);
            elementStatus = await verify.verifyElementVisible(dayPartDemoPage.noDataMatchFound);
            if(elementStatus)
            {
                await dayPartDemoPage.addDayPartsDemo("Pittsburgh","Morning");
            }
            await browser.sleep(2000);
            await action.scrollToTopOfPage();
            await homePage.appclickOnMenuListLeftPanel(subMenuName);
            await verify.verifyElementIsDisplayed(inventoryPage.headerLocalProgram,"Verify Local Programs is displayed");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a market from the list of markets", async () => {

        let stepAction = "Select a market from the list of markets";
        let stepData = "Pittsburgh";
        let stepExpResult = "User should select Market from the dropdown";
        let stepActualResult = "User is selected Market from the dropdown";

        try {
            await localProgramPage.SelectMarketOptions(stepData);
            await localProgramPage.SelectChannelsOptions(["WPGH"]);
            await localProgramPage.SelectDaypartsOptions(["Morning"]);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
    // --------------Test Step------------
    it("Click on Add button ", async () => {

        let stepAction = "Click on Add button ";
        let stepData = "";
        let stepExpResult = "User should click add button and select Program from the dropdown and a pop up opens";
        let stepActualResult = "User should click add button and select Program from the dropdown and a pop up opens";
        try {
            await localProgramPage.clickonButtonsRatecardPage("xgc-add-program-orbit-button","Add");
            await localProgramPage.clickDropdownOptionInventory("Local Program");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Fill all the required and necessary fields to Add Local Program page", async () => {
        let stepAction = "Fill all the required and necessary fields to Add Local Program page";
        let stepData = "";
        let stepExpResult = "User should provide all the details in Channels, Selling Title , Day-parts, Start Time, End Time, Day(s), and click on save button to save the record";
        let stepActualResult = "User should provide all the details in Channels, Selling Title , Day-parts, Start Time, End Time, Day(s), and click on save button to save the record";
        try {
            await localProgramPage.clickMultiselectDropDownInventoryAddProgram("channel");
            await localProgramPage.clickMultiSelectOptionInventoryAddProgram("WPGH");
            await localProgramPage.clickDescriptionInputInventory();
            await localProgramPage.enterTextInInputInventoryAddProgram("xgc-program-name",sellingTitleValue);
            await localProgramPage.clickMultiselectDropDownInventoryAddProgram("daypart");
            await localProgramPage.clickMultiSelectOptionInventoryAddProgram("Morning");
            await localProgramPage.clickDaysOptionInventorydialog("Mo");
            await localProgramPage.clickDaysOptionInventorydialog("Tu");
            await localProgramPage.clickDaysOptionInventorydialog("We");
            await localProgramPage.clickClockIconTimeOptionInventorydialog("xgc-starttime-time-picker");
            await localProgramPage.clickTimeInInventorydialogTimetable("01:00");
            await localProgramPage.clickTimeInInventorydialogTimetable("01:00");
            await localProgramPage.clickClockIconTimeOptionInventorydialog("xgc-endtime-time-picker");
            await localProgramPage.clickTimeInInventorydialogTimetable("04:00");
            await localProgramPage.clickTimeInInventorydialogTimetable("04:10");
            await localProgramPage.enterDateInInputInventoryAddProgram("xgc-telecast-startdate-date-picker",currentDate);
            await localProgramPage.enterDateInInputInventoryAddProgram("xgc-telecast-enddate-date-picker",endDate);
            await localProgramPage.clickonButtonsRatecardPage("xgc-save-local-program","Save");
            await browser.sleep(1000);
            await localProgramPage.verifyPopupMessageRatecard("Successfully added");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

   // --------------Test Step------------
   it("After adding the program select the required fields to check the program is added to the list in Local Program view page", async () => {

    let stepAction = "After adding the program select the required fields to check the program is added to the list in Local Program view page";
    let stepData = "";
    let stepExpResult = "User should provide details in Market, Channels, Dayparts And Active button selection as created for the program before and click on search button. The details should display in the grid.";
    let stepActualResult = "The details are displayed in the grid.";
    try {
        await localProgramPage.clickProgramLinkInLocalProgramGrid("Program Track","Selling Title",sellingTitleValue);
        report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
        report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

});
    // --------------Test Step------------
    it("Navigate to the local Program Rating Tracks view", async () => {

        let stepAction = "Navigate to the local Program Rating Tracks view";
        let stepData = "";
        let stepExpResult = "User should see Program Rating Tracks page ";
        let stepActualResult = "User is able to see Program Rating Tracks page ";
        try {
            // await localProgramPage.clickProgramLinkInLocalProgramGrid("Link","Selling Title",sellingTitleValue);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, "Covered Previous spec");
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
// --------------Test Step------------
    it("Check the added program is displayed in the Programs Section", async () => {
        let stepAction = "Check the added program is displayed in the Programs Section";
        let stepData = sellingTitleValue;
        let stepExpResult = "User should see the added program";
        let stepActualResult = "User see the added program";
        try {
            await programTracksPage.clickProgramInRatingsTracksView(stepData);
            await programTracksPage.verifySellingTitleInProgramsList(stepData);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
       // --------------Test Step------------
       it("Select Nielsen from the source column", async () => {

        let stepAction = "Select Nielsen from the source column";
        let stepData = "Nielsen";
        let stepExpResult = "User should select Nielsen from Source";
        let stepActualResult = "User is selected Nielsen from Source";
        try {
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-source",stepData)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
        // --------------Test Step------------
        it("Select one or more optios from collections method column", async () => {

            let stepAction = "Select one or more optios from collections method column";
            let stepData = "LPM";
            let stepExpResult = "'User should select one or more options from ";
            let stepActualResult = "'User is selected one or more options from ";
    
            try {
                await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-collection",stepData);
                report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
            }
    
            catch (err) {
                report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
            }
    
        });
            // --------------Test Step------------
    it("Check an extra sample type is available for the particular market", async () => {
        let stepAction = "Check an extra sample type is available for the particular market";
        let stepData = "DMA Universe";
        let stepExpResult = "User should see "+stepData+" sample type";
        let stepActualResult = "User is able to see "+stepData+" sample type ";
        let subMenuName = "Local Programming";
        let count = 0;
        try {
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-sample",stepData);
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-playback","Live + Same Day (LS)");
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-booktypes","Surveys");
            await browser.sleep(3000);
            headers = await programTracksPage.getDayPartHeaders();
            let lastIndexValue = headers[headers.length - 1];
            await action.scrollToTopOfPage();
            await browser.sleep(3000);
            await settingsPage.clickOnSettings();
            await settingsPage.clickOnSettingsSubMenuLink("Daypart Demo");
            await settingsPage.clickOnDayPartDemoSubMenuLink("Local Daypart Demo");
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("Dayparts","Morning");
            await dayPartDemoPage.clickOnDeleteButton(lastIndexValue);
            await browser.sleep(1000);
            await localProgramPage.verifyPopupMessageRatecard(dayPartDemoPage.deleteMessage);
            await browser.sleep(1000);
            await action.scrollToTopOfPage();
            await homePage.appclickOnMenuListLeftPanel(subMenuName);
            await localProgramPage.SelectMarketOptions("Pittsburgh");
            await localProgramPage.SelectChannelsOptions(["WPGH"]);
            await localProgramPage.SelectDaypartsOptions(["Morning"]);
            await localProgramPage.clickProgramLinkInLocalProgramGrid("Program Track","Selling Title",sellingTitleValue);
            await programTracksPage.clickProgramInRatingsTracksView(sellingTitleValue);
            await programTracksPage.verifySellingTitleInProgramsList(sellingTitleValue);
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-source","Nielsen");
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("Cxgc-list-collection","LPM");
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-sample","DMA Universe");
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-playback","Live + Same Day (LS)");
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("xgc-list-booktypes","Surveys");
            await browser.sleep(3000);
            await programTracksPage.validateDayPartRemovedFromProgramTrack(String(lastIndexValue).trim());
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
});
