/*
***********************************************************************************************************
* @Script Name :  XGCT-4670
* @Description :  Inventory(Local Program View) - Verify that user can "Delete Local Program" successfully through UI.23-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 23-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { ProgramRatingTracks } from '../../../../../Pages/Inventory/ProgramRatingTracks';
import { AppCommonFunctions } from '../../../../../Libs/ApplicationLibs/CommAppLib';
import { LocalLengthFactorsPricing } from '../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { InventoryPage } from '../../../../../Pages/Inventory/InventoryCommon';
import { globalTestData } from '../../../../../TestData/globalTestData';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let homePage = new HomePageFunction();
let globalValue = new globalvalues();
let localProgrammingPage = new LocalProgramming();
let ProgramRatingTrack = new ProgramRatingTracks();
let common = new AppCommonFunctions();
let localLF = new LocalLengthFactorsPricing();
let inventory = new InventoryPage();
let programDetails;
let programDetails1;
let modifyprogramDetails;
let modifyprogramDetails1;
let ramdonValue = new Date().toLocaleString().split('/').join('').split(' ').join('').split(',').join('').split(':').join('').split('AM').join('').split('PM').join('');

//Variables Declaration
let TestCase_ID = 'XGCT-4670_Delete_TC'
let TestCase_Title = 'Inventory(Local Program View) - Verify that user can "Delete Local Program" successfully through UI.'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('CLONE A LOCAL PROGRAM: VERIFY THE USER CAN PERFORM A CLONE OPERATION A PROGRAM ', () => {




  // --------------Test Step------------
  it("Open browser and enter the URL provide in Test data", async () => {

    let stepAction = "Open browser and enter the URL provide in Test data";
    let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
    let stepExpResult = "XGCampaign web page is displayed";
    let stepActualResult = "XGCampaign web page is displayed";


    try {
      await globalValue.LaunchStoryBook();


      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Click on Programs tab", async () => {

    let stepAction = "Click on Programs tab";
    let stepData = "";
    let stepExpResult = "Should display Day Parts page view and Inventory option in the left panel";
    let stepActualResult = " Day Parts page view and Inventory option in the left panel is displayed";
    let menuName = 'Programming';


    try {
      await homePage.appclickOnNavigationList(menuName);


      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });

  // --------------Test Step------------
  it("Click on Local Programs option", async () => {

    let stepAction = "Click on Local Programs option";
    let stepData = "";
    let stepExpResult = "Local Programs page should be displayed";
    let stepActualResult = "Local Programs page is displayed";
    let subMenuName = 'Local Programming'

    try {
      await homePage.appclickOnMenuListLeftPanel(subMenuName);
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Click 'Delete' button <without selecting any record on the grid>.", async () => {

    let stepAction = "Click 'Delete' button <without selecting any record on the grid>.";
    let stepData = "";
    let stepExpResult = "'Following floating error message should be displayed as ''Please select a Local Program to delete''.'";
    let stepActualResult = " floating error message is displayed as ''Please select a Local Program to delete''.'";
    programDetails = {
      "Channels": globalTestData.multipleChnnels,

      "Selling Title": "AT_" + new Date().getTime(),
      "Description": "AT_" + new Date().getTime(),
      "Dayparts": ["Morning"],
      "Days": ["Mo", "Tu", "We"],
      "Start Time": ["12:00", "12:10"],
      "End Time": ["09:00", "09:20"],
      "FTC": "04/01/2020",
      "LTC": "04/11/2020"
    }

    try {
      await inventory.addProgram(programDetails, "local program")
      await localProgrammingPage.clickDeleteButton("Delete");
      let Ntext1: string = String(await localProgrammingPage.GetTextFromElement(localProgrammingPage.Notification, "Toast Notification"));
      let Ntext = Ntext1.split('\n').join();
      console.log("Notification*** " + Ntext);
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("'Select any one record and Click ''Delete'' button.'", async () => {

    let stepAction = "'Select any one record and Click Delete button.'";
    let stepData = "";
    let stepExpResult = "Confirmation pop-up message should be displayed as ' Are you sure to delete this Local Program?' with Yes /No button";
    let stepActualResult = "Confirmation pop-up message is displayed as ' Are you sure to delete this Local Program?' with Yes /No button";

    try {
      await localProgrammingPage.selectRowFromRateCardViewTableByIndex(1);
      await localProgrammingPage.clickDeleteButton("Delete");
      await localProgrammingPage.verifyPopUpHeader("Delete Program")

      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Click 'No' on the pop-up dialog box and review the grid data.", async () => {

    let stepAction = "Click 'No' on the pop-up dialog box and review the grid data.";
    let stepData = "";
    let stepExpResult = "- Should be landed in 'Local Program View' page.- Record should NOT be get deleted.";
    let stepActualResult = "- Is landed in 'Local Program View' page.- Record is NOT be get deleted.";

    try {
      await localProgrammingPage.clickCancelButton("click on cancel button")
      await action.SetText(localProgrammingPage.globalFilter, programDetails["Selling Title"], "selling title")
      let values = await element(localProgrammingPage.sellingtitle).getText();

      console.log(values)

      if (values == programDetails["Selling Title"]) {
        console.log("pass")
      } else {

        console.log("fail")
      }
      await localProgrammingPage.clearTextBox(localProgrammingPage.globalFilter)
      await browser.sleep(5000)

      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Select any one record and Click ''Delete'' button.'", async () => {

    let stepAction = "Select any one record and Click ''Delete'' button.'";
    let stepData = "";
    let stepExpResult = "Confirmation pop-up message should be displayed as ' Are you sure to delete this Local Program?' with Yes /No button";
    let stepActualResult = "Confirmation pop-up message is displayed as ' Are you sure to delete this Local Program?' with Yes /No button";

    try {
      // await action.ClearText(localProgrammingPage.globalFilter,"selling title")
      // await browser.sleep(5000)
      await localProgrammingPage.selectRowFromRateCardViewTableByIndex(2);
      await localProgrammingPage.clickDeleteButton("Delete");
      await localProgrammingPage.verifyPopUpHeader("Delete Program")


      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Click 'Yes' on the pop-up dialog box and review the grid.", async () => {

    let stepAction = "Click 'Yes' on the pop-up dialog box and review the grid.";
    let stepData = "";
    let stepExpResult = "- Should be landed in 'Local Program View' page.- Deleted records should NOT be displayed on the grid.";
    let stepActualResult = " - Is landed in 'Local Program View' page.- Deleted records is NOT be displayed on the grid.";

    try {
      await localProgrammingPage.clickdeleteinpopup("click on delete button")
      await browser.sleep(5000)
      let Ntext1: string = String(await localProgrammingPage.GetTextFromElement(localProgrammingPage.Notification, "Toast Notification"));
      let Ntext = Ntext1.split('\n').join();
      console.log("Notification*** " + Ntext);
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("'Select more than one records(multiple) and Click ''Delete'' button.'", async () => {

    let stepAction = "'Select more than one records(multiple) and Click ''Delete'' button.'";
    let stepData = "";
    let stepExpResult = "Confirmation pop-up message should be displayed as ' Are you sure to delete this Local Program?' with Yes /No button.";
    let stepActualResult = "Confirmation pop-up message is displayed as ' Are you sure to delete this Local Program?' with Yes /No button.";
    programDetails1 = {
      "Channels": globalTestData.multipleChnnels,

      "Selling Title": "AT_" + new Date().getTime(),
      "Description": "AT_" + new Date().getTime(),
      "Dayparts": ["Morning"],
      "Days": ["Mo", "Tu", "We"],
      "Start Time": ["12:00", "12:10"],
      "End Time": ["09:00", "09:20"],
      "FTC": "04/01/2020",
      "LTC": "04/11/2020"
    }


    try {

      await inventory.addProgram(programDetails1, "local program")
      await localProgrammingPage.selectRowFromRateCardViewTableByIndex(2);
      await browser.sleep(5000)
      await localProgrammingPage.selectRowFromRateCardViewTableByIndex(1);
      await localProgrammingPage.clickDeleteButton("Delete");

      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Click 'No' on the pop-up dialog box and review the grid data.", async () => {

    let stepAction = "Click 'No' on the pop-up dialog box and review the grid data.";
    let stepData = "";
    let stepExpResult = "- Should be landed in 'Local Program View' page.- Record should NOT be get deleted.";
    let stepActualResult = "- Is landed in 'Local Program View' page.- Record is NOT be get deleted.";

    try {
      await localProgrammingPage.clickCancelButton("click on cancel button")
      await action.SetText(localProgrammingPage.globalFilter, programDetails1["Selling Title"], "selling title")
      let values = await element(localProgrammingPage.sellingtitle).getText();

      console.log(values)

      if (values == programDetails1["Selling Title"]) {
        console.log("pass")
      } else {

        console.log("fail")
      }
      await localProgrammingPage.clearTextBox(localProgrammingPage.globalFilter)
      await browser.sleep(5000)

      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });





  // --------------Test Step------------
  it("Click 'Yes' on the pop-up dialog box and review the grid.", async () => {

    let stepAction = "Click 'Yes' on the pop-up dialog box and review the grid.";
    let stepData = "";
    let stepExpResult = "- Should be landed in 'Local Program View' page.- Deleted records should NOT be displayed on the grid.";
    let stepActualResult = " - Is landed in 'Local Program View' page.- Deleted records is NOT be displayed on the grid.";

    try {
      // await localProgrammingPage.selectRowFromRateCardViewTableByIndex(1);
      await localProgrammingPage.clickDeleteButton("Delete");

      await localProgrammingPage.clickdeleteinpopup("click on delete button")
      let Ntext1: string = String(await localProgrammingPage.GetTextFromElement(localProgrammingPage.Notification, "Toast Notification"));
      let Ntext = Ntext1.split('\n').join();
      console.log("Notification*** " + Ntext);
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Goto Database and review the deleted records are still exists.'", async () => {

    let stepAction = "Goto Database and review the deleted records are still exists.'";
    let stepData = "<May not testable for QA>";
    let stepExpResult = "- Deleted Records should be exists in database and it will go for permanent deletion after 90 days.";
    let stepActualResult = "- Deleted Records should be exists in database and it will go for permanent deletion after 90 days.";

    try {

      report.ReportStatus(stepAction, stepData, 'SKIP', stepExpResult, 'Skip - Due to DB validation');
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });

});
