/*
***********************************************************************************************************
* @Script Name :  XGCT-8139
* @Description :  Inventory-PI-0 Refactor - Local/Network Inventory (Programs)-Local Program creation and Edit19-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 19-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { ProgramRatingTracks } from '../../../../../Pages/Inventory/ProgramRatingTracks';
import { AppCommonFunctions } from '../../../../../Libs/ApplicationLibs/CommAppLib';
import { LocalLengthFactorsPricing } from '../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { InventoryPage } from '../../../../../Pages/Inventory/InventoryCommon';
import { globalTestData } from '../../../../../TestData/globalTestData';




//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let homePage = new HomePageFunction();
let globalValue = new globalvalues();
let localProgrammingPage = new LocalProgramming();
let ProgramRatingTrack = new ProgramRatingTracks();
let common = new AppCommonFunctions();
let localLF = new LocalLengthFactorsPricing();
let inventory = new InventoryPage();
let programDetails;
let modifyprogramDetails;
let modifyprogramDetails1;

//Variables Declaration
let TestCase_ID = 'XGCT-8139_Edit_TC'
let TestCase_Title = 'Inventory-PI-0 Refactor - Local/Network Inventory (Programs)-Local Program creation and Edit'

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('INVENTORY-PI-0 REFACTOR - LOCAL/NETWORK INVENTORY (PROGRAMS)-LOCAL PROGRAM CREATION AND EDIT', () => {

  // --------------Test Step------------
  it("Open browser and enter the URL provide in Test data", async () => {

    let stepAction = "Open browser and enter the URL provide in Test data";
    let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
    let stepExpResult = "XGCampaign web page is displayed";
    let stepActualResult = "XGCampaign web page is displayed";


    try {
      await globalValue.LaunchStoryBook();


      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Click on Programs tab", async () => {

    let stepAction = "Click on Programs tab";
    let stepData = "";
    let stepExpResult = "Should display Day Parts page view and Inventory option in the left panel";
    let stepActualResult = " Day Parts page view and Inventory option in the left panel is displayed";
    let menuName = 'Programming';


    try {
      await homePage.appclickOnNavigationList(menuName);


      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });

  // --------------Test Step------------
  it("Click on Local Programs option", async () => {

    let stepAction = "Click on Local Programs option";
    let stepData = "";
    let stepExpResult = "Local Programs page should be displayed";
    let stepActualResult = "Local Programs page is displayed";
    let subMenuName = 'Local Programming'

    try {
      await homePage.appclickOnMenuListLeftPanel(subMenuName);
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Click Add button,select Add Local Program.", async () => {

    let stepAction = "Click Add button,select Add Local Program.";
    let stepData = "";
    let stepExpResult = "'''Add Local Program'' page should be displayed.'";
    let stepActualResult = "'''Add Local Program'' page is displayed.'";
    programDetails = {
      "Channels": globalTestData.multipleChnnels,

      "Selling Title": "AT_" + new Date().getTime(),
      "Description": "AT_" + new Date().getTime(),
      "Dayparts": ["Morning"],
      "Days": ["Mo", "Tu", "We"],
      "Start Time": ["12:00", "12:10"],
      "End Time": ["09:00", "09:20"],
      "FTC": "04/01/2020",
      "LTC": "04/11/2020"
    }

    try {


      await inventory.addProgram(programDetails, "local program")
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });

  // --------------Test Step------------
  it("Check the User is able to view newly created local program in the Grid.", async () => {

    let stepAction = "Check the User is able to view newly created local program in the Grid.";
    let stepData = "";
    let stepExpResult = "Should be able to view the newly created program in the grid.";
    let stepActualResult = "able to view the newly created program in the grid.";

    try {
      await verify.verifyProgressBarNotPresent()
      console.log(5000)
      console.log(programDetails["Selling Title"])
      await action.SetText(localProgrammingPage.globalFilter, programDetails["Selling Title"], "selling title")
      let values = await element(localProgrammingPage.sellingtitle).getText();

      console.log(values)

      if (values == programDetails["Selling Title"]) {
        console.log("pass")
      } else {

        console.log("fail")
      }



      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Click existing program name under 'Selling Title' column on the grid.", async () => {

    let stepAction = "Click existing program name under 'Selling Title' column on the grid.";
    let stepData = "";
    let stepExpResult = "'''Edit Local Program'' page should be displayed.'";
    let stepActualResult = "'''Edit Local Program'' page is displayed.'";

    try {
      await action.Click(localProgrammingPage.sellingtitle, "selling title");


      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Review 'Edit Local Program' page.", async () => {

    let stepAction = "Review 'Edit Local Program' page.";
    let stepData = "";
    let stepExpResult = "'- All the existing details should be displayed for that selected Local Program.Ex:";
    let stepActualResult = "'- All the existing details is displayed for that selected Local Program.Ex:";

    try {
      let valueinsellingtitle = await action.GetTextFromInput(localProgrammingPage.sellingtitleinpopup, "selling title")
      if (valueinsellingtitle == programDetails["Selling Title"]) {
        console.log("pass")
      } else {
        console.log("fail")
      }

      let valueinDescription = await action.GetTextFromInput(localProgrammingPage.descriptioninpopup, "descriptioninpopup")
      if (valueinDescription == programDetails["Description"]) {
        console.log("pass")
      } else {
        console.log("fail")
      }

      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Modify the following fields by (remove /delete) 1 of the values in each field below:", async () => {

    let stepAction = "Modify the following fields by (remove /delete) 1 of the values in each field below:";
    let stepData = "";
    let stepExpResult = "Modified values should be listed successfully.";
    let stepActualResult = "Modified values are listed successfully.";
    modifyprogramDetails = {

      "Selling Title": "AT_" + new Date().getTime(),
      "Description": "AT_" + new Date().getTime(),
      "Dayparts": ["Daytime"],
      "Days": ["Th"],
      "Start Time": ["1:00", "1:10"],
      "End Time": ["08:00", "09:20"],
      "FTC": "04/02/2020",
      "LTC": "04/12/2020"
    }
    let StartTime1 = "2:00 PM"
    let EndTime1 = "7:00 PM"

    try {
      await browser.sleep(5000)
      await action.ClearText(localProgrammingPage.sellingtitleinpopup, "")
      await browser.sleep(5000)
      await action.SetText(localProgrammingPage.sellingtitleinpopup, modifyprogramDetails["Selling Title"], "Selling Title")
      await action.ClearText(localProgrammingPage.descriptioninpopup, "")
      await action.SetText(localProgrammingPage.descriptioninpopup, modifyprogramDetails["Description"], "Description")
      await action.ClearText(localProgrammingPage.starttime, "")
      await action.SetText(localProgrammingPage.starttime, StartTime1, "StartTime_AddP")
      await browser.sleep(1000)
      await action.ClearText(localProgrammingPage.endtime, "")
      await action.SetText(localProgrammingPage.endtime, EndTime1, "StartTime_AddP")

      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Click Cancel button on the bottom.", async () => {

    let stepAction = "Click Cancel button on the bottom.";
    let stepData = "";
    let stepExpResult = "'Should be landed in ''Local Program View' page";
    let stepActualResult = " landed in ''Local Program View' page";

    try {
      await browser.sleep(5000)
      await action.Click(localProgrammingPage.cancel, "")

      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Modify the following fields by addi 1 of the values in each field below:", async () => {

    let stepAction = "Modify the following fields by addi 1 of the values in each field below:";
    let stepData = "";
    let stepExpResult = "Modified values should be listed successfully.";
    let stepActualResult = "Modified values are listed successfully.";
    modifyprogramDetails1 = {

      "Selling Title": "AT_" + new Date().getTime(),
      "Description": "AT_" + new Date().getTime(),
      "Dayparts": ["Daytime"],
      "Days": ["Mo", "Tu", "We"],
      "Start Time": ["1:00", "1:10"],
      "End Time": ["08:00", "09:20"],
      "FTC": "04/02/2020",
      "LTC": "04/12/2020"
    }
    let StartTime1 = "3:00 PM"
    let EndTime1 = "8:00 PM"

    try {
      await action.Click(localProgrammingPage.sellingtitle, "selling title");

      await browser.sleep(5000)
      await action.ClearText(localProgrammingPage.sellingtitleinpopup, "")
      await browser.sleep(5000)
      await action.SetText(localProgrammingPage.sellingtitleinpopup, modifyprogramDetails["Selling Title"], "Selling Title")
      await action.ClearText(localProgrammingPage.descriptioninpopup, "")
      await action.SetText(localProgrammingPage.descriptioninpopup, modifyprogramDetails["Description"], "Description")
      await action.ClearText(localProgrammingPage.starttime, "")
      await action.SetText(localProgrammingPage.starttime, StartTime1, "StartTime_AddP")
      await browser.sleep(1000)
      await action.ClearText(localProgrammingPage.endtime, "")
      await action.SetText(localProgrammingPage.endtime, EndTime1, "StartTime_AddP")
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Click Update button on the bottom.", async () => {

    let stepAction = "Click Update button on the bottom.";
    let stepData = "";
    let stepExpResult = "'Floating success message should be displayed as 'Successfully Updated Local Program'.";
    let stepActualResult = "'Floating success message is displayed as 'Successfully Updated Local Program'.";

    try {
      await action.MouseMoveToElement(localProgrammingPage.update, "")
      await browser.sleep(5000)
      await action.Click(localProgrammingPage.update, "")
      await localLF.verifyToastNotification("Successfully updated")

      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Click again, newly editied program and review all the fields (which ever modified)", async () => {

    let stepAction = "Click again, newly editied program and review all the fields (which ever modified)";
    let stepData = "";
    let stepExpResult = "Modified values should be listed successfully.";
    let stepActualResult = "Modified values are listed successfully.";

    try {
      await localProgrammingPage.clearTextBox(localProgrammingPage.globalFilter)
      await action.SetText(localProgrammingPage.globalFilter, modifyprogramDetails1["Selling Title"], "selling title")
      await action.Click(localProgrammingPage.sellingtitle, "selling title");
      await browser.sleep(5000)
      let valueinsellingtitle = await action.GetTextFromInput(localProgrammingPage.sellingtitleinpopup, "selling title")
      console.log(valueinsellingtitle)
      if (valueinsellingtitle == modifyprogramDetails1["Selling Title"]) {
        console.log("pass")
      } else {
        console.log("fail")
      }

      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


});
