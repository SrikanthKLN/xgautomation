/*
***********************************************************************************************************
* @Script Name :  XGCT-4051
* @Description :  Clone a Local Program: Verify the user can perform a clone operation a program 22-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 22-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

 //Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { ProgramRatingTracks } from '../../../../../Pages/Inventory/ProgramRatingTracks';
import { AppCommonFunctions } from '../../../../../Libs/ApplicationLibs/CommAppLib';
import { LocalLengthFactorsPricing } from '../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { InventoryPage } from '../../../../../Pages/Inventory/InventoryCommon';
import { globalTestData } from '../../../../../TestData/globalTestData';
 
 //Import Class Objects Instantiation
 let report = new Reporter();
 let verify = new VerifyLib();
 let action = new ActionLib();
 let homePage = new HomePageFunction();
 let globalValue = new globalvalues();
 let localProgrammingPage = new LocalProgramming();
 let ProgramRatingTrack = new ProgramRatingTracks();
 let common = new AppCommonFunctions();
 let localLF = new LocalLengthFactorsPricing();
 let inventory = new InventoryPage();
 let programDetails;
 let modifyprogramDetails;
 let modifyprogramDetails1;
 let ramdonValue = new Date().toLocaleString().split('/').join('').split(' ').join('').split(',').join('').split(':').join('').split('AM').join('').split('PM').join('');

 //Variables Declaration
let TestCase_ID = 'XGCT-4051_Clone_TC'
let TestCase_Title = 'Verify the user can perform a clone operation a program'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID,TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('CLONE A LOCAL PROGRAM: VERIFY THE USER CAN PERFORM A CLONE OPERATION A PROGRAM ', () => {




// --------------Test Step------------
it("Open browser and enter the URL provide in Test data", async () => {

    let stepAction = "Open browser and enter the URL provide in Test data";
    let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
    let stepExpResult = "XGCampaign web page is displayed";
    let stepActualResult = "XGCampaign web page is displayed";


    try {
      await globalValue.LaunchStoryBook();


      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Click on Programs tab", async () => {

    let stepAction = "Click on Programs tab";
    let stepData = "";
    let stepExpResult = "Should display Day Parts page view and Inventory option in the left panel";
    let stepActualResult = " Day Parts page view and Inventory option in the left panel is displayed";
    let menuName = 'Programming';


    try {
      await homePage.appclickOnNavigationList(menuName);


      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });

  // --------------Test Step------------
  it("Click on Local Programs option", async () => {

    let stepAction = "Click on Local Programs option";
    let stepData = "";
    let stepExpResult = "Local Programs page should be displayed";
    let stepActualResult = "Local Programs page is displayed";
    let subMenuName = 'Local Programming'

    try {
      await homePage.appclickOnMenuListLeftPanel(subMenuName);
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


// --------------Test Step------------
it("Select a program from the grid", async () => {

let stepAction = "Select a program from the grid";
let stepData = "";
let stepExpResult = "User should be able to select a record/program";
let stepActualResult = "User is  able to select a record/program";
programDetails = {
    "Channels": globalTestData.multipleChnnels,

    "Selling Title": "AT_" + new Date().getTime(),
    "Description": "AT_" + new Date().getTime(),
    "Dayparts": ["Morning"],
    "Days": ["Mo", "Tu", "We"],
    "Start Time": ["12:00", "12:10"],
    "End Time": ["09:00", "09:20"],
    "FTC": "04/01/2020",
    "LTC": "04/11/2020"
  }

try { 
    await inventory.addProgram(programDetails, "local program")
    await localProgrammingPage.selectRowFromRateCardViewTableByIndex(1);
  await browser.sleep(5000)

report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Once a program is selected,then click on the Clone button", async () => {

let stepAction = "Once a program is selected,then click on the Clone button";
let stepData = "";
let stepExpResult = "Clone local program pop up opens with Selling title field to add a new title,Clone and cancel buttons";
let stepActualResult = "Clone local program pop up opens with Selling title field to add a new title,Clone and cancel buttons";

try { 
    await inventory.clickonButtonsRatecardPage("Clone")
    await localProgrammingPage.verifyClonePoupInventory();
    await localProgrammingPage.verifysellingTitleInInventoryDialog();
    await localProgrammingPage.verifyButtonsInventoryDialog("Clone");
    await localProgrammingPage.verifyButtonsInventoryDialog("Cancel");

report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Add title and Click on clone from the clone Local program window", async () => {

let stepAction = "Add title and Click on clone from the clone Local program window";
let stepData = "";
let stepExpResult = "'Verify that the Confirmation message  should displayed that a program clone is successful ";
let stepActualResult = " Confirmation message  is displayed that a program clone is successful ";

try {
    await localProgrammingPage.enterTextInsellingTitleInInventoryDialog("Clone4051"+ramdonValue);
    await browser.sleep(1000);
    await localProgrammingPage.clickButtonsInventoryDialog("Clone");
    await localProgrammingPage.verifyPopupMessageRatecard("Successfully cloned local programs.");
   
    await inventory.clickonButtonsRatecardPage("Search");
    await browser.sleep(4000);
    await localProgrammingPage.verifydataInRateCardViewTableusingRateCard(3,"Clone4051"+ramdonValue,"Clone4051"+ramdonValue);
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select multiple programs/records", async () => {

let stepAction = "Select multiple programs/records";
let stepData = "";
let stepExpResult = "The clone button should get disabled";
let stepActualResult = "The clone button is disabled";

try { 
    await localProgrammingPage.selectRowFromRateCardViewTableByIndex(1);
    await localProgrammingPage.selectRowFromRateCardViewTableByIndex(2);


report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


});
