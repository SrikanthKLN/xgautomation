/*
***********************************************************************************************************
* @Script Name :  XGCT-8142
* @Description :  Inventory-PI-0 Refactor - Local/Network Inventory (Programs)-Local Orbit Program creation and Edit16-8-2019
* @Page Object Name(s) : InventoryLib
* @Dependencies/Libs : AppCommonFunctions,HomePageFunction,LocalProgramming,VerifyLib,ActionLib
* @Pre-Conditions : 
* @Creation Date : 21-8-2019
* @Author : Venkata Sivakumar
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, by} from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { AppCommonFunctions } from '../../../../../Libs/ApplicationLibs/CommAppLib';
import { globalTestData } from '../../../../../TestData/globalTestData';
import { InventoryPage } from '../../../../../Pages/Inventory/InventoryCommon';



//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let globalFunction = new globalvalues();
let homePage = new HomePageFunction();
let commonLib = new AppCommonFunctions();
let inventoryLib = new InventoryPage();
//Variables Declaration
let TestCase_ID = 'XGCT-8142'
let TestCase_Title = 'Inventory-PI-0 Refactor - Local/Network Inventory (Programs)-Local Orbit Program creation and Edit'
let programDetails;
let editSellingTitle;
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('INVENTORY-PI-0 REFACTOR - LOCAL/NETWORK INVENTORY (PROGRAMS)-LOCAL ORBIT PROGRAM CREATION AND EDIT', () => {


    // --------------Test Step------------
    it("Open browser and enter the URL provide in Test data", async () => {

        let stepAction = "Open browser and enter the URL provide in Test data";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "XGCampaign web page is displayed";
        let stepActualResult = "XGCampaign web page is displayed";

        try {
            await globalFunction.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on Inventory option in the left panel'", async () => {

        let stepAction = "Click on Inventory option in the left panel'";
        let stepData = "";
        let stepExpResult = "'Inventory option should be expanded with the below options:";
        let stepActualResult = "'Inventory option should be expanded with the below options:";

        try {
            await homePage.appclickOnNavigationList(globalTestData.programmingTabName);
            await homePage.appclickOnMenuListLeftPanel(globalTestData.leftMenuLocalProgrammingName);
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
 
    // --------------Test Step------------
    it("Click Add button,select Add Orbit Program.'", async () => {

        let stepAction = "Click Add button,select Add Orbit Program.'";
        let stepData = "";
        let stepExpResult = "'''Add Local Orbit'' page should be displayed.'";
        let stepActualResult = "'''Add Local Orbit'' page should be displayed.'";
        programDetails = {
            "Channels": ["WPGH"],
            "Selling Title": "AT_" + new Date().getTime(),
            "Description": "AT_" + new Date().getTime(),
            "Dayparts": ["Morning"],
            "Days": ["Mo", "Tu", "We"],
            "Start Time": ["12:00", "12:10"],
            "End Time": ["13:00", "01:10"],
            "FTC": "04/01/20",
            "LTC": "04/11/20",
        }
        try {
            await inventoryLib.addProgram(programDetails, "local orbit program");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select the following options 1. Channels(single/multiple) 2. Selling Title 3. Dayparts(one or more ), Lock 4. Genre(one or more) 5. Description(alpha/ numeric/ special characters) 6.Click Add under Orbit Details, Days selection(one or more ) 7. Both Start Time and End Time(Start time should not be greater than end time),add a two or more distinct day/time range(s)  8. FTC , LTC dates both(LTC date cannot be Lesser than FTC date)  9. Hiatus start and End dates(between the telecast start and end dates)  10. External ID 11. Auto build PAV,Source 12.Keyword Tags(one or more).13.Active/Inactive.Click Save button.Check  the user can add the program to the system", async () => {

        let stepAction = "Select the following options 1. Channels(single/multiple) 2. Selling Title 3. Dayparts(one or more ), Lock 4. Genre(one or more) 5. Description(alpha/ numeric/ special characters) 6.Click Add under Orbit Details, Days selection(one or more ) 7. Both Start Time and End Time(Start time should not be greater than end time),add a two or more distinct day/time range(s)  8. FTC , LTC dates both(LTC date cannot be Lesser than FTC date)  9. Hiatus start and End dates(between the telecast start and end dates)  10. External ID 11. Auto build PAV,Source 12.Keyword Tags(one or more).13.Active/Inactive.Click Save button.Check  the user can add the program to the system";
        let stepData = "";
        let stepExpResult = "'''Successfully added'' message should be displayed and should create a program .Data should get reset after saving'";
        let stepActualResult = "'''Successfully added'' message should be displayed and should create a program .Data should get reset after saving'";

        try {

            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, "Covered in previous spec");
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the User is able to view newly created Orbit program in the Grid.", async () => {

        let stepAction = "Check the User is able to view newly created Orbit program in the Grid.";
        let stepData = "";
        let stepExpResult = "Should be able to view the newly created Orbit program in the grid.";
        let stepActualResult = "Should be able to view the newly created Orbit program in the grid.";
        try {
            await commonLib.selectDropDownValue("Market", "Pittsburgh");
            await commonLib.selectMultiOptionsFromDropDown("channel", ["WPGH"]);
            await commonLib.selectMultiOptionsFromDropDown("daypart", ["Morning"]);
            await action.ClearText(commonLib.globalFilter,"");
            await action.SetText(commonLib.globalFilter, programDetails["Selling Title"], "");
            await verify.verifyProgressBarNotPresent();
            await browser.sleep(5000);
            let elementStatus: boolean = await verify.verifyElementVisible(commonLib.noDataMatchFound);
            if (elementStatus) throw "Failed-" + programDetails["Selling Title"] + " is not displayed in the grid";
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    // --------------Test Step 6------------
    it("Navigate to Program Type as Orbit and click on the hyper link under 'Selling Title' column on the grid.Check display of local orbit program.", async () => {

        let stepAction = "Navigate to Program Type as Orbit and click on the hyper link under 'Selling Title' column on the grid.Check display of local orbit program.";
        let stepData = "";
        let stepExpResult = "'Edit Local Orbit program' page should be displayed.";
        let stepActualResult = "'Edit Local Orbit program' page is displayed.";

        try {
            let coulmnIndex = await commonLib.getTableColumnIndex("Selling Title");
            await action.Click(by.xpath(commonLib.gridCellData.replace("rowdynamic", "1").replace("columndynamic", String(coulmnIndex))), "");
            await verify.verifyElementIsDisplayed(inventoryLib.editProgramHeader, "");
            await action.Click(inventoryLib.cancelButton, "");
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Edit all values of a record.Click on Update button.Check whether the user can modify a local orbit program in the view.", async () => {

        let stepAction = "Edit all values of a record.Click on Update button.Check whether the user can modify a local orbit program in the view.";
        let stepData = "";
        let stepExpResult = "'Successfully Updated' message should be displayed.";
        let stepActualResult = "'Successfully Updated' message is displayed.'";
         editSellingTitle = programDetails["Selling Title"];
        let editProgramDetails = {
            "Selling Title": "AT_" + new Date().getTime(),
            "Description": "AT_" + new Date().getTime(),
            "Dayparts": ["Daytime"],
            "Days": ["Mo", "Tu", "Th"],
            "Start Time": ["12:00", "12:40"],
            "End Time": ["13:00", "01:50"],
            "FTC": "04/01/20",
            "LTC": "04/11/20",
        }
        try {
            await action.Click(commonLib.imgClear,"");
            await commonLib.selectDropDownValue("Market", "Pittsburgh");
            await commonLib.selectMultiOptionsFromDropDown("channel", ["WPGH"]);
            await commonLib.selectMultiOptionsFromDropDown("daypart", ["Morning"]);
            await inventoryLib.editProgram(editSellingTitle, editProgramDetails, "localorbitprogram");
            editSellingTitle = editProgramDetails["Selling Title"];
            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Edit the Selling Title of a local orbit program and click on Update button.Check whether the user can update.", async () => {

        let stepAction = "Edit the Selling Title of a local orbit program and click on Update button.Check whether the user can update.";
        let stepData = "";
        let stepExpResult = "'Successfully updated message should be displayed.'";
        let stepActualResult = "Successfully updated message is displayed.'";
        let sellingTitle = editSellingTitle;
        let editProgramDetails = { "Selling Title": "AT_" + new Date().getTime() }

        try {
            await action.Click(commonLib.imgClear,"");
            await commonLib.selectDropDownValue("Market", "Pittsburgh");
            await commonLib.selectMultiOptionsFromDropDown("channel", ["WPGH"]);
            await commonLib.deSelectAllMultiDropdownOptions("daypart");
            await commonLib.selectMultiOptionsFromDropDown("daypart", ["Daytime"]);
            await action.Click(inventoryLib.searchButton,"");
            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent();
            await inventoryLib.editProgram(sellingTitle, editProgramDetails, "localorbitprogram");
            editSellingTitle = await editProgramDetails["Selling Title"];
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Do not enter least one character in the field name(Selling Title)and click on Update button.Check whether error message is displayed.", async () => {

        let stepAction = "Do not enter least one character in the field name(Selling Title)and click on Update button.Check whether error message is displayed.";
        let stepData = "";
        let stepExpResult = "'''Selling Title is required'' error message should be displayed below the field name.'";
        let stepActualResult = "'''Selling Title is required'' error message is displayed  below the field name.'";
        let sellingTitle = editSellingTitle;
        try {
            await action.Click(commonLib.imgClear,"");
            await commonLib.selectDropDownValue("Market", "Pittsburgh");
            await commonLib.selectMultiOptionsFromDropDown("channel", ["WPGH"]);
            await commonLib.deSelectAllMultiDropdownOptions("daypart");
            await commonLib.selectMultiOptionsFromDropDown("daypart", ["Daytime"]);
            await action.Click(inventoryLib.searchButton,"");
            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent();
            await action.ClearText(commonLib.globalFilter, "");
            await action.SetText(commonLib.globalFilter, sellingTitle, "");
            await verify.verifyProgressBarNotPresent();
            await browser.sleep(2000);
            let status: boolean = await verify.verifyElementVisible(commonLib.noDataMatchFound);
            if (status) throw "Failed-No data match is found is displayed.'" + sellingTitle + "' record is not displayed in the Grid";
            let coulmnIndex = await commonLib.getTableColumnIndex("Selling Title");
            await action.Click(by.xpath(commonLib.gridCellData.replace("rowdynamic", "1").replace("columndynamic", String(coulmnIndex))), "");
            await verify.verifyElementIsDisplayed(inventoryLib.editProgramHeader, "");
            await browser.sleep(2000);
            await commonLib.clearTextUsingBackSpace(sellingTitle,inventoryLib.sellingTitleInput);
            await action.Click(inventoryLib.descriptionTextArea, "");
            let errorMsg = await action.GetText(inventoryLib.sellingTitleInputError, "");
            if (!String(errorMsg).trim().includes("Selling Title is required")) throw "Failed-Selling Title is required error is not displayed.Actual Error:" + errorMsg;
            await action.Click(inventoryLib.cancelButton, "Click On Cancel Button");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Add one/more than one Dayparts for a local orbit program.Check whether the user can add it", async () => {

        let stepAction = "Add one/more than one Dayparts for a local orbit program.Check whether the user can add it";
        let stepData = "";
        let stepExpResult = "'Successfully updated message should be displayed'";
        let stepActualResult = "Successfully updated message is displayed'";
        let editProgramDetails = {"Dayparts": ["Morning", "Daytime", "Early Fringe"] }
        try {
            await action.Click(commonLib.imgClear,"");
            await commonLib.selectDropDownValue("Market", "Pittsburgh");
            await commonLib.selectMultiOptionsFromDropDown("channel", ["WPGH"]);
            await commonLib.deSelectAllMultiDropdownOptions("daypart");
            await commonLib.selectMultiOptionsFromDropDown("daypart", ["Daytime"]);
            await action.Click(inventoryLib.searchButton,"");
            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent();
            await inventoryLib.editProgram(editSellingTitle, editProgramDetails, "localorbitprogram");
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Remove one/more than one Dayparts for a local orbit program.Click on Update.Check whether the user can remove it.", async () => {

        let stepAction = "Remove one/more than one Dayparts for a local orbit program.Click on Update.Check whether the user can remove it.";
        let stepData = "";
        let stepExpResult = "'Successfully updated' message should be displayed.'";
        let stepActualResult = "'Successfully updated' message is displayed.'";
        let sellingTitle = editSellingTitle;
        let editProgramDetails = { "Dayparts": ["Morning"] }
        try {
            await action.Click(commonLib.imgClear,"");
            await commonLib.selectDropDownValue("Market", "Pittsburgh");
            await commonLib.selectMultiOptionsFromDropDown("channel", ["WPGH"]);
            await commonLib.deSelectAllMultiDropdownOptions("daypart");
            await commonLib.selectMultiOptionsFromDropDown("daypart", ["Morning", "Daytime", "Early Fringe"]);
            await action.Click(inventoryLib.searchButton,"");
            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent();
            await inventoryLib.editProgram(sellingTitle, editProgramDetails, "localorbitprogram");
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Add one Daypart.Click on Update button.Check whether the user must include at least one Daypart designation to complete.", async () => {

        let stepAction = "Check whether the user must include at least one Daypart designation to complete.'";
        let stepData = "";
        let stepExpResult = "'''Successfully updated'' message should be displayed.'";
        let stepActualResult = "'''Successfully updated'' message should be displayed.'";

        try {
            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, "Covered In Previous Spec");
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Do not add one Daypart.Click on Update button.Check whether error message is displayed.", async () => {

        let stepAction = "Do not add one Daypart.Click on Update button.Check whether error message is displayed.";
        let stepData = "";
        let stepExpResult = "'Daypart is required' error message should be displayed.'";
        let stepActualResult = "'Daypart is required' error message is displayed.'";
        let sellingTitle = editSellingTitle;
        try {
           
            await action.Click(commonLib.imgClear,"");
            await commonLib.selectDropDownValue("Market", "Pittsburgh");
            await commonLib.selectMultiOptionsFromDropDown("channel", ["WPGH"]);
            await commonLib.deSelectAllMultiDropdownOptions("daypart");
            await commonLib.selectMultiOptionsFromDropDown("daypart", ["Morning"]);
            await action.Click(inventoryLib.searchButton,"");
            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent();
            await action.ClearText(commonLib.globalFilter, "");
            await action.SetText(commonLib.globalFilter, sellingTitle, "");
            await verify.verifyProgressBarNotPresent();
            await browser.sleep(2000);
            let status: boolean = await verify.verifyElementVisible(commonLib.noDataMatchFound);
            if (status) throw "Failed-No data match is found is displayed.'" + sellingTitle + "' record is not displayed in the Grid";
            let coulmnIndex = await commonLib.getTableColumnIndex("Selling Title");
            await action.Click(by.xpath(commonLib.gridCellData.replace("rowdynamic", "1").replace("columndynamic", String(coulmnIndex))), "");
            await verify.verifyElementIsDisplayed(inventoryLib.editProgramHeader, "");
            await commonLib.deSelectAllMultiDropdownOptions("daypart", "popup");
            await action.Click(inventoryLib.descriptionTextArea, "");
            let errorMsg = await action.GetText(commonLib.fieldError, "");
            if (!String(errorMsg).trim().includes("Dayparts is required")) throw "Failed-Dayparts is required error is not displayed.Actual Error:" + errorMsg;
            await action.Click(inventoryLib.cancelButton, "Click On Cancel Button");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Enter alpha, numeric and special characters in Description and click on Update button.Check whether the system can accept.", async () => {

        let stepAction = "Enter alpha, numeric and special characters in Description and click on Update button.Check whether the system can accept.";
        let stepData = "";
        let stepExpResult = "'Successfully updated' message should be displayed.'";
        let stepActualResult = "'Successfully updated' message is displayed.'";
        let sellingTitle = editSellingTitle;
        let editProgramDetails = { "Description": "AT_123$%^"}
        try {
            await action.Click(commonLib.imgClear,"");
            await commonLib.selectDropDownValue("Market", "Pittsburgh");
            await commonLib.selectMultiOptionsFromDropDown("channel", ["WPGH"]);
            await commonLib.deSelectAllMultiDropdownOptions("daypart");
            await commonLib.selectMultiOptionsFromDropDown("daypart", ["Morning"]);
            await action.Click(inventoryLib.searchButton,"");
            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent();
            await inventoryLib.editProgram(sellingTitle, editProgramDetails, "localorbitprogram");
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Edit the telecast start date(FTC) and end date(s)(LTC) for the local orbit program and click on Update button.Check whether the user can edit.", async () => {

        let stepAction = "Edit the telecast start date(FTC) and end date(s)(LTC) for the local orbit program and click on Update button.Check whether the user can edit.";
        let stepData = "";
        let stepExpResult = "'Successfully updated' message should be displayed.'";
        let stepActualResult = "'Successfully updated' message is displayed.'";
        let sellingTitle = editSellingTitle;
        let editProgramDetails = {
            "FTC": "04/05/20",
            "LTC": "04/20/20"
        }
        try {
            await action.Click(commonLib.imgClear,"");
            await commonLib.selectDropDownValue("Market", "Pittsburgh");
            await commonLib.selectMultiOptionsFromDropDown("channel", ["WPGH"]);
            await commonLib.deSelectAllMultiDropdownOptions("daypart");
            await commonLib.selectMultiOptionsFromDropDown("daypart", ["Morning"]);
            await action.Click(inventoryLib.searchButton,"");
            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent();
            await inventoryLib.editProgram(sellingTitle, editProgramDetails, "localorbitprogram");
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Enter the start date(FTC).Check whether the end date(LTC) cannot be entered starting earlier than the start date.", async () => {

        let stepAction = "Enter the start date(FTC).Check whether the end date(LTC) cannot be entered starting earlier than the start date.";
        let stepData = "";
        let stepExpResult = "Unable to select the end date as starting earlier than the start date.";
        let stepActualResult = "Unable to select the end date as starting earlier than the start date.";
        try {
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Add /Edit one or more hiatus start date,end dates between the telecast start(FTC) and end dates (LTC).Check whether the user can add.", async () => {

        let stepAction = "Add /Editone or more hiatus start date,end dates between the telecast start(FTC) and end dates (LTC).Check whether the user can add.";
        let stepData = "";
        let stepExpResult = "'Successfully updated' message should be displayed.'";
        let stepActualResult = "'Successfully updated' message is displayed.'";
        let sellingTitle = editSellingTitle;
        let editProgramDetails = {
            "Hiatus Start Date": "04/06/20",
            "Hiatus End Date": "04/10/20"
        }
        try {
            await action.Click(commonLib.imgClear,"");
            await commonLib.selectDropDownValue("Market", "Pittsburgh");
            await commonLib.selectMultiOptionsFromDropDown("channel", ["WPGH"]);
            await commonLib.deSelectAllMultiDropdownOptions("daypart");
            await commonLib.selectMultiOptionsFromDropDown("daypart", ["Morning"]);
            await action.Click(inventoryLib.searchButton,"");
            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent();
            await inventoryLib.editProgram(sellingTitle, editProgramDetails, "localorbitprogram");
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
      }
    });

    // --------------Test Step------------
    it("Remove a previously-created hiatus period.Click Update.Check the user is allowed to do it.", async () => {

        let stepAction = "Remove a previously-created hiatus period.Click Update.Check the user is allowed to do it.";
        let stepData = "";
        let stepExpResult = "Should be able to remove successfully.";
        let stepActualResult = "Should be able to remove successfully.";
        let sellingTitle = editSellingTitle;
        let editProgramDetails = {
            "Hiatus Start Date": "",
            "Hiatus End Date": ""
        }
        try {
            await action.Click(commonLib.imgClear,"");
            await commonLib.selectDropDownValue("Market", "Pittsburgh");
            await commonLib.selectMultiOptionsFromDropDown("channel", ["WPGH"]);
            await commonLib.deSelectAllMultiDropdownOptions("daypart");
            await commonLib.selectMultiOptionsFromDropDown("daypart", ["Morning"]);
            await action.Click(inventoryLib.searchButton,"");
            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent();
            await inventoryLib.editProgram(sellingTitle,editProgramDetails,"localorbitprogram");
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Add/Edit individual days for each defined range.Check the user can Add.", async () => {

        let stepAction = "Add/Edit individual days for each defined range.Check the user can Add.";
        let stepData = "";
        let stepExpResult = "Should be able to add/update successfully.";
        let stepActualResult = "Should be able to add/update successfully.";
        let editProgramDetails = {"Days": ["Mo"] }
        try {
            await action.Click(commonLib.imgClear,"");
            await commonLib.selectDropDownValue("Market", "Pittsburgh");
            await commonLib.selectMultiOptionsFromDropDown("channel", ["WPGH"]);
            await commonLib.deSelectAllMultiDropdownOptions("daypart");
            await commonLib.selectMultiOptionsFromDropDown("daypart", ["Morning"]);
            await action.Click(inventoryLib.searchButton,"");
            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent();
            await inventoryLib.editProgram(editSellingTitle, editProgramDetails, "localorbitprogram");
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
    catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Edit multiple record values.Click Update.Check the system has updated the record based on the changes made.", async () => {

        let stepAction = "Edit multiple record values.Click Update.Check the system has updated the record based on the changes made.";
        let stepData = "";
        let stepExpResult = "Should be updated successfully.";
        let stepActualResult = "Should be updated successfully.";
        try {
            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, "Covered In Previous spec");
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
 })
