/*
***********************************************************************************************************
* @Script Name :  XGCT-8142
* @Description :  Inventory-PI-0 Refactor - Local/Network Inventory (Programs)-Local Orbit Program Clone and Delete21-8-2019
* @Page Object Name(s) : InventoryLib
* @Dependencies/Libs : AppCommonFunctions,HomePageFunction,LocalProgramming,VerifyLib,ActionLib
* @Pre-Conditions : 
* @Creation Date : 21-8-2019
* @Author : Venkata Sivakumar
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { AppCommonFunctions } from '../../../../../Libs/ApplicationLibs/CommAppLib';
import { globalTestData } from '../../../../../TestData/globalTestData';
import { InventoryPage } from '../../../../../Pages/Inventory/InventoryCommon';



//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let globalFunction = new globalvalues();
let homePage = new HomePageFunction();
let commonLib = new AppCommonFunctions();
let inventoryLib = new InventoryPage();
//Variables Declaration
let TestCase_ID = 'XGCT-8142_CloneAndDelete'
let TestCase_Title = 'Inventory-PI-0 Refactor - Local/Network Inventory (Programs)-Local Orbit Program clone and delete'
let programDetails;
let cloneProgramDetails;
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('INVENTORY-PI-0 REFACTOR - LOCAL/NETWORK INVENTORY (PROGRAMS)-LOCAL ORBIT PROGRAM CLONE AND DELETE', () => {


    // --------------Test Step------------
    it("Open browser and enter the URL provide in Test data", async () => {

        let stepAction = "Open browser and enter the URL provide in Test data";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "XGCampaign web page is displayed";
        let stepActualResult = "XGCampaign web page is displayed";

        try {
            await globalFunction.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    // --------------Test Step------------
    it("Click on Inventory option in the left panel'", async () => {

        let stepAction = "Click on Inventory option in the left panel'";
        let stepData = "";
        let stepExpResult = "'Inventory option should be expanded with the below options:";
        let stepActualResult = "'Inventory option should be expanded with the below options:";

        try {
            await homePage.appclickOnNavigationList(globalTestData.programmingTabName);
            await homePage.appclickOnMenuListLeftPanel(globalTestData.leftMenuLocalProgrammingName);
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Clone record", async () => {

        let stepAction = "Clone record";
        let stepData = "";
        let stepExpResult = "Successfully cloned local programs message should be displayed";
        let stepActualResult = "Successfully cloned local programs message is displayed";
        programDetails = {
            "Channels": ["WPGH"],
            "Selling Title": "AT_" + new Date().getTime(),
            "Description": "AT_" + new Date().getTime(),
            "Dayparts": ["Morning"],
            "Days": ["Mo", "Tu", "We"],
            "Start Time": ["12:00", "12:10"],
            "End Time": ["13:00", "01:10"],
            "FTC": "04/01/20",
            "LTC": "04/11/20",
        }
        try {
            await commonLib.selectDropDownValue("Market", "Pittsburgh");
            await commonLib.selectMultiOptionsFromDropDown("channel", ["WPGH"]);
            await commonLib.selectMultiOptionsFromDropDown("daypart", ["Morning"]);
            await inventoryLib.addProgram(programDetails, "localorbitprogram");
            cloneProgramDetails = { "Existing Program": programDetails["Selling Title"], "Clone Program": "AT_" + new Date().getTime() };
            await inventoryLib.cloneProgram(cloneProgramDetails, "localorbitprogram");
            await verify.verifyProgressBarNotPresent();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Delete Record", async () => {

        let stepAction = "Delete Record";
        let stepData = "";
        let stepExpResult = "Successfully deleted local program(s) message should be displayed";
        let stepActualResult = "Successfully deleted local program(s) message is displayed";
        let sellingTitles = [cloneProgramDetails["Existing Program"], cloneProgramDetails["Clone Program"]];
        let failedCount = 0;
        let errorMsg = ""
        try {
            await commonLib.selectDropDownValue("Market", "Pittsburgh");
            await commonLib.selectMultiOptionsFromDropDown("channel", ["WPGH"]);
            await commonLib.selectMultiOptionsFromDropDown("daypart", ["Morning"]);
            await inventoryLib.deletePrograms(sellingTitles, "localorbitprogram");
            await verify.verifyProgressBarNotPresent();
            for (let index = 0; index < sellingTitles.length; index++) {
                await action.ClearText(commonLib.globalFilter, "");
                await action.SetText(commonLib.globalFilter, sellingTitles[index], "");
                await verify.verifyProgressBarNotPresent();
                let status: boolean = await verify.verifyElementVisible(commonLib.noDataMatchFound);
                if (!status) {
                    failedCount = failedCount + 1;
                    errorMsg = errorMsg + sellingTitles[index] + "is not deleted."
                }
            }
            if (failedCount > 0) throw "Failed-" + errorMsg;
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
})
