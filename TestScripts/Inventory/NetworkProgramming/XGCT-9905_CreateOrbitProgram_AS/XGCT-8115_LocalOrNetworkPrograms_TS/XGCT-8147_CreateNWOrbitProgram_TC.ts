/*
***********************************************************************************************************
* @Script Name :  XGCT-8147
* @Description :  Inventory-PI-0 Refactor - Local/Network Inventory (Programs)-Network Orbit Program creation19-8-2019
* @Page Object Name(s) : LocalProgramming
* @Dependencies/Libs : AppCommonFunctions,HomePageFunction,VerifyLib,ActionLib
* @Pre-Conditions : 
* @Creation Date : 19-8-2019
* @Author : Venkata Sivakumar
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { AppCommonFunctions } from '../../../../../Libs/ApplicationLibs/CommAppLib';
import { globalTestData } from '../../../../../TestData/globalTestData';
import { InventoryPage } from '../../../../../Pages/Inventory/InventoryCommon';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let globalFunction = new globalvalues();
let homePage = new HomePageFunction();
let localProgramming = new LocalProgramming();
let commonLib = new AppCommonFunctions();
let programDetails;
let inventoryPg = new InventoryPage();

//Variables Declaration
let TestCase_ID = 'XGCT-8147'
let TestCase_Title = 'Inventory-PI-0 Refactor - Local/Network Inventory (Programs)-Network Orbit Program creation'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('INVENTORY-PI-0 REFACTOR - LOCAL/NETWORK INVENTORY (PROGRAMS)-NETWORK ORBIT PROGRAM CREATION', () => {

    // --------------Test Step------------
    it("Open browser and enter the URL provide in Test data", async () => {

        let stepAction = "Open browser and enter the URL provide in Test data";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "XGCampaign web page should be displayed";
        let stepActualResult = "XGCampaign web page is displayed";

        try {
            await globalFunction.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Click on Inventory option in the left panel'", async () => {

        let stepAction = "Click on Inventory option in the left panel'";
        let stepData = "";
        let stepExpResult = "'Inventory option should be expanded with the below options:";
        let stepActualResult = "'Inventory option is expanded with the below options:";

        try {
            await homePage.appclickOnNavigationList(globalTestData.programmingTabName);
            await homePage.appclickOnMenuListLeftPanel(globalTestData.leftMenuNetworkProgrammingName);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click Add button,select Add Orbit Program.'", async () => {

        let stepAction = "Click Add button,select Add Orbit Program.'";
        let stepData = "";
        let stepExpResult = "Inventory Combo Program Creation page(Add Network Orbit) should be displayed.";
        let stepActualResult = "Inventory Combo Program Creation page(Add Network Orbit) is displayed.";
        programDetails = {
            "Selling Title": "AT_" + new Date().getTime(),
            "Description": "AT_" + new Date().getTime(),
            "Dayparts": ["Morning"],
            "Days": ["Mo", "Tu", "We"],
            "Start Time": ["12:00", "12:10"],
            "End Time": ["09:00", "09:20"],
            "FTC": "04/01/2020",
            "LTC": "04/11/2020"
        }
        try {
            await inventoryPg.addProgram(programDetails, "networkorbitprogram");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click Save button.Check  the user can add the program to the system'", async () => {

        let stepAction = "Click Save button.Check  the user can add the program to the system'";
        let stepData = "";
        let stepExpResult = "'''Successfully added'' message should be displayed and should create a program .Data should get reset after saving'";
        let stepActualResult = "'''Successfully added'' message is displayed and should create a program";
        try {
            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, "Covered in previuos spec");
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Check the User is able to view newly created local program in the Grid.", async () => {

        let stepAction = "Check the User is able to view newly created local program in the Grid.";
        let stepData = "";
        let stepExpResult = "Should be able to view the newly created program in the grid.";
        let stepActualResult = "Able to view the newly created program in the grid.";

        try {
            await action.SetText(commonLib.globalFilter, programDetails["Selling Title"], "")
            await browser.sleep(5000);
            await verify.verifyProgressBarNotPresent();
            let elementStatus: boolean = await verify.verifyElementVisible(commonLib.noDataMatchFound);
            if (elementStatus) throw "Failed-" + programDetails["Selling Title"] + " is not displayed in the grid";
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });


    // --------------Test Step------------
    it("Click on Add Button,select Orbit.", async () => {

        let stepAction = "Click on Add Button,select Orbit.";
        let stepData = "";
        let stepExpResult = "Add Network Orbit should be displayed,Save button should be disabled.";
        let stepActualResult = "Add Network Orbit is displayed,Save button is disabled.";

        try {
            await inventoryPg.clickonButtonsRatecardPage("Add");
            await localProgramming.clickDropdownOptionInventory("Orbit Program");
            await verify.verifyElementIsDisplayed(inventoryPg.disableSaveButton,"");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("'Click ''Cancel'' button on the bottom.'", async () => {

        let stepAction = "'Click ''Cancel'' button on the bottom.'";
        let stepData = "";
        let stepExpResult = "'Should be landed in ''Network Program View' page.'";
        let stepActualResult = "'Network Program View' page is displayed'";

        try {
            await action.Click(inventoryPg.cancelButton,"");
            await verify.verifyElementIsDisplayed(inventoryPg.networkProgramView,"");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Click on Add Button,select Orbit.", async () => {

        let stepAction = "Click on Add Button,select Orbit.";
        let stepData = "";
        let stepExpResult = "Add Network Orbit should be displayed,Save button should be disabled.";
        let stepActualResult = "Add Network Orbit should be displayed,Save button is disabled.";

        try {
            await inventoryPg.clickonButtonsRatecardPage("Add");
            await localProgramming.clickDropdownOptionInventory("Orbit Program");
            await verify.verifyElementIsDisplayed(inventoryPg.disableSaveButton,"");
            await action.Click(inventoryPg.cancelButton,"");
            await verify.verifyElementIsDisplayed(inventoryPg.networkProgramView,"");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("'Select ''Selling Title'',''Dayparts''.Check ''Save'' button is enabled.'", async () => {

        let stepAction = "'Select ''Selling Title'',''Dayparts''.Check ''Save'' button is enabled.'";
        let stepExpResult = "'''Save'' button should be enabled.'";
        let stepActualResult = "'''Save'' button is enabled.'";
        programDetails = {
            "Selling Title": "AT_" + new Date().getTime(),
            "Description": "AT_" + new Date().getTime(),
            "Dayparts": ["Morning"],
        }
        let stepData = "";
        let errorMessage:string = "Please add orbit details";
        try {
            await inventoryPg.addProgram(programDetails,"networkorbit",errorMessage);
            await action.Click(inventoryPg.cancelButton,"");
            await verify.verifyElementIsDisplayed(inventoryPg.networkProgramView,"");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("'Click on ''Save'' button.Check valid error message is displayed.'", async () => {

        let stepAction = "'Click on ''Save'' button.Check valid error message is displayed.'";
        let stepData = "";
        let stepExpResult = "'''Please add orbit details'' error message should be displayed.'";
        let stepActualResult = "'''Please add orbit details'' error message is displayed.'";
        try {
            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, "Covered in previuos spec");
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("'Click on ''Add'' button under Orbit details.Do not enter Start Time,End Time and click on ''Add'' button.Check valid error message is displayed.'", async () => {

        let stepAction = "'Click on ''Add'' button under Orbit details.Do not enter Start Time,End Time and click on ''Add'' button.Check valid error message is displayed.'";
        
        let stepExpResult = "'''Start Time and End Time cannot be blank in orbit details'' error message should be displayed.'";
        let stepActualResult = "'''Start Time and End Time cannot be blank in orbit details'' error message is displayed.'";
        programDetails = {
            "Selling Title": "AT_" + new Date().getTime(),
            "Description": "AT_" + new Date().getTime(),
            "Dayparts": ["Morning"],
        }
        let stepData = "";
        try {
            await inventoryPg.clickonButtonsRatecardPage("Add");
            await localProgramming.clickDropdownOptionInventory("Orbit Program");
            await inventoryPg.enterTextInInputInventoryAddProgram("programName", programDetails["Selling Title"]);
            await action.SetText(inventoryPg.descriptionTextArea, programDetails["Description"], "");
            await commonLib.selectMultiOptionsFromDropDown("daypart", programDetails["Dayparts"], "dialog");
            await verify.verifyElementIsDisplayed(inventoryPg.enableSaveButton,"");
          await action.Click(inventoryPg.addOrbitButton,"");
          
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("'Enter Start Time,End Time and click on ''Add'' button.Check valid error message is displayed.'", async () => {

        let stepAction = "'Enter Start Time,End Time and click on ''Add'' button.Check valid error message is displayed.'";
        let stepData = "";
        let stepExpResult = "'''Please Select Days'' error message should be displayed.'";
        let stepActualResult = "'''Please Select Days'' error message is displayed.'";
        let errorMessage:string = "StartTime and EndTime cannot be blank in orbit details";
        try {
            await action.Click(inventoryPg.addOrbitButton,"");
            await browser.sleep(2000);
            await localProgramming.verifyPopupMessageRatecard(errorMessage);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("'Select one or more days.And click on ''Save'' button.'", async () => {

        let stepAction = "'Select one or more days.And click on ''Save'' button.'";
       
        let stepExpResult = "'''Successfully added'' message should be displayed.'";
        let stepActualResult = "'''Successfully added'' message  is displayed.'";
        programDetails = {
            "Start Time": ["09:00","09:50"],
            "End Time": ["10:00","10:50"],
            "Days":["Mo","Tu"]
        }
        let stepData = "";
        let message = "Successfully Added";
        try {
            await inventoryPg.selectStartTimeInLocalOrNetworkOrbitProgram(programDetails);
            await inventoryPg.selectEndTimeInLocalOrNetworkOrbitProgram(programDetails);
            await inventoryPg.selectDaysInLocalOrNetworkOrbitProgram(programDetails);
            await action.Click(inventoryPg.enableSaveButton,"");
            await browser.sleep(2000);
            await localProgramming.verifyPopupMessageRatecard(message);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });
});
