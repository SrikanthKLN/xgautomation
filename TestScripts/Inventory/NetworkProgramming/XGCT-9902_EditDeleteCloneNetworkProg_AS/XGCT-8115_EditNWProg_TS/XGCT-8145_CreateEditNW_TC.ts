/*
***********************************************************************************************************
* @Script Name :  XGCT-8145
* @Description :  Inventory-PI-0 Refactor - Local/Network Inventory (Programs)-Network Program creation and Edit19-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : Action Lib, VerifyLib
* @Pre-Conditions : LocalProgramming,ProgramRatingTracks,InventoryLib
* @Creation Date : 19-8-2019
* @Author : Sivaraj
* @Modified By & Date:21-8-2019
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { ProgramRatingTracks } from '../../../../../Pages/Inventory/ProgramRatingTracks';
import { globalTestData } from '../../../../../TestData/globalTestData';
import { AppCommonFunctions } from '../../../../../Libs/ApplicationLibs/CommAppLib';
import { LocalLengthFactorsPricing } from '../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { InventoryPage } from '../../../../../Pages/Inventory/InventoryCommon';



//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let homePage = new HomePageFunction();
let globalValue = new globalvalues();
let localProgrammingPage = new LocalProgramming();
let ProgramRatingTrack = new ProgramRatingTracks();
let common = new AppCommonFunctions();
let localLF = new LocalLengthFactorsPricing();
let inventoryPage = new InventoryPage();
let programDetails;
let modifyprogramDetails;

//Variables Declaration
let TestCase_ID = 'XGCT-8145'
let TestCase_Title = 'Inventory-PI-0 Refactor - Local/Network Inventory (Programs)-Network Program creation and Edit'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('INVENTORY-PI-0 REFACTOR - LOCAL/NETWORK INVENTORY (PROGRAMS)-NETWORK PROGRAM CREATION AND EDIT', () => {

  // --------------Test Step------------
  it("Open browser and enter the URL provide in Test data", async () => {

    let stepAction = "Open browser and enter the URL provide in Test data";
    let stepData = "https://xgplatform-dev.myimagine.com/#/";
    let stepExpResult = "XGCampaign web page is displayed";
    let stepActualResult = "XGCampaign web page is displayed";

    try {
      await globalValue.LaunchStoryBook();
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Click on Inventory option in the left panel'", async () => {

    let stepAction = "Click on Inventory option in the left panel'";
    let stepData = "";
    let stepExpResult = "'Inventory option should be expanded with the below options:";
    let stepActualResult = "'Inventory option expanded with the below options:";
    let menuName = 'Programming';
    let subMenuName = 'Network Programming'
    try {
      await homePage.appclickOnNavigationList(menuName);
      await homePage.appclickOnMenuListLeftPanel(subMenuName);

      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Click Add button,select Add Network Program.'", async () => {

    let stepAction = "Click Add button,select Add Network Program.'";
    let stepData = "";
    let stepExpResult = "'''Add Network Program'' page should be displayed.'";
    let stepActualResult = "'''Add Network Program'' page  displayed.'";
    programDetails = {
      "Selling Title": "AT_" + new Date().getTime(),
      "Description": "AT_" + new Date().getTime(),
      "Dayparts": ["Morning"],
      "Days": ["Mo", "Tu", "We"],
      "Start Time": ["12:00", "12:10"],
      "End Time": ["09:00", "09:20"],
      "FTC": "04/01/2020",
      "LTC": "04/11/2020"
    }


    try {
      await inventoryPage.addProgram(programDetails, "networkprogram");
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });

  // --------------Test Step------------
  it("12. Auto build PAV,Source 12.one or more Keyword Tags.13.Genre(one or more).Click Save button.Check  the user can add the program to the system.'", async () => {

    let stepAction = "12. Auto build PAV,Source 12.one or more Keyword Tags.13.Genre(one or more).Click Save button.Check  the user can add the program to the system.'";
    let stepData = "";
    let stepExpResult = "'''Successfully added'' message should be displayed and should create a program .Data should get reset after saving'";
    let stepActualResult = "'''Successfully added'' message displayed and should create a program .Data reset after saving'";
    
    try {
      //await localProgrammingPage.verifyPopupMessageRatecard("Successfully Added");

      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Check the User is able to view newly created local program in the Grid.", async () => {

    let stepAction = "Check the User is able to view newly created local program in the Grid.";
    let stepData = "";
    let stepExpResult = "Should be able to view the newly created program in the grid.";
    let stepActualResult = "Able to view the newly created program in the grid.";

    try {
      await action.SetText(localProgrammingPage.globalFilterNW, programDetails["Selling Title"], "selling title")
      await browser.sleep(2000)
      let values = await element(localProgrammingPage.sellingtitleNW).getText();

      console.log(values)

      if (values == programDetails["Selling Title"]) {
        console.log("pass")
      } else {

        console.log("fail")
      }
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Check display of local orbit program.'", async () => {

    let stepAction = "Check display of local orbit program.'";
    let stepData = "";
    let stepExpResult = "'''Edit Network Program'' page should be displayed.'";
    let stepActualResult = "'''Edit Network Program'' page displayed.'";

    try {
      await action.Click(localProgrammingPage.sellingtitleNW, "selling title");
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("- 'Daypart(s)''", async () => {

    let stepAction = "- 'Daypart(s)''";
    let stepData = "";
    let stepExpResult = "Modified values should be listed successfully.";
    let stepActualResult = "Modified values listed successfully.";
    modifyprogramDetails = {

      "Selling Title": "AT_" + new Date().getTime(),
      "Description": "AT_" + new Date().getTime(),
      "Dayparts": ["Daytime"],
      "Days": ["Th"],
      "Start Time": ["1:00", "1:10"],
      "End Time": ["08:00", "09:20"],
      "FTC": "04/02/2020",
      "LTC": "04/12/2020"
    }


    try {
      await browser.sleep(2000)
      await action.ClearText(localProgrammingPage.sellingtitleinpopupNW, "")
      await action.SetText(localProgrammingPage.sellingtitleinpopupNW, modifyprogramDetails["Selling Title"], "Selling Title")
      await browser.sleep(5000)
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Click Cancel button on the bottom.", async () => {

    let stepAction = "Click Cancel button on the bottom.";
    let stepData = "";
    let stepExpResult = "'Should be landed in ''Local Program View' page";
    let stepActualResult = "'Should be landed in ''Local Program View' page";

    try {
      await action.Click(localProgrammingPage.cancelNW, "")
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Check the User is able to view newly created local program in the Grid.", async () => {

    let stepAction = "- External ID'";
    let stepData = "";
    let stepExpResult = "Modified values should be listed successfully.";
    let stepActualResult = "Modified values listed successfully.";
    modifyprogramDetails = {
      "Channels": globalTestData.multipleChnnels,

      "Selling Title": "AT_" + new Date().getTime(),
      "Description": "AT_" + new Date().getTime(),
      "Dayparts": ["Daytime"],
      "Days": ["Mo", "Tu", "We"],
      "Start Time": ["1:00", "1:10"],
      "End Time": ["08:00", "09:20"],
      "FTC": "04/02/2020",
      "LTC": "04/12/2020"
    }


    try {
      await browser.sleep(2000)
      await action.Click(localProgrammingPage.sellingtitleNW, "selling title");
      await browser.sleep(2000)
      await action.ClearText(localProgrammingPage.sellingtitleinpopupNW, "")
      await action.SetText(localProgrammingPage.sellingtitleinpopupNW, modifyprogramDetails["Selling Title"], "Selling Title")

      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }


    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Click Update button on the bottom.", async () => {

    let stepAction = "Click Update button on the bottom.";
    let stepData = "";
    let stepExpResult = "'Floating success message should be displayed as 'Successfully Updated Network Program'.";
    let stepActualResult = "'Floating success message displayed as 'Successfully Updated Network Program'.";

    try {
      await action.MouseMoveToElement(localProgrammingPage.updateNW, "")
      await action.Click(localProgrammingPage.updateNW, "")
      await localLF.verifyToastNotification("Successfully updated")
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Click again, newly editied program and review all the fields (which ever modified)", async () => {

    let stepAction = "Click again, newly editied program and review all the fields (which ever modified)";
    let stepData = "";
    let stepExpResult = "Modified values should be listed successfully.";
    let stepActualResult = "Modified values listed successfully.";

    try {
      await action.ClearText(localProgrammingPage.globalFilterNW, "selling title")
        await action.SetText(localProgrammingPage.globalFilterNW, modifyprogramDetails["Selling Title"], "selling title")
        await browser.sleep(5000)
      await action.Click(localProgrammingPage.sellingtitleNW, "selling title");
      await browser.sleep(2000)
      let valueinsellingtitle = await action.GetTextFromInput(localProgrammingPage.sellingtitleinpopupNW, "selling title")
      if (valueinsellingtitle == modifyprogramDetails["Selling Title"]) {
        console.log("pass")
      } else {
        console.log("fail")
      }
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });

  // --------------Test Step------------
  it("Modify the Dayparts field to blank and Click Update.", async () => {

    let stepAction = "Modify the Dayparts field to blank and Click Update.";
    let stepData = "";
    let stepExpResult = "if dayparts are marked as blank field should provide an validation until  least one Daypart is selected";
    let stepActualResult = "if dayparts are marked as blank field provide an validation until  least one Daypart is selected";
    programDetails = {

      "Dayparts": ["Morning"],

    }
    try {
          await common.unSelectMultiOptionsFromDropDown("daypart", programDetails["Dayparts"], "dialog")
          await verify.verifyElement(localProgrammingPage.dayPartValidation, "verified");
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("To verify start and end time validations", async () => {

    let stepAction = "To verify start and end time validations";
    let stepData = "";
    let stepExpResult = "validation message should be displayed to the user saying End time cannot be less than start time.";
    let stepActualResult = "validation message displayed to the user saying End time cannot be less than start time.";

    try {
      await action.MouseMoveJavaScript(localProgrammingPage.startTime,"")
      await localProgrammingPage.clearTextBox(localProgrammingPage.startTime);
      await verify.verifyElement(localProgrammingPage.dateValidation, "verified")
      await action.Click(localProgrammingPage.endDate, "Clicked")
      browser.sleep(1000)

      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("To verify add and remove options for the days", async () => {

    let stepAction = "To verify add and remove options for the days";
    let stepData = "";
    let stepExpResult = "User should have option to add more days to existing selection and user should be able to removed one / many from existing list.";
    let stepActualResult = "User should have option to add more days to existing selection and user should be able to removed one / many from existing list.";

    try {
      await localProgrammingPage.clickDaysOptionInventorydialog("Mo");
      await localProgrammingPage.clickDaysOptionInventorydialog("Tu");
      await localProgrammingPage.clickDaysOptionInventorydialog("We");
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("Modify the field to blank and update", async () => {

    let stepAction = "Modify the field to blank and update";
    let stepData = "";
    let stepExpResult = "if days are marked as blank field should provide an validation until  valid date in the field name to complete";
    let stepActualResult = "if days are marked as blank field should provide an validation until  valid date in the field name to complete";

    try {
      await verify.verifyElement(localProgrammingPage.saveDiasable, "")
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


  // --------------Test Step------------
  it("To verify start and end date validations", async () => {

    let stepAction = "To verify start and end date validations";
    let stepData = "";
    let stepExpResult = "Hiatus date range end date cannot be entered as starting earlier than the start date";
    let stepActualResult = "Hiatus date range end date cannot be entered as starting earlier than the start date";

    try {
      await localProgrammingPage.clearTextBox(localProgrammingPage.startdate);
      await action.SetText(localProgrammingPage.startdate, "01/09", "")
      await verify.verifyElement(localProgrammingPage.dayValidation, "")
      await browser.sleep(2000)
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });

  // --------------Test Step------------
  it("if user enables Auto build PAV from edit page, check for the options that are relevant", async () => {

    let stepAction = "if user enables Auto build PAV from edit page, check for the options that are relevant";
    let stepData = "";
    let stepExpResult = "'user has access to BOTH Nielsen and Comscore ratings sources, verify the system allows the user to change the rating source as ''Nielsen'' 'Comscore'' or ''All'' from the previous selection.'";
    let stepActualResult = "'user has access to BOTH Nielsen and Comscore ratings sources, verify the system allows the user to change the rating source as ''Nielsen'' 'Comscore'' or ''All'' from the previous selection.'";
    programDetails = {

      "Source": ["Nielsen", "Comscore"],

    }
    try {
      await action.ClickJavaScript(localProgrammingPage.Apav, "clicked")
      await browser.sleep(1000)
      await common.selectMultiOptionsFromDropDown("channel", programDetails["Source"], "dialog");
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });

  // --------------Test Step------------
  it("Check the network program edit page designations.", async () => {

    let stepAction = "Check the network program edit page designations.";
    let stepData = "";
    let stepExpResult = "User should have option to change the program as Network or live.";
    let stepActualResult = "User  have option to change the program as Network or live.";

    try {
      await verify.verifyElement(localProgrammingPage.liveOption, "option available")
      report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
    }

    catch (err) {
      report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
    }

  });


});
