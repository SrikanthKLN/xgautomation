/*
***********************************************************************************************************
* @Script Name :  XGCT-4818
* @Description :  xG Campaign - Inventory - Delete Network Parent Program : Verify the Delete functionality of Network Parent Program23-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 23-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

 //Import Statements
 import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
 import { Reporter } from '../../../../../Utility/htmlResult';
 import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
 import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
 import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
 import { globalvalues } from '../../../../../Utility/globalvalue';
 import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { AppCommonFunctions } from '../../../../../Libs/ApplicationLibs/CommAppLib';
import { InventoryPage } from '../../../../../Pages/Inventory/InventoryCommon';

 
   
   //Import Class Objects Instantiation
   let report = new Reporter();
   let verify = new VerifyLib();
   let action = new ActionLib();
   let homePage = new HomePageFunction();
   let globalValue = new globalvalues();
   let localProgrammingPage = new LocalProgramming();
   let common = new AppCommonFunctions();
   let inventoryPage=new InventoryPage();
   let programDetails;
   

//Variables Declaration
let TestCase_ID = 'XGCT-4818'
let TestCase_Title = 'xG Campaign - Inventory - Delete Network Parent Program : Verify the Delete functionality of Network Parent Program'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID,TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - INVENTORY - DELETE NETWORK PARENT PROGRAM : VERIFY THE DELETE FUNCTIONALITY OF NETWORK PARENT PROGRAM', () => {
// --------------Test Step------------
it("Navigate to the Storybook URL", async () => {

let stepAction = "Navigate to the Storybook URL";
let stepData = "http://qaxgstorybook.azurewebsites.net/";
let stepExpResult = "user should be navigated to the Storybook";
let stepActualResult = "user should be navigated to the Storybook";

try {
    await globalValue.LaunchStoryBook();
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Navigate to the Inventory -->Network Program View", async () => {

let stepAction = "Navigate to the Inventory -->Network Program View";
let stepData = "";
let stepExpResult = "User should be able to see Inventory Network Program view";
let stepActualResult = "User should be able to see Inventory Network Program view";
let menuName = 'Programming';
let subMenuName = 'Network Programming'
programDetails = {
    "Selling Title": "AT_" + new Date().getTime(),
    "Description": "AT_" + new Date().getTime(),
    "Dayparts": ["Morning"],
    "Days": ["Mo", "Tu", "We"],
    "Start Time": ["12:00", "12:10"],
    "End Time": ["09:00", "09:20"],
    "FTC": "04/01/2020",
    "LTC": "04/11/2020"
}
try {
    await homePage.appclickOnNavigationList(menuName);
    await homePage.appclickOnMenuListLeftPanel(subMenuName);
    await inventoryPage.addProgram(programDetails, "networkprogram");
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});
// --------------Test Step------------
it("To check the cancel functionality click on cancel button from the popup", async () => {

    let stepAction = "To check the cancel functionality click on cancel button from the popup";
    let stepData = "";
    let stepExpResult = "'1)Pop up should be closed and the user stays back in the same Network program view/grid";
    let stepActualResult = "'1)Pop up should be closed and the user stays back in the same Network program view/grid";
    
    try {
        await action.SetText(localProgrammingPage.globalFilterNW,programDetails["Selling Title"],"selling title")
        verify.verifyProgressBarNotPresent();
        await browser.sleep(2000)
        await action.Click(common.gridHeaderCheckBox,"")
        await browser.sleep(2000)
        await action.Click(inventoryPage.deleteButton, "Click On Delete Button");
        await action.Click(inventoryPage.confirmDeletePopupCancelButton,"");
    report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
     }
    
    catch (err) {
    report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
     }
    
    });
    


// --------------Test Step------------
it("Select a program from the grid", async () => {

let stepAction = "Select a program from the grid";
let stepData = "";
let stepExpResult = "User should be able to select a record/program";
let stepActualResult = "User should be able to select a record/program";


try {
    await inventoryPage.deletePrograms([programDetails["Selling Title"]], "Network")
   
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Once a program is selected,then click on the Delete button", async () => {

let stepAction = "Once a program is selected,then click on the Delete button";
let stepData = "";
let stepExpResult = "A pop up should display saying Are you sure to delete this program(s) with delete and cancel buttons";
let stepActualResult = "Covered in previous step";

try {

report.ReportStatus(stepAction,stepData,'Skip',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


// --------------Test Step------------
it("Select delete button from the pop", async () => {

let stepAction = "Select delete button from the pop";
let stepData = "";
let stepExpResult = "'1)The selected record need to be removed from the Network program view ";
let stepActualResult = "Covered in Previous step  ";

try {

report.ReportStatus(stepAction,stepData,'SKIP',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});

// --------------Test Step------------
it("To check the child records removed from the Local Program view navigate to the Inventory---> Local Program view", async () => {

let stepAction = "To check the child records removed from the Local Program view navigate to the Inventory---> Local Program view";
let stepData = "";
let stepExpResult = "'1)User should be able to see the Local Program view";
let stepActualResult = "Covered in  privious Step";

try {
    await browser.sleep(2000)
    await verify.verifyProgressBarNotPresent();
   let  elementStatus=await verify.verifyElementVisible(common.noDataMatchFound);
   if(!elementStatus)throw "'No data matching' is not displayed after deleting the "+programDetails["Selling Title"]
        
report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});




// --------------Test Step------------
it("To check the functionality of soft delete", async () => {

let stepAction = "To check the functionality of soft delete";
let stepData = "";
let stepExpResult = "user should validate this in data base";
let stepActualResult = "user should validate this in data base";

try {

report.ReportStatus(stepAction,stepData,'Skip',stepExpResult ,"Due to DB Validation");
 }

catch (err) {
report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);
 }

});


});
