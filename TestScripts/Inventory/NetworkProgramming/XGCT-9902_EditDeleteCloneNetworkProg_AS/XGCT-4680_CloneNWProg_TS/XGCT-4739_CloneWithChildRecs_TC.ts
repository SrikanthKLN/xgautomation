/*
***********************************************************************************************************
* @Script Name :  XGCT-4739
* @Description :  xG Campaign - Inventory - Clone Network Parent Program: Given the user has elected to continue the clone operation, verify the new local child record(s) has captured all details of the original Network parent program23-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 23-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { AppCommonFunctions } from '../../../../../Libs/ApplicationLibs/CommAppLib';
import { InventoryPage } from '../../../../../Pages/Inventory/InventoryCommon';
import { NetworkProgrammingSmoke } from '../../../../../Pages/SMOKE-TESTS-PAGES/NetworkProgrammingSmoke';
import { Gutils } from '../../../../../Utility/gutils';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let homePage = new HomePageFunction();
let globalValue = new globalvalues();
let localProgrammingPage = new LocalProgramming();
let common = new AppCommonFunctions();
let inventoryPage = new InventoryPage();
let networkProgram = new NetworkProgrammingSmoke();
let gUtils = new Gutils();
let programDetails;
//Variables Declaration
let TestCase_ID = 'XGCT-4739'
let recordChild = {};
let _rowcont: number;
let cloneprogramDetails
let currentDate = gUtils.getcurrentDate();
let endDate = gUtils.getcurrentDateAfterSomeDays(10);
let TestCase_Title = 'xG Campaign - Inventory - Clone Network Parent Program: Given the user has elected to continue the clone operation verify the new local child record(s) has captured all details of the original Network parent program'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - INVENTORY - CLONE NETWORK PARENT PROGRAM: GIVEN THE USER HAS ELECTED TO CONTINUE THE CLONE OPERATION VERIFY THE NEW LOCAL CHILD RECORD(S) HAS CAPTURED ALL DETAILS OF THE ORIGINAL NETWORK PARENT PROGRAM', () => {

    // --------------Test Step------------
    it("Navigate to the Storybook URL", async () => {

        let stepAction = "Navigate to the Storybook URL";
        let stepData = "https://xgplatform-dev.myimagine.com";
        let stepExpResult = "User should be navigated to the Storybook";
        let stepActualResult = "User navigated to the Storybook";

        try {
            await globalValue.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to the Inventory -->Local Program View", async () => {

        let stepAction = "Navigate to the Inventory -->Local Program View";
        let stepData = "";
        let stepExpResult = "Verify that the user is able to view the Local Program view";
        let stepActualResult = "User is able to view the Network Program view";
        let menuName = 'Programming';
        let subMenuName = 'Network Programming'
        programDetails = {
            "Selling Title": "AT_" + new Date().getTime(),
            "Description": "AT_" + new Date().getTime(),
            "Dayparts": ["Morning"],
            "Days": ["Mo", "Tu", "We"],
            "Start Time": ["12:00", "12:10"],
            "End Time": ["09:00", "09:20"],
            "FTC": "04/01/2020",
            "LTC": "04/11/2020"
        }
        try {
            await homePage.appclickOnNavigationList(menuName);
            await homePage.appclickOnMenuListLeftPanel(subMenuName);
            await inventoryPage.addProgram(programDetails, "networkprogram");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
         catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check that a cloned/child record of Network parent is visible in the Local view", async () => {

        let stepAction = "Check that a cloned/child record of Network parent is visible in the Local view";
        let stepData = "";
        let stepExpResult = "User is able to view the cloned/child record of the Network Parent program in the local program view";
        let stepActualResult = "User is able to view the cloned/child record of the Network Parent program in the local program view";
        cloneprogramDetails = {
            "Existing Program": programDetails["Selling Title"],
            "Clone Program": "AT_" + new Date().getTime()

        }
        try {

            await inventoryPage.cloneProgram(cloneprogramDetails, "Network")

            await action.ClearText(localProgrammingPage.globalFilterNW, cloneprogramDetails["Clone Program"])
            await browser.sleep(1000)
            await action.SetText(localProgrammingPage.globalFilterNW, cloneprogramDetails["Clone Program"], "selling title")
            await browser.sleep(5000)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
           
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check all the details captured for cloned child program of Network program", async () => {

        let stepAction = "Check all the details captured for cloned child program of Network program";
        let stepData = "";
        let stepExpResult = "'Verify the following details are captured";
        let stepActualResult = "following details are captured";

        try {
            await action.Click(common.expandButton, "clicked on expand button")
            _rowcont = Number(await networkProgram.returnRowCountInChildTableNetworkPrograms());
            if (_rowcont < 1 || typeof (_rowcont) == undefined) {
                throw new Error("Fail :No Child Record is present");

            }
            console.log("Row Count" + _rowcont);
            for (let i = 1; i <= _rowcont; i++) {
                let recordOne = Object(await networkProgram.returnDataInChildTableNetworkPrograms(i));
                recordChild[i] = recordOne;
                // console.log(Object.keys(recordOne));
                await networkProgram.verifyDataInChildTableNetworkPrograms("Market", recordOne["Market"]);
                await networkProgram.verifyDataInChildTableNetworkPrograms("Channel", recordOne["Channel"]);
                await networkProgram.verifyDataInChildTableNetworkPrograms("Selling Title", cloneprogramDetails["Clone Program"]);
                await networkProgram.verifyDataInChildTableNetworkPrograms("Dayparts", "EM");
                await networkProgram.verifyDataInChildTableNetworkPrograms("Days", "M-W");
                await networkProgram.verifyDataInChildTableNetworkPrograms("Start Time", recordOne["Start Time"]);
                await networkProgram.verifyDataInChildTableNetworkPrograms("End Time", recordOne["End Time"]);
                await networkProgram.verifyDataInChildTableNetworkPrograms("FTC", programDetails["FTC"]);
                await networkProgram.verifyDataInChildTableNetworkPrograms("LTC", programDetails["LTC"]);
            }
              
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
