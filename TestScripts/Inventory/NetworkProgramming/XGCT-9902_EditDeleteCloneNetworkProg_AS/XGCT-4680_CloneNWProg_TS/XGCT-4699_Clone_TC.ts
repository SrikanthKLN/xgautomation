/*
***********************************************************************************************************
* @Script Name :  XGCT-4699
* @Description :  xG Campaign - Inventory - Clone Network Parent Program : Verify the user can perform a clone operation for one or more Network parent program records23-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 23-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../Pages/Home/HomePage';
import { globalvalues } from '../../../../../Utility/globalvalue';
import { LocalProgramming } from '../../../../../Pages/Inventory/LocalProgramming';
import { AppCommonFunctions } from '../../../../../Libs/ApplicationLibs/CommAppLib';
import { InventoryPage } from '../../../../../Pages/Inventory/InventoryCommon';
import { NetworkProgrammingSmoke } from '../../../../../Pages/SMOKE-TESTS-PAGES/NetworkProgrammingSmoke';
import { Gutils } from '../../../../../Utility/gutils';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let homePage = new HomePageFunction();
let globalValue = new globalvalues();
let localProgrammingPage = new LocalProgramming();
let common = new AppCommonFunctions();
let inventoryPage = new InventoryPage();
let networkProgram = new NetworkProgrammingSmoke();
let gUtils = new Gutils();
let programDetails;

//Variables Declaration
let TestCase_ID = 'XGCT-4699'
let recordChild = {};
let _rowcont: number;
let prgramData
let currentDate = gUtils.getcurrentDate();
let endDate = gUtils.getcurrentDateAfterSomeDays(10);

let TestCase_Title = 'xG Campaign - Inventory - Clone Network Parent Program : Verify the user can perform a clone operation for one or more Network parent program records'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - INVENTORY - CLONE NETWORK PARENT PROGRAM : VERIFY THE USER CAN PERFORM A CLONE OPERATION FOR ONE OR MORE NETWORK PARENT PROGRAM RECORDS', () => {
    // --------------Test Step------------
    it("Navigate to the Storybook URL", async () => {

        let stepAction = "Navigate to the Storybook URL";
        let stepData = "https://xgplatform-dev.myimagine.com";
        let stepExpResult = "User should be navigated to the Storybook";
        let stepActualResult = "User should be navigated to the Storybook";

        try {
            await globalValue.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to the Inventory -->Network Program View", async () => {

        let stepAction = "Navigate to the Inventory -->Network Program View";
        let stepData = "";
        let stepExpResult = "User is able to view Inventory network Program grid";
        let stepActualResult = "User is able to view Inventory network Program grid";
        let menuName = 'Programming';
        let subMenuName = 'Network Programming'
        programDetails = {
            "Selling Title": "AT_" + new Date().getTime(),
            "Description": "AT_" + new Date().getTime(),
            "Dayparts": ["Morning"],
            "Days": ["Mo", "Tu", "We"],
            "Start Time": ["12:00", "12:10"],
            "End Time": ["09:00", "09:20"],
            "FTC": "04/01/2020",
            "LTC": "04/11/2020"
        }
        try {
            await homePage.appclickOnNavigationList(menuName);
            await homePage.appclickOnMenuListLeftPanel(subMenuName);
            await inventoryPage.addProgram(programDetails, "networkprogram");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select network,day parts from the filter section and click on search", async () => {

        let stepAction = "Select network,day parts from the filter section and click on search";
        let stepData = "";
        let stepExpResult = "The data in the grid is refreshed as per selected criteria";
        let stepActualResult = "Covered in Prvious Step";

        try {

            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Do not select any program and click on clone button on top of the grid", async () => {

        let stepAction = "Do not select any program and click on clone button on top of the grid";
        let stepData = "";
        let stepExpResult = "A message should display saying Please select a program(s) to clone";
        let stepActualResult = "A message should display saying Please select a program(s) to clone";

        try {
            await action.Click(inventoryPage.cloneButton, "")
            await localProgrammingPage.verifyPopupMessageRatecard("Please select a program for clone");

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a program from the grid", async () => {

        let stepAction = "Select a program from the grid";
        let stepData = "";
        let stepExpResult = "User should be able to select a program";
        let stepActualResult = "User should be able to select a program";
        let cloneprogramDetails = {
            "Existing Program": programDetails["Selling Title"],
            "Clone Program": "AT_" + new Date().getTime()

        }
        try {

            await inventoryPage.cloneProgram(cloneprogramDetails, "Network")

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select the clone button above the grid", async () => {

        let stepAction = "Select the clone button above the grid";
        let stepData = "";
        let stepExpResult = "Clone Network program pop up opens with Selling title field to add a new title,Clone and cancel buttons";
        let stepActualResult = "Covered in Prvious Step";

        try {

            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Add title and Click on clone from the clone network window", async () => {

        let stepAction = "Add title and Click on clone from the clone network window";
        let stepData = "";
        let stepExpResult = "'Verify that the ";
        let stepActualResult = "Covered in Prvious Step";

        try {

            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
