/*
***********************************************************************************************************
* @Script Name :  XGCT-7362
* @Description :  Genre- Regression test-cases3-9-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 3-9-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { NetworkProgramming } from '../../../../../../Pages/Inventory/NetworkProgramming';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { LocalProgramming } from '../../../../../../Pages/Inventory/LocalProgramming';
import { ProgramRatingTracks } from '../../../../../../Pages/Inventory/ProgramRatingTracks';
import { GenresPage } from '../../../../../../Pages/Inventory/Reference/Genres/Genres';

//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let globalFunc = new globalvalues();
let networkProgram=new NetworkProgramming();
let HomePage = new HomePageFunction();
let localProgPage = new LocalProgramming();
let programTracksPage = new ProgramRatingTracks();
let genrePg=new GenresPage();

let genrename="AT_GeE" + new Date().getTime();
let genrenameN="AT_GeEN" + new Date().getTime();
let genrenameEdit="AT_GenEdit" + new Date().getTime();
let genrenameNEdit="AT_Edit" + new Date().getTime();





//Variables Declaration
let TestCase_ID = 'XGCT-7362_Add_Edit_TC'
let TestCase_Title = 'Genre- Regression test-cases Add Edit'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('GENRE- REGRESSION TEST-CASES ADD EDIT', () => {

   
    // --------------Test Step------------
    it("Check if user is able to add the Genre to the application", async () => {

        let stepAction = "Check if user is able to add the Genre to the application";
        let stepData = "";
        let stepExpResult = "A Genre should be added successfully";
        let stepActualResult = "A Genre should be added successfully";

        try {
            await globalFunc.LaunchStoryBook();
            await HomePage.appclickOnNavigationList("Programming");
            await HomePage.appclickOnMenuListLeftPanel("Reference");
            await HomePage.appclickOnMenuListLeftPanel("Genres");
            await HomePage.appclickOnMenuListLeftPanel("Local Genres");
            await verify.verifyProgressBarNotPresent();
            await browser.sleep(1000);
            await programTracksPage.clickComponentPdropdown("xgc-dropdown-market");
            await programTracksPage.selectOptionsFromComponentPdropdown("Pittsburgh");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-channels");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await action.scrollToTopOfPage();
            await localProgPage.clickonButtonsRatecardPage("xgc-btn-add-genre","Add");
            await localProgPage.enterTextInInputInventoryAddProgramUsingLabel("Name",genrename);
            await localProgPage.enterTextInInputInventoryAddProgramUsingLabel("Description","Description Gener");
            await programTracksPage.selectCheckBox("Campaign");
            await browser.sleep(1000);
            await localProgPage.clickonButtonsRatecardPage("xgc-genre-edit","Create");

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check by Cloning a Genre.", async () => {

        let stepAction = "Check by Cloning a Genre.";
        let stepData = "";
        let stepExpResult = "'System should ask to enter Genre title.";
        let stepActualResult = "'System should ask to enter Genre title.";

        try {
           
            await localProgPage.GridValidations("Name",genrename);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Edit- selected Genre ", async () => {

        let stepAction = "Edit- selected Genre'";
        let stepData = "";
        let stepExpResult = "Genre should get deleted from the list.";
        let stepActualResult = "Genre should get deleted from the list.";

        try {
            await genrePg.clickDataInGenerViewTable(3,genrename);
            await localProgPage.enterTextInInputInventoryAddProgramUsingLabel("Name",genrenameEdit);
            await localProgPage.clickonButtonsRatecardPage("xgc-genre-edit","Update");
            await localProgPage.GridValidations("Name",genrenameEdit);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("- Change the status of the genre as inactive and update'", async () => {

        let stepAction = "- Change the status of the genre as inactive and update'";
        let stepData = "";
        let stepExpResult = "'Changes should be saved in the list.";
        let stepActualResult = "'Changes should be saved in the list.";

        try {
            await genrePg.clickDataInGenerViewTable(2,genrenameEdit);
            await browser.sleep(3000);
            await browser.refresh();
            //await localProgPage.verifyPopupMessageRatecard("Genre status Updated  successfully.");
            await HomePage.appclickOnMenuListLeftPanel("Local Programming");
            await programTracksPage.clickComponentPdropdown("xgc-markets");
            await programTracksPage.selectOptionsFromComponentPdropdown("Pittsburgh");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-channels");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-dayparts");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await action.scrollToTopOfPage();
            await localProgPage.clickonButtonsRatecardPage("xgc-add-program-orbit-button","Add");
            await localProgPage.clickDropdownOptionInventory("Local Program");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("channel");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("daypart");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await genrePg.enterTextInXgTypeahead("xgc-select-genre",genrenameEdit);
            await genrePg.verifyNoSuggetionInAutocompletewidget(genrenameEdit);
            await localProgPage.clickonButtonsRatecardPage("xgc-cancel-local-program","Cancel");
            await HomePage.appclickOnMenuListLeftPanel("Reference");
            await HomePage.appclickOnMenuListLeftPanel("Genres");
            await HomePage.appclickOnMenuListLeftPanel("Local Genres");
            await verify.verifyProgressBarNotPresent();
            await genrePg.clickDataInGenerViewTable(2,genrenameEdit);
            await browser.sleep(3000);
            await browser.refresh();
            
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the display of local genres'", async () => {

        let stepAction = "Check the display of local genres'";
        let stepData = "";
        let stepExpResult = "if local admin add a genre - it is considered as global genre and it should be displayed in the  local not in  network programs.";
        let stepActualResult = "if local admin add a genre - it is considered as global genre and it should be displayed in the local not in  network programs.";

        try {
            await HomePage.appclickOnMenuListLeftPanel("Local Programming");
            await programTracksPage.clickComponentPdropdown("xgc-markets");
            await programTracksPage.selectOptionsFromComponentPdropdown("Pittsburgh");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-channels");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-dayparts");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await action.scrollToTopOfPage();
            await localProgPage.clickonButtonsRatecardPage("xgc-add-program-orbit-button","Add");
            await localProgPage.clickDropdownOptionInventory("Local Program");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("channel");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
           
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("daypart");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await genrePg.enterTextInXgTypeahead("xgc-select-genre",genrenameEdit);
            await genrePg.verifyOptionsFromComponentAutoComplete(genrenameEdit);
            await localProgPage.clickonButtonsRatecardPage("xgc-cancel-local-program","Cancel");

            await HomePage.appclickOnMenuListLeftPanel("Network Programming");
            await verify.verifyProgressBarNotPresent();
            await localProgPage.clickonButtonsRatecardPage("xgc-network-program-add", "Add");
            await localProgPage.clickDropdownOptionInventory("Network Program");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("xgc-dropdown-daypart");
            await networkProgram.selectAllOptionsFromMultiSelectDropDown();
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("xgc-dropdown-daypart");
            await genrePg.enterTextInXgTypeahead("xgc-typeahead-genre",genrenameEdit);
            await genrePg.verifyNoSuggetionInAutocompletewidget(genrenameEdit);
            await genrePg.clickCloseXbuttonPopUp();
            await HomePage.appclickOnMenuListLeftPanel("Reference");
            await HomePage.appclickOnMenuListLeftPanel("Genres");
            await HomePage.appclickOnMenuListLeftPanel("Local Genres");
            await verify.verifyProgressBarNotPresent();
            await genrePg.clickDataInGenerViewTable(1,genrenameEdit);
            await action.scrollToTopOfPage();
            await localProgPage.clickonButtonsRatecardPage("xgc-btn-delete-genre","Delete");
            await genrePg.clickButtonInConfirmationPopUp("Yes");
            await genrePg.verifyDataNotPresentInGenerViewTable(1,genrenameEdit);
           
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it(" Check the display of Network genres'", async () => {

        let stepAction = "Edit and Check the display of Network genres";
        let stepData = "";
        let stepExpResult = "if network admin add a genre -  it should be displayed in the network programs and local program.";
        let stepActualResult = "if network admin add a genre -  it should be displayed in network programs and local program.";

        try {
           
           
            await HomePage.appclickOnMenuListLeftPanel("Network Genres");
            await verify.verifyProgressBarNotPresent();
            await browser.sleep(1000);
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-dropdown-network");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await action.scrollToTopOfPage();
            await localProgPage.clickonButtonsRatecardPage("xgc-btn-add-genre","Add");
            await localProgPage.enterTextInInputInventoryAddProgramUsingLabel("Name",genrenameN);
            await localProgPage.enterTextInInputInventoryAddProgramUsingLabel("Description","Description Gener Network");
            await programTracksPage.selectCheckBox("Campaign");
            await browser.sleep(1000);
            await localProgPage.clickonButtonsRatecardPage("xgc-genre-edit","Create");
            await browser.sleep(1000);
            await localProgPage.GridValidations("Name",genrenameN);
            await genrePg.clickDataInGenerViewTable(3,genrenameN);
            await browser.sleep(1000);
            await localProgPage.enterTextInInputInventoryAddProgramUsingLabel("Name",genrenameNEdit);
            await localProgPage.clickonButtonsRatecardPage("xgc-genre-edit","Update");
            await localProgPage.GridValidations("Name",genrenameNEdit); 
            await HomePage.appclickOnMenuListLeftPanel("Network Programming");
            await verify.verifyProgressBarNotPresent();
            await localProgPage.clickonButtonsRatecardPage("xgc-network-program-add", "Add");
            await localProgPage.clickDropdownOptionInventory("Network Program");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("xgc-dropdown-daypart");
            await networkProgram.selectAllOptionsFromMultiSelectDropDown();
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("xgc-dropdown-daypart");
            await genrePg.enterTextInXgTypeahead("xgc-typeahead-genre",genrenameNEdit);
            await genrePg.verifyOptionsFromComponentAutoComplete(genrenameNEdit);
            await genrePg.clickCloseXbuttonPopUp();
            await HomePage.appclickOnMenuListLeftPanel("Local Programming");
            await programTracksPage.clickComponentPdropdown("xgc-markets");
            await programTracksPage.selectOptionsFromComponentPdropdown("Pittsburgh");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-channels");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-dayparts");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await action.scrollToTopOfPage();
            await localProgPage.clickonButtonsRatecardPage("xgc-add-program-orbit-button","Add");
            await localProgPage.clickDropdownOptionInventory("Local Program");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("channel");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
           
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("daypart");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await genrePg.enterTextInXgTypeahead("xgc-select-genre",genrenameNEdit);
            await genrePg.verifyOptionsFromComponentAutoComplete(genrenameNEdit);
            await localProgPage.clickonButtonsRatecardPage("xgc-cancel-local-program","Cancel");

            await HomePage.appclickOnMenuListLeftPanel("Reference");
            await HomePage.appclickOnMenuListLeftPanel("Genres");
            await HomePage.appclickOnMenuListLeftPanel("Network Genres");
            await verify.verifyProgressBarNotPresent();
            await genrePg.clickDataInGenerViewTable(1,genrenameNEdit);
            await action.scrollToTopOfPage();
            await localProgPage.clickonButtonsRatecardPage("xgc-btn-delete-genre","Delete");
            await genrePg.clickButtonInConfirmationPopUp("Yes");
            await genrePg.verifyDataNotPresentInGenerViewTable(1,genrenameNEdit);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
