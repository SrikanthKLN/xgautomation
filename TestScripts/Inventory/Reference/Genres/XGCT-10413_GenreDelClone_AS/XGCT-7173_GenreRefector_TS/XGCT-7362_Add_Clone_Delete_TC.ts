/*
***********************************************************************************************************
* @Script Name :  XGCT-7362
* @Description :  Genre- Regression test-cases3-9-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 3-9-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/


//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { NetworkProgramming } from '../../../../../../Pages/Inventory/NetworkProgramming';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { LocalProgramming } from '../../../../../../Pages/Inventory/LocalProgramming';
import { ProgramRatingTracks } from '../../../../../../Pages/Inventory/ProgramRatingTracks';
import { GenresPage } from '../../../../../../Pages/Inventory/Reference/Genres/Genres';





//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let globalFunc = new globalvalues();
let networkProgram = new NetworkProgramming();
let HomePage = new HomePageFunction();
let localProgPage = new LocalProgramming();
let programTracksPage = new ProgramRatingTracks();
let genrePg = new GenresPage();

let genrename = "AT_GeD" + new Date().getTime();
let genrenameCL = "AT_GLC" + new Date().getTime();
let genrenameN = "AT_GeND" + new Date().getTime();
let genrenameNC = "AT_GNC" + new Date().getTime();






//Variables Declaration
let TestCase_ID = 'XGCT-7362_Add_Clone_Delete_TC'
let TestCase_Title = 'Genre- Regression test-cases Add Clone Delete'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('GENRE- REGRESSION TEST-CASES ADD CLONE Delete', () => {


    // --------------Test Step------------
    it("Check if user is able to add the Genre to the application", async () => {

        let stepAction = "Check if user is able to add the Genre to the application";
        let stepData = "";
        let stepExpResult = "A Genre should be added successfully";
        let stepActualResult = "A Genre should be added successfully";

        try {
            await globalFunc.LaunchStoryBook();
            await HomePage.appclickOnNavigationList("Programming");
            await HomePage.appclickOnMenuListLeftPanel("Reference");
            await HomePage.appclickOnMenuListLeftPanel("Genres");
            await HomePage.appclickOnMenuListLeftPanel("Local Genres");
            await verify.verifyProgressBarNotPresent();
            await browser.sleep(1000);
            await programTracksPage.clickComponentPdropdown("xgc-dropdown-market");
            await programTracksPage.selectOptionsFromComponentPdropdown("Pittsburgh");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-channels");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await action.scrollToTopOfPage();
            await localProgPage.clickonButtonsRatecardPage("xgc-btn-add-genre", "Add");
            await localProgPage.enterTextInInputInventoryAddProgramUsingLabel("Name", genrename);
            await localProgPage.enterTextInInputInventoryAddProgramUsingLabel("Description", "Description Gener");
            await programTracksPage.selectCheckBox("Campaign");
            await browser.sleep(1000);
            await localProgPage.clickonButtonsRatecardPage("xgc-genre-edit", "Create");

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check by Cloning a Genre.", async () => {

        let stepAction = "Check by Cloning a Genre.";
        let stepData = genrename;
        let stepExpResult = "'System should ask to enter Genre title.";
        let stepActualResult = "'System should ask to enter Genre title.";

        try {

            await localProgPage.GridValidations("Name", genrename);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });
    // --------------Test Step------------
    it("Clone- selected Genre ", async () => {

        let stepAction = "Clone- selected Genre'";
        let stepData = "";
        let stepExpResult = "Genre Should be cloned  ";
        let stepActualResult = "Genre Should is cloned ";

        try {
            await genrePg.clickDataInGenerViewTable(1, genrename);
            await action.scrollToTopOfPage();
            await localProgPage.clickonButtonsRatecardPage("xgc-btn-clone-genre", "Clone");
            await genrePg.enterTextInPopUp(genrenameCL);
            await genrePg.clickButtonInClonePopUp("Clone");
           

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Delete- selected Genre ", async () => {

        let stepAction = "Delete- selected Genre'";
        let stepData = "";
        let stepExpResult = "Genre should get deleted from the list.";
        let stepActualResult = "Genre should get deleted from the list.";

        try {
            await genrePg.clickDataInGenerViewTable(1, genrename);
            await genrePg.clickDataInGenerViewTable(1, genrenameCL);
            await action.scrollToTopOfPage();
            await localProgPage.clickonButtonsRatecardPage("xgc-btn-delete-genre", "Delete");
            await genrePg.clickButtonInConfirmationPopUp("Yes");
            await genrePg.verifyDataNotPresentInGenerViewTable(1, genrename);
            await genrePg.verifyDataNotPresentInGenerViewTable(1, genrenameCL);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the display of local genres- deleted'", async () => {

        let stepAction = "Check the display of local genres- deleted'";
        let stepData = genrename;
        let stepExpResult = "deleted genres should not display in local program ";
        let stepActualResult = "deleted genres is not displayed in local program";

        try {

            await HomePage.appclickOnMenuListLeftPanel("Local Programming");
            await programTracksPage.clickComponentPdropdown("xgc-markets");
            await programTracksPage.selectOptionsFromComponentPdropdown("Pittsburgh");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-channels");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-dayparts");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await action.scrollToTopOfPage();
            await localProgPage.clickonButtonsRatecardPage("xgc-add-program-orbit-button", "Add");
            await localProgPage.clickDropdownOptionInventory("Local Program");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("channel");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("daypart");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await genrePg.enterTextInXgTypeahead("xgc-select-genre", genrenameCL);
            await genrePg.verifyNoSuggetionInAutocompletewidget(genrenameCL);
            await localProgPage.clickonButtonsRatecardPage("xgc-cancel-local-program", "Cancel");


            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });




    // --------------Test Step------------
    it(" Check the display of network genres- deleted", async () => {

        let stepAction = "Check the display of network genres- deleted'";
        let stepData = "";
        let stepExpResult = "deleted genres should not displayed in local program and network program";
        let stepActualResult = "deleted genres is not displayed in local program and network program";

        try {

            await HomePage.appclickOnMenuListLeftPanel("Reference");
            await HomePage.appclickOnMenuListLeftPanel("Genres");
            await HomePage.appclickOnMenuListLeftPanel("Network Genres");
            await verify.verifyProgressBarNotPresent();
            await browser.sleep(1000);
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-dropdown-network");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await action.scrollToTopOfPage();
            await localProgPage.clickonButtonsRatecardPage("xgc-btn-add-genre", "Add");
            await localProgPage.enterTextInInputInventoryAddProgramUsingLabel("Name", genrenameN);
            await localProgPage.enterTextInInputInventoryAddProgramUsingLabel("Description", "Description Gener Network");
            await browser.sleep(1000);
            await programTracksPage.selectCheckBox("Campaign");
            await browser.sleep(1000);
            await localProgPage.clickonButtonsRatecardPage("xgc-genre-edit", "Create");
            await browser.sleep(1000);
            await localProgPage.GridValidations("Name", genrenameN);

            await genrePg.clickDataInGenerViewTable(1, genrenameN);
            await action.scrollToTopOfPage();
            await localProgPage.clickonButtonsRatecardPage("xgc-btn-clone-genre", "Clone");
            await genrePg.enterTextInPopUp(genrenameNC);
            await genrePg.clickButtonInClonePopUp("Clone");
            await localProgPage.GridValidations("Name", genrenameNC);

            await genrePg.clickDataInGenerViewTable(1, genrenameN);
            await genrePg.clickDataInGenerViewTable(1, genrenameNC);
            await action.scrollToTopOfPage();
            await localProgPage.clickonButtonsRatecardPage("xgc-btn-delete-genre", "Delete");
            await genrePg.clickButtonInConfirmationPopUp("Yes");
            await genrePg.verifyDataNotPresentInGenerViewTable(1, genrenameN);
            await genrePg.verifyDataNotPresentInGenerViewTable(1, genrenameNC);

            await HomePage.appclickOnMenuListLeftPanel("Network Programming");
            await verify.verifyProgressBarNotPresent();
            await localProgPage.clickonButtonsRatecardPage("xgc-network-program-add", "Add");
            await localProgPage.clickDropdownOptionInventory("Network Program");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("xgc-dropdown-daypart");
            await networkProgram.selectAllOptionsFromMultiSelectDropDown();
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("xgc-dropdown-daypart");
            await genrePg.enterTextInXgTypeahead("xgc-typeahead-genre", genrenameNC);
            await genrePg.verifyNoSuggetionInAutocompletewidget(genrenameNC);
            await genrePg.clickCloseXbuttonPopUp();
            await HomePage.appclickOnMenuListLeftPanel("Local Programming");
            await programTracksPage.clickComponentPdropdown("xgc-markets");
            await programTracksPage.selectOptionsFromComponentPdropdown("Pittsburgh");
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-channels");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await programTracksPage.clickFieldWithComboboxMultiselect("xgc-dayparts");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await action.scrollToTopOfPage();
            await localProgPage.clickonButtonsRatecardPage("xgc-add-program-orbit-button", "Add");
            await localProgPage.clickDropdownOptionInventory("Local Program");
            await localProgPage.clickMultiselectDropDownInventoryAddProgram("channel");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();

            await localProgPage.clickMultiselectDropDownInventoryAddProgram("daypart");
            await programTracksPage.clickMultiSelectAllOptions();
            await programTracksPage.clickCloseButtonMultiSelectListDropdown();
            await genrePg.enterTextInXgTypeahead("xgc-select-genre", genrenameNC);
            await genrePg.verifyNoSuggetionInAutocompletewidget(genrenameNC);
            await localProgPage.clickonButtonsRatecardPage("xgc-cancel-local-program", "Cancel");


            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
