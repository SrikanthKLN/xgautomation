/*
***********************************************************************************************************
* @Script Name :  XGCT-8204
* @Description :  Inventory - PI-0 Refactor for Tags-Verify that user is able to Clone,Delete a  Tag4-9-2019
* @Page Object Name(s) : Tags
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib,globalvalues,HomePageFunction
* @Pre-Conditions : 
* @Creation Date : 4-9-2019
* @Author : Adithya K
* @Modified By & Date:06-09-2019
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../../TestData/globalTestData';
import { Tags } from '../../../../../../Pages/Inventory/Reference/Tags/Tags';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let home = new HomePageFunction();
let localLF = new LocalLengthFactorsPricing();
let tagPg = new Tags();

let testdata_TagName: string
let testdata_RetrievedTagName1
let testdata_RetrievedTagName2

//Variables Declaration
let TestCase_ID = 'XGCT-8204_CloneDeleteNetworkTag_TC'
let TestCase_Title = 'Inventory - PI-0 Refactor for Tags-Verify that user is able to Clone Delete a  Tag'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('INVENTORY - PI-0 REFACTOR FOR TAGS-VERIFY THAT USER IS ABLE TO CLONE,DELETE A  TAG', () => {

    // --------------Test Step------------
    it("Navigate to the chassis integration Web app URL.", async () => {

        let stepAction = "Navigate to the chassis integration Web app URL.";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "User should navigate to the xgcampaignwebapp.";
        let stepActualResult = "User should navigate to the xgcampaignwebapp.";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Programs>Inventory > Tag View", async () => {

        let stepAction = "Navigate to Programs>Inventory > Tag View";
        let stepData = "";
        let stepExpResult = "'''Tag Values'' page should be displayed with the grid.'";
        let stepActualResult = "'''Tag Values'' page should be displayed with the grid.'";

        try {
            await home.appclickOnNavigationList("Programming");
            await home.appclickOnMenuListLeftPanel("Reference");
            await home.appclickOnMenuListLeftPanel("Tags");
            await home.appclickOnMenuListLeftPanel("Network Tags");
            await verify.verifyProgressBarNotPresent()
            await localLF.verifyPageHeader('Tags');
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a record  and click on Clone button on top of the grid", async () => {

        let stepAction = "Select a record  and click on Clone button on top of the grid";
        let stepData = "";
        let stepExpResult = "'''Clone Tag'' pop up should open with Tag Title field to add a new title.Clone and Cancel buttons should be available.'";
        let stepActualResult = "'''Clone Tag'' pop up should open with Tag Title field to add a new title.Clone and Cancel buttons should be available.'";

        try {
            testdata_TagName = await localLF.makeAlphaNumericId(8);
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await action.Click(tagPg.add_Button, "add_Button")
            await localLF.verifyPopUpHeader(tagPg.addTagPopUpheaderText);
            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await action.Click(tagPg.create_Button, "create_Button")
            await localLF.verifyToastNotification(tagPg.tagAddedNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await action.MouseMoveToElement(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await action.Click(tagPg.clone_Button, "clone_Button")
            await localLF.verifyPopUpHeader(tagPg.cloneTagPopUpheaderText)
            await verify.verifyElement(tagPg.tagTilteClone_TextBox, "tagTilteClone_TextBox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter Tag Title and Click on Clone button from the Clone Tag popup.Check that the  Confirmation message is displayed and check a cloned record is added to the grid with the Title as added in the Clone Tag popup.", async () => {

        let stepAction = "Enter Tag Title and Click on Clone button from the Clone Tag popup.Check that the  Confirmation message is displayed and check a cloned record is added to the grid with the Title as added in the Clone Tag popup.";
        let stepData = "";
        let stepExpResult = "Confirmation message  should be displayed as 'Successfully cloned XXXXX(new Title)' and a cloned record should be added to the grid with the Title as added in the Clone Tag popup.";
        let stepActualResult = "Confirmation message  should be displayed as 'Successfully cloned XXXXX(new Title)' and a cloned record should be added to the grid with the Title as added in the Clone Tag popup.";

        try {
            testdata_TagName = await localLF.makeAlphaNumericId(8);
            await action.SetText(tagPg.tagTilteClone_TextBox, testdata_TagName, "tagTilteClone_TextBox")
            await action.Click(element.all(tagPg.clone_Button).get(1), "clone_Button")
            await localLF.verifyToastNotification(tagPg.tagClonedNotificationMsg + testdata_TagName)
            await verify.verifyProgressBarNotPresent()
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Do not select any Tag and click on Clone button on top of the grid.", async () => {

        let stepAction = "Do not select any Tag and click on Clone button on top of the grid.";
        let stepData = "";
        let stepExpResult = "'A message should display as ''Please select a Tag for clone'''";
        let stepActualResult = "'A message should display as ''Please select a Tag for clone'''";

        try {
            await action.Click(tagPg.clone_Button, "clone_Button")
            await localLF.verifyToastNotification(tagPg.cloneSelectOneErrorMsg)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select multiple records to clone", async () => {

        let stepAction = "Select multiple records to clone";
        let stepData = "";
        let stepExpResult = "Clone button should get disabled when trying to clone multiple records";
        let stepActualResult = "Clone button should get disabled when trying to clone multiple records";

        try {
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await action.Click(localLF.secondRecordCheckbox, "secondRecordCheckbox")
            await verify.verifyElementIsDisabled(tagPg.clone_Button, "clone_Button")
            await action.Click(localLF.secondRecordCheckbox, "secondRecordCheckbox")
            await verify.verifyElementIsEnabled(tagPg.clone_Button, "clone_Button")
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select a record, click on Clone button on top of the grid.Add the same title which was given in the previous Clone/existing Tag and click on Clone from the pop up.", async () => {

        let stepAction = "Select a record, click on Clone button on top of the grid.Add the same title which was given in the previous Clone/existing Tag and click on Clone from the pop up.";
        let stepData = "Comedy";
        let stepExpResult = "'A message should display saying ''ERROR:Tag with  name 'XXXX'already exists'''";
        let stepActualResult = "'A message should display saying ''ERROR:Tag with  name 'XXXX'already exists'''";

        try {
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await action.Click(tagPg.clone_Button, "clone_Button")
            await action.SetText(tagPg.tagTilteClone_TextBox, testdata_TagName, "tagTilteClone_TextBox")
            await action.Click(element.all(tagPg.clone_Button).get(1), "clone_Button")
            await console.log("Text To be verified " + tagPg.tagExistsNotificationMsg.replace("dynamic", testdata_TagName))
            await localLF.verifyToastNotification(tagPg.tagExistsNotificationMsg.replace("dynamic", testdata_TagName))
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Add unique Title and Click on Cancel from the Clone Tag popup.Check that the  1)The pop up is closed  2)The user should be in the same page/grid", async () => {

        let stepAction = "Add unique Title and Click on Cancel from the Clone Tag popup.Check that the  1)The pop up is closed  2)The user should be in the same page/grid";
        let stepData = "";
        let stepExpResult = "1)The pop up should be closed  2)The user should be in the same page/grid.";
        let stepActualResult = "1)The pop up should be closed  2)The user should be in the same page/grid.";

        try {
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await action.Click(tagPg.clone_Button, "clone_Button")
            testdata_TagName = await localLF.makeAlphaNumericId(8);
            await action.SetText(tagPg.tagTilteClone_TextBox, testdata_TagName, "tagTilteClone_TextBox")
            await action.Click(element.all(tagPg.clone_Button).get(1), "clone_Button")
            await localLF.verifyToastNotification(tagPg.tagClonedNotificationMsg + testdata_TagName)
            await verify.verifyProgressBarNotPresent()
            await verify.verifyNotPresent(tagPg.clonePopUp, "clonePopUp")
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select the Tag added(Campaign and Inventory) and select Delete button.", async () => {

        let stepAction = "Select the Tag added(Campaign and Inventory) and select Delete button.";
        let stepData = "";
        let stepExpResult = "'A confirmation message should be displayed ''Are you sure to delete this Tag'' with Yes and No buttons.'";
        let stepActualResult = "'A confirmation message should be displayed ''Are you sure to delete this Tag'' with Yes and No buttons.'";

        try {
            testdata_TagName = await localLF.makeAlphaNumericId(8);
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await action.Click(tagPg.add_Button, "add_Button")
            await localLF.verifyPopUpHeader(tagPg.addTagPopUpheaderText);
            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await localLF.verifyCheckboxesActions(["Campaign"], "Click", "Campaign")
            await localLF.selectAllInMultiselectDropdown(tagPg.byMultiSelectDropDown, "byMultiSelectDropDown")
            await action.Click(tagPg.create_Button, "create_Button")
            await localLF.verifyToastNotification(tagPg.tagAddedNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.parentColumnHeaderText, tagPg.parentType_Campaign, "Equal")
            await action.MouseMoveToElement(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await action.Click(tagPg.delete_Button, "delete_Button")
            await verify.verifyElementIsDisplayed(tagPg.deleteDialogBox, "deleteDialogBox")
            await verify.verifyTextOfElement(tagPg.deleteConfirmationMsg, tagPg.deleteConfirmationText, "deleteConfirmationText")
            await verify.verifyElementIsDisplayed(tagPg.yes_Button, "yes_Button")
            await verify.verifyElementIsDisplayed(tagPg.no_Button, "no_Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Yes from the confirmation message.", async () => {

        let stepAction = "Select Yes from the confirmation message.";
        let stepData = "";
        let stepExpResult = "'The selected Tag should be removed from the view.''Total 1 record(s) deleted successfully for both'' should be displayed.'";
        let stepActualResult = "'The selected Tag should be removed from the view.''Total 1 record(s) deleted successfully for both'' should be displayed.'";

        try {
            await action.Click(tagPg.yes_Button, "yes_Button")
            await localLF.verifyToastNotification(tagPg.deleteInvCampTagNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await localLF.verifyNodataFoundInGrid()
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select multiple records and select No from the confirmation message window.", async () => {

        let stepAction = "Select multiple records and select No from the confirmation message window.";
        let stepData = "";
        let stepExpResult = "The window should be closed and navigated back to the view.";
        let stepActualResult = "The window should be closed and navigated back to the view.";

        try {
            testdata_RetrievedTagName1 = String(await action.GetText(tagPg.latestTagName, "latestTagName"))
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            testdata_RetrievedTagName2 = String(await action.GetText(tagPg.secondTagName, "secondTagName"))
            await action.Click(localLF.secondRecordCheckbox, "secondRecordCheckbox")
            await action.Click(tagPg.delete_Button, "delete_Button")
            await action.Click(tagPg.no_Button, "no_Button")
            await verify.verifyNotPresent(tagPg.deleteDialogBox, "deleteDialogBox")
            await localLF.verifyPageHeader('Tags');
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Delete button for the selected multiple records( 1 Campaign and Inventory record,1 Inventory record)", async () => {

        let stepAction = "Select Delete button for the selected multiple records( 1 Campaign and Inventory record,1 Inventory record)";
        let stepData = "";
        let stepExpResult = "'A confirmation message should be displayed ''Are you sure to delete this Tag'' with Yes and No buttons.'";
        let stepActualResult = "'A confirmation message should be displayed ''Are you sure to delete this Tag'' with Yes and No buttons.'";

        try {
            await action.Click(tagPg.delete_Button, "delete_Button")
            await verify.verifyElementIsDisplayed(tagPg.deleteDialogBox, "deleteDialogBox")
            await verify.verifyTextOfElement(tagPg.deleteConfirmationMsg, tagPg.deleteConfirmationText, "deleteConfirmationText")
            await verify.verifyElementIsDisplayed(tagPg.yes_Button, "yes_Button")
            await verify.verifyElementIsDisplayed(tagPg.no_Button, "no_Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Yes from the confirmation message window", async () => {

        let stepAction = "Select Yes from the confirmation message window";
        let stepData = "";
        let stepExpResult = "'All the selected Tags should be removed from the view.''Total 1 record(s) deleted successfully for both and Total 1 record(s) deleted successfully for Inventory'' message should be displayed.'";
        let stepActualResult = "'All the selected Tags should be removed from the view.''Total 1 record(s) deleted successfully for both and Total 1 record(s) deleted successfully for Inventory'' message should be displayed.'";

        try {
            await action.Click(tagPg.yes_Button, "yes_Button")
            await localLF.verifyToastNotification(tagPg.tagDeletedNotificationMsg)
            await verify.verifyProgressSpinnerCircleNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_RetrievedTagName1)
            await localLF.verifyNodataFoundInGrid()
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_RetrievedTagName2)
            await localLF.verifyNodataFoundInGrid()
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("To delete a Inventory keyword Select the Inventory Tag from the view and click on Delete", async () => {

        let stepAction = "To delete a Inventory keyword Select the Inventory Tag from the view and click on Delete";
        let stepData = "";
        let stepExpResult = "'A confirmation message should display saying ''Are you sure to delete this Tag''with Yes and No buttons.'";
        let stepActualResult = "'A confirmation message should display saying ''Are you sure to delete this Tag''with Yes and No buttons.'";

        try {
            testdata_TagName = await localLF.makeAlphaNumericId(8);
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await action.Click(tagPg.add_Button, "add_Button")
            await localLF.verifyPopUpHeader(tagPg.addTagPopUpheaderText);
            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await action.Click(tagPg.create_Button, "create_Button")
            await localLF.verifyToastNotification(tagPg.tagAddedNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.parentColumnHeaderText, tagPg.parentType_Inventory, "Equal")
            await action.Click(localLF.latestRecordCheckbox, "latestRecordCheckbox")
            await action.Click(tagPg.delete_Button, "delete_Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Yes from the confirmation message", async () => {

        let stepAction = "Select Yes from the confirmation message";
        let stepData = "";
        let stepExpResult = "'A message should display saying ''Total 1 record(s) deleted successfully for Inventory''.The confirmation message window should close and the record is removed from the view'";
        let stepActualResult = "'A message should display saying ''Total 1 record(s) deleted successfully for Inventory''.The confirmation message window should close and the record is removed from the view'";

        try {
            await action.Click(tagPg.yes_Button, "yes_Button")
            await localLF.verifyToastNotification(tagPg.deleteInvTagNotificationMsg)
            await verify.verifyProgressSpinnerCircleNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await localLF.verifyNodataFoundInGrid()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
