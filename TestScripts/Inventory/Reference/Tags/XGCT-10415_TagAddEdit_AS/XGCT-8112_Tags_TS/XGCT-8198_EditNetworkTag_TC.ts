/*
***********************************************************************************************************
* @Script Name :  XGCT-8198
* @Description :  Inventory- PI-0 Refactor for Tags-Verify that user is able to Edit a Tag3-9-2019
* @Page Object Name(s) : Tags
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib,globalvalues,HomePageFunction
* @Pre-Conditions : 
* @Creation Date : 03-09-2019
* @Author : Adithya K
* @Modified By & Date:03-09-2019
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../../TestData/globalTestData';
import { Tags } from '../../../../../../Pages/Inventory/Reference/Tags/Tags';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let home = new HomePageFunction();
let localLF = new LocalLengthFactorsPricing();
let tagPg = new Tags();

let testdata_TagName: string
let testdata_TagDescription: string
let campaignsByOptions: Array<string> = ["Local", "National", "Network"]
let singleDaypart: Array<string> = ["Late News"]
let singleDaypartCode: string = "LN"

//Variables Declaration
let TestCase_ID = 'XGCT-8198'
let TestCase_Title = 'Inventory- PI-0 Refactor for Tags-Verify that user is able to Edit a Tag'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('INVENTORY- PI-0 REFACTOR FOR TAGS-VERIFY THAT USER IS ABLE TO EDIT A TAG', () => {


    // --------------Test Step------------
    it("Navigate to the chassis integration Web app URL.", async () => {

        let stepAction = "Navigate to the chassis integration Web app URL.";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "User should navigate to the xgcampaignwebapp.";
        let stepActualResult = "User should navigate to the xgcampaignwebapp.";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Programs>Inventory > Tag View", async () => {

        let stepAction = "Navigate to Programs>Inventory > Tag View";
        let stepData = "";
        let stepExpResult = "'''Tag Values'' page should be displayed with the grid.'";
        let stepActualResult = "'''Tag Values'' page should be displayed with the grid.'";

        try {
            await home.appclickOnNavigationList("Programming");
            await home.appclickOnMenuListLeftPanel("Reference");
            await home.appclickOnMenuListLeftPanel("Tags");
            await home.appclickOnMenuListLeftPanel("Network Tags");
            await verify.verifyProgressBarNotPresent()
            await localLF.verifyPageHeader('Tags');
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on the hyper link of the Tag name", async () => {

        let stepAction = "Click on the hyper link of the Tag name";
        let stepData = "";
        let stepExpResult = "Edit Tag window should open with all the given details.";
        let stepActualResult = "Edit Tag window should open with all the given details.";

        try {
            testdata_TagName = await localLF.makeAlphaNumericId(8);
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await action.Click(tagPg.add_Button, "add_Button")
            await browser.sleep(2000)
            await localLF.verifyPopUpHeader(tagPg.addTagPopUpheaderText);
            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await action.SetText(tagPg.description_TextBox,testdata_TagName, "description_TextBox")
            await action.Click(tagPg.create_Button, "create_Button")
            await localLF.verifyToastNotification(tagPg.tagAddedNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await tagPg.clickOnTagEditLink(testdata_TagName)
            await localLF.verifyPopUpHeader(tagPg.editTagPopUpheaderText);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check that all the details are present for the Tag.", async () => {

        let stepAction = "Check that all the details are present for the Tag.";
        let stepData = "";
        let stepExpResult = "User should be able to see all the details of a given Tag and able to edit.";
        let stepActualResult = "User should be able to see all the details of a given Tag and able to edit.";

        try {
            await verify.verifyElementAttribute(tagPg.name_TextBox,"value", testdata_TagName, "name_TextBox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Edit the Name of the Tag(alphabets/numbers/Special characters).Check user is able to edit.", async () => {

        let stepAction = "Edit the Name of the Tag(alphabets/numbers/Special characters).Check user is able to edit.";
        let stepData = "New name Tag/30/!@#%&*()";
        let stepExpResult = "User should be able to edit the updated name.";
        let stepActualResult = "User should be able to edit the updated name.";

        try {
            testdata_TagName= "New name Tag/30/!@#&*()"
            await action.ClearText(tagPg.name_TextBox, "name_TextBox")
            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await verify.verifyElementAttribute(tagPg.name_TextBox,"value", testdata_TagName, "name_TextBox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter more than 25 characters in the Name field.Check the user is not able to enter.", async () => {

        let stepAction = "Enter more than 25 characters in the Name field.Check the user is not able to enter.";
        let stepData = "";
        let stepExpResult = "User should not be able to enter more than 25 characters in the Name field.";
        let stepActualResult = "User should not be able to enter more than 25 characters in the Name field.";

        try {
            testdata_TagName= "TagNameValidationWith25Ch"
            let testdata_TagName1= "TagNameValidationWith25Cha"
            await action.ClearText(tagPg.name_TextBox, "name_TextBox")
            await action.SetText(tagPg.name_TextBox, testdata_TagName1, "name_TextBox")
            await verify.verifyElementAttribute(tagPg.name_TextBox,"value", testdata_TagName, "name_TextBox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Edit the Description((alphabets/numbers/special characters) of Tag.Check the user is able to edit.", async () => {

        let stepAction = "Edit the Description((alphabets/numbers/special characters) of Tag.Check the user is able to edit.";
        let stepData = "New Description/45/!@#%&";
        let stepExpResult = "User should be able to edit the updated Description.";
        let stepActualResult = "User should be able to edit the updated Description.";

        try {
            testdata_TagName = await localLF.makeAlphaNumericId(8);
            testdata_TagDescription ="This Description/35/!@#$%"
            await action.ClearText(tagPg.name_TextBox, "name_TextBox")
            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await action.ClearText(tagPg.description_TextBox, "description_TextBox")
            await action.SetText(tagPg.description_TextBox, testdata_TagDescription, "description_TextBox")
            await verify.verifyElementAttribute(tagPg.description_TextBox,"value", testdata_TagDescription, "description_TextBox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Inactive radio button for the active Tag.", async () => {

        let stepAction = "Select Inactive radio button for the active Tag.";
        let stepData = "";
        let stepExpResult = "The Tag should be inactive now and the changes should be saved.";
        let stepActualResult = "The Tag should be inactive now and the changes should be saved.";

        try {
            await verify.verifyElementAttribute(tagPg.activeInactiveToggle,tagPg.toggleAttribute,"true","activeInactiveToggle")
            await action.Click(tagPg.activeInactiveToggle,"Toggle")
            await verify.verifyElementAttribute(tagPg.activeInactiveToggle,tagPg.toggleAttribute,"false","activeInactiveToggle")
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropdownPopUp, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await browser.sleep(1000)
            await action.Click(tagPg.update_Button, "update_Button")
            await localLF.verifyToastNotification(tagPg.tagUpdatedNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await verify.verifyProgressBarNotPresent()
            await verify.verifyElementAttribute(tagPg.toggleSwitch,tagPg.toggleAttribute,"false","Toggle")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Select Active radio button for the Inactive Tag.", async () => {

        let stepAction = "Select Active radio button for the Inactive Tag.";
        let stepData = "";
        let stepExpResult = "The Tag should be active now and the changes should be saved.";
        let stepActualResult = "The Tag should be active now and the changes should be saved.";

        try {
            await tagPg.clickOnTagEditLink(testdata_TagName);
            await browser.sleep(1000)
            await verify.verifyElementAttribute(tagPg.activeInactiveToggle,tagPg.toggleAttribute,"false","activeInactiveToggle")
            await action.Click(tagPg.activeInactiveToggle,"Toggle")
            await verify.verifyElementAttribute(tagPg.activeInactiveToggle,tagPg.toggleAttribute,"true","activeInactiveToggle")
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropdownPopUp, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await browser.sleep(1000)
            await action.Click(tagPg.update_Button, "update_Button")
            await localLF.verifyToastNotification(tagPg.tagUpdatedNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await verify.verifyProgressBarNotPresent()
            await verify.verifyElementAttribute(tagPg.toggleSwitch,tagPg.toggleAttribute,"true","Toggle")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Edit the Inventory,Campaign information in the Edit Tag and click on Update.Check Tag is updated.", async () => {

        let stepAction = "Edit the Inventory,Campaign information in the Edit Tag and click on Update.Check Tag is updated.";
        let stepData = "";
        let stepExpResult = "'User should be able to edit and save the updates for the Tag.''Successfully updated Tag'' message should be displayed.'";
        let stepActualResult = "'User should be able to edit and save the updates for the Tag.''Successfully updated Tag'' message should be displayed.'";

        try {
            await tagPg.clickOnTagEditLink(testdata_TagName);
            await browser.sleep(1000)
            await localLF.verifyCheckboxesActions(["Campaign"], "Click", "Campaign")
            await localLF.selectMultiSelectDropDownOptions(tagPg.byMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,[campaignsByOptions[0]], "byMultiSelectDropDown")
            await localLF.selectMultiSelectDropDownOptions(tagPg.dayPartsMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, singleDaypart, "Dayparts")
            await browser.sleep(1000)
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropdownPopUp, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await browser.sleep(1000)
            await action.Click(tagPg.update_Button, "update_Button")
            await localLF.verifyToastNotification(tagPg.tagUpdatedNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check for the updated information in the Tag view.", async () => {

        let stepAction = "Check for the updated information in the Tag view.";
        let stepData = "";
        let stepExpResult = "User should be able to see the updated and saved information in the view.";
        let stepActualResult = "User should be able to see the updated and saved information in the view.";

        try {
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.dayPartsColumnHeaderText, singleDaypartCode, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.parentColumnHeaderText, tagPg.parentType_Campaign, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.campaignsColumnHeaderText,campaignsByOptions[0], "Equal")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click on the hyper link of the Tag name.Edit all the required/mandatory details and click on Cancel in Edit Tag window.Check the Cancel operation is working.", async () => {

        let stepAction = "Click on the hyper link of the Tag name.Edit all the required/mandatory details and click on Cancel in Edit Tag window.Check the Cancel operation is working.";
        let stepData = "";
        let stepExpResult = "The edited details are not updated and the Edit Tag window should be closed.";
        let stepActualResult = "The edited details are not updated and the Edit Tag window should be closed.";

        try {
            let testdata_TagName1= "TagNameValidationWith25Cha"
            await tagPg.clickOnTagEditLink(testdata_TagName);
            await browser.sleep(1000)
            await action.ClearText(tagPg.name_TextBox, "name_TextBox")
            await action.SetText(tagPg.name_TextBox, testdata_TagName1, "name_TextBox")
            await localLF.selectAllInMultiselectDropdown(tagPg.byMultiSelectDropDown, "byMultiSelectDropDown")
            await localLF.selectAllInMultiselectDropdown(tagPg.dayPartsMultiSelectDropDown,"Dayparts")
            await browser.sleep(1000)
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropdownPopUp, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await browser.sleep(1000)
            await action.Click(tagPg.cancel_Button, "cancel_Button")
            await browser.sleep(500)
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.dayPartsColumnHeaderText, singleDaypartCode, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.parentColumnHeaderText, tagPg.parentType_Campaign, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.campaignsColumnHeaderText,campaignsByOptions[0], "Equal")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
