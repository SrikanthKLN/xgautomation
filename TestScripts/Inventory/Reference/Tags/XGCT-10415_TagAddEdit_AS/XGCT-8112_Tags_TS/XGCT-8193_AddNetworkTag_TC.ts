/*
***********************************************************************************************************
* @Script Name :  XGCT-8193
* @Description :  Inventory- PI-0 Refactor for Tags-Verify that user is able to Add a Tag in Tag View29-8-2019
* @Page Object Name(s) : Tags
* @Dependencies/Libs : Reporter,VerifyLib,ActionLib,globalvalues,HomePageFunction
* @Pre-Conditions : 
* @Creation Date : 29-8-2019
* @Author : Adithya K
* @Modified By & Date:03-09-2019
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { LocalLengthFactorsPricing } from '../../../../../../Pages/Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../../../TestData/globalTestData';
import { Tags } from '../../../../../../Pages/Inventory/Reference/Tags/Tags';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let global = new globalvalues();
let home = new HomePageFunction();
let localLF = new LocalLengthFactorsPricing();
let tagPg = new Tags();

let testdata_TagName: string
let testdata_TagDescription: string
let campaignsByOptions:Array<string>=["Local", "National", "Network"]
let salesRangeAll: string ="Local,National,Network"
let dayPartCodes:string = "EM,DT,EF,EN,PA,PT,LN,LF,WK,ON,ST,SP"
let singleDaypart:Array<string> =["Late News"]
let singleDaypartCode : string ="LN"

//Variables Declaration
let TestCase_ID = 'XGCT-8193'
let TestCase_Title = 'Inventory- PI-0 Refactor for Tags-Verify that user is able to Add a Tag in Tag View'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('INVENTORY- PI-0 REFACTOR FOR TAGS-VERIFY THAT USER IS ABLE TO ADD A TAG IN TAG VIEW', () => {


    // --------------Test Step------------
    it("Navigate to the chassis integration Web app URL.", async () => {

        let stepAction = "Navigate to the chassis integration Web app URL.";
        let stepData = "http://xgcampaignwebapp.azurewebsites.net/";
        let stepExpResult = "User should navigate to the Network Program view.";
        let stepActualResult = "User should navigate to the Network Program view.";

        try {
            await global.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Programs>Inventory > Tag View", async () => {

        let stepAction = "Navigate to Programs>Inventory > Tag View";
        let stepData = "";
        let stepExpResult = "'''Tag Values'' page should be displayed with the grid.'";
        let stepActualResult = "'''Tag Values'' page should be displayed with the grid.'";

        try {
            await home.appclickOnNavigationList("Programming");
            await home.appclickOnMenuListLeftPanel("Reference");
            await home.appclickOnMenuListLeftPanel("Tags");
            await home.appclickOnMenuListLeftPanel("Network Tags");
            await verify.verifyProgressBarNotPresent()
            await localLF.verifyPageHeader('Tags');
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click Add,select Add Tag.", async () => {

        let stepAction = "Click Add,select Add Tag.";
        let stepData = "";
        let stepExpResult = "'''Add Tag'' should be displayed in render section and Create button should be disabled.'";
        let stepActualResult = "'''Add Tag'' should be displayed in render section and Create button should be disabled.'";

        try {
            testdata_TagName = await localLF.makeAlphaNumericId(8);
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await action.Click(tagPg.add_Button, "add_Button")
            await browser.sleep(2000)
            await localLF.verifyPopUpHeader(tagPg.addTagPopUpheaderText);
            await verify.verifyElementIsDisabled(tagPg.create_Button, "create_Button")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter all the required/mandatory details and click on Create in Add Tag window.", async () => {

        let stepAction = "Enter all the required/mandatory details and click on Create in Add Tag window.";
        let stepData = "";
        let stepExpResult = "'User should be able to save the details and the Tag is added in the view.''Successfully added'' message should be displayed.'";
        let stepActualResult = "'User should be able to save the details and the Tag is added in the view.''Successfully added'' message should be displayed.'";

        try {

            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await action.Click(tagPg.create_Button, "create_Button")
            await localLF.verifyToastNotification(tagPg.tagAddedNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click Add,select Add Tag.Enter all the required/mandatory details and click on Cancel in Add Tag window.Check the Cancel operation is working.", async () => {

        let stepAction = "Click Add,select Add Tag.Enter all the required/mandatory details and click on Cancel in Add Tag window.Check the Cancel operation is working.";
        let stepData = "";
        let stepExpResult = "The entered details are not saved and the Keyword Tag is not added in the view.";
        let stepActualResult = "The entered details are not saved and the Keyword Tag is not added in the view.";

        try {
            testdata_TagName = await localLF.makeAlphaNumericId(8);
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await action.Click(tagPg.add_Button, "add_Button")
            // await browser.sleep(2000)
            // await localLF.verifyPopUpHeader(tagPg.addTagPopUpheaderText);
            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await action.Click(tagPg.cancel_Button, "cancel_Button")
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await localLF.verifyNodataFoundInGrid()
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click Add,select Add Tag in Tag view.In the Add Tag window select Campaign check box,enter all the required/mandatory details and click on Create button.Check Tag is created.", async () => {

        let stepAction = "Click Add,select Add Tag in Tag view.In the Add Tag window select Campaign check box,enter all the required/mandatory details and click on Create button.Check Tag is created.";
        let stepData = "";
        let stepExpResult = "'User is able to create a  Tag to a Campaign.''Successfully added'' message should be displayed.'";
        let stepActualResult = "'User is able to create a  Tag to a Campaign.''Successfully added'' message should be displayed.'";

        try {
            testdata_TagName = await localLF.makeAlphaNumericId(8);
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await action.Click(tagPg.add_Button, "add_Button")
            // await localLF.verifyPopUpHeader(tagPg.addTagPopUpheaderText);
            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await localLF.verifyCheckboxesActions(["Campaign"], "Click", "Campaign")
            await localLF.selectAllInMultiselectDropdown(tagPg.byMultiSelectDropDown, "byMultiSelectDropDown")
            await action.Click(tagPg.create_Button, "create_Button")
            await localLF.verifyToastNotification(tagPg.tagAddedNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.parentColumnHeaderText, tagPg.parentType_Campaign, "Equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click Add,select Add Tag.In the Add Tag window enter 25 Characters in the Name field.", async () => {

        let stepAction = "Click Add,select Add Tag.In the Add Tag window enter 25 Characters in the Name field.";
        let stepData = "";
        let stepExpResult = "User should be able to enter 25 characters.";
        let stepActualResult = "User should be able to enter 25 characters.";

        try {
            testdata_TagName= "TagNameValidationWith25Ch"
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await action.Click(tagPg.add_Button, "add_Button")
            // await localLF.verifyPopUpHeader(tagPg.addTagPopUpheaderText);
            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await verify.verifyElementAttribute(tagPg.name_TextBox,"value", testdata_TagName, "name_TextBox")
            await verify.verifyElementAttribute(tagPg.name_TextBox,"maxlength", "25", "name_TextBox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter 26 Characters in the Name field.", async () => {

        let stepAction = "Enter 26 Characters in the Name field.";
        let stepData = "";
        let stepExpResult = "User should not be able to enter the 26th character.";
        let stepActualResult = "User should not be able to enter the 26th character.";

        try {
            let testdata_TagName1= "TagNameValidationWith25Cha"
            await action.ClearText(tagPg.name_TextBox, "name_TextBox")
            await action.SetText(tagPg.name_TextBox, testdata_TagName1, "name_TextBox")
            await verify.verifyElementAttribute(tagPg.name_TextBox,"value", testdata_TagName, "name_TextBox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter  alphabets/numbers/special characters in the Name field.", async () => {

        let stepAction = "Enter  alphabets/numbers/special characters in the Name field.";
        let stepData = "New Keyword Tag/20/!@#";
        let stepExpResult = "User should be able to enter alphabets/numbers/Special characters.";
        let stepActualResult = "User should be able to enter alphabets/numbers/Special characters.";

        try {
            testdata_TagName ="New Keyword Tag/20/!@#$%"
            await action.ClearText(tagPg.name_TextBox, "name_TextBox")
            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await verify.verifyElementAttribute(tagPg.name_TextBox,"value", testdata_TagName, "name_TextBox")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter alphabets/numbers/special characters in the Description field.select Inventory check box,enter all the required/mandatory details and click on Create button.Check Tag is created.", async () => {

        let stepAction = "Enter alphabets/numbers/special characters in the Description field.select Inventory check box,enter all the required/mandatory details and click on Create button.Check Tag is created.";
        let stepData = "This Description/35/!@#";
        let stepExpResult = "'User should be able to enter alphabets/numbers/Special characters.''Successfully added'' message should be displayed.Tag should created to Inventory.'";
        let stepActualResult = "'User should be able to enter alphabets/numbers/Special characters.''Successfully added'' message should be displayed.Tag should created to Inventory.'";

        try {
            testdata_TagName = await localLF.makeAlphaNumericId(8);
            testdata_TagDescription ="This Description/35/!@#$%"
            await action.ClearText(tagPg.name_TextBox, "name_TextBox")
            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await action.ClearText(tagPg.description_TextBox, "description_TextBox")
            await action.SetText(tagPg.description_TextBox, testdata_TagDescription, "description_TextBox")
            await verify.verifyElementAttribute(tagPg.description_TextBox,"value", testdata_TagDescription, "description_TextBox")
            await action.Click(tagPg.create_Button, "create_Button")
            await localLF.verifyToastNotification(tagPg.tagAddedNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.parentColumnHeaderText, tagPg.parentType_Inventory, "Equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click Add,select Add Tag.Select the Campaign checkbox.Select the By as Local.", async () => {

        let stepAction = "Click Add,select Add Tag.Select the Campaign checkbox.Select the By as Local.";
        let stepData = "";
        let stepExpResult = "User should be able to designate Local as sales range.";
        let stepActualResult = "User should be able to designate Local as sales range.";

        try {
            testdata_TagName = await localLF.makeAlphaNumericId(8);
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await action.Click(tagPg.add_Button, "add_Button")
            // await localLF.verifyPopUpHeader(tagPg.addTagPopUpheaderText);
            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await localLF.verifyCheckboxesActions(["Campaign"], "Click", "Campaign")
            await localLF.selectMultiSelectDropDownOptions(tagPg.byMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,[campaignsByOptions[0]], "byMultiSelectDropDown")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter all the required/mandatory details and click on Create in Add Tag window.Check Local sales range is created.", async () => {

        let stepAction = "Enter all the required/mandatory details and click on Create in Add Tag window.Check Local sales range is created.";
        let stepData = "";
        let stepExpResult = "'A Tag is created and designated as Local sales range.''Successfully added'' message should be displayed.'";
        let stepActualResult = "'A Tag is created and designated as Local sales range.''Successfully added'' message should be displayed.'";

        try {
            await action.Click(tagPg.create_Button, "create_Button")
            await localLF.verifyToastNotification(tagPg.tagAddedNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.parentColumnHeaderText, tagPg.parentType_Campaign, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.campaignsColumnHeaderText,campaignsByOptions[0], "Equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click Add,select Add Tag.Select the Campaign checkbox,select the By as National.Enter all the required/mandatory details and click on Create in Add Tag window.", async () => {

        let stepAction = "Click Add,select Add Tag.Select the Campaign checkbox,select the By as National.Enter all the required/mandatory details and click on Create in Add Tag window.";
        let stepData = "";
        let stepExpResult = "'A Tag should be created and designated as National sales range.''Successfully added'' message should be displayed.'";
        let stepActualResult = "'A Tag should be created and designated as National sales range.''Successfully added'' message should be displayed.'";

        try {
            testdata_TagName = await localLF.makeAlphaNumericId(8);
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await action.Click(tagPg.add_Button, "add_Button")
            // await localLF.verifyPopUpHeader(tagPg.addTagPopUpheaderText);
            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await localLF.verifyCheckboxesActions(["Campaign"], "Click", "Campaign")
            await localLF.selectMultiSelectDropDownOptions(tagPg.byMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,[campaignsByOptions[1]], "byMultiSelectDropDown")
            await action.Click(tagPg.create_Button, "create_Button")
            await localLF.verifyToastNotification(tagPg.tagAddedNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.parentColumnHeaderText, tagPg.parentType_Campaign, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.campaignsColumnHeaderText, campaignsByOptions[1], "Equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click Add,select Add Tag.Select the By as Network.Enter all the required/mandatory details and click on save in Add Keyword Tag window.", async () => {

        let stepAction = "Click Add,select Add Tag.Select the By as Network.Enter all the required/mandatory details and click on save in Add Keyword Tag window.";
        let stepData = "";
        let stepExpResult = "'A Tag is created and designated as Network sales range and accessible for network users.''Successfully added'' message should be displayed.'";
        let stepActualResult = "'A Tag is created and designated as Network sales range and accessible for network users.''Successfully added'' message should be displayed.'";

        try {
            testdata_TagName = await localLF.makeAlphaNumericId(8);
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await action.Click(tagPg.add_Button, "add_Button")
            // await localLF.verifyPopUpHeader(tagPg.addTagPopUpheaderText);
            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await localLF.verifyCheckboxesActions(["Campaign"], "Click", "Campaign")
            await localLF.selectMultiSelectDropDownOptions(tagPg.byMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,[campaignsByOptions[2]], "byMultiSelectDropDown")
            await action.Click(tagPg.create_Button, "create_Button")
            await localLF.verifyToastNotification(tagPg.tagAddedNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.parentColumnHeaderText, tagPg.parentType_Campaign, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.campaignsColumnHeaderText, campaignsByOptions[2], "Equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click Add,select Add Tag.Select the By as Local,National and Network.Enter all the required/mandatory details and click on Create in Add Tag window.Check Tag is created.", async () => {

        let stepAction = "Click Add,select Add Tag.Select the By as Local,National and Network.Enter all the required/mandatory details and click on Create in Add Tag window.Check Tag is created.";
        let stepData = "";
        let stepExpResult = "'User should be designated for the sales range of Local,National and Network.A Tag is created ,designated and  accessible for all sales range.''Successfully added'' message should be displayed.'";
        let stepActualResult = "'User should be designated for the sales range of Local,National and Network.A Tag is created ,designated and  accessible for all sales range.''Successfully added'' message should be displayed.'";

        try {
            testdata_TagName = await localLF.makeAlphaNumericId(8);
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await action.Click(tagPg.add_Button, "add_Button")
            // await localLF.verifyPopUpHeader(tagPg.addTagPopUpheaderText);
            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await localLF.verifyCheckboxesActions(["Campaign"], "Click", "Campaign")
            await localLF.selectMultiSelectDropDownOptions(tagPg.byMultiSelectDropDown,localLF.multiSelectDropdownOptionsLocator,campaignsByOptions, "byMultiSelectDropDown")
            await action.Click(tagPg.create_Button, "create_Button")
            await localLF.verifyToastNotification(tagPg.tagAddedNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.parentColumnHeaderText, tagPg.parentType_Campaign, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.campaignsColumnHeaderText, salesRangeAll, "Equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click Add,select Add Tag.Select the all Dayparts.", async () => {

        let stepAction = "Click Add,select Add Tag.Select the all Dayparts.";
        let stepData = "";
        let stepExpResult = "'User should be able to designate All Dayparts as inventory range.''12 items selected'' should be displayed in the Dayparts field.'";
        let stepActualResult = "'User should be able to designate All Dayparts as inventory range.''12 items selected'' should be displayed in the Dayparts field.'";

        try {
            testdata_TagName = await localLF.makeAlphaNumericId(8);
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await action.Click(tagPg.add_Button, "add_Button")
            // await localLF.verifyPopUpHeader(tagPg.addTagPopUpheaderText);
            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await localLF.selectAllInMultiselectDropdown(tagPg.dayPartsMultiSelectDropDown,"Dayparts")
            let dayPartsAdded:string = String(await action.GetText(tagPg.dayPartsMultiSelectDropDown,"Dayparts"))
            if(dayPartsAdded.includes("12 items selected")||dayPartsAdded.includes("items selected")){
                action.ReportSubStep("functionName"," All Dropdown Options selected", "Pass");
             }
             else {
                action.ReportSubStep("functionName"," All Dropdown Options not selected", "Fail");
             }
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Enter all the required/mandatory details and click on Create in Add Tag window.Check the Tag is created.", async () => {

        let stepAction = "Enter all the required/mandatory details and click on Create in Add Tag window.Check the Tag is created.";
        let stepData = "";
        let stepExpResult = "'A Tag is created and designated for all the Dayparts.''Successfully added'' message should be displayed.'";
        let stepActualResult = "'A Tag is created and designated for all the Dayparts.''Successfully added'' message should be displayed.'";

        try {
            await action.Click(tagPg.create_Button, "create_Button")
            await localLF.verifyToastNotification(tagPg.tagAddedNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.dayPartsColumnHeaderText, dayPartCodes, "Equal")
            await action.Click(localLF.clearAllColumTextFilter, "clearAllColumTextFilter")
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Click Add,select Add Tag.Select the Dayparts range as Late news,enter all the required/mandatory details and click on Create in Add Tag window.Check the Tag is created.", async () => {

        let stepAction = "Click Add,select Add Tag.Select the Dayparts range as Late news,enter all the required/mandatory details and click on Create in Add Tag window.Check the Tag is created.";
        let stepData = "";
        let stepExpResult = "'A Tag is created and designated for Late News Daypart.''Successfully added'' message should be displayed.'";
        let stepActualResult = "'A Tag is created and designated for Late News Daypart.''Successfully added'' message should be displayed.'";

        try {
            testdata_TagName = await localLF.makeAlphaNumericId(8);
            await localLF.selectMultiSelectDropDownOptions(tagPg.networkMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, [globalTestData.networkName[0]], "Network")
            await verify.verifyProgressBarNotPresent()
            await action.Click(tagPg.add_Button, "add_Button")
            // await localLF.verifyPopUpHeader(tagPg.addTagPopUpheaderText);
            await action.SetText(tagPg.name_TextBox, testdata_TagName, "name_TextBox")
            await localLF.selectMultiSelectDropDownOptions(tagPg.dayPartsMultiSelectDropDown, localLF.multiSelectDropdownOptionsLocator, singleDaypart, "Dayparts")
            await browser.sleep(1000)
            await action.Click(tagPg.create_Button, "create_Button")
            await localLF.verifyToastNotification(tagPg.tagAddedNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            await tagPg.gridWithStatusColumnFilter(tagPg.nameColumnHeaderText, testdata_TagName)
            await tagPg.gridWithStatusColumnDataValidation(tagPg.nameColumnHeaderText, testdata_TagName, "Equal")
            await tagPg.gridWithStatusColumnDataValidation(tagPg.dayPartsColumnHeaderText, singleDaypartCode, "Equal")
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Update Tag status(active to inactive) in Tag Values window.", async () => {

        let stepAction = "Update Tag status(active to inactive) in Tag Values window.";
        let stepData = "";
        let stepExpResult = "'''Tag status updated successfully'' message should be displayed.'";
        let stepActualResult = "'''Tag status updated successfully'' message should be displayed.'";

        try {
            await verify.verifyElementAttribute(tagPg.toggleSwitch,tagPg.toggleAttribute,"true","Toggle")
            await action.Click(tagPg.toggleSwitch,"Toggle")
            await verify.verifyElementAttribute(tagPg.toggleSwitch,tagPg.toggleAttribute,"false","Toggle")
            await localLF.verifyToastNotification(tagPg.tagStausNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Update Tag status(Inactive to active) in Tag Values window.", async () => {

        let stepAction = "Update Tag status(Inactive to active) in Tag Values window.";
        let stepData = "";
        let stepExpResult = "'''Tag status updated successfully'' message should be displayed.'";
        let stepActualResult = "'''Tag status updated successfully'' message should be displayed.'";

        try {
            await verify.verifyElementAttribute(tagPg.toggleSwitch,tagPg.toggleAttribute,"false","Toggle")
            await action.Click(tagPg.toggleSwitch,"Toggle")
            await verify.verifyElementAttribute(tagPg.toggleSwitch,tagPg.toggleAttribute,"true","Toggle")
            await localLF.verifyToastNotification(tagPg.tagStausNotificationMsg)
            await verify.verifyProgressBarNotPresent()
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
