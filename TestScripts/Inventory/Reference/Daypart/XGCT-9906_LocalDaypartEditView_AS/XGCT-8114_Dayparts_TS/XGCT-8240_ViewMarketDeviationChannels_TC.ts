/*
***********************************************************************************************************
* @Script Name :  XGCT-8240
* @Description :  xG Campaign - PI-0 Refactor for Dayparts - Verify the functionality of Daypart View - Market, Deviation Channels.26-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 26-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { AppCommonFunctions } from '../../../../../../Libs/ApplicationLibs/CommAppLib';
import { globalTestData } from '../../../../../../TestData/globalTestData';
import { LocalDayparts } from '../../../../../../Pages/Inventory/Reference/Dayparts/LocalDayparts';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let homePage = new HomePageFunction();
let glogalFunc = new globalvalues();
let commonLib = new AppCommonFunctions();
let localDayparts = new LocalDayparts();
// let localDayparts = new dayPartView();
//Variables Declaration
let TestCase_ID = 'XGCT-8240'
let TestCase_Title = 'xG Campaign - PI-0 Refactor for Dayparts - Verify the functionality of Daypart View - Market Deviation Channels'
//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - PI-0 REFACTOR FOR DAYPARTS - VERIFY THE FUNCTIONALITY OF DAYPART VIEW - MARKET, DEVIATION CHANNELS.', () => {

    // --------------Test Step------------
    it("Open browser and navigate to URL : http://xgcampaignwebapp.azurewebsites.net/", async () => {

        let stepAction = "Open browser and navigate to URL : http://xgcampaignwebapp.azurewebsites.net/";
        let stepData = "";
        let stepExpResult = "Campaign Web Application page should be launched.";
        let stepActualResult = "Campaign Web Application page should be launched.";
        try {
            await glogalFunc.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Navigate to Programs-->Inventory -->Daypart View.", async () => {

        let stepAction = "Navigate to Programs-->Inventory -->Daypart View.";
        let stepData = "";
        let stepExpResult = "Daypart View page should be displayed.";
        let stepActualResult = "Daypart View page is displayed.";
        try {
            await homePage.appclickOnNavigationList(globalTestData.programmingTabName);
            await homePage.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await homePage.appclickOnMenuListLeftPanel(globalTestData.leftMenuDayPartName);
            await homePage.appclickOnMenuListLeftPanel(globalTestData.leftMenuLocalDayPartsName);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Check if user can view all the available Markets displayed in markets drop down list.", async () => {

        let stepAction = "Check if user can view all the available Markets displayed in markets drop down list.";
        let stepData = "";
        let stepExpResult = "All the available Markets should be available in Market drop down list.";
        let stepActualResult = "All the available Markets are available in Market drop down list.";
        let expectedDropDownItems = ["Select","Pittsburgh", "Cincinnati", "Albany, NY", "Mobile-Pensacola (Ft Walt)", "El Paso (Las Cruces)"];
        let failedCount = 0;
        let errorMsg = "";
        try {
            let dropDownItems:Array<any>;
            await verify.verifyElementIsDisplayed(localDayparts.buttonReassignDayPart,"Verify Reassign Daypart button");
            dropDownItems = await commonLib.getDropDownItems("Market");
           for(let index = 0 ; index < expectedDropDownItems.length ; index ++)
           {
            for(let jIndex = 0 ; jIndex < dropDownItems.length ; jIndex ++)
            {
                await console.log(dropDownItems[jIndex])
                if(expectedDropDownItems[index] != dropDownItems[jIndex] && jIndex == (dropDownItems.length - 1))
                {
                    failedCount = failedCount + 1;
                    errorMsg = errorMsg + expectedDropDownItems[index]+" option is not available \n"
                }
                else if(expectedDropDownItems[index] == dropDownItems[jIndex])
                {
                    break;
                }
            }
           }
           if(failedCount>0)throw "Failed-"+errorMsg;
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user is allowed for selection of Market.", async () => {

        let stepAction = "Check if user is allowed for selection of Market.";
        let stepData = "";
        let stepExpResult = "User should be able to select Market from Market drop down list.";
        let stepActualResult = "User should be able to select Market from Market drop down list.";

        try {
                await commonLib.selectDropDownValue("Market",globalTestData.marketslist[0]);
                await verify.verifyElementIsDisplayed(localDayparts.defaultNotifyMsg,"Verify Default Notify Message");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user can view the selected channels.", async () => {

        let stepAction = "Check if user can view the selected channels.";
        let stepData = "";
        let stepExpResult = "User should be able to view the selected channels.";
        let stepActualResult = "User is able to view the selected channels.";

        try {

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check for the selection of channel.", async () => {

        let stepAction = "Check for the selection of channel.";
        let stepData = "";
        let stepExpResult = "Required channels should be allowed for selection.";
        let stepActualResult = "( Covered in next step -Check if user can add deviaton for channel specific 'spec' ) ";

        try {

            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user is able to exclude channel from selection.", async () => {

        let stepAction = "Check if user is able to exclude channel from selection.";
        let stepData = "";
        let stepExpResult = "User should have an option to exclude channel.";
        let stepActualResult = "(Covered in below specs)";

        try {

            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user is able to add deviation to the daypart structure.", async () => {

        let stepAction = "Check if user is able to add deviation to the daypart structure.";
        let stepData = "";
        let stepExpResult = "User should be able to add deviation to the daypart.";
        let stepActualResult = "(Covered in below specs)";

        try {
           
            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, "Covered in 'Check if user can add deviaton for channel specific' spec");
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user can add deviaton for channel specific.", async () => {

        let stepAction = "Check if user can add deviaton for channel specific.";
        let stepData = "";
        let stepExpResult = "User should be able to add deviation to the channel specific.";
        let stepActualResult = "User is able to add deviation to the channel specific.";

        try {
            await browser.sleep(5000);
            await localDayparts.addDeviationChannel("ESPN (CABLE)");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check for deviation time details", async () => {

        let stepAction = "Check for deviation time details";
        let stepData = "";
        let stepExpResult = "User should be able to add time details for each deviation.";
        let stepActualResult = "User should be able to add time details for each deviation.";

        try {
            await browser.sleep(5000);
            await verify.verifyProgressBarNotPresent();
            await localDayparts.ClickOnDeviationChannel("ESPN(CABLE)");
            await verify.verifyProgressBarNotPresent();
            await action.SetText(element.all(localDayparts.textFilter).get(0), "Daytime", "dayPartName")
            await localDayparts.dayPartGridValidation("Monday","5:30-6:00 AM");
            await localDayparts.dayPartGridValidation("Tuesday","11:00 AM-11:00 AM");
            await localDayparts.dayPartGridValidation("Wednesday","9:00 AM-5:00 PM");
            await localDayparts.dayPartGridValidation("Thursday","9:00 AM-5:00 PM");
            await localDayparts.dayPartGridValidation("Friday","9:00 AM-6:00 PM");
            await localDayparts.dayPartGridValidation("Saturday","9:00-10:00 AM");
            await localDayparts.dayPartGridValidation("Sunday","9:00 AM-12:00 PM");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user can save the deviation data.", async () => {

        let stepAction = "Check if user can save the deviation data.";
        let stepData = "";
        let stepExpResult = "User should be able to save the added deviation data.";
        let stepActualResult = "Useris able to save the added deviation data.";

        try {

            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user can view multiple day/time of daypart for Deviated channel.", async () => {

        let stepAction = "Check if user can view multiple day/time of daypart for Deviated channel.";
        let stepData = "";
        let stepExpResult = "User should be able to view multiple day/time dayparts for deviated channel.";
        let stepActualResult = "Useris able to view multiple day/time dayparts for deviated channel.";

        try {
            await localDayparts.dayPartGridValidation("Daypart Name","Daytime");
            await action.ClearText(element.all(localDayparts.textFilter).get(0),"");
            await action.SetText(element.all(localDayparts.textFilter).get(0), "Early Fringe", "dayPartName")
            await localDayparts.dayPartGridValidation("Daypart Name","Early Fringe");
            await action.ClearText(element.all(localDayparts.textFilter).get(0),"");
            await action.SetText(element.all(localDayparts.textFilter).get(0), "Prime", "dayPartName")
            await localDayparts.dayPartGridValidation("Daypart Name","Prime");
            await action.ClearText(element.all(localDayparts.textFilter).get(0),"");
            await action.SetText(element.all(localDayparts.textFilter).get(0), "Morning", "dayPartName")
            await localDayparts.dayPartGridValidation("Daypart Name","Morning");
            await action.ClearText(element.all(localDayparts.textFilter).get(0),"");
            await action.SetText(element.all(localDayparts.textFilter).get(0), "Weekend", "dayPartName")
            await localDayparts.dayPartGridValidation("Daypart Name","Weekend");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user can add/update valid multiple day/time daypart for deviated channel", async () => {

        let stepAction = "Check if user can add/update valid multiple day/time daypart for deviated channel";
        let stepData = "";
        let stepExpResult = "Should be able to add/update valid multiple day/time for deviated channel.";
        let stepActualResult = "Able to add/update valid multiple day/time for deviated channel.";

        try {
            await action.ClearText(element.all(localDayparts.textFilter).get(0),"");
            await action.SetText(element.all(localDayparts.textFilter).get(0), "Daytime", "dayPartName")
            let Daypart1 = await localDayparts.returnComponentsTagNameContainingText("a","Daytime","dayPartName");
            await action.Click(Daypart1,"Daytime");
            await browser.sleep(1000);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    it("Check if user can update daypart multiple distinct day/time for deviated channel.", async () => {

        let stepAction = "Check if user can update daypart multiple distinct day/time for deviated channel.";
        let stepData = "";
        let stepExpResult = "Updated distinct day/time for deviated channel should be displayed.";
        let stepActualResult = "Updated distinct day/time for deviated channel is displayed.";

        try {
            await localDayparts.enterTimeInDaypartEdit("MONStart","06:30 AM","");
            await localDayparts.enterTimeInDaypartEdit("MONEnd","07:30 AM","");
            await localDayparts.enterTimeInDaypartEdit("TUEStart","06:30 AM","");
            await localDayparts.enterTimeInDaypartEdit("TUEEnd","07:30 AM","");
            await action.Click(localDayparts.SaveButton_AddP,"Click On Update");
            await browser.sleep(2000);
            await commonLib.verifyPopupMessage("Update Daypart Deviation Details successfully");
            await localDayparts.deleteDeviationChannel("ESPN(CABLE)");
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    it("Check if the user is notified that all cable stations will follow the broadcast structure.", async () => {

        let stepAction = "Check if the user is notified that all cable stations will follow the broadcast structure.";
        let stepData = "";
        let stepExpResult = "'Default Notification should be displayed to user ''All cable stations will follow the below broadcast structure'''";
        let stepActualResult = "'Default Notification is displayed to user ''All cable stations will follow the below broadcast structure'''";
        try {
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, "Covered in 'Check if user is allowed for selection of Market' Spec");
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Skipping the last 3 Test Steps as not applicable in chassis------------
    xit("Check if user is able to add a cable station.", async () => {

        let stepAction = "Check if user is able to add a cable station.";
        let stepData = "";
        let stepExpResult = "User should be able to add cable stations.";
        let stepActualResult = "User is able to add cable stations.";

        try {

            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, stepActualResult);
        }
        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }
    });

    // --------------Test Step------------
    xit("Check option to add cable station in the deviation", async () => {

        let stepAction = "Check option to add cable station in the deviation";
        let stepData = "";
        let stepExpResult = "From deviations, User should be allowed to add a cable station.";
        let stepActualResult = "From deviations, User is allowed to add a cable station.";

        try {

            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });

    // --------------Test Step------------
    xit("Check the linking of Cable station deviation to the daypart", async () => {

        let stepAction = "Check the linking of Cable station deviation to the daypart";
        let stepData = "";
        let stepExpResult = "User should be allowed to link cable station to the daypart.";
        let stepActualResult = '';

        try {

            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
