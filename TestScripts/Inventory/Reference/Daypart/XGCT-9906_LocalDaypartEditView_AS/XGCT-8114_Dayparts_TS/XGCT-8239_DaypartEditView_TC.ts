/*
***********************************************************************************************************
* @Script Name :  XGCT-8239
* @Description :  xG Campaign - PI-0 Refactor for Dayparts - Verify the functionality of Daypart View - view/edit/update : Start Time, End Time, Daypart Weekdays.23-8-2019
* @Page Object Name(s) : XXX
* @Dependencies/Libs : 
* @Pre-Conditions : 
* @Creation Date : 23-8-2019
* @Author : 
* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/

//Import Statements
import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Reporter } from '../../../../../../Utility/htmlResult';
import { VerifyLib } from '../../../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../../../Libs/GeneralLibs/ActionLib';
import { globalvalues } from '../../../../../../Utility/globalvalue';
import { HomePageFunction } from '../../../../../../Pages/Home/HomePage';
import { LocalDayparts } from '../../../../../../Pages/Inventory/Reference/Dayparts/LocalDayparts';
import { globalTestData } from '../../../../../../TestData/globalTestData';


//Import Class Objects Instantiation
let report = new Reporter();
let verify = new VerifyLib();
let action = new ActionLib();
let globalFunction = new globalvalues();
let homePage = new HomePageFunction();
let dayPartv = new LocalDayparts()

//Variables Declaration
let dayPartName = "Access"//let DayPartsCode: string = "EM,DT,EF,EN,PA,SP,PT,LN,LF,WK,ON,ST"
let MONstartTime = "01:00 AM"
let MONendTime = "02:00 AM"
let TUEstartTime = "02:00 AM"
let TUEendTime = "03:00 AM"
let WEDstartTime = "04:00 AM"
let WEDendTime = "05:00 AM"
let THUstartTime = "06:00 AM"
let THUendTime = "07:00 AM"
let FRIstartTime = "08:00 AM"
let FRIendTime = "09:00 AM"
let SATstartTime = "10:00 AM"
let SATendTime = "11:00 AM"
let SUNstartTime = "12:00 AM"
let SUNendTime = "12:00 PM"
let dayPartCode = "PA"

let TestCase_ID = 'XGCT-8239'
let TestCase_Title = 'xG Campaign - PI-0 Refactor for Dayparts - Verify the functionality of Daypart View - view/edit/update - Start Time End Time Daypart Weekdays.'

//HTML Report generate intiation
report.InitializeSigleHtmlReport(TestCase_ID, TestCase_Title);
globalvalues.reportInstance = report;

//**********************************  TEST CASE Implementation ***************************
describe('XG CAMPAIGN - PI-0 REFACTOR FOR DAYPARTS - VERIFY THE FUNCTIONALITY OF DAYPART VIEW - VIEW/EDIT/UPDATE : START TIME, END TIME, DAYPART WEEKDAYS.', () => {

    // --------------Test Step------------
    it("Open browser and navigate to URL : http://xgcampaignwebapp.azurewebsites.net/", async () => {

        let stepAction = "Open browser and navigate to URL : http://xgcampaignwebapp.azurewebsites.net/";
        let stepData = "";
        let stepExpResult = "Campaign Web Application page should be launched.";
        let stepActualResult = "Campaign Web Application page should be launched.";

        try {
            await globalFunction.LaunchStoryBook();
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Navigate to Programs-->Inventory -->Daypart View.", async () => {

        let stepAction = "Navigate to Programs-->Inventory -->Daypart View.";
        let stepData = "";
        let stepExpResult = "Daypart View page should be displayed.";
        let stepActualResult = "Daypart View page should be displayed.";

        try {
            await homePage.appclickOnNavigationList(globalTestData.programmingTabName);
            await homePage.appclickOnMenuListLeftPanel(globalTestData.leftMenuReferenceName);
            await homePage.appclickOnMenuListLeftPanel(globalTestData.leftdaypart);
            await homePage.appclickOnMenuListLeftPanel(globalTestData.leftLocalDayparts);
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user can view the Daypart Name.", async () => {

        let stepAction = "Check if user can view the Daypart Name.";
        let stepData = "";
        let stepExpResult = "User should be able to view the Daypart name in the list page.";
        let stepActualResult = "User should be able to view the Daypart name in the list page.";

        try {
            let Daypart1 = await dayPartv.returnComponentsTagNameContainingText("a", dayPartName, "dayPartName")
            await action.Click(Daypart1, "Late News")
            await browser.sleep(1000)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user can view the Daypart Code.", async () => {

        let stepAction = "Check if user can view the Daypart Code.";
        let stepData = "";
        let stepExpResult = "User should be able to view dayparts code in the list page.";
        let stepActualResult = "User should be able to view dayparts code in the list page.";

        try {
            await dayPartv.enterTimeInDaypartEdit("MONStart", MONstartTime, "MONStart")
            await dayPartv.enterTimeInDaypartEdit("MONEnd", MONendTime, "MONEnd")


            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user can view daypart start time.", async () => {

        let stepAction = "Check if user can view daypart start time.";
        let stepData = "";
        let stepExpResult = "User should be able to view dayparts start time in the list page.";
        let stepActualResult = "User should be able to view dayparts start time in the list page.";

        try {
            await dayPartv.enterTimeInDaypartEdit("TUEStart", TUEstartTime, "TUEStart")
            await dayPartv.enterTimeInDaypartEdit("TUEEnd", TUEendTime, "TUEEnd")


            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check for the availability of time(HH:MM) in the start time.", async () => {

        let stepAction = "Check for the availability of time(HH:MM) in the start time.";
        let stepData = "";
        let stepExpResult = "Time format(HH:MM) should be available.";
        let stepActualResult = "Time format(HH:MM) should be available.";

        try {
            await dayPartv.enterTimeInDaypartEdit("WEDStart", WEDstartTime, "WEDStart")
            await dayPartv.enterTimeInDaypartEdit("WEDEnd", WEDendTime, "WEDEnd")

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the Start Time format.", async () => {

        let stepAction = "Check the Start Time format.";
        let stepData = "";
        let stepExpResult = "Time should be displayed in the 12 hour format followed by AM/PM";
        let stepActualResult = "Time should be displayed in the 12 hour format followed by AM/PM";

        try {
            await dayPartv.enterTimeInDaypartEdit("THUStart", THUstartTime, "THUStart")
            await dayPartv.enterTimeInDaypartEdit("THUEnd", THUendTime, "THUEnd")



            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check by Updating daypart Start Time.", async () => {

        let stepAction = "Check by Updating daypart Start Time.";
        let stepData = "";
        let stepExpResult = "Daypart Start Time should be updated and displayed in the list.";
        let stepActualResult = "Daypart Start Time should be updated and displayed in the list.";

        try {
            await dayPartv.enterTimeInDaypartEdit("FRIStart", FRIstartTime, "FRIStart")
            await dayPartv.enterTimeInDaypartEdit("FRIEnd", FRIendTime, "FRIEnd")


            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check for Invalid time/format update.", async () => {

        let stepAction = "Check for Invalid time/format update.";
        let stepData = "Add invalid Data: 456665/testfail into the time field";
        let stepExpResult = "'Field should display validation error message ''Invalid time format''.'";
        let stepActualResult = "'Field should display validation error message ''Invalid time format''.'";

        try {
            await dayPartv.enterTimeInDaypartEdit("SATStart", SATstartTime, "SATStart")
            await dayPartv.enterTimeInDaypartEdit("SATEnd", SATendTime, "SATEnd")
            await dayPartv.enterTimeInDaypartEdit("SUNStart", SUNstartTime, "SUNStart")
            await dayPartv.enterTimeInDaypartEdit("SUNEnd", SUNendTime, "SUNEnd")
            await browser.sleep(1000)
            await action.Click(dayPartv.SaveButton_AddP, "Save")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user can view daypart End time.", async () => {

        let stepAction = "Check if user can view daypart End time.";
        let stepData = "";
        let stepExpResult = "User should be able to view dayparts End time in the list page.";
        let stepActualResult = "User should be able to view dayparts End time in the list page.";

        try {
            await browser.sleep(1000)
            await dayPartv.VerifyColumnNameDisplayedInGrid(["Monday", "Tuesday", "Wednesday"])
            await action.SetText(element.all(dayPartv.textFilter).get(0), dayPartName, "dayPartName")


            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check for the availability of time(HH:MM) in the End time.", async () => {

        let stepAction = "Check for the availability of time(HH:MM) in the End time.";
        let stepData = "";
        let stepExpResult = "Time format(HH:MM) should be available.";
        let stepActualResult = "Time format(HH:MM) should be available.";

        try {
            await dayPartv.dayPartGridValidation("Daypart Name", dayPartName)
            await dayPartv.dayPartGridValidation("Daypart Code", dayPartCode)
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check the End Time format.", async () => {

        let stepAction = "Check the End Time format.";
        let stepData = "";
        let stepExpResult = "Time should be displayed in the 12 hour format followed by AM/PM";
        let stepActualResult = "Time should be displayed in the 12 hour format followed by AM/PM";

        try {
            await dayPartv.dayPartGridValidation("Monday", "1:00-2:00 AM")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check by Updating daypart EndTime.", async () => {

        let stepAction = "Check by Updating daypart EndTime.";
        let stepData = "";
        let stepExpResult = "Daypart EndTime should be updated and displayed in the list.";
        let stepActualResult = "Daypart EndTime should be updated and displayed in the list.";

        try {

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check for Invalid time/format update.", async () => {

        let stepAction = "Check for Invalid time/format update.";
        let stepData = "Add invalid Data: 456665/testfail into the time field";
        let stepExpResult = "'Field should display validation error message ''Invalid time format''.'";
        let stepActualResult = "'Field should display validation error message ''Invalid time format''.'";

        try {

            let Daypart1 = await dayPartv.returnComponentsTagNameContainingText("a", dayPartName, "dayPartName")
            await action.Click(Daypart1, "Late News")
            await browser.sleep(1000)


            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user can view daypart Week days.", async () => {

        let stepAction = "Check if user can view daypart Week days.";
        let stepData = "";
        let stepExpResult = "User should be able to view dayparts Days in the list page.";
        let stepActualResult = "User should be able to view dayparts Days in the list page.";

        try {
            await dayPartv.enterTimeInDaypartEdit("MONStart", "asd", "MONStart")
            await browser.sleep(1000)
            let errorMsg = await dayPartv.returnComponentsTagNameContainingText("div", "Invalid time format", "Error message")
            await verify.verifyTextOfElement(errorMsg, "Invalid time format", "Error message")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check for the valid daypart Days.", async () => {

        let stepAction = "Check for the valid daypart Days.";
        let stepData = "";
        let stepExpResult = "Valid daypart Days should be displayed.";
        let stepActualResult = "Valid daypart Days should be displayed.";

        try {
            await dayPartv.enterTimeInDaypartEdit("SUNStart", SUNstartTime, "SUNStart")

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check by Updating time in daypart days.", async () => {

        let stepAction = "Check by Updating time in daypart days.";
        let stepData = "";
        let stepExpResult = "Updated time for respective daypart days should be displayed in the list.";
        let stepActualResult = "Updated time for respective daypart days should be displayed in the list.";

        try {
            await dayPartv.enterTimeInDaypartEdit("SUNEnd", "", "SUNEnd")
            await dayPartv.enterTimeInDaypartEdit("SUNEnd", SUNendTime, "SUNEnd")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check for Time format for Daypart Days.", async () => {

        let stepAction = "Check for Time format for Daypart Days.";
        let stepData = "";
        let stepExpResult = "Time should be displayed in the 12 hour format followed by AM/PM";
        let stepActualResult = "Time should be displayed in the 12 hour format followed by AM/PM";

        try {
            await dayPartv.enterTimeInDaypartEdit("SUNEnd", "", "SUNEnd")
            await dayPartv.enterTimeInDaypartEdit("SUNEnd", SUNendTime, "SUNEnd")
            await browser.sleep(1000)
            await action.Click(dayPartv.SaveButton_AddP, "Save")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check for Invalid data.", async () => {

        let stepAction = "Check for Invalid data.";
        let stepData = "Add invalid Data: 456665/testfail/@@@ehk into the time field";
        let stepExpResult = "'Field should display validation error message ''Invalid time format''.'";
        let stepActualResult = "'Field should display validation error message ''Invalid time format''.'";

        try {
            await browser.sleep(1000)
            await dayPartv.VerifyColumnNameDisplayedInGrid(["Monday", "Tuesday", "Wednesday"])


            // await action.SetText(element.all(dayPartv.textFilter).get(0), dayPartName, "dayPartName")

            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user can view single day/time of daypart.", async () => {

        let stepAction = "Check if user can view single day/time of daypart.";
        let stepData = "";
        let stepExpResult = "User should be able to view single day/time dayparts.";
        let stepActualResult = "User should be able to view single day/time dayparts.";

        try {
            // await dayPartv.dayPartGridValidation("Monday", "1:00-2:00 AM")
            report.ReportStatus(stepAction, stepData, 'Pass', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user can add/update valid single day/time daypart.", async () => {

        let stepAction = "Check if user can add/update valid single day/time daypart.";
        let stepData = "";
        let stepExpResult = "Should be able to add/update a valid single day/time. ";
        let stepActualResult = "Should be able to add/update a valid single day/time. ";

        try {


            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if added/updated valid single day/time daypart is displayed.", async () => {

        let stepAction = "Check if added/updated valid single day/time daypart is displayed.";
        let stepData = "";
        let stepExpResult = "Added/Updated single day/time should be displayed.";
        let stepActualResult = "Added/Updated single day/time should be displayed.";

        try {


            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, "Covered in 'Check if user can view single day/time of daypart 'spec");
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user can view multiple day/time of daypart.", async () => {

        let stepAction = "Check if user can view multiple day/time of daypart.";
        let stepData = "";
        let stepExpResult = "User should be able to view multiple day/time dayparts.";
        let stepActualResult = "User should be able to view multiple day/time dayparts.";

        try {


            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, "Covered in 'Check if user can view single day/time of daypart 'spec");
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user can add/update valid multiple day/time daypart.", async () => {

        let stepAction = "Check if user can add/update valid multiple day/time daypart.";
        let stepData = "";
        let stepExpResult = "Should be able to add/update valid multiple day/time .";
        let stepActualResult = "Should be able to add/update valid multiple day/time .";

        try {

            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


    // --------------Test Step------------
    it("Check if user can update daypart multiple distinct day/time.", async () => {

        let stepAction = "Check if user can update daypart multiple distinct day/time.";
        let stepData = "";
        let stepExpResult = "Updated distinct day/time should be displayed.";
        let stepActualResult = "Updated distinct day/time should be displayed.";

        try {

            report.ReportStatus(stepAction, stepData, 'Skip', stepExpResult, stepActualResult);
        }

        catch (err) {
            report.ReportStatus(stepAction, stepData, 'Fail', stepExpResult, err);
        }

    });


});
