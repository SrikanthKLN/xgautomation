@echo off
set CsvFileName=XGCT-7432
set TypeScriptFileName=XGCT-7432


cd D:\XGCQAAutomation\Utility

echo *********Copy to result to backup folder**********
node --max-old-space-size=2000 generateBasicTemplate.js %CsvFileName% %TypeScriptFileName%|more
Pause