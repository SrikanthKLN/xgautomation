@echo off
set projectPath=D:\xGC-Automation

echo *********delete java script folder**********
cd %projectPath%\JavaScriptFiles
DEL /F/Q/S *.* > NUL|more
echo *********delete java script folder--Done**********

npm cache clean --force|more

echo *********Compile In to Typescript**********
tsc|more
echo *********Compilation done In to Typescript**********

echo *********Move To Destination Folder**********
cd %projectPath%\JavaScriptFiles\Config

echo *********Protractor Execution Started**********
CALL  protractor Config.js

echo *********Protractor Execution Completed**********
echo *********genrate Report**********
cd %projectPath%\JavaScriptFiles\Utility

node --max-old-space-size=2000 conreportGen.js|more
echo *********Report Generation Completed**********

taskkill /IM "chromedriver_76.0.3809.12.exe" /F
::echo *********Copy to result to backup folder**********
::node --max-old-space-size=2000 copyresultintobackup.js|more
Pause