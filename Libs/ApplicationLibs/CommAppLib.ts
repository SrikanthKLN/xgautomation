import { browser, element, by, $, $$, ExpectedConditions, protractor, ProtractorBy, ElementFinder, WebElement } from 'protractor';
import { By } from 'selenium-webdriver';


import { Gutils } from '../../Utility/gutils';
import { Logger } from '../../Utility/logger';
import { globalvalues } from '../../Utility/globalvalue';
import { ActionLib } from '../../Libs/GeneralLibs/ActionLib';
import { Reporter } from '../../Utility/htmlResult';
import { VerifyLib } from '../GeneralLibs/VerificationLib';

let logger = new Logger();
let gUtility = new Gutils();
let report = new Reporter();
let action = new ActionLib();;
let verify = new VerifyLib();

//variables
let elementStatus: boolean;
let sortingErrorMessage: string;
export class AppCommonFunctions {
    pageHeader = by.css("[class='xg-main-header']");

    // dropDown = "xg-dropdown[label='dynamic'] div[class*='ui-dropdown-trigger']";
    dropDown = "//label[normalize-space()='dynamic']/ancestor::xg-dropdown//div[contains(@class,'ui-dropdown-trigger')]"
    dropDownOptions = by.css("div[class*='ui-widget-content'] ul[class*='ui-dropdown-list']>li");
    multiSelectDropDown = "xg-multi-select[attr-name*='dynamic'] div[class*='ui-multiselect-trigger']";;
    multiSelectDropDownOptions = by.css("div[class*='ui-widget-content'] ul[class*='ui-multiselect-list'] li");
    multiSelectDropDownInPopup = "div[class*='popup'] xg-multi-select[attr-name*='dynamic'] div[class*='ui-multiselect-trigger']";
    multiSelectClose = by.css("a[class*='ui-multiselect-close']");
    addButton = by.css("xg-button[attr-name*='xgc-add'] button");
    globalFilter = by.css("xg-grid-toolbar input[placeholder*='Global Filter']");
    // noDataMatchFound = by.xpath("//div[contains(@class,'ui-table-unfrozen-view')]//td[normalize-space()='No data match is found']");
    // noDataMatchFound = by.xpath("//div[contains(@class,'ui-table-unfrozen-view') or contains(@class,'ui-table-wrapper')]//td[normalize-space()='No data match is found']");
    // tableHeader = by.xpath("//div[contains(@class,'ui-table-unfrozen-view')]//table/thead[@class='ui-table-thead']/tr/th[@tabindex]");
    // gridTableSearchInput = "//div[contains(@class,'ui-table-unfrozen-view')]//table/thead/tr[contains(@class,'xg-search-filter')]/th[@class='ng-star-inserted'][dynamic]//input[contains(@class,'text-box') or contains(@class,'inputtext')]";
    imgClear = by.xpath("//i[contains(@class,'circle ng-star-inserted')]");
    // filterTypeDropDown = "//div[contains(@class,'ui-table-unfrozen-view')]//table/thead/tr[contains(@class,'xg-search-filter')]/th[not(contains(@class,'checkbox'))][dynamic]/xg-grid-column-filter//p-dropdown//label/div/*";
    paginationActiveLastInGrid = by.xpath("//a[contains(@class,'ui-paginator-last') and not(contains(@class,'disabled'))]");
    paginationActiveNextInGrid = by.xpath("//a[contains(@class,'ui-paginator-next') and not(contains(@class,'disabled'))]");
    paginationActiveFirstInGrid = by.xpath("//a[contains(@class,'ui-paginator-first') and not(contains(@class,'disabled'))]");
    paginationActivePrevInGrid = by.xpath("//a[contains(@class,'ui-paginator-prev') and not(contains(@class,'disabled'))]");
    activePaginationInGrid = by.xpath("//span[@class='ui-paginator-pages']/a[contains(@class,'ui-state-active')]");
    paginationInactiveFirstInGrid = by.css("a[class*='ui-paginator-first'][class*='disabled']");
    paginationInactiveLastInGrid = by.css("a[class*='ui-paginator-last'][class*='disabled']");
    paginationInactivePrevInGrid = by.css("a[class*='ui-paginator-prev'][class*='disabled']");
    paginationInactiveNextInGrid = by.css("a[class*='ui-paginator-next'][class*='disabled']");
    activePageNumber = "//div[contains(@class,'ui-paginator-bottom')]/span[@class='ui-paginator-pages']/a[contains(@class,'ui-state-active') and text()='dynamic']";
    // rowCount = by.xpath("//div[contains(@class,'ui-table-unfrozen-view')]//table/tbody/tr");
    rowCount = by.xpath("//div[contains(@class,'ui-table-unfrozen-view') or contains(@class,'ui-table-wrapper')]//table/tbody/tr");
    // gridCellData = "//div[contains(@class,'ui-table-unfrozen-view')]//table/tbody/tr[rowdynamic]/td[not(contains(@class,'checkbox'))][columndynamic]/xg-grid-data-cell/span[@class='ng-star-inserted']";

    // multiDropDownSelectedStatus = "div[class*='ui-widget-content'] ul[class*='ui-multiselect-list'] li:nth-child(dynamic) span[class*='check']"
    multiDropDownSelectedStatus = "div[class*='ui-widget-content'] ul[class*='ui-multiselect-list']>p-multiselectitem:nth-child(dynamic)>li span[class*='check']"

    gridHeaderCheckBox = by.xpath("//xg-grid[@attr-name='xgc-program-grid' or @attr-name='xgc-display-network-programs']//tr[contains(@class,'xg-grid-topHead')]/th[contains(@class,'xg-grid-column-select-checkbox')]//div[contains(@class,'ui-chkbox-box') and not(contains(@class,'ui-state-active'))]");
    gridHeaderChecBoxChecked = by.xpath("//xg-grid[@attr-name='xgc-program-grid' or @attr-name='xgc-display-network-programs']//tr[contains(@class,'xg-grid-topHead')]/th[contains(@class,'xg-grid-column-select-checkbox')]//div[contains(@class,'ui-chkbox-box') and contains(@class,'ui-state-active')]");
    multiSelectDropDownHeaderCheckBoxActive = by.css("div[class*='ui-widget-content'] div[class*='ui-widget-header'] div[class*='ui-chkbox-box'][class*='active']");
    multiSelectDropDownHeaderCheckBox = by.css("div[class*='ui-widget-content'] div[class*='ui-widget-header'] div[class*='ui-chkbox-box']:not([class*='active'])");
    expandButton = by.xpath("//xg-grid[@attr-name='xgc-program-grid' or @attr-name='xgc-display-network-programs']//button[@ng-reflect-message='Expand']")

    noDataMatchFound = by.xpath("//div[contains(@class,'ui-table-unfrozen-view') or contains(@class,'ui-table-wrapper')]//td[normalize-space()='No data match is found']");
    tableHeader = by.xpath("//div[contains(@class,'ui-table-unfrozen-view') or contains(@class,'ui-table-wrapper')]//table/thead[@class='ui-table-thead']/tr/th[@tabindex]");
    gridTableSearchInput = "//div[contains(@class,'ui-table-unfrozen-view') or contains(@class,'ui-table-wrapper')]//table/thead/tr[contains(@class,'xg-search-filter')]/th[@class='ng-star-inserted'][dynamic]//input[contains(@class,'text-box') or contains(@class,'inputtext')]";
    filterTypeDropDown = "//div[contains(@class,'ui-table-unfrozen-view') or contains(@class,'ui-table-wrapper')]//table/thead/tr[contains(@class,'xg-search-filter')]/th[not(contains(@class,'checkbox'))][dynamic]/xg-grid-column-filter//p-dropdown//label/div/*";
    gridCellData = "//div[contains(@class,'ui-table-unfrozen-view') or contains(@class,'ui-table-wrapper')]//table/tbody/tr[rowdynamic]/td[not(contains(@class,'checkbox'))][columndynamic]/xg-grid-data-cell/span[@class='ng-star-inserted']";
    // multiDropDownSelectedStatus = "div[class*='ui-widget-content'] ul[class*='ui-multiselect-list'] li:nth-child(dynamic) span[class*='check']"
    // gridHeaderCheckBox = by.xpath("//xg-grid[@attr-name='xgc-program-grid' or @attr-name='xgc-display-network-programs']//tr[contains(@class,'xg-grid-topHead')]/th[contains(@class,'xg-grid-column-select-checkbox')]//div[contains(@class,'ui-chkbox-box') and not(contains(@class,'ui-state-active'))]");
    // gridHeaderChecBoxChecked = by.xpath("//xg-grid[@attr-name='xgc-program-grid' or @attr-name='xgc-display-network-programs']//tr[contains(@class,'xg-grid-topHead')]/th[contains(@class,'xg-grid-column-select-checkbox')]//div[contains(@class,'ui-chkbox-box') and contains(@class,'ui-state-active')]");
    // multiSelectDropDownHeaderCheckBoxActive = by.css("div[class*='ui-widget-content'] div[class*='ui-widget-header'] div[class*='ui-chkbox-box'][class*='active']");
    // multiSelectDropDownHeaderCheckBox = by.css("div[class*='ui-widget-content'] div[class*='ui-widget-header'] div[class*='ui-chkbox-box']:not([class*='active'])");
    fieldError = by.css("div[class*='alert-dange']");
    gridSearchTextBox = "//div[contains(@class,'ui-table-unfrozen-view') or contains(@class,'ui-table-wrapper')]//table/thead[@class='ui-table-thead']/tr[contains(@class,'xg-search-filter')]/th[@class='ng-star-inserted'][dynamic]//p-dropdown/following-sibling::input"

    sideMenuLink = "//img-lib-menu//span[normalize-space(text())='dynamic']"

    multiSelectSearchInput = by.css("div[class*='multiselect-filter']>input");




    /**
 * desc: 
 * @method clickOnComponentLink
 * @author: 
 * @param :{}
 * @return none
 */
    async clickOnComponentLink(linkname: string) {
        try {
            await browser.sleep(5000);
            let XpXgTextAreaLink = by.xpath('//*[contains(text(),"' + linkname + '")]');
            await browser.executeScript('arguments[0].scrollIntoView()', element(XpXgTextAreaLink).getWebElement());
            await action.Click(XpXgTextAreaLink, linkname);
        }
        catch (err) {
            throw new Error(err);
        }

    }


    /**
 * desc: 
 * @method validateSearchBoxForTableHeaders
 * @author: 
 * @param :{}
 * @return none
 */
    async validateSearchBoxForTableHeaders() {
        let failedCount = 0;
        let errorMessage = "";
        let isPresent;
        try {
            await browser.sleep(500);
            element.all(by.xpath("//tr[contains(@class,'xg-search-filter')]//th[@class='ng-star-inserted']")).then(async function (_element) {
                isPresent = element(_element).isDisplayed;
                if (!isPresent) {
                    failedCount = failedCount + 1;
                    errorMessage = errorMessage + " Search is not available \n";
                }
            });
            if (failedCount > 0) {
                await action.ReportSubStep("validateSearchFields in Headers", errorMessage, "Fail");
            }
        }
        catch (err) {
            throw new Error('Error');
        }
    }

    /**
 * desc: 
 * @method validateHeaders
 * @author: 
 * @param :{}
 * @return none
 */
    async validateHeaders(headers: Array<any>) {
        let failedCount = 0;
        let errorMessage = "";
        try {
            await browser.sleep(500);
            let header: string
            let elementsCount = await action.getWebElementsCount(by.css("div[class*='ui-table-unfrozen-view'] thead[class='ui-table-thead']>tr>th[tabindex]"));
            for (let i = 2; i <= elementsCount + 1; i++) {
                action.MouseMoveJavaScript(by.css("div[class*='ui-table-unfrozen-view'] thead[class='ui-table-thead']>tr>th[tabindex]:nth-child(" + i + ")"), "");
                header = String(await action.GetText(by.css("div[class*='ui-table-unfrozen-view'] thead[class='ui-table-thead']>tr>th[tabindex]:nth-child(" + i + ")"), ""));
                console.log("received:" + header.trim() + "gave:" + headers[i - 2].trim());
                if (header.trim() != headers[i - 2].trim() && i == elementsCount) {
                    failedCount = failedCount + 1;
                    errorMessage = errorMessage + headers[i - 2] + " is not available \n";
                }
                else if (header.trim() == headers[i - 2].trim()) {
                    continue;
                }
            }
            if (failedCount > 0) {
                await action.ReportSubStep("validateHeaders", errorMessage, "Fail");
            }
        }
        catch (err) {
            throw new Error('Error');
        }
    }

    /**
        * desc: It is used to select the dropdown option based on option name if option is not available it will fail
        * @method selectDropDownValue
        * @author: Venkata Sivakumar
        * @param :{labelName:string,optionName:string}
        * @return none
        */
    async selectDropDownValue(labelName: string, optionName: string) {
        try {
            elementStatus = await verify.verifyElementVisible(by.xpath(this.dropDown.replace("dynamic", labelName.trim())));
            if (elementStatus) {
                await action.Click(by.xpath(this.dropDown.replace("dynamic", labelName.trim())), "Click On " + labelName + " dropdown");
                await verify.verifyElementIsDisplayed(this.dropDownOptions, "Dropdown is expanded");
                let elements = await action.getWebElements(this.dropDownOptions);
                if (elements.length == 0) {
                    await action.ReportSubStep("selectDropDownValue", "There are no options available in " + labelName + " dropdown", "Fail");
                }
                else {
                    for (let index = 0; index < elements.length; index++) {
                        let option = await elements[index].getText();
                        if (option.trim() == optionName.trim()) {
                            await elements[index].click();
                            await browser.sleep(1000);
                            break;
                        }
                        else if (option.trim() != optionName.trim() && index == elements.length - 1) {
                            await action.ReportSubStep("selectDropDownValue", "There is no option available with name " + optionName + " in " + labelName + " dropdown", "Fail");
                        }
                    }
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
     * desc: It is used to select the dropdown option based on option names if any one of the option is not available it will fail
     * @method selectDropDownValue
     * @author: Venkata Sivakumar
     * @param :{labelName:string,optionNames:Array<string>}
     * @return none
     */

    async selectMultiOptionsFromDropDown(labelName: string, optionNames: Array<string>, role: string = "") {
        try {
            if (role == "") {
                elementStatus = await verify.verifyElementVisible(by.css(this.multiSelectDropDown.replace("dynamic", labelName.trim())));
                if (elementStatus) {
                    await action.Click(by.css(this.multiSelectDropDown.replace("dynamic", labelName.trim())), "Click On " + labelName + " dropdown");
                }
                else {
                    await action.ReportSubStep("selectMultiOptionsFromDropDown", by.css(this.multiSelectDropDown.replace("dynamic", labelName.trim())) + " locator is not available", "Fail");
                }
            }
            else {
                elementStatus = await verify.verifyElementVisible(by.css(this.multiSelectDropDownInPopup.replace("dynamic", labelName.trim())));
                if (elementStatus) {
                    await action.Click(by.css(this.multiSelectDropDownInPopup.replace("dynamic", labelName.trim())), "Click On " + labelName + " dropdown");
                }
                else {
                    await action.ReportSubStep("selectMultiOptionsFromDropDown", by.css(this.multiSelectDropDownInPopup.replace("dynamic", labelName.trim())) + " locator is not available", "Fail");
                }
            }
            await browser.sleep(1000);
            await verify.verifyElementIsDisplayed(this.multiSelectDropDownOptions, "Verify Multi Select Dropdown is expanded");
            let elements = await action.getWebElements(this.multiSelectDropDownOptions);
            if (elements.length == 0) {
                await action.ReportSubStep("selectMultiOptionsFromDropDown", "There are no options available in " + labelName + " multi select dropdown", "Fail");
            }
            else {
                for (let arrayIndex = 0; arrayIndex < optionNames.length; arrayIndex++) {
                    for (let index = 0; index < elements.length; index++) {
                        let option = await elements[index].getText();
                        if (option.trim() == optionNames[arrayIndex].trim()) {
                            try {
                                // let status = await elements[index].isSelected();
                                let status = await verify.verifyElementVisible(by.css(this.multiDropDownSelectedStatus.replace("dynamic", String(index + 1))));
                                if (!status) {
                                    await elements[index].click();
                                }
                            } catch (error) {
                                await elements[index].click();
                            }
                            break;
                        }
                        else if (option.trim() != optionNames[arrayIndex].trim() && index == elements.length - 1) {
                            await action.ReportSubStep("selectMultiOptionsFromDropDown", "There is no option available with name " + optionNames[arrayIndex].trim() + " in " + labelName + " dropdown", "Fail");
                        }
                    }
                }
                // await action.Click(this.multiSelectClose, "Click On Close symbol");
                await action.ClickJavaScript(this.multiSelectClose, "Click On Close symbol");
                await browser.sleep(3000);
            }
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
     * desc: It is used to deselect the dropdown option based on option names if any one of the options is not available it will fail
     * @method selectDropDownValue
     * @author: Venkata Sivakumar
     * @param :{labelName:string,optionNames:Array<string>}
     * @return none
     */
    async unSelectMultiOptionsFromDropDown(labelName: string, optionNames: Array<string>, role: string = "") {
        try {
            if (role == "") {
                elementStatus = await verify.verifyElementVisible(by.css(this.multiSelectDropDown.replace("dynamic", labelName.trim())));
                if (elementStatus) {
                    await action.Click(by.css(this.multiSelectDropDown.replace("dynamic", labelName.trim())), "Click On " + labelName + " dropdown");
                }
                else {
                    await action.ReportSubStep("unSelectMultiOptionsFromDropDown", by.css(this.multiSelectDropDown.replace("dynamic", labelName.trim())) + " locator is not available", "Fail");
                }
            }
            else {
                elementStatus = await verify.verifyElementVisible(by.css(this.multiSelectDropDownInPopup.replace("dynamic", labelName.trim())));
                if (elementStatus) {
                    await action.Click(by.css(this.multiSelectDropDownInPopup.replace("dynamic", labelName.trim())), "Click On " + labelName + " dropdown");
                }
                else {
                    await action.ReportSubStep("unSelectMultiOptionsFromDropDown", by.css(this.multiSelectDropDownInPopup.replace("dynamic", labelName.trim())) + " locator is not available", "Fail");
                }
            }
            await browser.sleep(1000);
            await verify.verifyElementIsDisplayed(this.multiSelectDropDownOptions, "Verify Multi Select Dropdown is expanded");
            let elements = await action.getWebElements(this.multiSelectDropDownOptions);
            if (elements.length == 0) {
                await action.ReportSubStep("unSelectMultiOptionsFromDropDown", "There are no options available in " + labelName + " multi select dropdown", "Fail");
            }
            else {
                for (let arrayIndex = 0; arrayIndex < optionNames.length; arrayIndex++) {
                    for (let index = 0; index < elements.length; index++) {
                        let option = await elements[index].getText();
                        if (option.trim() == optionNames[arrayIndex].trim()) {
                            // let status = await elements[index].isSelected();
                            let status = await verify.verifyElementVisible(by.css(this.multiDropDownSelectedStatus.replace("dynamic", String(index + 1))));
                            if (status) {
                                await elements[index].click();
                            }
                            break;
                        }
                        else if (option.trim() != optionNames[arrayIndex].trim() && index == elements.length - 1) {
                            await action.ReportSubStep("unSelectMultiOptionsFromDropDown", "There is no option available with name " + optionNames[arrayIndex].trim() + " in " + labelName + " dropdown", "Fail");
                        }
                    }
                }
                // await action.Click(this.multiSelectClose, "Click On Close symbol");
                await action.ClickJavaScript(this.multiSelectClose, "Click On Close symbol");
                await browser.sleep(1000);
            }
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
         * desc: It is used to click on Add button
         * @method clickOnAddButton
         * @author: Venkata Sivakumar
         * @param :{}
         * @return none
         */
    async clickOnAddButton() {
        try {
            await verify.verifyElementIsDisplayed(this.addButton, "Verify Add Button is displayed");
            await action.Click(this.addButton, "Click On Add Button");
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
     * desc: verify Popup Message
     * @method verifyPopupMessage
     * @author: Venkata Sivakumar
     * @param :{msgText: string}
     * @return none
     */
    async verifyPopupMessage(msgText: string) {

        try {
            let locMessage = by.css('div[class*="ui-toast-detail"]');
            await verify.verifyTextInElement(locMessage, msgText, "alret msg");
        }
        catch (err) {
            throw new Error(err);
        }
    }

    /**
* desc: It returns the column index based on the header name provided if header name is available otherwise it will fail the test script
* @method getTableColumnIndex
* @author: Venkata Sivakumar
* @param :{columnName: string}
* @return index
*/
    async getTableColumnIndex(columnName: string) {
        try {
            let elements: Array<WebElement> = await element.all(this.tableHeader).getWebElements();
            let size = await elements.length;
            for (let index = 0; index < size; index++) {
                await action.scrollToToWebElement(elements[index]);
                let actualColumnHeader = await elements[index].getText();
                if (actualColumnHeader.trim() == columnName.trim()) {
                    return (Number(index) + 1);
                }
                else if (actualColumnHeader.trim() != columnName.trim() && index == (size - 1)) {
                    console.log("There is no Column with name:" + columnName + " in the table");
                    await action.ReportSubStep("Retrieve Column Index From the table", columnName.trim() + " column is not available in the table", 'Fail');
                }
            }
        } catch (error) {
            console.log("Exception raised in getTableColumnIndex method:" + error);
            throw new Error(error);
        }
    }

    /**
     * desc: It is used to validate sorting functionality for headers in the grid table and returns true for successfull sorting otherwise false
     * @method validateSortOrderInGridTable
     * @author: Venkata Sivakumar
     * @param :{headerNames: Array<any>, sortOrders: Array<any>}
     * @return : Boolean
     */

    async validateSortOrderInGridTable(headerNames: Array<any>, sortOrders: Array<any>) {
        try {
            let headerName;
            let failedCount;
            let actualColumnValues: Array<any>;
            let expectedValues: Array<any>
            sortingErrorMessage = "";
            for (let i = 0; i < headerNames.length; i++) {
                failedCount = 0;
                expectedValues = new Array();
                expectedValues = await this.getTableColumnValues(headerNames[i]);
                let columnIndex = await this.getTableColumnIndex(headerNames[i]);
                let ascendingOrderValues;
                if (headerNames[i].trim() == "Rate") {
                    ascendingOrderValues = expectedValues.sort((n1, n2) => n1 - n2);
                }
                else if (headerNames[i].trim() == "Created Date" || headerNames[i].trim() == "Modified Date" || headerNames[i].trim() == "Last Modified Date") {
                    ascendingOrderValues = expectedValues.sort((n1, n2) => new Date(n1).getTime() - new Date(n2).getTime());
                }
                else {
                    ascendingOrderValues = expectedValues.sort();
                }
                let headerClickCount = 0;
                headerName = headerNames[i];
                for (let sortIndex = 0; sortIndex < sortOrders.length; sortIndex++) {
                    actualColumnValues = new Array();
                    if (String(sortOrders[sortIndex]).toLowerCase().replace(" ", "").trim() == "ascending") {
                        let element: WebElement = await browser.findElement(by.xpath("//div[contains(@class,'ui-table-unfrozen-view')]//table/thead/tr/th[@tabindex][" + columnIndex + "]"));
                        await action.scrollToToWebElement(element);
                        await action.Click(by.xpath("//div[contains(@class,'ui-table-unfrozen-view')]//table/thead/tr/th[@tabindex][" + columnIndex + "]"), "Click On header " + headerNames[i] + " to ascending");
                        await browser.sleep(2000);
                        headerClickCount = headerClickCount + 1;
                        actualColumnValues = await this.getTableColumnValues(headerNames[i]);
                        for (let index = 0; index < ascendingOrderValues.length; index++) {
                            if (ascendingOrderValues[index] != actualColumnValues[index]) {
                                failedCount = failedCount + 1;
                            }
                        }
                        await browser.sleep(2000);
                    }
                    else if (String(sortOrders[sortIndex]).toLowerCase().replace(" ", "").trim() == "descending") {
                        let descendingOrderValues = ascendingOrderValues.reverse();
                        if (headerClickCount == 0) {
                            let element: WebElement = await browser.findElement(by.xpath("//div[contains(@class,'ui-table-unfrozen-view')]//table/thead/tr/th[@tabindex][" + columnIndex + "]"));
                            await action.scrollToToWebElement(element);
                            await action.Click(by.xpath("//div[contains(@class,'ui-table-unfrozen-view')]//table/thead/tr/th[@tabindex][" + columnIndex + "]"), "Click On header " + headerNames[i] + " to ascending");
                            await browser.sleep(1000);
                            await action.Click(by.xpath("//div[contains(@class,'ui-table-unfrozen-view')]//table/thead/tr/th[@tabindex][" + columnIndex + "]"), "Click On header " + headerNames[i] + " to descending");
                        }
                        else {
                            await action.Click(by.xpath("//div[contains(@class,'ui-table-unfrozen-view')]//table/thead/tr/th[@tabindex][" + columnIndex + "]"), "Click On header " + headerNames[i] + " to descending");
                        }
                        actualColumnValues = await this.getTableColumnValues(headerNames[i]);
                        for (let index = 0; index < descendingOrderValues.length; index++) {
                            if (descendingOrderValues[index] != actualColumnValues[index]) {
                                failedCount = failedCount + 1;
                                headerName = headerNames[i];
                            }
                        }
                        await browser.sleep(2000);
                    }
                }
                if (failedCount != 0) {
                    sortingErrorMessage = sortingErrorMessage + headerName.trim() + " ";
                }
            }
            if (failedCount == 0) {
                return true;
            }
            else {
                return false;
            }
        } catch (error) {
            throw new Error(error)
        }
    }

    /**
      * desc: It is used to retrieve the  column values from the table based on the table header name provided and it returns the table columns values in the form of Array
      * @method getTableColumnValues
      * @author: Venkata Sivakumar
      * @param :{headerNames: string}
      * @return : columnValues
      */
    async getTableColumnValues(headerName: string) {
        try {
            await browser.sleep(2000);
            let status: Boolean = await verify.verifyElementVisible(this.paginationActiveFirstInGrid);
            if (status) {
                await action.Click(this.paginationActiveFirstInGrid, "Click On First Pagination");
                await browser.sleep(2000);
                await verify.verifyElementIsDisplayed(this.paginationActiveFirstInGrid, "Verify Paginator first is disabled");
            }
            status = await verify.verifyElementVisible(this.paginationActiveLastInGrid);
            if (status) {
                await action.Click(this.paginationActiveLastInGrid, "Click On Last Pagination");
                await browser.sleep(2000);
                await verify.verifyElementIsDisplayed(this.paginationInactiveLastInGrid, "Verify Paginator Last is disabled");
            }
            let numberOfPages = await action.GetText(this.activePaginationInGrid, "Retieve the pagination Count");
            numberOfPages = Number(numberOfPages)
            if (status) {
                await action.Click(this.paginationActiveFirstInGrid, "");
                await browser.sleep(2000);
                await verify.verifyElementIsDisplayed(this.paginationInactiveFirstInGrid, "Verify Paginator First is disabled");
            }
            let columnIndex = await this.getTableColumnIndex(headerName);
            let columnValues: Array<any> = new Array<any>();
            for (let index = 1; index <= numberOfPages; index++) {
                let rowsElements = await element.all(this.rowCount).getWebElements();
                let rowsCount = await rowsElements.length;
                for (let rowIndex = 1; rowIndex <= rowsCount; rowIndex++) {
                    let element: WebElement = await browser.findElement(by.xpath(this.gridCellData.replace("rowdynamic", String(rowIndex)).replace("columndynamic", String(columnIndex))));
                    await action.scrollToToWebElement(element);
                    let actualCellData = await action.GetText(by.xpath(this.gridCellData.replace("rowdynamic", String(rowIndex)).replace("columndynamic", String(columnIndex))), "Retrieve the cell data");
                    if (String(actualCellData).trim() != "") {
                        if (headerName.trim() == "Rate") {
                            columnValues.push(String(actualCellData).trim().replace("$", ""));
                        }
                        else if (headerName.trim() == "Created Date" || headerName.trim() == "Modified Date" || headerName.trim() == "Last Modified Date") {
                            if (String(actualCellData).trim().indexOf(",") > 0) {
                                columnValues.push(new Date(String(actualCellData).trim().split(",")[0] + String(actualCellData).trim().split(",")[1].replace("AM", "").replace("PM", "")));
                            }
                            else {
                                columnValues.push(new Date(String(actualCellData).trim()));
                            }
                        }
                        else {
                            columnValues.push(String(actualCellData).trim());
                        }
                    }
                }
                if (index != numberOfPages) {
                    await action.Click(this.paginationActiveNextInGrid, "Click on Next pagination");
                    await browser.sleep(2000);
                    await verify.verifyElementIsDisplayed(by.xpath(this.activePageNumber.replace("dynamic", String(index + 1))), "Verify " + String(index + 1) + " page is active");
                }
            }
            if (status) {
                await action.Click(this.paginationActiveFirstInGrid, "Click On First pagination");
                await browser.sleep(2000);
                await verify.verifyElementIsDisplayed(this.paginationInactiveFirstInGrid, "Verify Paginator First is disabled");
            }
            if (columnValues.length == 0) {
                columnValues[0] = "";
                return columnValues;
            }
            else {
                return columnValues;
            }
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
* desc: To Erase Multi select Dropdown options
* @method deSelectAllMultiDropdownOptions
* @author: Venkata Sivakumar
* @param :{labelName: string,role: string = ""}
* @return none
*/
    async deSelectAllMultiDropdownOptions(labelName: string, role: string = "") {
        try {
            if (role == "") {
                elementStatus = await verify.verifyElementVisible(by.css(this.multiSelectDropDown.replace("dynamic", labelName.trim())));
                if (elementStatus) {
                    await action.Click(by.css(this.multiSelectDropDown.replace("dynamic", labelName.trim())), "Click On " + labelName + " dropdown");
                }
                else {
                    await action.ReportSubStep("deSelectAllMultiDropdownOptions", by.css(this.multiSelectDropDown.replace("dynamic", labelName.trim())) + " locator is not available", "Fail");
                }
            }
            else {
                elementStatus = await verify.verifyElementVisible(by.css(this.multiSelectDropDownInPopup.replace("dynamic", labelName.trim())));
                if (elementStatus) {
                    await action.Click(by.css(this.multiSelectDropDownInPopup.replace("dynamic", labelName.trim())), "Click On " + labelName + " dropdown");
                }
                else {
                    await action.ReportSubStep("deSelectAllMultiDropdownOptions", by.css(this.multiSelectDropDownInPopup.replace("dynamic", labelName.trim())) + " locator is not available", "Fail");
                }
            }
            await browser.sleep(1000);
            await verify.verifyElementIsDisplayed(this.multiSelectDropDownOptions, "Verify Multi Select Dropdown is expanded");
            let elements = await action.getWebElements(this.multiSelectDropDownOptions);
            if (elements.length == 0) {
                await action.ReportSubStep("deSelectAllMultiDropdownOptions", "There are no options available in " + labelName + " multi select dropdown", "Fail");
            }
            else {
                let checkBoxActiveStatus: boolean = await verify.verifyElementVisible(this.multiSelectDropDownHeaderCheckBoxActive);
                let checkBoxStatus: boolean = await verify.verifyElementVisible(this.multiSelectDropDownHeaderCheckBox);
                if (checkBoxActiveStatus) {
                    await action.Click(this.multiSelectDropDownHeaderCheckBoxActive, "unselect the checkbox");
                    await verify.verifyElementIsDisplayed(this.multiSelectDropDownHeaderCheckBox, "verify checkbox is unselected");
                }
                else if (checkBoxStatus) {
                    await action.Click(this.multiSelectDropDownHeaderCheckBox, "select all checkbox");
                    await verify.verifyElementIsDisplayed(this.multiSelectDropDownHeaderCheckBoxActive, "verify select all checkbox is selected");
                    await action.Click(this.multiSelectDropDownHeaderCheckBoxActive, "unselect the checkbox");
                    await verify.verifyElementIsDisplayed(this.multiSelectDropDownHeaderCheckBox, "verify checkbox is unselected");
                }
                else if (checkBoxActiveStatus == false && checkBoxStatus == false) {
                    await action.ReportSubStep("deSelectAllMultiDropdownOptions", this.multiSelectDropDownHeaderCheckBox + " and " + this.multiSelectDropDownHeaderCheckBoxActive + " Locators are not found", "Fail");
                }
                await action.Click(this.multiSelectClose, "Click On Close symbol");
                await browser.sleep(1000);
            }
        } catch (error) {
            throw new Error(error);
        }
    }




    /**
    * desc: To remove characters from textbox/textarea character by character
    * @method clearTextUsingBackSpace
    * @author: Venkata Sivakumar
    * @param :{text: string, locator: By}
    * @return none
    */
    async clearTextUsingBackSpace(text: string, locator: By) {
        try {
            // await action.Click(locator,"");

            await verify.verifyElementIsDisplayed(locator, "");


            for (let index = 0; index < String(text).length; index++) {
                await element(locator).sendKeys(protractor.Key.BACK_SPACE);
                await browser.sleep(1000);
            }
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
 * desc: used to get dropdown items into Array format
 * @method getDropDownItems
 * @author: Venkata Sivakumar
 * @param :{labelName:string}
 * @return dropDownItems
 */
    async getDropDownItems(labelName: string) {
        try {
            let dropDownItems: Array<any> = new Array<any>();
            elementStatus = await verify.verifyElementVisible(by.css(this.dropDown.replace("dynamic", labelName.trim())));
            if (elementStatus) {
                await action.Click(by.css(this.dropDown.replace("dynamic", labelName.trim())), "Click On " + labelName + " dropdown");
                await verify.verifyElementIsDisplayed(this.dropDownOptions, "Dropdown is expanded");
                let elements = await action.getWebElements(this.dropDownOptions);
                if (elements.length == 0) {
                    await action.ReportSubStep("getDropDownItems", "There are no options available in " + labelName + " dropdown", "Fail");
                }
                else {
                    for (let index = 0; index < elements.length; index++) {
                        let option = await elements[index].getText();
                        dropDownItems.push(String(option).trim());
                    }
                    await action.Click(by.css(this.dropDown.replace("dynamic", labelName.trim())), "Click On " + labelName + " dropdown");
                }
                return dropDownItems;
            }
            else {
                await action.ReportSubStep("getDropDownItems", by.css(this.dropDown.replace("dynamic", labelName.trim())) + " locator is not found", "Fail");
            }

        } catch (error) {
            throw new Error(error);
        }
    }

    /**
 * desc: It is used to click on submenu link
 * @method clickOnSubMenuLink
 * @author: Venkata Sivakumar
 * @param :{mainLinkName:string,subMenuLinkName:string = ""}
 * @return none
 */
    async clickOnMenuLink(mainLinkName: string, subMenuLinkName: string = "") {
        try {
            if (subMenuLinkName.trim() == "") {
                await action.Click(by.xpath(this.sideMenuLink.replace("dynamic", mainLinkName.trim())), "Click On " + mainLinkName)
            }
            else {
                let status: boolean = await verify.verifyElementVisible(by.xpath(this.sideMenuLink.replace("dynamic", subMenuLinkName.trim())));
                if (!status) {
                    await action.Click(by.xpath(this.sideMenuLink.replace("dynamic", mainLinkName.trim())), "Click On " + mainLinkName);
                }
            }
        } catch (error) {
            throw new Error(error)
        }
    }

    /**
* desc: It is used to search and select the multi select dropdown option based on option names if any one of the option is not available it will fail
* @method selectMultiOptionsFromDropDownUsingSearch
* @author: Venkata Sivakumar
* @param :{locator: By, optionNames: Array<string>}
* @return none
*/

    async selectMultiOptionsFromDropDownUsingSearch(locator: By, optionNames: Array<string>) {
        try {
            elementStatus = await verify.verifyElementVisible(locator);
            if (elementStatus) {
                await action.Click(locator, "Click On multi select dropdown");
            }
            else {
                await action.ReportSubStep("selectMultiOptionsFromDropDownUsingSearch", locator + " locator is not available", "Fail");
            }
            await browser.sleep(1000);
            await verify.verifyElementIsDisplayed(this.multiSelectDropDownOptions, "Verify Multi Select Dropdown is expanded");
            let elements = await action.getWebElements(this.multiSelectDropDownOptions);
            if (elements.length == 0) {
                await action.ReportSubStep("selectMultiOptionsFromDropDownUsingSearch", "There are no options available", "Fail");
            }
            else {
                for (let arrayIndex = 0; arrayIndex < optionNames.length; arrayIndex++) {
                    for (let index = 0; index < elements.length; index++) {
                        let option = await elements[index].getText();
                        if (option.trim() == optionNames[arrayIndex].trim()) {
                            await action.ClearText(this.multiSelectSearchInput, "Clear test in Search text box");
                            await action.SetText(this.multiSelectSearchInput, optionNames[arrayIndex].trim(), "Search Filter");
                            let status = await verify.verifyElementVisible(by.css(this.multiDropDownSelectedStatus.replace("dynamic", String(index + 1))));
                            if (!status) {
                                await elements[index].click();
                            }
                            break;
                        }
                        else if (option.trim() != optionNames[arrayIndex].trim() && index == elements.length - 1) {
                            await action.ReportSubStep("selectMultiOptionsFromDropDownUsingSearch", "There is no option available with name " + optionNames[arrayIndex].trim() + " in the dropdown", "Fail");
                        }
                    }
                }
                await action.ClickJavaScript(this.multiSelectClose, "Click On Close symbol");
                await browser.sleep(1000);
            }
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
    * desc: It is used to search and deselect the multi select dropdown option based on option names if any one of the options is not available it will fail
    * @method unSelectMultiOptionsFromDropDownUsingSearch
    * @author: Venkata Sivakumar
    * @param :{locator: By, optionNames: Array<string>}
    * @return none
    */
    async unSelectMultiOptionsFromDropDownUsingSearch(locator: any, optionNames: Array<string>) {
        try {

            elementStatus = await verify.verifyElementVisible(locator);
            if (elementStatus) {
                await action.Click(locator, "Click On multi select dropdown");
            }
            else {
                await action.ReportSubStep("selectMultiOptionsFromDropDownUsingSearch", locator + " locator is not available", "Fail");
            }
            await browser.sleep(1000);
            await verify.verifyElementIsDisplayed(this.multiSelectDropDownOptions, "Verify Multi Select Dropdown is expanded");
            let elements = await action.getWebElements(this.multiSelectDropDownOptions);
            if (elements.length == 0) {
                await action.ReportSubStep("unSelectMultiOptionsFromDropDownUsingSearch", "There are no options available in multi select dropdown", "Fail");
            }
            else {
                for (let arrayIndex = 0; arrayIndex < optionNames.length; arrayIndex++) {
                    for (let index = 0; index < elements.length; index++) {
                        let option = await elements[index].getText();
                        if (option.trim() == optionNames[arrayIndex].trim()) {
                            await action.ClearText(this.multiSelectSearchInput, "Clear test in Search text box");
                            await action.SetText(this.multiSelectSearchInput, optionNames[arrayIndex].trim(), "Search Filter");
                            let status = await verify.verifyElementVisible(by.css(this.multiDropDownSelectedStatus.replace("dynamic", String(index + 1))));
                            if (status) {
                                await action.scrollToToWebElement(await elements[index]);
                                await elements[index].click();
                            }
                            break;
                        }
                        else if (option.trim() != optionNames[arrayIndex].trim() && index == elements.length - 1) {
                            await action.ReportSubStep("unSelectMultiOptionsFromDropDownUsingSearch", "There is no option available with name " + optionNames[arrayIndex].trim() + " in the multiselect dropdown", "Fail");
                        }
                    }
                }
                await action.ClickJavaScript(this.multiSelectClose, "Click On Close symbol");
                await browser.sleep(1000);
            }
        } catch (error) {
            throw new Error(error);
        }
    }


}