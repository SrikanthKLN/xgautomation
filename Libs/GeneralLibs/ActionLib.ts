import { browser, element, by, $, $$, ExpectedConditions, protractor, ProtractorBy, ElementFinder, WebElement } from 'protractor';
import { Gutils } from '../../Utility/gutils';
import { Logger } from '../../Utility/logger';
import { globalvalues } from '../../Utility/globalvalue';
import { VerifyLib } from './VerificationLib';
import { By } from 'selenium-webdriver';


let logger = new Logger();
let gUtility = new Gutils();
let verify = new VerifyLib();

export class ActionLib {
    /**
  * desc: Clear Text from Input  
  * @method ClearText
  * @author: vikas
  * @param :{locator ,log:name of the element}
  * @return none
  */

    ClearText(locator, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.elementToBeClickable(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    _getElement.clear().then(function () {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "ClearText " + locator + " " + log + " ---PASS");
                        resolve("pass");
                    }, function (err) {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "ClearText " + locator + " " + log + " ---FAIL " + err);
                        logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "ClearText " + locator + " " + log + " ---FAIL " + err);
                        console.log("!!! FAIL -- FAIL --- ClearText --" + locator + "--- element:: " + log);
                        reject(err);
                    });

                }, function (err) {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "ClearText " + locator + " " + log + " ---ERROR " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "ClearText " + locator + " " + log + " ---ERROR " + err);
                    console.log("!!! FAIL -- FAIL --- elementToBeClickable ClearText --" + locator + "--- element:: " + log + " " + err);
                    reject(err);
                });

            });
        });
    }
    /**
   * desc: enter  Text in Input  element
   * @method SetText
   * @author: vikas
   * @param :{locator ,txtToEnter:text to enter ,log:name of the element}
   * @return none
   */
    SetText(locator, txtToEnter: string, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.elementToBeClickable(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    _getElement.sendKeys(txtToEnter).then(function () {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "SetText " + locator + " " + log + " ---PASS");
                        resolve("pass");
                    }, function (err) {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "SetText " + locator + " " + log + " ---FAIL " + err);
                        logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "SetText " + locator + " " + log + " ---FAIL " + err);
                        console.log("!!! FAIL -- FAIL --- SetText --" + locator + "--- element:: " + log);
                        reject(err);
                    });

                }, function (err) {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "SetText " + locator + " " + log + " ---ERROR " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "SetText " + locator + " " + log + " ---ERROR " + err);
                    console.log("!!! FAIL -- FAIL ---elementToBeClickable  SetText --" + locator + "--- element:: " + log + " " + err);
                    reject(err);
                });
            });
        });
    }

    /**
   * desc: click on element 
   * @method Click
   * @author: vikas
   * @param :{locator,log:name of the element}
   * @return none
   */
    Click(locator, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.elementToBeClickable(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    _getElement.click().then(function () {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "Click " + locator + " " + log + " ---PASS");
                        resolve("pass");
                    }, function (err) {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "Click " + locator + " " + log + " ---FAIL " + err);
                        logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "Click " + locator + " " + log + " ---FAIL " + err);
                        console.log("!!! FAIL -- FAIL ---Click --" + locator + "--- element:: " + log);
                        reject(err);
                    });

                }, function (err) {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "Click " + locator + " " + log + " ---ERROR " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "Click " + locator + " " + log + " ---ERROR " + err);
                    console.log("!!! FAIL -- FAIL ---elementToBeClickable  Click --" + locator + "--- element:: " + log + " " + err);
                    reject(err);
                });

            });
        });
    }
    /**
  * desc: Get Text From Input Element
  * @method GetTextFromInput
  * @author: vikas
  * @param :{locator,log:name of the element}
  * @return none
  */
    GetTextFromInput(locator, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.visibilityOf(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    _getElement.getAttribute('value').then(function (value) {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "GetTextFromInput " + locator + " " + log + " ---PASS");
                        resolve(value);
                    }, function (err) {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "GetTextFromInput " + locator + " " + log + " ---FAIL " + err);
                        logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "GetTextFromInput " + locator + " " + log + " ---FAIL " + err);
                        console.log("!!! FAIL -- FAIL ---GetTextFromInput --" + locator + "--- element:: " + log);
                        reject(err);
                    });

                }, function (err) {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "GetTextFromInput " + locator + " " + log + " ---ERROR " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "GetTextFromInput " + locator + " " + log + " ---ERROR " + err);
                    console.log("!!! FAIL -- FAIL ---visibilityOf GetTextFromInput --" + locator + "--- element:: " + log + " " + err);
                    reject(err);
                });

            });
        });
    }
    /**
   * desc: Get Text from Element
   * @method GetTextFromInput
   * @author: vikas
   * @param :{locator,log:name of the element}
   * @return none
   */
    GetText(locator, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.visibilityOf(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    _getElement.getText().then(function (value) {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "GetText " + locator + " " + log + " ---PASS");
                        resolve(value);
                    }, function (err) {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "GetText " + locator + " " + log + " ---FAIL " + err);
                        logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "GetText " + locator + " " + log + " ---FAIL " + err);
                        console.log("!!! FAIL -- FAIL ---GetText --" + locator + "--- element:: " + log);
                        reject(err);
                    });

                }, function (err) {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "GetText " + locator + " " + log + " ---ERROR " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "GetText " + locator + " " + log + " ---ERROR " + err);
                    console.log("!!! FAIL -- FAIL ---visibilityOf GetText --" + locator + "--- element:: " + log + " " + err);
                    reject(err);
                });

            });
        });
    }
    /**
   * desc: Mouser Move till given element /used for scroll as well
   * @method MouseMoveToElement
   * @author: vikas
   * @param :{locator,log:name of the element}
   * @return none
   */
    MouseMoveToElement(locator, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.presenceOf(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    browser.actions().mouseMove(_getElement).perform().then(function () {

                        resolve("pass");

                    }, function (err) {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "MouseMoveToElement " + locator + " " + log + " ---ERROR " + err);
                        logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "MouseMoveToElement " + locator + " " + log + " ---ERROR " + err);
                        console.log("!!! FAIL -- FAIL --- MouseMoveToElement --" + locator + "--- element::  " + log + " " + err);
                        reject(err);
                    });

                }, function (err) {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "MouseMoveToElement " + locator + " " + log + " ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "MouseMoveToElement " + locator + " " + log + " ---FAIL " + err);
                    console.log("!!! FAIL -- FAIL --- MouseMoveToElement presenceOf --" + locator + "--- element:: " + log + " " + err);
                    reject(err);
                });

            });
        });
    }
    /**
      * desc: get Element Attribute like value , class name,title etc.
      * @method getElementAttribute
      * @author: vikas
      * @param :{locator ,attributeName:value/class/titile,expectedvalue:value to verify log:name of the element}
      * @return none
      */
    getElementAttribute(locator: any, attributeName: any, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.presenceOf(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    _getElement.getAttribute(attributeName).then(function (value) {
                        resolve(value);
                    }, function (err) {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "getElementAttribute err " + locator + " " + log + " ---Fail " + err);
                        logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "getElementAttribute err " + locator + " " + log + " ---Fail " + err);
                        reject(err);
                    })
                }, function (err) {
                    console.log("!!! FAIL -- FAIL --- getElementAttribute --" + locator + "--- element:: " + log + " " + err);
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "getElementAttribute locator not Found" + locator + " " + log + " ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "getElementAttribute locator not Found" + locator + " " + log + " ---FAIL " + err);
                    reject(err);
                });
            });
        });

    }

    /**
        * desc: get Element CSS value like color ,back ground color,title etc.
        * @method getElementCSSvalue
        * @author: vikas
        * @param :{locator ,cssName:color/back ground color,expectedvalue:value to verify log:name of the element}
        * @return none
        */
    getElementCSSvalue(locator: any, cssName: any, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.presenceOf(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    _getElement.getCssValue(cssName).then(function (value) {
                        resolve(value);

                    }, function (err) {
                        logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "getElementCSSvalue err " + locator + " " + log + " ---Fail " + err);
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "getElementCSSvalue err " + locator + " " + log + " ---Fail " + err);
                        reject(err);
                    })
                }, function (err) {
                    console.log("!!! FAIL -- FAIL --- getElementCSSvalue --" + locator + "--- element:: " + log + " " + err);
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "getElementCSSvalue locator not Found" + locator + " " + log + " ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "getElementCSSvalue locator not Found" + locator + " " + log + " ---FAIL " + err);
                    reject(err);
                });
            });
        });

    }
    /**
     * desc: Mouser Move till given element(Java script function) /used for scroll as well
     * @method MouseMoveJavaScript
     * @author: vikas
     * @param :{locator,log:name of the element}
     * @return none
     */
    MouseMoveJavaScript(locator, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.presenceOf(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    browser.executeScript('arguments[0].scrollIntoView()', _getElement.getWebElement()).then(function () {
                        resolve("pass");
                    }, function (err) {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "MouseMoveJavaScript " + locator + " " + log + " ---ERROR " + err);
                        logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "MouseMoveJavaScript " + locator + " " + log + " ---ERROR " + err);
                        console.log("!!! FAIL -- FAIL --- MouseMoveJavaScript --" + locator + "--- element::  " + log + " " + err);
                        reject(err);
                    });

                }, function (err) {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "MouseMoveJavaScript " + locator + " " + log + " ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "MouseMoveJavaScript " + locator + " " + log + " ---FAIL " + err);
                    console.log("!!! FAIL -- FAIL --- MouseMoveJavaScript presenceOf --" + locator + "--- element:: " + log + " " + err);
                    reject(err);
                });
            });

        });
    }
    /**
     * desc: click on element java script function
     * @method ClickJavaScript
     * @author: vikas
     * @param :{locator,log:name of the element}
     * @return none
     */
    ClickJavaScript(locator, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.presenceOf(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    browser.executeScript('arguments[0].click()', _getElement.getWebElement()).then(function () {
                        resolve("pass");
                    }, function (err) {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "ClickJavaScript " + locator + " " + log + " ---ERROR " + err);
                        logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "ClickJavaScript " + locator + " " + log + " ---ERROR " + err);
                        console.log("!!! FAIL -- FAIL --- ClickJavaScript --" + locator + "--- element::  " + log + " " + err);
                        reject(err);
                    });

                }, function (err) {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "ClickJavaScript " + locator + " " + log + " ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "ClickJavaScript " + locator + " " + log + " ---FAIL " + err);
                    console.log("!!! FAIL -- FAIL --- ClickJavaScript presenceOf --" + locator + "--- element:: " + log + " " + err);
                    reject(err);
                });
            });

        });
    }
    /**
     * desc: Switch To Frame
     * @method SwitchToFrame
     * @author: vikas
     * @param :{frm:number of the frame starting from 0}
     * @return none
     */
    SwitchToFrame(frm: number) {
        return new Promise((resolve, reject) => {
            browser.waitForAngular().then(function () {
                var EC = protractor.ExpectedConditions;
                browser.wait(EC.presenceOf(element(by.tagName("iframe"))), gUtility.mediumDynamicWait()).then(function () {
                    browser.driver.switchTo().frame(frm).then(function () {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "SwitchToFrame " + frm + " ---PASS");
                        resolve("pass");
                    }, function (err) {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "SwitchToFrame " + frm + " ---FAIL " + err);
                        logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "SwitchToFrame " + frm + " ---FAIL " + err);
                        console.log("!!! FAIL -- FAIL ---SwitchToFrame --- element:: " + frm);
                        reject(err);
                    });

                }, function (err) {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "SwitchToFrame " + frm + " ---ERROR " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "SwitchToFrame " + frm + " ---ERROR " + err);
                    console.log("!!! FAIL -- FAIL ---SwitchToFrame --- No Frame " + frm + " " + err);
                    reject(err);
                });
            });
        });
    }
    /**
     * desc: Switch To Default Frame
     * @method SwitchToDefaultFrame
     * @author: vikas
     * @param :{}
     * @return none
     */
    SwitchToDefaultFrame() {
        return new Promise((resolve, reject) => {
            browser.waitForAngular().then(function () {
                browser.driver.switchTo().defaultContent().then(function () {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "SwitchToDefaultFrame ---PASS");
                    resolve("pass");
                }, function (err) {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "SwitchToDefaultFrame ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "SwitchToDefaultFrame ---FAIL " + err);
                    console.log("!!! FAIL -- FAIL ---SwitchToDefaultFrame -- " + err);
                    reject(err);
                });
            });
        });
    }

    /**
     * desc: Report Sub Step - Reporting function at page level (not reporting to main result),Reporting at log
     * @method ReportSubStep
     * @author: vikas
     * @param :{stepName:function Name,description:description of step,status:PASS/FAIL}
     * @return none
     */
    ReportSubStep(stepName, description, status: string) {

        if ((status.toUpperCase().indexOf("FAIL") > -1)) {

            logger.Log(globalvalues.reportInstance.TestCaseName + "-----" + stepName + "--" + description + " ---FAIL");
            logger.LogFail(globalvalues.reportInstance.TestCaseName + "-----" + stepName + "--" + description + " ---FAIL");
            console.log(globalvalues.reportInstance.TestCaseName + "-----" + stepName + "--" + description + " ---FAIL");
            throw new Error(description);
        }
        else {
            logger.Log(globalvalues.reportInstance.TestCaseName + "--" + description + " ---PASS");
        }
    }
    /**
      * desc: make Dynamic Locator Contains Text from locator having css value
      * @method makeDynamicLocatorContainsText
      * @author: vikas
      * @param :{locator,textToFind:name of the element,containsOrEqual:CONTAINS/EQUAL}
      * @return none
      */
    makeDynamicLocatorContainsText(locator: any, textToFind: any, containsOrEqual: any) {
        return new Promise((resolve, reject) => {
            browser.waitForAngular().then(function () {
                let EC = protractor.ExpectedConditions;
                let flagout: boolean = false;

                let link = element.all(locator).reduce(function (result, elem, index) {
                    if (result) return result;
                    return browser.actions().mouseMove(elem).perform().then(function () {
                        return elem.getText().then(function (value) {
                            // console.log("--Text value "+value );

                            if (containsOrEqual.toUpperCase() == "CONTAINS") {
                                if (value.trim().indexOf(textToFind) > -1) {
                                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "makeDynamicLocatorContainsText ---PASS");
                                    return elem;


                                }
                            } else {
                                if (value.trim() == textToFind) {
                                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "makeDynamicLocatorContainsText ---PASS");
                                    return elem;


                                }
                            }

                        }, function (err) { console.log("--error  get text dynamic"); })
                    }, function (err) { console.log("--error  Mouse Move dynamic"); })

                }, flagout).then(function (element) {
                    resolve(element);
                })
            });
        });
    }


    /**
 * desc: Used to get webelements size
 * @method getWebElementsCount
 * @author: Venkata Sivakumar
 * @param :{locator: any}
 * @return none
 */
    async getWebElementsCount(locator: any) {
        try {
            await verify.verifyElementIsDisplayed(locator, "Verify Element is displayed")
            let webElements: Array<WebElement> = await browser.findElements(locator);
            return await webElements.length;
        } catch (error) {
            console.log("Exception raised in getWebElementsCount")
            return 0;
        }
    }

    /**
* desc: Returns required web elements
* @method getWebElements
* @author: Venkata Sivakumar
* @param :{locator: any}
* @return none
*/

    async getWebElements(locator: any) {
        try {
            await verify.verifyElementIsDisplayed(locator, "Verify Element is displayed")
            let webElements: Array<WebElement> = await browser.findElements(locator);
            return webElements;
        } catch (error) {
            return null;
        }
    }


    /**
* desc: Scroll to Top of the Page
* @method scrollToTopOfPage
* @author: Venkata Sivakumar
* @param :{locator: any}
* @return none
*/

    async scrollToTopOfPage() {
        try {
            await browser.executeScript("window.scrollTo(0, 0)");
        } catch (error) {
            throw new Error(error);
        }
    }


    /**
* desc: Scroll to bottom of page
* @method scrollToBottoOfPage
* @author: Venkata Sivakumar
* @param :{locator: any}
* @return none
*/
    async scrollToBottoOfPage() {
        try {
            await browser.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        } catch (error) {
            throw new Error(error);
        }
    }


    /**
* desc: Scroll to element
* @method scrollToToWebElement
* @author: Venkata Sivakumar
* @param :{webElement: WebElement}
* @return none
*/
    async scrollToToWebElement(webElement: WebElement) {
        try {
            await browser.executeScript("arguments[0].scrollIntoView()", webElement);
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
* desc: Used to wait until element is invisible based on given time
* @method waitForElementInvisible
* @author: Venkata Sivakumar
* @param :{locator: By, timeWait:Number}
* @return none
*/
async waitForElementInvisible(locator: By, timeWait: Number) {
    try {
        for (let index = 1; index <= timeWait; index++) {
            await browser.sleep(1000);
            let status = await verify.verifyElementVisible(locator);
            if (!status) break;
            else if (status && index == timeWait) {
                await this.ReportSubStep("waitForElementInvisible", locator + " is still visible on webpage after " + timeWait + " seconds timewait", "Fail");
            }
        }
    } catch (error) {
        throw new Error(error);
    }
}


}

