import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';
import { Gutils } from '../../Utility/gutils';
import { Logger } from '../../Utility/logger';
import { globalvalues } from '../../Utility/globalvalue';

let logger = new Logger();
let gUtility = new Gutils();

export class VerifyLib {

    /**
   * desc: verify Element is present (checks the presence)
   * @method verifyElement
   * @author: vikas
   * @param :{locator ,log:name of the element}
   * @return none
   */
    verifyElement(locator, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.presenceOf(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElement " + locator + " " + log + " ---PASS");

                    resolve("pass");
                }, function (err) {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElement " + locator + " " + log + " ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyElement " + locator + " " + log + " ---FAIL " + err);
                    console.log("!!! FAIL -- FAIL --- verifyElement --" + locator + "--- element:: " + log + " " + err);
                    reject(err);
                });
            });

        });
    }
    /**
  * desc: verify Element Is Displayed in Page (checks is vibility)
  * @method verifyElementIsDisplayed
  * @author: vikas
  * @param :{locator ,log:name of the element}
  * @return none
  */
    verifyElementIsDisplayed(locator, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.visibilityOf(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsDisplayed " + locator + " " + log + " ---PASS");
                    resolve("pass");
                }, function (err) {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsDisplayed " + locator + " " + log + " ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsDisplayed " + locator + " " + log + " ---FAIL " + err);
                    console.log("!!! FAIL -- FAIL ---verifyElementIsDisplayed --" + locator + "--- element:: " + log + " " + err);
                    reject(err);
                });
            });
        });
    }
    /**
 * desc: verify element is Not Present in Page (checks not present)
 * @method verifyNotPresent
 * @author: vikas
 * @param :{locator ,log:name of the element}
 * @return none
 */
    verifyNotPresent(locator, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.not(EC.presenceOf(_getElement)), gUtility.mediumDynamicWait()).then(() => {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyNotPresent " + locator + " " + log + " ---PASS");
                    resolve("pass");
                }, function (err) {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyNotPresent " + locator + " " + log + " ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyNotPresent " + locator + " " + log + " ---FAIL " + err);
                    console.log("!!! FAIL -- FAIL ---verifyNotPresent --" + locator + "--- element:: " + log + " " + err);
                    reject(err);
                });
            });
        });
    }

    /**
    * desc: verify Progress Bar Not Present in application
    * @method verifyProgressBarNotPresent
    * @author: vikas
    * @param :{}
    * @return none
    */
   verifyProgressBarNotPresent() {
    return new Promise((resolve, reject) => {
        let locator = by.xpath('//p-progressbar');
        let EC = protractor.ExpectedConditions;
        
        browser.waitForAngular().then(function () {
            browser.sleep(1000);
            browser.wait(EC.not(EC.presenceOf(element(locator))), 360000).then(() => {
                logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyProgressBarNotPresent " + locator + " " + "Progress bar grid" + " ---PASS");
                resolve("pass");
            }, function (err) {
                logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyProgressBarNotPresent " + locator + " " + err + " ---FAIL " + err);
                logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyProgressBarNotPresent " + locator + " " + err + " ---FAIL " + err);
                console.log("!!! FAIL -- FAIL ---verifyProgressBarNotPresent --" + locator + "--- element:: " + err);
                reject(err);
            });
        });
    });
}

    /**
    * desc: verify Progress Spinner Circle Not Present in application
    * @method verifyProgressSpinnerCircleNotPresent
    * @author: Adithya K
    * @param :{}
    * @return none
    */
    verifyProgressSpinnerCircleNotPresent() {
        return new Promise((resolve, reject) => {
            let locator = by.css('circle[class*="ui-progress-spinner-circle"]');
            let EC = protractor.ExpectedConditions;
            browser.waitForAngular().then(function () {
                browser.wait(EC.not(EC.presenceOf(element(locator))), 360000).then(() => {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyProgressSpinnerCircleNotPresent " + locator + " " + "Progress spinner circle in grid" + " ---PASS");
                    resolve("pass");
                }, function (err) {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyProgressSpinnerCircleNotPresent " + locator + " " + err + " ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyProgressSpinnerCircleNotPresent " + locator + " " + err + " ---FAIL " + err);
                    console.log("!!! FAIL -- FAIL ---verifyProgressSpinnerCircleNotPresent --" + locator + "--- element:: " + err);
                    reject(err);
                });
            });
        });
    }

    /**
   * desc: verify Text In Element (contains text)
   * @method verifyTextInElement
   * @author: vikas
   * @param :{locator ,expectedText:text to verify,log:name of the element}
   * @return none
   */
    verifyTextInElement(locator: any, expectedText: any, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.textToBePresentInElement(_getElement, expectedText), gUtility.mediumDynamicWait()).then(() => {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyTextInElement " + locator + " " + log + " ---PASS");
                    resolve("pass");
                }, function (err) {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyTextInElement " + locator + " " + expectedText + " " + log + " ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyTextInElement " + locator + " " + expectedText + " " + log + " ---FAIL " + err);
                    console.log("!!! FAIL -- FAIL ---verifyTextInElement --" + locator + "--text " + expectedText + "- element:: " + log + " " + err);
                    reject(err);
                });
            });
        });
    }

    /**
     * desc: verify Element Is Selected
     * @method verifyElementIsSelected
     * @author: vikas
     * @param :{locator ,log:name of the element}
     * @return none
     */
    verifyElementIsSelected(locator, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.presenceOf(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    _getElement.isSelected().then(function (value) {
                        if (value == true) {
                            logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsSelected " + locator + " " + log + " ---PASS");
                            resolve("pass");
                        }
                        else {
                            console.log("!!! FAIL -- FAIL ---verifyElementIsSelected --value " + value + "" + locator + "--- element:: " + log);
                            logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsSelected -- value is " + value + "" + locator + " " + log + " ---Fail");
                            logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsSelected -- value is " + value + "" + locator + " " + log + " ---Fail");
                            reject("Fail");
                        }
                    })


                }, function (err) {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsSelected " + locator + " " + log + " ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsSelected " + locator + " " + log + " ---FAIL " + err);
                    console.log("!!! FAIL -- FAIL ---verifyElementIsSelected --" + locator + "--- element:: Locator not Found " + log + " " + err);
                    reject(err);
                });
            });
        });
    }

    /**
    * desc: verify Element Is Not Selected
    * @method verifyElementIsNotSelected
    * @author: vikas
    * @param :{locator ,log:name of the element}
    * @return none
    */
    verifyElementIsNotSelected(locator, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.presenceOf(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    _getElement.isSelected().then(function (value) {
                        if (value == false) {
                            logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsNotSelected " + locator + " " + log + " ---PASS");
                            resolve("pass");
                        }
                        else {
                            console.log("!!! FAIL -- FAIL ---verifyElementIsNotSelected --value " + value + "" + locator + "--- element:: " + log);
                            logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsNotSelected -- value is " + value + "" + locator + " " + log + " ---Fail");
                            logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsNotSelected -- value is " + value + "" + locator + " " + log + " ---Fail");
                            reject("Fail");
                        }
                    })


                }, function (err) {
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsNotSelected " + locator + " " + log + " ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsNotSelected " + locator + " " + log + " ---FAIL " + err);
                    console.log("!!! FAIL -- FAIL ---verifyElementIsNotSelected --" + locator + "--- element:: Locator not Found " + log + " " + err);
                    reject(err);
                });
            });
        });
    }
    /**
     * desc: verify Element Is Enabled
     * @method verifyElementIsEnabled
     * @author: vikas
     * @param :{locator ,log:name of the element}
     * @return none
     */

    verifyElementIsEnabled(locator, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.presenceOf(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    _getElement.isEnabled().then(function (value) {
                        if (value == true) {
                            logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsEnabled " + locator + " " + log + " ---PASS");
                            resolve("PASS");
                        }
                        else {
                            console.log("!!! FAIL -- FAIL ---verifyElementIsEnabled element is not enable--" + locator + "--- element:: " + log);
                            logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsEnabled" + locator + " " + log + " ---FAIL element is not enabled");
                            logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsEnabled" + locator + " " + log + " ---FAIL element is not enabled");
                            reject("Fail");
                        }

                    }, function (err) {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsEnabled " + locator + " " + log + " ---FAIL " + err);
                        logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsEnabled " + locator + " " + log + " ---FAIL " + err);
                        reject(err);
                    })
                }, function (err) {
                    console.log("!!! FAIL -- FAIL ---verifyElementIsEnabled --" + locator + "--- element:: " + log + " " + err);
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsEnabled" + locator + " " + log + " ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsEnabled" + locator + " " + log + " ---FAIL " + err);
                    reject(err);
                });
            });
        });

    }

    /**
     * desc: verify Element Is Disabled
     * @method verifyElementIsDisabled
     * @author: vikas
     * @param :{locator ,log:name of the element}
     * @return none
     */
    verifyElementIsDisabled(locator, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.presenceOf(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    _getElement.isEnabled().then(function (value) {
                        if (value == false) {
                            logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsDisabled " + locator + " " + log + " ---PASS");
                            resolve("PASS");
                        }
                        else {
                            console.log("!!! FAIL -- FAIL ---verifyElementIsDisabled element is not disabled --" + locator + "--- element:: " + log);
                            logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsDisabled" + locator + " " + log + " ---FAIL element is not disabled");
                            logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsDisabled" + locator + " " + log + " ---FAIL element is not disabled");
                            reject("Fail");
                        }

                    }, function (err) {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsDisabled " + locator + " " + log + " ---FAIL " + err);
                        reject(err);
                    })
                }, function (err) {
                    console.log("!!! FAIL -- FAIL ---verifyElementIsDisabled --" + locator + "--- element:: " + log + " " + err);
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsDisabled" + locator + " " + log + " ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementIsDisabled" + locator + " " + log + " ---FAIL " + err);
                    reject(err);
                });
            });
        });

    }
    /**
      * desc: verify Element Attribute like value , class name,title etc.
      * @method verifyElementAttribute
      * @author: vikas
      * @param :{locator ,attributeName:value/class/titile,expectedvalue:value to verify log:name of the element}
      * @return none
      */
    verifyElementAttribute(locator: any, attributeName: any, expectedvalue: any, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.presenceOf(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    _getElement.getAttribute(attributeName).then(function (value) {
                        if (value.trim().indexOf(expectedvalue.trim()) > -1) {
                            logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementAttribute " + locator + " " + log + " ---PASS");
                            resolve("PASS");
                        }
                        else {
                            console.log("!!! FAIL -- FAIL ---verifyElementAttribute attribute actual " + value + " --" + locator + "--- element:: " + log);
                            logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementAttribute" + locator + " " + log + " ---FAIL attribute actual " + value);
                            logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementAttribute" + locator + " " + log + " ---FAIL attribute actual " + value);
                            reject("Fail");
                        }

                    }, function (err) {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementAttribute err " + locator + " " + log + " ---Fail " + err);
                        logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementAttribute err " + locator + " " + log + " ---Fail " + err);
                        reject(err);
                    })
                }, function (err) {
                    console.log("!!! FAIL -- FAIL --- verifyElementAttribute --" + locator + "--- element:: " + log + " " + err);
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementAttribute locator not Found" + locator + " " + log + " ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementAttribute locator not Found" + locator + " " + log + " ---FAIL " + err);
                    reject(err);
                });
            });
        });

    }

    /**
      * desc: verify Element CSS value like color ,back ground color,title etc.
      * @method verifyElementCSSvalue
      * @author: vikas
      * @param :{locator ,cssName:color/back ground color,expectedvalue:value to verify log:name of the element}
      * @return none
      */
    verifyElementCSSvalue(locator: any, cssName: any, expectedvalue: any, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.presenceOf(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    _getElement.getCssValue(cssName).then(function (value) {
                        if (value.trim().indexOf(expectedvalue.trim()) > -1) {
                            logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementCSSvalue " + locator + " " + log + " ---PASS");
                            resolve("PASS");
                        }
                        else {
                            console.log("!!! FAIL -- FAIL ---verifyElementCSSvalue CSS Value actual " + value + " --" + locator + "--- element:: " + log);
                            logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementCSSvalue" + locator + " " + log + " ---FAIL CSS Value actual " + value);
                            logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementCSSvalue" + locator + " " + log + " ---FAIL CSS Value actual " + value);
                            reject("Fail");
                        }

                    }, function (err) {
                        logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementCSSvalue err " + locator + " " + log + " ---Fail " + err);
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementCSSvalue err " + locator + " " + log + " ---Fail " + err);
                        reject(err);
                    })
                }, function (err) {
                    console.log("!!! FAIL -- FAIL --- verifyElementCSSvalue --" + locator + "--- element:: " + log + " " + err);
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementCSSvalue locator not Found" + locator + " " + log + " ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyElementCSSvalue locator not Found" + locator + " " + log + " ---FAIL " + err);
                    reject(err);
                });
            });
        });

    }

    /**
      * desc: verify Text Of Element
      * @method verifyTextOfElement
      * @author: vikas
      * @param :{locator,expectedvalue:value to verify log:name of the element}
      * @return none
      */
    verifyTextOfElement(locator: any, expectedvalue: any, log: string) {
        return new Promise((resolve, reject) => {
            let EC = protractor.ExpectedConditions;
            let _getElement;
            if (String(locator.constructor.name).toLowerCase().indexOf("elementfinder") > -1) {
                _getElement = locator;
            }
            else {
                _getElement = element(locator);
            }
            browser.waitForAngular().then(function () {
                browser.wait(EC.presenceOf(_getElement), gUtility.mediumDynamicWait()).then(() => {
                    _getElement.getText().then(function (value) {
                        try {
                            if (value.trim().indexOf(expectedvalue.trim()) > -1) {
                                logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyTextOfElement " + locator + " " + log + " ---PASS");
                                resolve("PASS");
                            }
                            else {
                                console.log("!!! FAIL -- FAIL ---verifyTextOfElement text actual " + value + " --" + locator + "--- element:: " + log);
                                logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyTextOfElement" + locator + " " + log + " ---FAIL text actual " + value);
                                logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyTextOfElement" + locator + " " + log + " ---FAIL text actual " + value);
                                reject("Fail");
                            }
                        } catch (err) {
                            throw err
                        }
                    }, function (err) {
                        logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyTextOfElement err " + locator + " " + log + " ---Fail " + err);
                        logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyTextOfElement err " + locator + " " + log + " ---Fail " + err);
                        reject(err);
                    })
                }, function (err) {
                    console.log("!!! FAIL -- FAIL --- verifyTextOfElement --" + locator + "--- element:: " + log + " " + err);
                    logger.Log(globalvalues.reportInstance.TestCaseName + "--" + "verifyTextOfElement locator not Found" + locator + " " + log + " ---FAIL " + err);
                    logger.LogFail(globalvalues.reportInstance.TestCaseName + "--" + "verifyTextOfElement locator not Found" + locator + " " + log + " ---FAIL " + err);
                    reject(err);
                });
            });
        });

    }


    /**
 * desc: it is used to verify whether element is visible or not and it returns true if element is visible otherwise returns false
 * @method verifyElementVisible
 * @author: Venkata Sivakumar
 * @param :{locator: any}
 * @return Boolean
 */
    async verifyElementVisible(locator: any) {
        try {
            let status = await element(locator).isDisplayed();
            return Boolean(status);
        } catch (error) {
            return false;
        }
    }
}