export class globalTestData {

    static marketName: string = "Pittsburgh"
    static marketName2: string = "El Paso (Las Cruces)"
    static singleChannelElPaso: Array<string> = ["KFOX"]
    static multipleChnnels: Array<string> = ["WPGH", "WPNT"]
    static networkName: Array<string> = ["FOX", "NBC"]
    static channelsToBeVerifiedInGrid: string = "WPGH WPNT"
    static spotLengthGridChannelValidation: string = "WPGH,WPNT"
    static networksToBeVerifiedInGrid: string = "FOX"
    static programNameRemovePAV: string = "NBALive";
    static singleChannel1: Array<string> = ["WPNT"]
    static singleChannel: Array<string> = ["WPGH"]
    static singleChannelsToBeVerifiedInGrid: string = "WPGH"
    static tagName: string = "AT_DoNotDelete"
    static tagName1: string = "AT_DoNotDelete1"
    static marketslist: Array<string> = ["Pittsburgh", "Cincinnati", "Albany, NY", "Mobile-Pensacola (Ft Walt)", "El Paso (Las Cruces)"]

    static pricingTabName: string = "Pricing"
    static leftMenuReferenceName: string = "Reference"
    static localRateCardleftMenuName: string = "Local Rate Card"
    static networkRateCardleftMenuName: string = "Network Rate Card"
    static programmingTabName: string = "Programming"
    static leftMenuLocalProgrammingName: string = "Local Programming"
    static leftMenuNetworkProgrammingName: string = "Network Programming"

    //For Smoke Test - Comscore Program Name
    static sellingTitleForComscore: string = "Narcos"//"Just a program"
    static sellingTitleForComscoreNetwork: string = "ComScoreTestDoNotDelete"

    static leftdaypart: string = "Daypart"
    static leftLocalDayparts: string = "Local Dayparts"
    static sellingTitleForNielsenNetwork: string = "NielsenTestDoNotDelete";

    static leftMenuLocalDayPartsName = "Local Dayparts";
    static leftMenuNetworkDayPartsName = "Network Dayparts";
    static leftMenuDayPartName = "Daypart";
    static leftMenuGenresName = "Genres";
    static leftMenuTagsName = "Tags";

    static leftMenuLocalLengthFactors: string = "Local Length Factors";
    static leftMenuNetworkLengthFactors: string = "Network Length Factors";
    static leftMenuLocalSpotLengths: string = "Local Spot Lengths";
    static leftMenuNetworkSpotLengths: string = "Network Spot Lengths";
    static leftMenuLocalSectionLevels: string = "Local Section Levels";
    static leftMenuNetworkSectionLevels: string = "Network Section Levels";

    static leftMenuLocalRateEntry = "Local Rate Entry";
    static leftMenuNetworkRateEntry = "Network Rate Entry";
    static dayParts: Array<string> = ["Morning", "Daytime", "Early Fringe", "Access", "Prime", "Late Fringe", "Weekend", "Overnight", "Sports", "Specials"];

    static leftMenuLocalTagsName = "Local Tags";
    static leftMenuNetworkTagsName = "Network Tags";
}