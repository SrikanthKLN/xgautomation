import { Config } from 'protractor';
import { browser, element, by, protractor } from 'protractor';
import { globalvalues } from '../Utility/globalvalue';
import { Gutils } from '../Utility/gutils';

var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');

export let config: Config = {

    framework: 'jasmine2',
    jasmineNodeOpts: {
        onComplete: null,
        isVerbose: true,
        showColors: true,
        includeStackTrace: false,
        defaultTimeoutInterval: 3600000

    },
    getPageTimeout: 500000,
    allScriptsTimeout: 3600000,

    // The address of a running selenium server.
    // seleniumAddress: 'http://localhost:4444/wd/hub',

    'autoStartStopServer': true,
    capabilities: {
        'browserName': 'chrome',
        shardTestFiles: true,
        maxInstances: 3,

        chromeOptions: {
            args: ['--disable-infobars'],
            // args: [ "--headless", "--disable-gpu", "--window-size=800x600" ],
            prefs: {
                // disable chrome's annoying password manager
                'profile.password_manager_enabled': false,
                'credentials_enable_service': false,
                'password_manager_enabled': false
            }
        },
    },

    specs: [

        //PI-2.1 Scripts

        // '../TestScripts/Pricing/Reference/XGCT-9009_LengthFactorViewAddLengthFactor/XGCT-8374_LengthFactorViewTestSet/XGCT-9024_LengthFactorViewLocal.js',
        // '../TestScripts/Pricing/Reference/XGCT-9009_LengthFactorViewAdd/XGCT-8374_LengthFactorViewAddTestSet/XGCT-9024_LengthFactorViewLocal.js',

        // '../TestScripts/Pricing/Reference/XGCT-9013_PricingSectionLevelCreation/XGCT-8370_PricingValidationSectionview_TestSet/*.js',

        // '../TestScripts/Inventory/XGCT-9001_ProgramTracks/XGCT-8364_Testset/*.js',
        // '../TestScripts/Inventory/XGCT-9002_SelectDemosTargetsRatingsMetrics/XGCT-8365_LocalProgramRatingTracksView_TestSet/*.js',

        // '../TestScripts/Inventory/XGCT-8973_AssignPAVSingleMultipleRecs/XGCT-8364_AssignPAVLocalPrgsRatingsTracks_TestSet/*.js'

        // '../TestScripts/Pricing/XGCT-9007_CopyRateCardbyRateCard/XGCT-8372_RateEntryView/XGCT-8808_RateCardCopyRate.js',

        // '../TestScripts/Inventory/XGCT-9000_RemoveAllAssignedPAVRecs/XGCT-8365_TestSet/*.js', //SingleInstance

        // '../TestScripts/Inventory/AssignPAV/XGCT-8973_AssignPAVSingleMultipleRecs_AS/XGCT-8364_LocalPrgsRatingsTracksView_TS/XGCT-8410_SingleRecNielson_TC.js',
        // '../TestScripts/Inventory/AssignPAV/XGCT-8973_AssignPAVSingleMultipleRecs_AS/XGCT-8364_LocalPrgsRatingsTracksView_TS/XGCT-8411_SingleRecNielsonEnablePAV_TC.js',
        // '../TestScripts/Inventory/AssignPAV/XGCT-8973_AssignPAVSingleMultipleRecs_AS/XGCT-8364_LocalPrgsRatingsTracksView_TS/XGCT-8412_SingleRecNielsonEnablePAVTP_TC.js',
        // '../TestScripts/Inventory/AssignPAV/XGCT-8973_AssignPAVSingleMultipleRecs_AS/XGCT-8364_LocalPrgsRatingsTracksView_TS/XGCT-8433_MultipleRecs_TC.js',

        // '../TestScripts/Inventory/AssignPAV/XGCT-9000_RemoveAllAssignedPAVRecs_AS/XGCT-8365_LocalPrgRatingTracks_TS/XGCT-8570_RemovePAVOptions_TC.js',
        // '../TestScripts/Inventory/AssignPAV/XGCT-9000_RemoveAllAssignedPAVRecs_AS/XGCT-8365_LocalPrgRatingTracks_TS/XGCT-8571_RemoveAllPAVSurvey_TC.js',

        // '../TestScripts/Inventory/ProgramRatingTracks/XGCT-9001_ProgramTracks_AS/XGCT-8364_LocalPrgTracksAssignPAV_TS/XGCT-8547_RoundingDecimal_TC.js',
        // '../TestScripts/Inventory/ProgramRatingTracks/XGCT-9001_ProgramTracks_AS/XGCT-8364_LocalPrgTracksAssignPAV_TS/XGCT-8550_MonthlySurveyDataPlacement_TC.js',
        // '../TestScripts/Inventory/ProgramRatingTracks/XGCT-9001_ProgramTracks_AS/XGCT-8364_LocalPrgTracksAssignPAV_TS/XGCT-7277.js'

        // '../TestScripts/Inventory/ProgramRatingTracks/XGCT-9002_SelectDemosTargetsRatingsMetrics_AS/XGCT-8365_LocalProgramRatingTracksView_TS/XGCT-8535_RemovePrevAddDemoTarget_TC.js',

        // '../TestScripts/Pricing/RateEntry/XGCT-9007_CopyRateCardbyRateCard_AS/XGCT-8372_RateEntryView_TS/XGCT-8808_RateCardCopyRate_TC.js',

        // '../TestScripts/Pricing/Reference/LengthFactors/XGCT-9009_LengthFactorViewAdd_AS/XGCT-8374_LengthFactorViewAdd_TS/XGCT-9024_LengthFactorViewLocal_TC.js',
        // '../TestScripts/Pricing/Reference/LengthFactors/XGCT-9009_LengthFactorViewAdd_AS/XGCT-8374_LengthFactorViewAdd_TS/XGCT-9025_LengthFactorViewNetwork_TC.js',
        // '../TestScripts/Pricing/Reference/LengthFactors/XGCT-9009_LengthFactorViewAdd_AS/XGCT-8374_LengthFactorViewAdd_TS/XGCT-9034_AddLengthFactorView_TC.js',

        // '../TestScripts/Pricing/Reference/SectionLevels/XGCT-9013_SectionLevelCreation_AS/XGCT-8370_PricingValidationSectionview_TS/XGCT-8481_SectionViewLocal_TC.js',
        // '../TestScripts/Pricing/Reference/SectionLevels/XGCT-9013_SectionLevelCreation_AS/XGCT-8370_PricingValidationSectionview_TS/XGCT-8483_SectionViewNetwork_TC.js',

        // '../TestScripts/Pricing/Reference/LengthFactors/XGCT-9011_LeghtFactorCreateDelSpotLength/XGCT-8449_create.js',
        // '../TestScripts/Pricing/Reference/LengthFactors/XGCT-9011_LeghtFactorCreateDelSpotLength/XGCT-8448_Delete_copy.js',

        // '../TestScripts/Reporting/XGCT-9004_DemographicsRefGridFilterSearch/XGCT-8644.js',


        // '../TestScripts/Pricing/RateEntry/XGCT-9008_SpotLengthViewEditDelete_AS/XGCT-8372_PricingRateEntryView_TS/*.js',
        //   '../TestScripts/Pricing/RateEntry/XGCT-9008_SpotLengthViewEditDelete_AS/XGCT-8371_SpotLengthVIew_TS/*.js',

        // '../TestScripts/Inventory/AssignPAV/XGCT-8996_ButtonsGridRecordselection_AS/XGCT-8364_TS/XGCT-8404.js'

        // '../TestScripts/Pricing/Reference/LengthFactors/XGCT-9010_XGCT-9010_EditCloneDeleteMapToRateCardRateEntryView/XGCT-8374_LengthFactorViewTestSet/*.js',

        // '../TestScripts/Inventory/ProgramRatingTracks/XGCT-8998_HideInactiveSurveyRecordFilter_AS/XGCT-8080_HideInactiveSurveyRecordFilter_TS/*.js',
        // '../TestScripts/Reporting/ReferenceData/XGCT-9004_DemographicsRefGridFilterSearch_AS/XGCT-8643_DemoRef_TS/XGCT-8647.js',

        // '../TestScripts/Pricing/SpotLengths/XGCT-9012_LengthFactorSpotLenAdd_AS/XGCT-8374_LengthFactorView_TS/*.js',

        // '../TestScripts/Reporting/ReferenceData/XGCT-9005_MarketReferenceFilterSearch_AS/XGCT-8530_MarketReference_TS/XGCT-8553.js'

        // '../TestScripts/Pricing/Reference/RateCard/XGCT-9006/XGCT-8368_TestSet/*.js',
        // '../TestScripts/Pricing/Reference/RateCard/XGCT-9006/XGCT-8369_TestSet/*.js'

        // '../TestScripts/Pricing/Reference/LengthFactors/XGCT-9009_LengthFactorViewAdd_AS/XGCT-8374_LengthFactorViewAdd_TS/*.js',

        // '../TestScripts/Reporting/ReferenceData/XGCT-9005_MarketReferenceFilterSearch_AS/XGCT-8530_MarketReference_TS/XGCT-8553.js',

        // '../TestScripts/Pricing/Reference/LengthFactors/XGCT-9011_CreateDelSpotLength_AS/XGCT-8371_SPotLengthView_TS/XGCT-8449_CreateSpotLength_TC.js',
        // '../TestScripts/Pricing/SpotLengths/XGCT-9012_LengthFactorSpotLenAdd_AS/XGCT-8374_LengthFactorView_TS/XGCT-9029_SpotLengthAddLocal_TC.js',
        
        // '../TestScripts/Pricing/RateEntry/XGCT-9008_SpotLengthViewEditDelete_AS/XGCT-8371_SpotLengthVIew_TS/XGCT-8446_SpotLenView_TC.js',
        // '../TestScripts/Pricing/RateEntry/XGCT-9008_SpotLengthViewEditDelete_AS/XGCT-8372_PricingRateEntryView_TS/XGCT-8159_EnterRatesByWeekLocal_TC.js',
        // '../TestScripts/Inventory/ProgramRatingTracks/XGCT-8998_HideInactiveSurveyRecordFilter_AS/XGCT-8080_HideInactiveSurveyRecordFilter_TS/*.js',
        // '../TestScripts/Pricing/Reference/LengthFactors/XGCT-9009_LengthFactorViewAdd_AS/XGCT-8374_LengthFactorViewAdd_TS/*.js',
        // '../TestScripts/Pricing/Reference/LengthFactors/XGCT-9010_RateEntryCloneDelMapToRateCard_AS/XGCT-8374_LengthFactorView_TS/*.js',
        
        // '../TestScripts/Pricing/Reference/LengthFactors/XGCT-9011_CreateDelSpotLength_AS/XGCT-8371_SPotLengthView_TS/XGCT-8448_DeleteSpotLength_TC.js',
        // '../TestScripts/Pricing/Reference/LengthFactors/XGCT-9907_LFEditMapToRatecardRateEntryNetwork_AS/XGCT-8374_LengthFactorView_TS/XGCT-9434_LengthFactorEdit_TC.js'

        // PI2.2 Scripts

        // '../TestScripts/Inventory/LocalProgramming/XGCT-9901_LocalProgEditDelClone_AS/XGCT-4670_DeleteLocalProg_TS/XGCT-4670_Delete_TC.js',
        // '../TestScripts/Inventory/LocalProgramming/XGCT-9901_LocalProgEditDelClone_AS/XGCT-4004_CloneLocalProgram_TS/XGCT-4051_Clone_TC.js',
        // '../TestScripts/Inventory/LocalProgramming/XGCT-9901_LocalProgEditDelClone_AS/XGCT-4004_CloneLocalProgram_TS/XGCT-4055_CloneView_TC.js',
        // '../TestScripts/Inventory/LocalProgramming/XGCT-9901_LocalProgEditDelClone_AS/XGCT-8115_LocalNWPrograms_TS/XGCT-8139_Edit_TC.js',
        // '../TestScripts/Inventory/NetworkProgramming/XGCT-9902_EditDeleteCloneNetworkProg_AS/XGCT-4680_CloneNWProg_TS/XGCT-4699_Clone_TC.js',
        // '../TestScripts/Inventory/NetworkProgramming/XGCT-9902_EditDeleteCloneNetworkProg_AS/XGCT-4680_CloneNWProg_TS/XGCT-4739_CloneWithChildRecs_TC.js',
        // '../TestScripts/Inventory/NetworkProgramming/XGCT-9902_EditDeleteCloneNetworkProg_AS/XGCT-4681_DeleteNWProg_TS/XGCT-4818_DeleteNW_TC.js',
        // '../TestScripts/Inventory/NetworkProgramming/XGCT-9902_EditDeleteCloneNetworkProg_AS/XGCT-8115_EditNWProg_TS/XGCT-8145_CreateEditNW_TC.js',
        // '../TestScripts/Inventory/LocalProgramming/XGCT-9903_CreateEditLocalOrbitPrg_AS/XGCT-8115_LocalNetworkProgs_TS/XGCT-8142_CreateEditLocalObitPrg_TC.js',
        // '../TestScripts/Inventory/LocalProgramming/XGCT-9904_DeleteCloneLocalOrbitProg_AS/XGCT-8115_LocalOrbitPrograms_TS/XGCT-8142_CloneAndDelete_TC.js',
        '../TestScripts/Inventory/NetworkProgramming/XGCT-9905_CreateOrbitProgram_AS/XGCT-8115_LocalOrNetworkPrograms_TS/XGCT-8147_CreateNWOrbitProgram_TC.js',
        // '../TestScripts/Inventory/Reference/Daypart/XGCT-9906_LocalDaypartEditView_AS/XGCT-8114_Dayparts_TS/XGCT-8239_DaypartEditView_TC.js',
        // '../TestScripts/Inventory/Reference/Daypart/XGCT-9906_LocalDaypartEditView_AS/XGCT-8114_Dayparts_TS/XGCT-8240_ViewMarketDeviationChannels_TC.js',
        // '../TestScripts/Pricing/Reference/LengthFactors/XGCT-9907_LFEditMapToRatecardRateEntryNetwork_AS/XGCT-8374_LengthFactorView_TS/XGCT-9434_LengthFactorEdit_TC.js',
        // '../TestScripts/Pricing/Reference/LengthFactors/XGCT-9907_LFEditMapToRatecardRateEntryNetwork_AS/XGCT-8374_LengthFactorView_TS/XGCT-9932_RateEntryMapToRateCard_TC.js',
        // '../TestScripts/Reporting/ReferenceData/XGCT-9908_MarketRefFilterSearchComscore_AS/XGCT-8530_MarketReference_TS/XGCT-8552_FilterComscore_TC.js',
        // '../TestScripts/Reporting/ReferenceData/XGCT-9908_MarketRefFilterSearchComscore_AS/XGCT-8530_MarketReference_TS/XGCT-8554_SearchComscore_TC.js',
        // '../TestScripts/Pricing/SpotLengths/XGCT-9012_LengthFactorSpotLenAdd_AS/XGCT-8374_LengthFactorView_TS/XGCT-9032_SpotLengthAddNetwork_TC.js',

        //SMOKE Tests - 15 mins for each script
        // '../TestScripts/SMOKE-TESTS/XGCT-9900_CreateLocalProgram_AS/XGCT-9931_LocalNetworkProgramSmokeTest_TS/XGCT-9934_LocalProgramCreate_Nielsen_TC.js',
        // '../TestScripts/SMOKE-TESTS/XGCT-9900_CreateLocalProgram_AS/XGCT-9931_LocalNetworkProgramSmokeTest_TS/CreateLocalProg_Nielsen_Spec.js',

        // '../TestScripts/SMOKE-TESTS/XGCT-9900_CreateLocalProgram_AS/XGCT-9931_LocalNetworkProgramSmokeTest_TS/CreateLocalProg_Comscore_Spec.js',
        // '../TestScripts/SMOKE-TESTS/XGCT-9899_CreateNetworkProgram_AS/XGCT-9931_NetworkProgramming_TS/XGCT-9935_NetworkProgramCreate_Nielsen_TC.js',

        // '../TestScripts/SMOKE-TESTS/XGCT-9900_CreateLocalProgram_AS/XGCT-9931_LocalNetworkProgramSmokeTest_TS/XGCT-9934_LocalProgCreate_ComScore.js'
        // '../TestScripts/SMOKE-TESTS/XGCT-9900_CreateLocalProgram_AS/XGCT-9931_LocalNetworkProgramSmokeTest_TS/*.js'

        //PI2.3


        // '../TestScripts/Inventory/Reference/Tags/XGCT-10415_TagAddEdit_AS/XGCT-8112_Tags_TS/XGCT-8193_AddNetworkTag_TC.js',
        // '../TestScripts/Inventory/Reference/Tags/XGCT-10415_TagAddEdit_AS/XGCT-8112_Tags_TS/XGCT-8198_EditNetworkTag_TC.js',
        // '../TestScripts/Inventory/Reference/Genres/XGCT-10411_GenreAddEdit_AS/XGCT-7173_GenreRefector_TS/*.js',
        // '../TestScripts/Inventory/Reference/Genres/XGCT-10413_GenreDelClone_AS/XGCT-7173_GenreRefector_TS/XGCT-7362_Add_Clone_Delete_TC.js',

        // '../TestScripts/Inventory/Reference/Tags/XGCT-10416_TagCloneDelete_AS/XGCT-8112_Tags_TS/XGCT-8204_CloneDeleteNetworkTag_TC.js',
        
    ],

    rootElement: "body",
    beforeLaunch: async function () {
        // globalvalues.addTimeStamp();

        var fsr = require('fs-extra');
        let fs = require('fs');
        let path = require('path');
        let gutils = new Gutils();

        await fs.truncate('../../TestResult/actionLog.log', 0, function () {
            console.log('Clearing the data of log file')
        });

        await fs.truncate('../../TestResult/stepsfailLog.log', 0, function () {
            console.log('Clearing the data of log file')
        });

        await fsr.copySync('../../TestResult', '../../ResultBackup/' + gutils.currentTimeStamp())
        console.log('success!')

        await fsr.removeSync('../../TestResult/CustomHtmlResult');
        await fsr.removeSync('../../TestResult/jasmineReport');
        await fsr.removeSync('../../TestResult/ResultCSV');

        let absoluteCustom = await path.resolve('../../TestResult/CustomHtmlResult');
        let absoluteCSV = await path.resolve('../../TestResult/ResultCSV');
        let absoluteJasmine = await path.resolve('../../TestResult/jasmineReport');
        await fs.mkdirSync(absoluteCustom);
        await fs.mkdirSync(absoluteCSV);
        await fs.mkdirSync(absoluteJasmine);

        console.log("BEFORE LAUNCH");

    },
    onPrepare: function () {
        browser.driver.manage().window().maximize();
        jasmine.getEnv().addReporter(
            new Jasmine2HtmlReporter({
                savePath: '../../TestResult/jasmineReport/',
                screenshotsFolder: 'images',
                consolidate: true,
                consolidateAll: false,
                cleanDestination: false,  // false in my saved config - true in tut.
            })
        );
    },

    // A callback function called once tests are finished.
    onComplete: function () {
        // At this point, tests will be done but global objects will still be
        globalvalues.reportInstance.finalize();
        console.log("ON COMPLETE");
    },
    // A callback function called once the tests have finished running and the WebDriver instance has been shut down. It is passed the exit code
    // (0 if the tests passed). This is called once per capability.
    onCleanUp: function () {
        console.log("ON CLEANUP");

    },

    // A callback function called once all tests have finished running and the WebDriver instance has been shut down. It is passed the exit code
    // (0 if the tests passed). This is called only once before the program exits (after onCleanUp).
    afterLaunch: async function () {

        var fs = require('fs');
        var output = '';
        await fs.readdirSync('../../TestResult/jasmineReport/').forEach(function (file) {
            if (!(fs.lstatSync('../../TestResult/jasmineReport/' + file).isDirectory()))
                output = output + fs.readFileSync('../../TestResult/jasmineReport/' + file);
        });
        await fs.writeFileSync('../../TestResult/jasmineReport/ConsolidatedReport.html', output, 'utf8');


        console.log("AFTER LAUNCH");
    },
    resultJsonOutputFile: '../../TestResult/resultsoutput.json'
};