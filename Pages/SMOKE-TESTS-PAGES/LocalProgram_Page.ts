import { by, browser, WebElement, utils, element } from "protractor";
import { ActionLib } from "../../Libs/GeneralLibs/ActionLib";
import { VerifyLib } from "../../Libs/GeneralLibs/VerificationLib";
import { Gutils } from "../../Utility/gutils";
import { globalTestData } from "../../TestData/globalTestData";
import { protractor } from "protractor/built/ptor";

let action = new ActionLib();
let verify = new VerifyLib();
let elementStatus: boolean;

export class localProgrammingPage {

    //Variables Declarations    
    programmingLinkText: string = "Programming"
    localprogrammingLinkText: string = "Local Programming"
    localProgrammingPageHeader: string = 'Local Programs'
    addProgramtypeLink_Local: string = "Local Program"
    sellingTitleColumnHeader: string = "Selling Title"
    programTrackColumnHeader: string = "Program Track"
    programRatingTracksPageHeader: string = "Program Rating Tracks"

    //Locators Declaration
    addlp = by.css('xg-button[attr-name="xgc-add-program-orbit-button"]');
    popupHeader = by.css('div[class*="ui-dialog-titlebar"]')
    addProgramLink = 'xg-button[attr-name*="add"] li a'
    marketsingleSelectDropDown = by.css('xg-dropdown[attr-name*="market"] label[class*="ui-dropdown-label"]')
    channelMultiSelectDropDown = by.css('xg-multi-select[attr-name*="channel"] label[class*="ui-multiselect-label"]')
    daypartMultiSelectDropDown = by.css('xg-multi-select[attr-name*="daypart"] label[class*="ui-multiselect-label"]')
    singleSelectDropdownOptionsLocator = by.css('li[class*="ui-dropdown-item"]')
    multiSelectDropdownOptionsLocator = by.css('li[class*="ui-multiselect-item"]')
    multiSelectClose = by.css("a[class*='ui-multiselect-close']");
    dropDown = "xg-dropdown[label='dynamic'] div[class*='ui-dropdown-trigger']";
    dropDownOptions = by.css("div[class*='ui-widget-content'] ul[class*='ui-dropdown-list']>li");
    multiSelectDropDown = "xg-multi-select[attr-name*='dynamic'] div[class*='ui-multiselect-trigger']";
    multiSelectDropDownOptions = by.css("div[class*='ui-widget-content'] ul[class*='ui-multiselect-list'] li");
    multiSelectDropDownInPopup = "div[class*='popup'] xg-multi-select[attr-name*='dynamic'] div[class*='ui-multiselect-trigger']";
    genreInput = by.xpath("//xg-typeahead[@attr-name='xgc-select-genre' or @attr-name='xgc-typeahead-genre']//input");
    tagInput = by.xpath("//xg-typeahead[@attr-name='xgc-select-keyword' or @attr-name='xgc-typeahead-tag']//input");

    externalIdInput = by.css("xg-input[attr-name*='external'] input");
    lockCheckBox = by.css("xg-checkbox[attr-name*='lock'] input[ng-reflect-model='false']+label");
    lockCheckBoxActive = by.css("xg-checkbox[attr-name*='lock'] input[ng-reflect-model='true']+label");
    ftcInput = by.xpath("//xg-date-picker[@attr-name='xgc-telecast-startdate-date-picker' or @attr-name='xgc-date-start' or @attr-name='xgc-startdate-date-picker' or @attr-name='xgc-telecast-start-date']//input");
    ltcInput = by.xpath("//xg-date-picker[@attr-name='xgc-telecast-enddate-date-picker' or @attr-name='xgc-date-end' or @attr-name='xgc-enddate-date-picker' or @attr-name='xgc-telecast-end-date']//input");
    popupActiveCheckBoxSelect = by.xpath("//xg-checkbox[@attr-name='xgc-program-active' or @attr-name='xgc-check-active']//input[@ng-reflect-model='true']/following-sibling:label");
    popupActiveCheckBox = by.xpath("//xg-checkbox[@attr-name='xgc-program-active' or @attr-name='xgc-check-active']//input[@ng-reflect-model='false']/following-sibling:label");
    tagSuggestionListItems = by.xpath("//xg-typeahead[@attr-name='xgc-select-keyword' or @attr-name='xgc-typeahead-tag']//ul[contains(@class,'ui-autocomplete-items')]/li");
    genreSuggestionListItems = by.xpath("//xg-typeahead[@attr-name='xgc-select-genre' or @attr-name='xgc-typeahead-genre']//ul[contains(@class,'ui-autocomplete-items')]/li");
    descriptionTextArea = by.xpath("//xg-textarea[@attr-name='xgc-program-description' or @attr-name='xgc-description']//textarea");
    addOrbitButton = by.css("xg-button[attr-name*='add-orbits'] button");
    orbitDetailsTableHeaders = by.xpath("//h3[text()='Orbit Details']/../following-sibling:div//xg-grid[@attr-name='xgc-program-grid' or @attr-name='xgc-orbit-programs']//table/thead/tr/th[not(@ng-reflect-field='Delete')]");
    orbitDetailsTableCheckBox = "//h3[text()='Orbit Details']/../following-sibling:div//xg-grid[@attr-name='xgc-program-grid' or @attr-name='xgc-orbit-programs']//table//tr/td[dynamic]/xg-grid-data-cell//mat-checkbox";
    feedOptions = by.xpath("//xg-radio-group[contains(@attr-name,'xgc-radio') or contains(@attr-name,'network')]//label");
    startTime = by.css("xg-time-picker[attr-name='xgc-starttime-time-picker'] input")
    endTime = by.css("xg-time-picker[attr-name='xgc-endtime-time-picker'] input")
    saveLP = by.css("xg-button[attr-name='xgc-save-local-program']")

    //settings Page Locators
    linkSettingsExpanded = by.xpath("//ul[@class='navigation-list']//div[contains(@class,'expanded')]//span[text()='Settings']");
    linkSettings = by.xpath("//ul[@class='navigation-list']//div[not(contains(@class,'expanded'))]//span[text()='Settings']");
    linkDaypartDemo = by.xpath("//ul[@class='sub-list']//div[not(contains(@class,'expanded'))]//span[text()='Daypart Demo']");
    linkDaypartDemoExpanded = by.xpath("//ul[@class='sub-list']//div[contains(@class,'expanded')]//span[text()='Daypart Demo']");
    settingsSubMenuLink = "//ul[@class='navigation-list']//div[contains(@class,'expanded')]//span[text()='Settings']/../following-sibling:div/ul[@class='sub-list']//div[not(contains(@class,'expanded'))]//span[text()='dynamic']";
    settingsSubMenuLinkExpanded = "//ul[@class='navigation-list']//div[contains(@class,'expanded')]//span[text()='Settings']/../following-sibling:div/ul[@class='sub-list']//div[contains(@class,'expanded')]//span[text()='dynamic']";
    dayPartDemoSubMenuLink = "//ul[@class='navigation-list']//div[contains(@class,'expanded')]//span[text()='Settings']/../following-sibling:div/ul[@class='sub-list']//div[contains(@class,'expanded')]//span[text()='Daypart Demo']/../following-sibling:div/ul[@class='sub-list']//span[text()='dynamic']";

    addButton = by.css("xg-button[attr-name='xgc-rowadd'] button:not([disabled])");
    demoSuccessMessage = "Demos are successfully added";
    addPlusButton = by.css("xg-button[icon='fa fa-plus'] button[title='Add']:not([disabled])");
    addDemoButton = by.css("xg-button[attr-name='xgc-add-demo'] button:not([disabled])");//Methods

    firstRowName = by.css("div[class='xg-grid xg-frozen-grid'] table[class*='body-table']>tbody>tr:nth-child(1)>td:nth-child(1) span[class*='xg-inline-block']");
    firstRowMetrics = by.css("div[class='xg-grid xg-frozen-grid'] table[class*='body-table']>tbody>tr:nth-child(1)>td:nth-child(6) search-text-highlighter span[class='ng-star-inserted']");
    firstRowRatings = by.css("div[class*='ui-table-unfrozen-view'] table[class*='scrollable']>tbody>tr:nth-child(1)>td:nth-child(1)");

    globalFilter = by.css('xg-grid[attr-name*="xgc-program-grid"] xg-grid-toolbar input[placeholder*="Global Filter"]');
    market = by.css("xg-dropdown[attr-name='xgc-markets']  div[class*='ui-dropdown-trigger']")
    marketOptions = "//span[contains(text(),'dynamic')]";
    comscoreButton = by.css("xg-button[attr-name='xgc-button-comscore']:not([class='active'])");
    comscoreActiveButton = by.css("xg-button[attr-name='xgc-button-comscore'][class='active']");

    /**
* @desc: click XGList Content Item
* @method: appclickOnNavigationList
* @author: vikas
* @param :{}
* @return: none
*/
    async appclickOnNavigationList(headerText: string) {

        try {

            let locator = by.xpath('//nav[contains(@class,"nav-list")]//span[contains(text(),"' + headerText + '")]');
            await action.MouseMoveToElement(locator, headerText);
            await action.Click(locator, headerText);

        }
        catch (err) {
            await action.ReportSubStep("appclickOnNavigationList", "click on main link" + headerText, "Fail");
        }
    }
    /**
   * @desc: appclickOnMenuListLeftPanel
   * @method: appclickOnMenuListLeftPanel
   * @author: vikas
   * @param :{}
   * @return: none
   */
    async appclickOnMenuListLeftPanel(headerText: string) {

        try {
            let locator = by.xpath('//img-lib-menu-item//span[contains(text(),"' + headerText + '")]');
            await action.MouseMoveToElement(locator, headerText);
            await action.Click(locator, headerText);

        }
        catch (err) {
            await action.ReportSubStep("appclickOnMenuListLeftPanel", "click on main link left panel" + headerText, "Fail");
        }
    }


    /**
    * @desc: To click on AddProgram link
    * @method: clickonAddProgram
    * @author: Adithya K
    * @param :{programType= Local program/Orbit Program}
    * @return: none
    */
    async clickonAddProgram(programType) {
        try {
            let addProgram = by.cssContainingText(this.addProgramLink, programType)
            await browser.waitForAngular();
            await verify.verifyElement(addProgram, programType)
            await action.Click(addProgram, programType)
        }
        catch (error) {
            throw new Error(error);
        }
    }



    /**
   * @desc: To Verify Page Header In Grid
   * @method: verifyPageHeader
   * @author: Adithya
   * @param :{headerName: headerName to be Verified}
   * @return: none
   **/
    async verifyPageHeader(headerName: string) {
        try {
            await browser.waitForAngular();
            let Component = by.cssContainingText('[class*="xg-main-header"]', headerName)
            await verify.verifyElement(Component, headerName);
            action.ReportSubStep("verifyPageHeader", "Displayed " + " " + headerName + "", "Pass");

        }
        catch (err) {
            action.ReportSubStep("verifyPageHeader", "Not Displayed " + headerName + "", "Fail");
            throw new Error(err);
        }

    }

    /**
   * @desc: To verify & validate Days checkboxes displayed in Row of Grid 
   * @method: verifyRowDaysCheckboxesArray
   * @author: Adithya
   * @param :{Row: Row No to be validated, ColumnName: Column to be validated, type: type of Validation or Action, label: Label of Checkbox}
   * @return: none
   */
    async buttonActionsVerify(linkname: Array<string>, type: string, label: string) {
        try {
            await browser.waitForAngular();

            for (let i = 0; i < linkname.length; i++) {

                linkname[i] = linkname[i].toLowerCase()
                let component = by.css('xg-button[attr-name*="' + linkname[i] + '"] button[class*="xg-button"]');
                await verify.verifyElement(component, "CheckBox")
                if (type == "Displayed") {
                    await verify.verifyElement(component, label)
                    action.ReportSubStep("verifyDays", label + " Element Displayed", "Pass")
                }
                else if (type == "Click") {
                    await verify.verifyElement(component, label)
                    await action.MouseMoveToElement(component, label)
                    await action.Click(component, label)
                }

                else if (type == "Enabled") {
                    await verify.verifyElementIsEnabled(component, label)
                    action.ReportSubStep("verifyDays", label + " Element Enabled", "Pass")
                }
                else if (type == "Disabled") {
                    await verify.verifyElementIsDisabled(component, label)
                    action.ReportSubStep("verifyDays", label + " Element Disabled", "Pass")
                }

            }


        }
        catch (err) {
            action.ReportSubStep("verifyDays", "Element Not found", "Fail");
            throw new Error('Error');
        }
    }



    /**
* @desc: click buttons in Ratecard Page
* @method: clickonButtonsRatecardPage
* @author: vikas
* @param :{}
* @return: none
*/
    async clickonButtonsRatecardPage(buttonName: string) {

        try {
            let Xpbutton = by.xpath('//button[normalize-space(text())="' + buttonName + '" and not(@disabled)]');
            await action.MouseMoveToElement(Xpbutton, buttonName)
            await action.Click(Xpbutton, buttonName);
        }
        catch (err) {
            throw new Error(err);
        }
    }



    /**
    * @desc: enter text in Input Inventory Add Program
    * @method: enterTextInInputInventoryAddProgram
    * @author: vikas
    * @param :{}
    * @return: none
    */
    async enterTextInInputInventoryAddProgram(name, textToenter) {
        try {
            let locator = by.css('xg-input[name*="' + name + '"] input');
            await action.MouseMoveToElement(locator, name);
            await action.SetText(locator, textToenter, name);
        }
        catch (err) {
            throw new Error(err);
        }
    }


    /**
     * @desc: click Clock Icon Time Option Inventory dialog
     * @method: clickClockIconTimeOptionInventorydialog
     * @author: vikas
     * @param :{}
     * @return: none
     */
    async clickClockIconTimeOptionInventorydialog(labelName: string) {

        try {
            let locator = by.css('xg-time-picker[name*="' + labelName + '"] i[class*="fa fa-clock"]');
            // let locator = by.xpath("//xg-time-picker[contains(@name,'"+labelName+"') or contains(@ng-reflect-name,'"+labelName+"')]//i[contains(@class,'fa fa-clock')]")
            await action.Click(locator, labelName + " clock icon");
        }
        catch (err) {
            throw new Error(err);
        }
    }


    /**
        * @desc: click Time In Inventory dialog Time table
        * @method: clickTimeInInventorydialogTimetable
        * @author: vikas
        * @param :{}
        * @return: none
        */
    async clickTimeInInventorydialogTimetable(timeOption: string) {

        try {
            let locator = by.xpath('//table[contains(@class,"xg-hours-table")]//td[contains(text(),"' + timeOption + '")]');
            await browser.sleep(1000);
            await action.Click(locator, timeOption);
        }
        catch (err) {
            throw new Error(err);
        }
    }
    /**
     * @desc: click Days Option Inventory dialog
     * @method: clickDaysOptionInventorydialog
     * @author: vikas
     * @param :{}
     * @return: none
     */
    async clickDaysOptionInventorydialog(optionName: string) {

        try {
            let locator = by.css('p-dialog xg-day-picker[name*="days"] label[class*="xg-check-label"]');
            let eleitem = await action.makeDynamicLocatorContainsText(locator, optionName, "Equal");
            await action.Click(eleitem, optionName);
            await browser.sleep(1000)
        }
        catch (err) {
            throw new Error(err);
        }
    }
    /**
* @desc: enter Date In Input Inventory Add Program
* @method: enterDateInInputInventoryAddProgram
* @author: vikas
* @param :{}
* @return: none
*/
    async enterDateInInputInventoryAddProgram(name, edateToenter) {
        try {
            let locator = by.css('xg-date-picker[name*="' + name + '"] input');
            await action.MouseMoveToElement(locator, name);
            await action.SetText(locator, edateToenter, name);
        }
        catch (err) {
            throw new Error(err);
        }
    }


    /**
    * @desc: click drop down Option Inventory
    * @method: clickDropdownOptionInventory
    * @author: vikas
    * @param :{}
    * @return: none
    */
    async clickDropdownOptionInventory(buttonAttribute: string, optionName: string) {

        try {
            let locator = by.css('xg-button[attr-name*="' + buttonAttribute + '"] ul#dropdownMenuButton a');
            let eleitem;

            eleitem = await action.makeDynamicLocatorContainsText(locator, optionName, "Equal");
            await action.MouseMoveJavaScript(eleitem, optionName);
            await action.Click(eleitem, optionName);
        }
        catch (err) {
            action.ReportSubStep("clickDropdownOptionInventory", "click option " + optionName, "Fail");
        }
    }


    /**
         * @desc: It is used to select the dropdown option based on option name if option is not available it will fail
         * @method: selectDropDownValue
         * @author: Venkata Sivakumar
         * @param :{labelName:string,optionName:string}
         * @return: none
         */
    async selectDropDownValue(labelName: string, optionName: string) {
        try {
            elementStatus = await verify.verifyElementVisible(by.css(this.dropDown.replace("dynamic", labelName.trim())));
            if (elementStatus) {
                await action.Click(by.css(this.dropDown.replace("dynamic", labelName.trim())), "Click On " + labelName + " dropdown");
                await verify.verifyElementIsDisplayed(this.dropDownOptions, "Dropdown is expanded");
                let elements = await action.getWebElements(this.dropDownOptions);
                if (elements.length == 0) {
                    await action.ReportSubStep("selectDropDownValue", "There are no options available in " + labelName + " dropdown", "Fail");
                }
                else {
                    for (let index = 0; index < elements.length; index++) {
                        let option = await elements[index].getText();
                        if (option.trim() == optionName.trim()) {
                            await elements[index].click();
                            await browser.sleep(1000);
                            break;
                        }
                        else if (option.trim() != optionName.trim() && index == elements.length - 1) {
                            await action.ReportSubStep("selectDropDownValue", "There is no option available with name " + optionName + " in " + labelName + " dropdown", "Fail");
                        }
                    }
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
   * @desc: click drop down Option Inventory
   * @method: addProgramDropdownInventory
   * @author: vikas
   * @param :{}
   * @return: none
   */
    async addProgramDropdownInventory(optionName: string) {

        try {
            let locator = by.css('ul#dropdownMenuButton a');
            let eleitem = await action.makeDynamicLocatorContainsText(locator, optionName, "Equal");
            await action.MouseMoveToElement(eleitem, optionName);
            await action.Click(eleitem, optionName);
        }
        catch (err) {
            throw new Error(err);
        }
    }
    /**
       * @desc: verify Popup Message Rate card
       * @method: verifyPopupMessageRatecard
       * @author: vikas
       * @param :{}
       * @return: none
       */
    async verifyPopupMessageRatecard(msgText: string) {

        // try {
        //     let locMessage = by.css('div[class*="ui-toast-detail"]');
        //     await verify.verifyTextInElement(locMessage, msgText, "alret msg");
        // }catch (err) { throw new Error(err);}
        try {
            let successMessages = new Array();
            let locMessage = by.css('div[class*="ui-toast-detail"]');
            let elements: Array<WebElement> = await action.getWebElements(locMessage);
            for (let index = 0; index < elements.length; index++) {
                let msg: string = await elements[index].getText();
                successMessages.push(msg);
                await console.log(msg);
            }
            for (let index = 0; index < successMessages.length; index++) {
                if (successMessages[index].includes(msgText)) { break; }
                else if (!successMessages[index].includes(msgText) && index == (successMessages.length - 1)) {
                    action.ReportSubStep("verifyPopupMessageRatecard", msgText + " is not displayed", "Fail");
                }
            } // await verify.verifyTextInElement(locMessage, msgText, "alret msg");

        }
        catch (err) {
            action.ReportSubStep("verifyPopupMessageRatecard", "Error in  function", "Fail");
        }
    }

    /**
 * @desc: It is used to select the dropdown option based on option names if any one of the option is not available it will fail
 * @method: selectDropDownValue
 * @author: Venkata Sivakumar
 * @param :{labelName:string,optionNames:Array<string>}
 * @return: none
 */

    async selectMultiOptionsFromDropDown(labelName: string, optionNames: Array<string>, role: string = "") {
        try {
            await browser.sleep(3000);
            if (role == "") {
                elementStatus = await verify.verifyElementVisible(by.css(this.multiSelectDropDown.replace("dynamic", labelName.trim())));
                if (elementStatus) {
                    await action.Click(by.css(this.multiSelectDropDown.replace("dynamic", labelName.trim())), "Click On " + labelName + " dropdown");
                }
                else {
                    await action.ReportSubStep("selectMultiOptionsFromDropDown", by.css(this.multiSelectDropDown.replace("dynamic", labelName.trim())) + " locator is not available", "Fail");
                }
            }
            else {
                elementStatus = await verify.verifyElementVisible(by.css(this.multiSelectDropDownInPopup.replace("dynamic", labelName.trim())));
                if (elementStatus) {
                    await action.Click(by.css(this.multiSelectDropDownInPopup.replace("dynamic", labelName.trim())), "Click On " + labelName + " dropdown");
                }
                else {
                    await action.ReportSubStep("selectMultiOptionsFromDropDown", by.css(this.multiSelectDropDownInPopup.replace("dynamic", labelName.trim())) + " locator is not available", "Fail");
                }
            }
            await browser.sleep(1000);
            await verify.verifyElementIsDisplayed(this.multiSelectDropDownOptions, "Verify Multi Select Dropdown is expanded");
            let elements = await action.getWebElements(this.multiSelectDropDownOptions);
            if (elements.length == 0) {
                await action.ReportSubStep("selectMultiOptionsFromDropDown", "There are no options available in " + labelName + " multi select dropdown", "Fail");
            }
            else {
                for (let arrayIndex = 0; arrayIndex < optionNames.length; arrayIndex++) {
                    for (let index = 0; index < elements.length; index++) {
                        let option = await elements[index].getText();
                        if (option.trim() == optionNames[arrayIndex].trim()) {
                            try {
                                let status = await elements[index].isSelected();
                                if (!status) {
                                    await elements[index].click();
                                }
                            } catch (error) {
                                await elements[index].click();
                            }
                            break;
                        }
                        else if (option.trim() != optionNames[arrayIndex].trim() && index == elements.length - 1) {
                            await action.ReportSubStep("selectMultiOptionsFromDropDown", "There is no option available with name " + optionNames[arrayIndex].trim() + " in " + labelName + " dropdown", "Fail");
                        }
                    }
                }
                await action.Click(this.multiSelectClose, "Click On Close symbol");
                await browser.sleep(1000);
            }
        } catch (error) {
            throw new Error(error);
        }
    }


    /**
    * @desc: To Erase MultiselectDropdown Selection
    * @method: EraseMultiselectDropdownSelection
    * @author: Adithya
    * @param :{Dropdown:locator of the Dropdown element,lableName:Related Label Name}
    * @return: none
    */
    async eraseMultiselectDropdownSelection(Dropdown, lableName: string, role = "") {
        try {
            await browser.waitForAngular();
            let CloseDropdown = by.css('a[class*="ui-multiselect-close"]')
            let XpCheckBox = by.css('div[class*="ui-multiselect"] input[type="checkbox"]')
            let XpCheckBoxClickComponent = by.css('div[class*="ui-multiselect-header"] div[class*="ui-chkbox-box"]')

            await verify.verifyElement(Dropdown, "Dropdown" + lableName)
            await action.Click(Dropdown, "Dropdown" + lableName)
            await action.MouseMoveToElement(XpCheckBox, lableName);
            await element(XpCheckBox).isSelected().then(async function (value) {
                if (value == false) {
                    await action.Click(XpCheckBoxClickComponent, "Select/Deselect All")
                    await action.Click(XpCheckBoxClickComponent, "Select/Deselect All")
                    action.ReportSubStep("functionName", lableName + " Drop down selection Made empty", "Pass");
                }
                else {
                    await action.Click(XpCheckBoxClickComponent, "Select/Deselect All")
                    action.ReportSubStep("functionName", lableName + " Drop down selection Made empty", "Pass");
                }
            })
            if (role == "") {
                await action.Click(CloseDropdown, "CloseDropdown")
            }
            action.ReportSubStep("EraseMultiselectDropdownSelection", "Drop down selection Made empty", "Pass");

        }
        catch (err) {
            action.ReportSubStep("EraseMultiselectDropdownSelection", "Drop down selection Not Made empty", "Fail");
            throw new Error(err);
        }

    }



    /**
    * @desc: To select Single Select DropDownOptions
    * @method: selectSingleSelectDropDownOptions
    * @author: Adithya
    * @param :{Dropdown Locator, DropdownOptions Locator, Data: Option to be selected}
    * @return: none
    **/
    async selectSingleSelectDropDownOptions(Dropdown, DropdownOptions, Data: string, log: string) {
        try {
            await browser.waitForAngular();
            await verify.verifyElement(Dropdown, log)
            await action.MouseMoveToElement(Dropdown, "Dropdown")
            await action.Click(Dropdown, log)
            await verify.verifyElement(DropdownOptions, "DropDown Options")
            let DropdownOptionsLocator = await action.makeDynamicLocatorContainsText(DropdownOptions, Data, "Equal")
            await action.Click(DropdownOptionsLocator, log)
        }
        catch (err) {
            action.ReportSubStep("selectSingleSelectDropDownOptions", "Requried dropdown not selected" + Data, "Fail");
            throw new Error(err);
        }
    }


    /**
    * @desc: To select MultiSelect DropDownOptions
    * @method: selectMultiSelectDropDownOptions
    * @author: Adithya
    * @param :{Dropdown Locator, DropdownOptions Locator, Data: Option to be selected}
    * @return: none
    **/
    async selectMultiSelectDropDownOptions(Dropdown, DropdownOptions, Data: Array<string>, log: string) {
        try {
            await browser.waitForAngular();
            let CloseDropdown = by.css('a[class*="ui-multiselect-close"]')
            await verify.verifyElement(Dropdown, log)
            await action.MouseMoveToElement(Dropdown, "Dropdown")
            await this.eraseMultiselectDropdownSelection(Dropdown, log, "Dropdown")
            await verify.verifyElement(DropdownOptions, "DropDown Options")
            for (let i = 0; i < Data.length; i++) {
                let DropdownOptionsLocator = await action.makeDynamicLocatorContainsText(DropdownOptions, Data[i], "Equal")
                await action.Click(DropdownOptionsLocator, log + " " + Data[i])
            }
            await action.Click(CloseDropdown, "CloseDropdown")
        }
        catch (err) {
            action.ReportSubStep("selectMultiSelectDropDownOptions", "Requried dropdown not selected" + log, "Fail");
            throw new Error(err);
        }
    }




    /**
       * @desc:click Row In PAV Popup Grid
       * @method: clickRowInPAVPopupGrid
       * @author: vikas
       * @param :{}
       * @return: none
       */
    async clickRowInPAVPopupGrid(row: number, column: number) {
        try {

            let locator = by.xpath('//xg-grid[contains(@attr-name,"xgc-build-pav-rating-data-grid")]//tbody//tr[contains(@class,"ng-star-inserted")][' + row + ']//td[contains(@class,"ui-resizable-column")][' + column + ']//div[contains(@class,"ui-chkbox-box")]');
            await verify.verifyProgressBarNotPresent();
            await browser.sleep(2000);
            await action.MouseMoveToElement(locator, "row" + row);
            await action.Click(locator, "row " + row);
        }
        catch (err) {
            action.ReportSubStep("clickRowInPAVPopupGrid", "click row " + row + " col " + column, "Fail");
        }

    }

    /**
      * @desc: click Data In Rating Source Table Tarcks Page
      * @method: clickDataInRatingSourceTableTarcksPage
      * @author: vikas
      * @param :{}
      * @return: _dataAll
      */
    async clickDataInRatingSourceTableTracksPage(attributeName: string, textToVerify: string) {
        try {
            let locator = by.xpath('//xg-column-list[contains(@attr-name,"' + attributeName + '")]//ul[contains(@class,"list-container")]//li//div[normalize-space(text())="' + textToVerify + '"]|//xg-header[contains(text(),"' + attributeName + '")]/../..//ul[contains(@class,"list-container")]//li//div[normalize-space(text())="' + textToVerify + '"]');

            await action.MouseMoveToElement(locator, attributeName);
            await action.Click(locator, attributeName + " " + textToVerify);

        }
        catch (err) {
            await action.ReportSubStep("clickProgramInRatingsTracksView", "click source collection" + attributeName + " " + textToVerify, "Fail");
        }

    }

    /**
      * @desc: click buttons in page
      * @method: clickonButtonsInPage
      * @author: vikas
      * @param :{}
      * @return: none
      */
    async clickonButtonsInPage(buttonAttribute: string, buttonName: string) {

        try {
            let Xpbutton = by.xpath('//xg-button[contains(@attr-name,"' + buttonAttribute + '")]//button[normalize-space(text())="' + buttonName + '"]');
            await browser.sleep(2000);
            await action.MouseMoveToElement(Xpbutton, buttonName)
            await action.Click(Xpbutton, buttonName);
        }
        catch (err) {
            action.ReportSubStep("clickonButtonsInPage", "click button " + buttonName, "Fail");
        }
    }

    /**
      * @desc: click Program In Ratings Tracks View
      * @method: clickProgramInRatingsTracksView
      * @author: vikas
      * @param :{}
      * @return: 
      */
    async clickProgramInRatingsTracksView(programTitle: string) {
        try {
            let locator = by.xpath('//xg-list[contains(@attr-name,"xgc-list-programs")]//div[contains(@class,"xg-programs-item")]//div[contains(text(),"' + programTitle + '")]');
            await action.MouseMoveToElement(locator, programTitle);
            await browser.sleep(2000);
            await action.Click(locator, programTitle);

        }
        catch (err) {
            await action.ReportSubStep("clickProgramInRatingsTracksView", "click program name" + programTitle, "Fail");
        }

    }


    //Setting Page Methods
    async clickOnSettings() {
        try {
            elementStatus = await verify.verifyElementVisible(this.linkSettingsExpanded);
            if (!elementStatus) {
                await action.Click(this.linkSettings, "Click On Settings link");
                await verify.verifyElementIsDisplayed(this.linkSettingsExpanded, "Verify Settings menu is expanded");
            }
        } catch (error) {
            throw new Error(error);
        }
    }

    async clickOnSettingsSubMenuLink(subMenuLinkName: string) {
        try {
            elementStatus = await verify.verifyElementVisible(by.xpath(this.settingsSubMenuLinkExpanded.replace("dynamic", subMenuLinkName)));
            if (!elementStatus) {
                await action.Click(by.xpath(this.settingsSubMenuLink.replace("dynamic", subMenuLinkName)), "Click On " + subMenuLinkName);
                await verify.verifyElementIsDisplayed(by.xpath(this.settingsSubMenuLinkExpanded.replace("dynamic", subMenuLinkName)), "Verify " + subMenuLinkName + " is expanded");
            }

        } catch (error) {
            throw new Error(error);
        }
    }

    /**
   * @desc: 
   * @method: 
   * @author: 
   * @param :
   * @return: 
   */
    async clickOnDayPartDemoSubMenuLink(subMenuLinkName: string) {
        try {
            await action.Click(by.xpath(this.dayPartDemoSubMenuLink.replace("dynamic", subMenuLinkName)), "Click On '" + subMenuLinkName + "' sub link");
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
     * @desc: Used to click on Add button in Demo Selector dialog
     * @method: clickOnAddButtonInDemoSelectorDialog
     * @author: Venkata Sivakumar
     * @param :{}
     * @return: none
     */
    async clickOnAddButtonInDemoSelectorDialog() {
        try {
            await action.Click(this.addPlusButton, "");
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
     * @desc: Used to click on Add button in Demo Selector dialog footer section
     * @method: clickOnAddButtonInDemoSelectorDialogFooter
     * @author: Venkata Sivakumar
     * @param :{}
     * @return: none
     */
    async clickOnAddButtonInDemoSelectorDialogFooter() {
        try {
            await action.Click(this.addDemoButton, "");
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
    * @desc:    Used to add day part based on market option and day part option
    * @method:  addDayPartsDemo
    * @author:  Venkata Sivakumar
    * @param :  {marketOption:string,dayPartsOption:string}
    * @return:   none
    */
    async addDayPartsDemo(marketOption: string, dayPartsOption: string, demoVal: string) {

        try {
            console.log("Demo Val:  " + demoVal)
            let startRange = demoVal.split('-')[0]
            startRange = startRange.replace(/[A-Z]/g, '')
            let endRange = demoVal.split('-')[1]

            switch (demoVal.charAt(0)) {
                case 'A': demoVal = 'Adults'; break;
                case 'M': demoVal = 'Male'; break;
                case 'W': demoVal = 'Female'; break;
                case 'P': demoVal = 'Person'; break;
                case 'H': demoVal = 'Household'; break;
                // case 'W': demoVal='Working Women';break;
                default: action.ReportSubStep("addDayPartsDemo", "Parameter passed does not match", "Fail")
            }

            await this.selectSingleSelectDropDownOptions(this.marketsingleSelectDropDown, this.singleSelectDropdownOptionsLocator, marketOption, "Market")
            await browser.sleep(2000);
            await this.clickDataInRatingSourceTableTracksPage("Dayparts", dayPartsOption.trim());
            await action.Click(this.addButton, "Click On Add Button");
            await this.clickDataInRatingSourceTableTracksPage("Parent Block", demoVal);
            await this.clickDataInRatingSourceTableTracksPage("Start Range", startRange);
            await this.clickDataInRatingSourceTableTracksPage("End Range", endRange);
            await this.clickOnAddButtonInDemoSelectorDialog();
            await this.clickOnAddButtonInDemoSelectorDialogFooter();
            await browser.sleep(1000);
            await this.verifyPopupMessageRatecard(this.demoSuccessMessage);
        } catch (error) {
            throw new Error(error);
        }
    }


    /**
 * @desc: Used to get the Share,Rating and HutPut values from API after calculation  
 * @method: getRatingsFromAPIAfterCalculation
 * @author: Venkata Sivakumar
 * @param :{dataDetails}
 * @return: metricsAndRatings
 */
    async getRatingsFromAPIAfterCalculation(dataDetails) {
        try {
            let finalShare = 0;
            let finalRating = 0;
            let count = dataDetails.length;
            let finalPeopleUsingTelevision = 0;
            let finalPUT = 0;
            let finalSIU = 0;
            let metricsAndRatings;
            for (let index = 0; index < dataDetails.length; index++) {

                let apiShareValue = dataDetails[index].share;
                let apiRating = dataDetails[index].rating;
                let apiTotalImpression = dataDetails[index].totalImpressions;
                let apiTotalUniverse = dataDetails[index].totalUniverse;
                let apiTotalHUTPUT = dataDetails[index].totalHUTPUT;
                let apiRatingHUTPUT = dataDetails[index].ratingHUTPUT;
                let apiRatingPeopleWatching = dataDetails[index].ratingPeopleWatching;
                let apiRatingUniverse = dataDetails[index].ratingUniverse;
                let apiUniverse = dataDetails[index].universe;
                let apiPeopleUsingTelevision = dataDetails[index].peopleUsingTelevision;
                finalRating = finalRating + (apiTotalImpression / apiTotalUniverse) * 100
                finalPeopleUsingTelevision = finalPeopleUsingTelevision + apiPeopleUsingTelevision;
                finalPUT = finalPUT + (apiRatingHUTPUT / apiRatingUniverse) * 100;
                finalSIU = finalSIU + (apiRatingHUTPUT / apiRatingUniverse) * 100;
                finalShare = finalShare + apiShareValue;
            }
            finalShare = finalShare / count;
            finalRating = finalRating / count;
            finalPUT = finalPUT / count;
            finalSIU = finalPeopleUsingTelevision / count;
            finalPeopleUsingTelevision = finalPeopleUsingTelevision / count;
            return metricsAndRatings = { "RTG": finalRating, "SHR": finalShare, "HP": finalPUT, "SIU": finalSIU };
        } catch (error) {
            throw new Error(error);
        }
    }


    /**
     * @desc: It is used to click on Comscore button
     * @method: clickOnComscore
     * @author: Venkata Sivakumar
     * @param :{}
     * @return: none
     */
    async clickOnComscore() {
        let elementStatus: boolean;
        try {
            elementStatus = await verify.verifyElementVisible(this.comscoreButton);
            if (elementStatus) {
                await action.Click(this.comscoreButton, "Click On Comscore");
                await verify.verifyElementIsDisplayed(this.comscoreActiveButton, "Verify comscore is selected");
            }
            else if (!this.comscoreActiveButton) {
                action.ReportSubStep("clickOnComscore", "Either " + this.comscoreButton + " Or " + this.comscoreActiveButton + " locators are not availble to find the comscore webelement", "Fail");
            }
        } catch (error) {

        }
    }



}


// let apiProgramName = dataDetails[index].programName;
// let apiStartTime = dataDetails[index].startTime;
// let apiEndTime = dataDetails[index].endTime;
    // /**
    // * @desc: clickToggaleProgramsTracksPAVTable
    // * @method: clickToggaleProgramsTracksPAVTable
    // * @author: vikas
    // * @param :{}
    // * @return: none
    // */
    // async clickProgramLinkInLocalProgramGrid(clickColumnName: string, columnMainName: string, maintexttoMatch: string) {
    //     try {
    //         let locTableRow = by.xpath('//xg-grid[contains(@attr-name,"xgc-program-grid")]//table//tbody//tr[contains(@class,"ng-star-inserted")]');
    //         let locColumns = by.xpath('//xg-grid[contains(@attr-name,"xgc-program-grid")]//table//thead//th[contains(@class,"ui-sortable-column")]');
    //         let columnIndexMain: number;
    //         let clickColumnIndex: number;
    //         let flag = false;
    //         let rowIndexG;

    //         await browser.sleep(2000);
    //         await verify.verifyProgressBarNotPresent();
    //         await browser.sleep(2000);
    //         await browser.waitForAngular();
    //         await element.all(locColumns).each(async function (ele, Index) {
    //             await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
    //                 await ele.getText().then(function (_valuecol) {
    //                     if (_valuecol.trim() == columnMainName) {
    //                         columnIndexMain = Index + 2;
    //                     }

    //                     if (_valuecol.trim() == clickColumnName) {
    //                         clickColumnIndex = Index + 2;
    //                     }
    //                 });
    //             });
    //         }).then(async function () {
    //             console.log("Main Col " + columnIndexMain);
    //             console.log("click Col " + clickColumnIndex);
    //             await element.all(locTableRow).each(async function (ele, Index) {
    //                 let rowIndex = Index + 1
    //                 let locColumn = by.xpath('//xg-grid[contains(@attr-name,"xgc-program-grid")]//table//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndexMain + ']');
    //                 await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
    //                     await element(locColumn).getText().then(async function (_text) {
    //                         console.log("main Text " + _text);
    //                         if (_text.trim() == maintexttoMatch) {
    //                             flag = true
    //                             rowIndexG = rowIndex;
    //                         }
    //                     });
    //                 });
    //             }).then(async function () {
    //                 console.log("Flag  " + flag);
    //                 if (flag == true) {
    //                     await browser.sleep(1000);
    //                     let locColumntoclick = by.xpath('//xg-grid[contains(@attr-name,"xgc-program-grid")]//table//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndexG + ']//td[contains(@class,"ui-resizable-column")][' + clickColumnIndex + ']//a[contains(text(),"Link")]');
    //                     await action.MouseMoveToElement(locColumntoclick, "Mouse Move to Link ");
    //                     await action.Click(locColumntoclick, "click Link");
    //                 }
    //                 else {
    //                     await action.ReportSubStep("clickToggaleProgramsTracksPAVTable", "click program link " + maintexttoMatch, "Fail");
    //                 }
    //             });
    //         });

    //     }
    //     catch (err) {
    //         await action.ReportSubStep("clickToggaleProgramsTracksPAVTable", "click program link " + maintexttoMatch, "Fail");
    //     }

    // }

/**
* @desc: Used to get the firstRow Metrics And Raings of first column of demo
* @method: getFirstRowMetricsAndRatings
* @author: Venkata Sivakumar
* @param :{}
* @return: metricsAndRatings
*/
//   async getFirstRowMetricsAndRatings() {

//     let metrics,ratings,metricsLength,ratingsLength,metricsAndRatings
//     let actualMetrics: Array<any> = new Array<any>();
//     let actualRatings: Array<any> = new Array<any>();
//     let metricsValues: Array<any>;
//     let ratingsValues: Array<any>;
//     let uiRTGValue,uiSHRValue,uiHPValue,uiSIUValue;

//     try {
//         await action.MouseMoveJavaScript(this.firstRowMetrics, "firstRowMetrics")
//         metrics = await action.GetText(this.firstRowMetrics, "");
//         ratings = await action.GetText(this.firstRowRatings, "");
//         metricsValues = metrics.trim().split("\n");
//         ratingsValues = ratings.trim().split("\n");
//         metricsValues.forEach(function (value) {
//             if (value.trim() != "") {
//                 actualMetrics.push(value.trim());
//             }
//         });
//         ratingsValues.forEach(function (value) {
//             if (value.trim() != "") {
//                 actualRatings.push(value.trim());
//             }
//         });
//         metricsLength = actualMetrics.length;
//         ratingsLength = actualRatings.length;
//         if (metricsLength == ratingsLength) {
//             for (let index = 0; index < metricsLength; index++) {
//                 switch (metricsValues[index]) {
//                     case "RTG":
//                         uiRTGValue = ratingsValues[index];
//                         break;
//                     case "SHR":
//                         uiSHRValue = ratingsValues[index];
//                         break;
//                     case "HP":
//                         uiHPValue = ratingsValues[index];
//                         break;
//                     case "SIU":
//                         uiSIUValue = ratingsValues[index];
//                         break;
//                     default:
//                         break;
//                 }
//             }
//         }
//         return metricsAndRatings = { "RTG": uiRTGValue, "SHR": uiSHRValue, "HP": uiHPValue, "SIU": uiSIUValue };
//     } catch (error) {
//         throw new Error(error);
//     }
// }