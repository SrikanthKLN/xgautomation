import { by, browser, WebElement, utils, element } from "protractor";
import { ActionLib } from "../../Libs/GeneralLibs/ActionLib";
import { VerifyLib } from "../../Libs/GeneralLibs/VerificationLib";
import { Gutils } from "../../Utility/gutils";
import { globalTestData } from "../../TestData/globalTestData";
import { protractor } from "protractor/built/ptor";


let action = new ActionLib();
let verify = new VerifyLib();
let gUtils = new Gutils();

let elementStatus: boolean;
export class localProgrammingSmoke {

    programDetailsWithSingleChannel = {
        "Channels": globalTestData.singleChannel,
        "Selling Title": "AT_S" + new Date().getTime(),
        "Description": "AT_" + new Date().getTime(),
        "Dayparts": ["Morning"],
        "Days": ["Mo", "Tu", "We", "Th", "Fr"],
        "Start Time": ["08:00", "08:00"],
        "End Time": ["09:00", "09:00"],
        // "FTC": "04/01/2020",
        // "LTC": "04/11/2020",
        // "Tag": "AT_DoNotDelete",
        // "Genre": "AT_DoNotDelete",
    }

    programDetailsForOrbitProgram = {
        "Channels": globalTestData.singleChannel,
        "Selling Title": "AT_O" + new Date().getTime(),
        "Description": "AT_" + new Date().getTime(),
        "Dayparts": ["Morning"],
        "Days": ["Mo", "Tu", "We", "Th", "Fr"],
        "Start Time": ["08:00", "08:00"],
        "End Time": ["09:00", "09:00"],
        // "FTC": "04/01/2020",
        // "LTC": "04/11/2020",
        // "Tag": "AT_DoNotDelete",
        // "Genre": "AT_DoNotDelete",
    }

    programDetailsWithMultipleChannels = {
        "Channels": globalTestData.multipleChnnels,
        "Selling Title": "AT_M" + new Date().getTime(),
        "Description": "AT_" + new Date().getTime(),
        "Dayparts": ["Morning"],
        "Days": ["Mo", "Tu", "We", "Th", "Fr"],
        "Start Time": ["08:00", "08:00"],
        "End Time": ["09:00", "09:00"],
        // "FTC": "04/01/2020",
        // "LTC": "04/11/2020",
        // "Tag": "AT_DoNotDelete",
        // "Genre": "AT_DoNotDelete",
    }

    programmingLinkText: string = "Programming"
    localprogrammingLinkText: string = "Local Programming"
    settingsLinkText: string = "Settings"
    localProgrammingPageHeader: string = 'Local Programs'
    addProgramtypeLink_Local: string = "Local Program"
    addProgramtypeLink_Orbit: string = "Orbit Program"
    addProgramPopUpHeader_Local: string = "Local Program"
    addProgramPopUpHeader_Orbit: string = "Orbit Program"
    editProgramPopUpHeader_Local: string = "Edit Local Program"
    editProgramPopUpHeader_Orbit: string = "Edit Local Orbit Program"
    programTypeColumnHeader: string = "Program Type"
    sellingTitleColumnHeader: string = "Selling Title"
    channelColumnHeader: string = "Channel"
    programTrackColumnHeader: string = "Program Track"
    programTypeInGrid_Local: string = "Local"
    programTypeInGrid_Orbit: string = "Orbit"
    programRatingTracksPageHeader: string = "Program Rating Tracks"
    popupHeader = by.css('div[class*="ui-dialog-titlebar"]')
    addProgramLink = 'xg-button[attr-name*="add"] li a'
    closePopUp = by.css('a[class*="ui-dialog-titlebar-close"] span')
    clearAllColumTextFilter = by.css('i[class*="far fa-times-circle"][mattooltip="Clear All"]')
    sellingTitleEditLink = element.all(by.css('xg-grid xg-grid-data-cell a')).get(0)
    programTrackLink = by.xpath('//a[contains(text(),"Link")]')
    marketsingleSelectDropDown = by.css('xg-dropdown[attr-name*="market"] label[class*="ui-dropdown-label"]')
    networksingleSelectDropDown = by.css('xg-dropdown[attr-name*="network"] label[class*="ui-dropdown-label"]')
    networkMultiSelectDropDown = by.css('xg-multi-select[attr-name*="network"] label[class*="ui-multiselect-label"]')
    channelMultiSelectDropDown = by.css('xg-multi-select[attr-name*="channel"] label[class*="ui-multiselect-label"]')
    daypartMultiSelectDropDown = by.css('xg-multi-select[attr-name*="daypart"] label[class*="ui-multiselect-label"]')
    singleSelectDropdownOptionsLocator = by.css('li[class*="ui-dropdown-item"]')
    multiSelectDropdownOptionsLocator = by.css('li[class*="ui-multiselect-item"]')
    singleSelectDropdownLocator = 'xg-dropdown[attr-name*="dynamic"] label[class*="ui-dropdown-label"]'
    mutliSelectDropdownLocator = 'xg-multi-select[attr-name*="dynamic"] label[class*="ui-multiselect-label"]'
    multiSelectClose = by.css("a[class*='ui-multiselect-close']");

    gridTable = by.css("div[class='xg-grid xg-frozen-grid'] div[class*='ui-table ui-widget']");
    dropDown = "xg-dropdown[label='dynamic'] div[class*='ui-dropdown-trigger']";
    dropDownOptions = by.css("div[class*='ui-widget-content'] ul[class*='ui-dropdown-list']>li");
    multiSelectDropDown = "xg-multi-select[attr-name*='dynamic'] div[class*='ui-multiselect-trigger']";
    multiSelectDropDownOptions = by.css("div[class*='ui-widget-content'] ul[class*='ui-multiselect-list'] li");
    multiSelectDropDownInPopup = "div[class*='popup'] xg-multi-select[attr-name*='dynamic'] div[class*='ui-multiselect-trigger']";
    // firstRowSellingTitle = by.css("xg-grid[attr-name='xgc-program-grid']>div[class='xg-grid'] table>tbody>tr:nth-child(1)>td:nth-child(3)");
    // activeCheckBox = by.css("xg-checkbox[attr-name='xgc-active'][ng-reflect-model='false'] input");
    // activeCheckBoxSelect = by.css("xg-checkbox[attr-name='xgc-active'][ng-reflect-model='true'] input");
    // deleteButton = by.css("xg-button[attr-name='xgc-delete-program-button'],[attr-name='xgc-network-program-delete']  button");
    // confirmPopupDeleteButton = by.css("xg-popup[attr-name*='xgc-delete']  xg-button[attr-name*='xgc-delete']");
    // confirmPopupCancelButton = by.css("xg-popup[attr-name*='xgc-delete']  xg-button[attr-name*='xgc-cancel']");
    // genreInput = by.xpath("//xg-typeahead[@attr-name='xgc-select-genre' or @attr-name='xgc-typeahead-genre']//input");
    // tagInput = by.xpath("//xg-typeahead[@attr-name='xgc-select-keyword' or @attr-name='xgc-typeahead-tag']//input");
    // externalIdInput = by.css("xg-input[attr-name*='external'] input");
    // lockCheckBox = by.css("xg-checkbox[attr-name*='lock'] input[ng-reflect-model='false']");
    // lockCheckBoxActive = by.css("xg-checkbox[attr-name*='lock'] input[ng-reflect-model='true']");
    // ftcInput = by.xpath("//xg-date-picker[@attr-name='xgc-telecast-startdate-date-picker' or @attr-name='xgc-date-start' or @attr-name='xgc-startdate-date-picker']//input");
    // ltcInput = by.xpath("//xg-date-picker[@attr-name='xgc-telecast-startdate-date-picker' or @attr-name='xgc-date-start' or @attr-name='xgc-enddate-date-picker']//input");
    // popupActiveCheckBoxSelect = by.xpath("//xg-checkbox[@attr-name='xgc-program-active' or @attr-name='xgc-check-active']//input[@ng-reflect-model='true']");
    // popupActiveCheckBox = by.xpath("//xg-checkbox[@attr-name='xgc-program-active' or @attr-name='xgc-check-active']//input[@ng-reflect-model='false']");
    // tagSuggestionListItems = by.xpath("//xg-typeahead[@attr-name='xgc-select-keyword' or @attr-name='xgc-typeahead-tag']//ul[contains(@class,'ui-autocomplete-items')]/li");
    // genreSuggestionListItems = by.xpath("//xg-typeahead[@attr-name='xgc-select-genre' or @attr-name='xgc-typeahead-genre']//ul[contains(@class,'ui-autocomplete-items')]/li");
    // descriptionTextArea = by.xpath("//xg-textarea[@attr-name='xgc-program-description' or @attr-name='xgc-description']//textarea");
    // addOrbitBuuton = by.css("xg-button[attr-name='xgc-add-orbits-button'] button");
    // orbitDetailsTableHeaders = by.xpath("//h3[text()='Orbit Details']/../following-sibling::div//xg-grid[@attr-name='xgc-program-grid']//table/thead/tr/th[not(@ng-reflect-field='Delete')]");
    // orbitDetailsTableCheckBox = "//h3[text()='Orbit Details']/../following-sibling::div//xg-grid[@attr-name='xgc-program-grid']//table//tr/td[dynamic]/xg-grid-data-cell//mat-checkbox";
    // cancelButton = by.css("xg-button[attr-name='xgc-cancel-program'] button");
    // feedOptions = by.css("xg-radio-group[attr-name*='xgc-radio'] label");
    // cloneButton = by.css("xg-button[attr-name*='xgc-clone'] button");
    // cloneSellingTitleInput = by.css("xg-input[attr-name*='selling-title'] input");
    // confirmClonePopupCloneButton = by.xpath("//xg-button[@attr-name='xgc-clone-program-button' or @attr-name='xgc-network-program-clone-confirm']//button[not(@disabled)]");
    // confirmClonePopupCancelButton = by.xpath("//xg-button[@attr-name='xgc-cancel-clone-program-button' or @attr-name='xgc-network-program-cancel-confirm']//button");
    // addOrbitButton = by.css("xg-button[attr-name*='add-orbits'] button");

    headerLocalProgram = by.xpath("//h3[normalize-space()='Local Programs']");
    firstRowSellingTitle = by.css("xg-grid[attr-name='xgc-program-grid']>div[class='xg-grid'] table>tbody>tr:nth-child(1)>td:nth-child(3)");
    activeCheckBox = by.css("xg-checkbox[attr-name='xgc-active'][ng-reflect-model='false'] input");
    activeCheckBoxSelect = by.css("xg-checkbox[attr-name='xgc-active'][ng-reflect-model='true'] input");
    deleteButton = by.css("xg-button[attr-name='xgc-delete-program-button'],[attr-name='xgc-network-program-delete']  button");
    confirmDeletePopupDeleteButton = by.css("xg-popup[attr-name*='xgc-delete']  xg-button[attr-name*='xgc-delete']");
    confirmDeletePopupCancelButton = by.css("xg-popup[attr-name*='xgc-delete']  xg-button[attr-name*='xgc-cancel']");
    genreInput = by.xpath("//xg-typeahead[@attr-name='xgc-select-genre' or @attr-name='xgc-typeahead-genre']//input");
    tagInput = by.xpath("//xg-typeahead[@attr-name='xgc-select-keyword' or @attr-name='xgc-typeahead-tag']//input");
    externalIdInput = by.css("xg-input[attr-name*='external'] input");
    lockCheckBox = by.css("xg-checkbox[attr-name*='lock'] input[ng-reflect-model='false']+label");
    lockCheckBoxActive = by.css("xg-checkbox[attr-name*='lock'] input[ng-reflect-model='true']+label");
    ftcInput = by.xpath("//xg-date-picker[@attr-name='xgc-telecast-startdate-date-picker' or @attr-name='xgc-date-start' or @attr-name='xgc-startdate-date-picker' or @attr-name='xgc-telecast-start-date']//input");
    ltcInput = by.xpath("//xg-date-picker[@attr-name='xgc-telecast-enddate-date-picker' or @attr-name='xgc-date-end' or @attr-name='xgc-enddate-date-picker' or @attr-name='xgc-telecast-end-date']//input");
    popupActiveCheckBoxSelect = by.xpath("//xg-checkbox[@attr-name='xgc-program-active' or @attr-name='xgc-check-active']//input[@ng-reflect-model='true']/following-sibling::label");
    popupActiveCheckBox = by.xpath("//xg-checkbox[@attr-name='xgc-program-active' or @attr-name='xgc-check-active']//input[@ng-reflect-model='false']/following-sibling::label");
    tagSuggestionListItems = by.xpath("//xg-typeahead[@attr-name='xgc-select-keyword' or @attr-name='xgc-typeahead-tag']//ul[contains(@class,'ui-autocomplete-items')]/li");
    genreSuggestionListItems = by.xpath("//xg-typeahead[@attr-name='xgc-select-genre' or @attr-name='xgc-typeahead-genre']//ul[contains(@class,'ui-autocomplete-items')]/li");
    descriptionTextArea = by.xpath("//xg-textarea[@attr-name='xgc-program-description' or @attr-name='xgc-description']//textarea");
    addOrbitButton = by.css("xg-button[attr-name*='add-orbits'] button");
    orbitDetailsTableHeaders = by.xpath("//h3[text()='Orbit Details']/../following-sibling::div//xg-grid[@attr-name='xgc-program-grid' or @attr-name='xgc-orbit-programs']//table/thead/tr/th[not(@ng-reflect-field='Delete')]");
    orbitDetailsTableCheckBox = "//h3[text()='Orbit Details']/../following-sibling::div//xg-grid[@attr-name='xgc-program-grid' or @attr-name='xgc-orbit-programs']//table//tr/td[dynamic]/xg-grid-data-cell//mat-checkbox";
    feedOptions = by.xpath("//xg-radio-group[contains(@attr-name,'xgc-radio') or contains(@attr-name,'network')]//label");
    cloneButton = by.css("xg-button[attr-name*='xgc-clone'] button");
    cloneSellingTitleInput = by.css("xg-input[attr-name*='selling-title'] input");
    confirmClonePopupCloneButton = by.xpath("//xg-button[@attr-name='xgc-clone-program-button' or @attr-name='xgc-network-program-clone-confirm']//button[not(@disabled)]");
    confirmClonePopupCancelButton = by.xpath("//xg-button[@attr-name='xgc-cancel-clone-program-button' or @attr-name='xgc-network-program-cancel-confirm']//button");
    cancelButton = by.css("xg-button[attr-name*='cancel'] button");
    networkProgramView = by.css("div[attr-name='xgc-network-program-view']");
    disableSaveButton = by.css("xg-button[attr-name*='save'] button[disabled]");
    enableSaveButton = by.css("xg-button[attr-name*='save'] button:not([disabled])");
    sellingTitleInput = by.css("xg-input[attr-name*='selling_title'] input");

    //settings Page Locators
    linkSettingsExpanded = by.xpath("//ul[@class='navigation-list']//div[contains(@class,'expanded')]//span[text()='Settings']");
    linkSettings = by.xpath("//ul[@class='navigation-list']//div[not(contains(@class,'expanded'))]//span[text()='Settings']");
    linkDaypartDemo = by.xpath("//ul[@class='sub-list']//div[not(contains(@class,'expanded'))]//span[text()='Daypart Demo']");
    linkDaypartDemoExpanded = by.xpath("//ul[@class='sub-list']//div[contains(@class,'expanded')]//span[text()='Daypart Demo']");
    settingsSubMenuLink = "//ul[@class='navigation-list']//div[contains(@class,'expanded')]//span[text()='Settings']/../following-sibling::div/ul[@class='sub-list']//div[not(contains(@class,'expanded'))]//span[text()='dynamic']";
    settingsSubMenuLinkExpanded = "//ul[@class='navigation-list']//div[contains(@class,'expanded')]//span[text()='Settings']/../following-sibling::div/ul[@class='sub-list']//div[contains(@class,'expanded')]//span[text()='dynamic']";
    dayPartDemoSubMenuLink = "//ul[@class='navigation-list']//div[contains(@class,'expanded')]//span[text()='Settings']/../following-sibling::div/ul[@class='sub-list']//div[contains(@class,'expanded')]//span[text()='Daypart Demo']/../following-sibling::div/ul[@class='sub-list']//span[text()='dynamic']";

    noDataMatchFound = by.xpath("//td[normalize-space()='No data match is found']");
    addButton = by.css("xg-button[attr-name='xgc-rowadd'] button:not([disabled])");
    demoSuccessMessage = "Demos are successfully added";
    addPlusButton = by.css("xg-button[icon='fa fa-plus'] button[title='Add']:not([disabled])");
    addDemoButton = by.css("xg-button[attr-name='xgc-add-demo'] button:not([disabled])");//Methods
    firstRowDemos = by.css("xg-grid[attr-name='xgc-daypart-demo-grid'] div[class='xg-grid'] table>tbody>tr:nth-child(1)>td:nth-child(1)");

    firstRowName = by.css("div[class='xg-grid xg-frozen-grid'] table[class*='body-table']>tbody>tr:nth-child(1)>td:nth-child(1) span[class*='xg-inline-block']");
    firstRowMetrics = by.css("div[class='xg-grid xg-frozen-grid'] table[class*='body-table']>tbody>tr:nth-child(1)>td:nth-child(6) search-text-highlighter span[class='ng-star-inserted']");
    firstRowRatings = by.css("div[class*='ui-table-unfrozen-view'] table[class*='scrollable']>tbody>tr:nth-child(1)>td:nth-child(1)");

    globalFilter = by.css('xg-grid[attr-name*="xgc-program-grid"] xg-grid-toolbar input[placeholder*="Global Filter"]');
    localProgram = '//xg-grid[contains(@attr-name,"xgc-program-grid")]//table//tbody//tr//td[contains(.,"dynamic")]';
    market = by.css("xg-dropdown[attr-name='xgc-markets']  div[class*='ui-dropdown-trigger']")

    marketOptions = "//span[contains(text(),'dynamic')]";
    displayOptions = by.css("[title='Display Options']")

    comscoreButton = by.css("xg-button[attr-name='xgc-button-comscore']:not([class='active'])");
    comscoreActiveButton = by.css("xg-button[attr-name='xgc-button-comscore'][class='active']");

    /**
* desc: click XGList Content Item
* @method appclickOnNavigationList
* @author: vikas
* @param :{}
* @return none
*/
    async appclickOnNavigationList(headerText: string) {

        try {

            let locator = by.xpath('//nav[contains(@class,"nav-list")]//span[contains(text(),"' + headerText + '")]');
            await action.MouseMoveToElement(locator, headerText);
            await action.Click(locator, headerText);

        }
        catch (err) {
            await action.ReportSubStep("appclickOnNavigationList", "click on main link" + headerText, "Fail");
        }
    }
    /**
   * desc: appclickOnMenuListLeftPanel
   * @method appclickOnMenuListLeftPanel
   * @author: vikas
   * @param :{}
   * @return none
   */
    async appclickOnMenuListLeftPanel(headerText: string) {

        try {
            let locator = by.xpath('//img-lib-menu-item//span[contains(text(),"' + headerText + '")]');
            await action.MouseMoveToElement(locator, headerText);
            await action.Click(locator, headerText);

        }
        catch (err) {
            await action.ReportSubStep("appclickOnMenuListLeftPanel", "click on main link left panel" + headerText, "Fail");
        }
    }


    /**
   * desc: verifyMenuListonLeftPanel
   * @method verifyMenuListonLeftPanel
   * @author: Srikanth
   * @param :{}
   * @return none
   */
    async verifyMenuListonLeftPanel(headerText: string) {

        try {
            let locator = by.xpath('//img-lib-menu-item//span[contains(text(),"' + headerText + '")]');
            await verify.verifyElementIsDisplayed(locator, " Navigation Items");

        }
        catch (err) {
            await action.ReportSubStep("verifyMenuListonLeftPanel", "Verify main link left panel" + headerText, "Fail");
        }
    }

    /**
    * desc: To click on AddProgram link
    * @method clickonAddProgram
    * @author: Adithya K
    * @param :{programType= Local program/Orbit Program}
    * @return none
    */
    async clickonAddProgram(programType) {
        try {
            let addProgram = by.cssContainingText(this.addProgramLink, programType)
            await browser.waitForAngular();
            await verify.verifyElement(addProgram, programType)
            await action.Click(addProgram, programType)
        }
        catch (error) {
            throw new Error(error);
        }
    }

    /**
    * desc: To verify Add Programtype
    * @method verifyAddProgramtype
    * @author: Adithya K
    * @param :{programType= Local program/Orbit Program}
    * @return none
    */
    async verifyAddProgramtype(programType) {
        try {
            let addProgram = by.cssContainingText(this.addProgramLink, programType)
            await browser.waitForAngular();
            await verify.verifyElement(addProgram, programType)
        }
        catch (error) {
            throw new Error(error);
        }
    }

    /**
    * desc: To verify Pop-Up Header
    * @method verifyPopUpHeader
    * @author: Adithya K
    * @param :{headerText= headerText to be verified}
    * @return none
    */
    async verifyPopUpHeader(headerText) {
        try {
            await browser.waitForAngular();
            await verify.verifyElement(this.popupHeader, headerText)
            await verify.verifyTextOfElement(this.popupHeader, headerText, headerText)
        }
        catch (error) {
            throw new Error(error);
        }
    }

    /**
   * desc: To Verify Page Header In Grid
   * @method verifyPageHeader
   * @author: Adithya
   * @param :{headerName: headerName to be Verified}
   * @return none
   **/
    async verifyPageHeader(headerName: string) {
        try {
            await browser.waitForAngular();
            let Component = by.cssContainingText('[class*="xg-main-header"]', headerName)
            await verify.verifyElement(Component, headerName);
            action.ReportSubStep("verifyPageHeader", "Displayed " + " " + headerName + "", "Pass");

        }
        catch (err) {
            action.ReportSubStep("verifyPageHeader", "Not Displayed " + headerName + "", "Fail");
            throw new Error(err);
        }

    }

    /**
   * desc: To verify & validate Days checkboxes displayed in Row of Grid 
   * @method verifyRowDaysCheckboxesArray
   * @author: Adithya
   * @param :{Row: Row No to be validated, ColumnName: Column to be validated, type: type of Validation or Action, label: Label of Checkbox}
   * @return none
   */
    async buttonActionsVerify(linkname: Array<string>, type: string, label: string) {
        try {
            await browser.waitForAngular();

            for (let i = 0; i < linkname.length; i++) {

                linkname[i] = linkname[i].toLowerCase()
                let component = by.css('xg-button[attr-name*="' + linkname[i] + '"] button[class*="xg-button"]');
                await verify.verifyElement(component, "CheckBox")
                if (type == "Displayed") {
                    await verify.verifyElement(component, label)
                    action.ReportSubStep("verifyDays", label + " Element Displayed", "Pass")
                }
                else if (type == "Click") {
                    await verify.verifyElement(component, label)
                    await action.MouseMoveToElement(component, label)
                    await action.Click(component, label)
                }

                else if (type == "Enabled") {
                    await verify.verifyElementIsEnabled(component, label)
                    action.ReportSubStep("verifyDays", label + " Element Enabled", "Pass")
                }
                else if (type == "Disabled") {
                    await verify.verifyElementIsDisabled(component, label)
                    action.ReportSubStep("verifyDays", label + " Element Disabled", "Pass")
                }

            }


        }
        catch (err) {
            action.ReportSubStep("verifyDays", "Element Not found", "Fail");
            throw new Error('Error');
        }
    }


    /**
    * desc: To Filter grid values in the view 
    * @method GridFilter
    * @author: Adithya
    * @param :{ColumnName:Column Name which is to be Validated,FilterData:Testdata to be Filtered}
    * @return none
    */
    async gridFilter(ColumnName: string, FilterData: string) {
        try {
            await browser.waitForAngular();
            let gridFilterIndex
            let gridColumnHeaderLocator = by.css('xg-grid th[style*="display: table-cell"]')
            let gridColumnFilterLocator = by.css('xg-grid table input[class*="ui-inputtext"]')
            let elementCount = await element.all(gridColumnHeaderLocator).count()
            let headerElements = await element.all(gridColumnHeaderLocator).getWebElements()
            let headerName;

            for (let index = 0; index < elementCount; index++) {
                headerName = await headerElements[index].getText();
                if (headerName.trim() == ColumnName.trim()) {
                    gridFilterIndex = index;
                    break;
                }
                else if (headerName.trim() != ColumnName.trim() && index == (elementCount - 1)) {
                    action.ReportSubStep("GridFilter", "Column Name not available in Grid", "Fail");
                }
            }
            let ColumnFilter = element.all(gridColumnFilterLocator).get(gridFilterIndex)
            await action.ClearText(ColumnFilter, headerName + " Column text Filter")
            for (let i = 0; i < FilterData.length; i++) {
                await browser.sleep(100);
                await action.SetText(ColumnFilter, FilterData.charAt(i), headerName + " Column text Filter")
                await browser.sleep(100);
            }
        }
        catch (err) {
            await action.ReportSubStep("GridFilter", "Text Not Entered", "Fail");
        }
    }


    /** 
    * desc: To Validate grid values in the view 
    * @method gridDataValidation
    * @author: Adithya 
    * @param :{ColumnName:Column Name which is to be Validated,data:Testdata to be Entered,equalContains:equal or Contains}
    * @return none
    */
    async gridDataValidation(ColumnName: string, data: string, equalContains: string) {
        try {
            await browser.waitForAngular();
            let gridColumnIndex;
            let gridColumnHeaderLocator = by.css('xg-grid th[style*="display: table-cell"]')
            let paginator_next = by.css('a[class*="ui-paginator-next"]')
            let elementCount = await element.all(gridColumnHeaderLocator).count()
            let headerElements = await element.all(gridColumnHeaderLocator).getWebElements()
            let headerName;

            for (let index = 0; index < elementCount; index++) {
                headerName = await headerElements[index].getText();
                if (headerName.trim() == ColumnName.trim()) {
                    gridColumnIndex = index + 2;
                    break;
                }
                else if (headerName.trim() != ColumnName.trim() && index == (elementCount - 1)) {
                    action.ReportSubStep("gridDataValidation", "Column Name not available in Grid", "Fail");
                }
            }
            await console.log("index of column is " + gridColumnIndex, "Column no is " + gridColumnIndex, "Name of column is " + headerName)
            await verify.verifyElement(by.css('xg-grid tr td:nth-child(' + gridColumnIndex + ')'), headerName)
            await action.MouseMoveToElement(by.css('xg-grid tr td:nth-child(' + gridColumnIndex + ')'), headerName)
            let gridDataCount = await element.all(by.css('xg-grid tr td:nth-child(' + gridColumnIndex + ')')).count()
            let gridDataElements = await element.all(by.css('xg-grid tr td:nth-child(' + gridColumnIndex + ')')).getWebElements()
            for (let colIndex = 0; colIndex < gridDataCount; colIndex++) {
                let text = await gridDataElements[colIndex].getText()
                text=text.trim()
                if (equalContains.toUpperCase() == "EQUAL") {
                    if (data == text) {
                        action.ReportSubStep("gridDataValidation", "Data available " + text, "Pass");
                    }
                    else {
                        action.ReportSubStep("gridDataValidation", "Data Not available " + text, "Fail");
                    }
                }
                else {
                    if (data.includes(text) || text.includes(data)) {
                        action.ReportSubStep("gridDataValidation", "Data available " + text, "Pass");
                    }
                    else {
                        action.ReportSubStep("gridDataValidation", "Data Not available " + text, "Fail");
                    }
                }
            }
        }
        catch (err) {
            await action.ReportSubStep("gridDataValidation", "Data Not available", "Fail");
            // throw new Error('Error');
        }
    }


    /**
   * desc: To verify DropDown Option Available
   * @method verifyDropDownOptionAvailable
   * @author: Adithya
   * @param :{Dropdown Locator, DropdownOptions Locator, Data: Options to be verified}
   * @return none
   **/
    async verifyDropDownOptionAvailable(DropDown, DropDownOptions, Data: Array<string>, log: string) {
        try {
            await browser.waitForAngular();
            let OptionsArray: Array<string> = []
            await action.Click(DropDown, log)
            await browser.sleep(2000)
            await element.all(DropDownOptions).each(function (item) {
                item.getText().then(function (options) {
                    OptionsArray.push(options)
                })
            })

            for (let i = 0; i < Data.length; i++) {
                if (OptionsArray.includes(Data[i])) {
                    await console.log(Data[i] + " available")
                    action.ReportSubStep("verifyDropDownOptionAvailable", Data[i] + " option available", "Pass")

                }
                else {
                    action.ReportSubStep("verifyDropDownOptionAvailable", Data[i] + " option not available", "Fail")
                }
            }
            await action.Click(DropDown, log)
        }
        catch (err) {
            action.ReportSubStep("verifyDropDownOptionAvailable", Data + " option not available ", "Fail");
            throw new Error('Error');
        }
    }

    /**
* desc: click buttons in Ratecard Page
* @method clickonButtonsRatecardPage
* @author: vikas
* @param :{}
* @return none
*/
    async clickonButtonsRatecardPage(buttonName: string) {

        try {
            let Xpbutton = by.xpath('//button[normalize-space(text())="' + buttonName + '" and not(@disabled)]');
            await action.MouseMoveToElement(Xpbutton, buttonName)
            await action.Click(Xpbutton, buttonName);
        }
        catch (err) {
            throw new Error(err);
        }
    }


    /**
* desc: click buttons in Ratecard Page
* @method clickonButtonsRatecardPage1
* @author: vikas
* @param :{}
* @return none
*/
    async clickonButtonsRatecardPage1(attributeName: string, buttonName: string) {

        try {
            let Xpbutton = by.xpath('//xg-button[contains(@attr-name,"' + attributeName + '")]//button[normalize-space(text())="' + buttonName + '" and not(@disabled)]');
            await action.MouseMoveToElement(Xpbutton, buttonName)
            await action.Click(Xpbutton, buttonName);
        }
        catch (err) {
            action.ReportSubStep("clickonButtonsRatecardPage1", "Error in  function", "Fail");
        }
    }

    /**
* desc: enter text in Input Inventory Add Program
* @method enterTextInInputInventoryAddProgram
* @author: vikas
* @param :{}
* @return none
*/
    async enterTextInInputInventoryAddProgram(name, textToenter) {
        try {
            let locator = by.css('xg-input[name*="' + name + '"] input');
            await action.MouseMoveToElement(locator, name);
            await action.SetText(locator, textToenter, name);
        }
        catch (err) {
            throw new Error(err);
        }
    }

    /**
  * desc: enter text in Input Inventory Add Program
  * @method enterTextInInputInventoryAddProgram1
  * @author: vikas
  * @param :{}
  * @return none
  */
    async enterTextInInputInventoryAddProgram1(attributeName: string, textToenter) {
        try {
            let locator = by.css('xg-input[attr-name*="' + attributeName + '"] input');
            await action.MouseMoveToElement(locator, attributeName);
            await action.SetText(locator, textToenter, attributeName);
        }
        catch (err) {
            action.ReportSubStep("enterTextInInputInventoryAddProgram1", "Error in  function", "Fail");
        }
    }
    /**
     * desc: click Clock Icon Time Option Inventory dialog
     * @method clickClockIconTimeOptionInventorydialog
     * @author: vikas
     * @param :{}
     * @return none
     */
    async clickClockIconTimeOptionInventorydialog(labelName: string) {

        try {
            let locator = by.css('xg-time-picker[name*="' + labelName + '"] i[class*="fa fa-clock"]');
            // let locator = by.xpath("//xg-time-picker[contains(@name,'"+labelName+"') or contains(@ng-reflect-name,'"+labelName+"')]//i[contains(@class,'fa fa-clock')]")
            await action.Click(locator, labelName + " clock icon");
        }
        catch (err) {
            throw new Error(err);
        }
    }

    /**
    * desc: click Clock Icon Time Option Inventory dialog
    * @method clickClockIconTimeOptionInventorydialog1
    * @author: vikas
    * @param :{}
    * @return none
    */
    async clickClockIconTimeOptionInventorydialog1(attributeName: string) {

        try {
            let locator = by.css('xg-time-picker[attr-name*="' + attributeName + '"] i[class*="fa fa-clock"]');
            await action.Click(locator, attributeName + " clock icon");
        }
        catch (err) {
            action.ReportSubStep("clickClockIconTimeOptionInventorydialog1", "Error in  function", "Fail");
        }
    }
    /**
        * desc: click Time In Inventory dialog Time table
        * @method clickTimeInInventorydialogTimetable
        * @author: vikas
        * @param :{}
        * @return none
        */
    async clickTimeInInventorydialogTimetable(timeOption: string) {

        try {
            let locator = by.xpath('//table[contains(@class,"xg-hours-table")]//td[contains(text(),"' + timeOption + '")]');
            await browser.sleep(1000);
            await action.Click(locator, timeOption);
        }
        catch (err) {
            throw new Error(err);
        }
    }
    /**
     * desc: click Days Option Inventory dialog
     * @method clickDaysOptionInventorydialog
     * @author: vikas
     * @param :{}
     * @return none
     */
    async clickDaysOptionInventorydialog(optionName: string) {

        try {
            let locator = by.css('p-dialog xg-day-picker[name*="days"] label[class*="xg-check-label"]');
            let eleitem = await action.makeDynamicLocatorContainsText(locator, optionName, "Equal");
            await action.Click(eleitem, optionName);
            await browser.sleep(1000)
        }
        catch (err) {
            throw new Error(err);
        }
    }
    /**
* desc: enter Date In Input Inventory Add Program
* @method enterDateInInputInventoryAddProgram
* @author: vikas
* @param :{}
* @return none
*/
    async enterDateInInputInventoryAddProgram(name, edateToenter) {
        try {
            let locator = by.css('xg-date-picker[name*="' + name + '"] input');
            await action.MouseMoveToElement(locator, name);
            await action.SetText(locator, edateToenter, name);
        }
        catch (err) {
            throw new Error(err);
        }
    }

    /**
 * desc: enter Date In Input Inventory Add Program
 * @method enterDateInInputInventoryAddProgram1
 * @author: vikas
 * @param :{}
 * @return none
 */
    async enterDateInInputInventoryAddProgram1(attributeName, edateToenter) {
        try {
            let locator = by.css('xg-date-picker[attr-name*="' + attributeName + '"] input');
            await action.MouseMoveToElement(locator, attributeName);
            await action.SetText(locator, edateToenter, attributeName);
        }
        catch (err) {
            action.ReportSubStep("enterDateInInputInventoryAddProgram1", "Error in  function", "Fail");
        }
    }
    /**
    * desc: click drop down Option Inventory
    * @method clickDropdownOptionInventory
    * @author: vikas
    * @param :{}
    * @return none
    */
    async clickDropdownOptionInventory(buttonAttribute: string, optionName: string) {

        try {
            let locator = by.css('xg-button[attr-name*="' + buttonAttribute + '"] ul#dropdownMenuButton a');
            let eleitem;

            eleitem = await action.makeDynamicLocatorContainsText(locator, optionName, "Equal");
            await action.MouseMoveJavaScript(eleitem, optionName);
            await action.Click(eleitem, optionName);
        }
        catch (err) {
            action.ReportSubStep("clickDropdownOptionInventory", "click option " + optionName, "Fail");
        }
    }

    /**
      * desc: click drop down Option Inventory
      * @method clickDropdownOptionInventory1
      * @author: vikas
      * @param :{}
      * @return none
      */
    async clickDropdownOptionInventory1(optionName: string) {

        try {
            let locator = by.css('ul#dropdownMenuButton a');
            let eleitem = await action.makeDynamicLocatorContainsText(locator, optionName, "Equal");
            await action.MouseMoveToElement(eleitem, optionName);
            await action.Click(eleitem, optionName);
        }
        catch (err) {
            action.ReportSubStep("clickDropdownOptionInventory1", "Error in  function", "Fail");
        }
    }
    /**
         * desc: It is used to select the dropdown option based on option name if option is not available it will fail
         * @method selectDropDownValue
         * @author: Venkata Sivakumar
         * @param :{labelName:string,optionName:string}
         * @return none
         */
    async selectDropDownValue(labelName: string, optionName: string) {
        try {
            elementStatus = await verify.verifyElementVisible(by.css(this.dropDown.replace("dynamic", labelName.trim())));
            if (elementStatus) {
                await action.Click(by.css(this.dropDown.replace("dynamic", labelName.trim())), "Click On " + labelName + " dropdown");
                await verify.verifyElementIsDisplayed(this.dropDownOptions, "Dropdown is expanded");
                let elements = await action.getWebElements(this.dropDownOptions);
                if (elements.length == 0) {
                    await action.ReportSubStep("selectDropDownValue", "There are no options available in " + labelName + " dropdown", "Fail");
                }
                else {
                    for (let index = 0; index < elements.length; index++) {
                        let option = await elements[index].getText();
                        if (option.trim() == optionName.trim()) {
                            await elements[index].click();
                            await browser.sleep(1000);
                            break;
                        }
                        else if (option.trim() != optionName.trim() && index == elements.length - 1) {
                            await action.ReportSubStep("selectDropDownValue", "There is no option available with name " + optionName + " in " + labelName + " dropdown", "Fail");
                        }
                    }
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
   * desc: click drop down Option Inventory
   * @method addProgramDropdownInventory
   * @author: vikas
   * @param :{}
   * @return none
   */
    async addProgramDropdownInventory(optionName: string) {

        try {
            let locator = by.css('ul#dropdownMenuButton a');
            let eleitem = await action.makeDynamicLocatorContainsText(locator, optionName, "Equal");
            await action.MouseMoveToElement(eleitem, optionName);
            await action.Click(eleitem, optionName);
        }
        catch (err) {
            throw new Error(err);
        }
    }
    /**
     * desc: Used to add local/network/orbit program
     * @method addProgram
     * @author: Venkata Sivakumar
     * @param :{programDetails, programType: string,errorMsg:string=""}
     * @return: none 
     */
    async addProgram(programDetails, programType: string, errorMsg: string = "") {
        try {
            let localProgramTypeStatus: boolean = programType.trim().replace(" ", "").toLowerCase() == "localprogram" || programType.trim().replace(" ", "").toLowerCase() == "local";
            let networkProgramTypeStatus: boolean = programType.trim().replace(" ", "").toLowerCase() == "networkprogram" || programType.trim().replace(" ", "").toLowerCase() == "network";
            let networkOrbitProgramTypeStatus: boolean = programType.trim().replace(" ", "").replace(" ", "").toLowerCase() == "networkorbitprogram" || programType.trim().replace(" ", "").toLowerCase() == "networkorbit";
            let localOrbitProgramTypeStatus: boolean = programType.trim().replace(" ", "").replace(" ", "").toLowerCase() == "localorbitprogram" || programType.trim().replace(" ", "").toLowerCase() == "localorbit";

            if (programDetails.length == 0) { await action.ReportSubStep("addProgram", programDetails + " Empty object has given", "Fail"); }
            else {

                await this.clickonButtonsRatecardPage("Add");
                if (localProgramTypeStatus) { await this.addProgramDropdownInventory("Local Program"); }
                else if (networkProgramTypeStatus) { await this.addProgramDropdownInventory("Add Network Program"); }
                else if (networkOrbitProgramTypeStatus) { await this.addProgramDropdownInventory("Add Orbit Program"); }
                else if (localOrbitProgramTypeStatus) { await this.addProgramDropdownInventory("Orbit Program"); }
                if (localProgramTypeStatus || localOrbitProgramTypeStatus) {
                    if (programDetails["Channels"] != undefined) { await this.selectMultiOptionsFromDropDown("channel", programDetails["Channels"], "dialog"); }
                }
                if (networkProgramTypeStatus) {
                    if (programDetails["Network"] != undefined) { await this.selectDropDownValue("Network", programDetails["Network"]); }
                }
                if (programDetails["Dayparts"] != undefined) { await this.selectMultiOptionsFromDropDown("daypart", programDetails["Dayparts"], "dialog"); }
                if (programDetails["Selling Title"] != undefined) { await this.enterTextInInputInventoryAddProgram("programName", programDetails["Selling Title"]); }
                if (programDetails["Description"] != undefined) { await action.SetText(this.descriptionTextArea, programDetails["Description"], ""); }
                if (networkProgramTypeStatus || networkOrbitProgramTypeStatus) {
                    await this.selectFeedOption(programDetails);
                    if (programDetails["Base Time Zone"] != undefined) { await this.selectDropDownValue("Base Time Zone", programDetails["Base Time Zone"]); }
                }
                if (localProgramTypeStatus || networkProgramTypeStatus) {
                    await this.selectStartTimeInLocalOrNetworkProgram(programDetails)
                    await this.selectEndTimeInLocalOrNetworkProgram(programDetails);
                    await this.selectDaysInLocalOrNetworkProgram(programDetails);
                }
                else if (localOrbitProgramTypeStatus || networkOrbitProgramTypeStatus) {
                    await this.selectStartTimeInLocalOrNetworkOrbitProgram(programDetails);
                    await this.selectEndTimeInLocalOrNetworkOrbitProgram(programDetails);
                    await this.selectDaysInLocalOrNetworkOrbitProgram(programDetails);
                }
                if (programDetails["FTC"] != undefined) { await action.SetText(this.ftcInput, programDetails["FTC"], ""); }
                if (programDetails["LTC"] != undefined) { await action.SetText(this.ltcInput, programDetails["LTC"], ""); }

                await this.selectLockOption(programDetails);
                await this.selectActiveOption(programDetails);
                await this.selectGenreOption(programDetails);
                await this.selectTagOption(programDetails);

                if (programDetails["Hiatus Start Date"] != undefined) { await this.enterDateInInputInventoryAddProgram("hiatusStartDate", programDetails["Hiatus Start Date"]); }
                if (programDetails["Hiatus End Date"] != undefined) { await this.enterDateInInputInventoryAddProgram("hiatusEndDate", programDetails["Hiatus End Date"]); }
                if (programDetails["External ID"] != undefined) { await action.SetText(this.externalIdInput, programDetails["External ID"], ""); }

                await this.clickonButtonsRatecardPage("Save");
                await browser.sleep(2000);

                if (errorMsg.trim() == "") {
                    if (networkOrbitProgramTypeStatus || networkProgramTypeStatus) { await this.verifyPopupMessageRatecard("Successfully Added"); }

                }// else if (localProgramTypeStatus || localOrbitProgramTypeStatus) {
                else if (localProgramTypeStatus) {
                    await this.verifyPopupMessageRatecard("Successfully added");
                }

                else if (errorMsg.trim() != "") { await this.verifyPopupMessageRatecard(errorMsg.trim()); }
            }
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
       * desc: Used to select start time in local or network program
       * @method selectStartTimeInLocalOrNetworkProgram
       * @author: Venkata Sivakumar
       * @param :{programDetails}
       * @return: none 
       */
    async selectStartTimeInLocalOrNetworkProgram(programDetails) {
        try {
            if (programDetails["Start Time"] != undefined) {
                await this.clickClockIconTimeOptionInventorydialog("start");
                await browser.sleep(2000);
                let startTimeValues: Array<any> = programDetails["Start Time"];
                for (let index = 0; index < startTimeValues.length; index++) {
                    await this.clickTimeInInventorydialogTimetable(String(startTimeValues[index]));
                    await browser.sleep(2000);
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
           * desc: Used to select start time in local or network orbit program
           * @method selectStartTimeInLocalOrNetworkOrbitProgram
           * @author: Venkata Sivakumar
           * @param :{programDetails}
           * @return: none 
           */
    async selectStartTimeInLocalOrNetworkOrbitProgram(programDetails) {
        try {
            if (programDetails["Start Time"] != undefined) {
                await action.Click(this.addOrbitButton, "Click On Orbit button");
                await this.clickClockIconTimeOptionOrbitDetails("start");
                await browser.sleep(2000);
                let startTimeValues: Array<any> = programDetails["Start Time"];
                await console.log("startTimeValues.length" + startTimeValues.length);
                for (let index = 0; index < startTimeValues.length; index++) {
                    await console.log("startTimeValues" + startTimeValues[index]);
                    await this.clickTimeInInventorydialogTimetable(String(startTimeValues[index]));
                    await browser.sleep(2000);
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
           * desc: Used to select end time in local or network program
           * @method selectEndTimeInLocalOrNetworkProgram
           * @author: Venkata Sivakumar
           * @param :{programDetails}
           * @return: none 
           */
    async selectEndTimeInLocalOrNetworkProgram(programDetails) {
        try {
            if (programDetails["End Time"] != undefined) {
                await this.clickClockIconTimeOptionInventorydialog("end");
                await browser.sleep(2000);
                let endTimeValues: Array<any> = programDetails["End Time"];
                await console.log("endTimeValues.length" + endTimeValues.length);
                for (let index = 0; index < endTimeValues.length; index++) {
                    await console.log("endTimeValues" + endTimeValues[index]);
                    await this.clickTimeInInventorydialogTimetable(String(endTimeValues[index]));
                    await browser.sleep(2000);
                }
            }
        } catch (error) {
            throw new Error(error);
        }

    }
    /**
       * desc: Used to select end time in local or network orbit program
       * @method selectEndTimeInLocalOrNetworkOrbitProgram
       * @author: Venkata Sivakumar
       * @param :{programDetails}
       * @return: none 
       */
    async selectEndTimeInLocalOrNetworkOrbitProgram(programDetails) {
        try {
            if (programDetails["End Time"] != undefined) {
                await this.clickClockIconTimeOptionOrbitDetails("end");
                await browser.sleep(2000);
                let endTimeValues: Array<any> = programDetails["End Time"];
                await console.log("endTimeValues.length" + endTimeValues.length);
                for (let index = 0; index < endTimeValues.length; index++) {
                    await this.clickTimeInInventorydialogTimetable(String(endTimeValues[index]));
                    await browser.sleep(2000);
                }
            }
        } catch (error) {
            throw new Error(error);
        }

    }
    /**
           * desc: Used to select Feed Option in local or network program
           * @method selectFeedOption
           * @author: Venkata Sivakumar
           * @param :{programDetails}
           * @return: none 
           */
    async selectFeedOption(programDetails) {
        try {
            if (programDetails["Feed"] != undefined) {
                let options: Array<WebElement> = await action.getWebElements(this.feedOptions);
                for (let index = 0; index < options.length; index++) {
                    let option: string = await options[index].getText();
                    if (option.trim() == programDetails["Feed"]) {
                        await options[index].click();
                        break;
                    }
                    else if (option.trim() != programDetails["Feed"] && index == (options.length - 1)) {
                        await action.ReportSubStep("addProgram", programDetails["Feed"] + " option is not available under Feed Section", "Fail");
                    }
                }
            }

        } catch (error) {
            throw new Error(error);
        }
    }
    /**
           * desc: Used to select days Option in local or network program
           * @method selectDaysInLocalOrNetworkProgram
           * @author: Venkata Sivakumar
           * @param :{programDetails}
           * @return: none 
           */
    async selectDaysInLocalOrNetworkProgram(programDetails) {
        try {
            if (programDetails["Days"] != undefined) {
                let dayValues: Array<string> = programDetails["Days"];
                let daysLength = dayValues.length;
                for (let index = 0; index < daysLength; index++) {
                    this.clickDaysOptionInventorydialog(dayValues[index]);
                    await browser.sleep(1000)
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
       * desc: Used to select days Option in local or network orbit program
       * @method selectDaysInLocalOrNetworkOrbitProgram
       * @author: Venkata Sivakumar
       * @param :{programDetails}
       * @return: none 
       */
    async selectDaysInLocalOrNetworkOrbitProgram(programDetails) {
        try {
            if (programDetails["Days"] != undefined) {
                let dayValues: Array<string> = programDetails["Days"];
                let daysLength = dayValues.length;
                let webElements: Array<WebElement> = await action.getWebElements(this.orbitDetailsTableHeaders);
                for (let index = 0; index < daysLength; index++) {
                    for (let jindex = 0; jindex < webElements.length; jindex++) {
                        let actualText: string = await webElements[jindex].getText();
                        if (actualText.trim() == dayValues[index]) {
                            await action.Click(by.xpath(this.orbitDetailsTableCheckBox.replace("dynamic", String(jindex + 1))), "");
                            await browser.sleep(1000)
                            break;
                        }
                    }
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
           * desc: Used to select or deslect lock
           * @method selectLockOption
           * @author: Venkata Sivakumar
           * @param :{programDetails}
           * @return: none 
           */
    async selectLockOption(programDetails) {
        try {
            if (programDetails["Lock"] != undefined) {
                if (programDetails["Lock"]) {
                    let elementStatus: boolean = await verify.verifyElementVisible(this.lockCheckBox);
                    let elementStatus1: boolean = await verify.verifyElementVisible(this.lockCheckBoxActive);
                    if (elementStatus) {
                        await action.Click(this.lockCheckBox, "Click On Lock checkbox to lock");
                        await verify.verifyElementIsDisplayed(this.lockCheckBoxActive, "verify lock checkbox is checked");
                    }
                    else if (elementStatus == false && elementStatus1 == false) {
                        await action.ReportSubStep("addProgram", this.lockCheckBox + " " + this.lockCheckBoxActive + " locators are not found", "Fail");
                    }
                }
                else {
                    let elementStatus: boolean = await verify.verifyElementVisible(this.lockCheckBoxActive);
                    let elementStatus1: boolean = await verify.verifyElementVisible(this.lockCheckBox);
                    if (elementStatus) {
                        await action.Click(this.lockCheckBoxActive, "Click On Lock checkbox to unlock");
                        await verify.verifyElementIsDisplayed(this.lockCheckBox, "verify lock checkbox is unchecked");
                    }
                    else if (elementStatus == false && elementStatus1 == false) {
                        await action.ReportSubStep("addProgram", this.lockCheckBox + " " + this.lockCheckBoxActive + " locators are not found", "Fail");
                    }
                }
            }

        } catch (error) {
            throw new Error(error);
        }
    }
    /**
           * desc: Used to select or deslect Active
           * @method selectActiveOption
           * @author: Venkata Sivakumar
           * @param :{programDetails}
           * @return: none 
           */
    async selectActiveOption(programDetails) {
        try {
            if (programDetails["Active"] != undefined) {
                if (programDetails["Active"]) {
                    let elementStatus: boolean = await verify.verifyElementVisible(this.popupActiveCheckBox);
                    let elementStatus1: boolean = await verify.verifyElementVisible(this.popupActiveCheckBoxSelect);
                    if (elementStatus) {
                        await action.Click(this.popupActiveCheckBox, "Click On Active checkbox to active");
                        await verify.verifyElementIsDisplayed(this.popupActiveCheckBoxSelect, "verify lock Active is checked");
                    }
                    else if (elementStatus == false && elementStatus1 == false) {
                        await action.ReportSubStep("addProgram", this.popupActiveCheckBox + " " + this.popupActiveCheckBoxSelect + " locators are not found", "Fail");
                    }
                }
                else {
                    let elementStatus: boolean = await verify.verifyElementVisible(this.popupActiveCheckBoxSelect);
                    let elementStatus1: boolean = await verify.verifyElementVisible(this.popupActiveCheckBox);
                    if (elementStatus) {
                        await action.Click(this.popupActiveCheckBoxSelect, "Click On Active checkbox to inactive");
                        await verify.verifyElementIsDisplayed(this.popupActiveCheckBox, "verify Active checkbox is unchecked");
                    }
                    else if (elementStatus == false && elementStatus1 == false) {
                        await action.ReportSubStep("addProgram", this.popupActiveCheckBox + " " + this.popupActiveCheckBoxSelect + " locators are not found", "Fail");
                    }
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
           * desc: Used to select Genre option
           * @method selectGenreOption
           * @author: Venkata Sivakumar
           * @param :{programDetails}
           * @return: none 
           */
    async selectGenreOption(programDetails) {
        try {
            if (programDetails["Genre"] != undefined) {
                await action.SetText(this.genreInput, programDetails["Genre"], "");
                await verify.verifyElementIsDisplayed(this.genreSuggestionListItems, "Verify suggestion list items displayed");
                let genreWebElements: Array<WebElement> = await action.getWebElements(this.genreSuggestionListItems);
                for (let index = 0; index < genreWebElements.length; index++) {
                    let actualText: string = await genreWebElements[index].getText();
                    if (actualText.trim() == programDetails["Genre"] || actualText.trim().includes(programDetails["Genre"])) {
                        await genreWebElements[index].click();
                        break;
                    }
                    else if (actualText.trim() != programDetails["Genre"] || !actualText.trim().includes(programDetails["Genre"]) && index == genreWebElements.length - 1) {
                        await action.ReportSubStep("addProgram", programDetails["Genre"] + " suggestion item is not displayed in Genre", "Fail");
                    }
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
         * desc: Used to select Tag option
         * @method selectTagOption
         * @author: Venkata Sivakumar
         * @param :{programDetails}
         * @return: none 
         */
    async selectTagOption(programDetails) {
        try {
            if (programDetails["Tag"] != undefined) {
                await action.SetText(this.tagInput, programDetails["Tag"], "");
                await verify.verifyElementIsDisplayed(this.tagSuggestionListItems, "Verify suggestion list items displayed");
                let genreWebElements: Array<WebElement> = await action.getWebElements(this.tagSuggestionListItems);
                for (let index = 0; index < genreWebElements.length; index++) {
                    let actualText: string = await genreWebElements[index].getText();
                    if (actualText.trim() == programDetails["Tag"] || actualText.trim().includes(programDetails["Tag"])) {
                        await genreWebElements[index].click();
                        break;
                    }
                    else if (actualText.trim() != programDetails["Tag"] || !actualText.trim().includes(programDetails["Tag"]) && index == genreWebElements.length - 1) {
                        await action.ReportSubStep("addProgram", programDetails["Tag"] + " suggestion item is not displayed in Tag", "Fail");
                    }
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
    * desc: click Clock Icon Time Option Inventory dialog
    * @method clickClockIconTimeOptionInventorydialog
    * @author: vikas
    * @param :{}
    * @return none
    */
    async clickClockIconTimeOptionOrbitDetails(labelName: string) {

        try {
            let locator = by.css('xg-time-picker[ng-reflect-name*="' + labelName + '"] i[class*="fa fa-clock"]');
            await action.Click(locator, labelName + " clock icon");
        }
        catch (err) {
            throw new Error(err);
        }
    }
    /**
       * desc: verify Popup Message Rate card
       * @method verifyPopupMessageRatecard
       * @author: vikas
       * @param :{}
       * @return none
       */
    async verifyPopupMessageRatecard(msgText: string) {

        try {
            let locMessage = by.css('div[class*="ui-toast-detail"]');
            await verify.verifyTextInElement(locMessage, msgText, "alret msg");
        }
        catch (err) {
            throw new Error(err);
        }
    }

    /**
 * desc: It is used to select the dropdown option based on option names if any one of the option is not available it will fail
 * @method selectDropDownValue
 * @author: Venkata Sivakumar
 * @param :{labelName:string,optionNames:Array<string>}
 * @return none
 */

    async selectMultiOptionsFromDropDown(labelName: string, optionNames: Array<string>, role: string = "") {
        try {
            await browser.sleep(3000);
            if (role == "") {
                elementStatus = await verify.verifyElementVisible(by.css(this.multiSelectDropDown.replace("dynamic", labelName.trim())));
                if (elementStatus) {
                    await action.Click(by.css(this.multiSelectDropDown.replace("dynamic", labelName.trim())), "Click On " + labelName + " dropdown");
                }
                else {
                    await action.ReportSubStep("selectMultiOptionsFromDropDown", by.css(this.multiSelectDropDown.replace("dynamic", labelName.trim())) + " locator is not available", "Fail");
                }
            }
            else {
                elementStatus = await verify.verifyElementVisible(by.css(this.multiSelectDropDownInPopup.replace("dynamic", labelName.trim())));
                if (elementStatus) {
                    await action.Click(by.css(this.multiSelectDropDownInPopup.replace("dynamic", labelName.trim())), "Click On " + labelName + " dropdown");
                }
                else {
                    await action.ReportSubStep("selectMultiOptionsFromDropDown", by.css(this.multiSelectDropDownInPopup.replace("dynamic", labelName.trim())) + " locator is not available", "Fail");
                }
            }
            await browser.sleep(1000);
            await verify.verifyElementIsDisplayed(this.multiSelectDropDownOptions, "Verify Multi Select Dropdown is expanded");
            let elements = await action.getWebElements(this.multiSelectDropDownOptions);
            if (elements.length == 0) {
                await action.ReportSubStep("selectMultiOptionsFromDropDown", "There are no options available in " + labelName + " multi select dropdown", "Fail");
            }
            else {
                for (let arrayIndex = 0; arrayIndex < optionNames.length; arrayIndex++) {
                    for (let index = 0; index < elements.length; index++) {
                        let option = await elements[index].getText();
                        if (option.trim() == optionNames[arrayIndex].trim()) {
                            try {
                                let status = await elements[index].isSelected();
                                if (!status) {
                                    await elements[index].click();
                                }
                            } catch (error) {
                                await elements[index].click();
                            }
                            break;
                        }
                        else if (option.trim() != optionNames[arrayIndex].trim() && index == elements.length - 1) {
                            await action.ReportSubStep("selectMultiOptionsFromDropDown", "There is no option available with name " + optionNames[arrayIndex].trim() + " in " + labelName + " dropdown", "Fail");
                        }
                    }
                }
                await action.Click(this.multiSelectClose, "Click On Close symbol");
                await browser.sleep(1000);
            }
        } catch (error) {
            throw new Error(error);
        }
    }


    /**
    * desc: To Erase MultiselectDropdown Selection
    * @method EraseMultiselectDropdownSelection
    * @author: Adithya
    * @param :{Dropdown:locator of the Dropdown element,lableName:Related Label Name}
    * @return none
    */
    async eraseMultiselectDropdownSelection(Dropdown, lableName: string,role="") {
        try {
            await browser.waitForAngular();
            let CloseDropdown = by.css('a[class*="ui-multiselect-close"]')
            let XpCheckBox = by.css('div[class*="ui-multiselect"] input[type="checkbox"]')
            let XpCheckBoxClickComponent = by.css('div[class*="ui-multiselect-header"] div[class*="ui-chkbox-box"]')

            await verify.verifyElement(Dropdown, "Dropdown" + lableName)
            await action.Click(Dropdown, "Dropdown" + lableName)
            await action.MouseMoveToElement(XpCheckBox, lableName);
            await element(XpCheckBox).isSelected().then(async function (value) {
                if (value == false) {
                    await action.Click(XpCheckBoxClickComponent, "Select/Deselect All")
                    await action.Click(XpCheckBoxClickComponent, "Select/Deselect All")
                    action.ReportSubStep("functionName", lableName + " Drop down selection Made empty", "Pass");
                }
                else {
                    await action.Click(XpCheckBoxClickComponent, "Select/Deselect All")
                    action.ReportSubStep("functionName", lableName + " Drop down selection Made empty", "Pass");
                }
            })
            if(role==""){
                await action.Click(CloseDropdown, "CloseDropdown")
            }
            action.ReportSubStep("EraseMultiselectDropdownSelection", "Drop down selection Made empty", "Pass");

        }
        catch (err) {
            action.ReportSubStep("EraseMultiselectDropdownSelection", "Drop down selection Not Made empty", "Fail");
            throw new Error(err);
        }

    }


    /**
    * desc: To select All In Multiselect Dropdown
    * @method selectAllInMultiselectDropdown
    * @author: Adithya
    * @param :{Dropdown:locator of the Dropdown element,lableName:Related Label Name}
    * @return none
    */
    async selectAllInMultiselectDropdown(Dropdown, lableName: string) {
        try {
            await browser.waitForAngular();
            let CloseDropdown = by.css('a[class*="ui-multiselect-close"]')
            let XpCheckBox = by.css('xg-multi-select input[type="checkbox"]')
            let XpCheckBoxClickComponent = by.css('div[class*="ui-multiselect-header"] div[class*="ui-chkbox-box"]')

            await verify.verifyElement(Dropdown, "Dropdown" + lableName)
            await action.Click(Dropdown, "Dropdown" + lableName)
            await action.MouseMoveToElement(XpCheckBox, lableName);
            await element(XpCheckBox).isSelected().then(async function (value) {
                if (value == false) {
                    await action.Click(XpCheckBoxClickComponent, "Select/Deselect All")
                    action.ReportSubStep("functionName", lableName + " All Dropdown Options selected", "Pass");
                }
                else {
                    action.ReportSubStep("functionName", lableName + " All Dropdown Options selected", "Pass");
                }
            })
            await action.Click(CloseDropdown, "CloseDropdown")
            action.ReportSubStep("selectAllInMultiselectDropdown", lableName + " All Dropdown Options selected", "Pass");
        }
        catch (err) {
            action.ReportSubStep("selectAllInMultiselectDropdown", lableName + " All Dropdown Options not selected", "Fail");
            throw new Error(err);
        }

    }


    /**
    * desc: To select Single Select DropDownOptions
    * @method selectSingleSelectDropDownOptions
    * @author: Adithya
    * @param :{Dropdown Locator, DropdownOptions Locator, Data: Option to be selected}
    * @return none
    **/
    async selectSingleSelectDropDownOptions(Dropdown, DropdownOptions, Data: string, log: string) {
        try {
            await browser.waitForAngular();
            await verify.verifyElement(Dropdown, log)
            await action.MouseMoveToElement(Dropdown, "Dropdown")
            await action.Click(Dropdown, log)
            await verify.verifyElement(DropdownOptions, "DropDown Options")
            let DropdownOptionsLocator = await action.makeDynamicLocatorContainsText(DropdownOptions, Data, "Equal")
            await action.Click(DropdownOptionsLocator, log)
        }
        catch (err) {
            action.ReportSubStep("selectSingleSelectDropDownOptions", "Requried dropdown not selected" + Data, "Fail");
            throw new Error(err);
        }
    }


    /**
    * desc: To select MultiSelect DropDownOptions
    * @method selectMultiSelectDropDownOptions
    * @author: Adithya
    * @param :{Dropdown Locator, DropdownOptions Locator, Data: Option to be selected}
    * @return none
    **/
    async selectMultiSelectDropDownOptions(Dropdown, DropdownOptions, Data: Array<string>, log: string) {
        try {
            await browser.waitForAngular();
            let CloseDropdown = by.css('a[class*="ui-multiselect-close"]')
            await verify.verifyElement(Dropdown, log)
            await action.MouseMoveToElement(Dropdown, "Dropdown")
            await this.eraseMultiselectDropdownSelection(Dropdown,log,"Dropdown")
            await verify.verifyElement(DropdownOptions, "DropDown Options")
            for (let i = 0; i < Data.length; i++) {
                let DropdownOptionsLocator = await action.makeDynamicLocatorContainsText(DropdownOptions, Data[i], "Equal")
                await action.Click(DropdownOptionsLocator, log + " " + Data[i])
            }
            await action.Click(CloseDropdown, "CloseDropdown")
        }
        catch (err) {
            action.ReportSubStep("selectMultiSelectDropDownOptions", "Requried dropdown not selected" + log, "Fail");
            throw new Error(err);
        }
    }


    /**
   * desc: verify Data In Assign PAV Ratings Programs Traks PAVTable
   * @method verifyDataInAssignPAVRatingsProgramsTraksPAVTable
   * @author: vikas
   * @param :{}
   * @return none
   */
    async verifyDataInAssignPAVRatingsProgramsTraksPAVTable(columnMainName: string, columnToVerifyName: string, textToverify: string, maintexttoMatch: string) {
        try {
            let locTableRow = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")]');
            let locColumns = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//th[contains(@class,"ui-sortable-column")]');
            let columnIndexMain: number;
            let columnIndex: number;
            let flag = false;
            let rowIndexG;

            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent();
            await browser.sleep(2000);
            await browser.waitForAngular();
            await element.all(locColumns).each(async function (ele, Index) {
                await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
                    await ele.getText().then(function (_valuecol) {
                        if (_valuecol.trim() == columnMainName) {
                            columnIndexMain = Index + 1;
                        }
                        if (_valuecol.trim() == columnToVerifyName) {
                            columnIndex = Index + 1;
                        }
                    });
                });
            }).then(async function () {


                await element.all(locTableRow).each(async function (ele, Index) {
                    let rowIndex = Index + 1
                    let locColumn = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndexMain + ']');
                    await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
                        await element(locColumn).getText().then(async function (_text) {
                            console.log("Main Text" + _text)
                            if (_text.trim().indexOf(maintexttoMatch) > -1) {
                                let locColumnReq = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndex + ']');
                                await browser.executeScript('arguments[0].scrollIntoView()', element(locColumnReq).getWebElement()).then(async function () {

                                    await element(locColumnReq).getText().then(function (_value) {
                                        console.log("subtext Text" + _value)
                                        if (_value.trim() == textToverify) {
                                            flag = true
                                            rowIndexG = rowIndex;
                                        }

                                    })
                                });
                            }
                        });
                    });
                }).then(async function () {
                    if (flag == true) {
                        await action.ReportSubStep("verifyDataInAssignPAVRatingsProgramsTraksPAVTable", "verify data " + textToverify, "Pass");
                    }
                    else {
                        await action.ReportSubStep("verifyDataInAssignPAVRatingsProgramsTraksPAVTable", "verify data " + textToverify, "Fail");
                    }
                });
            });

        }
        catch (err) {
            await action.ReportSubStep("verifyDataInAssignPAVRatingsProgramsTraksPAVTable", "verify data " + textToverify, "Fail");
        }

    }

    /**
     * desc: click Assign PAV Ratings Programs Traking PAVTable
     * @method clickAssignPAVRatingsProgramsTrakingPAVTable
     * @author: vikas
     * @param :{}
     * @return none
     */
    async clickAssignPAVRatingsProgramsTrakingPAVTable(clickColumnName: string, columnMainName: string, columnToVerifyName: string, textToverify: string, maintexttoMatch: string) {
        try {
            let locTableRow = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")]');
            let locColumns = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//th[contains(@class,"ui-sortable-column")]');
            let columnIndexMain: number;
            let columnIndex: number;
            let clickColumnIndex: number;
            let flag = false;
            let rowIndexG;
            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent();
            await browser.sleep(2000);
            await browser.waitForAngular();
            await element.all(locColumns).each(async function (ele, Index) {
                await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
                    await ele.getText().then(function (_valuecol) {
                        if (_valuecol.trim() == columnMainName) {
                            columnIndexMain = Index + 1;
                        }
                        if (_valuecol.trim() == columnToVerifyName) {
                            columnIndex = Index + 1;
                        }
                        if (_valuecol.trim() == clickColumnName) {
                            clickColumnIndex = Index + 1;
                        }
                    });
                });
            }).then(async function () {
                console.log("col main " + columnIndexMain);
                console.log("col col veri" + columnIndex);
                console.log("col click" + clickColumnIndex);
                await browser.sleep(2000);
                await element.all(locTableRow).each(async function (ele, Index) {
                    let rowIndex = Index + 1
                    let locColumn = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndexMain + ']');
                    await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
                        await browser.sleep(500);
                        await element(locColumn).getText().then(async function (_text) {
                            console.log("click text ++ " + _text);
                            if (_text.trim().indexOf(maintexttoMatch) > -1) {
                                let locColumnReq = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndex + ']');
                                await browser.actions().mouseMove(element(locColumnReq)).perform().then(async function () {

                                    await element(locColumnReq).getText().then(function (_value) {
                                        if (_value.trim().indexOf(textToverify) > -1) {
                                            flag = true
                                            rowIndexG = rowIndex;
                                        }

                                    })
                                });
                            }
                        });
                    });
                }).then(async function () {
                    if (flag == true) {
                        await browser.sleep(1000);
                        let locColumntoclick = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndexG + ']//td[contains(@class,"ui-resizable-column")][' + clickColumnIndex + ']//i[contains(@class,"pi pi-caret-down")]');
                        let locColumntAssign = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndexG + ']//td[contains(@class,"ui-resizable-column")][' + clickColumnIndex + ']//a[contains(text(),"Assign PAV")]');
                        await action.MouseMoveJavaScript(locColumntoclick, "column to click " + locColumntoclick);
                        await browser.sleep(1000);
                        await action.Click(locColumntoclick, "td with row " + rowIndexG + " column " + clickColumnIndex);
                        await action.Click(locColumntAssign, "assign link " + rowIndexG + " column " + clickColumnIndex);
                        action.ReportSubStep("clickAssignPAVRatingsProgramsTrakingPAVTable", "verify data" + textToverify, "Pass");
                    }
                    else {
                        await action.ReportSubStep("clickAssignPAVRatingsProgramsTrakingPAVTable", "verify data" + textToverify, "Fail");
                    }
                });
            });

        }
        catch (err) {
            await action.ReportSubStep("clickAssignPAVRatingsProgramsTrakingPAVTable", "verify data" + textToverify, "Fail");
        }

    }


    /**
       * desc:click Row In PAV Popup Grid
       * @method clickRowInPAVPopupGrid
       * @author: vikas
       * @param :{}
       * @return none
       */
    async clickRowInPAVPopupGrid(row: number, column: number) {
        try {

            let locator = by.xpath('//xg-grid[contains(@attr-name,"xgc-build-pav-rating-data-grid")]//tbody//tr[contains(@class,"ng-star-inserted")][' + row + ']//td[contains(@class,"ui-resizable-column")][' + column + ']//div[contains(@class,"ui-chkbox-box")]');
            await verify.verifyProgressBarNotPresent();
            await browser.sleep(2000);
            await action.MouseMoveToElement(locator, "row" + row);
            await action.Click(locator, "row " + row);
        }
        catch (err) {
            action.ReportSubStep("clickRowInPAVPopupGrid", "click row " + row + " col " + column, "Fail");
        }

    }

    /**
      * desc: click Data In Rating Source Table Tarcks Page
      * @method clickDataInRatingSourceTableTarcksPage
      * @author: vikas
      * @param :{}
      * @return _dataAll
      */
    async clickDataInRatingSourceTableTarcksPage(attributeName: string, textToVerify: string) {
        try {
            let locator = by.xpath('//xg-column-list[contains(@attr-name,"' + attributeName + '")]//ul[contains(@class,"list-container")]//li//div[normalize-space(text())="' + textToVerify + '"]|//xg-header[contains(text(),"' + attributeName + '")]/../..//ul[contains(@class,"list-container")]//li//div[normalize-space(text())="' + textToVerify + '"]');

            await action.MouseMoveToElement(locator, attributeName);
            await action.Click(locator, attributeName + " " + textToVerify);

        }
        catch (err) {
            await action.ReportSubStep("clickProgramInRatingsTracksView", "click source collection" + attributeName + " " + textToVerify, "Fail");
        }

    }

    /**
      * desc: click buttons in page
      * @method clickonButtonsInPage
      * @author: vikas
      * @param :{}
      * @return none
      */
    async clickonButtonsInPage(buttonAttribute: string, buttonName: string) {

        try {
            let Xpbutton = by.xpath('//xg-button[contains(@attr-name,"' + buttonAttribute + '")]//button[normalize-space(text())="' + buttonName + '"]');
            await browser.sleep(2000);
            await action.MouseMoveToElement(Xpbutton, buttonName)
            await action.Click(Xpbutton, buttonName);
        }
        catch (err) {
            action.ReportSubStep("clickonButtonsInPage", "click button " + buttonName, "Fail");
        }
    }

    /**
      * desc: click Program In Ratings Tracks View
      * @method clickProgramInRatingsTracksView
      * @author: vikas
      * @param :{}
      * @return 
      */
    async clickProgramInRatingsTracksView(programTitle: string) {
        try {
            let locator = by.xpath('//xg-list[contains(@attr-name,"xgc-list-programs")]//div[contains(@class,"xg-programs-item")]//div[contains(text(),"' + programTitle + '")]');
            await action.MouseMoveToElement(locator, programTitle);
            await browser.sleep(2000);
            await action.Click(locator, programTitle);

        }
        catch (err) {
            await action.ReportSubStep("clickProgramInRatingsTracksView", "click program name" + programTitle, "Fail");
        }

    }

    /**
   * desc: verify selling title value in Programs Traks
   * @method verifySellingTitleInProgramsList
   * @author: Venkata Sivakumar
   * @param :{sellingTitleColumnValue: string}
   * @return none
   */
    async verifySellingTitleInProgramsList(sellingTitleColumnValue: string) {
        try {
            let count = await action.getWebElementsCount(by.css("div.xg-programs-list li.list-item.ng-star-inserted"));
            for (let index = 1; index <= count; index++) {
                let actualValue = await action.GetText(by.css("div.xg-programs-list li.list-item.ng-star-inserted:nth-child(" + String(index) + ") div.xg-program-title"), "");
                if (String(actualValue).trim() != sellingTitleColumnValue.trim()) {
                    action.ReportSubStep("verifySellingTitleInProgramsList", "Verify " + sellingTitleColumnValue + " in Programs Track", "Fail");
                }
                else if (String(actualValue).trim() == sellingTitleColumnValue.trim()) {
                    break;
                }
            }
        }
        catch (error) {
            action.ReportSubStep("verifySellingTitleInProgramsList", "Verify " + sellingTitleColumnValue + " in Programs Track", "Fail");
        }
    }

    /**
      * desc: verify Active Inactive In PAV Ratings Programs Traks PAV Table
      * @method verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable
      * @author: vikas
      * @param :{}
      * @return none
      */
    async verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable(columnMainName: string, columnToVerifyName: string, textToverify: string, activeColName: string, trueFalse: string, maintexttoMatch: string) {
        try {
            let locTableRow = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")]');
            let locColumns = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//th[contains(@class,"ui-sortable-column")]');
            let columnIndexMain: number;
            let columnIndex: number;
            let activeCol: number;
            let flag = false;
            let rowIndexG;

            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent();
            await browser.waitForAngular();
            await element.all(locColumns).each(async function (ele, Index) {
                await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
                    await ele.getText().then(function (_valuecol) {
                        if (_valuecol.trim() == columnMainName) {
                            columnIndexMain = Index + 1;
                        }
                        if (_valuecol.trim() == columnToVerifyName) {
                            columnIndex = Index + 1;
                        }
                        if (_valuecol.trim() == activeColName) {
                            activeCol = Index + 1;
                        }
                    });
                });
            }).then(async function () {

                await browser.sleep(1000);
                await element.all(locTableRow).each(async function (ele, Index) {
                    let rowIndex = Index + 1
                    let locColumn = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndexMain + ']');
                    await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
                        await element(locColumn).getText().then(async function (_text) {
                            console.log("Main Text" + _text)
                            if (_text.trim().indexOf(maintexttoMatch) > -1) {
                                let locColumnReq = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndex + ']');
                                let locColumnReq1 = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + activeCol + ']//input[contains(@class,"mat-slide-toggle-input")]');
                                await browser.executeScript('arguments[0].scrollIntoView()', element(locColumnReq).getWebElement()).then(async function () {
                                    await element(locColumnReq).getText().then(async function (_value) {
                                        console.log("subtext Text" + _value)
                                        if (_value.trim().indexOf(textToverify) > -1) {
                                            await element(locColumnReq1).getAttribute('aria-checked').then(function (_atribute) {
                                                if (_atribute.trim() == trueFalse) {
                                                    flag = true
                                                    rowIndexG = rowIndex;
                                                }

                                            });

                                        }

                                    })
                                });
                            }
                        });
                    });
                }).then(async function () {
                    if (flag == true) {
                        await action.ReportSubStep("verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable", "verify data " + trueFalse, "Pass");
                    }
                    else {
                        await action.ReportSubStep("verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable", "verify data " + trueFalse, "Fail");
                    }
                });
            });

        }
        catch (err) {
            await action.ReportSubStep("verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable", "verify active or inactive Pav record " + trueFalse, "Fail");
        }

    }

    //Setting Page Methods
    async clickOnSettings() {
        try {
            elementStatus = await verify.verifyElementVisible(this.linkSettingsExpanded);
            if (!elementStatus) {
                await action.Click(this.linkSettings, "Click On Settings link");
                await verify.verifyElementIsDisplayed(this.linkSettingsExpanded, "Verify Settings menu is expanded");
            }
        } catch (error) {
            throw new Error(error);
        }
    }

    async clickOnSettingsSubMenuLink(subMenuLinkName: string) {
        try {
            elementStatus = await verify.verifyElementVisible(by.xpath(this.settingsSubMenuLinkExpanded.replace("dynamic", subMenuLinkName)));
            if (!elementStatus) {
                await action.Click(by.xpath(this.settingsSubMenuLink.replace("dynamic", subMenuLinkName)), "Click On " + subMenuLinkName);
                await verify.verifyElementIsDisplayed(by.xpath(this.settingsSubMenuLinkExpanded.replace("dynamic", subMenuLinkName)), "Verify " + subMenuLinkName + " is expanded");
            }

        } catch (error) {
            throw new Error(error);
        }
    }

    async clickOnDayPartDemoSubMenuLink(subMenuLinkName: string) {
        try {
            await action.Click(by.xpath(this.dayPartDemoSubMenuLink.replace("dynamic", subMenuLinkName)), "Click On '" + subMenuLinkName + "' sub link");
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
     * desc: Used to click on Add button in Demo Selector dialog
     * @method clickOnAddButtonInDemoSelectorDialog
     * @author: Venkata Sivakumar
     * @param :{}
     * @return none
     */
    async clickOnAddButtonInDemoSelectorDialog() {
        try {
            await action.Click(this.addPlusButton, "");
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
     * desc: Used to click on Add button in Demo Selector dialog footer section
     * @method clickOnAddButtonInDemoSelectorDialogFooter
     * @author: Venkata Sivakumar
     * @param :{}
     * @return none
     */
    async clickOnAddButtonInDemoSelectorDialogFooter() {
        try {
            await action.Click(this.addDemoButton, "");
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
    * desc: Used to add day part based on market option and day part option
    * @method addDayPartsDemo
    * @author: Venkata Sivakumar
    * @param :{marketOption:string,dayPartsOption:string}
    * @return none
    */
    async addDayPartsDemo(marketOption: string, dayPartsOption: string) {
        try {
            await this.selectSingleSelectDropDownOptions(this.marketsingleSelectDropDown, this.singleSelectDropdownOptionsLocator, marketOption, "Market")
            await browser.sleep(2000);
            await this.clickDataInRatingSourceTableTarcksPage("Dayparts", dayPartsOption.trim());
            await action.Click(this.addButton, "Click On Add Button");
            await this.clickDataInRatingSourceTableTarcksPage("Parent Block", "Adults");
            await this.clickDataInRatingSourceTableTarcksPage("Start Range", "18");
            await this.clickDataInRatingSourceTableTarcksPage("End Range", "20");
            await this.clickOnAddButtonInDemoSelectorDialog();
            await this.clickOnAddButtonInDemoSelectorDialogFooter();
            await browser.sleep(1000);
            await this.verifyPopupMessageRatecard(this.demoSuccessMessage);
        } catch (error) {
            throw new Error(error);
        }
    }

    async metricsDisplayOptionsPropValues() {
        try {

            let maxValueAttribute = "max"
            let currentValueAttribute = "ng-reflect-value"
            let metricsmaxValue = "3"
            let rtgDefaultValue = "1"
            let shrDefaultValue = "0"
            let hpDefaultValue = "1"
            let rtgMatSliderLocator = by.css("mat-slider[attr-name='xgc-mat-slider-rtg']")
            let shrMatSliderLocator = by.css("mat-slider[attr-name='xgc-mat-slider-shr']")
            let hpMatSliderLocator = by.css("mat-slider[attr-name='xgc-mat-slider-hp']")

            await verify.verifyElementAttribute(rtgMatSliderLocator, maxValueAttribute, metricsmaxValue, "RTG")
            await verify.verifyElementAttribute(shrMatSliderLocator, maxValueAttribute, metricsmaxValue, "SHR")
            await verify.verifyElementAttribute(hpMatSliderLocator, maxValueAttribute, metricsmaxValue, "HP")
            await verify.verifyElementAttribute(rtgMatSliderLocator, currentValueAttribute, rtgDefaultValue, "RTG")
            await verify.verifyElementAttribute(shrMatSliderLocator, currentValueAttribute, shrDefaultValue, "SHR")
            await verify.verifyElementAttribute(hpMatSliderLocator, currentValueAttribute, hpDefaultValue, "HP")
        }
        catch (error) {
            throw new Error(error);
        }
    }

    /**
    * desc: clickToggaleProgramsTracksPAVTable
    * @method clickToggaleProgramsTracksPAVTable
    * @author: vikas
    * @param :{}
    * @return none
    */
    async clickProgramLinkInLocalProgramGrid(clickColumnName: string, columnMainName: string, maintexttoMatch: string) {
        try {
            let locTableRow = by.xpath('//xg-grid[contains(@attr-name,"xgc-program-grid")]//table//tbody//tr[contains(@class,"ng-star-inserted")]');
            let locColumns = by.xpath('//xg-grid[contains(@attr-name,"xgc-program-grid")]//table//thead//th[contains(@class,"ui-sortable-column")]');
            let columnIndexMain: number;
            let clickColumnIndex: number;
            let flag = false;
            let rowIndexG;

            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent();
            await browser.sleep(2000);
            await browser.waitForAngular();
            await element.all(locColumns).each(async function (ele, Index) {
                await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
                    await ele.getText().then(function (_valuecol) {
                        if (_valuecol.trim() == columnMainName) {
                            columnIndexMain = Index + 2;
                        }

                        if (_valuecol.trim() == clickColumnName) {
                            clickColumnIndex = Index + 2;
                        }
                    });
                });
            }).then(async function () {
                console.log("Main Col " + columnIndexMain);
                console.log("click Col " + clickColumnIndex);
                await element.all(locTableRow).each(async function (ele, Index) {
                    let rowIndex = Index + 1
                    let locColumn = by.xpath('//xg-grid[contains(@attr-name,"xgc-program-grid")]//table//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndexMain + ']');
                    await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
                        await element(locColumn).getText().then(async function (_text) {
                            console.log("main Text " + _text);
                            if (_text.trim() == maintexttoMatch) {
                                flag = true
                                rowIndexG = rowIndex;
                            }
                        });
                    });
                }).then(async function () {
                    console.log("Flag  " + flag);
                    if (flag == true) {
                        await browser.sleep(1000);
                        let locColumntoclick = by.xpath('//xg-grid[contains(@attr-name,"xgc-program-grid")]//table//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndexG + ']//td[contains(@class,"ui-resizable-column")][' + clickColumnIndex + ']//a[contains(text(),"Link")]');
                        await action.MouseMoveToElement(locColumntoclick, "Mouse Move to Link ");
                        await action.Click(locColumntoclick, "click Link");
                    }
                    else {
                        await action.ReportSubStep("clickToggaleProgramsTracksPAVTable", "click program link " + maintexttoMatch, "Fail");
                    }
                });
            });

        }
        catch (err) {
            await action.ReportSubStep("clickToggaleProgramsTracksPAVTable", "click program link " + maintexttoMatch, "Fail");
        }

    }

    /**
    * desc: slide Decimal value In Mat Slider
    * @method slideDecimalvalueInMatSlider
    * @author: vikas
    * @param :{}
    * @return 
    */
    async slideDecimalvalueInMatSlider(nameOfMatslider: string, slideValue: Number) {
        try {
            let defalutV;
            let locatorMat = by.xpath('//mat-slider[contains(@attr-name,"' + nameOfMatslider + '")]');
            let locatorCircle = by.xpath('//mat-slider[contains(@attr-name,"' + nameOfMatslider + '")]');
            // await action.MouseMoveToElement(locatorMat, nameOfMatslider);
            await browser.actions().mouseMove(element(locatorMat)).perform();
            await browser.sleep(2000);
            await action.Click(locatorCircle, nameOfMatslider + "Slider Value");
            await browser.sleep(2000);
            let defalutValue = Number(await element(locatorMat).getAttribute("aria-valuenow"));
            //console.log("Default Value 1"+defalutValue);
            if (slideValue > defalutValue) {

                await browser.sleep(1000);
                defalutV = Number(await element(locatorMat).getAttribute("aria-valuenow"));
                await browser.sleep(1000);
                while (defalutV < slideValue) {
                    await element(locatorCircle).sendKeys(protractor.Key.ARROW_RIGHT);
                    // console.log("right Arrow Clicked ")
                    await browser.sleep(1000);
                    defalutV = Number(await element(locatorMat).getAttribute("aria-valuenow"));
                }
            }
            else {

                await browser.sleep(1000);
                defalutV = Number(await element(locatorMat).getAttribute("aria-valuenow"));
                await browser.sleep(1000);
                while (defalutV > slideValue) {
                    await element(locatorCircle).sendKeys(protractor.Key.ARROW_LEFT);
                    //console.log("Left Arrow Clicked ")
                    await browser.sleep(1000);
                    defalutV = Number(await element(locatorMat).getAttribute("aria-valuenow"));
                }
            }

        }
        catch (err) {
            await action.ReportSubStep("slideDecimalvalueInMatSlider", "click on slider" + nameOfMatslider, "Fail");
        }

    }

    /**
   * desc: Used to get the firstRow Metrics And Raings of first column of demo  
   * @method: getFirstRowMetricsAndRatings
   * @author: Venkata Sivakumar
   * @param :{}
   * @return metricsAndRatings
   */
    async getFirstRowMetricsAndRatings() {
        let metrics;
        let ratings;
        let actualMetrics: Array<any> = new Array<any>();
        let actualRatings: Array<any> = new Array<any>();
        let metricsValues: Array<any>;
        let ratingsValues: Array<any>;
        let metricsLength;
        let ratingsLength;
        let uiRTGValue;
        let uiSHRValue;
        let uiHPValue;
        let uiSIUValue;
        let metricsAndRatings;
        try {
            await action.MouseMoveJavaScript(this.firstRowMetrics, "firstRowMetrics")
            metrics = await action.GetText(this.firstRowMetrics, "");
            ratings = await action.GetText(this.firstRowRatings, "");
            metricsValues = metrics.trim().split("\n");
            ratingsValues = ratings.trim().split("\n");
            metricsValues.forEach(function (value) {
                if (value.trim() != "") {
                    actualMetrics.push(value.trim());
                }
            });
            ratingsValues.forEach(function (value) {
                if (value.trim() != "") {
                    actualRatings.push(value.trim());
                }
            });
            metricsLength = actualMetrics.length;
            ratingsLength = actualRatings.length;
            if (metricsLength == ratingsLength) {
                for (let index = 0; index < metricsLength; index++) {
                    switch (metricsValues[index]) {
                        case "RTG":
                            uiRTGValue = ratingsValues[index];
                            break;
                        case "SHR":
                            uiSHRValue = ratingsValues[index];
                            break;
                        case "HP":
                            uiHPValue = ratingsValues[index];
                            break;
                        case "SIU":
                            uiSIUValue = ratingsValues[index];
                            break;
                        default:
                            break;
                    }
                }
            }
            return metricsAndRatings = { "RTG": uiRTGValue, "SHR": uiSHRValue, "HP": uiHPValue, "SIU": uiSIUValue };
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
    * desc: Used to get the Share,Rating and HutPut values from API after calculation  
    * @method: getRatingsFromAPIAfterCalculation
    * @author: Venkata Sivakumar
    * @param :{dataDetails}
    * @return metricsAndRatings
    */
    async getRatingsFromAPIAfterCalculation(dataDetails) {
        try {
            let finalShare = 0;
            let finalRating = 0;
            let count = dataDetails.length;
            let finalPeopleUsingTelevision = 0;
            let finalPUT = 0;
            let finalSIU = 0;
            let metricsAndRatings;
            for (let index = 0; index < dataDetails.length; index++) {
                let apiProgramName = dataDetails[index].programName;
                let apiStartTime = dataDetails[index].startTime;
                let apiEndTime = dataDetails[index].endTime;
                let apiShareValue = dataDetails[index].share;
                let apiRating = dataDetails[index].rating;
                let apiTotalImpression = dataDetails[index].totalImpressions;
                let apiTotalUniverse = dataDetails[index].totalUniverse;
                let apiTotalHUTPUT = dataDetails[index].totalHUTPUT;
                let apiRatingHUTPUT = dataDetails[index].ratingHUTPUT;
                let apiRatingPeopleWatching = dataDetails[index].ratingPeopleWatching;
                let apiRatingUniverse = dataDetails[index].ratingUniverse;
                let apiUniverse = dataDetails[index].universe;
                let apiPeopleUsingTelevision = dataDetails[index].peopleUsingTelevision;
                finalRating = finalRating + (apiTotalImpression / apiTotalUniverse) * 100
                finalPeopleUsingTelevision = finalPeopleUsingTelevision + apiPeopleUsingTelevision;
                finalPUT = finalPUT + (apiRatingHUTPUT / apiRatingUniverse) * 100;
                finalSIU = finalSIU + (apiRatingHUTPUT / apiRatingUniverse) * 100;
                finalShare = finalShare + apiShareValue;
            }
            finalShare = finalShare / count;
            finalRating = finalRating / count;
            finalPUT = finalPUT / count;
            finalSIU = finalSIU / count;
            finalPeopleUsingTelevision = finalPeopleUsingTelevision / count;
            return metricsAndRatings = { "RTG": finalRating, "SHR": finalShare, "HP": finalPUT, "SIU": finalSIU };
        } catch (error) {
            throw new Error(error);
        }
    }


    /**
         * desc: click Multi select Drop Down Inventory Add Program 
         * @method clickMultiselectDropDownInventoryAddProgram
         * @author: vikas
         * @param :{}
         * @return none
         */
    async clickMultiselectDropDownInventoryAddProgram(attributeName: string) {
        try {
            let locator = by.css('p-dialog xg-multi-select[attr-name*="' + attributeName + '"] p-multiselect');
            await browser.sleep(3000);
            await action.MouseMoveToElement(locator, attributeName);
            await action.Click(locator, attributeName);
        }
        catch (err) {
            action.ReportSubStep("clickMultiselectDropDownInventoryAddProgram", "Error in  function", "Fail");
        }
    }

    /**
   * desc: enter Text In Global Filter
   * @method enterTextInGlobalFilter
   * @author: vikas
   * @param :{}
   * @return none
   */
    async enterTextInGlobalFilter(textToenter: string) {

        try {

            await action.MouseMoveToElement(this.globalFilter, "Global Filter");
            await action.ClearText(this.globalFilter, "GlobalFilter Clear");
            await action.SetText(this.globalFilter, textToenter, "Global Filter")

        }
        catch (err) {
            await action.ReportSubStep("enterTextInGlobalFilter", "Serach In Global filter", "Fail");
        }
    }

    /**
   * desc: 
   * @method SelectMarketOptions
   * @author: vikas
   * @param :{}
   * @return none
   */
    async SelectMarketOptions(optionName: string) {
        try {
            await action.Click(this.market, "Market")
            await browser.sleep(1000);
            await action.Click(by.xpath(this.marketOptions.replace("dynamic", String(optionName))), optionName);
        }
        catch (err) {
            action.ReportSubStep("SelectMarketOptions", "Error in  function", "Fail");
        }
    }


    /**
     * desc: It is used to click on Comscore button
     * @method clickOnComscore
     * @author: Venkata Sivakumar
     * @param :{}
     * @return none
     */
    async clickOnComscore() {
        let elementStatus: boolean;
        try {
            elementStatus = await verify.verifyElementVisible(this.comscoreButton);
            if (elementStatus) {
                await action.Click(this.comscoreButton, "Click On Comscore");
                await verify.verifyElementIsDisplayed(this.comscoreActiveButton, "Verify comscore is selected");
            }
            else if (!this.comscoreActiveButton) {
                action.ReportSubStep("clickOnComscore", "Either " + this.comscoreButton + " Or " + this.comscoreActiveButton + " locators are not availble to find the comscore webelement", "Fail");
            }
        } catch (error) {

        }
    }


    async metricsDisplayOptionsPropValuesComScore() {
        try {

            let maxValueAttribute = "max"
            let currentValueAttribute = "ng-reflect-value"
            let metricsmaxValue = "3"
            let rtgDefaultValue = "1"
            let shrDefaultValue = "0"
            let siuDefaultValue = "1"
            let rtgMatSliderLocator = by.css("mat-slider[attr-name='xgc-mat-slider-rtg']")
            let shrMatSliderLocator = by.css("mat-slider[attr-name='xgc-mat-slider-shr']")
            let siuMatSliderLocator = by.css("mat-slider[attr-name='xgc-mat-slider-siu']")

            await verify.verifyElementAttribute(rtgMatSliderLocator, maxValueAttribute, metricsmaxValue, "RTG")
            await verify.verifyElementAttribute(shrMatSliderLocator, maxValueAttribute, metricsmaxValue, "SHR")
            await verify.verifyElementAttribute(siuMatSliderLocator, maxValueAttribute, metricsmaxValue, "SIU")
            await verify.verifyElementAttribute(rtgMatSliderLocator, currentValueAttribute, rtgDefaultValue, "RTG")
            await verify.verifyElementAttribute(shrMatSliderLocator, currentValueAttribute, shrDefaultValue, "SHR")
            await verify.verifyElementAttribute(siuMatSliderLocator, currentValueAttribute, siuDefaultValue, "SIU")
        }
        catch (error) {
            throw new Error(error);
        }
    }

}
