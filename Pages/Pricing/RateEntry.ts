import { browser, element, by, protractor, promise, ElementFinder, ElementArrayFinder, ExpectedConditions } from 'protractor';
import { Gutils } from '../../Utility/gutils';
import { Reporter } from '../../Utility/htmlResult';
import { VerifyLib } from '../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../Libs/GeneralLibs/ActionLib';
import { AppCommonFunctions } from '../../Libs/ApplicationLibs/CommAppLib';

let gUtils = new Gutils();

let report = new Reporter();
let Verify = new VerifyLib();
let action = new ActionLib();
let commonLib = new AppCommonFunctions();



export class RateEntry {

   //8307
   //tr:nth-child(1) td:nth-child(12) xg-grid-data-cell input[name="numberInput"]
   //i[class="fa fa-asterisk"]


   marketName: string = "Pittsburgh"
   multipleChnnels: Array<string> = ["WPGH", "WPXI"]
   networkName: Array<string> = ["FOX"]
   channelsToBeVerifiedInGrid: string = "WPGH WPXI"
   networksToBeVerifiedInGrid: string = "FOX"
   startDateRentryView: string = "11/04/2019"
   endDateRentryView1: string = "11/17/2019" // 2weeks from start date
   endDateRentryView2: string = "12/01/2019" // 4 weeks from start date

   gross_RadioButton = by.xpath('//label[contains(text(),"Gross")]')
   baseLengthDropdown = by.css('xg-dropdown[attr-name="xgc-rate-entry-baselength"] label[class*="ui-dropdown-label"]')
   spotLengthDropdown = by.css('xg-dropdown[attr-name="xgc-rate-entry-spotlength"] label[class*="ui-dropdown-label"]')
   baseLengthDropdownOptions = by.css('li[class*="ui-dropdown-item"]')
   ratesInput_TextBox = by.css('xg-grid-data-cell input[name="numberInput"]')
   button_Confirm = by.css('button[title="Confirm"]')
   button_CopyRates = by.css('xg-button[attr-name="xgc-rate-entry-copyrates"] button[class*="xg-button"]')
   copyRatesFromWeekLink = by.css('li[title="From Week"] a')
   // button_Confirm = by.css('xg-button[attr-name="xgc-rate-entry-confirm"] button[class*="xg-button"]')
   button_Close = by.css('xg-button[attr-name="xgc-rate-entry-close"] button[class*="xg-button"]')
   button_Update = by.css('xg-button[attr-name="xgc-rate-entry-update"] button[class*="xg-button"][title="Update"]')
   button_Yes = by.css('xg-button[attr-name="xgc-rate-entry-yes"] button[class*="xg-button"]')
   button_No = by.css('button[title="No"]')
   // button_No = by.css('xg-button[attr-name="xgc-rate-entry-no"] button[class*="xg-button"]')
   button_Cancel = by.css('xg-button[label="Cattr-name="xgc-rate-entry-cancel"] button[class*="xg-button"]')
   button_Search = by.css('xg-button[attr-name="xgc-rate-entry-search"] button[class*="xg-button"]')
   dateRangesInPopUp = by.css('label[class="xg-text-label"]')
   variedRatesInPopUp = by.xpath('//label//following-sibling::span')
   week1program1rateInGrid = by.css('xg-grid tr:nth-child(1) td:nth-child(13) xg-grid-data-cell div div')
   week1program1rateInGrid_network = by.css('xg-grid tr:nth-child(1) td:nth-child(12) xg-grid-data-cell div div')
   loadedGrid = by.css('div[class*="xg-grid"]')
   cancelUpdatesPopup = by.xpath('//span[contains(text(),"Cancel Updates")]')
   Channel = by.css('xg-multi-select[attr-name="xgc-rate-entry-channels"] div[class*="ui-multiselect ui-widget"]')
   RateCard = by.css("xg-dropdown[attr-name='xgc-rate-entry-ratecard'] label[class*='ui-dropdown-label']")
   RateCardOptions = by.css('xg-dropdown[attr-name="xgc-rate-entry-ratecard"] span[class*="ng-star-inserted"]')
   startDate_Textbox = by.css('xg-date-picker[attr-name="xgc-rate-entry-startdate"] input[name*="DateInput"]')
   endDate_Textbox = by.css('xg-date-picker[attr-name="xgc-rate-entry-enddate"] input[name*="DateInput"]')
   notation = by.css("[class='fa fa-asterisk']")
   calculate = by.css("[title='Calculate']")
   Users_LocalAdmin = by.css('select[class="css-p3s1kf e1lk8yn71"] option:nth-child(2)')
   Users_NetworkAdmin = by.css('select[class="css-p3s1kf e1lk8yn71"] option:nth-child(1)')
   netOption = by.xpath("//label[text()='Net']")
   enterNetpercentage = by.css("input[name='numberInput']")
   NetworkMultiSelectDropdwon = by.css('xg-multi-select[attr-name="xgc-rate-entry-network"] div[class*="ui-multiselect-label"]')
   SelectAllInDropdown = by.css('div[class*="ui-chkbox-box"]')
   CloseDropdown = by.css('a[class="ui-multiselect-close ui-corner-all"]')
   Daypart = by.css('xg-multi-select[attr-name="xgc-rate-entry-daypart"] div[class*="ui-multiselect ui-widget"]')
   tag = by.css('xg-multi-select[attr-name="xgc-rate-entry-tag"] div[class*="ui-multiselect ui-widget"]')

   weekprogram1rateInGrid = by.css('xg-grid tr:nth-child(1) td:nth-child(12) xg-grid-data-cell div div');
   weekprogram2rateInGrid = by.css('xg-grid tr:nth-child(1) td:nth-child(14) xg-grid-data-cell div div');
   DropDownOptions = by.css('li[class*="ui-multiselect-item ui-corner-all"]')

   searchText = by.css("[type='number']")
   pDropdown = by.xpath("//tr[2]/th[2]/xg-grid-column-filter/div/div/div/p-dropdown")
   filterWithGreaterthan = by.css("[title='Greater Than']")
   filterWithLessthan = by.css("[title='Less Than']")
   filterwithGequal = by.css("title='Greater Than or Equal'")
   closeSearch = by.css("[class='far fa-times-circle ng-star-inserted']")
   addSpotLength = by.css("[title='Add']")
   editSpotLength = by.css("[name='numberInput']")
   clickSort = by.css("[arialabel='Activate to sort']")
   selectNw = by.xpath("//span[text()='NBC']")
   delete = by.css("[title='Delete']")
   selectSpotLength = by.xpath("//p-table//div[contains(@class,'ui-table-unfrozen-view')]//tr//td[1]")
   deleteDisabled = by.css("[title='Delete'][disabled]")
   deletepopup = by.xpath("//div[@role='dialog']//span[text()='Delete Spot Length']")
   confirmDelete = by.css("[title='Yes']")
   noData = by.xpath("//td[text()=' No data match is found ']")
   update = by.css("[title='Update']")

   /*
        * desc: To Validate grid values in the view 
        * @method GridValidationNp
        * @author: Adithya 
        * @param :{ColumnName:Column Name which is to be Validated,data:Testdata to be Entered}
        * @return none
     */
   async GridValidationsUpdated(ColumnName: string, data: string) {
      try {
         //await browser.waitForAngular();
         await browser.sleep(1500);
         await element.all(by.tagName('th')).each(async function (value, index) {
            value.getText().then(async function (text) {
               if (text == ColumnName) {
                  console.log("index of column is " + index, "Name of column is " + text)
                  let ColNo = Number(index) + 1
                  console.log("Column no is" + ColNo)
                  await browser.sleep(1000)
                  await Verify.verifyElement(by.css('xg-grid tr td:nth-child(' + ColNo + ')'), text)
                  await action.MouseMoveToElement(by.css('xg-grid tr td:nth-child(' + ColNo + ')'), text)
                  await element.all(by.css('xg-grid tr td:nth-child(' + ColNo + ')')).each(function (value) {
                     value.getText().then(function (text) {
                        console.log(ColumnName + " is " + text)
                        if (data.includes(text) || text.includes(data)) {
                           action.ReportSubStep("GridValidations", "Data available " + text, "Pass");
                        }
                        else {
                           action.ReportSubStep("GridValidations", "Data Not available " + text, "Fail");
                        }
                     })
                  })
               }

            })
         })

      }
      catch (err) {
         throw new Error('Error');
      }
   }

   /**
   * desc: To generate Random numbers for id's
   * @method makeNumericId 
   * @author: Adithya 
   * @param :{}   
   * @return none
   
   */

   async makeNumericId(length) {
      let text = "";
      let possible = "0123456789";

      for (var i = 0; i < length; i++)
         text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
   }

   /**
        * desc: To Verify ColumnName Displayed In Grid
        * @method VerifyColumnNameDisplayedInGrid
        * @author: Siva
        * @param :{}
        * @return none
        */
   async ColumnNameValidations(ColumnName: Array<any>) {
      try {
         //await browser.waitForAngular();
         for (let i = 0; i < ColumnName.length; i++) {
            let Component = by.xpath('//th[contains(text(),"' + ColumnName[i] + '")]');
            await browser.executeScript('arguments[0].scrollIntoView()', element(Component).getWebElement());
            await browser.sleep(500);
            await Verify.verifyElementIsDisplayed(Component, ColumnName[i]);
            action.ReportSubStep("VerifyColumnNameDisplayedInGrid", "Displayed " + " " + ColumnName[i] + "", "Pass");
         }
      }
      catch (err) {
         action.ReportSubStep("VerifyColumnNameDisplayedInGrid", "Not Displayed " + ColumnName + "", "Fail");
         throw new Error(err);
      }

   }

   async ClickOnSingleSelectDropDownUsingLabel(Label: string, Data: string, log: string) {
      try {
         //await browser.waitForAngular();
         let Dropdown = by.css('xg-dropdown[attr-name="' + Label + '"] label[class*="ui-dropdown-label"]')
         let DropdownOptions = by.css('xg-dropdown[attr-name="' + Label + '"] span[class*="ng-star-inserted"]')
         await browser.sleep(500);
         await Verify.verifyElement(Dropdown, log)
         await action.MouseMoveToElement(Dropdown, "Dropdown")
         await action.Click(Dropdown, log)
         await Verify.verifyElement(DropdownOptions, "DropDown Options")
         let DropdownOptionsLocator = await action.makeDynamicLocatorContainsText(DropdownOptions, Data, "Equal")
         await action.Click(DropdownOptionsLocator, log)
      }
      catch (err) {
         action.ReportSubStep("ClickOnSingleSelectDropDownUsingLabel", "Requried dropdown not selected" + Label, "Fail");
         throw new Error(err);
      }
   }

   async SelectMultiSelectDropDownOptions(Dropdown, DropdownOptions, Data: Array<string>, log: string) {
      try {
         //await browser.waitForAngular();
         await browser.sleep(500);
         await Verify.verifyElement(Dropdown, log)
         await action.MouseMoveToElement(Dropdown, "Dropdown")
         await action.Click(Dropdown, log)
         await Verify.verifyElement(DropdownOptions, "DropDown Options")
         for (let i = 0; i < Data.length; i++) {
            let DropdownOptionsLocator = await action.makeDynamicLocatorContainsText(DropdownOptions, Data[i], "Equal")
            await action.Click(DropdownOptionsLocator, log + " " + Data[i])
         }
         await action.Click(this.CloseDropdown, "CloseDropdown")
      }
      catch (err) {
         action.ReportSubStep("ClickOnSingleSelectDropDownUsingLabel", "Requried dropdown not selected" + log, "Fail");
         throw new Error(err);
      }
   }

   /**
    * desc: To Erase MultiselectDropdown Selection
    * @method EraseMultiselectDropdownSelection
    * @author: Adithya
    * @param :{Dropdown:locator of the Dropdown element,log:Related Label Name}
    * @return none
    */
   async EraseMultiselectDropdownSelection(Dropdown, log: any) {
      try {
         //await browser.waitForAngular();

         await action.Click(Dropdown, "Dropdown" + log)
         await action.Click(element.all(this.SelectAllInDropdown).get(0), "Select All")
         await action.Click(element.all(this.SelectAllInDropdown).get(0), "Select All")
         await action.Click(this.CloseDropdown, "CloseDropdown")
         await browser.sleep(500)
         action.ReportSubStep("EraseMultiselectDropdownSelection", "Drop down selection Made empty", "Pass");
      }
      catch (err) {
         action.ReportSubStep("EraseMultiselectDropdownSelection", "Drop down selection Not Made empty", "Fail");
         throw new Error(err);
      }

   }
   /**
    * desc: To select All In Multiselect Dropdown
    * @method selectAllInMultiselectDropdown
    * @author: Adithya
    * @param :{Dropdown:locator of the Dropdown element,log:Related Label Name}
    * @return none
    */
   async selectAllInMultiselectDropdown(Dropdown, log: any) {
      try {
         //await browser.waitForAngular();

         await action.Click(Dropdown, "Dropdown" + log)
         await action.Click(element.all(this.SelectAllInDropdown).get(0), "Select All")
         await action.Click(this.CloseDropdown, "CloseDropdown")
         await browser.sleep(500)
         action.ReportSubStep("selectAllInMultiselectDropdown", "Drop down selection Made empty", "Pass");
      }
      catch (err) {
         action.ReportSubStep("selectAllInMultiselectDropdown", "Drop down selection Not Made empty", "Fail");
         throw new Error(err);
      }

   }

   /**
* desc: To verify and click on Dropdown & Dropdown Options- to select required Dropdown Option
* @method VerifyDropDown
* @author: Adithya
* @param :{Dropdown:locator of the Dropdown element,DropdownOptions:locator of the Dropdown Options element,data:Testdata to be Entered, log:Related Label Name
* @return none
*/
   async VerifyDropDown(Dropdown, DropdownOptions, Data: Array<any>, log: string) {
      try {
         await browser.waitForAngular();
         await browser.sleep(500);
         await Verify.verifyElement(Dropdown, log)
         await action.MouseMoveToElement(Dropdown, "Dropdown")
         await action.Click(Dropdown, log)
         await Verify.verifyElement(DropdownOptions, "DropDown Options")
         await element.all(DropdownOptions).each(async function (Options) {
            Options.getText().then(function (text) {
               for (let i = 0; i < Data.length; i++) {
                  if (text.includes(Data[i])) {
                     console.log(text + " Option Available")
                  }
                  else {
                  }
               }
            })
         })
         await element.all(DropdownOptions).each(async function (Options) {
            Options.getText().then(function (text) {
               for (let i = 0; i < Data.length; i++) {
                  if (text.includes(Data[i])) {
                     Options.click()
                     console.log("Clicked on " + text)
                  }
                  else {
                  }
               }
            })
         })
         await action.Click(this.CloseDropdown, "CloseDropdown")
      }
      catch (err) {
         throw new Error(err);
      }
   }

   /**
      * desc: To Verify column contain data in grid
      * @method  ColumnContainGivenData
      * @author: Siva
      * @param :{ColumnName:Column Name which is to be Validated,}
      * @return none
      */

   async ColumnContainGivenData(columnheader: string, data: string) {
      let ColNo;
      await element.all(by.xpath('//p-table//div[contains(@class," ui-table-unfrozen-view")]//table[contains(@class,"ui-table-scrollable-header-table")]//thead//tr[1]//th[contains(@class,"ui-sortable-column")]')).each(async function (value, index) {
         await browser.actions().mouseMove(value).perform().then(async function () {
            await value.getText().then(async function (text) {
               if (text.trim() == columnheader) {
                  ColNo = Number(index) + 1
               }
            })
         }, function (err) { });
      })
         .then(async function () {
            let columns = await element(by.xpath('//p-table//div[contains(@class,"ui-table-unfrozen-view")]//tr[1]//td[contains(@class,"ui-resizable-column noTopBottomBorder ng-star-inserted")][' + ColNo + ']'));
            await action.MouseMoveToElement(columns, " dhsk");
            await columns.getText().then(async function (_text) {
               console.log("value of " + columnheader + " is:" + _text)
               if (_text.trim().includes(data)) {
                  action.ReportSubStep("GridValidations", "Data available " + _text, "Pass");
               }
               else {

                  action.ReportSubStep("GridValidations", "Data Not available " + _text, "Fail");
               }
            });
         })
   }
   /**
           * desc: To Verify ColumnName is not Displayed In Grid
           * @method ColumnNameNotDisplayedValidations
           * @author: Siva
           * @param :{}
           * @return none
           */
   async ColumnNameNotDisplayedValidations(ColumnName: Array<any>) {
      try {
         for (let i = 0; i < ColumnName.length; i++) {
            let Component = by.xpath('//th[contains(text(),"' + ColumnName[i] + '")]');
            await browser.sleep(500);
            await Verify.verifyNotPresent(Component, ColumnName[i]);
            action.ReportSubStep("VerifyColumnNameDisplayedInGrid", "Not Displayed " + ColumnName[i] + "", "Pass");

         }
      }
      catch (err) {
         action.ReportSubStep("VerifyColumnNameDisplayedInGrid", "Displayed " + " " + ColumnName + "", "Fail");
         throw new Error(err);
      }

   }
   /**
    * desc: To verify spotlengths sorder order in grid
    * @method GridValidationsUpdated1
    * @author: Siva
    * @param :{}
    * @return none
    */

   async GridValidationsUpdated1(ColumnName: string, data: string, operator: string) {
      try {
         //await browser.waitForAngular();
         await browser.sleep(1500);
         await element.all(by.tagName('th')).each(async function (value, index) {
            value.getText().then(async function (text) {
               if (text == ColumnName) {
                  let ColNo = Number(index) + 1
                  await browser.sleep(1000)
                  await Verify.verifyElement(by.css('xg-grid tr td:nth-child(' + ColNo + ')'), text)
                  await action.MouseMoveToElement(by.css('xg-grid tr td:nth-child(' + ColNo + ')'), text)
                  await element.all(by.css('xg-grid tr td:nth-child(' + ColNo + ')')).each(function (value) {
                     value.getText().then(function (text) {
                        if (operator = 'GT') {
                           if (text > data) {
                              action.ReportSubStep("GridValidations", "Data available " + text, "Pass");
                           }
                           else {
                              action.ReportSubStep("GridValidations", "Data Not available " + text, "Fail");
                           }
                        }
                        if (operator = 'EQ') {

                           if (text = data) {
                              action.ReportSubStep("GridValidations", "Data available " + text, "Pass");
                           }
                           else {
                              action.ReportSubStep("GridValidations", "Data Not available " + text, "Fail");
                           }
                        }
                     })
                  })
               }

            })
         })

      }
      catch (err) {
         throw new Error('Error');
      }
   }
   /**
    * desc: To verify spotlengths order in grid
    * @method verifysortorderforlenthfactor
    * @author: Siva
    * @param :{}
    * @return none
    */
   async verifysortorderforspotlengths() {
      let sIndex = 1;
      const results = [];
      var ele = element.all(by.xpath("//p-table//div[contains(@class,'ui-table-unfrozen-view')]//tr[contains(@class,'noTopBottomBorder')]"))
      ele.each(function () {
         element.all(by.xpath("//p-table//div[contains(@class,'ui-table-unfrozen-view')]//tr[contains(@class,'noTopBottomBorder')][" + sIndex + "]//td[2]")).getText().then(function (value) {
            //console.log(value);
            results.push(value)
         })
         sIndex = sIndex + 1;
      }).then(function () {

         console.log("Results", results);
         let Ascendingresults = results.slice();
         Ascendingresults = Ascendingresults.sort(function (a, b) { return a - b });
         console.log("Ascending Order", Ascendingresults);
         expect(Ascendingresults).toEqual(results);

         if (JSON.stringify(Ascendingresults) == JSON.stringify(results))
            console.log('market Ranks are in Ascending order by default')
         else
            console.log('market Ranks are not in Ascending order By default')

      })
   }


   async GridValidationsUpdated2(ColumnName: string, data: string, operator: string) {
      try {
            let failedCount = 0;
            let errorMessage = "";
            let actualValues:Array<string> = await commonLib.getTableColumnValues(ColumnName);
            if (operator == "GT") {
               for(let index = 0; index < actualValues.length;index++)
               {
                  if(!(Number(actualValues[index]) > Number(data)))
                  {
                     failedCount = failedCount + 1;
                     errorMessage = errorMessage + actualValues[index]+" is displayed at row number"+(index+1)
                  }
               }
               if(failedCount > 0)
               {
                  await action.ReportSubStep("GridValidationsUpdated2",errorMessage,"Fail")
               }
            }
            else if(operator == "EQ")
            {
               for(let index = 0; index < actualValues.length;index++)
               {
                  if(!(Number(actualValues[index]) == Number(data)))
                  {
                     failedCount = failedCount + 1;
                     errorMessage = errorMessage + actualValues[index]+" is displayed at row number"+(index+1)
                  }
               }
               if(failedCount > 0)
               {
                  await action.ReportSubStep("GridValidationsUpdated2",errorMessage,"Fail")
               } 
            }
            
      }
      catch (err) {
         throw new Error('Error');
      }
   }

}