import { browser, element, by, protractor, promise } from 'protractor';
import { Gutils } from '../../../Utility/gutils';
import { ActionLib } from '../../../Libs/GeneralLibs/ActionLib';
import { VerifyLib } from '../../../Libs/GeneralLibs/VerificationLib';
import { globalTestData } from '../../../TestData/globalTestData';


let gUtils = new Gutils();
let action = new ActionLib();
let verify = new VerifyLib();

export class LocalLengthFactorsPricing {

   //view, add
   gridLocator = by.css('div[class="xg-grid"]')
   lengthFactorsHeader = by.xpath('//h3[contains(text(),"Length Factors")]')
   headerLengthFactor = by.css('[class*="xg-main-header"]')
   marketsingleSelectDropDown = by.css('xg-dropdown[attr-name*="market"] label[class*="ui-dropdown-label"]')
   networksingleSelectDropDown = by.css('xg-dropdown[attr-name*="network"] label[class*="ui-dropdown-label"]')
   networkMultiSelectDropDown = by.css('xg-multi-select[attr-name*="network"] label[class*="ui-multiselect-label"]')
   channelMultiSelectDropDown = by.css('xg-multi-select[attr-name*="channel"] label[class*="ui-multiselect-label"]')
   singleSelectDropdownOptionsLocator = by.css('li[class*="ui-dropdown-item"]')
   multiSelectDropdownOptionsLocator = by.css('li[class*="ui-multiselect-item"]')
   singleSelectDropdownLocator = 'xg-dropdown[attr-name*="dynamic"] label[class*="ui-dropdown-label"]'
   mutliSelectDropdownLocator = 'xg-multi-select[attr-name*="dynamic"] label[class*="ui-multiselect-label"]'
   popUpSingleSelectDropdownLocator = 'popup xg-dropdown[attr-name*="dynamic"] label[class*="ui-dropdown-label"]'
   popUpMutliSelectDropdownLocator = 'popup xg-multi-select[attr-name*="dynamic"] label[class*="ui-multiselect-label"]'
   lengthFactorViewspotlengths = by.xpath('//div[contains(@class,"xg-list-grid")]//ul//li[contains(text(),"")]//preceding-sibling::li')
   rateEntryViewspotlengths = by.xpath('//span[contains(@class,"mat-checkbox-label")] [contains(text(),"s")]')
   defaultLengthFactors = by.xpath('//div[contains(@class,"xg-list-grid")]//ul//li[contains(text(),"")]//following-sibling::li')
   globalFilterInput = by.css('div[class*="globalFilter"] input')
   textBox_locator = 'xg-input[attr-name*="dynamic"] input[class*="xg-text-box"]'
   textArea_locator = 'xg-textarea[attr-name*="dynamic"] textarea[class*="xg-text-box"]'
   datesTextBox_locator = 'xg-date-picker[attr-name*="dynamic"] input[name*="DateInput"]'
   roundToDropdown = by.css('xg-dropdown[name*="Round To"] label[class*="ui-dropdown-label"]')
   columnPicker = by.css('mat-icon[aria-label="Column Picker"]')
   // defalutLinkLF = by.cssContainingText('a', 'Default')
   defalutLinkLF = by.css('a[attr-name="xgc-add-edit-anchor-length-factor"]')
   closePopUp = by.css('a[class*="ui-dialog-titlebar-close"] span')

   //clone
   latestRecordCheckbox = by.css('xg-grid tr:nth-child(1) td:nth-child(1)')
   clearAllColumTextFilter = by.css('i[class*="far fa-times-circle"][mattooltip="Clear All"]')
   latestChannelNames = by.css('xg-grid tr:nth-child(1) td:nth-child(3)')
   latestDescription = by.css('xg-grid tr:nth-child(1) td:nth-child(5)')
   latestDescription_Network = by.css('xg-grid tr:nth-child(1) td:nth-child(4)')
   gridCheckboxesSelection = by.css('xg-grid tr td:nth-child(1)')
   addedDaypartTagDeviation = by.xpath('//div[contains(@class,"xg-group-list-head")]//following-sibling::ul//li//a')
   deviationDefaultLFInputBox = by.xpath('//div[contains(@class,"xg-list-grid")]//ul//li[contains(text(),"")][2]//following-sibling::li//input')
   // addDeviation = by.css('div[class="xg-group-list-head"] i')
   addDeviation = by.css('i[attr-name="xgc-add-edit-anchor-length-deviation-toggle"]')
   cancelBtn_SpotLengths = by.css('xg-button[label="Cancel"] button')

   //Delete
   latestGroupName = by.css('xg-grid tr:nth-child(1) td:nth-child(4)')
   secondGroupName = by.css('xg-grid tr:nth-child(2) td:nth-child(4)')
   latestGroupName_Network = by.css('xg-grid tr:nth-child(1) td:nth-child(3)')
   secondGroupName_Network = by.css('xg-grid tr:nth-child(2) td:nth-child(3)')
   secondRecordCheckbox = by.css('xg-grid tr:nth-child(2) td:nth-child(1)')

   spotLenthadd = by.css("[title='Add']");
   addSpot = by.css("input[ng-reflect-maxlength='5']")
   addLength = by.css("input[ng-reflect-maxlength='8']")
   save = by.css("[title='Save']")
   editSpotLength = by.css("[name='numberInput']")
   selectSpotLength = by.xpath("//p-table//div[contains(@class,'ui-table-unfrozen-view')]//tr//td[1]")
   update = by.css("[title='Update']")
   searchspotlength = by.css("input[type='number']")

   tagsDropDown = by.css("xg-multi-select[attr-name='xgc-add-edit-tags-length-factor'] div[class*='multiselect-trigger']");


   /**
    * desc: To select Date From Calender icon
    * @method selectDateFromCalender
    * @author: Adithya
    * @param :{attr_name: attr-name of the calender icon, dateBeSelected: Ex:02, monthToBeSelected: ex:Aug, yearToBeSelected: ex:2019}
    * @return none
    */
   async selectDateFromCalender(attr_name: string, dateBeSelected: number, monthToBeSelected: string, yearToBeSelected: string) {
      try {
         let calenderIcon = by.css('xg-date-picker[attr-name*="' + attr_name + '"] i')
         let monthYearCalender = by.css('div[class*="xg-headerMonthYear"] a')
         let calenderDates = by.css('div[class*="xg-datevalue"]')
         let calenderMonths = by.css('div[class*="monthvalue"]')
         let calenderYears = by.css('div[class*="yearvalue"]')

         await browser.waitForAngular();
         await action.Click(calenderIcon, "calenderIcon")
         await browser.sleep(1000)
         await action.Click(element.all(monthYearCalender).get(1), "calenderYears")
         let Year = await action.makeDynamicLocatorContainsText(calenderYears, yearToBeSelected, "Equal")
         await action.Click(Year, yearToBeSelected)
         await action.Click(element.all(monthYearCalender).get(0), "calenderMonths")
         let Month = await action.makeDynamicLocatorContainsText(calenderMonths, monthToBeSelected, "Equal")
         await action.Click(Month, monthToBeSelected)
         await browser.sleep(1000)
         let Date = await action.makeDynamicLocatorContainsText(calenderDates, dateBeSelected, "Equal")
         await action.Click(Date, "date " + dateBeSelected)
         action.ReportSubStep("selectDateFromCalender", "Date Selected as per given inputs", "Pass");
      }
      catch (err) {
         action.ReportSubStep("selectDateFromCalender", "Date not Selected as per given inputs", "Fail");
         throw new Error(err);
      }
   }


   /**
   * desc: To verify Default SpotLengths
   * @method verifyDefaultSpotLengths
   * @author: Adithya
   * @param :{}
   * @return none
   **/
   async verifyDefaultSpotLengths() {
      try {
         await browser.waitForAngular();
         let fivesLength = await action.makeDynamicLocatorContainsText(this.lengthFactorViewspotlengths, "5s", "Equal")
         await verify.verifyElement(fivesLength, "fivesLength")
         let tensLength = await action.makeDynamicLocatorContainsText(this.lengthFactorViewspotlengths, "10s", "Equal")
         await verify.verifyElement(tensLength, "tensLength")
         let fifteensLength = await action.makeDynamicLocatorContainsText(this.lengthFactorViewspotlengths, "15s", "Equal")
         await verify.verifyElement(fifteensLength, "fifteensLength")
         let sixtysLength = await action.makeDynamicLocatorContainsText(this.lengthFactorViewspotlengths, "60s", "Equal")
         await verify.verifyElement(sixtysLength, "sixtysLength")
      }
      catch (err) {
         action.ReportSubStep("verifyDefaultSpotLengths", "Default spot lengths not available ", "Fail");
         throw new Error(err);
      }
   }

   /**
   * desc: To select Single Select DropDownOptions
   * @method selectSingleSelectDropDownOptions
   * @author: Adithya
   * @param :{Dropdown Locator, DropdownOptions Locator, Data: Option to be selected}
   * @return none
   **/
   async selectSingleSelectDropDownOptions(Dropdown, DropdownOptions, Data: string, log: string) {
      try {
         await browser.waitForAngular();
         await verify.verifyElement(Dropdown, log)
         await action.MouseMoveToElement(Dropdown, "Dropdown")
         await action.Click(Dropdown, log)
         await verify.verifyElement(DropdownOptions, "DropDown Options")
         let DropdownOptionsLocator = await action.makeDynamicLocatorContainsText(DropdownOptions, Data, "Equal")
         await action.Click(DropdownOptionsLocator, log)
      }
      catch (err) {
         action.ReportSubStep("selectSingleSelectDropDownOptions", "Requried dropdown not selected" + Data, "Fail");
         throw new Error(err);
      }
   }

   /**
   * desc: To Verify Page Header In Grid
   * @method verifyPageHeader
   * @author: Adithya
   * @param :{headerName: headerName to be Verified}
   * @return none
   **/
   async verifyPageHeader(headerName: string) {
      try {
         await browser.waitForAngular();
         let Component = by.cssContainingText('[class*="xg-main-header"]', headerName)
         await verify.verifyElement(Component, headerName);
         action.ReportSubStep("verifyPageHeader", "Displayed " + " " + headerName + "", "Pass");

      }
      catch (err) {
         action.ReportSubStep("verifyPageHeader", "Not Displayed " + headerName + "", "Fail");
         throw new Error(err);
      }

   }

   /**
   * desc: To Verify ColumnName Displayed In Grid
   * @method VerifyColumnNameDisplayedInGrid
   * @author: Adithya
   * @param :{ColumnName: Column Names to be Verified}
   * @return none
   **/
   async VerifyColumnNameDisplayedInGrid(ColumnName: Array<any>) {
      try {
         await browser.waitForAngular();
         for (let i = 0; i < ColumnName.length; i++) {
            let Component = by.xpath('//th[contains(text(),"' + ColumnName[i] + '")]');
            await browser.executeScript('arguments[0].scrollIntoView()', element(Component).getWebElement());
            await verify.verifyElementIsDisplayed(Component, ColumnName[i]);
            action.ReportSubStep("VerifyColumnNameDisplayedInGrid", "Displayed " + " " + ColumnName[i] + "", "Pass");
         }
      }
      catch (err) {
         action.ReportSubStep("VerifyColumnNameDisplayedInGrid", "Not Displayed " + ColumnName + "", "Fail");
         throw new Error(err);
      }

   }

   /**
   * desc: To select MultiSelect DropDownOptions
   * @method selectMultiSelectDropDownOptions
   * @author: Adithya
   * @param :{Dropdown Locator, DropdownOptions Locator, Data: Option to be selected}
   * @return none
   **/
   async selectMultiSelectDropDownOptions(Dropdown, DropdownOptions, Data: Array<string>, log: string) {
      try {
         await browser.waitForAngular();
         let CloseDropdown = by.css('a[class*="ui-multiselect-close"]')
         await verify.verifyElement(Dropdown, log)
         await action.MouseMoveToElement(Dropdown, "Dropdown")
         await this.eraseMultiselectDropdownSelection(Dropdown,log,"Dropdown")
         await verify.verifyElement(DropdownOptions, "DropDown Options")
         for (let i = 0; i < Data.length; i++) {
            let DropdownOptionsLocator = await action.makeDynamicLocatorContainsText(DropdownOptions, Data[i], "Equal")
            await action.Click(DropdownOptionsLocator, log + " " + Data[i])
         }
         await action.Click(CloseDropdown, "CloseDropdown")
      }
      catch (err) {
         action.ReportSubStep("selectMultiSelectDropDownOptions", "Requried dropdown not selected" + log, "Fail");
         throw new Error(err);
      }
   }



   /**
   * desc: To verify DropDown Option Available
   * @method verifyDropDownOptionAvailable
   * @author: Adithya
   * @param :{Dropdown Locator, DropdownOptions Locator, Data: Options to be verified}
   * @return none
   **/
   async verifyDropDownOptionAvailable(DropDown, DropDownOptions, Data: Array<string>, log: string) {
      try {
         await browser.waitForAngular();
         let OptionsArray: Array<string> = []
         await action.Click(DropDown, log)
         await browser.sleep(2000)
         await element.all(DropDownOptions).each(function (item) {
            item.getText().then(function (options) {
               OptionsArray.push(options)
            })
         })

         for (let i = 0; i < Data.length; i++) {
            if (OptionsArray.includes(Data[i])) {
               await console.log(Data[i] + " available")
               action.ReportSubStep("verifyDropDownOptionAvailable", Data[i] + " option available", "Pass")

            }
            else {
               action.ReportSubStep("verifyDropDownOptionAvailable", Data[i] + " option not available", "Fail")
            }
         }
         await action.Click(DropDown, log)
      }
      catch (err) {
         action.ReportSubStep("verifyDropDownOptionAvailable", Data + " option not available ", "Fail");
         throw new Error('Error');
      }
   }

   /**
   * desc: To verify & validate Days checkboxes displayed in Row of Grid 
   * @method verifyRowDaysCheckboxesArray
   * @author: Adithya
   * @param :{Row: Row No to be validated, ColumnName: Column to be validated, type: type of Validation or Action, label: Label of Checkbox}
   * @return none
   */
   async buttonActionsVerify(linkname: Array<string>, type: string, label: string) {
      try {
         await browser.waitForAngular();

         for (let i = 0; i < linkname.length; i++) {

            linkname[i] = linkname[i].toLowerCase()
            let component = by.css('xg-button[attr-name*="' + linkname[i] + '"] button[class*="xg-button"]');
            await verify.verifyElement(component, "CheckBox")
            if (type == "Displayed") {
               await verify.verifyElement(component, label)
               action.ReportSubStep("verifyDays", label + " Element Displayed", "Pass")
            }
            else if (type == "Click") {
               await verify.verifyElement(component, label)
               await action.MouseMoveToElement(component, label)
               await action.Click(component, label)
            }

            else if (type == "Enabled") {
               await verify.verifyElementIsEnabled(component, label)
               action.ReportSubStep("verifyDays", label + " Element Enabled", "Pass")
            }
            else if (type == "Disabled") {
               await verify.verifyElementIsDisabled(component, label)
               action.ReportSubStep("verifyDays", label + " Element Disabled", "Pass")
            }

         }


      }
      catch (err) {
         action.ReportSubStep("verifyDays", "Element Not found", "Fail");
         throw new Error('Error');
      }
   }


   /**
   * desc: To Erase MultiselectDropdown Selection
   * @method EraseMultiselectDropdownSelection
   * @author: Adithya
   * @param :{Dropdown:locator of the Dropdown element,lableName:Related Label Name}
   * @return none
   */
   async eraseMultiselectDropdownSelection(Dropdown, lableName: string, role = "") {
      try {
         await browser.waitForAngular();
         let CloseDropdown = by.css('a[class*="ui-multiselect-close"]')
         let XpCheckBox = by.css('xg-multi-select input[type="checkbox"]')
         let XpCheckBoxClickComponent = by.css('div[class*="ui-multiselect-header"] div[class*="ui-chkbox-box"]')

         await action.Click(Dropdown, "Dropdown" + lableName)
         await action.MouseMoveToElement(XpCheckBox, lableName);
         await element(XpCheckBox).isSelected().then(async function (value) {
            if (value == false) {
               await action.Click(XpCheckBoxClickComponent, "Select/Deselect All")
               await action.Click(XpCheckBoxClickComponent, "Select/Deselect All")
               action.ReportSubStep("functionName", lableName + " Drop down selection Made empty", "Pass");
            }
            else {
               await action.Click(XpCheckBoxClickComponent, "Select/Deselect All")
               action.ReportSubStep("functionName", lableName + " Drop down selection Made empty", "Pass");
            }
         })
         if (role == "") {
            await action.Click(CloseDropdown, "CloseDropdown")
         }
         action.ReportSubStep("EraseMultiselectDropdownSelection", "Drop down selection Made empty", "Pass");

      }
      catch (err) {
         action.ReportSubStep("EraseMultiselectDropdownSelection", "Drop down selection Not Made empty", "Fail");
         throw new Error(err);
      }

   }


   /**
   * desc: To select All In Multiselect Dropdown
   * @method selectAllInMultiselectDropdown
   * @author: Adithya
   * @param :{Dropdown:locator of the Dropdown element,lableName:Related Label Name}
   * @return none
   */
   async selectAllInMultiselectDropdown(Dropdown, lableName: string) {
      try {
         await browser.waitForAngular();
         let CloseDropdown = by.css('a[class*="ui-multiselect-close"]')
         let XpCheckBox = by.css('xg-multi-select input[type="checkbox"]')
         let XpCheckBoxClickComponent = by.css('div[class*="ui-multiselect-header"] div[class*="ui-chkbox-box"]')

         await action.Click(Dropdown, "Dropdown" + lableName)
         await action.MouseMoveToElement(XpCheckBox, lableName);
         await element(XpCheckBox).isSelected().then(async function (value) {
            if (value == false) {
               await action.Click(XpCheckBoxClickComponent, "Select/Deselect All")
               action.ReportSubStep("functionName", lableName + " All Dropdown Options selected", "Pass");
            }
            else {
               action.ReportSubStep("functionName", lableName + " All Dropdown Options selected", "Pass");
            }
         })
         await action.Click(CloseDropdown, "CloseDropdown")
         action.ReportSubStep("selectAllInMultiselectDropdown", lableName + " All Dropdown Options selected", "Pass");
      }
      catch (err) {
         action.ReportSubStep("selectAllInMultiselectDropdown", lableName + " All Dropdown Options not selected", "Fail");
         throw new Error(err);
      }

   }

   /**
   * desc: To generate Random numbers for id's
   * @method makeNumericId 
   * @author: Adithya 
   * @param :{}   
   * @return none
   */
   async makeNumericId(length) {
      let text = "";
      let possible = "01234567893456";
      for (var i = 0; i < length; i++)
         text += possible.charAt(Math.floor(Math.random() * possible.length));
      return Number(text);
   }

   /**
   * desc: To generate Random makeAlphaNumericId for id's
   * @method makeAlphaNumericId
   * @author: Adithya
   * @param :{length:Testdata length to be craeted}
   * @return none   
   */
   async makeAlphaNumericId(length) {
      let text = "AT_";
      let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < length; i++)
         text += possible.charAt(Math.floor(Math.random() * possible.length));
      return text;
   }

   /**
   * desc: To Filter grid values in the view 
   * @method GridFilter
   * @author: Adithya
   * @param :{ColumnName:Column Name which is to be Validated,FilterData:Testdata to be Filtered}
   * @return none
   */
   async gridFilter(ColumnName: string, FilterData: string) {
      try {
         await browser.waitForAngular();
         let gridFilterIndex
         let gridColumnHeaderLocator = by.css('xg-grid th[style*="display: table-cell"]')
         let gridColumnFilterLocator = by.css('xg-grid table input[class*="ui-inputtext"]')
         let elementCount = await element.all(gridColumnHeaderLocator).count()
         let headerElements = await element.all(gridColumnHeaderLocator).getWebElements()
         let headerName;

         for (let index = 0; index < elementCount; index++) {
            headerName = await headerElements[index].getText();
            if (headerName.trim() == ColumnName.trim()) {
               gridFilterIndex = index;
               break;
            }
            else if (headerName.trim() != ColumnName.trim() && index == (elementCount - 1)) {
               action.ReportSubStep("GridFilter", "Column Name not available in Grid", "Fail");
            }
         }
         let ColumnFilter = element.all(gridColumnFilterLocator).get(gridFilterIndex)
         await action.ClearText(ColumnFilter, headerName + " Column text Filter")
         for (let i = 0; i < FilterData.length; i++) {
            await browser.sleep(100);
            await action.SetText(ColumnFilter, FilterData.charAt(i), headerName + " Column text Filter")
            await browser.sleep(100);
         }
         action.ReportSubStep("GridFilter", "Text Entered Successfully", "Pass");
      }
      catch (err) {
         action.ReportSubStep("GridFilter", "Text Not Entered", "Fail");
         throw new Error('Error');
      }
   }


   /** 
   * desc: To Validate grid values in the view 
   * @method gridDataValidation
   * @author: Adithya 
   * @param :{ColumnName:Column Name which is to be Validated,data:Testdata to be Entered,equalContains:equal or Contains}
   * @return none
   */
   async gridDataValidation(ColumnName: string, data: string, equalContains: string) {
      try {
         await browser.waitForAngular();
         let gridColumnIndex;
         let gridColumnHeaderLocator = by.css('xg-grid th[style*="display: table-cell"]')
         let paginator_next = by.css('a[class*="ui-paginator-next"]')
         let elementCount = await element.all(gridColumnHeaderLocator).count()
         let headerElements = await element.all(gridColumnHeaderLocator).getWebElements()
         let headerName;

         for (let index = 0; index < elementCount; index++) {
            headerName = await headerElements[index].getText();
            if (headerName.trim() == ColumnName.trim()) {
               gridColumnIndex = index + 2;
               break;
            }
            else if (headerName.trim() != ColumnName.trim() && index == (elementCount - 1)) {
               action.ReportSubStep("gridDataValidation", "Column Name not available in Grid", "Fail");
            }
         }
         await console.log("index of column is " + gridColumnIndex, "Column no is " + gridColumnIndex, "Name of column is " + headerName)
         await verify.verifyElement(by.css('xg-grid tr td:nth-child(' + gridColumnIndex + ')'), headerName)
         await action.MouseMoveToElement(by.css('xg-grid tr td:nth-child(' + gridColumnIndex + ')'), headerName)
         let gridDataCount = await element.all(by.css('xg-grid tr td:nth-child(' + gridColumnIndex + ')')).count()
         let gridDataElements = await element.all(by.css('xg-grid tr td:nth-child(' + gridColumnIndex + ')')).getWebElements()
         for (let colIndex = 0; colIndex < gridDataCount; colIndex++) {
            let text = await gridDataElements[colIndex].getText()
            if (equalContains.toUpperCase() == "EQUAL") {
               if (data == text) {
                  action.ReportSubStep("gridDataValidation", "Data available " + text, "Pass");
               }
               else {
                  action.ReportSubStep("gridDataValidation", "Data Not available " + text, "Fail");
               }
            }
            else {
               if (data.includes(text) || text.includes(data)) {
                  action.ReportSubStep("gridDataValidation", "Data available " + text, "Pass");
               }
               else {
                  action.ReportSubStep("gridDataValidation", "Data Not available " + text, "Fail");
               }
            }
         }

         action.ReportSubStep("gridDataValidation", "Data available", "Pass");
      }
      catch (err) {
         action.ReportSubStep("gridDataValidation", "Data Not available", "Fail");
         throw new Error('Error');
      }
   }

   /**
   * desc: To verify Nodata match Found In Grid
   * @method verifyNodataFoundInGrid
   * @author: Adithya
   * @param :{}
   * @return none
   */
   async verifyNodataFoundInGrid() {
      try {
         await browser.waitForAngular();
         // let NodataFOund = by.xpath('//td[contains(text(),"No data match is found")]')
         let NodataFOund = by.xpath('//div[contains(@class,"xg-grid")]//td[contains(text(),"No data match is found")]')
         await verify.verifyTextOfElement(NodataFOund, "No data match is found", "NoData")
         action.ReportSubStep("verifyNodataFoundInGrid", "No data match is found", "Pass");
      }
      catch (err) {
         action.ReportSubStep("verifyNodataFoundInGrid", "Grid data match is found", "Fail");
         throw new Error('Error');
      }
   }

   /**
   * desc: To verify & validate Toast Notification 
   * @method verifyToastNotification
   * @author: Adithya
   * @param :{NotificationMsg:Notification to be displayed}
   * @return none
   */
   async verifyToastNotification(NotificationMsg: string) {
      try {
         let NotificationLocator = by.css('div[class*="ui-toast-message-content"]')
         await browser.waitForAngular();
         await verify.verifyElement(NotificationLocator, "NotificationLocator")
         let Ntext: string = String(await action.GetText(NotificationLocator, "Toast Notification"));
         Ntext = Ntext.split('\n').join();
         console.log("Notification*** " + Ntext);
         if (Ntext.includes(NotificationMsg)) {
            action.ReportSubStep("verifyToastNotification", "Error Message matched Successfully", "Pass")
         }
         else {
            action.ReportSubStep("verifyToastNotification", "Error Message Not matched", "Fail")
         }
         action.ReportSubStep("verifyToastNotification", "Error Message dispalyed Successfully", "Pass");
      }
      catch (err) {
         action.ReportSubStep("verifyToastNotification", "Error Message Not dispalyed", "Fail");
         throw new Error(err);
      }
   }

   /**
   * desc: To verify & validate Days checkboxes displayed in Row of Grid 
   * @method verifyRowDaysCheckboxesArray
   * @author: Adithya
   * @param :{Row: Row No to be validated, ColumnName: Column to be validated, type: type of Validation or Action, label: Label of Checkbox}
   * @return none
   */
   async verifyCheckboxesActions(linkname: Array<string>, type: string, label: string) {
      try {
         //await browser.waitForAngular();
         for (let i = 0; i < linkname.length; i++) {
            if (type == "Click") {
               let component = by.css('xg-checkbox[labelname="' + linkname[i] + '"] label[class*="xg-check-label"]');
               await verify.verifyElement(component, "CheckBox")
               await action.Click(component, label)
               action.ReportSubStep("verifyDays", label + " Element Clicked", "Pass")
            }
            else {
               let component = by.css('xg-checkbox[labelname="' + linkname[i] + '"] input[type="checkbox"]');
               await verify.verifyElement(component, "CheckBox")
               if (type == "Displayed") {
                  await verify.verifyElement(component, label)
                  action.ReportSubStep("verifyDays", label + " Element Displayed", "Pass")
               }
               else if (type == "Selected") {
                  await verify.verifyElementIsSelected(component, label)
                  action.ReportSubStep("verifyDays", label + " Element Selected", "Pass")
               }
               else if (type == "Deselected") {
                  await verify.verifyElementIsNotSelected(component, label)
                  action.ReportSubStep("verifyDays", label + " Element Deselected", "Pass")
               }

               else if (type == "Enabled") {
                  await verify.verifyElementIsEnabled(component, label)
                  action.ReportSubStep("verifyDays", label + " Element Enabled", "Pass")
               }
               else if (type == "Disabled") {
                  await verify.verifyElementIsDisabled(component, label)
                  action.ReportSubStep("verifyDays", label + " Element Disabled", "Pass")
               }
            }
         }
      }
      catch (err) {
         action.ReportSubStep("verifyDays", "Element Not found", "Fail");
         throw new Error('Error');
      }
   }

   /**
   * desc: To return spot lengths Array in Length Factor view/Rate entry view without other data
   * @method returnspotlengthsArray
   * @author: Adithya
   * @param :{spotlengthslocator,OptionsArray:Array in which spot lenths to be stored,datatoberemoved:Array in which other data to be removed}
   * @return none
   */
   async returnspotlengthsArray(spotlengthslocator, OptionsArray: Array<string>, datatoberemoved: Array<String>) {
      try {
         await browser.waitForAngular();
         await element.all(spotlengthslocator).each(function (item) {
            item.getText().then(function (options) {
               OptionsArray.push(options)
            })
         })
         for (let i = 0; i < OptionsArray.length; i++) {
            console.log("Values are " + OptionsArray[i])
         }
         for (let i = 0; i < datatoberemoved.length; i++) {
            let indexofdata = OptionsArray.indexOf(String(datatoberemoved[i]))
            OptionsArray.splice(indexofdata, 1)
         }
         for (let i = 0; i < OptionsArray.length; i++) {
            console.log("Values after removing other details are " + OptionsArray[i])
         }
         action.ReportSubStep("returnspotlengthsArray", "Spot Lengths Array Returned successfully", "Pass");
      }
      catch (err) {
         action.ReportSubStep("returnspotlengthsArray", "Spot Lengths Array not Returned successfully", "Fail");
         throw new Error('Error');
      }
   }

   /**
    * desc: To verify Pop-Up Header
    * @method verifyPopUpHeader
    * @author: Adithya K
    * @param :{headerText= headerText to be verified}
    * @return none
    */
   async verifyPopUpHeader(headerText) {
      try {
          let popupHeader = by.css('div[class*="ui-dialog-titlebar"]')
          let matDialogTitle = by.css('[class*="mat-dialog-title"]')
          await browser.waitForAngular();
          let status = await element(popupHeader).isPresent();
          if (status) {
              await verify.verifyElement(popupHeader, headerText)
              await verify.verifyTextOfElement(popupHeader, headerText, headerText)
          }
          else {
              await verify.verifyElement(matDialogTitle, headerText)
              await verify.verifyTextOfElement(matDialogTitle, headerText, headerText)
          }
      }
      catch (error) {
          action.ReportSubStep("verifyPopUpHeader", "Popup Header Not present", "Fail");
          throw new Error(error);
      }
  }


   /**
    * desc: To verify verify sort order
    * @method verifysortorder
    * @author: Adithya K
    * @param :{Locator}
    * @return none
    */
   async verifysortorder(eleLocator) {
      const results = [];
      element.all(eleLocator).getText().then(function (value) {
         results.push(value)
      }).then(function () {
         console.log("Results", results);
         let lengthsorder = results.slice();
         lengthsorder = lengthsorder.sort(function (a, b) { return a - b });
         console.log("Spot lenths order", lengthsorder);
         expect(lengthsorder).toEqual(results);
         if (JSON.stringify(lengthsorder) == JSON.stringify(results)) {
            console.log('Spot lenths are in Ascendingorder')
            action.ReportSubStep("verifysortorderforlenthfactor", "Spot lenths are in Ascendingorder", "Pass")
         }
         else {
            console.log('Spot Length are not in Ascendingorder')
            action.ReportSubStep("verifysortorderforlenthfactor", "Spot Length are not in Ascendingorder", "Fail");
         }
      })
   }

   async verifySortOrderSpotLengthslenthfactorView() {
      try {
         let results = [];
         await element.all(by.xpath('//li[contains(text(),"s")]')).each(function (item) {
            item.getText().then(function (text) {
               text = text.replace("s", "")
               results.push(text)
            })
         })
         let sortedarray = results.sort((n1, n2) => n1 - n2);
         if (sortedarray == results) {
            action.ReportSubStep("verifysortorderforlenthfactor", "Spot lenths are in Ascendingorder", "Pass")
            console.log("Pass")
         }
         else {
            action.ReportSubStep("verifysortorderforlenthfactor", "Spot Length are not in Ascendingorder", "Fail");
            console.log("Fail")
         }
      }
      catch (error) {
         throw new Error(error);
      }
   }

   /**
   * desc: To verify & validate Toast Notification 
   * @method verifyToastNotification
   * @author: Adithya
   * @param :{NotificationMsg:Notification to be displayed}
   * @return none
   */
   async enterDIffLengthsIfSpotLengthsAlreadyExists() {
      try {
         let NotificationLocator = by.css('div[class*="ui-toast-message-content"]')
         let successMsg = "Add Spot Length,Spot Length Added Successfully."
         let localMsg = "This Spot Length exists for channel WPGH. Please remove this channel from the selection and try again."
         let NetworkMsg = "This Spot Length exists for network FOX.Please remove this network from the selection and try again."
         await browser.waitForAngular();
         await verify.verifyElement(NotificationLocator, "NotificationLocator")
         let Ntext: string = String(await action.GetText(NotificationLocator, "Toast Notification"));
         Ntext = Ntext.split('\n').join();
         console.log("Notification*** " + Ntext);
         if (Ntext.includes(localMsg) || Ntext.includes(NetworkMsg)) {
            let testdata_SpotLength = await this.makeNumericId(4)
            let testdata_SpotLengthFactor = "0." + testdata_SpotLength
            await action.ClearText(element.all(by.css(this.textBox_locator.replace("dynamic", "xgc-spot-length"))).get(0), "SpotLength")
            await action.ClearText(by.css(this.textBox_locator.replace("dynamic", "xgc-spot-length-factor")), "SpotLengthFactor")
            await action.SetText(element.all(by.css(this.textBox_locator.replace("dynamic", "xgc-spot-length"))).get(0), String(testdata_SpotLength), "SpotLength")
            await action.SetText(by.css(this.textBox_locator.replace("dynamic", "xgc-spot-length-factor")), (testdata_SpotLengthFactor), "SpotLengthFactor")
            await this.buttonActionsVerify(["Save"], "Click", "Save Button")
            let Ntext: string = String(await action.GetText(NotificationLocator, "Toast Notification"));
            Ntext = Ntext.split('\n').join();
            if (Ntext.includes(successMsg)) {
               action.ReportSubStep("verifyToastNotification", "Error Message matched Successfully", "Pass")
            }
            else {
               action.ReportSubStep("verifyToastNotification", "Error Message Not matched", "Fail")
            }
            action.ReportSubStep("enterDIffLengthsIfSpotLengthsAlreadyExists", "Unique Lengths Added Successfully", "Pass")
         }
         else if (Ntext.includes(successMsg)) {
            action.ReportSubStep("enterDIffLengthsIfSpotLengthsAlreadyExists", "Unique Lengths Added Successfully", "Pass")
         }
         else {
            action.ReportSubStep("enterDIffLengthsIfSpotLengthsAlreadyExists", "Notification Msg Not matched", "Fail")
         }
      }
      catch (err) {
         action.ReportSubStep("enterDIffLengthsIfSpotLengthsAlreadyExists", "Error Message Not dispalyed", "Fail");
         throw new Error(err);
      }
   }

}  