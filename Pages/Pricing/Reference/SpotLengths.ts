import { browser, element, by, protractor, promise } from 'protractor';
import { ActionLib } from '../../../Libs/GeneralLibs/ActionLib';
import { VerifyLib } from '../../../Libs/GeneralLibs/VerificationLib';
import { LocalLengthFactorsPricing } from './LocalLengthFactors';


let action = new ActionLib();
let verify = new VerifyLib();
let localLF = new LocalLengthFactorsPricing();

export class SpotLengths{
    deleteConfirmationText = 'Are you sure you want to delete the selected record(s)?'
    deletedNotificationMsg = 'Successfully deleted Spot Length(s).'
    spotLengthColumnName = 'Spot Length'
    deleteConfirmationMsg = by.xpath('//div[contains(text(),"  Are you sure you want to delete the selected record(s)? ")]')
    latestRecordCheckbox = by.css('xg-grid div[class*="ui-table-unfrozen-view"] tr:nth-child(1) td:nth-child(1)')
    secondRecordCheckbox = by.css('xg-grid div[class*="ui-table-unfrozen-view"] tr:nth-child(2) td:nth-child(1)')
    noDataFound = by.cssContainingText('xg-grid div[class*="ui-table-unfrozen-view"] tr td',"No data match is found")
    customSpotLength= by.xpath('//div[contains(@class,"ui-table-unfrozen-view")]//span[contains(text(),"Custom")]')
    latestSpotLength = by.css('xg-grid div[class*="ui-table-unfrozen-view"] tr:nth-child(1) td:nth-child(2)')
    secondSpotLength = by.css('xg-grid div[class*="ui-table-unfrozen-view"] tr:nth-child(2) td:nth-child(2)')
    
    /**
     * desc: To add Spot Length 
     * @method addSpotLength
     * @author: Adithya
     * @param :{}
     * @return none
     */
    async addSpotLength() {
        try {
            await browser.waitForAngular();
            await localLF.buttonActionsVerify(["Add"], "Click", "Save Button")
            await browser.sleep(1000)
            let testdata_SpotLength = await localLF.makeNumericId(4)
            let testdata_SpotLengthFactor = "0." + testdata_SpotLength
            await action.SetText(element.all(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length"))).get(0), String(testdata_SpotLength), "SpotLength")
            await action.SetText(by.css(localLF.textBox_locator.replace("dynamic", "xgc-spot-length-factor")), (testdata_SpotLengthFactor), "SpotLengthFactor")
            await localLF.buttonActionsVerify(["Save"], "Click", "Save Button")
            await localLF.verifyToastNotification("Add Spot Length,Spot Length Added Successfully.")
            await verify.verifyProgressSpinnerCircleNotPresent()
            await verify.verifyProgressBarNotPresent()
            action.ReportSubStep("addSpotLength", "SpotLength Added is "+testdata_SpotLength, "Pass");
            return testdata_SpotLength;
        }
        catch (err) {
            action.ReportSubStep("addSpotLength", "SpotLength Not Added", "Fail");
            throw new Error(err);
        }

    }

    /**
   * desc: To verify Nodata match Found In Grid
   * @method verifyNodataFoundInSpotLengthGrid
   * @author: Adithya
   * @param :{}
   * @return none
   */
   async verifyNodataFoundInSpotLengthGrid() {
    try {
       await browser.waitForAngular();
       let noDataFound = by.cssContainingText('xg-grid div[class*="ui-table-unfrozen-view"] tr td',"No data match is found")
       await verify.verifyTextOfElement(noDataFound, "No data match is found", "NoData")
       action.ReportSubStep("verifyNodataFoundInSpotLengthGrid", "No data match is found", "Pass");
    }
    catch (err) {
       action.ReportSubStep("verifyNodataFoundInSpotLengthGrid", "Grid data match is found", "Fail");
       throw new Error('Error');
    }
 }


}  