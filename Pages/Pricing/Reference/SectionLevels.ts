import { browser, element, by, protractor, promise } from 'protractor';
import { ActionLib } from '../../../Libs/GeneralLibs/ActionLib';
import { VerifyLib } from '../../../Libs/GeneralLibs/VerificationLib';
import { Gutils } from '../../../utility/gutils';


let gUtils = new Gutils();
let action = new ActionLib();
let verify = new VerifyLib();

export class SectionLevels {
  
  sectionButtons: string = '//xg-button[@ng-reflect-label="dynamic"]//button';
  popUpDialog: string = '//mat-dialog-container//h1[contains(text(),"dynamic")]';
  MarketChannelDropdown = "//label[text()='dynamic']/parent::div//div[contains(@class,'trigger')]";
  description = by.xpath("//label[contains(text(),'Description')]/../following-sibling::textarea");
  SectionDescription = by.css("textarea[name='textInput']");
  sectionNameTraffic = "//label[contains(text(),'dynamic')]/following-sibling::input";
  clickSave = by.css("[title='Save']");
  serachsectionname = by.css("tr:nth-child(2) th:nth-child(2) input[class*='inputtext']");
  clickCancel = by.css("[title='Cancel']");
  toggleSwitch = by.css("[class*='mat-slide-toggle-input']");
  tableFirstRow = by.css("tbody tr:nth-child(1)");

  /**
* desc: verify Dropdown selected values
* @method verifyDropdownSelectedValue
* @author: Srikanth
* @param :{label}
* @param :{selectedValue}
* @return none
*/
  async verifyDropdownSelectedValue(label, selectedValue) {
    await browser.waitForAngular().then(function () {
      try {
        var loc = by.xpath("//label[contains(text(),'" + label + "')]/parent::div//label[contains(@class,'ui-')]");
        verify.verifyTextInElement(loc, selectedValue, label + " selected value");
      } catch (err) {
        throw new Error('Error');
      }
    });
  }
  /**
* desc: Document Section or Traffic Translator
* @method enterTextIntoNameTraffic
* @author: Srikanth
* @param :{label}
* @param :{textToEnter}
* @return none
*/
  async enterTextIntoNameTraffic(label, textToEnter) {
    await browser.waitForAngular().then(function () {
      try {
        var loc = by.xpath("//label[contains(text(),'" + label + "')]/following-sibling::input");
        action.SetText(loc, textToEnter, label);
      } catch (err) {
        throw new Error('Error');
      }
    });
  }
  /**
* desc: verify Section or Traffic Translator text
* @method verifyTextInSectionTraffic
* @author: Srikanth
* @param :{label}
* @param :{textToVerify}
* @return none
*/
  async verifyTextIninputAndTextArea(locator, textToVerify) {
    await browser.waitForAngular().then(function () {
      try {
        action.GetTextFromInput(locator, " text field ").then(function (_value: string) {
          if (_value.includes(textToVerify)) {
            action.ReportSubStep("Verify element in text field ", "Data available " + textToVerify, "Pass");
          }
          else {
            action.ReportSubStep("Verify element in text field ", "Data not available " + textToVerify, "Fail");
          }
        });
      }
      catch (err) {
        throw new Error('Error');
      }
    });
  }
  /**
 * desc: verify Section or Traffic Translator text
 * @method verifyTextInSectionTraffic
 * @author: Srikanth
 * @param :{label}
 * @param :{textToVerify}
 * @return none
 */
  async verifyGridDataPresent(column, textToVerify) {
    await browser.waitForAngular().then(async function () {
      try {
        var indexOfCol;
        let tableHeaders = by.xpath("//table//th[@preorderablecolumn]");
        await element.all(tableHeaders).each(function (ele, index) {
          ele.getText().then(function (_text) {
            if (_text.includes(column)) {
              indexOfCol = index + 1;
              console.log("indexOfCol" + indexOfCol);
            }
          });
        });
        let tableRows = element(by.xpath('//tbody/tr/td[' + indexOfCol + ']'));
        await tableRows.getText().then(async function (_rowData) {
          await console.log("text blank" + _rowData);
          if (_rowData.includes(textToVerify)) {
            action.ReportSubStep("Verify Table Row Data ", "Data available " + textToVerify, "Pass");
          }
          else {
            action.ReportSubStep("Verify Table Row Data ", "Data is not as expected ", "FAIL");
          }
        });
      }
      catch (err) {
        throw new Error('Error');
      }
    });
  }
}

