import { by, browser, WebElement } from "protractor";
import { AppCommonFunctions } from "../../../Libs/ApplicationLibs/CommAppLib";
import { ActionLib } from "../../../Libs/GeneralLibs/ActionLib";
import { VerifyLib } from "../../../Libs/GeneralLibs/VerificationLib";


let commonLib = new AppCommonFunctions();
let action = new ActionLib();
let verify = new VerifyLib();
export class RateCardPage {

    addCardMessage = "Successfully added Rate Card";
    cloneRateCardMessage = "Successfully cloned rate card";
    deleteRateCardMessage = "Successfully deleted Rate Card";
    errorRateCardMessage = "Rate card already exits";
    //Locators
    name = by.css("xg-input[attr-name='xgc-ratecard-ratecardName'] input");
    // cloneRateCardName = by.css("xg-input[attr-name='xgc-name-ratecard'] input");
    cloneRateCardName = by.xpath("//*[normalize-space()='Name']/following-sibling::input");
    description = by.css("xg-input[attr-name='xgc-ratecard-ratecardDescription'] input");
    publicSlider = by.css("xg-input-switch[attr-name='xgc-ratecard-isPrivate'][ng-reflect-label='Public'] div[class='mat-slide-toggle-thumb-container']");
    privateSlider = by.css("xg-input-switch[attr-name='xgc-ratecard-isPrivate']:not([ng-reflect-label='Public']) div[class='mat-slide-toggle-thumb-container']");
    saveButton = by.css("xg-button[attr-name='xgc-ratecard-save'] button:not(disabled)");
    cancelButton = by.css("xg-button[attr-name='xgc-ratecard-cancel'] button:not(disabled)");
    gridCheckBox = "//*[normalize-space()='dynamic']/preceding-sibling::td[contains(@class,'xg-grid-column-select-checkbox')]//div[contains(@class,'ui-chkbox-box') and not(contains(@class,'active'))]";
    gridActiveCheckBox = "//*[normalize-space()='dynamic']/preceding-sibling::td[contains(@class,'xg-grid-column-select-checkbox')]//div[contains(@class,'ui-chkbox-box') and contains(@class,'active')]"
    cloneOrCopyButton = by.css("xg-button[attr-name='xgc-copy-clone-ratecard']>button");
    cloneOrCopyOptions = by.css("xg-button[attr-name='xgc-copy-clone-ratecard']>ul[class*='dropdown-options']>li");
    cloneRateCardDialogHeader = by.xpath("//div[@attr-name='xgc-ratecard-view']/following-sibling::p-dialog[@header='Clone Rate Card']/div[@role='dialog']");
    cloneDialogConfirmActiveButton = by.css("xg-button[attr-name='xgc-clone-confirm-ratecard'][ng-reflect-disabled='false'] button");
    cloneDialogConfirmInActiveButton = by.css("xg-button[attr-name='xgc-clone-confirm-ratecard'][ng-reflect-disabled='true'] button");
    cloneDialogCancleButton = by.css("xg-button[attr-name='xgc-cancel-ratecard'] button");
    deleteRateCardButton = by.css("xg-button[attr-name='xgc-delete-ratecard'] button");
    deleteRateCardPopupYesButton = by.css("xg-popup[attr-name='xgc-delete-ratecrad-confirmation-popup'] div[class*='ui-dialog'][role='dialog'] xg-button[attr-name='xgc-delete-spot-confirmation'] button");
    deleteRateCardPopupNoButton = by.css("xg-popup[attr-name='xgc-delete-ratecrad-confirmation-popup'] div[class*='ui-dialog'][role='dialog'] xg-button[attr-name='xgc-cancel-delete'] button");
    gridActiveSlider = by.css("input[aria-checked='true']+div[class='mat-slide-toggle-thumb-container']");
    gridInActiveSlider = by.css("input[aria-checked='false']+div[class='mat-slide-toggle-thumb-container']");
    addRateCardHeader = by.xpath("//div[contains(@class,'popup')]/h1[normalize-space()='Add Rate Card']");
    //Methods

    /**
        * desc: It is used to add rate card
        * @method addRateCard
        * @author: Venkata Sivakumar
        * @param :{cardDetails,errorMessageNotification:boolean = false}
        * @return none
        */

    async addRateCard(cardDetails, errorMessageNotification: boolean = false) {
        try {
            await verify.verifyElementIsDisplayed(this.addRateCardHeader,"Verify Add Rate Card header is displayed");
            if (cardDetails["Channels"] != undefined) {
                await commonLib.selectMultiOptionsFromDropDown("channel", cardDetails["Channels"],"dialog");
            }
            await action.ClearText(this.name, "Clear the text in Name");
            if (cardDetails["Name"] != undefined) {
                await action.SetText(this.name, cardDetails["Name"], "Enter text in Name field");
            }
            await action.ClearText(this.description, "Clear the text in Name");
            if (cardDetails["Description"] != undefined) {
                await action.SetText(this.description, cardDetails["Description"], "Enter text in Description field");
            }
            if (cardDetails["Length Factor"] != undefined) {
                await commonLib.selectDropDownValue("Length Factor", cardDetails["Length Factor"]);
            }
            if (cardDetails["Section Level"] != undefined) {
                await commonLib.selectDropDownValue("Section Level", cardDetails["Section Level"]);
            }
            if (cardDetails["Public"] != undefined) {
                if (String(cardDetails["Public"]).toLowerCase() == "no") {
                    let elementStatus = await verify.verifyElementVisible(this.publicSlider);
                    let elementStatus1 = await verify.verifyElementVisible(this.privateSlider);
                    if (elementStatus) {
                        await action.Click(this.publicSlider, "Move Slider to Private");
                        await browser.sleep(1000);
                        await verify.verifyElementIsDisplayed(this.privateSlider,"");
                    }
                    else if (elementStatus == false && elementStatus1 == false) {
                        await action.ReportSubStep("addRateCard", "Either " + this.publicSlider + " and " + this.privateSlider + " Locators are not visible", "Fail");
                    }
                    if (cardDetails["Users"] != undefined) {
                    await commonLib.selectMultiOptionsFromDropDown("User", cardDetails["Users"],"dialog");
                    }
                }
                else if (String(cardDetails["Public"]).toLowerCase() == "yes") {
                    let elementStatus = await verify.verifyElementVisible(this.privateSlider);
                    let elementStatus1 = await verify.verifyElementVisible(this.publicSlider);
                    if (elementStatus) {
                        await action.Click(this.privateSlider, "Move Slider to Public");
                        await browser.sleep(1000);
                        await verify.verifyElementIsDisplayed(this.publicSlider,"");
                    }
                    else if (elementStatus == false && elementStatus1 == false) {
                        await action.ReportSubStep("addRateCard", "Either " + this.publicSlider + " and " + this.privateSlider + " Locators are not visible", "Fail");
                    }
                }
            }
            await action.Click(this.saveButton, "Click On Save button");
            await browser.sleep(1000);
            if (errorMessageNotification == false) {
                await commonLib.verifyPopupMessage(this.addCardMessage);
            }
            else {
                await commonLib.verifyPopupMessage(this.errorRateCardMessage);
            }
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
            * desc: It is used to click on copy or clone button
            * @method clickOnCopyOrCloneButton
            * @author: Venkata Sivakumar
            * @param :{buttonName:string}
            * @return none
            */

    async clickOnCopyOrCloneButton(buttonName: string) {
        try {
            await action.Click(this.cloneOrCopyButton, "Click On Copy/Clone button");
            let elements: Array<WebElement> = await action.getWebElements(this.cloneOrCopyOptions);
            for (let index = 0; index < elements.length; index++) {
                if ((await elements[index].getText()).trim().toLowerCase() == buttonName.trim().toLowerCase()) {
                    await elements[index].click();
                    break;
                }
                else if ((await elements[index].getText()).trim().toLowerCase() != buttonName.trim().toLowerCase() && index == elements.length - 1) {
                    await action.ReportSubStep("clickOnCopyOrCloneButton", buttonName + " is not available in options", "Fail");
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }
}
