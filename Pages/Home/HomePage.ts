import { browser, element, by, protractor, promise } from 'protractor';
import { ActionLib } from '../../Libs/GeneralLibs/ActionLib';
import { VerifyLib } from '../../Libs/GeneralLibs/VerificationLib';
import { Gutils } from '../../Utility/gutils';

let gUtils = new Gutils();
let action = new ActionLib();
let verify = new VerifyLib();

export class HomePageFunction {

   /**
  * desc: click XGList Content Item
  * @method appclickOnNavigationList
  * @author: vikas
  * @param :{}
  * @return none
  */
   async appclickOnNavigationList(headerText: string) {

      try {

         let locator = by.xpath('//nav[contains(@class,"nav-list")]//span[contains(text(),"' + headerText + '")]');
         await action.MouseMoveToElement(locator, headerText);
         await action.Click(locator, headerText);

      }
      catch (err) {
         await action.ReportSubStep("appclickOnNavigationList", "click on main link" + headerText, "Fail");
      }
   }
   /**
 * desc: appclickOnMenuListLeftPanel
 * @method appclickOnMenuListLeftPanel
 * @author: vikas
 * @param :{}
 * @return none
 */
   async appclickOnMenuListLeftPanel(headerText: string) {

      try {
         let locator = by.xpath('//img-lib-menu-item//span[contains(text(),"' + headerText + '")]');
         await action.MouseMoveToElement(locator, headerText);
         await action.Click(locator, headerText);

      }
      catch (err) {
         await action.ReportSubStep("appclickOnMenuListLeftPanel", "click on main link left panel" + headerText, "Fail");
      }
   }


   /**
* desc: verifyMenuListonLeftPanel
* @method verifyMenuListonLeftPanel
* @author: Srikanth
* @param :{}
* @return none
*/
   async verifyMenuListonLeftPanel(headerText: string) {

      try {
         let locator = by.xpath('//img-lib-menu-item//span[contains(text(),"' + headerText + '")]');
         await verify.verifyElementIsDisplayed(locator, " Navigation Items");

      }
      catch (err) {
         await action.ReportSubStep("verifyMenuListonLeftPanel", "Verify main link left panel" + headerText, "Fail");
      }
   }
}    