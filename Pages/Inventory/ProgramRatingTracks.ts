import { browser, element, by, protractor, promise } from 'protractor';
import { ActionLib } from '../../Libs/GeneralLibs/ActionLib';
import { VerifyLib } from '../../Libs/GeneralLibs/VerificationLib';
import { Gutils } from '../../Utility/gutils';

let gUtils = new Gutils();
let action = new ActionLib();
let verify = new VerifyLib();

export class ProgramRatingTracks {

   locProgressBar = by.css('p-progressbar[mode="indeterminate"]');
   gridDayPartHeaders = "div[class='ui-table-scrollable-view ui-table-unfrozen-view'] table[class*='header'] tr[class*='topHead']>th";

   firstRowName = by.css("div[class='xg-grid xg-frozen-grid'] table[class*='body-table']>tbody>tr:nth-child(1)>td:nth-child(1) span[class*='xg-inline-block']");
   firstRowMetrics = by.css("div[class='xg-grid xg-frozen-grid'] table[class*='body-table']>tbody>tr:nth-child(1)>td:nth-child(6) search-text-highlighter span[class='ng-star-inserted']");
   firstRowRatings = by.css("div[class*='ui-table-unfrozen-view'] table[class*='scrollable']>tbody>tr:nth-child(1)>td:nth-child(1)");

   gridTable = by.css("div[class='xg-grid xg-frozen-grid'] div[class*='ui-table ui-widget']");

   SurveyElement = by.css("[title='Survey']")
   button_generic = by.css('xg-button button[class*="xg-button"]')
   hideInactiveSurveysLink = by.xpath('//a[contains(text()," Hide Inactive Surveys ")]')
   showInactiveSurveysLink = by.xpath('//a[contains(text()," Show Inactive Surveys ")]');
   showcheck = by.xpath('//a[contains(text()," Show Inactive Surveys ")]')


   /**
* desc: slide Decimal value In Mat Slider
* @method slideDecimalvalueInMatSlider
* @author: vikas
* @param :{}
* @return 
*/
   async slideDecimalvalueInMatSlider(nameOfMatslider: string, slideValue: Number) {
      try {
         let defalutV;
         let locatorMat = by.xpath('//mat-slider[contains(@attr-name,"' + nameOfMatslider + '")]');
         let locatorCircle = by.xpath('//mat-slider[contains(@attr-name,"' + nameOfMatslider + '")]');
         // await action.MouseMoveToElement(locatorMat, nameOfMatslider);
         await browser.actions().mouseMove(element(locatorMat)).perform();
         await browser.sleep(2000);
         await action.Click(locatorCircle, nameOfMatslider + "Slider Value");
         await browser.sleep(2000);
         let defalutValue = Number(await element(locatorMat).getAttribute("aria-valuenow"));
         //console.log("Default Value 1"+defalutValue);
         if (slideValue > defalutValue) {

            await browser.sleep(1000);
            defalutV = Number(await element(locatorMat).getAttribute("aria-valuenow"));
            await browser.sleep(1000);
            while (defalutV < slideValue) {
               await element(locatorCircle).sendKeys(protractor.Key.ARROW_RIGHT);
               // console.log("right Arrow Clicked ")
               await browser.sleep(1000);
               defalutV = Number(await element(locatorMat).getAttribute("aria-valuenow"));
            }
         }
         else {

            await browser.sleep(1000);
            defalutV = Number(await element(locatorMat).getAttribute("aria-valuenow"));
            await browser.sleep(1000);
            while (defalutV > slideValue) {
               await element(locatorCircle).sendKeys(protractor.Key.ARROW_LEFT);
               //console.log("Left Arrow Clicked ")
               await browser.sleep(1000);
               defalutV = Number(await element(locatorMat).getAttribute("aria-valuenow"));
            }
         }

      }
      catch (err) {
         await action.ReportSubStep("slideDecimalvalueInMatSlider", "click on slider" + nameOfMatslider, "Fail");
      }

   }
   /**
     * desc: get Decimal Value PAV Ratings Programs Traking Table
     * @method verifyDecimalValuePAVRatingsProgramsTrakingTable
     * @author: vikas
     * @param :{}
     * @return none
     */
   async verifyDecimalValuePAVRatingsProgramsTrakingTable(valueRTGDecimal: any, headerText: string) {
      try {
         let locTableHeader = by.xpath('//xg-grid[not(contains(@class,"xg-sub-grid ng-star-inserted"))]//div[contains(@class,"table-frozen-view")]//th');
         let locTableRow = by.xpath('//xg-grid[not(contains(@class,"xg-sub-grid ng-star-inserted"))]//div[contains(@class,"table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")]');
         let counterHeader;
         let dataColumn1;
         let flag = false;
         await element.all(locTableHeader).each(async function (ele, Index) {
            await browser.actions().mouseMove(ele).perform().then(async function () {
               await ele.getText().then(async function (_textHeader) {
                  if (_textHeader.trim() == headerText) {
                     counterHeader = Index + 1;
                     dataColumn1 = 1;
                  }
               })
            })
         })
         await browser.sleep(1000);
         await verify.verifyNotPresent(this.locProgressBar, "Proress bar");
         await element.all(locTableRow).each(async function (ele, Index) {
            let rowIndex = Index + 1
            // console.log("Headr" +counterHeader);
            // console.log("Headr +1" +dataColumn1);

            let locColumn = by.xpath('//xg-grid[not(contains(@class,"xg-sub-grid ng-star-inserted"))]//div[contains(@class,"table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + counterHeader + ']');
            let colotoVerify = by.xpath('//xg-grid[not(contains(@class,"xg-sub-grid ng-star-inserted"))]//div[contains(@class,"ui-table-unfrozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + dataColumn1 + ']');
            await browser.actions().mouseMove(element(locColumn)).perform().then(async function () {
               await browser.sleep(1000);
               await element(locColumn).getText().then(async function (_text) {
                  //console.log(rowIndex + "Row Text " + _text)
                  await element(colotoVerify).getText().then(function (_text1) {
                     //console.log("col Text " + _text1)
                     let allValue = _text.split('\n');
                     let allValueC = _text1.split('\n');

                     for (let i = 0; i < allValue.length; i++) {
                        // console.log("row value" + allValue[i])
                        // console.log("colum value " + allValueC[i])
                        //  console.log(". value "+allValueC[i].toString().indexOf('.'))
                        let keyValueapp = allValue[i].split('(').join('').split(')').join('').trim();
                        // console.log("keyValueapp "+keyValueapp)
                        let count = valueRTGDecimal[keyValueapp];

                        if (Number(count) >= 0 && (count != undefined)) {
                           if (allValueC[i].toString().indexOf('.') > -1) {
                              let dCount = allValueC[i].toString().split(".")[1].length;
                              if (Number(dCount) == Number(count)) {
                                 flag = true;
                                 action.ReportSubStep("verifyDecimalValuePAVRatingsProgramsTrakingTable", "verify value after decimal" + count + " count from app" + dCount, "PASS");
                              }
                              else {
                                 action.ReportSubStep("verifyDecimalValuePAVRatingsProgramsTrakingTable", "verify value after decimal" + count + " count from app" + dCount, "FAIL");
                              }

                           }
                           else {
                              if ((count == 0) && (allValueC[i].toString().indexOf('.') == -1)) {
                                 flag = true;
                                 action.ReportSubStep("verifyDecimalValuePAVRatingsProgramsTrakingTable", "verify value after decimal" + count, "PASS");
                              }
                              else {
                                 action.ReportSubStep("verifyDecimalValuePAVRatingsProgramsTrakingTable", "verify value after decimal" + count, "FAIL");
                              }

                           }
                        }
                     }
                  });



               });
            });




         }).then(function () {
            if (flag == true) {
               action.ReportSubStep("verifyDecimalValuePAVRatingsProgramsTrakingTable", "Ratings are loaded", "PASS");
            }
            else {
               action.ReportSubStep("verifyDecimalValuePAVRatingsProgramsTrakingTable", "Ratings are not loaded", "Fail");
            }
         })

      }
      catch (err) {
         action.ReportSubStep("verifyDecimalValuePAVRatingsProgramsTrakingTable", "verify value after decimal", "FAIL");
      }

   }

   /**
  * desc: verify selling title value in Programs Traks
  * @method verifySellingTitleInProgramsList
  * @author: Venkata Sivakumar
  * @param :{sellingTitleColumnValue: string}
  * @return none
  */
   async verifySellingTitleInProgramsList(sellingTitleColumnValue: string) {
      try {
         let count = await action.getWebElementsCount(by.css("div.xg-programs-list li.list-item.ng-star-inserted"));
         for (let index = 1; index <= count; index++) {
            let actualValue = await action.GetText(by.css("div.xg-programs-list li.list-item.ng-star-inserted:nth-child(" + String(index) + ") div.xg-program-title"), "");
            if (String(actualValue).trim() != sellingTitleColumnValue.trim()) {
               action.ReportSubStep("verifySellingTitleInProgramsList", "Verify " + sellingTitleColumnValue + " in Programs Track", "Fail");
            }
            else if (String(actualValue).trim() == sellingTitleColumnValue.trim()) {
               break;
            }
         }
      }
      catch (error) {
         action.ReportSubStep("verifySellingTitleInProgramsList", "Verify " + sellingTitleColumnValue + " in Programs Track", "Fail");
      }
   }
   /**
        * desc: Used to get the Day Part Headers from Program track grid
        * @method getDayPartHeaders
        * @author: Venkata Sivakumar
        * @param :{}
        * @return headerValues:Array<any>
        */
   async getDayPartHeaders() {
      try {
         let headerValues: Array<any> = new Array<any>();
         let elementStatus: boolean;
         elementStatus = await verify.verifyElementVisible(by.css(this.gridDayPartHeaders));
         if (elementStatus) {
            let count = await action.getWebElementsCount(by.css(this.gridDayPartHeaders));
            for (let index = 1; index <= count; index++) {
               let text = await action.GetText(by.css(this.gridDayPartHeaders + ":nth-child(" + String(index) + ")"), "");
               await headerValues.push(String(text).trim());
            }
            return await headerValues
         }
         else {
            return headerValues
         }
      } catch (error) {
         action.ReportSubStep("getDayPartHeaders", "getDayPartHeaders", "Fail");
      }
   }
   /**
        * desc: Used to validate day part is removed from Program Track Grid
        * @method getDayPartHeaders
        * @author: Venkata Sivakumar
        * @param :{dayPartValue:string}
        * @return none
        */
   async validateDayPartRemovedFromProgramTrack(dayPartValue: string) {
      let count = 0;
      try {
         let headers = await this.getDayPartHeaders();
         for (let index = 0; index < headers.length; index++) {
            if (String(headers[index]).trim() == String(dayPartValue).trim()) {
               count = count + 1;
            }
         }
         if (count != 0) {
            action.ReportSubStep("validateDayPartRemovedFromProgramTrackView", "Verify " + dayPartValue + " value is removed form the grid table column", "Fail");
         }
      } catch (error) {
         action.ReportSubStep("validateDayPartRemovedFromProgramTrackView", "Verify " + dayPartValue + " value is removed form the grid table column", "Fail");
      }
   }


   /**
* desc: click Program In Ratings Tracks View
* @method clickProgramInRatingsTracksView
* @author: vikas
* @param :{}
* @return 
*/
   async clickProgramInRatingsTracksView(programTitle: string) {
      try {
         let locator = by.xpath('//xg-list[contains(@attr-name,"xgc-list-programs")]//div[contains(@class,"xg-programs-item")]//div[contains(text(),"' + programTitle + '")]');
         await action.MouseMoveToElement(locator, programTitle);
         await browser.sleep(2000);
         await action.Click(locator, programTitle);

      }
      catch (err) {
         await action.ReportSubStep("clickProgramInRatingsTracksView", "click program name" + programTitle, "Fail");
      }

   }
   /**
       * desc: click Data In Rating Source Table Tarcks Page
       * @method clickDataInRatingSourceTableTarcksPage
       * @author: vikas
       * @param :{}
       * @return _dataAll
       */
   async clickDataInRatingSourceTableTarcksPage(attributeName: string, textToVerify: string) {
      try {
         let locator = by.xpath('//xg-column-list[contains(@attr-name,"' + attributeName + '")]//ul[contains(@class,"list-container")]//li//div[normalize-space(text())="' + textToVerify + '"]|//xg-header[contains(text(),"' + attributeName + '")]/../..//ul[contains(@class,"list-container")]//li//div[normalize-space(text())="' + textToVerify + '"]');

         await action.MouseMoveToElement(locator, attributeName);
         await action.Click(locator, attributeName + " " + textToVerify);

      }
      catch (err) {
         await action.ReportSubStep("clickProgramInRatingsTracksView", "click source collection" + attributeName + " " + textToVerify, "Fail");
      }

   }


   /**
          * desc: clickToggaleProgramsTracksPAVTable
          * @method clickToggaleProgramsTracksPAVTable
          * @author: vikas
          * @param :{}
          * @return none
          */
   async clickToggaleProgramsTracksPAVTable(clickColumnName: string, columnMainName: string, columnToVerifyName: string, textToverify: string, maintexttoMatch: string, CheckUncheck: string) {
      try {
         let locTableRow = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")]');
         let locColumns = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//th[contains(@class,"ui-sortable-column")]');
         let columnIndexMain: number;
         let columnIndex: number;
         let clickColumnIndex: number;
         let flag = false;
         let rowIndexG;
         await browser.sleep(2000);
         await verify.verifyProgressBarNotPresent();
         await browser.sleep(2000);
         await browser.waitForAngular();
         await element.all(locColumns).each(async function (ele, Index) {
            await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
               await ele.getText().then(function (_valuecol) {
                  if (_valuecol.trim() == columnMainName) {
                     columnIndexMain = Index + 1;
                  }
                  if (_valuecol.trim() == columnToVerifyName) {
                     columnIndex = Index + 1;
                  }
                  if (_valuecol.trim() == clickColumnName) {
                     clickColumnIndex = Index + 1;
                  }
               });
            });
         }).then(async function () {


            await element.all(locTableRow).each(async function (ele, Index) {

               let rowIndex = Index + 1
               let locColumn = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndexMain + ']');
               await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {

                  await element(locColumn).getText().then(async function (_text) {
                     if (_text.trim() == maintexttoMatch) {
                        let locColumnReq = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndex + ']');
                        await browser.actions().mouseMove(element(locColumnReq)).perform().then(async function () {

                           await element(locColumnReq).getText().then(function (_value) {
                              if (_value.trim() == textToverify) {
                                 flag = true
                                 rowIndexG = rowIndex;
                              }

                           })
                        });

                     }
                  });
               });
            }).then(async function () {
               console.log("Flag  " + flag);
               if (flag == true) {
                  await browser.sleep(1000);
                  let locColumntoclick = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndexG + ']//td[contains(@class,"ui-resizable-column")][' + clickColumnIndex + ']//input[contains(@class,"mat-slide-toggle-input")]');
                  let loc2 = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndexG + ']//td[contains(@class,"ui-resizable-column")][' + clickColumnIndex + ']//div[normalize-space(@class)="mat-slide-toggle-thumb"]');
                  await action.MouseMoveJavaScript(locColumntoclick, "Mouse Move to aria check");
                  await element(locColumntoclick).getAttribute('aria-checked').then(async function (value) {
                     console.log("Value ++" + value);
                     if ((value.trim() == "false") && (CheckUncheck.toUpperCase() == "CHECK")) {
                        await action.MouseMoveJavaScript(loc2, "td with row " + rowIndexG + " column " + clickColumnIndex);
                        await browser.sleep(1000);
                        await action.Click(loc2, "td with row " + rowIndexG + " column " + clickColumnIndex);
                        await browser.sleep(2000);
                        action.ReportSubStep("clickToggaleProgramsTracksPAVTable", "verify data" + textToverify, "Pass");
                     }
                     if ((value.trim() == "true") && (CheckUncheck.toUpperCase() == "UNCHECK")) {
                        await action.MouseMoveJavaScript(loc2, "td with row " + rowIndexG + " column " + clickColumnIndex);
                        await browser.sleep(1000);
                        await action.Click(loc2, "td with row " + rowIndexG + " column " + clickColumnIndex);
                        await browser.sleep(2000);
                        action.ReportSubStep("clickToggaleProgramsTracksPAVTable", "click data toggale" + textToverify, "Pass");
                     }
                  })


               }
               else {
                  await action.ReportSubStep("clickToggaleProgramsTracksPAVTable", "click data toggale" + textToverify, "Fail");
               }
            });
         });

      }
      catch (err) {
         await action.ReportSubStep("clickToggaleProgramsTracksPAVTable", "click data toggale" + textToverify, "Fail");
      }

   }


   /**
           * desc: click Assign PAV Ratings Programs Traking PAVTable
           * @method clickAssignPAVRatingsProgramsTrakingPAVTable
           * @author: vikas
           * @param :{}
           * @return none
           */
   async clickAssignPAVRatingsProgramsTrakingPAVTable(clickColumnName: string, columnMainName: string, columnToVerifyName: string, textToverify: string, maintexttoMatch: string) {
      try {
         let locTableRow = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")]');
         let locColumns = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//th[contains(@class,"ui-sortable-column")]');
         let columnIndexMain: number;
         let columnIndex: number;
         let clickColumnIndex: number;
         let flag = false;
         let rowIndexG;
         await browser.sleep(2000);
         await verify.verifyProgressBarNotPresent();
         await browser.sleep(2000);
         await browser.waitForAngular();
         await element.all(locColumns).each(async function (ele, Index) {
            await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
               await ele.getText().then(function (_valuecol) {
                  if (_valuecol.trim() == columnMainName) {
                     columnIndexMain = Index + 1;
                  }
                  if (_valuecol.trim() == columnToVerifyName) {
                     columnIndex = Index + 1;
                  }
                  if (_valuecol.trim() == clickColumnName) {
                     clickColumnIndex = Index + 1;
                  }
               });
            });
         }).then(async function () {
            console.log("col main " + columnIndexMain);
            console.log("col col veri" + columnIndex);
            console.log("col click" + clickColumnIndex);
            await browser.sleep(2000);
            await element.all(locTableRow).each(async function (ele, Index) {
               let rowIndex = Index + 1
               let locColumn = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndexMain + ']');
               await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
                  await browser.sleep(500);
                  await element(locColumn).getText().then(async function (_text) {
                     console.log("click text ++ " + _text);
                     if (_text.trim().indexOf(maintexttoMatch) > -1) {
                        let locColumnReq = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndex + ']');
                        await browser.actions().mouseMove(element(locColumnReq)).perform().then(async function () {

                           await element(locColumnReq).getText().then(function (_value) {
                              if (_value.trim().indexOf(textToverify) > -1) {
                                 flag = true
                                 rowIndexG = rowIndex;
                              }

                           })
                        });
                     }
                  });
               });
            }).then(async function () {
               if (flag == true) {
                  await browser.sleep(1000);
                  let locColumntoclick = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndexG + ']//td[contains(@class,"ui-resizable-column")][' + clickColumnIndex + ']//i[contains(@class,"pi pi-caret-down")]');
                  let locColumntAssign = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndexG + ']//td[contains(@class,"ui-resizable-column")][' + clickColumnIndex + ']//a[contains(text(),"Assign PAV")]');
                  await action.MouseMoveJavaScript(locColumntoclick, "column to click " + locColumntoclick);
                  await browser.sleep(1000);
                  await action.Click(locColumntoclick, "td with row " + rowIndexG + " column " + clickColumnIndex);
                  await action.Click(locColumntAssign, "assign link " + rowIndexG + " column " + clickColumnIndex);
                  action.ReportSubStep("clickAssignPAVRatingsProgramsTrakingPAVTable", "verify data" + textToverify, "Pass");
               }
               else {
                  await action.ReportSubStep("clickAssignPAVRatingsProgramsTrakingPAVTable", "verify data" + textToverify, "Fail");
               }
            });
         });

      }
      catch (err) {
         await action.ReportSubStep("clickAssignPAVRatingsProgramsTrakingPAVTable", "verify data" + textToverify, "Fail");
      }

   }


   /**
      * desc:click Row In PAV Popup Grid
      * @method clickRowInPAVPopupGrid
      * @author: vikas
      * @param :{}
      * @return none
      */
   async clickRowInPAVPopupGrid(row: number, column: number) {
      try {

         let locator = by.xpath('//xg-grid[contains(@attr-name,"xgc-build-pav-rating-data-grid")]//tbody//tr[contains(@class,"ng-star-inserted")][' + row + ']//td[contains(@class,"ui-resizable-column")][' + column + ']//div[contains(@class,"ui-chkbox-box")]');
         await verify.verifyProgressBarNotPresent();
         await browser.sleep(2000);
         await action.MouseMoveToElement(locator, "row" + row);
         await action.Click(locator, "row " + row);
      }
      catch (err) {
         action.ReportSubStep("clickRowInPAVPopupGrid", "click row " + row + " col " + column, "Fail");
      }

   }
   /**
   * desc: click buttons in page
   * @method clickonButtonsInPage
   * @author: vikas
   * @param :{}
   * @return none
   */
   async clickonButtonsInPage(buttonAttribute: string, buttonName: string) {

      try {
         let Xpbutton = by.xpath('//xg-button[contains(@attr-name,"' + buttonAttribute + '")]//button[normalize-space(text())="' + buttonName + '"]');
         await browser.sleep(2000);
         await action.MouseMoveToElement(Xpbutton, buttonName)
         await action.Click(Xpbutton, buttonName);
      }
      catch (err) {
         action.ReportSubStep("clickonButtonsInPage", "click button " + buttonName, "Fail");
      }
   }
   /**
     * desc: click drop down Option Inventory
     * @method clickDropdownOptionInventory
     * @author: vikas
     * @param :{}
     * @return none
     */
   async clickDropdownOptionInventory(buttonAttribute: string, optionName: string) {

      try {
         let locator = by.css('xg-button[attr-name*="' + buttonAttribute + '"] ul#dropdownMenuButton a');
         let eleitem;

         eleitem = await action.makeDynamicLocatorContainsText(locator, optionName, "Equal");
         await action.MouseMoveJavaScript(eleitem, optionName);
         await action.Click(eleitem, optionName);
      }
      catch (err) {
         action.ReportSubStep("clickDropdownOptionInventory", "click option " + optionName, "Fail");
      }
   }
   /**
      * desc: verify Data In Assign PAV Ratings Programs Traks PAVTable
      * @method verifyDataInAssignPAVRatingsProgramsTraksPAVTable
      * @author: vikas
      * @param :{}
      * @return none
      */
   async verifyDataInAssignPAVRatingsProgramsTraksPAVTable(columnMainName: string, columnToVerifyName: string, textToverify: string, maintexttoMatch: string) {
      try {
         let locTableRow = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")]');
         let locColumns = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//th[contains(@class,"ui-sortable-column")]');
         let columnIndexMain: number;
         let columnIndex: number;
         let flag = false;
         let rowIndexG;

         await browser.sleep(2000);
         await verify.verifyProgressBarNotPresent();
         await browser.sleep(2000);
         await browser.waitForAngular();
         await element.all(locColumns).each(async function (ele, Index) {
            await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
               await ele.getText().then(function (_valuecol) {
                  if (_valuecol.trim() == columnMainName) {
                     columnIndexMain = Index + 1;
                  }
                  if (_valuecol.trim() == columnToVerifyName) {
                     columnIndex = Index + 1;
                  }
               });
            });
         }).then(async function () {


            await element.all(locTableRow).each(async function (ele, Index) {
               let rowIndex = Index + 1
               let locColumn = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndexMain + ']');
               await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
                  await element(locColumn).getText().then(async function (_text) {
                     console.log("Main Text" + _text)
                     if (_text.trim().indexOf(maintexttoMatch) > -1) {
                        let locColumnReq = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndex + ']');
                        await browser.executeScript('arguments[0].scrollIntoView()', element(locColumnReq).getWebElement()).then(async function () {

                           await element(locColumnReq).getText().then(function (_value) {
                              console.log("subtext Text" + _value)
                              if (_value.trim() == textToverify) {
                                 flag = true
                                 rowIndexG = rowIndex;
                              }

                           })
                        });
                     }
                  });
               });
            }).then(async function () {
               if (flag == true) {
                  await action.ReportSubStep("verifyDataInAssignPAVRatingsProgramsTraksPAVTable", "verify data " + textToverify, "Pass");
               }
               else {
                  await action.ReportSubStep("verifyDataInAssignPAVRatingsProgramsTraksPAVTable", "verify data " + textToverify, "Fail");
               }
            });
         });

      }
      catch (err) {
         await action.ReportSubStep("verifyDataInAssignPAVRatingsProgramsTraksPAVTable", "verify data " + textToverify, "Fail");
      }

   }
   /**
   * desc: verify Active Inactive In PAV Ratings Programs Traks PAV Table
   * @method verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable
   * @author: vikas
   * @param :{}
   * @return none
   */
   async verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable(columnMainName: string, columnToVerifyName: string, textToverify: string, activeColName: string, trueFalse: string, maintexttoMatch: string) {
      try {
         let locTableRow = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")]');
         let locColumns = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//th[contains(@class,"ui-sortable-column")]');
         let columnIndexMain: number;
         let columnIndex: number;
         let activeCol: number;
         let flag = false;
         let rowIndexG;

         await browser.sleep(2000);
         await verify.verifyProgressBarNotPresent();
         await browser.waitForAngular();
         await element.all(locColumns).each(async function (ele, Index) {
            await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
               await ele.getText().then(function (_valuecol) {
                  if (_valuecol.trim() == columnMainName) {
                     columnIndexMain = Index + 1;
                  }
                  if (_valuecol.trim() == columnToVerifyName) {
                     columnIndex = Index + 1;
                  }
                  if (_valuecol.trim() == activeColName) {
                     activeCol = Index + 1;
                  }
               });
            });
         }).then(async function () {

            await browser.sleep(1000);
            await element.all(locTableRow).each(async function (ele, Index) {
               let rowIndex = Index + 1
               let locColumn = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndexMain + ']');
               await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
                  await element(locColumn).getText().then(async function (_text) {
                     console.log("Main Text" + _text)
                     if (_text.trim().indexOf(maintexttoMatch) > -1) {
                        let locColumnReq = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndex + ']');
                        let locColumnReq1 = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + activeCol + ']//input[contains(@class,"mat-slide-toggle-input")]');
                        await browser.executeScript('arguments[0].scrollIntoView()', element(locColumnReq).getWebElement()).then(async function () {
                           await element(locColumnReq).getText().then(async function (_value) {
                              console.log("subtext Text" + _value)
                              if (_value.trim().indexOf(textToverify) > -1) {
                                 await element(locColumnReq1).getAttribute('aria-checked').then(function (_atribute) {
                                    if (_atribute.trim() == trueFalse) {
                                       flag = true
                                       rowIndexG = rowIndex;
                                    }

                                 });

                              }

                           })
                        });
                     }
                  });
               });
            }).then(async function () {
               if (flag == true) {
                  await action.ReportSubStep("verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable", "verify data " + trueFalse, "Pass");
               }
               else {
                  await action.ReportSubStep("verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable", "verify data " + trueFalse, "Fail");
               }
            });
         });

      }
      catch (err) {
         await action.ReportSubStep("verifyActiveInactiveInPAVRatingsProgramsTraksPAVTable", "verify active or inactive Pav record " + trueFalse, "Fail");
      }

   }
   /*
       * desc:  To Verify Checkbox is selected or not (dynamic obj)
       * @method: verifyCheckBoxIsSelected
       * @author: Dhana
       * @param : row number in the grid
       * @return none
    */

   // async verifyCheckBoxIsSelected(row: number) {
   //    try {
   //       let locator = by.css('xg-grid[attr-name*="xgc-build-pav-rating-data-grid"] tbody tr[class*="ng-star-inserted"]:nth-of-type(' + row + ') td:nth-of-type(1) div[class*="ui-state-active"]');
   //       await browser.sleep(3000);
   //       await verify.verifyElement(locator, 'Pav records');
   //    }
   //    catch (err) {
   //       await action.ReportSubStep("verifyCheckBoxIsSelected", "verify row is selected" + row, "Fail");
   //    }

   // }
   /**
     * desc: verify Buttons in Inventory page
     * @method verifyButtonsInventory
     * @author: vikas
     * @param :{}
     * @return none
     */
   async verifyButtonsInventory(buttonAttribute: string, buttonName: string) {

      try {
         let locator = by.css('xg-button[attr-name*="' + buttonAttribute + '"] #dropdownMenuButton');
         let eleitem;
         eleitem = await action.makeDynamicLocatorContainsText(locator, buttonName, "Equal");
         await verify.verifyElementIsDisplayed(eleitem, buttonName);
      }
      catch (err) {
         await action.ReportSubStep("verifyButtonsInventory", "verify button" + buttonName, "Fail");
      }
   }
   /**
     * desc: verify drop down Option Inventory
     * @method verifydropdownOptionInventory
     * @author: vikas
     * @param :{}
     * @return none
     */
   async verifydropdownOptionInventory(buttonAttribute: string, optionName: string) {

      try {
         let locator = by.css('xg-button[attr-name*="' + buttonAttribute + '"] ul#dropdownMenuButton a');
         let eleitem;
         eleitem = await action.makeDynamicLocatorContainsText(locator, optionName, "Equal");
         await verify.verifyElementIsDisplayed(eleitem, optionName);

      }
      catch (err) {
         await action.ReportSubStep("verifydropdownOptionInventory", "verify drop dwon option" + optionName, "Fail");
      }
   }
   /**
      * desc: verify data is not present in PAV Ratigs program 
      * @method verifyDataIsNotPresentInAssignPAVRatingsProgramsTraksPAVTable
      * @author: vikas
      * @param :{}
      * @return none
      */
   async verifyDataIsNotPresentInAssignPAVRatingsProgramsTraksPAVTable(columnMainName: string, columnToVerifyName: string, textToverify: string, maintexttoMatch: string) {
      try {
         let locTableRow = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")]');
         let locColumns = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//th[contains(@class,"ui-sortable-column")]');
         let columnIndexMain: number;
         let columnIndex: number;
         let flag = false;
         let rowIndexG;
         await browser.sleep(2000);
         await verify.verifyProgressBarNotPresent();
         await browser.sleep(2000);
         await element.all(locColumns).each(async function (ele, Index) {
            await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
               await ele.getText().then(function (_valuecol) {
                  if (_valuecol.trim() == columnMainName) {
                     columnIndexMain = Index + 1;
                  }
                  if (_valuecol.trim() == columnToVerifyName) {
                     columnIndex = Index + 1;
                  }
               });
            });
         }).then(async function () {


            await element.all(locTableRow).each(async function (ele, Index) {
               let rowIndex = Index + 1
               let locColumn = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndexMain + ']');
               await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
                  await element(locColumn).getText().then(async function (_text) {
                     console.log("Main Text" + _text)
                     if (_text.trim().indexOf(maintexttoMatch) > -1) {
                        let locColumnReq = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndex + ']');
                        await browser.executeScript('arguments[0].scrollIntoView()', element(locColumnReq).getWebElement()).then(async function () {

                           await element(locColumnReq).getText().then(function (_value) {
                              console.log("subtext Text" + _value)
                              if (_value.trim() == textToverify) {
                                 flag = true
                                 rowIndexG = rowIndex;
                              }

                           })
                        });
                     }
                  });
               });
            }).then(async function () {
               if (flag == true) {
                  action.ReportSubStep("verifyDataIsNotPresentInAssignPAVRatingsProgramsTraksPAVTable", "verify data not present" + textToverify, "Fail");
               }
               else {
                  action.ReportSubStep("verifyDataIsNotPresentInAssignPAVRatingsProgramsTraksPAVTable", "verify data not present" + textToverify, "Pass");
               }
            });
         });

      }
      catch (err) {
         await action.ReportSubStep("verifyDataIsNotPresentInAssignPAVRatingsProgramsTraksPAVTable", "verify data not present" + textToverify, "Fail");

      }

   }
   /**
   * desc: click fileds like Markets,Channel(s) etc with multiselect option
   * @method clickFieldWithComboboxMultiselect
   * @author: vikas
   * @param :{}
   * @return none
   */
   async clickFieldWithComboboxMultiselect(attributeName: string) {
      try {
         await browser.sleep(2000)
         let Xpmutiselect = by.xpath('//xg-multi-select[contains(@attr-name,"' + attributeName + '")]//p-multiselect');
         await action.MouseMoveToElement(Xpmutiselect, attributeName);
         await action.Click(Xpmutiselect, attributeName);

      }
      catch (err) {
         await action.ReportSubStep("clickFieldWithComboboxMultiselect", "click multiselect drop down" + attributeName, "Fail");
      }

   }
   /**
   * desc: select option from Multi select 
   * @method selectOptionFromMultiselect
   * @author: vikas
   * @param :{}
   * @return none
   */
   async selectOptionFromMultiselect(textToSelect: string, log: string) {
      try {
         let Xpmutiselect = by.xpath('//p-multiselectitem//li//label[contains(text(),"' + textToSelect + '")]|//p-multiselectitem//li//span[contains(text(),"' + textToSelect + '")]');
         await action.MouseMoveToElement(Xpmutiselect, log);
         await action.Click(Xpmutiselect, log);
      }
      catch (err) {
         await action.ReportSubStep("selectOptionFromMultiselect", "click option from drop down" + textToSelect, "Fail");
      }

   }
   /**
   * desc: return Option From Multi select Is Present
   * @method returnOptionFromMultiselectIsPresent
   * @author: vikas
   * @param :{}
   * @return none
   */
   async returnOptionFromMultiselectIsPresent(textToSelect: string, log: string) {
      try {
         let Xpmutiselect = by.xpath('//p-multiselectitem//li//label[contains(text(),"' + textToSelect + '")]|//p-multiselectitem//li//span[contains(text(),"' + textToSelect + '")]');
         await browser.sleep(1000)
         await element(Xpmutiselect).isPresent().then(function (value) {
            if (value == true) {
               return true
            }
            else {
               return false
            }
         })
      }
      catch (err) {
         return false
      }

   }
   /**
   * desc: click Close Button Multi Select List Dropdown
   * @method clickCloseButtonMultiSelectListDropdown
   * @author: vikas
   * @param :{}
   * @return none
   */
   async clickCloseButtonMultiSelectListDropdown() {
      try {
         let locator = by.css('a[class*="ui-multiselect-close"] span[class*="pi pi-times"]');
         await action.MouseMoveJavaScript(locator, "x Button Multiselect");
         await action.Click(locator, 'x Button Multiselect');
      }
      catch (err) {
         await action.ReportSubStep("clickCloseButtonMultiSelectListDropdown", "click on dropdown x button", "Fail");
      }

   }
   /**
   * desc: select check box of commission  like Telivision , Digital etc  
   * @method selectCheckBox
   * @author: vikas
   * @param :{}
   * @return none
   */
   async selectCheckBox(lableName: string) {
      try {
         let locator = by.xpath('//xg-checkbox//input[contains(@type,"checkbox")]/../label[normalize-space(text())="' + lableName + '"]');
         await action.MouseMoveJavaScript(locator, lableName);
         await action.Click(locator, lableName);
      }
      catch (err) {
         await action.ReportSubStep("selectCheckBox", "select check box " + lableName, "Fail");
      }

   }

   /**
        * desc: click Multi Select All Options
        * @method clickMultiSelectAllOptions
        * @author: vikas
        * @param :{}
        * @return none
        */
   async clickMultiSelectAllOptions() {

      try {
         let locator = by.xpath('//div[contains(@class,"ui-widget-header ui-corner-all")]//div[contains(@class,"ui-chkbox-box")]');
         await browser.sleep(2000);
         await action.MouseMoveJavaScript(locator, "All Option");
         await action.Click(locator, "All Option multi select");
      }
      catch (err) {
         await action.ReportSubStep("clickMultiSelectAllOptions", "click select all from drop down", "Fail");
      }
   }
   /**
         * desc: select all options from Multi Select DropDown
         * @method selectAllOptionsFromMultiSelectDropDown
         * @author: vikas
         * @param :{}
         * @return none
         */
   async selectAllOptionsFromMultiSelectDropDown() {

      try {
         let locator = by.xpath('//p-multiselectitem//li[contains(@class,"ui-multiselect-item")]//div[contains(@class,"ui-chkbox-box")]');
         await browser.sleep(2000);
         await element.all(locator).each(async function (ele, Index) {
            await browser.actions().mouseMove(ele).perform().then(async function () {
               await action.Click(ele, "Option " + Index);
            });

         });

      }
      catch (err) {
         await action.ReportSubStep("clickMultiSelectAllOptions", "click select all from drop down", "Fail");
      }
   }

   /**
    * desc: click Days Option Short Cut ActiveInactive
    * @method clickDaysOptionShortCutActiveInactive
    * @author: vikas
    * @param :{}
    * @return none
    */
   async clickDaysOptionShortCutActiveInactive(attrinuteName: string, activeInactive: string, optionName: string) {
      try {
         let loactor;
         if (activeInactive.toUpperCase() == "ACTIVE") {
            loactor = by.xpath('//xg-day-picker[contains(@attr-name,"' + attrinuteName + '") or contains(@name,"' + attrinuteName + '")]//span//a[contains(text(),"' + optionName + '")][contains(@class,"active")]');
         }
         if (activeInactive.toUpperCase() == "INACTIVE") {
            loactor = by.xpath('//xg-day-picker[contains(@attr-name,"' + attrinuteName + '") or contains(@name,"' + attrinuteName + '")]//span//a[contains(text(),"' + optionName + '")]');
         }



         await action.Click(loactor, optionName + " and selection " + activeInactive);

      }
      catch (err) {
         await action.ReportSubStep("clickDaysOptionShortCutActiveInactive", "click on option " + optionName, "Fail");
      }

   }


   /**
    * desc: click single select drop down
    * @method clickComponentPdropdown
    * @author: vikas
    * @param :{}
    * @return none
    */
   async clickComponentPdropdown(dropdownAttrName: string) {

      try {
         let locator = by.css('xg-dropdown[attr-name*="' + dropdownAttrName + '"] p-dropdown[class*="xg-dropdown"] label');
         await action.MouseMoveToElement(locator, dropdownAttrName);
         await action.Click(locator, dropdownAttrName + " DropDown");
      }
      catch (err) {
         await action.ReportSubStep("clickComponentPdropdown", "click on drop down " + dropdownAttrName, "Fail");
      }
   }

   /**
   * desc: click single select drop down in Popup
   * @method clickComponentPdropdownInPopup
   * @author: vikas
   * @param :{}
   * @return none
   */
   async clickComponentPdropdownInPopup(dropdownAttrName: string) {

      try {
         let locator = by.css('xg-popup xg-dropdown[attr-name*="' + dropdownAttrName + '"] p-dropdown[class*="xg-dropdown"] label');
         await action.MouseMoveToElement(locator, dropdownAttrName);
         await action.Click(locator, dropdownAttrName + " DropDown");
      }
      catch (err) {
         await action.ReportSubStep("clickComponentPdropdown", "click on drop down " + dropdownAttrName, "Fail");
      }
   }

   /**
   * desc: select options from Component Pdropdown
   * @method selectOptionsFromComponentPdropdown
   * @author: vikas
   * @param :{}
   * @return none
   */
   async selectOptionsFromComponentPdropdown(optionText: string) {

      try {
         let locator = by.xpath('//ul[contains(@class,"ui-dropdown-items")]//li[contains(@class,"ui-dropdown-item")]//span[normalize-space(text())="' + optionText + '"]');
         await action.MouseMoveToElement(locator, optionText);
         await action.Click(locator, optionText + " DropDown");

      }
      catch (err) {
         await action.ReportSubStep("selectOptionsFromComponentPdropdown", "click on drop down option " + optionText, "Fail");
      }
   }

   /**
   * desc: Used to get the firstRow Metrics And Raings of first column of demo  
   * @method: getFirstRowMetricsAndRatings
   * @author: Venkata Sivakumar
   * @param :{}
   * @return metricsAndRatings
   */

   async getFirstRowMetricsAndRatings() {
      let metrics;
      let ratings;
      let actualMetrics: Array<any> = new Array<any>();
      let actualRatings: Array<any> = new Array<any>();
      let metricsValues: Array<any>;
      let ratingsValues: Array<any>;
      let metricsLength;
      let ratingsLength;
      let uiRTGValue;
      let uiSHRValue;
      let uiHPValue;
      let uiSIUValue;
      let metricsAndRatings;
      try {
         metrics = await action.GetText(this.firstRowMetrics, "");
         ratings = await action.GetText(this.firstRowRatings, "");
         metricsValues = metrics.trim().split("\n");
         ratingsValues = ratings.trim().split("\n");
         metricsValues.forEach(function (value) {
            if (value.trim() != "") {
               actualMetrics.push(value.trim());
            }
         });
         ratingsValues.forEach(function (value) {
            if (value.trim() != "") {
               actualRatings.push(value.trim());
            }
         });
         metricsLength = actualMetrics.length;
         ratingsLength = actualRatings.length;
         if (metricsLength == ratingsLength) {
            for (let index = 0; index < metricsLength; index++) {
               switch (metricsValues[index]) {
                  case "RTG":
                     uiRTGValue = ratingsValues[index];
                     break;
                  case "SHR":
                     uiSHRValue = ratingsValues[index];
                     break;
                  case "HP":
                     uiHPValue = ratingsValues[index];
                     break;
                  case "SIU":
                     uiSIUValue = ratingsValues[index];
                     break;
                  default:
                     break;
               }
            }
         }
         return metricsAndRatings = { "RTG": uiRTGValue, "SHR": uiSHRValue, "HP": uiHPValue, "SIU": uiSIUValue };
      } catch (error) {
         throw new Error(error);
      }
   }

   /**
 * desc: Used to get the Share,Rating and HutPut values from API after calculation  
 * @method: getRatingsFromAPIAfterCalculation
 * @author: Venkata Sivakumar
 * @param :{dataDetails}
 * @return metricsAndRatings
 */
   async getRatingsFromAPIAfterCalculation(dataDetails) {
      try {
         let finalShare = 0;
         let finalRating = 0;
         let count = dataDetails.length;
         let finalPeopleUsingTelevision = 0;
         let finalPUT = 0;
         let metricsAndRatings;
         for (let index = 0; index < dataDetails.length; index++) {
            let apiProgramName = dataDetails[index].programName;
            let apiStartTime = dataDetails[index].startTime;
            let apiEndTime = dataDetails[index].endTime;
            let apiShareValue = dataDetails[index].share;
            let apiRating = dataDetails[index].rating;
            let apiTotalImpression = dataDetails[index].totalImpressions;
            let apiTotalUniverse = dataDetails[index].totalUniverse;
            let apiTotalHUTPUT = dataDetails[index].totalHUTPUT;
            let apiRatingHUTPUT = dataDetails[index].ratingHUTPUT;
            let apiRatingPeopleWatching = dataDetails[index].ratingPeopleWatching;
            let apiRatingUniverse = dataDetails[index].ratingUniverse;
            let apiUniverse = dataDetails[index].universe;
            let apiPeopleUsingTelevision = dataDetails[index].peopleUsingTelevision;
            finalRating = finalRating + (apiTotalImpression / apiTotalUniverse) * 100
            finalPeopleUsingTelevision = finalPeopleUsingTelevision + apiPeopleUsingTelevision;
            finalPUT = finalPUT + (apiRatingHUTPUT / apiRatingUniverse) * 100;
         }
         finalShare = finalShare / count;
         finalRating = finalRating / count;
         finalPUT = finalPUT / count;
         finalPeopleUsingTelevision = finalPeopleUsingTelevision / count;
         return metricsAndRatings = { "RTG": finalRating, "SHR": finalShare, "HP": finalPUT };
      } catch (error) {
         throw new Error(error);
      }
   }


   /**
         * desc: verifyXgmultiselectSelectionValue
         * @method verifyXgmultiselectSelectionValue
         * @author: vikas
         * @param :{}
         * @return none
         */
   async verifyXgmultiselectSelectionValue(dropDownName: string, selectedOption: string) {
      try {
         let loactor = by.xpath("//xg-multi-select[contains(@name,'" + dropDownName + "')]//p-multiselect//span[contains(@class,'ui-multiselect-label')]|//xg-multi-select[contains(@name,'" + dropDownName + "')]//p-multiselect//label[contains(@class,'ui-multiselect-label')]");
         await verify.verifyTextInElement(loactor, selectedOption, selectedOption + " and drop down " + dropDownName);
      }
      catch (err) {
         await action.ReportSubStep("verifyXgmultiselectSelectionValue", "verify multi select drop down option" + selectedOption, "Fail");
      }

   }


   /**
* desc: retunComboboxMultiselectOptionAtIndex
* @method retunComboboxMultiselectOptionAtIndex
* @author: vikas
* @param :{}
* @return none
*/
   async retunComboboxMultiselectOptionAtIndex(attrname: string, index: number) {
      try {
         let Xpmutiselect = by.xpath('//xg-multi-select[contains(@attr-name,"' + attrname + '")]//following-sibling::p-multiselectitem[' + index + ']//label');
         await action.MouseMoveToElement(Xpmutiselect, attrname);
         let eleValue = await action.GetText(Xpmutiselect, attrname);
         return eleValue;
      }
      catch (err) {
         await action.ReportSubStep("retunComboboxMultiselectOptionAtIndex", "Retun option at " + attrname, "Fail");
      }

   }


   /*
  * desc: verifyDataInXgcbuildPavRatingDataGrid
  * @method verifyDataInXgcbuildPavRatingDataGrid
  * @author: vikas
  * @param :{}
  * @return none
  */
   async verifyDataInXgcbuildPavRatingDataGrid(columnName: string, textToVerify: string) {
      try {
         let locTableRow = by.xpath('//xg-grid[contains(@attr-name,"xgc-build-pav-rating-data-grid")]//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")]');
         let locColumns = by.xpath('//xg-grid[contains(@attr-name,"xgc-build-pav-rating-data-grid")]//div[contains(@class,"ui-table-frozen-view")]//th[contains(@class,"ui-sortable-column")]');
         let columnIndex: number;
         let flag = false;

         await browser.sleep(2000);
         await verify.verifyProgressBarNotPresent();
         await browser.sleep(2000);
         await browser.waitForAngular();
         await element.all(locColumns).each(async function (ele, Index) {
            await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
               await ele.getText().then(function (_valuecol) {
                  if (_valuecol.trim() == columnName) {
                     console.log("Col Name " + _valuecol);
                     columnIndex = Index + 2;
                  }
               });
            });
         }).then(async function () {
            console.log("ColumnIndex" + columnIndex);

            await element.all(locTableRow).each(async function (ele, Index) {
               let rowIndex = Index + 1
               let locColumn = by.xpath('//xg-grid[contains(@attr-name,"xgc-build-pav-rating-data-grid")]//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndex + ']');
               await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
                  await element(locColumn).getText().then(async function (_text) {
                     console.log("Main Text" + _text)
                     if (_text.trim().indexOf(textToVerify) > -1) {
                        flag = true;
                     }
                  });
               });
            }).then(async function () {
               if (flag == true) {
                  await action.ReportSubStep("verifyDataInXgcbuildPavRatingDataGrid", "verify data " + textToVerify, "Pass");
               }
               else {
                  await action.ReportSubStep("verifyDataInXgcbuildPavRatingDataGrid", "verify data " + textToVerify, "Fail");
               }
            });
         });

      }
      catch (err) {
         await action.ReportSubStep("verifyDataInXgcbuildPavRatingDataGrid", "verify data Error" + textToVerify, "Fail");
      }

   }


   /**
         * desc:  verify Input Field Text in rate card 
         * @method verifyInputFieldText
         * @author: vikas
         * @param :{}
         * @return none
         */
   async verifyInputFieldText(headerName: string, TextValue: string) {

      try {
         let XpInput = by.xpath('//label[contains(text(),"' + headerName + '")]/following-sibling::input');
         await browser.waitForAngular();
         await action.MouseMoveToElement(XpInput, headerName + "Input");
         let _text: string = String(await action.GetTextFromInput(XpInput, headerName + "Input"));
         if (_text.trim() == TextValue) {
            action.ReportSubStep("verifyInputFieldText", "text " + _text, "Pass");
         }
         else {
            action.ReportSubStep("verifyInputFieldText", "text " + _text, "Fail");
         }

      }
      catch (err) {
         action.ReportSubStep("verifyInputFieldText", "text Error" + headerName, "Fail");
      }
   }


   /**
        * desc: verify check box is selected of commission  like Telivision , Digital etc  
        * @method verifyCheckBoxIsSelected
        * @author: vikas
        * @param :{}
        * @return none
        */
   async verifyCheckBoxIsSelected(lableName: string) {
      try {
         let XpCheckBox = by.xpath('//label[normalize-space(text())="' + lableName + '"]//preceding-sibling::input[contains(@type,"checkbox")]');
         await browser.waitForAngular();
         await action.MouseMoveToElement(XpCheckBox, lableName);
         await browser.sleep(1000);
         await element(XpCheckBox).isSelected().then(function (value) {
            console.log("Check box value " + value);
            if (value == true) {
               action.ReportSubStep("verifyCheckBoxIsSelected", lableName + " Is Selected", "Pass");
            }
            else {
               action.ReportSubStep("verifyCheckBoxIsSelected", lableName + " Is not Selected", "Fail");
            }
         }, function (err) {
            action.ReportSubStep("verifyCheckBoxIsSelected", lableName + "Error in getting check box value", "Fail");
         });

      }
      catch (err) {
         action.ReportSubStep("verifyCheckBoxIsSelected", lableName + "Error in  try block", "Fail");
      }

   }


   /**
  * desc:verify Row In PAVPopupGridIsNotSelected
  * @method verifyRowInPAVPopupGridIsNotSelected
  * @author: vikas
  * @param :{}
  * @return none
  */
   async verifyRowInPAVPopupGridIsNotSelected(row: number, column: number) {
      try {

         let locator = by.xpath('//xg-grid[contains(@attr-name,"xgc-build-pav-rating-data-grid")]//tbody//tr[' + row + ']//td[' + column + ']//div[contains(@class,"ui-chkbox-box")]//span[contains(@class,"ui-chkbox-icon")]');
         await browser.sleep(2000);
         await browser.waitForAngular()
         await verify.verifyProgressBarNotPresent();
         let cValue = String(await action.getElementAttribute(locator, 'class', 'verifyRowInPAVPopupGridIsChecked'));
         if (cValue.indexOf("pi pi-check") > -1) {
            action.ReportSubStep("verifyRowInPAVPopupGridIsNotSelected", locator, "Fail");
         }
         else {
            action.ReportSubStep("verifyRowInPAVPopupGridIsNotSelected", locator, "Pass");
         }


      }
      catch (err) {
         action.ReportSubStep("verifyRowInPAVPopupGridIsNotSelected", "Error In Block", "Fail");
      }

   }

   /**
        * desc:verify Row In PAV Popup Grid Is Checked
        * @method verifyRowInPAVPopupGridIsChecked
        * @author: vikas
        * @param :{}
        * @return none
        */
   async verifyRowInPAVPopupGridIsChecked(row: number, column: number) {
      try {

         let locator = by.xpath('//xg-grid[contains(@attr-name,"xgc-build-pav-rating-data-grid")]//tbody//tr[' + row + ']//td[' + column + ']//div[contains(@class,"ui-chkbox-box")]//span[contains(@class,"ui-chkbox-icon")]');
         await browser.sleep(2000);
         await browser.waitForAngular();
         await verify.verifyProgressBarNotPresent();
         await verify.verifyElementAttribute(locator, 'class', 'pi pi-check', 'Row ' + row);
      }
      catch (err) {
         action.ReportSubStep("verifyRowInPAVPopupGridIsChecked", "Error in  try block", "Fail");
      }

   }


   /**
* desc: verify XG Input Switch Status Switch In Grid
* @method verifyXGInputSwitchStatusInSUBGrid
* @author: Adithya K
* @param :{}
* @return none
*/
   async verifyXGInputSwitchStatusInSUBGrid(TRUEORFALSE: string) {
      try {
         let locator = by.css('xg-grid div[class*="ui-table-frozen-view"] tr mat-slide-toggle div input')
         let status: string
         let attributeName: string = "aria-checked"
         // await action.SwitchToFrame(0);
         await element.all(locator).each(async function (val) {
            val.getAttribute(attributeName).then(function (text) {
               status = String(text.toUpperCase());
               console.log(status)
               if (status == TRUEORFALSE) {
                  action.ReportSubStep("verifyXGInputSwitchStatusInSUBGrid", "XG InputSwitch Status is " + TRUEORFALSE, "Pass");
               }
               else {
                  action.ReportSubStep("verifyXGInputSwitchStatusInSUBGrid", "XG InputSwitch Status is not " + TRUEORFALSE, "Fail");
               }
            })
         })
         // await action.SwitchToDefaultFrame();
      }
      catch (err) {
         action.ReportSubStep("verifyXGInputSwitchStatusInSUBGrid", "XG InputSwitch Status is not " + TRUEORFALSE, "Fail");
         throw new Error(err);
      }
   }
   /**
      * desc: To verify show InActive Surveys InSUBGrid
      * @method verifyshowInActiveSurveysInSUBGrid
      * @author: Adithya K
      * @param :{}
      * @return none
      */
   async verifyshowInActiveSurveysInSUBGrid() {
      try {
         let locator = by.css('xg-grid div[class*="ui-table-frozen-view"] tr mat-slide-toggle div input')
         let status: string
         let attributeName: string = "aria-checked"
         //await action.SwitchToFrame(0);
         await element.all(locator).each(async function (val) {
            val.getAttribute(attributeName).then(function (text) {
               status = String(text.toUpperCase());
               console.log(status)
               if (status == "TRUE" || status == "FALSE") {
                  action.ReportSubStep("verifyshowInActiveSurveysInSUBGrid", "InActive Surveys displayed In SUBGrid", "Pass");
               }
               else {
                  action.ReportSubStep("verifyshowInActiveSurveysInSUBGrid", "InActive Surveys not displayed In SUBGrid", "Fail");
               }
            })
         })
         //await action.SwitchToDefaultFrame();
      }
      catch (err) {
         action.ReportSubStep("verifyshowInActiveSurveysInSUBGrid", "InActive Surveys not displayed In SUBGrid", "Fail");
         throw new Error(err);
      }
   }

   /**
           * desc: retrun Assign PAV Ratings Programs Records PAV Table
           * @method returnAssignPAVRatingsProgramsRecordsPAVTable
           * @author: vikas
           * @param :{}
           * @return none
           */
   async returnAssignPAVRatingsProgramsRecordsPAVTable(columnMainName: string, columnToVerifyName: string, textToverify: string) {
      try {
         let locTableRow = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")]');
         let locColumns = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//th[contains(@class,"ui-sortable-column")]');
         let columnIndexMain: number;
         let columnIndex: number;
         let clickColumnIndex: number;
         let AllData = {}


         await browser.sleep(2000);
         await verify.verifyProgressBarNotPresent();
         await browser.sleep(2000);
         await browser.waitForAngular();
         await element.all(locColumns).each(async function (ele, Index) {
            await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
               await ele.getText().then(function (_valuecol) {
                  if (_valuecol.trim() == columnMainName) {
                     columnIndexMain = Index + 1;
                  }
                  if (_valuecol.trim() == columnToVerifyName) {
                     columnIndex = Index + 1;
                  }
               });
            });
         }).then(async function () {
            console.log("col main " + columnIndexMain);
            console.log("col col veri" + columnIndex);

            await browser.sleep(2000);
            await element.all(locTableRow).each(async function (ele, Index) {
               let rowIndex = Index + 1
               let locColumn = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndexMain + ']');
               await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
                  await browser.sleep(500);
                  await element(locColumn).getText().then(async function (_text) {
                     console.log("click text ++ " + _text);

                     let locColumnReq = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndex + ']');
                     await browser.actions().mouseMove(element(locColumnReq)).perform().then(async function () {

                        await element(locColumnReq).getText().then(function (_value) {
                           if (_value.trim().indexOf(textToverify) > -1) {
                              AllData[rowIndex] = _text.trim();
                           }

                        })
                     });

                  });
               });
            })
         });
         return AllData;
      }
      catch (err) {
         await action.ReportSubStep("returnAssignPAVRatingsProgramsRecordsPAVTable", "Retrun Data", "Fail");
      }

   }

}