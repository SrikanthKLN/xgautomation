import { browser, element, by, WebElement } from "protractor";
import { LocalProgramming } from "../../Pages/Inventory/LocalProgramming";
import { ActionLib } from "../../Libs/GeneralLibs/ActionLib";
import { VerifyLib } from "../../Libs/GeneralLibs/VerificationLib";
import { AppCommonFunctions } from "../../Libs/ApplicationLibs/CommAppLib";

let action = new ActionLib();
let verify = new VerifyLib();
let localProgram = new LocalProgramming();
let commonLib = new AppCommonFunctions();

export class InventoryPage {
    //Locators
    headerLocalProgram = by.xpath("//h3[normalize-space()='Local Programs']");
    firstRowSellingTitle = by.css("xg-grid[attr-name='xgc-program-grid']>div[class='xg-grid'] table>tbody>tr:nth-child(1)>td:nth-child(3)");
    activeCheckBox = by.css("xg-checkbox[attr-name='xgc-active'][ng-reflect-model='false'] input");
    activeCheckBoxSelect = by.css("xg-checkbox[attr-name='xgc-active'][ng-reflect-model='true'] input");
    deleteButton = by.xpath("//xg-button[@attr-name='xgc-delete-program-button' or @attr-name='xgc-network-program-delete']//button");
    confirmDeletePopupDeleteButton = by.css("xg-popup[attr-name*='xgc-delete']  xg-button[attr-name*='xgc-delete']");
    confirmDeletePopupCancelButton = by.css("xg-popup[attr-name*='xgc-delete']  xg-button[attr-name*='xgc-cancel']");
    genreInput = by.xpath("//xg-typeahead[@attr-name='xgc-select-genre' or @attr-name='xgc-typeahead-genre']//input");
    tagInput = by.xpath("//xg-typeahead[@attr-name='xgc-select-keyword' or @attr-name='xgc-typeahead-tag']//input");
    externalIdInput = by.css("xg-input[attr-name*='external'] input");
    lockCheckBox = by.css("xg-checkbox[attr-name*='lock'] input[ng-reflect-model='false']+label");
    lockCheckBoxActive = by.css("xg-checkbox[attr-name*='lock'] input[ng-reflect-model='true']+label");
    ftcInput = by.xpath("//xg-date-picker[@attr-name='xgc-telecast-startdate-date-picker' or @attr-name='xgc-date-start' or @attr-name='xgc-startdate-date-picker' or @attr-name='xgc-telecast-start-date']//input");
    ltcInput = by.xpath("//xg-date-picker[@attr-name='xgc-telecast-enddate-date-picker' or @attr-name='xgc-date-end' or @attr-name='xgc-enddate-date-picker' or @attr-name='xgc-telecast-end-date']//input");
    popupActiveCheckBoxSelect = by.xpath("//xg-checkbox[@attr-name='xgc-program-active' or @attr-name='xgc-check-active']//input[@ng-reflect-model='true']/following-sibling::label");
    popupActiveCheckBox = by.xpath("//xg-checkbox[@attr-name='xgc-program-active' or @attr-name='xgc-check-active']//input[@ng-reflect-model='false']/following-sibling::label");
    tagSuggestionListItems = by.xpath("//xg-typeahead[@attr-name='xgc-select-keyword' or @attr-name='xgc-typeahead-tag']//ul[contains(@class,'ui-autocomplete-items')]/li");
    genreSuggestionListItems = by.xpath("//xg-typeahead[@attr-name='xgc-select-genre' or @attr-name='xgc-typeahead-genre']//ul[contains(@class,'ui-autocomplete-items')]/li");
    descriptionTextArea = by.xpath("//xg-textarea[@attr-name='xgc-program-description' or @attr-name='xgc-description']//textarea");
    addOrbitButton = by.css("xg-button[attr-name*='add-orbits'] button");
    orbitDetailsTableHeaders = by.xpath("//h3[text()='Orbit Details']/../following-sibling::div//xg-grid[@attr-name='xgc-program-grid' or @attr-name='xgc-orbit-programs']//table/thead/tr/th[not(@ng-reflect-field='Delete')]");
    orbitDetailsTableCheckBox = "//h3[text()='Orbit Details']/../following-sibling::div//xg-grid[@attr-name='xgc-program-grid' or @attr-name='xgc-orbit-programs']//table//tr/td[dynamic]/xg-grid-data-cell//mat-checkbox";
    feedOptions = by.xpath("//xg-radio-group[contains(@attr-name,'xgc-radio') or contains(@attr-name,'network')]//label");
    cloneButton = by.css("xg-button[attr-name*='xgc-clone'] button");
    cloneSellingTitleInput = by.css("xg-input[attr-name*='selling-title'] input");
    confirmClonePopupCloneButton = by.xpath("//xg-button[@attr-name='xgc-clone-program-button' or @attr-name='xgc-network-program-clone-confirm']//button[not(@disabled)]");
    confirmClonePopupCancelButton = by.xpath("//xg-button[@attr-name='xgc-cancel-clone-program-button' or @attr-name='xgc-network-program-cancel-confirm']//button");
    cancelButton = by.css("xg-button[attr-name*='cancel'] button");
    networkProgramView = by.css("div[attr-name='xgc-network-program-view']");
    disableSaveButton = by.css("xg-button[attr-name*='save'] button[disabled]");
    enableSaveButton = by.css("xg-button[attr-name*='save'] button:not([disabled])");
    closePopUp = by.css("xg-popup[class*='popup'] div[class*='ui-dialog-titlebar'] a[class*='close']");
    daysOptions = by.xpath("//xg-day-picker[@attr-name='xgc-days-day-picker' or @name='days']//div[@class='xg-dayPicker-items']//xg-checkbox//label");
    dayOptionCheckedStatus = "//xg-day-picker[@attr-name='xgc-days-day-picker' or @name='days']//div[@class='xg-dayPicker-items']//label[normalize-space()='dynamic']/ancestor::xg-checkbox[@ng-reflect-model='true']";
    dayOptionCheckBox = "//xg-day-picker[@attr-name='xgc-days-day-picker' or @name='days']//div[@class='xg-dayPicker-items']//label[normalize-space()='dynamic']/ancestor::xg-checkbox[@ng-reflect-model='false']";
    orbitDetailsTableDaysCheckBoxStatus = "//h3[text()='Orbit Details']/../following-sibling::div//xg-grid[@attr-name='xgc-program-grid' or @attr-name='xgc-orbit-programs']//table//tr/td[dynamic]/xg-grid-data-cell//mat-checkbox[@ng-reflect-checked='true']";
    orbitDetailsTableDaysCheckBox = "//h3[text()='Orbit Details']/../following-sibling::div//xg-grid[@attr-name='xgc-program-grid' or @attr-name='xgc-orbit-programs']//table//tr/td[dynamic]/xg-grid-data-cell//mat-checkbox[@ng-reflect-checked='false']";
    sellingTitleInput = by.xpath("//xg-input[@attr-name='xgc-program-name' or @attr-name='xgc-network-program-selling_title']//input");
    sellingTitleInputError = by.xpath("//xg-input[@attr-name='xgc-program-name' or @attr-name='xgc-network-program-selling_title']//div[contains(@class,'alert-danger')]");
    // editProgramHeader = by.xpath("//xg-popup[@attr-name='xgc-edit-orbit-program' or @attr-name='xgc-edit-local-program' or @attr-name='xgc-edit-network-program']//div[contains(@class,'ui-dialog-titlebar')]");
    searchButton = by.css("xg-button[attr-name*='search'] button");
    editProgramHeader = by.xpath("//xg-popup[contains(@attr-name,'edit-orbit-program') or contains(@attr-name,'edit-network-program') or @attr-name='xgc-edit-local-program']//div[contains(@class,'ui-dialog-titlebar')]");
    addProgramHeader = by.xpath("//xg-popup[contains(@attr-name,'network-program') or contains(@attr-name,'local-program') or contains(@attr-name,'network-orbit-program') or contains(@attr-name,'orbit-program')]//div[contains(@class,'ui-dialog-titlebar')]");

    // startTimeInput = by.xpath("//xg-time-picker[@attr-name='xgc-starttime-time-picker' or @attr-name='xgc-time-start']//input");
    // endTimeInput = by.xpath("//xg-time-picker[@attr-name='xgc-endtime-time-picker' or @attr-name='xgc-time-end']//input");
    startTimeInput = by.xpath("//xg-time-picker[@attr-name='xgc-starttime-time-picker' or @attr-name='xgc-time-start' or contains(@ng-reflect-name,'startTime')]//input")
    endTimeInput = by.xpath("//xg-time-picker[@attr-name='xgc-endtime-time-picker' or @attr-name='xgc-time-end' or contains(@ng-reflect-name,'endTime')]//input");
    //Methods
    async VerifyTextFromElement(ElementName, LabelName: string, data: string) {

        try {

            await browser.sleep(500);
            await action.SwitchToFrame(0);
            await verify.verifyElement(ElementName, LabelName);
            await element(ElementName).getText().then(function (text) {
                console.log(text)
                expect(text).toContain(data)
            })
        }
        catch (err) {
            throw new Error(err);
        }
    }

    /**
     * desc: Used to add local/network/orbit program
     * @method addProgram
     * @author: Venkata Sivakumar
     * @param :{programDetails, programType: string,errorMsg:string=""}
     * @return: none 
     */
    async addProgram(programDetails, programType: string, errorMsg: string = "") {
        try {
            let localProgramTypeStatus: boolean = programType.trim().replace(" ", "").toLowerCase() == "localprogram" || programType.trim().replace(" ", "").toLowerCase() == "local";
            let networkProgramTypeStatus: boolean = programType.trim().replace(" ", "").toLowerCase() == "networkprogram" || programType.trim().replace(" ", "").toLowerCase() == "network";
            let networkOrbitProgramTypeStatus: boolean = programType.trim().replace(" ", "").replace(" ", "").toLowerCase() == "networkorbitprogram" || programType.trim().replace(" ", "").toLowerCase() == "networkorbit";
            let localOrbitProgramTypeStatus: boolean = programType.trim().replace(" ", "").replace(" ", "").toLowerCase() == "localorbitprogram" || programType.trim().replace(" ", "").toLowerCase() == "localorbit";
            if (programDetails.length == 0) { await action.ReportSubStep("addProgram", programDetails + " Empty object has given", "Fail"); }
            else {
                await this.clickonButtonsRatecardPage("Add");
                if (localProgramTypeStatus) { await localProgram.clickDropdownOptionInventory("Local Program"); }
                else if (networkProgramTypeStatus) { await localProgram.clickDropdownOptionInventory("Network Program"); }
                else if (networkOrbitProgramTypeStatus) { await localProgram.clickDropdownOptionInventory("Orbit Program"); }
                else if (localOrbitProgramTypeStatus) { await localProgram.clickDropdownOptionInventory("Orbit Program"); }
                await browser.sleep(2000);
                await verify.verifyElementIsDisplayed(this.addProgramHeader, "Verify header");

                if (localProgramTypeStatus || localOrbitProgramTypeStatus) {
                    if (programDetails["Channels"] != undefined) {
                        await commonLib.selectMultiOptionsFromDropDown("channel", programDetails["Channels"], "dialog");
                    }
                }
                if (networkProgramTypeStatus) { if (programDetails["Network"] != undefined) { await commonLib.selectDropDownValue("Network", programDetails["Network"]); } }
                if (programDetails["Dayparts"] != undefined) { await commonLib.selectMultiOptionsFromDropDown("daypart", programDetails["Dayparts"], "dialog"); }
                if (programDetails["Selling Title"] != undefined) { await this.enterTextInInputInventoryAddProgram("programName", programDetails["Selling Title"]); }
                if (programDetails["Description"] != undefined) { await action.ClearText(this.descriptionTextArea, ""); await action.SetText(this.descriptionTextArea, programDetails["Description"], ""); }
                if (networkProgramTypeStatus || networkOrbitProgramTypeStatus) {
                    await this.selectFeedOption(programDetails);
                    if (programDetails["Base Time Zone"] != undefined) { await commonLib.selectDropDownValue("Base Time Zone", programDetails["Base Time Zone"]); }
                }
                if (localProgramTypeStatus || networkProgramTypeStatus) {
                    await this.selectStartTimeInLocalOrNetworkProgram(programDetails)
                    await this.selectEndTimeInLocalOrNetworkProgram(programDetails);
                    await this.selectDaysInLocalOrNetworkProgram(programDetails);
                }
                else if (localOrbitProgramTypeStatus || networkOrbitProgramTypeStatus) {
                    await this.selectStartTimeInLocalOrNetworkOrbitProgram(programDetails);
                    await this.selectEndTimeInLocalOrNetworkOrbitProgram(programDetails);
                    await this.selectDaysInLocalOrNetworkOrbitProgram(programDetails);
                }
                if (programDetails["FTC"] != undefined) { await action.ClearText(this.ftcInput, ""); await action.SetText(this.ftcInput, programDetails["FTC"], ""); }
                if (programDetails["LTC"] != undefined) { await action.ClearText(this.ltcInput, ""); await action.SetText(this.ltcInput, programDetails["LTC"], ""); }
                await this.selectLockOption(programDetails);
                await this.selectActiveOption(programDetails);
                await this.selectGenreOption(programDetails);
                await this.selectTagOption(programDetails);
                if (programDetails["Hiatus Start Date"] != undefined) { await localProgram.enterDateInInputInventoryAddProgram("hiatus-start", programDetails["Hiatus Start Date"]); }
                if (programDetails["Hiatus End Date"] != undefined) { await localProgram.enterDateInInputInventoryAddProgram("hiatus-end", programDetails["Hiatus End Date"]); }
                if (programDetails["External ID"] != undefined) { await action.SetText(this.externalIdInput, programDetails["External ID"], ""); }
                await this.clickonButtonsRatecardPage("Save");
                await browser.sleep(2000);
                if (errorMsg.trim() == "") {
                    if (networkOrbitProgramTypeStatus || networkProgramTypeStatus) { await localProgram.verifyPopupMessageRatecard("Successfully Added"); }
                    else if (localProgramTypeStatus || localOrbitProgramTypeStatus) { await localProgram.verifyPopupMessageRatecard("Successfully added"); }
                    else { await action.Click(this.closePopUp, "Click On Close popup"); }
                }
                else if (errorMsg.trim() != "") { await localProgram.verifyPopupMessageRatecard(errorMsg.trim()); }
            }
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
       * desc: Used to select start time in local or network program
       * @method selectStartTimeInLocalOrNetworkProgram
       * @author: Venkata Sivakumar
       * @param :{programDetails}
       * @return: none 
       */
    async selectStartTimeInLocalOrNetworkProgram(programDetails) {
        try {
            if (programDetails["Start Time"] != undefined) {
                // await localProgram.clickClockIconTimeOptionInventorydialog("start");
                // await browser.sleep(2000);
                let startTimeValues: Array<any> = programDetails["Start Time"];
                // for (let index = 0; index < startTimeValues.length; index++) {
                //     await localProgram.clickTimeInInventorydialogTimetable(String(startTimeValues[index]));
                //     await browser.sleep(2000);
                // }
                await action.SetText(this.startTimeInput, String(startTimeValues[startTimeValues.length - 1]), "");
            }


        } catch (error) {
            throw new Error(error);
        }
    }
    /**
           * desc: Used to select start time in local or network orbit program
           * @method selectStartTimeInLocalOrNetworkOrbitProgram
           * @author: Venkata Sivakumar
           * @param :{programDetails}
           * @return: none 
           */
    async selectStartTimeInLocalOrNetworkOrbitProgram(programDetails, programType: string = "") {
        try {
            if (programDetails["Start Time"] != undefined) {
                if (programType == "") { await action.Click(this.addOrbitButton, "Click On Orbit button"); }
                // await this.clickClockIconTimeOptionOrbitDetails("start");
                // await browser.sleep(2000);
                let startTimeValues: Array<any> = programDetails["Start Time"];
                await action.SetText(this.startTimeInput, String(startTimeValues[startTimeValues.length - 1]), "");
                // for (let index = 0; index < startTimeValues.length; index++) {
                //     await localProgram.clickTimeInInventorydialogTimetable(String(startTimeValues[index]));
                //     await browser.sleep(2000);
                // }
            }
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
           * desc: Used to select end time in local or network program
           * @method selectEndTimeInLocalOrNetworkProgram
           * @author: Venkata Sivakumar
           * @param :{programDetails}
           * @return: none 
           */
    async selectEndTimeInLocalOrNetworkProgram(programDetails) {
        try {
            if (programDetails["End Time"] != undefined) {
                // await localProgram.clickClockIconTimeOptionInventorydialog("end");
                // await browser.sleep(2000);
                let endTimeValues: Array<any> = programDetails["End Time"];
                await action.SetText(this.endTimeInput, String(endTimeValues[endTimeValues.length - 1]), "");
                // for (let index = 0; index < endTimeValues.length; index++) {
                //     await localProgram.clickTimeInInventorydialogTimetable(String(endTimeValues[index]));
                //     await browser.sleep(2000);
                // }
            }
        } catch (error) {
            throw new Error(error);
        }

    }
    /**
       * desc: Used to select end time in local or network orbit program
       * @method selectEndTimeInLocalOrNetworkOrbitProgram
       * @author: Venkata Sivakumar
       * @param :{programDetails}
       * @return: none 
       */
    async selectEndTimeInLocalOrNetworkOrbitProgram(programDetails) {
        try {
            if (programDetails["End Time"] != undefined) {
                // await this.clickClockIconTimeOptionOrbitDetails("end");
                // await browser.sleep(2000);
                let endTimeValues: Array<any> = programDetails["End Time"];
                await action.SetText(this.endTimeInput, String(endTimeValues[endTimeValues.length - 1]), "");
                // for (let index = 0; index < endTimeValues.length; index++) {
                //     await localProgram.clickTimeInInventorydialogTimetable(String(endTimeValues[index]));
                //     await browser.sleep(2000);
                // }
            }
        } catch (error) {
            throw new Error(error);
        }

    }
    /**
           * desc: Used to select Feed Option in local or network program
           * @method selectFeedOption
           * @author: Venkata Sivakumar
           * @param :{programDetails}
           * @return: none 
           */
    async selectFeedOption(programDetails) {
        try {
            if (programDetails["Feed"] != undefined) {
                let options: Array<WebElement> = await action.getWebElements(this.feedOptions);
                for (let index = 0; index < options.length; index++) {
                    let option: string = await options[index].getText();
                    if (option.trim() == programDetails["Feed"]) {
                        await options[index].click();
                        break;
                    }
                    else if (option.trim() != programDetails["Feed"] && index == (options.length - 1)) {
                        await action.ReportSubStep("addProgram", programDetails["Feed"] + " option is not available under Feed Section", "Fail");
                    }
                }
            }

        } catch (error) {
            throw new Error(error);
        }
    }

    /**
           * desc: Used to select days Option in local or network program
           * @method selectDaysInLocalOrNetworkProgram
           * @author: Venkata Sivakumar
           * @param :{programDetails}
           * @return: none 
           */
    async selectDaysInLocalOrNetworkProgram(programDetails) {
        try {
            if (programDetails["Days"] != undefined) {
                let dayValues: Array<string> = programDetails["Days"];
                let daysLength = dayValues.length;
                for (let index = 0; index < daysLength; index++) {
                    await localProgram.clickDaysOptionInventorydialog(dayValues[index]);
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
       * desc: Used to select days Option in local or network orbit program
       * @method selectDaysInLocalOrNetworkOrbitProgram
       * @author: Venkata Sivakumar
       * @param :{programDetails}
       * @return: none 
       */
    async selectDaysInLocalOrNetworkOrbitProgram(programDetails) {
        try {
            if (programDetails["Days"] != undefined) {
                let dayValues: Array<string> = programDetails["Days"];
                let daysLength = dayValues.length;
                let webElements: Array<WebElement> = await action.getWebElements(this.orbitDetailsTableHeaders);
                for (let index = 0; index < daysLength; index++) {
                    for (let jindex = 0; jindex < webElements.length; jindex++) {
                        let actualText: string = await webElements[jindex].getText();
                        if (actualText.trim() == dayValues[index]) {
                            await action.Click(by.xpath(this.orbitDetailsTableCheckBox.replace("dynamic", String(jindex + 1))), "");
                            break;
                        }
                    }
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
           * desc: Used to select or deslect lock
           * @method selectLockOption
           * @author: Venkata Sivakumar
           * @param :{programDetails}
           * @return: none 
           */
    async selectLockOption(programDetails) {
        try {
            if (programDetails["Lock"] != undefined) {
                if (programDetails["Lock"]) {
                    let elementStatus: boolean = await verify.verifyElementVisible(this.lockCheckBox);
                    let elementStatus1: boolean = await verify.verifyElementVisible(this.lockCheckBoxActive);
                    if (elementStatus) {
                        await action.Click(this.lockCheckBox, "Click On Lock checkbox to lock");
                        await verify.verifyElementIsDisplayed(this.lockCheckBoxActive, "verify lock checkbox is checked");
                    }
                    else if (elementStatus == false && elementStatus1 == false) {
                        await action.ReportSubStep("addProgram", this.lockCheckBox + " " + this.lockCheckBoxActive + " locators are not found", "Fail");
                    }
                }
                else {
                    let elementStatus: boolean = await verify.verifyElementVisible(this.lockCheckBoxActive);
                    let elementStatus1: boolean = await verify.verifyElementVisible(this.lockCheckBox);
                    if (elementStatus) {
                        await action.Click(this.lockCheckBoxActive, "Click On Lock checkbox to unlock");
                        await verify.verifyElementIsDisplayed(this.lockCheckBox, "verify lock checkbox is unchecked");
                    }
                    else if (elementStatus == false && elementStatus1 == false) {
                        await action.ReportSubStep("addProgram", this.lockCheckBox + " " + this.lockCheckBoxActive + " locators are not found", "Fail");
                    }
                }
            }

        } catch (error) {
            throw new Error(error);
        }
    }
    /**
           * desc: Used to select or deslect Active
           * @method selectActiveOption
           * @author: Venkata Sivakumar
           * @param :{programDetails}
           * @return: none 
           */
    async selectActiveOption(programDetails) {
        try {
            if (programDetails["Active"] != undefined) {
                if (programDetails["Active"]) {
                    let elementStatus: boolean = await verify.verifyElementVisible(this.popupActiveCheckBox);
                    let elementStatus1: boolean = await verify.verifyElementVisible(this.popupActiveCheckBoxSelect);
                    if (elementStatus) {
                        await action.Click(this.popupActiveCheckBox, "Click On Active checkbox to active");
                        await verify.verifyElementIsDisplayed(this.popupActiveCheckBoxSelect, "verify lock Active is checked");
                    }
                    else if (elementStatus == false && elementStatus1 == false) {
                        await action.ReportSubStep("addProgram", this.popupActiveCheckBox + " " + this.popupActiveCheckBoxSelect + " locators are not found", "Fail");
                    }
                }
                else {
                    let elementStatus: boolean = await verify.verifyElementVisible(this.popupActiveCheckBoxSelect);
                    let elementStatus1: boolean = await verify.verifyElementVisible(this.popupActiveCheckBox);
                    if (elementStatus) {
                        await action.Click(this.popupActiveCheckBoxSelect, "Click On Active checkbox to inactive");
                        await verify.verifyElementIsDisplayed(this.popupActiveCheckBox, "verify Active checkbox is unchecked");
                    }
                    else if (elementStatus == false && elementStatus1 == false) {
                        await action.ReportSubStep("addProgram", this.popupActiveCheckBox + " " + this.popupActiveCheckBoxSelect + " locators are not found", "Fail");
                    }
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
           * desc: Used to select Genre option
           * @method selectGenreOption
           * @author: Venkata Sivakumar
           * @param :{programDetails}
           * @return: none 
           */
    async selectGenreOption(programDetails) {
        try {
            if (programDetails["Genre"] != undefined) {
                await action.SetText(this.genreInput, programDetails["Genre"], "");
                await verify.verifyElementIsDisplayed(this.genreSuggestionListItems, "Verify suggestion list items displayed");
                let genreWebElements: Array<WebElement> = await action.getWebElements(this.genreSuggestionListItems);
                for (let index = 0; index < genreWebElements.length; index++) {
                    let actualText: string = await genreWebElements[index].getText();
                    if (actualText.trim() == programDetails["Genre"] || actualText.trim().includes(programDetails["Genre"])) {
                        await genreWebElements[index].click();
                        break;
                    }
                    else if (actualText.trim() != programDetails["Genre"] || !actualText.trim().includes(programDetails["Genre"]) && index == genreWebElements.length - 1) {
                        await action.ReportSubStep("addProgram", programDetails["Genre"] + " suggestion item is not displayed in Genre", "Fail");
                    }
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
         * desc: Used to select Tag option
         * @method selectTagOption
         * @author: Venkata Sivakumar
         * @param :{programDetails}
         * @return: none 
         */
    async selectTagOption(programDetails) {
        try {
            if (programDetails["Tag"] != undefined) {
                await action.SetText(this.tagInput, programDetails["Tag"], "");
                await verify.verifyElementIsDisplayed(this.tagSuggestionListItems, "Verify suggestion list items displayed");
                let genreWebElements: Array<WebElement> = await action.getWebElements(this.tagSuggestionListItems);
                for (let index = 0; index < genreWebElements.length; index++) {
                    let actualText: string = await genreWebElements[index].getText();
                    if (actualText.trim() == programDetails["Tag"] || actualText.trim().includes(programDetails["Tag"])) {
                        await genreWebElements[index].click();
                        break;
                    }
                    else if (actualText.trim() != programDetails["Tag"] || !actualText.trim().includes(programDetails["Tag"]) && index == genreWebElements.length - 1) {
                        await action.ReportSubStep("addProgram", programDetails["Tag"] + " suggestion item is not displayed in Tag", "Fail");
                    }
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
* desc: click Clock Icon Time Option Inventory dialog
* @method clickClockIconTimeOptionInventorydialog
* @author: vikas
* @param :{}
* @return none
*/
    async clickClockIconTimeOptionOrbitDetails(labelName: string) {

        try {
            let locator = by.css('xg-time-picker[ng-reflect-name*="' + labelName + '"] i[class*="fa fa-clock"]');
            await action.Click(locator, labelName + " clock icon");
        }
        catch (err) {
            throw new Error(err);
        }
    }

    /**
  * desc: click buttons in Ratecard Page
  * @method clickonButtonsRatecardPage
  * @author: vikas
  * @param :{}
  * @return none
  */
    async clickonButtonsRatecardPage(buttonName: string) {

        try {
            let Xpbutton = by.xpath('//button[normalize-space(text())="' + buttonName + '" and not(@disabled)]');
            await action.MouseMoveToElement(Xpbutton, buttonName)
            await action.Click(Xpbutton, buttonName);
        }
        catch (err) {
            throw new Error(err);
        }
    }

    /**
 * desc: enter text in Input Inventory Add Program
 * @method enterTextInInputInventoryAddProgram
 * @author: vikas
 * @param :{}
 * @return none
 */
    async enterTextInInputInventoryAddProgram(name, textToenter) {
        try {
            let locator = by.css('xg-input[name*="' + name + '"] input');
            await action.MouseMoveToElement(locator, name);
            await browser.sleep(2000);
            await action.ClearText(locator, "clear text");
            await action.SetText(locator, textToenter, name);
        }
        catch (err) {
            throw new Error(err);
        }
    }

    /**
* desc: used to clone a local/network/orbit program
* @method cloneProgram
* @author: Venkata Sivakumar
* @param :{cloneProgramDetails}
* @return none
*/
    async cloneProgram(cloneProgramDetails, programType: string) {
        try {
            if (cloneProgramDetails.length == 0) {
                await action.ReportSubStep("cloneProgram", "Empty details object has given", "Fail");
            }
            else {
                let localProgramStatus = programType.toLowerCase().trim().replace(" ", "") == "localprogram" || programType.toLowerCase().trim().replace(" ", "") == "local" || programType.toLowerCase().trim().replace(" ", "").replace(" ", "") == "localorbitprogram" || programType.toLowerCase().trim().replace(" ", "") == "localorbit";
                let networkProgramStatus = programType.toLowerCase().trim() == "networkprogram" || programType.toLowerCase().trim() == "network";
                if (cloneProgramDetails["Existing Program"] != undefined) {
                    await action.ClearText(commonLib.globalFilter, "Clear filter");
                    await action.SetText(commonLib.globalFilter, cloneProgramDetails["Existing Program"], "Filter");
                    await verify.verifyProgressBarNotPresent();
                    await browser.sleep(2000);
                    let elementStatus: boolean = await verify.verifyElementVisible(commonLib.noDataMatchFound);
                    if (elementStatus) {
                        await action.ReportSubStep("cloneProgram", cloneProgramDetails["Existing Program"] + " program is not available", "Fail");
                    }
                    await action.Click(commonLib.gridHeaderCheckBox, "");
                    await verify.verifyElementIsDisplayed(commonLib.gridHeaderChecBoxChecked, "");
                    if (cloneProgramDetails["Clone Program"] != undefined) {
                        await action.Click(this.cloneButton, "");
                        await action.SetText(this.cloneSellingTitleInput, cloneProgramDetails["Clone Program"], "");
                        await action.Click(this.confirmClonePopupCloneButton, "");
                        await browser.sleep(1000);
                        if (localProgramStatus) { await localProgram.verifyPopupMessageRatecard("Successfully cloned local programs"); }
                        else if (networkProgramStatus) { await localProgram.verifyPopupMessageRatecard("Successfully cloned network program(s)"); }
                    }
                }
                else {
                    await action.ReportSubStep("cloneProgram", "Existing Program key not exist in provided cloneProgramDetails object", "Fail");
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
       * desc: Used to delete Local Or network programs
       * @method deletePrograms
       * @author: Venkata Sivakumar
       * @param :{sellingTitles:Array<any>,programType:string}
       * @return:none 
       */
    async deletePrograms(sellingTitles: Array<any>, programType: string) {
        try {
            let elementStatus: boolean;
            let failedCount = 0;
            let errorMessage: string = "";
            let localProgramStatus = programType.toLowerCase().trim().replace(" ", "") == "localprogram" || programType.toLowerCase().trim().replace(" ", "") == "local" || programType.toLowerCase().trim().replace(" ", "").replace(" ", "") == "localorbitprogram" || programType.toLowerCase().trim().replace(" ", "") == "localorbit";
            let networkProgramStatus = programType.toLowerCase().trim() == "networkprogram" || programType.toLowerCase().trim() == "network" || programType.toLowerCase().trim() == "networkorbitprogram" || programType.toLowerCase().trim() == "networkorbit";
            elementStatus = await verify.verifyElementVisible(commonLib.gridHeaderChecBoxChecked);
            if (elementStatus) {
                await action.Click(commonLib.gridHeaderChecBoxChecked, "Click On CheckBox to uncheck");
                await verify.verifyElementIsDisplayed(commonLib.gridHeaderCheckBox, "Verify chekbox is unchecked");
            }
            for (let index = 0; index < sellingTitles.length; index++) {
                await action.ClearText(commonLib.globalFilter, "Clear the filter");
                await action.SetText(commonLib.globalFilter, sellingTitles[index], "Perform filter");
                await verify.verifyProgressBarNotPresent();
                await browser.sleep(3000);
                elementStatus = await verify.verifyElementVisible(commonLib.noDataMatchFound);
                if (elementStatus) {
                    failedCount = failedCount + 1;
                    errorMessage = errorMessage + sellingTitles[index] + " ";
                }
                else {
                    await action.Click(commonLib.gridHeaderCheckBox, "Click On CheckBox");
                    await verify.verifyElementIsDisplayed(commonLib.gridHeaderChecBoxChecked, "Verify chekbox is checked");
                    await action.Click(this.deleteButton, "Click On Delete Button");
                    await verify.verifyElementIsDisplayed(this.confirmDeletePopupDeleteButton, "Verify Delete button is displayed");
                    await action.Click(this.confirmDeletePopupDeleteButton, "Click On Delete Button");
                    await browser.sleep(1000);
                    if (localProgramStatus) {
                        await commonLib.verifyPopupMessage("Successfully deleted local program(s)");
                    }
                    else if (networkProgramStatus) {
                        await commonLib.verifyPopupMessage("Successfully deleted network program(s)");
                    }
                }
            }
            if (failedCount > 0) {
                await action.ReportSubStep("deletePrograms", +errorMessage + " is/are not available in the gird to delete the program", "Fail");
            }
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
       * desc: Used to unselect days Option in local or network program
       * @method unSelectDaysInLocalOrNetworkProgram
       * @author: Venkata Sivakumar
       * @param :{programDetails}
       * @return: none 
       */
    async unSelectDaysInLocalOrNetworkProgram(programDetails) {
        try {
            if (programDetails["Days"] != undefined) {
                let dayValues: Array<string> = programDetails["Days"];
                let daysLength = dayValues.length;
                let elements: Array<WebElement> = await action.getWebElements(this.daysOptions);
                let count = await element.length;
                for (let index = 0; index < daysLength; index++) {
                    for (let jIndex = 0; jIndex < count; jIndex++) {
                        if (String(await elements[jIndex].getText()).trim() == dayValues[index]) {
                            let status: boolean = await verify.verifyElementVisible(by.xpath(this.dayOptionCheckedStatus.replace("dynamic", String(dayValues[index]))));
                            if (status) {
                                await elements[jIndex].click();
                                await verify.verifyElementIsDisplayed(by.xpath(this.dayOptionCheckBox.replace("dynamic", String(dayValues[index]))), "Verify " + dayValues[index] + " option is unchecked");
                            }
                        }
                    }
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
       * desc: Used to unselect days Option in local or network orbit program
       * @method unSelectDaysInLocalOrNetworkOrbitProgram
       * @author: Venkata Sivakumar
       * @param :{programDetails}
       * @return: none 
       */
    async unSelectDaysInLocalOrNetworkOrbitProgram(programDetails) {
        try {
            if (programDetails["Days"] != undefined) {
                let dayValues: Array<string> = programDetails["Days"];
                let daysLength = dayValues.length;
                let webElements: Array<WebElement> = await action.getWebElements(this.orbitDetailsTableHeaders);
                for (let index = 0; index < daysLength; index++) {
                    for (let jindex = 0; jindex < webElements.length; jindex++) {
                        let actualText: string = await webElements[jindex].getText();
                        if (actualText.trim() == dayValues[index]) {
                            let status: boolean = await verify.verifyElementVisible(by.xpath(this.orbitDetailsTableDaysCheckBoxStatus.replace("dynamic", String(jindex + 1))));
                            if (status) {
                                await action.Click(by.xpath(this.orbitDetailsTableCheckBox.replace("dynamic", String(jindex + 1))), "");
                                await verify.verifyElementIsDisplayed(by.xpath(this.orbitDetailsTableDaysCheckBox.replace("dynamic", String(jindex + 1))), "Verify " + dayValues[index] + " check box is unchecked");
                                break;
                            }
                        }
                    }
                }
            }
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
* desc: Used to edit local/network/orbit program
* @method editProgram
* @author: Venkata Sivakumar
* @param :{programDetails, programType: string,errorMsg:string=""}
* @return: none 
*/
    async editProgram(sellingTitle: string, programDetails, programType: string, errorMsg: string = "") {
        try {
            let daysDetails = { "Days": ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"] };
            let localProgramTypeStatus: boolean = programType.trim().replace(" ", "").toLowerCase() == "localprogram" || programType.trim().replace(" ", "").toLowerCase() == "local";
            let networkProgramTypeStatus: boolean = programType.trim().replace(" ", "").toLowerCase() == "networkprogram" || programType.trim().replace(" ", "").toLowerCase() == "network";
            let networkOrbitProgramTypeStatus: boolean = programType.trim().replace(" ", "").replace(" ", "").toLowerCase() == "networkorbitprogram" || programType.trim().replace(" ", "").toLowerCase() == "networkorbit";
            let localOrbitProgramTypeStatus: boolean = programType.trim().replace(" ", "").replace(" ", "").toLowerCase() == "localorbitprogram" || programType.trim().replace(" ", "").toLowerCase() == "localorbit";
            if (programDetails.length == 0) { await action.ReportSubStep("editProgram", programDetails + " Empty object has given", "Fail"); }
            else {
                // await browser.sleep(5000);
                await action.ClearText(commonLib.globalFilter, "");
                await action.SetText(commonLib.globalFilter, sellingTitle.trim(), "");
                await browser.sleep(2000);
                await verify.verifyProgressBarNotPresent();

                let status: boolean = await verify.verifyElementVisible(commonLib.noDataMatchFound);
                if (status) { await action.ReportSubStep("editProgram", sellingTitle.trim() + " record is not avaialble in the Grid.No data match is found is displayed", "Fail"); }
                let columnIndex = await commonLib.getTableColumnIndex("Selling Title");
                await action.Click(by.xpath(commonLib.gridCellData.replace("rowdynamic", "1").replace("columndynamic", String(columnIndex))), "Click Or record");
                await verify.verifyElementIsDisplayed(this.editProgramHeader, "");

                if (programDetails["Dayparts"] != undefined) {
                    await commonLib.deSelectAllMultiDropdownOptions("daypart", "dialog");
                    await commonLib.selectMultiOptionsFromDropDown("daypart", programDetails["Dayparts"], "dialog");
                }
                if (programDetails["Selling Title"] != undefined) { await this.enterTextInInputInventoryAddProgram("programName", programDetails["Selling Title"]); }
                if (programDetails["Description"] != undefined) { await action.ClearText(this.descriptionTextArea, ""); await action.SetText(this.descriptionTextArea, programDetails["Description"], ""); }
                if (networkProgramTypeStatus || networkOrbitProgramTypeStatus) {
                    await this.selectFeedOption(programDetails);
                    if (programDetails["Base Time Zone"] != undefined) { await commonLib.selectDropDownValue("Base Time Zone", programDetails["Base Time Zone"]); }
                }
                if (localProgramTypeStatus || networkProgramTypeStatus) {
                    await this.selectStartTimeInLocalOrNetworkProgram(programDetails)
                    await this.selectEndTimeInLocalOrNetworkProgram(programDetails);
                    if (programDetails["Days"] != undefined) {
                        await browser.sleep(1000);
                        await this.unSelectDaysInLocalOrNetworkProgram(daysDetails);
                    }
                    await this.selectDaysInLocalOrNetworkProgram(programDetails);
                }
                else if (localOrbitProgramTypeStatus || networkOrbitProgramTypeStatus) {
                    await this.selectStartTimeInLocalOrNetworkOrbitProgram(programDetails, "edit");
                    await this.selectEndTimeInLocalOrNetworkOrbitProgram(programDetails);
                    if (programDetails["Days"] != undefined) {
                        await browser.sleep(1000);
                        await this.unSelectDaysInLocalOrNetworkOrbitProgram(daysDetails);
                    }
                    await this.selectDaysInLocalOrNetworkOrbitProgram(programDetails);
                }
                if (programDetails["FTC"] != undefined) { await browser.sleep(2000); await action.ClearText(this.ftcInput, ""); await browser.sleep(2000); await action.SetText(this.ftcInput, programDetails["FTC"], ""); }
                if (programDetails["LTC"] != undefined) { await browser.sleep(2000); await action.ClearText(this.ltcInput, ""); await browser.sleep(2000); await action.SetText(this.ltcInput, programDetails["LTC"], ""); }
                await this.selectLockOption(programDetails);
                await this.selectActiveOption(programDetails);
                await this.selectGenreOption(programDetails);
                await this.selectTagOption(programDetails);
                if (programDetails["Hiatus Start Date"] != undefined) { await localProgram.enterDateInInputInventoryAddProgram("hiatus-start", programDetails["Hiatus Start Date"]); }
                if (programDetails["Hiatus End Date"] != undefined) { await localProgram.enterDateInInputInventoryAddProgram("hiatus-end", programDetails["Hiatus End Date"]); }
                if (programDetails["External ID"] != undefined) { await browser.sleep(2000); await action.ClearText(this.externalIdInput, ""); await browser.sleep(2000); await action.SetText(this.externalIdInput, programDetails["External ID"], ""); }
                await this.clickonButtonsRatecardPage("Update");
                await browser.sleep(2000);
                if (errorMsg.trim() == "") {
                    if (networkOrbitProgramTypeStatus || networkProgramTypeStatus) { await localProgram.verifyPopupMessageRatecard("Successfully updated"); }
                    else if (localProgramTypeStatus || localOrbitProgramTypeStatus) { await localProgram.verifyPopupMessageRatecard("Successfully updated"); }
                }
                else if (errorMsg.trim() != "") { await localProgram.verifyPopupMessageRatecard(errorMsg.trim()); }
            }
        } catch (error) {
            throw new Error(error);
        }
    }
}