import { browser, element, by, protractor, promise } from 'protractor';
import { ActionLib } from '../../Libs/GeneralLibs/ActionLib';
import { VerifyLib } from '../../Libs/GeneralLibs/VerificationLib';
import { Gutils } from '../../Utility/gutils';

let action = new ActionLib();
let verify = new VerifyLib();
let elementStatus:boolean;

export class SettingsPage {

    //Locators
    linkSettingsExpanded = by.xpath("//ul[@class='navigation-list']//div[contains(@class,'expanded')]//span[text()='Settings']");
    linkSettings = by.xpath("//ul[@class='navigation-list']//div[not(contains(@class,'expanded'))]//span[text()='Settings']");
    linkDaypartDemo = by.xpath("//ul[@class='sub-list']//div[not(contains(@class,'expanded'))]//span[text()='Daypart Demo']");
    linkDaypartDemoExpanded = by.xpath("//ul[@class='sub-list']//div[contains(@class,'expanded')]//span[text()='Daypart Demo']");
    settingsSubMenuLink = "//ul[@class='navigation-list']//div[contains(@class,'expanded')]//span[text()='Settings']/../following-sibling::div/ul[@class='sub-list']//div[not(contains(@class,'expanded'))]//span[text()='dynamic']";
    settingsSubMenuLinkExpanded = "//ul[@class='navigation-list']//div[contains(@class,'expanded')]//span[text()='Settings']/../following-sibling::div/ul[@class='sub-list']//div[contains(@class,'expanded')]//span[text()='dynamic']";
    dayPartDemoSubMenuLink = "//ul[@class='navigation-list']//div[contains(@class,'expanded')]//span[text()='Settings']/../following-sibling::div/ul[@class='sub-list']//div[contains(@class,'expanded')]//span[text()='Daypart Demo']/../following-sibling::div/ul[@class='sub-list']//span[text()='dynamic']";
    
    //Methods
    async clickOnSettings() {
        try {
            elementStatus = await verify.verifyElementVisible(this.linkSettingsExpanded);
            if(!elementStatus)
            {
                await action.Click(this.linkSettings,"Click On Settings link");
                await verify.verifyElementIsDisplayed(this.linkSettingsExpanded,"Verify Settings menu is expanded");
            }
       } catch (error) {
                throw new Error(error);
        }
    }

    async clickOnSettingsSubMenuLink(subMenuLinkName:string)
    {
        try {
            elementStatus = await verify.verifyElementVisible(by.xpath(this.settingsSubMenuLinkExpanded.replace("dynamic",subMenuLinkName)));
            if(!elementStatus)
            {
                await action.Click(by.xpath(this.settingsSubMenuLink.replace("dynamic",subMenuLinkName)),"Click On "+subMenuLinkName);
                await verify.verifyElementIsDisplayed(by.xpath(this.settingsSubMenuLinkExpanded.replace("dynamic",subMenuLinkName)),"Verify "+subMenuLinkName+" is expanded");
            }

        } catch (error) {
            throw new Error(error);
        }
    }

    async clickOnDayPartDemoSubMenuLink(subMenuLinkName:string)
    {
        try {
            await action.Click(by.xpath(this.dayPartDemoSubMenuLink.replace("dynamic",subMenuLinkName)),"Click On '"+subMenuLinkName+"' sub link");
        } catch (error) {
            throw new Error(error);
        }
    }
}    