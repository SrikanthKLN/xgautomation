import { by } from "protractor";

export class NetworkTagsPage{
    //Locators
    networkMultiSelectDropDown = by.css("xg-multi-select[name='network'] div[class*='multiselect-trigger']");
    nameInPopUp = by.css("div[role='dialog'] xg-input[name='name'] input");
    descriptionInPopUp = by.css("div[role='dialog'] xg-input[name='desc'] input");
    activeToggleInPopUp = by.xpath("//div[@role='dialog']//xg-input-switch[@ng-reflect-model='true']//span[contains(text(),'Active/Inactive')]/preceding-sibling::div/div");
    inActiveToggleInPopUp = by.xpath("//div[@role='dialog']//xg-input-switch[@ng-reflect-model='false']//span[contains(text(),'Active/Inactive')]/preceding-sibling::div/div");
    inventoryCheckBoxActive = by.css("div[role='dialog'] xg-checkbox[name='inventory'][ng-reflect-model='true'] label");
    inventoryCheckBox = by.css("div[role='dialog'] xg-checkbox[name='inventory'][ng-reflect-model='false'] label");
    campaignCheckBoxActive = by.css("div[role='dialog'] xg-checkbox[name='campaign'][ng-reflect-model='true'] label");
    campaignCheckBox = by.css("div[role='dialog'] xg-checkbox[name='campaign'][ng-reflect-model='false'] label");
    dayPartsDropDownInPopUp = by.css("div[role='dialog'] xg-multi-select[name='ddlInv'] div[class*='multiselect-trigger']");
    createButtonInPopUp = by.css("div[class*='popup'] xg-button button[title='Create']:not([disabled])");
    cancelButtonInPopUp = by.css("div[class*='popup'] xg-button button[title='Cancel']:not([disabled])");
    networkOnlyActiveToggleInPopUp = by.xpath("//div[@role='dialog']//xg-input-switch[@ng-reflect-model='true']//span[contains(text(),'Network Only')]/preceding-sibling::div/div")
    networkOnlyInActiveToggleInPopUp = by.xpath("//div[@role='dialog']//xg-input-switch[@ng-reflect-model='false']//span[contains(text(),'Network Only')]/preceding-sibling::div/div")
    networksMultiSelectDropDownInPopUp = by.css("div[role='dialog'] xg-multi-select[name='ddlNetwork'] div[class*='multiselect-trigger']");
}