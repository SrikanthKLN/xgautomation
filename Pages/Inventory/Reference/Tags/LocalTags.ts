import { by } from "protractor";

export class LocalTagsPage
{
    //Locators

    marketDrodpown = by.css("xg-dropdown[name='market'] div[class*='dropdown-trigger']");
    channelDropDown = by.css("xg-multi-select[name='channel'] div[class*='multiselect-trigger']");
    addButton = by.css("xg-button[attr-name='xgc-tag-view'] button");
    cloneButton = by.css("xg-button button[title='Clone']");
    deleteButton = by.css("xg-button button[title='Delete']");
    addTagHeader = by.xpath("//div[@role='dialog']//span[normalize-space()='Add Tag']");
    channelDropDownInPopup = by.css("div[role='dialog'] xg-multi-select[name='channel'] div[class*='multiselect-trigger']");
    nameInPopUp = by.css("div[role='dialog'] xg-input[name='name'] input");
    descriptionInPopUp = by.css("div[role='dialog'] xg-input[name='desc'] input");
    activeToggleInPopUp = by.css("div[role='dialog'] xg-input-switch[ng-reflect-model='true'] input[id*='mat-slide-toggle']+div");
    inActiveToggleInPopUp = by.css("div[role='dialog'] xg-input-switch[ng-reflect-model='false'] input[id*='mat-slide-toggle']+div");
    inventoryCheckBoxActive = by.css("div[role='dialog'] xg-checkbox[name='inventory'][ng-reflect-model='true'] label");
    inventoryCheckBox = by.css("div[role='dialog'] xg-checkbox[name='inventory'][ng-reflect-model='false'] label");
    campaignCheckBoxActive = by.css("div[role='dialog'] xg-checkbox[name='campaign'][ng-reflect-model='true'] label");
    campaignCheckBox = by.css("div[role='dialog'] xg-checkbox[name='campaign'][ng-reflect-model='false'] label");
    dayPartsDropDownInPopUp = by.css("div[role='dialog'] xg-multi-select[name='ddlInv'] div[class*='multiselect-trigger']");
    createButtonInPopUp = by.css("div[class*='popup'] xg-button button[title='Create']:not([disabled])");
    cancelButtonInPopUp = by.css("div[class*='popup'] xg-button button[title='Cancel']:not([disabled])");
}