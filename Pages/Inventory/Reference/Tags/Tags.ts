import { browser, element, by, protractor, promise } from 'protractor';
import { Gutils } from '../../../../Utility/gutils';
import { ActionLib } from '../../../../Libs/GeneralLibs/ActionLib';
import { VerifyLib } from '../../../../Libs/GeneralLibs/VerificationLib';
import { LocalLengthFactorsPricing } from '../../../Pricing/Reference/LocalLengthFactors';
import { globalTestData } from '../../../../TestData/globalTestData';


let gUtils = new Gutils();
let action = new ActionLib();
let verify = new VerifyLib();
let localLF = new LocalLengthFactorsPricing()

export class Tags {

    //Variables Declaration
    tagAddedNotificationMsg = "Successfully Tag added"
    tagUpdatedNotificationMsg = "Successfully updated Tag"
    tagClonedNotificationMsg = "Successfully cloned "
    tagExistsNotificationMsg = "ERROR: Tag with name dynamic already exists"
    cloneSelectOneErrorMsg = "Please select a Tag for clone"
    tagStausNotificationMsg = "Tag status Updated successfully."
    deleteConfirmationText = "Are you sure you want to delete this Tag ?"
    deleteInvCampTagNotificationMsg = "Total 1 record(s) deleted successfully for both"
    tagDeletedNotificationMsg = "record(s) deleted successfully"
    deleteInvTagNotificationMsg = "Total 1 record(s) deleted successfully for Inventory"

    addTagPopUpheaderText = "Add Tag"
    editTagPopUpheaderText = "Edit Tag"
    cloneTagPopUpheaderText = "Clone Tag"
    nameColumnHeaderText = "Name"
    parentColumnHeaderText = "Parent"
    dayPartsColumnHeaderText = "Dayparts"
    campaignsColumnHeaderText = "Campaigns"
    parentType_Campaign = "Inventory, Campaign"
    parentType_Inventory = "Inventory"
    toggleAttribute = "ng-reflect-model"

    // Locators Declaration
    name_TextBox = by.css('xg-input[name="name"] input[class*="xg-text-box"]');
    description_TextBox = by.css('xg-input[name="desc"] input[class*="xg-text-box"]');
    dayPartsMultiSelectDropDown = by.css('xg-multi-select[name*="ddlInv"] label[class*="ui-multiselect-label"]')
    byMultiSelectDropDown = by.css('xg-multi-select[name*="ddlSal"] label[class*="ui-multiselect-label"]')
    networkMultiSelectDropDown = by.css('xg-multi-select[ng-reflect-label*="Network"] label[class*="ui-multiselect-label"]')
    channelMultiSelectDropDown = by.css('xg-multi-select[name*="channel"] label[class*="ui-multiselect-label"]')
    cancel_Button = by.css('xg-button button[class*="xg-button"][title="Cancel"]')
    create_Button = by.css('xg-button button[class*="xg-button"][title="Create"]')
    update_Button = by.css('xg-button button[class*="xg-button"][title="Update"]')
    clone_Button = by.css('xg-button button[class*="xg-button"][title="Clone"]')
    add_Button = by.css('xg-button button[class*="xg-button"][title="Add"]')
    delete_Button = by.css('xg-button button[class*="xg-button"][title="Delete"]')
    toggleSwitch = by.css('xg-grid xg-grid-data-cell mat-slide-toggle')
    activeInactiveToggle = by.css('xg-input-switch[name="state"] mat-slide-toggle')
    networkMultiSelectDropdownPopUp = by.css('xg-popup xg-multi-select[ng-reflect-label*="Network"] label[class*="ui-multiselect-label"]')

    //clone
    clonePopUp = by.css('xg-popup[ng-reflect-header*="Clone Tag"] div[role="dialog"]')
    deleteDialogBox = by.css('div[class*="ui-dialog ui-confirmdialog"]')
    tagTilteClone_TextBox = by.css('xg-input[name="tagTitle"] input[class*="xg-text-box"]')

    latestTagName = by.css('xg-grid tr:nth-child(1) td:nth-child(3)')
    secondTagName = by.css('xg-grid tr:nth-child(2) td:nth-child(3)')
    latestDayParts = by.css('xg-grid tr:nth-child(1) td:nth-child(3)')
    latestDescription = by.css('xg-grid tr:nth-child(1) td:nth-child(5)')

    //Delete
    deleteConfirmationMsg = by.css('[class="ui-confirmdialog-message"]')
    yes_Button = by.buttonText("Yes")
    no_Button = by.buttonText("No")


    /**
   * desc: To Filter grid values in the view 
   * @method gridWithStatusColumnFilter
   * @author: Adithya
   * @param :{ColumnName:Column Name which is to be Validated,FilterData:Testdata to be Filtered}
   * @return none
   */
    async returnGridColumnIndex(ColumnName: string) {
        try {
            await browser.waitForAngular();
            let GridColumnIndex
            let gridColumnHeaderLocator = by.css('xg-grid th[style*="display: table-cell"]')
            let elementCount = await element.all(gridColumnHeaderLocator).count()
            let headerElements = await element.all(gridColumnHeaderLocator).getWebElements()
            let headerName;

            for (let index = 0; index < elementCount; index++) {
                headerName = await headerElements[index].getText();
                if (headerName.trim() == ColumnName.trim()) {
                    GridColumnIndex = index;
                    action.ReportSubStep("returnGridColumnIndex", "returned Grid Column Index Successfully", "Pass");
                    return GridColumnIndex;
                }
                else if (headerName.trim() != ColumnName.trim() && index == (elementCount - 1)) {
                    action.ReportSubStep("returnGridColumnIndex", "Column Name not available in Grid", "Fail");
                }
            }

        }
        catch (err) {
            throw new Error('Error');
        }
    }

    /**
      * desc: To Filter grid values in the view 
      * @method gridWithStatusColumnFilter
      * @author: Adithya
      * @param :{ColumnName:Column Name which is to be Validated,FilterData:Testdata to be Filtered}
      * @return none
      */
    async gridWithStatusColumnFilterWithColumnIndex(GridColumnIndex: number, FilterData: string, log: string) {
        try {
            await browser.waitForAngular();
            let gridColumnFilterLocator = by.css('xg-grid table input[class*="ui-inputtext"]')

            let ColumnFilter = element.all(gridColumnFilterLocator).get(GridColumnIndex)
            await action.ClearText(ColumnFilter, log)
            for (let i = 0; i < FilterData.length; i++) {
                await browser.sleep(100);
                await action.SetText(ColumnFilter, FilterData.charAt(i), log)
                await browser.sleep(100);
            }
            action.ReportSubStep("gridWithStatusColumnFilter", "Text Entered Successfully", "Pass");
        }
        catch (err) {
            action.ReportSubStep("gridWithStatusColumnFilter", "Text Not Entered", "Fail");
            throw new Error('Error');
        }
    }

    /**
   * desc: To Filter grid values in the view 
   * @method gridWithStatusColumnFilter
   * @author: Adithya
   * @param :{ColumnName:Column Name which is to be Validated,FilterData:Testdata to be Filtered}
   * @return none
   */
    async gridWithStatusColumnFilter(ColumnName: string, FilterData: string) {
        try {
            await browser.waitForAngular();
            let gridWithStatusColumnFilterIndex
            let gridColumnHeaderLocator = by.css('xg-grid th[class*="column"]')
            let gridColumnFilterLocator = by.css('xg-grid table input[class*="ui-inputtext"]')
            let elementCount = await element.all(gridColumnHeaderLocator).count()
            let headerElements = await element.all(gridColumnHeaderLocator).getWebElements()
            let headerName;

            for (let index = 0; index < elementCount; index++) {
                headerName = await headerElements[index].getText();
                if (headerName.trim() == ColumnName.trim()) {
                    gridWithStatusColumnFilterIndex = index - 2;
                    break;
                }
                else if (headerName.trim() != ColumnName.trim() && index == (elementCount - 1)) {
                    action.ReportSubStep("gridWithStatusColumnFilter", "Column Name not available in Grid", "Fail");
                }
            }
            let ColumnFilter = element.all(gridColumnFilterLocator).get(gridWithStatusColumnFilterIndex)
            await action.ClearText(ColumnFilter, headerName + " Column text Filter")
            for (let i = 0; i < FilterData.length; i++) {
                await browser.sleep(100);
                await action.SetText(ColumnFilter, FilterData.charAt(i), headerName + " Column text Filter")
                await browser.sleep(100);
            }
            action.ReportSubStep("gridWithStatusColumnFilter", "Text Entered Successfully", "Pass");
        }
        catch (err) {
            action.ReportSubStep("gridWithStatusColumnFilter", "Text Not Entered", "Fail");
            throw new Error('Error');
        }
    }

    /** 
   * desc: To Validate grid values in the view 
   * @method gridWithStatusColumnDataValidation
   * @author: Adithya 
   * @param :{ColumnName:Column Name which is to be Validated,data:Testdata to be Entered,equalContains:equal or Contains}
   * @return none
   */
    async gridWithStatusColumnDataValidation(ColumnName: string, data: string, equalContains: string) {
        try {
            await browser.waitForAngular();
            let gridColumnIndex;
            let gridColumnHeaderLocator = by.css('xg-grid th[class*="column"]')
            let elementCount = await element.all(gridColumnHeaderLocator).count()
            let headerElements = await element.all(gridColumnHeaderLocator).getWebElements()
            let headerName;

            for (let index = 0; index < elementCount; index++) {
                headerName = await headerElements[index].getText();
                if (headerName.trim() == ColumnName.trim()) {
                    gridColumnIndex = index + 1;
                    break;
                }
                else if (headerName.trim() != ColumnName.trim() && index == (elementCount - 1)) {
                    action.ReportSubStep("gridWithStatusColumnDataValidation", "Column Name not available in Grid", "Fail");
                }
            }
            await console.log("index of column is " + gridColumnIndex, "Column no is " + gridColumnIndex, "Name of column is " + headerName)
            await verify.verifyElement(by.css('xg-grid tr td:nth-child(' + gridColumnIndex + ')'), headerName)
            await action.MouseMoveToElement(by.css('xg-grid tr td:nth-child(' + gridColumnIndex + ')'), headerName)
            let gridDataCount = await element.all(by.css('xg-grid tr td:nth-child(' + gridColumnIndex + ')')).count()
            let gridDataElements = await element.all(by.css('xg-grid tr td:nth-child(' + gridColumnIndex + ')')).getWebElements()
            for (let colIndex = 0; colIndex < gridDataCount; colIndex++) {
                let text = await gridDataElements[colIndex].getText()
                if (equalContains.toUpperCase() == "EQUAL") {
                    if (data.trim() == text.trim()) {
                        action.ReportSubStep("gridWithStatusColumnDataValidation", "Data available " + text, "Pass");
                    }
                    else {
                        action.ReportSubStep("gridWithStatusColumnDataValidation", "Data Not available " + text, "Fail");
                    }
                }
                else {
                    if (data.trim().includes(text.trim()) || text.trim().includes(data.trim())) {
                        action.ReportSubStep("gridWithStatusColumnDataValidation", "Data available " + text, "Pass");
                    }
                    else {
                        action.ReportSubStep("gridWithStatusColumnDataValidation", "Data Not available " + text, "Fail");
                    }
                }
            }

            action.ReportSubStep("gridWithStatusColumnDataValidation", "Data available", "Pass");
        }
        catch (err) {
            action.ReportSubStep("gridWithStatusColumnDataValidation", "Data Not available", "Fail");
            throw new Error('Error');
        }
    }

    /** 
   * desc: To click On Tag Edit Link in the view 
   * @method clickOnTagEditLink
   * @author: Adithya 
   * @param :{tagName:Tag Name}
   * @return none
   */
    async clickOnTagEditLink(tagName) {
        try {
            let tagNameLink = by.cssContainingText('xg-grid xg-grid-data-cell a', tagName)
            await action.Click(tagNameLink, tagName)
        }
        catch{
            throw new Error('Error');
        }
    }




}