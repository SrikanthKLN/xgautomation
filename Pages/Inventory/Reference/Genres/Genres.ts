import { browser, element, by, protractor, promise, WebElement } from 'protractor';
import { ActionLib } from '../../../../Libs/GeneralLibs/ActionLib';
import { VerifyLib } from '../../../../Libs/GeneralLibs/VerificationLib';

let action = new ActionLib();
let verify = new VerifyLib();


export class GenresPage {
 /**
      * desc: clickDataInGenerViewTable
      * @method clickDataInGenerViewTable
      * @author: vikas
      * @param :{}
      * @return none
      */
     async clickDataInGenerViewTable(clickColumnIndex: number,maintexttoMatch: string) {
        try {
           
            await verify.verifyProgressBarNotPresent();
            let locColumntoclick = by.xpath('//xg-grid[contains(@attr-name,"xgc-genre-view")]//table//tbody//tr[contains(@class,"ng-star-inserted")][contains(.,"'+maintexttoMatch+'")]//td[contains(@class,"ui-resizable-column")]['+clickColumnIndex+']');
            await action.MouseMoveToElement(locColumntoclick, "Mouse Move to Link ");
            await action.Click(locColumntoclick, "click Link"); 

        }
        catch (err) {
            await action.ReportSubStep("clickDataInGenerViewTable", "click data in gener View Table :error In Function " + maintexttoMatch, "Fail");
        }

    }
  /**
      * desc: verifyDataNotPresentInGenerViewTable
      * @method verifyDataNotPresentInGenerViewTable
      * @author: vikas
      * @param :{}
      * @return none
      */
     async verifyDataNotPresentInGenerViewTable(ColumnIndex: number, maintexttoMatch: string) {
        try {
           
            await verify.verifyProgressBarNotPresent();
            let locdata = by.xpath('//xg-grid[contains(@attr-name,"xgc-genre-view")]//table//tbody//tr[contains(@class,"ng-star-inserted")][contains(.,"'+maintexttoMatch+'")]//td[contains(@class,"ui-resizable-column")]['+ColumnIndex+']');
         
            await verify.verifyNotPresent(locdata, maintexttoMatch); 

        }
        catch (err) {
            await action.ReportSubStep("verifyDataNotPresentInGenerViewTable", "verify data : Error In Function" + maintexttoMatch, "Fail");
        }

    }  
    /**
       * desc: enter text in popup
       * @method enterTextInPopUp
       * @author: vikas
       * @param :{}
       * @return none
       */
      async enterTextInPopUp(textToenter: string) {

        try {
           let locInputPopup = by.css("p-dialog input[name*='textInput']");
           await action.SetText(locInputPopup, textToenter, "Input dialog box");
          
        }
        catch (err) {
            await action.ReportSubStep("enterTextInPopUp", " :error In Function " , "Fail");
        }
  
     } 
   /**
      * desc: click Button In clone poup
      * @method clickButtonInClonePopUp
      * @author: vikas
      * @param :{}
      * @return none
      */
     async clickButtonInClonePopUp(buttonName: string) {

        try {
           let locButtonName = by.xpath("//p-dialog//button[contains(text(),'"+buttonName+"')]");
           await action.Click(locButtonName, buttonName);
         
        }
        catch (err) {
            await action.ReportSubStep("clickButtonInClonePopUp", " :error In Function " , "Fail");
        }
  
     } 
    /**
      * desc: click close button In Popup
      * @method clickCloseXbuttonPopUp
      * @author: vikas
      * @param :{}
      * @return none
      */
     async clickCloseXbuttonPopUp() {

        try {
           let locButtonName = by.xpath("//p-dialog//span[contains(@class,'pi pi-times')]");
           await action.Click(locButtonName, "x button");
        }
        catch (err) {
            await action.ReportSubStep("clickCloseXbuttonPopUp", " :error In Function " , "Fail");
        }
  
     }     
   /**
     * desc: click Button In Confirmation Pop Up
     * @method clickButtonInConfirmationPopUp
     * @author: vikas
     * @param :{}
     * @return none
     */
    async clickButtonInConfirmationPopUp(buttonName: string) {

        try {
           let XpButtonName = by.xpath("//p-confirmdialog//span[contains(text(),'"+buttonName+"')]");
        
           await action.Click(XpButtonName, buttonName);
        }
        catch (err) {
            await action.ReportSubStep("clickButtonInConfirmationPopUp", " :error In Function " , "Fail");
        }
  
     }
  /**
     * desc: enter text In Xg Type ahead
     * @method enterTextInXgTypeahead
     * @author: vikas
     * @param :{}
     * @return none
     */
    async enterTextInXgTypeahead(attributeName, textToenter) {
        try {
           let locator = by.css('xg-typeahead[attr-name*="' + attributeName + '"] input');
        
           await action.MouseMoveToElement(locator, attributeName);
           await action.SetText(locator, textToenter, attributeName);
        }
        catch (err) {
            await action.ReportSubStep("enterTextInXgTypeahead", " :error In Function " , "Fail");
        }
  
     }
  /**
      * desc: verify no suggetion search result in fileds like Advertiser etc with Autocomplete option
      * @method verifyNoSuggetionInAutocompletewidget
      * @author: vikas
      * @param :{}
      * @return none
      */
     async verifyNoSuggetionInAutocompletewidget(log: string) {
        try {
           let XpNoSuggetion = by.xpath('//*[contains(text(),"No suggestions available")]');
         
           await action.MouseMoveToElement( XpNoSuggetion, log);
           await verify.verifyElementIsDisplayed( XpNoSuggetion, log);
        }
        catch (err) {
            await action.ReportSubStep("verifyNoSuggetionInAutocompletewidget", " :error In Function " , "Fail");
        }
  
     } 
   /**
    * desc: verifyOptionsFromComponentAutoComplete
    * @method verifyOptionsFromComponentAutoComplete
    * @author: vikas
    * @param :{}
    * @return none
    */
   async verifyOptionsFromComponentAutoComplete(optionText: string) {

    try {
       let locator = by.xpath('//ul[contains(@class,"ui-autocomplete-items")]//li[contains(@class,"ui-autocomplete-list-item")]//span[contains(text(),"' + optionText + '")]');
      
       await verify.verifyElement(locator, optionText + " DropDown");
       
    }
    catch (err) {
        await action.ReportSubStep("verifyOptionsFromComponentAutoComplete", " :error In Function " , "Fail");
    }
 }                 
}   