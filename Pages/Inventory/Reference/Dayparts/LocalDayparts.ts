import { browser, element, by, protractor, WebElement} from 'protractor';
import { VerifyLib } from '../../../../Libs/GeneralLibs/VerificationLib';
import { ActionLib } from '../../../../Libs/GeneralLibs/ActionLib';
import { AppCommonFunctions } from '../../../../Libs/ApplicationLibs/CommAppLib';

let action = new ActionLib();
let verify = new VerifyLib();
let commonLib = new AppCommonFunctions();

export class LocalDayparts {

   SaveButton_AddP = by.css('xg-popup xg-button[attr-name="xgc-btn-daypart-save"] button[class*="xg-button"]')
   Market = by.css('xg-dropdown[attr-name="xgc-markets"] label[class*="ui-dropdown-label"]')
   Tag = by.css('xg-multi-select[attr-name="xgc-dropdown-tags"] div[class*="ui-multiselect-label"]')
   textFilter = by.css('input[class*="ui-inputtext"]')
   clearAll=by.css("[mattooltip='Clear All']");
   
   buttonReassignDayPart = by.css("xg-button[attr-name='xgc-popup-submit'] button[title='Reassign Daypart']:not([disabled])");
   buttonAddDeviation = by.css("i[attr-name='xgc-deviation-dailog']");
   buttonCancel = by.css("xg-button[attr-name='xgc-popup-cancel'] button[title='Cancel']:not([disabled])");
   buttonSave = by.css("xg-button[attr-name='xgc-popup-submit'] button[title='Save']:not([disabled])");
   deviationChannelList = by.css("div[attr-name='xgc-daypart-deviation'] ul[class='ng-star-inserted'] li>a");
   deleteDeviationOption = "div[attr-name='xgc-daypart-deviation'] ul[class='ng-star-inserted'] li:nth-child(dynamic) i[attr-name='xgc-localadmin-delete-deviation']";
   buttonYes = by.css("xg-button[attr-name='xgc-btn-confirm-save'] button:not([disabled])");
   buttonNo = by.css("xg-button[attr-name='xgc-btn-confirm-clear'] button:not([disabled])");
   defaultNotifyMsg = by.xpath("//xg-grid[@attr-name='xgc-daypart-definition-grid']//div[normalize-space()='All cable stations will follow the below broadcast structure']");
   

   /**
       * desc: To enter Start/End Time In Daypart Edit Page
       * @method enterTimeInDaypartEdit
       * @author: Adithya
       * @param :{DayTime: Eg:MONStart/TUEEnd, data: Text to enter}
       * @return none
       */
   async enterTimeInDaypartEdit(DayTime: string, data: string, log: any) {
      try {
         //await browser.waitForAngular();
         let Component = by.css('xg-time-picker[name*="' + DayTime + 'Time"] input[name*="timeInput"]');
         await browser.executeScript('arguments[0].scrollIntoView()', element(Component).getWebElement());
         await browser.sleep(500);
         await action.ClearText(Component, '"' + DayTime + '"Time');
         await action.SetText(Component, data, '"' + DayTime + '"Time');
         action.ReportSubStep("enterTimeInDaypartEdit", "Entered Time In Daypart Edit " + DayTime + "Time", "Pass");
      }
      catch (err) {
         action.ReportSubStep("enterTimeInDaypartEdit", "Not Entered Time In Daypart Edit " + DayTime + "Time", "Fail");
         throw new Error(err);
      }

   }


   /**
    * desc: To return Components using TagName and Containing Text
    * @method returnComponentsTagNameContainingText
    * @author: Adithya
    * @param :{}
    * @return none
    */
   async returnComponentsTagNameContainingText(TagName: string, ContainsText: string, log: any) {
      try {
         //await browser.waitForAngular();
         let Component = by.xpath('//' + TagName + '[contains(text(),"' + ContainsText + '")]');
         return Component

      }
      catch (err) {
         throw new Error(err);
      }

   }

   /**
     * desc: To return Components using TagName and Containing Text
    * @method verifyDayPartsDisplayedinGrid
    * @author: Adithya
    * @param :{ElementLocator, DayParts: Default Dayparts, log: Dayparts/Daypart Code}
    * @return none
    */
   async verifyDayPartsDisplayedinGrid(ElementLocator, DayParts: string, log: any) {
      try {
         let defaultDaypartCount: number = 12
         let daypartCount = await element.all(ElementLocator).count()
         if (daypartCount == defaultDaypartCount) {
            action.ReportSubStep("verifyDayPartsDisplayedinGrid", "Day Part Count is " + daypartCount, "Pass")
            await element.all(ElementLocator).each(function (ReDayparts) {
               ReDayparts.getText().then(function (text) {

                  if (DayParts.includes(text)) {
                     console.log(text)
                     action.ReportSubStep("verifyDayPartsDisplayedinGrid", "Day Part avaiable" + text, "Pass")
                  }
                  else {
                     action.ReportSubStep("verifyDayPartsDisplayedinGrid", "Day  not Part avaiable" + text, "Fail")
                  }
               })
            })
         }
         else {
            action.ReportSubStep("verifyDayPartsDisplayedinGrid", "Day Part Count is not 12 it is  " + daypartCount, "Fail")
         }
      }
      catch (err) {
         throw new Error(err);
      }

   }

   /**
   * desc: To Validate day Part grid values in the view 

   * @method dayPartGridValidation

   * @author: Adithya

   * @param :{ColumnName:Column Name which is to be Validated,data:Testdata to be Entered}

   * @return none

   */


   async dayPartGridValidation(ColumnName: string, data: string) {
      try {
         //await browser.waitForAngular();
         await browser.sleep(1500);
         await element.all(by.tagName('th')).each(async function (value, index) {
            value.getText().then(async function (text) {
               if (text.trim() == ColumnName) {
                  // console.log("index of column is " + index, "Name of column is " + text)
                  let ColNo = Number(index) + 1
                  await browser.executeScript('window.scrollTo(100,250)');
                  await browser.sleep(1000)
                  await element.all(by.css('xg-grid tr td:nth-child(' + ColNo + ')')).each(function (value) {
                     value.getText().then(function (text) {
                        console.log(ColumnName + " is " + text)
                        if (text.trim() == data) {
                           action.ReportSubStep("dayPartGridValidation", "Column Name is " + ColumnName + "Data available " + text, "Pass");
                        }
                        else {
                           action.ReportSubStep("dayPartGridValidation", "Column Name is " + ColumnName + "Data available " + text, "Fail");
                        }
                     })
                  })
               }
            })
         })

      }
      catch (err) {
         throw new Error('Error');
      }
   }
   
   /**
   * desc: To Verify ColumnName Displayed In Grid
   * @method VerifyColumnNameDisplayedInGrid
   * @author: Adithya
   * @param :{}
   * @return none
   */
async VerifyColumnNameDisplayedInGrid(ColumnName: Array<any>) {
   try {
      //await browser.waitForAngular();
      for (let i = 0; i < ColumnName.length; i++) {
         let Component = by.xpath('//th[contains(text(),"' + ColumnName[i] + '")]');
         await browser.executeScript('arguments[0].scrollIntoView()', element(Component).getWebElement());
         await browser.sleep(500);
         await verify.verifyElementIsDisplayed(Component, ColumnName[i]);
         action.ReportSubStep("VerifyColumnNameDisplayedInGrid", "Displayed " + " " + ColumnName[i] + "", "Pass");
      }
   }
   catch (err) {
      action.ReportSubStep("VerifyColumnNameDisplayedInGrid", "Not Displayed " + ColumnName + "", "Fail");
      throw new Error(err);
   }

}

 /**
    * desc: used to add a deviation channel
    * @method addDeviationChannel
    * @author: Venkata Sivakumar
    * @param :{channelName:string}
    * @return none
    */
   async addDeviationChannel(channelName: string) {
      try {
          await verify.verifyElementIsDisplayed(this.buttonAddDeviation, "Verify Add Deviation button is displayed");
          await action.Click(this.buttonAddDeviation, "Click On Add Deviation button");
          await commonLib.selectDropDownValue("Channels", "ESPN (CABLE)");
          await action.Click(this.buttonSave, "Click On Save button");
          await browser.sleep(2000);
          await commonLib.verifyPopupMessage("Deviation Channel created successfully")
      } catch (error) {
          throw new Error(error);
      }
  }

  /**
* desc: used to delete a deviation channel
* @method deleteDeviationChannel
* @author: Venkata Sivakumar
* @param :{deviationChannelName:string}
* @return none
*/
  async deleteDeviationChannel(deviationChannelName: string) {
      try {
          await verify.verifyElementIsDisplayed(this.deviationChannelList, "Verify Devaition Channel List is displayed");
          let elements: Array<WebElement> = await action.getWebElements(this.deviationChannelList);
          if (elements.length == 0) await action.ReportSubStep("deleteDeviationChannel", "There are no deviation channels exist to delete", "Fail");
          for (let index = 0; index < elements.length; index++) {
              let text = await elements[index].getText();
              if (String(text).trim() == deviationChannelName.trim()) {
                  await elements[index].click();
                  await browser.sleep(2000);
                  await action.Click(by.css(this.deleteDeviationOption.replace("dynamic", String(index + 1))), "Click On Deviate Delete Option")
                  await verify.verifyElementIsDisplayed(this.buttonYes, "Verify Yes buuton is displayed");
                  await action.Click(this.buttonYes, "Click On Yes button");
                  await browser.sleep(2000);
                  await commonLib.verifyPopupMessage("Deviation Channel deleted successfully");
                  break;
              }
              else if (String(text).trim() != deviationChannelName.trim() && index == elements.length - 1) {
                  await action.ReportSubStep("deleteDeviationChannel", deviationChannelName + " is not available", "Fail");
              }
          }
      } catch (error) {
          throw new Error(error);
      }
  }

      /**
* desc: used to click on a deviation channel
* @method ClickOnDeviationChannel
* @author: Venkata Sivakumar
* @param :{deviationChannelName:string}
* @return none
*/
async ClickOnDeviationChannel(deviationChannelName: string) {
  try {
      await verify.verifyElementIsDisplayed(this.deviationChannelList, "Verify Devaition Channel List is displayed");
      let elements: Array<WebElement> = await action.getWebElements(this.deviationChannelList);
      if (elements.length == 0) await action.ReportSubStep("ClickOnDeviationChannel", "There are no deviation channels exist to delete", "Fail");
      for (let index = 0; index < elements.length; index++) {
          let text = await elements[index].getText();
          if (String(text).trim() == deviationChannelName.trim()) {
              await elements[index].click();
              await verify.verifyProgressBarNotPresent();
              break;
          }
          else if (String(text).trim() != deviationChannelName.trim() && index == (elements.length - 1)) {
              await action.ReportSubStep("ClickOnDeviationChannel", deviationChannelName + " is not available", "Fail");
          }
      }
  } catch (error) {
      throw new Error(error);
  }
}
}
