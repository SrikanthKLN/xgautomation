import { by, browser, ElementFinder, WebElement } from "protractor";
import { ActionLib } from "../../../Libs/GeneralLibs/ActionLib";
import { VerifyLib } from "../../../Libs/GeneralLibs/VerificationLib";
import { LocalProgramming } from "../LocalProgramming";
import { ProgramRatingTracks } from "../ProgramRatingTracks";

let action = new ActionLib();
let verify = new VerifyLib();
let localProgramPage = new LocalProgramming();
let programTracksPage = new ProgramRatingTracks();

export class DayPartDemoPage {
    //Constants
    deleteMessage = "Successfully deleted Description";
    demoSuccessMessage = "Demos are successfully added";
    //Locators
    graphicSelectorButton = "//xgc-lib-demo-selector-dialog//div[contains(@class,'xg-pull-right')]//xg-button[@icon='fa fa-plus']//button[normalize-space(text())='dynamic' and not(@disabled)]";
    selectorDialogLowerSectionButton = "//xgc-lib-demo-selector-dialog//div[contains(@class,'xg-pull-right')]//xg-button[not(@icon='fa fa-plus')]//button[normalize-space(text())='dynamic' and not(@disabled)]";
    xgListContentItem = "//xg-list//xg-header[contains(text(),'dynamicheader')]/../..//xg-standard-list-item//div[contains(text(),'dynamicitem')]";
    rowsCount = "//xg-grid[@attr-name='xgc-daypart-demo-grid']//table/tbody/tr";
    gridRowDeleteButton = "//span[normalize-space()='dynamic']/ancestor::td/following-sibling::td//button";
    deleteLink = by.css("a[attr-name='xgc-row-delete']");
    addButton = by.css("xg-button[attr-name='xgc-rowadd'] button:not([disabled])");
    addPlusButton = by.css("xg-button[icon='fa fa-plus'] button[title='Add']:not([disabled])");
    addDemoButton = by.css("xg-button[attr-name='xgc-add-demo'] button:not([disabled])");
    addToAllButton = by.css("xg-button[attr-name='xgc-add-to-all-demo'] button:not([disabled])");
    cancelButton = by.css("xg-button[attr-name='xgc-cancel-demo'] button:not([disabled])");
    noDataMatchFound = by.xpath("//td[normalize-space()='No data match is found']");
    firstRowDemos = by.css("xg-grid[attr-name='xgc-daypart-demo-grid'] div[class='xg-grid'] table>tbody>tr:nth-child(1)>td:nth-child(1)");
    comscoreButton = by.css("xg-button[attr-name='xgc-button-comscore']:not([class='active'])");
    comscoreActiveButton = by.css("xg-button[attr-name='xgc-button-comscore'][class='active']");

    // xg-grid[attr-name='xgc-daypart-demo-grid'] div[class*='ui-table ui-widget'] tbody>tr
    //Methods
    /**
     * desc: click Button In demo Selector Dialog Lower Section
     * @method clickButtonIndemoSelectorDialogLowerSection
     * @author: vikas
     * @param :{}
     * @return none
     */
    async clickButtonIndemoSelectorDialogLowerSection(buttonName: string) {

        try {
            let locator = by.xpath(this.selectorDialogLowerSectionButton.replace("dynamic", String(buttonName)));
            await browser.sleep(2000);
            await action.MouseMoveToElement(locator, buttonName + " In lib demo selector dialog");
            await action.Click(locator, buttonName + " In lib demo selector dialog");
        }
        catch (err) {
            throw new Error(err);
        }
    }
    /**
       * desc: click Button In Xg Demographic Selector
       * @method clickButtonInXgDemographicSelector
       * @author: vikas
       * @param :{}
       * @return none
       */
    async clickButtonInXgDemographicSelector(buttonName: string) {

        try {
            let locator = by.xpath(this.graphicSelectorButton.replace("dynamic", String(buttonName)));
            await browser.sleep(4000);
            await action.MouseMoveToElement(locator, buttonName + " In XgDemographicSelector");
            await action.Click(locator, buttonName + " In XgDemographicSelector");
        }
        catch (err) {
            throw new Error(err);
        }
    }
    /**
       * desc: verify XG List Content Item
       * @method verifyXGListContentItem
       * @author: vikas
       * @param :{}
       * @return none
       */
    async verifyXGListContentItem(headerText: string, itemName: string) {

        try {
            let locator = by.xpath(this.xgListContentItem.replace("dynamicheader", String(headerText)).replace("dynamicitem", String(itemName)));
            await action.MouseMoveToElement(locator, itemName);
            await verify.verifyElementIsDisplayed(locator, itemName);
        }
        catch (err) {
            throw new Error(err);
        }
    }
    /**
      * desc: click XGList Content Item
      * @method clickXGListContentItem
      * @author: vikas
      * @param :{}
      * @return none
      */
    async clickXGListContentItem(headerText: string, itemName: string) {

        try {
            let locator = by.xpath(this.xgListContentItem.replace("dynamicheader", String(headerText)).replace("dynamicitem", String(itemName)));
            await browser.sleep(1000);
            await action.MouseMoveToElement(locator, itemName);
            await action.Click(locator, itemName);
        }
        catch (err) {
            throw new Error(err);
        }
    }

    /**
         * desc: Used to click on delete button based on demo Or description value
         * @method clickOnDeleteButton
         * @author: Venkata Sivakumar
         * @param :{demoOrDescriptionText: string}
         * @return none
         */
    async clickOnDeleteButton(demoOrDescriptionText: string) {
        try {
            await verify.verifyElementIsDisplayed(by.xpath(this.gridRowDeleteButton.replace("dynamic", String(demoOrDescriptionText).trim())), "");
            await action.Click(by.xpath(this.gridRowDeleteButton.replace("dynamic", String(demoOrDescriptionText).trim())), "")
            await verify.verifyElementIsDisplayed(this.deleteLink, "Verify Delete Option");
            await action.Click(this.deleteLink, "Click On Delete Option");
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
         * desc: Used to click on Add button in Demo Selector dialog
         * @method clickOnAddButtonInDemoSelectorDialog
         * @author: Venkata Sivakumar
         * @param :{}
         * @return none
         */
    async clickOnAddButtonInDemoSelectorDialog() {
        try {
            await action.Click(this.addPlusButton, "");
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
         * desc: Used to click on Add button in Demo Selector dialog footer section
         * @method clickOnAddButtonInDemoSelectorDialogFooter
         * @author: Venkata Sivakumar
         * @param :{}
         * @return none
         */
    async clickOnAddButtonInDemoSelectorDialogFooter() {
        try {
            await action.Click(this.addDemoButton, "");
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
         * desc: Used to click on Add To All button in Demo Selector dialog footer section
         * @method clickOnAddToAllButtonInDemoSelectorDialogFooter
         * @author: Venkata Sivakumar
         * @param :{}
         * @return none
         */
    async clickOnAddToAllButtonInDemoSelectorDialogFooter() {
        try {
            await action.Click(this.addToAllButton, "");
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
         * desc: Used to click on Cancel button in Demo Selector dialog footer section
         * @method clickOnCancelButtonInDemoSelectorDialogFooter
         * @author: Venkata Sivakumar
         * @param :{}
         * @return none
         */
    async clickOnCancelButtonInDemoSelectorDialogFooter() {
        try {
            await action.Click(this.cancelButton, "");
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
     * desc: Used to add day part based on market option and day part option
     * @method addDayPartsDemo
     * @author: Venkata Sivakumar
     * @param :{marketOption:string,dayPartsOption:string}
     * @return none
     */
    async addDayPartsDemo(marketOption: string, dayPartsOption: string) {
        try {
            await localProgramPage.SelectMarketOptions(marketOption.trim());
            await browser.sleep(2000);
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("Dayparts", dayPartsOption.trim());
            await action.Click(this.addButton, "Click On Add Button");
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("Parent Block", "Adults");
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("Start Range", "18");
            await programTracksPage.clickDataInRatingSourceTableTarcksPage("End Range", "20");
            await this.clickOnAddButtonInDemoSelectorDialog();
            await this.clickOnAddButtonInDemoSelectorDialogFooter();
            await browser.sleep(1000);
            await localProgramPage.verifyPopupMessageRatecard(this.demoSuccessMessage);
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
     * desc: It is used to click on Comscore button
     * @method clickOnComscore
     * @author: Venkata Sivakumar
     * @param :{}
     * @return none
     */
    async clickOnComscore()
    {
        let elementStatus:boolean;
        try {
            elementStatus = await verify.verifyElementVisible(this.comscoreButton);
            if(elementStatus)
            {
                await action.Click(this.comscoreButton,"Click On Comscore");
                await verify.verifyElementIsDisplayed(this.comscoreActiveButton,"Verify comscore is selected");
            }
            else if(!this.comscoreActiveButton)
            {
                action.ReportSubStep("clickOnComscore","Either "+this.comscoreButton+" Or "+this.comscoreActiveButton+" locators are not availble to find the comscore webelement","Fail");
            }
        } catch (error) {
            
        }
    }


}
