import { browser, element, by, protractor, promise, WebElement } from 'protractor';
import { ActionLib } from '../../Libs/GeneralLibs/ActionLib';
import { VerifyLib } from '../../Libs/GeneralLibs/VerificationLib';
import { Gutils } from '../../Utility/gutils';
import { AppCommonFunctions } from '../../Libs/ApplicationLibs/CommAppLib';

let gUtils = new Gutils();
let action = new ActionLib();
let verify = new VerifyLib();
let commonLib = new AppCommonFunctions();

export class LocalProgramming {

    //Locators  

    // market = by.xpath("//label[contains(text(),'Market')]//..//div[contains(@class,'ui-helper-hidden-accessible')]/following-sibling::label")
    marketOptions = "//span[contains(text(),'dynamic')]";
    sortIcon = by.xpath("//th[1]/p-sorticon[@arialabel='Activate to sort']")
    sortCleared = by.css("[class='ui-sortable-column ui-resizable-column ng-star-inserted ui-state-highlight']")
    reset = by.css("[class='fa fa-sync']")
    removeProgram = by.css("[title='Remove Program Averages']")
    clickRatingBuild = by.xpath("//button[text()=' Rating Build ']")
    checkSurveys = by.xpath("//label[text()='Survey(s)']")
    assignPlaybacks = by.xpath("//label[text()='Assign to All Playback Types']")
    verifyCancel = by.css("[title='Cancel']")
    clickLink = by.xpath("//a[text()=' Link ']")
    displayOptions = by.css("[title='Display Options']")
    clickProgram = by.xpath("//xg-grid//div[contains(@class,'ui-table-frozen-view')]//tbody//tr[contains(@class,'ng-star-inserted')]//i[@class='pi pi-caret-down']")

    channelsOptionsSelected = "//p-multiselectitem[@ng-reflect-selected = 'true']//*[contains(text(),'dynamic')]";
    dayPartsOptionsSelected = "//p-multiselectitem[@ng-reflect-selected = 'true']//*[contains(text(),'dynamic')]";
    firstRowStartTime = by.css("xg-grid[attr-name='xgc-program-grid']>div[class='xg-grid'] table>tbody>tr:nth-child(1)>td:nth-child(6)");
    firstRowEndTime = by.css("xg-grid[attr-name='xgc-program-grid']>div[class='xg-grid'] table>tbody>tr:nth-child(1)>td:nth-child(7)");

    // market = by.xpath("//label[contains(text(),'Market')]//..//div[contains(@class,'ui-helper-hidden-accessible')]/following-sibling::label");
    market = by.css("xg-dropdown[attr-name='xgc-markets']  div[class*='ui-dropdown-trigger']")
    channels = by.xpath("//label[contains(text(),'Channels')]//..//div[contains(@class,'ui-helper-hidden-accessible')]/following-sibling::div");
    channelsOptions = "//p-multiselectitem[@ng-reflect-selected = 'false']//*[contains(text(),'dynamic')]"
    multiSelectCloseOption = by.css("a[class*='ui-multiselect-close']");
    dayParts = by.xpath("//label[contains(text(),'Dayparts')]//..//div[contains(@class,'ui-helper-hidden-accessible')]/following-sibling::div");
    dayPartsOptions = "//p-multiselectitem[@ng-reflect-selected = 'false']//*[contains(text(),'dynamic')]"

    globalFilter = by.css('xg-grid[attr-name*="xgc-program-grid"] xg-grid-toolbar input[placeholder*="Global Filter"]');
    globalFilterNW = by.css('xg-grid[attr-name*="xgc-display-network-programs"] xg-grid-toolbar input[placeholder*="Global Filter"]');

    firstRowSellingTitle = by.css("xg-grid[attr-name='xgc-program-grid']>div[class='xg-grid'] table>tbody>tr:nth-child(1)>td:nth-child(3)");
    activeCheckBox = by.css("xg-checkbox[attr-name='xgc-active'][ng-reflect-model='false'] input");
    activeCheckBoxSelect = by.css("xg-checkbox[attr-name='xgc-active'][ng-reflect-model='true'] input");
    localProgram = '//xg-grid[contains(@attr-name,"xgc-program-grid")]//table//tbody//tr//td[contains(.,"dynamic")]'

    //Siva
    sellingtitleinpopupNW = by.css('xg-input[attr-name="xgc-network-program-selling_title"] input')
    cancelNW = by.css('xg-button[attr-name="xgc-cancel-program"] button')
    updateNW = by.css('xg-button[attr-name="xgc-save-program"] button')


    //dhana
    
        
    sellingtitleNW = by.css('xg-grid tr:nth-child(1) td:nth-child(4)')
    liveOption = by.xpath("//label[text()='Live']")
    startTime = by.css("[attr-name='xgc-time-start'] [name='timeInput']")
    endDate = by.css("[attr-name='xgc-time-end'] [name='timeInput']")
    dateValidation = by.xpath("//*[@attr-name='xgc-time-start']// div[text()=' Start Time is ']")
    dayPartValidation = by.xpath("//*[@attr-name='xgc-dropdown-daypart']// div[text()=' Dayparts is ']")
    Apav = by.xpath("//*[@attr-name='xgc-check-pav'] //label[text()='Auto Build PAV']")
    startdate = by.css("[attr-name='xgc-date-start'] [name='DateInput']")
    dayValidation = by.xpath("//*[@attr-name='xgc-date-start'] //div[text()=' Invalid Date Format ']")
    saveDiasable = by.css("[attr-name*='xgc-save-program'] [type='Save'][disabled")


    //dhana
    deleteButton = by.css("xg-button[attr-name='xgc-delete-program-button'],[attr-name='xgc-network-program-delete']  button");
    confirmPopupDeleteButton = by.css("xg-popup[attr-name*='xgc-delete']  xg-button[attr-name*='xgc-delete']");
    confirmPopupCancelButton = by.css("xg-popup[attr-name*='xgc-delete']  xg-button[attr-name*='xgc-cancel']");
    genreInput = by.css("xg-typeahead[attr-name='xgc-select-genre'],[attr-name='xgc-typeahead-genre'] input");
    tagInput = by.css("xg-typeahead[attr-name='xgc-select-keyword'],[attr-name='xgc-typeahead-tag'] input");
    externalIdInput = by.css("xg-input[attr-name*='external'] input");
    lockCheckBox = by.css("xg-checkbox[attr-name*='lock'] input[ng-reflect-model='false']");
    lockCheckBoxActive = by.css("xg-checkbox[attr-name*='lock'] input[ng-reflect-model='true']");
    ftcInput = by.xpath("//xg-date-picker[@attr-name='xgc-telecast-startdate-date-picker' or @attr-name='xgc-date-start']//input");
    ltcInput = by.xpath("//xg-date-picker[@attr-name='xgc-telecast-enddate-date-picker'or @attr-name='xgc-date-end']//input");
    popupActiveCheckBoxSelect = by.css("xg-checkbox[attr-name='xgc-program-active'],[attr-name='xgc-check-active'] input[ng-reflect-model='true']");
    popupActiveCheckBox = by.css("xg-checkbox[attr-name='xgc-program-active'],[attr-name='xgc-check-active'] input[ng-reflect-model='false']");
    tagSuggestionListItems = by.css("xg-typeahead[attr-name='xgc-select-keyword'] ul[class*='ui-autocomplete-items']>li");
    genreSuggestionListItems = by.css("xg-typeahead[attr-name='xgc-select-genre'] ul[class*='ui-autocomplete-items']>li");
    descriptionTextArea = by.css("xg-textarea[attr-name='xgc-program-description'] textarea");
    addOrbitBuuton = by.css("xg-button[attr-name='xgc-add-orbits-button'] button");
    orbitDetailsTableHeaders = by.xpath("//h3[text()='Orbit Details']/../following-sibling::div//xg-grid[@attr-name='xgc-program-grid']//table/thead/tr/th[not(@ng-reflect-field='Delete')]");
    orbitDetailsTableCheckBox = "//h3[text()='Orbit Details']/../following-sibling::div//xg-grid[@attr-name='xgc-program-grid']//table//tr/td[dynamic]/xg-grid-data-cell//mat-checkbox";
    cancelButton = by.css("xg-button[attr-name='xgc-cancel-program'] button");
    feedOptions = by.css("xg-radio-group[attr-name*='xgc-radio'] label");
    cloneButton = by.css("xg-button[attr-name*='xgc-clone'] button");
    cloneSellingTitleInput = by.css("xg-input[attr-name*='selling-title'] input");
    confirmClonePopupCloneButton = by.css("xg-button[attr-name='xgc-clone-program-button'],[attr-name='xgc-network-program-clone-confirm'] button:not([disabled])"); 
    confirmClonePopupCancelButton = by.css("xg-button[attr-name='xgc-cancel-clone-program-button'],[attr-name='xgc-network-program-cancel-confirm'] button");
    sellingtitle = by.css('xg-grid tr:nth-child(1) td:nth-child(3)')
    sellingtitleinpopup =by.css('xg-input[attr-name="xgc-program-name"] input')
    descriptioninpopup = by.css('xg-textarea[attr-name="xgc-program-description"] textarea')
    cancel = by.css('xg-button[attr-name="xgc-cancel-local-program"] button')
    update = by.css('xg-button[attr-name="xgc-save-local-program"] button')
    delete = by.css('xg-button[attr-name="xgc-delete-program-button"] button[class*="xg-button"]')
    Notification = by.css('div[class*="ui-toast ui-widget ui-toast-top-right"]')
    popupheader = by.css('div[class*="ui-dialog-titlebar"]')
    cancelinpopup = by.css(' xg-button[attr-name="xgc-cancel-delete-button"] button[class*="xg-button"]')
    deleteinpopup=by.css('xg-popup xg-button[attr-name="xgc-delete-program-button"]')
    sellingtitleingrid = by.css("xg-grid tr:nth-child(1) td:nth-child(3)");
    starttime = by.css('xg-time-picker[attr-name="xgc-starttime-time-picker"] input[name*="timeInput"]')
    endtime = by.css('xg-time-picker[attr-name="xgc-endtime-time-picker"] input[name*="timeInput"]')
    /**
         * desc: click Multi select Drop Down Inventory Add Program 
         * @method clickMultiselectDropDownInventoryAddProgram
         * @author: vikas
         * @param :{}
         * @return none
         */
    async clickMultiselectDropDownInventoryAddProgram(attributeName: string) {
        try {
            let locator = by.css('p-dialog xg-multi-select[attr-name*="' + attributeName + '"] p-multiselect');
            await browser.sleep(3000);
            await action.MouseMoveToElement(locator, attributeName);
            await action.Click(locator, attributeName);
        }
        catch (err) {
            action.ReportSubStep("clickMultiselectDropDownInventoryAddProgram", "Error in  function", "Fail");
        }
    }
    /**
         * desc: click Multi Select Option Inventory Add Program
         * @method clickMultiSelectOptionInventoryAddProgram
         * @author: vikas
         * @param :{}
         * @return none
         */
    async clickMultiSelectOptionInventoryAddProgram(optionName: string) {

        try {
            let locator = by.css('p-multiselectitem li label');
            let eleitem = await action.makeDynamicLocatorContainsText(locator, optionName, "Equal");
            await action.Click(eleitem, optionName);
        }
        catch (err) {
            action.ReportSubStep("clickMultiSelectOptionInventoryAddProgram", "Error in  function", "Fail");
        }
    }
    /**
* desc: click buttons in Ratecard Page
* @method clickonButtonsRatecardPage
* @author: vikas
* @param :{}
* @return none
*/
    async clickonButtonsRatecardPage(attributeName: string, buttonName: string) {

        try {
            let Xpbutton = by.xpath('//xg-button[contains(@attr-name,"' + attributeName + '")]//button[normalize-space(text())="' + buttonName + '" and not(@disabled)]');
            await action.MouseMoveToElement(Xpbutton, buttonName)
            await action.Click(Xpbutton, buttonName);
        }
        catch (err) {
            action.ReportSubStep("clickonButtonsRatecardPage", "Error in  function", "Fail");
        }
    }
    /**
     * desc: verify drop down Option Inventory
     * @method verifydropdownOptionInventory
     * @author: vikas
     * @param :{}
     * @return none
     */
    async verifydropdownOptionInventory(optionName: string) {

        try {
            let locator = by.css('ul#dropdownMenuButton a');
            let eleitem = await action.makeDynamicLocatorContainsText(locator, optionName, "Equal");
            await verify.verifyElementIsDisplayed(eleitem, optionName);
        }
        catch (err) {
            action.ReportSubStep("verifydropdownOptionInventory", "Error in  function", "Fail");
        }
    }

    /**
        * desc: click drop down Option Inventory
        * @method clickDropdownOptionInventory
        * @author: vikas
        * @param :{}
        * @return none
        */
    async clickDropdownOptionInventory(optionName: string) {

        try {
            let locator = by.css('ul#dropdownMenuButton a');
            let eleitem = await action.makeDynamicLocatorContainsText(locator, optionName, "Equal");
            await action.MouseMoveToElement(eleitem, optionName);
            await action.Click(eleitem, optionName);
        }
        catch (err) {
            action.ReportSubStep("clickDropdownOptionInventory", "Error in  function", "Fail");
        }
    }
    /**
      * desc: click Description Input 
      * @method clickDescriptionInputInventory
      * @author: vikas
      * @param :{}
      * @return none
      */
    async clickDescriptionInputInventory() {
        try {
            let locator = by.css('xg-textarea[attr-name*="xgc-program-description"] textarea');
            await action.MouseMoveToElement(locator, "Description");
            await action.Click(locator, "Description");
        }
        catch (err) {
            action.ReportSubStep("clickDescriptionInputInventory", "Error in  function", "Fail");
        }
    }
    /**
   * desc: enter Text In Description Input 
   * @method enterTextInDescriptionInput
   * @author: vikas
   * @param :{}
   * @return none
   */
    async enterTextInDescriptionInput(textToenter) {
        try {
            let locator = by.css('xg-textarea[attr-name*="xgc-program-description"] textarea');
            await action.MouseMoveToElement(locator, "Description");
            await action.SetText(locator, textToenter, "Description");
        }
        catch (err) {
            action.ReportSubStep("enterTextInDescriptionInput", "Error in  function", "Fail");
        }

    }
    /**
      * desc: click Days Option Inventory dialog
      * @method clickDaysOptionInventorydialog
      * @author: vikas
      * @param :{}
      * @return none
      */
    async clickDaysOptionInventorydialog(optionName: string) {

        try {
            // let locator = by.css('p-dialog xg-day-picker[attr-name*="xgc-days-day-picker"] label[class*="xg-check-label"]');
            let locator = by.css('p-dialog xg-day-picker[name*="days"] label[class*="xg-check-label"]');
            let eleitem = await action.makeDynamicLocatorContainsText(locator, optionName, "Equal");
            await action.Click(eleitem, optionName);
        }
        catch (err) {
            action.ReportSubStep("clickDaysOptionInventorydialog", "Error in  function", "Fail");
        }
    }
    /**
    * desc: click Clock Icon Time Option Inventory dialog
    * @method clickClockIconTimeOptionInventorydialog
    * @author: vikas
    * @param :{}
    * @return none
    */
    async clickClockIconTimeOptionInventorydialog(attributeName: string) {

        try {
            let locator = by.css('xg-time-picker[attr-name*="' + attributeName + '"] i[class*="fa fa-clock"]');
            await action.Click(locator, attributeName + " clock icon");
        }
        catch (err) {
            action.ReportSubStep("clickClockIconTimeOptionInventorydialog", "Error in  function", "Fail");
        }
    }
    /**
       * desc: click Time In Inventory dialog Time table
       * @method clickTimeInInventorydialogTimetable
       * @author: vikas
       * @param :{}
       * @return none
       */
    async clickTimeInInventorydialogTimetable(timeOption: string) {

        try {
            let locator = by.xpath('//table[contains(@class,"xg-hours-table")]//td[contains(text(),"' + timeOption + '")]');
            await browser.sleep(1000);
            await action.Click(locator, timeOption);
        }
        catch (err) {
            action.ReportSubStep("clickTimeInInventorydialogTimetable", "Error in  function", "Fail");
        }
    }
    /**
 * desc: enter Date In Input Inventory Add Program
 * @method enterDateInInputInventoryAddProgram
 * @author: vikas
 * @param :{}
 * @return none
 */
    async enterDateInInputInventoryAddProgram(attributeName, edateToenter) {
        try {
            let locator = by.css('xg-date-picker[attr-name*="' + attributeName + '"] input');
            await action.MouseMoveToElement(locator, attributeName);
            await action.SetText(locator, edateToenter, attributeName);
        }
        catch (err) {
            action.ReportSubStep("enterDateInInputInventoryAddProgram", "Error in  function", "Fail");
        }
    }
    /**
     * desc: verify Popup Message Rate card
     * @method verifyPopupMessageRatecard
     * @author: vikas
     * @param :{}
     * @return none
     */
    async verifyPopupMessageRatecard(msgText: string) {

        try {
            let locMessage = by.css('div[class*="ui-toast-detail"]');
            await verify.verifyTextInElement(locMessage, msgText, "alret msg");
        }
        catch (err) {
            action.ReportSubStep("verifyPopupMessageRatecard", "Error in  function", "Fail");
        }
    }

    async SelectMarketOptions(optionName: string) {
        try {
            await action.Click(this.market, "Market")
            await browser.sleep(1000);
            await action.Click(by.xpath(this.marketOptions.replace("dynamic", String(optionName))), optionName);
        }
        catch (err) {
            action.ReportSubStep("SelectMarketOptions", "Error in  function", "Fail");
        }
    }

    /**
  * desc: enter text in Input Inventory Add Program
  * @method enterTextInInputInventoryAddProgram
  * @author: vikas
  * @param :{}
  * @return none
  */
    async enterTextInInputInventoryAddProgram(attributeName: string, textToenter) {
        try {
            let locator = by.css('xg-input[attr-name*="' + attributeName + '"] input');
            await action.MouseMoveToElement(locator, attributeName);
            await action.SetText(locator, textToenter, attributeName);
        }
        catch (err) {
            action.ReportSubStep("enterTextInInputInventoryAddProgram", "Error in  function", "Fail");
        }
    }

    async GridValidations(ColumnName: string, data: string) {
        try {
            await browser.sleep(1500);
            await element.all(by.tagName('th')).each(async function (value, index) {
                value.getText().then(async function (text) {
                    if (text == ColumnName) {
                        console.log("index of column is " + index, "Name of column is " + text)
                        let ColNo = Number(index) + 1
                        console.log("Column no is" + ColNo)
                        await browser.executeScript('window.scrollTo(100,250)');
                        await browser.sleep(1000)
                        await element.all(by.css('xg-grid tr td:nth-child(' + ColNo + ') search-text-highlighter')).each(function (value) {
                            value.getText().then(function (text) {
                                console.log(ColumnName + " is " + text)
                                if (text.includes(data)) {
                                    action.ReportSubStep("GridValidations", "Data available " + text, "Pass");
                                }
                                else {
                                    action.ReportSubStep("GridValidations", "Data Not available " + text, "Fail");
                                }
                            })
                        })
                    }
                })
            })
        }
        catch (err) {
            action.ReportSubStep("GridValidations", "Error in  function", "Fail");
        }
    }
    /**
       * desc: verify check box is selected of commission  like Telivision , Digital etc  
       * @method verifyCheckBoxIsSelected
       * @author: vikas
       * @param :{}
       * @return none
       */
    async verifyCheckBoxIsSelected(lableName: string) {
        try {
            let XpCheckBox = by.xpath('//label[normalize-space(text())="' + lableName + '"]//preceding-sibling::input[contains(@type,"checkbox")]');

            await action.MouseMoveToElement(XpCheckBox, lableName);
            await browser.sleep(1000);
            await element(XpCheckBox).isSelected().then(function (value) {
                console.log("Check box value " + value);
                if (value == true) {
                    action.ReportSubStep("functionName", lableName + " Is Selected", "Pass");
                }
                else {
                    action.ReportSubStep("functionName", lableName + " Is not Selected", "Fail");
                }
            }, function (err) {
                action.ReportSubStep("functionName", lableName + "Error in getting check box value", "Fail");
            });

        }
        catch (err) {
            action.ReportSubStep("verifyCheckBoxIsSelected", "Error in  function", "Fail");
        }

    }
    /**
           * desc: verify check box is not  selected of commission  like Telivision , Digital etc  
           * @method verifyCheckBoxIsNotSelected
           * @author: vikas
           * @param :{}
           * @return none
           */
    async verifyCheckBoxIsNotSelected(lableName: string) {
        try {
            let XpCheckBox = by.xpath('//label[contains(text(),"' + lableName + '")]//preceding-sibling::input[contains(@type,"checkbox")]');

            await action.MouseMoveToElement(XpCheckBox, lableName);
            await browser.sleep(1000);
            await element(XpCheckBox).isSelected().then(function (value) {
                console.log("Check box value verifyCheckBoxIsNotSelected " + value);
                if (value == false) {
                    action.ReportSubStep("functionName", lableName + " Is not Selected", "Pass");
                }
                else {
                    action.ReportSubStep("functionName", lableName + " Is  Selected", "Fail");
                }
            }, function (err) {
                action.ReportSubStep("functionName", lableName + "Error in getting check box value", "Fail");
            });

        }
        catch (err) {
            action.ReportSubStep("verifyCheckBoxIsNotSelected", "Error in  function", "Fail");
        }

    }
    /**
      * desc: verify Decimal Option
      * @method verifyDecimalOption
      * @author: vikas
      * @param :{}
      * @return 
      */
    async verifyDecimalOption(nameOfMatslider: string, valuetoVerify: string) {
        try {
            let locator = by.xpath('//div[contains(text(),"Decimal")]/..//mat-slider[contains(@name,"' + nameOfMatslider + '")][contains(.,"' + valuetoVerify + '")]');
            await action.MouseMoveToElement(locator, nameOfMatslider);
            await browser.sleep(2000);
            await verify.verifyElementIsDisplayed(locator, nameOfMatslider + "Slider Value");
        }
        catch (err) {
            action.ReportSubStep("verifyDecimalOption", "Error in  function", "Fail");
        }

    }





    /**
      * desc: clickToggaleProgramsTracksPAVTable
      * @method clickToggaleProgramsTracksPAVTable
      * @author: vikas
      * @param :{}
      * @return none
      */
    async clickProgramLinkInLocalProgramGrid(clickColumnName: string, columnMainName: string, maintexttoMatch: string) {
        try {
            let locTableRow = by.xpath('//xg-grid[contains(@attr-name,"xgc-program-grid")]//table//tbody//tr[contains(@class,"ng-star-inserted")]');
            let locColumns = by.xpath('//xg-grid[contains(@attr-name,"xgc-program-grid")]//table//thead//th[contains(@class,"ui-sortable-column")]');
            let columnIndexMain: number;
            let clickColumnIndex: number;
            let flag = false;
            let rowIndexG;

            await browser.sleep(2000);
            await verify.verifyProgressBarNotPresent();
            await browser.sleep(2000);
            await browser.waitForAngular();
            await element.all(locColumns).each(async function (ele, Index) {
                await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
                    await ele.getText().then(function (_valuecol) {
                        if (_valuecol.trim() == columnMainName) {
                            columnIndexMain = Index + 2;
                        }

                        if (_valuecol.trim() == clickColumnName) {
                            clickColumnIndex = Index + 2;
                        }
                    });
                });
            }).then(async function () {
                console.log("Main Col " + columnIndexMain);
                console.log("click Col " + clickColumnIndex);
                await element.all(locTableRow).each(async function (ele, Index) {
                    let rowIndex = Index + 1
                    let locColumn = by.xpath('//xg-grid[contains(@attr-name,"xgc-program-grid")]//table//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndexMain + ']');
                    await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
                        await element(locColumn).getText().then(async function (_text) {
                            console.log("main Text " + _text);
                            if (_text.trim() == maintexttoMatch) {
                                flag = true
                                rowIndexG = rowIndex;
                            }
                        });
                    });
                }).then(async function () {
                    console.log("Flag  " + flag);
                    if (flag == true) {
                        await browser.sleep(1000);
                        let locColumntoclick = by.xpath('//xg-grid[contains(@attr-name,"xgc-program-grid")]//table//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndexG + ']//td[contains(@class,"ui-resizable-column")][' + clickColumnIndex + ']//a[contains(text(),"Link")]');
                        await action.MouseMoveToElement(locColumntoclick, "Mouse Move to Link ");
                        await action.Click(locColumntoclick, "click Link");
                    }
                    else {
                        await action.ReportSubStep("clickToggaleProgramsTracksPAVTable", "click program link " + maintexttoMatch, "Fail");
                    }
                });
            });

        }
        catch (err) {
            await action.ReportSubStep("clickToggaleProgramsTracksPAVTable", "click program link " + maintexttoMatch, "Fail");
        }

    }
    /**
       * desc: enter Text In Global Filter
       * @method enterTextInGlobalFilter
       * @author: vikas
       * @param :{}
       * @return none
       */
    async enterTextInGlobalFilter(textToenter: string) {

        try {

            await action.MouseMoveToElement(this.globalFilter, "Global Filter");
            await action.ClearText(this.globalFilter, "GlobalFilter Clear");
            await action.SetText(this.globalFilter, textToenter, "Global Filter")

        }
        catch (err) {
            await action.ReportSubStep("enterTextInGlobalFilter", "Serach In Global filter", "Fail");
        }
    }


    /**
      * desc: 
      * @method SelectChannelsOptions
      * @author: 
      * @param :{}
      * @return 
      */
    async SelectChannelsOptions(optionNames: Array<any>) {
        try {
            let elementStatus: boolean;
            await browser.sleep(1000);
            await action.Click(this.channels, "Channels");
            await browser.sleep(1000);
            for (let index = 0; index < optionNames.length; index++) {
                elementStatus = await verify.verifyElementVisible(by.xpath(this.channelsOptions.replace("dynamic", String(optionNames[index]).trim())));
                if (elementStatus) {
                    await action.Click(by.xpath(this.channelsOptions.replace("dynamic", String(optionNames[index]).trim())), optionNames[index]);
                    await browser.sleep(1000);
                    await verify.verifyElementIsDisplayed(by.xpath(this.channelsOptionsSelected.replace("dynamic", String(optionNames[index]).trim())), optionNames[index]);
                }
                else {
                    console.log(this.channelsOptions.replace("dynamic", String(optionNames[index]).trim()) + " element is not displayed.")
                }
            }
            await action.Click(this.multiSelectCloseOption, "");
            await browser.sleep(1000);
        }
        catch (err) {
            action.ReportSubStep("SelectChannelsOptions", "Error in  function", "Fail");
        }
    }
    /**
     * desc: It is used to unselect the options from Channels drodopwn 
     * @method unSelectChannelsOptions
     * @author: Venkata Sivakumar
     * @param :{optionNames: Array<any>}
     * @return 
     */
    async unSelectChannelsOptions(optionNames: Array<any>) {
        try {
            let elementStatus: boolean;
            await browser.sleep(1000);
            await action.Click(this.channels, "Channels");
            await browser.sleep(1000);
            for (let index = 0; index < optionNames.length; index++) {
                elementStatus = await verify.verifyElementVisible(by.xpath(this.channelsOptionsSelected.replace("dynamic", String(optionNames[index]).trim())));
                if (elementStatus) {
                    await action.Click(by.xpath(this.channelsOptionsSelected.replace("dynamic", String(optionNames[index]).trim())), optionNames[index]);
                    await browser.sleep(1000);
                    await verify.verifyElementIsDisplayed(by.xpath(this.channelsOptions.replace("dynamic", String(optionNames[index]).trim())), "Verify " + optionNames[index] + " is unselected");
                }
                else {
                    console.log(this.channelsOptions.replace("dynamic", String(optionNames[index]).trim()) + " element is not displayed.")
                }
            }
            await action.Click(this.multiSelectCloseOption, "");
            await browser.sleep(1000);
        }
        catch (err) {
            action.ReportSubStep("unSelectChannelsOptions", "Error in  function", "Fail");
        }
    }

    /**
     * desc: 
     * @method SelectDaypartsOptions
     * @author: vikas
     * @param :{}
     * @return 
     */
    async SelectDaypartsOptions(optionNames: Array<any>) {
        try {
            let elementStatus: boolean;
            await browser.sleep(1000);
            await action.Click(this.dayParts, "Dayparts");
            await browser.sleep(1000);
            for (let index = 0; index < optionNames.length; index++) {
                elementStatus = await verify.verifyElementVisible(by.xpath(this.dayPartsOptions.replace("dynamic", String(optionNames[index]).trim())));
                if (elementStatus) {
                    await action.Click(by.xpath(this.dayPartsOptions.replace("dynamic", String(optionNames[index]).trim())), optionNames[index]);
                    await browser.sleep(1000);
                    await verify.verifyElementIsDisplayed(by.xpath(this.dayPartsOptionsSelected.replace("dynamic", String(optionNames[index]).trim())), "Verify " + optionNames[index] + " is unselected");
                }
                else {
                    console.log(this.dayPartsOptions.replace("dynamic", String(optionNames[index]).trim()) + " element is not displayed.")
                }
            }
            await action.Click(this.multiSelectCloseOption, "");
            await browser.sleep(1000);
        }
        catch (err) {
            action.ReportSubStep("SelectDaypartsOptions", "Error in  function", "Fail");
        }
    }

    /**
      * desc: Used to unslecte the options from the dayparts dropdown
      * @method unSelectDaypartsOptions
      * @author: vikas
      * @param :{}
      * @return 
      */
    async unSelectDaypartsOptions(optionNames: Array<any>) {
        try {
            let elementStatus: boolean;
            await browser.sleep(1000);
            await action.Click(this.dayParts, "Dayparts");
            await browser.sleep(1000);
            for (let index = 0; index < optionNames.length; index++) {
                elementStatus = await verify.verifyElementVisible(by.xpath(this.dayPartsOptionsSelected.replace("dynamic", String(optionNames[index]).trim())));
                if (elementStatus) {
                    await action.Click(by.xpath(this.dayPartsOptionsSelected.replace("dynamic", String(optionNames[index]).trim())), optionNames[index]);
                    await browser.sleep(1000);
                    await verify.verifyElementIsDisplayed(by.xpath(this.dayPartsOptions.replace("dynamic", String(optionNames[index]).trim())), "Verify " + optionNames[index] + " is unselected");
                }
                else {
                    console.log(this.dayPartsOptions.replace("dynamic", String(optionNames[index]).trim()) + " element is not displayed.")
                }
            }
            await action.Click(this.multiSelectCloseOption, "");
            await browser.sleep(1000);
        }
        catch (err) {
            action.ReportSubStep("unSelectDaypartsOptions", "Error in  function", "Fail");
        }
    }


    /**
       * desc: Used to select the Active Check box
       * @method clickOnActiveCheckBox
       * @author: Venkata Sivakumar
       * @param :{}
       * @return none
       */
    async clickOnActiveCheckBox() {
        try {
            let elementStatus: boolean = await verify.verifyElementVisible(this.activeCheckBox);
            if (elementStatus) {
                await action.Click(this.activeCheckBox, "Click on Active checkbox to select");
                await verify.verifyElementIsDisplayed(this.activeCheckBoxSelect, "Verify Active check box is selected");
            }
        } catch (error) {
            action.ReportSubStep("clickOnActiveCheckBox", "Error in  function", "Fail");
        }
    }
    /**
           * desc: Used to add local program and it returns athe added sellingtitle value
           * @method addLocalProgram
           * @author: Venkata Sivakumar
           * @param :{channelsOptions:Array<string>,dayPartsOptions:Array<string>,days:Array<string>}
           * @return sellingTitleValue
           */
    async addLocalProgram(channelsOptions: Array<string>, dayPartsOptions: Array<string>, days: Array<string>) {
        try {
            let currentDate = gUtils.getcurrentDate();
            let endDate = gUtils.getcurrentDateAfterSomeDays(10);
            let sellingTitleValue = "AT_" + new Date().getTime();
            await browser.sleep(5000);
            await this.clickonButtonsRatecardPage("xgc-add-program-orbit-button", "Add");
            // await this.clickDropdownOptionInventory("Add Local Program");
            await this.clickDropdownOptionInventory("Local Program");
            await this.clickMultiselectDropDownInventoryAddProgram("channel");
            for (let index = 0; index < channelsOptions.length; index++) {
                await this.clickMultiSelectOptionInventoryAddProgram(channelsOptions[index]);
            }
            await this.clickDescriptionInputInventory();
            await this.enterTextInInputInventoryAddProgram("xgc-program-name", sellingTitleValue);
            await this.clickMultiselectDropDownInventoryAddProgram("daypart");
            for (let index = 0; index < dayPartsOptions.length; index++) {
                await this.clickMultiSelectOptionInventoryAddProgram(dayPartsOptions[index]);
            }
            for (let index = 0; index < days.length; index++) {
                await this.clickDaysOptionInventorydialog(days[index]);
            }
            await this.clickClockIconTimeOptionInventorydialog("xgc-starttime-time-picker");
            await this.clickTimeInInventorydialogTimetable("01:00");
            await this.clickTimeInInventorydialogTimetable("01:00");
            await this.clickClockIconTimeOptionInventorydialog("xgc-endtime-time-picker");
            await this.clickTimeInInventorydialogTimetable("04:00");
            await this.clickTimeInInventorydialogTimetable("04:10");
            await this.enterDateInInputInventoryAddProgram("xgc-telecast-startdate-date-picker", currentDate);
            await this.enterDateInInputInventoryAddProgram("xgc-telecast-enddate-date-picker", endDate);
            await this.clickonButtonsRatecardPage("xgc-save-local-program", "Save");
            await browser.sleep(1000);
            await this.verifyPopupMessageRatecard("Successfully added");
            return sellingTitleValue;
        } catch (error) {
            action.ReportSubStep("addLocalProgram", "Error in  function", "Fail");
        }
    }


    /**
* desc: click buttons in Ratecard Page
* @method clickonButtonsRatecardPage1
* @author: vikas
* @param :{}
* @return none
*/
    async clickonButtonsRatecardPage1(buttonName: string) {

        try {
            let Xpbutton = by.xpath('//button[normalize-space(text())="' + buttonName + '" and not(@disabled)]');
            await action.MouseMoveToElement(Xpbutton, buttonName)
            await action.Click(Xpbutton, buttonName);
        }
        catch (err) {
            throw new Error(err);
        }
    }


    /**
   * desc: click buttons in Ratecard Page
   * @method clickClockIconTimeOptionOrbitDetails
   * @author: dhana
   * @param :{}
   * @return none
   */
    async clickClockIconTimeOptionOrbitDetails(labelName: string) {

        try {
            let locator = by.css('xg-time-picker[ng-reflect-name*="' + labelName + '"] i[class*="fa fa-clock"]');
            await action.Click(locator, labelName + " clock icon");
        }
        catch (err) {
            throw new Error(err);
        }
    }



    /**
     * desc: To Verify ColumnName is not Displayed In Grid
     * @methodColumnNameNotDisplayedValidations
     * @author: Siva
     * @param :{}
     * @return none
     */
    async clearTextBox(locator) {
        let keys = protractor.Key;
        let data: string = "(P18+2345677"
        await browser.sleep(500)
        for (let i = 0; i < data.length; i++) {
            await element(locator).sendKeys(keys.BACK_SPACE);
            await browser.sleep(500)
        }
    }


  /**
     * desc: select Row From Rate Card using index
     * @method selectRowFromRateCardViewTableByIndex
     * @author: dhana
     * @param :{}
     * @return none
     */
    async selectRowFromRateCardViewTableByIndex(rowIndex: number) {
        try {
           let XpTableRow = by.css('tbody tr[class*="ng-star-inserted"]:nth-of-type(' + rowIndex + ') div[class*="ui-chkbox-box"]');
         
           await action.MouseMoveToElement(XpTableRow, "RowIndex " + rowIndex);
           await action.Click(XpTableRow, "RowIndex " + rowIndex);
      
        }
        catch (err) {
           throw new Error(err);
        }
    
     } 
        /**
          * desc: verify Clone Poup Inventory
          * @method verifyClonePoupInventory
          * @author: dhana
          * @param :{}
          * @return none
          */
         async verifyClonePoupInventory() {
    
            try {
               let locator = by.css('p-dialog div[class*="ui-dialog-titlebar"] span');
           
               await verify.verifyTextInElement(locator, "Clone Program", "Clone Popup");
          
            }
            catch (err) {
               throw new Error(err);
            }
         } 
            /**
         * desc: verify selling Title In Inventory Dialog
         * @method verifysellingTitleInInventoryDialog
         * @author: dhana
         * @param :{}
         * @return none
         */
       async verifysellingTitleInInventoryDialog() {
    
        try {
           let locator = by.xpath('//p-dialog//input[contains(@placeholder,"Selling Title") or contains(@placeholder,"sellingTitle")]');
      
           await verify.verifyElementIsDisplayed(locator, "sellingTitle");
        
        }
        catch (err) {
           throw new Error(err);
        }
     }
        /**
          * desc: verify Buttons Inventory Dialog
          * @method verifyButtonsInventoryDialog
          * @author: dhana
          * @param :{}
          * @return none
          */
         async verifyButtonsInventoryDialog(buttonName: string) {
    
            try {
               let locator = by.css('p-dialog button[title*="' + buttonName + '"]');
          
               await verify.verifyElementIsDisplayed(locator, buttonName);
          
            }
            catch (err) {
               throw new Error(err);
            }
         } 
           /**
          * desc: enter Text Inselling Title In Inventory Dialog
          * @method enterTextInsellingTitleInInventoryDialog
          * @author: dhana
          * @param :{}
          * @return none
          */
       async enterTextInsellingTitleInInventoryDialog(textToenter: string) {
    
        try {
           let locator = by.xpath('//p-dialog//input[contains(@placeholder,"Selling Title") or contains(@placeholder,"sellingTitle")]');
    
           await action.SetText(locator, textToenter, "sellingTitle");
       
        }
        catch (err) {
           throw new Error(err);
        }
     }
      /**
         * desc: click ButtonsInventoryDialog
         * @method clickButtonsInventoryDialog
         * @author: dhana
         * @param :{}
         * @return none
         */
        async clickButtonsInventoryDialog(buttonName: string) {
    
            try {
               let locator = by.css('p-dialog button[title*="' + buttonName + '"]');
         
               await action.Click(locator, buttonName);
         
            }
            catch (err) {
               throw new Error(err);
            }
         }
            /**
        * desc: verifydataInRateCardViewTableusingRateCard
        * @method verifydataInRateCardViewTableusingRateCard
        * @author: dhana
        * @param :{}
        * @return none
        */
       async verifydataInRateCardViewTableusingRateCard(columnIndex: number, textToverify: string, ratecardName: string) {
        try {
           let locTableRow = by.css('tbody tr[class*="ng-star-inserted"]');
           let flag = false;
       
           await browser.sleep(1000);
           await element.all(locTableRow).each(async function (ele, Index) {
              let rowIndex = Index + 1
              let locColumn = by.css('tbody tr[class*="ng-star-inserted"]:nth-of-type(' + rowIndex + ') td[class*="ui-resizable-column"]:nth-of-type(3)');
              await browser.actions().mouseMove(element(locColumn)).perform();
              let _text = String(await action.GetText(locColumn, "Index" + rowIndex));
              console.log("Value ++RC"+_text);
              if (_text.trim() == ratecardName) {
                 let locColumnReq = by.css('tbody tr[class*="ng-star-inserted"]:nth-of-type(' + rowIndex + ') td[class*="ui-resizable-column"]:nth-of-type(' + columnIndex + ')');
                 await browser.actions().mouseMove(element(locColumnReq)).perform();
                 await element(locColumnReq).getText().then(function (_value) {
                    console.log("Value ++"+_value);
                    console.log("EX value ++"+textToverify);
                    if (_value.trim() == textToverify) {
                       flag = true
                    }
    
                 })
    
    
    
              }
           }).then(function () {
              if (flag == true) {
                 action.ReportSubStep("verifydataInRateCardViewTableusingRateCard", "verify data" + flag, "Pass");
              }
              else {
                 action.ReportSubStep("verifydataInRateCardViewTableusingRateCard", "verify data" + flag, "Fail");
              }
           });
       
        }
        catch (err) {
           throw new Error(err);
        }
    
     }
     /**
        * desc: verify buttons is disable
        * @method verifyButtonisDisable
        * @author: dhana
        * @param :{}
        * @return none
        */
       async verifyButtonisDisable(buttonName: string) {
    
        try {
           let locbutton = by.css('button');
           let eleitem;
      
           eleitem = await action.makeDynamicLocatorContainsText(locbutton, buttonName, "Equal");
           await verify.verifyElementIsDisabled(eleitem, buttonName);
      
        }
        catch (err) {
           throw new Error(err);
        }
     }
      /**
        * desc: verify clickDeleteButton
        * @method clickDeleteButton
        * @author: dhana
        * @param :{buttonName}
        * @return none
        */
     async clickDeleteButton(buttonName: string) {
        try {
           //await browser.waitForAngular();
           
           await action.Click(this.delete, buttonName);
        }
        catch (err) {
           throw new Error(err);
        }
     } 
       /**
        * desc: verify GetTextFromElement
        * @method GetTextFromElement
        * @author: dhana
        * @param :{ElementName,LabelName}
        * @return none
        */
     async GetTextFromElement(ElementName, LabelName: string) {
    
        try {
           //await browser.waitForAngular();
           await browser.sleep(500);
           await verify.verifyElement(ElementName, LabelName);
           return await element(ElementName).getText();
        }
    
        catch (err) {
           action.ReportSubStep("GetTextFromElement", "Element Text Retrieved", "Fail");
           throw new Error(err);
        }
    
     } 
     /**
     * desc: To verify Pop-Up Header
     * @method verifyPopUpHeader
     * @author: Adithya K
     * @param :{headerText= headerText to be verified}
     * @return none
     */
    async verifyPopUpHeader(headerText) {
        try {
        await browser.waitForAngular();
        await verify.verifyElement(this.popupheader, headerText)
        await verify.verifyTextOfElement(this.popupheader, headerText, headerText)
        }
        catch (error) {
        throw new Error(error);
        }
        } 
         /**
     * desc: To verify clickCancelButton
     * @method clickCancelButton
     * @author: dhana
     * @param :{button}
     * @return none
     */
        async clickCancelButton(buttonName: string) {
            try {
               //await browser.waitForAngular();
               
               await action.Click(this.cancelinpopup, buttonName);
            }
            catch (err) {
               throw new Error(err);
            }
         }
          /**
     * desc: To verify clickdeleteinpopup
     * @method clickdeleteinpopup
     * @author: dhana
     * @param :{buttonName}
     * @return none
     */
         async clickdeleteinpopup(buttonName: string) {
            try {
               //await browser.waitForAngular();
               
               await action.Click(this.deleteinpopup, buttonName);
            }
            catch (err) {
               throw new Error(err);
            }
         } 
 
         /**
 * desc: enter text in Input Inventory Add Program using Label
 * @method enterTextInInputInventoryAddProgramUsingLabel
 * @author: vikas
 * @param :{}
 * @return none
 */
 async enterTextInInputInventoryAddProgramUsingLabel(labelName: string, textToenter) {
    try {
        let locator = by.xpath('//xg-input[contains(@labelname,"'+labelName+'")]//input');
        
        await verify.verifyElementIsEnabled(locator, labelName);
        await browser.sleep(2000);
        await element(locator).sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "a"));
        await browser.sleep(1000);
        await element(locator).sendKeys(protractor.Key.BACK_SPACE);
        await browser.sleep(1000);
        await action.SetText(locator, textToenter, labelName);
    }
    catch (err) {
        action.ReportSubStep("enterTextInInputInventoryAddProgram", "Error in  function", "Fail");
    }


   }
}



