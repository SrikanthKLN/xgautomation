import { browser, element, by, protractor, promise } from 'protractor';
import { ActionLib } from '../../Libs/GeneralLibs/ActionLib';
import { VerifyLib } from '../../Libs/GeneralLibs/VerificationLib';
import { Gutils } from '../../Utility/gutils';

let gUtils = new Gutils();
let action = new ActionLib();
let verify = new VerifyLib();

export class NetworkProgramming {

   descriptionLocator = by.css('xg-textarea[attr-name*="xgc-description"] textarea');
   globalFilterNetwork = by.css('xg-grid[attr-name*="xgc-display-network-programs"] xg-grid-toolbar input[placeholder*="Global Filter"]');
   networkProgram = '//xg-grid[contains(@attr-name,"xgc-display-network-programs")]//table//tbody//tr//td[contains(.,"dynamic")]'
   gridTable = by.css("div[class='xg-grid xg-frozen-grid'] div[class*='ui-table ui-widget']");
   firstRowName = by.css("div[class='xg-grid xg-frozen-grid'] table[class*='body-table']>tbody>tr:nth-child(1)>td:nth-child(1) span[class*='xg-inline-block']");
   firstRowMetrics = by.css("div[class='xg-grid xg-frozen-grid'] table[class*='body-table']>tbody>tr:nth-child(1)>td:nth-child(6) search-text-highlighter span[class='ng-star-inserted']");
   firstRowRatings = by.css("div[class*='ui-table-unfrozen-view'] table[class*='scrollable']>tbody>tr:nth-child(1)>td:nth-child(1)");
   /**
    * desc: enter Text In Description Network
    * @method enterTextInDescriptionNetwork
    * @author: vikas
    * @param :{}
    * @return none
    */
   async enterTextInDescriptionNetwork(textToenter) {
      try {
         await action.MouseMoveToElement(this.descriptionLocator, "Description");
         await action.SetText(this.descriptionLocator, textToenter, "Description");
      }
      catch (err) {
         action.ReportSubStep("enterTextInDescriptionNetwork", "Error in  function", "Fail");
      }

   }
   /**
     * desc: clickButtonXgDetailsToggleExpendedCollepsedNeworkChildRecord
     * @method clickButtonXgDetailsToggleExpendedCollepsedNeworkChildRecord
     * @author: vikas
     * @param :{}
     * @return none
     */
   async clickButtonXgDetailsToggleExpendedCollepsedNeworkChildRecord(clickColumnIndex: number, columnMainName: string, maintexttoMatch: string, expendCollpesed: string) {
      try {
         let locTableRow = by.xpath('//xg-grid[contains(@attr-name,"xgc-display-network-programs")]//table//tbody//tr[contains(@class,"ng-star-inserted")]');
         let locColumns = by.xpath('//xg-grid[contains(@attr-name,"xgc-display-network-programs")]//table//thead//th[contains(@class,"ui-sortable-column")]');
         let columnIndexMain: number;

         let flag = false;
         let rowIndexG;

         await browser.sleep(2000);
         await verify.verifyProgressBarNotPresent();
         await browser.sleep(2000);
         await browser.waitForAngular();
         await element.all(locColumns).each(async function (ele, Index) {
            await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
               await ele.getText().then(function (_valuecol) {
                  if (_valuecol.trim() == columnMainName) {
                     return columnIndexMain = Index + 3;
                  }
               });
            });
         }).then(async function () {
            console.log("Main Col " + columnIndexMain);
            console.log("click Col " + clickColumnIndex);
            await element.all(locTableRow).each(async function (ele, Index) {
               let rowIndex = Index + 1
               let locColumn = by.xpath('//xg-grid[contains(@attr-name,"xgc-display-network-programs")]//table//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndexMain + ']');
               await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
                  await element(locColumn).getText().then(async function (_text) {
                     console.log("main Text " + _text);
                     if (_text.trim() == maintexttoMatch) {
                        rowIndexG = rowIndex;
                        return flag = true
                     }
                  });
               });
            }).then(async function () {
               console.log("Flag  " + flag);
               if (flag == true) {
                  await browser.sleep(1000);
                  if (expendCollpesed.toUpperCase() == "COLLAPSE") {
                     let locator = by.css('xg-grid[attr-name*="xgc-display-network-programs"] tbody tr:nth-of-type(' + rowIndexG + ') td:nth-of-type(' + clickColumnIndex + ') button span[class="mat-button-wrapper"] i[class*="fa fa-chevron-down"]');
                     await action.MouseMoveJavaScript(locator, 'COLLAPSE');
                     await action.Click(locator, 'COLLAPSE');
                  }
                  if (expendCollpesed.toUpperCase() == "EXPAND") {
                     let locator = by.css('xg-grid[attr-name*="xgc-display-network-programs"] tbody tr:nth-of-type(' + rowIndexG + ') td:nth-of-type(' + clickColumnIndex + ') button span[class="mat-button-wrapper"] i[class*="fa fa-chevron-right"]');
                     await action.MouseMoveJavaScript(locator, 'EXPAND');
                     await action.Click(locator, 'EXPAND');
                  }
               }
               else {
                  await action.ReportSubStep("clickButtonXgDetailsToggleExpendedCollepsedNeworkChildRecord", "click  expand Or Collapse network child record " + maintexttoMatch, "Fail");
               }
            });
         });

      }
      catch (err) {
         await action.ReportSubStep("clickButtonXgDetailsToggleExpendedCollepsedNeworkChildRecord", "Error In Function " + maintexttoMatch, "Fail");
      }

   }
   /**
      * desc: click Button Xg Details Toggle Expended Collepsed
      * @method clickButtonXgDetailsToggleExpendedCollepsed
      * @author: vikas
      * @param :{}
      * @return none
      */
   async clickButtonXgDetailsToggleExpendedCollepsed(row: number, column: number, expendCollpesed: string) {
      try {

         if (expendCollpesed.toUpperCase() == "COLLAPSE") {
            let locator = by.css('xg-grid[attr-name*="xgc-display-network-programs"] tbody tr[class*="ng-star-inserted"]:nth-of-type(' + row + ') td[class*="ui-resizable-column"]:nth-of-type(' + column + ') button span[class="mat-button-wrapper"] i[class*="fa fa-chevron-down"]');
            await action.MouseMoveJavaScript(locator, 'COLLAPSE');
            await action.Click(locator, 'COLLAPSE');
         }
         if (expendCollpesed.toUpperCase() == "EXPAND") {
            let locator = by.css('xg-grid[attr-name*="xgc-display-network-programs"] tbody tr[class*="ng-star-inserted"]:nth-of-type(' + row + ') td[class*="ui-resizable-column"]:nth-of-type(' + column + ') button span[class="mat-button-wrapper"] i[class*="fa fa-chevron-right"]');
            await action.MouseMoveJavaScript(locator, 'EXPAND');
            await action.Click(locator, 'EXPAND');
         }


      }
      catch (err) {
         await action.ReportSubStep("clickButtonXgDetailsToggleExpendedCollepsed", "Error In Function ", "Fail");
      }

   }
   /*
   * desc: verifyDataInChildTableNetworkPrograms
   * @method verifyDataInChildTableNetworkPrograms
   * @author: vikas
   * @param :{}
   * @return none
   */
   async verifyDataInChildTableNetworkPrograms(columnName: string, textToVerify: string) {
      try {
         let locTableRow = by.xpath('//xg-grid[contains(@attr-name,"xgc-child-network-programs")]//tbody//tr[contains(@class,"ng-star-inserted")]');
         let locColumns = by.xpath('//xg-grid[contains(@attr-name,"xgc-child-network-programs")]//th[contains(@class,"ui-sortable-column")]');
         let columnIndex: number;

         let flag = false;

         await verify.verifyProgressBarNotPresent();
         await browser.waitForAngular();
         await element.all(locColumns).each(async function (ele, Index) {
            await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
               await ele.getText().then(function (_valuecol) {
                  if (_valuecol.trim() == columnName) {
                     console.log("Col Name " + _valuecol);
                     return columnIndex = Index + 1;
                  }
               });
            });
         }).then(async function () {
            console.log("ColumnIndex" + columnIndex);

            await element.all(locTableRow).each(async function (ele, Index) {
               let rowIndex = Index + 1
               let locColumn = by.xpath('//xg-grid[contains(@attr-name,"xgc-child-network-programs")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndex + ']');
               await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
                  await element(locColumn).getText().then(async function (_text) {
                     console.log("Main Text" + _text)
                     if (_text.trim().indexOf(textToVerify) > -1) {

                        return flag = true;
                     }
                  });
               });
            }).then(async function () {
               if (flag == true) {

                  await action.ReportSubStep("verifyDataInChildTableNetworkPrograms", "verify data " + textToVerify, "Pass");
               }
               else {
                  await action.ReportSubStep("verifyDataInChildTableNetworkPrograms", "verify data " + textToVerify, "Fail");
               }
            });
         });

      }
      catch (err) {
         await action.ReportSubStep("verifyDataInChildTableNetworkPrograms", "verify data Error in function" + textToVerify, "Fail");
      }

   }

   /*
    * desc: verifyDataNotPresentInChildTableNetworkPrograms
    * @method verifyDataNotPresentInChildTableNetworkPrograms
    * @author: vikas
    * @param :{}
    * @return none
    */
   async verifyDataNotPresentInChildTableNetworkPrograms(columnName: string, textToVerify: string) {
      try {
         let locTableRow = by.xpath('//xg-grid[contains(@attr-name,"xgc-child-network-programs")]//tbody//tr[contains(@class,"ng-star-inserted")]');
         let locColumns = by.xpath('//xg-grid[contains(@attr-name,"xgc-child-network-programs")]//th[contains(@class,"ui-sortable-column")]');
         let columnIndex: number;

         let flag = false;

         await verify.verifyProgressBarNotPresent();
         await browser.waitForAngular();
         await element.all(locColumns).each(async function (ele, Index) {
            await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
               await ele.getText().then(function (_valuecol) {
                  if (_valuecol.trim() == columnName) {
                     console.log("Col Name " + _valuecol);
                     return columnIndex = Index + 1;
                  }
               });
            });
         }).then(async function () {
            console.log("ColumnIndex" + columnIndex);

            await element.all(locTableRow).each(async function (ele, Index) {
               let rowIndex = Index + 1
               let locColumn = by.xpath('//xg-grid[contains(@attr-name,"xgc-child-network-programs")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndex + ']');
               await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
                  await element(locColumn).getText().then(async function (_text) {
                     console.log("Main Text" + _text)
                     if (_text.trim().indexOf(textToVerify) > -1) {

                        return flag = true;
                     }
                  });
               });
            }).then(async function () {
               if (flag == true) {

                  await action.ReportSubStep("verifyDataNotPresentInChildTableNetworkPrograms", "verify data not present" + textToVerify, "Fail");
               }
               else {
                  await action.ReportSubStep("verifyDataNotPresentInChildTableNetworkPrograms", "verify data not present " + textToVerify, "Pass");
               }
            });
         });

      }
      catch (err) {
         await action.ReportSubStep("verifyDataNotPresentInChildTableNetworkPrograms", "verify data Error in function" + textToVerify, "Fail");
      }

   }
   /*
    * desc: verifyActiveInactiveRecordInChildTableNetworkPrograms
    * @method verifyActiveInactiveRecordInChildTableNetworkPrograms
    * @author: vikas
    * @param :{}
    * @return none
    */
   async verifyActiveInactiveRecordInChildTableNetworkPrograms(columnName: string, textToVerify: string, activeOrInactive: string) {
      try {
         let locTableRow = by.xpath('//xg-grid[contains(@attr-name,"xgc-child-network-programs")]//tbody//tr[contains(@class,"ng-star-inserted")]');
         let locColumns = by.xpath('//xg-grid[contains(@attr-name,"xgc-child-network-programs")]//th[contains(@class,"ui-sortable-column")]');
         let columnIndex: number;
         let rowIndexG;
         let flag = false;


         await verify.verifyProgressBarNotPresent();

         await browser.waitForAngular();
         await element.all(locColumns).each(async function (ele, Index) {
            await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
               await ele.getText().then(function (_valuecol) {
                  if (_valuecol.trim() == columnName) {
                     console.log("Col Name " + _valuecol);
                     return columnIndex = Index + 1;
                  }
               });
            });
         }).then(async function () {
            console.log("ColumnIndex" + columnIndex);

            await element.all(locTableRow).each(async function (ele, Index) {
               let rowIndex = Index + 1
               let locColumn = by.xpath('//xg-grid[contains(@attr-name,"xgc-child-network-programs")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndex + ']');
               await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
                  await element(locColumn).getText().then(async function (_text) {
                     console.log("Main Text" + _text)
                     if (_text.trim().indexOf(textToVerify) > -1) {
                        rowIndexG = rowIndex;
                        return flag = true;
                     }
                  });
               });
            }).then(async function () {
               if (flag == true) {

                  if (activeOrInactive.toLocaleUpperCase() == "ACTIVE") {
                     let locColumnactiveRec = by.xpath('//xg-grid[contains(@attr-name,"xgc-child-network-programs")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndexG + ']//td[contains(@class,"ui-resizable-column")][1]//div[contains(@role,"checkbox")][contains(@aria-checked,"true")]');
                     await verify.verifyElement(locColumnactiveRec, "Active Record");
                  }
                  else {
                     let locColumnactiveRec = by.xpath('//xg-grid[contains(@attr-name,"xgc-child-network-programs")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndexG + ']//td[contains(@class,"ui-resizable-column")][1]//div[contains(@role,"checkbox")][contains(@aria-checked,"false")]');
                     await verify.verifyElement(locColumnactiveRec, "In active  Record");
                  }

               }
               else {
                  await action.ReportSubStep("verifyActiveInactiveRecordInChildTableNetworkPrograms", "verify data " + textToVerify, "Fail");
               }
            });
         });

      }
      catch (err) {
         await action.ReportSubStep("verifyActiveInactiveRecordInChildTableNetworkPrograms", "verify data Error in function" + textToVerify, "Fail");
      }

   }
   /**
       * desc: enter Text In Global Filter Network Program
       * @method enterTextInGlobalFilterNetworkProgram
       * @author: vikas
       * @param :{}
       * @return none
       */
   async enterTextInGlobalFilterNetworkProgram(textToenter: string) {

      try {
         await action.MouseMoveToElement(this.globalFilterNetwork, "Global Filter");
         await action.SetText(this.globalFilterNetwork, textToenter, "Global Filter")
      }
      catch (err) {
         await action.ReportSubStep("enterTextInGlobalFilterNetworkProgram", "Serach In Global filter , Error In function", "Fail");
      }
   }

   /*
  * desc: returnDataInChildTableNetworkPrograms
  * @method returnDataInChildTableNetworkPrograms
  * @author: vikas
  * @param :{}
  * @return none
  */
   async returnDataInChildTableNetworkPrograms(rowIndex: number) {
      try {

         let locColumns = by.xpath('//xg-grid[contains(@attr-name,"xgc-child-network-programs")]//th[contains(@class,"ui-sortable-column")]');
         let allRecord = {};
         await browser.sleep(2000);
         await verify.verifyProgressBarNotPresent();
         await browser.sleep(2000);
         await browser.waitForAngular();
         await element.all(locColumns).each(async function (ele, Index) {
            await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
               await ele.getText().then(async function (_valuecol) {
                  let columnIndex = Index + 1;
                  let locColumn = by.xpath('//xg-grid[contains(@attr-name,"xgc-child-network-programs")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndex + ']');
                  await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
                     await element(locColumn).getText().then(async function (_text) {
                        if ((typeof (_text)) != undefined && (_text != "")) {
                           console.log("column return Data" + _valuecol)
                           console.log("Main Text return Data" + _text);
                           allRecord[_valuecol.trim()] = _text.trim();
                           console.log("Market ++ " + allRecord[_valuecol.trim()]);
                        }


                     }, function (err) { });
                  });

               });
            });
         }).then(function () {

         })
         console.log("Market ++ " + allRecord["Market"]);
         return allRecord;
      }
      catch (err) {
         await action.ReportSubStep("returnDataInChildTableNetworkPrograms", "retrun value from child table", "Fail");
      }

   }

   /*
   * desc: returnRowCountInChildTableNetworkPrograms
   * @method returnRowCountInChildTableNetworkPrograms
   * @author: vikas
   * @param :{}
   * @return none
   */
   async returnRowCountInChildTableNetworkPrograms() {
      try {
         let locTableRow = by.xpath('//xg-grid[contains(@attr-name,"xgc-child-network-programs")]//tbody//tr[contains(@class,"ng-star-inserted")]');

         await browser.sleep(2000);
         await verify.verifyProgressBarNotPresent();
         await browser.sleep(2000);
         await browser.waitForAngular();
         let rowCount: number = Number(await element.all(locTableRow).count());
         console.log("row Count" + rowCount);
         return rowCount;
      }
      catch (err) {
         await action.ReportSubStep("returnRowCountInChildTableNetworkPrograms", "Return function Error ", "Fail");
      }

   }
   /*
     * desc: makeActiveInactiveRecordInChildTableNetworkPrograms
     * @method makeActiveInactiveRecordInChildTableNetworkPrograms
     * @author: vikas
     * @param :{}
     * @return none
     */
   async makeActiveInactiveRecordInChildTableNetworkPrograms(columnIndex: number, activeOrInactive: string) {
      try {
         let locTableRow = by.xpath('//xg-grid[contains(@attr-name,"xgc-child-network-programs")]//tbody//tr[contains(@class,"ng-star-inserted")]');

         await verify.verifyProgressBarNotPresent();
         await browser.waitForAngular();
         await element.all(locTableRow).each(async function (ele, Index) {
            let rowIndex = Index + 1
            let locColumnactiveRec = by.xpath('//xg-grid[contains(@attr-name,"xgc-child-network-programs")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndex + ']//div[contains(@role,"checkbox")]');

            if (activeOrInactive.toLocaleUpperCase() == "ACTIVE") {
               await action.MouseMoveToElement(locColumnactiveRec, "Record Child");
               let attrValue = String(await action.getElementAttribute(locColumnactiveRec, 'aria-checked', "col Child Record"));
               if (attrValue.trim() == "false") {

                  await action.Click(locColumnactiveRec, "Record Child");
               }
            }
            else {
               await action.MouseMoveToElement(locColumnactiveRec, "Record Child");
               let attrValue = String(await action.getElementAttribute(locColumnactiveRec, 'aria-checked', "col Child Record"));
               if (attrValue.trim() == "true") {

                  await action.Click(locColumnactiveRec, "Record Child");
               }
            }
         });


      }
      catch (err) {
         await action.ReportSubStep("makeActiveInactiveRecordInChildTableNetworkPrograms", "Make Active Inactive Error in function", "Fail");
      }

   }

   /**
   * desc: click single select drop down in Popup
   * @method clickComponentPdropdownInPopup
   * @author: vikas
   * @param :{}
   * @return none
   */
   async clickComponentPdropdownInPopup(dropdownAttrName: string) {

      try {
         let locator = by.css('xg-popup xg-dropdown[attr-name*="' + dropdownAttrName + '"] p-dropdown[class*="xg-dropdown"] label');
         await action.MouseMoveToElement(locator, dropdownAttrName);
         await action.Click(locator, dropdownAttrName + " DropDown");
      }
      catch (err) {
         await action.ReportSubStep("clickComponentPdropdown", "click on drop down " + dropdownAttrName, "Fail");
      }
   }

   /**
     * desc: select options from Component Pdropdown
     * @method selectOptionsFromComponentPdropdown
     * @author: vikas
     * @param :{}
     * @return none
     */
   async selectOptionsFromComponentPdropdown(optionText: string) {

      try {
         let locator = by.xpath('//ul[contains(@class,"ui-dropdown-items")]//li[contains(@class,"ui-dropdown-item")]//span[normalize-space(text())="' + optionText + '"]');
         await action.MouseMoveToElement(locator, optionText);
         await action.Click(locator, optionText + " DropDown");

      }
      catch (err) {
         await action.ReportSubStep("selectOptionsFromComponentPdropdown", "click on drop down option " + optionText, "Fail");
      }
   }

   /**
              * desc: select all options from Multi Select DropDown
              * @method selectAllOptionsFromMultiSelectDropDown
              * @author: vikas
              * @param :{}
              * @return none
              */
   async selectAllOptionsFromMultiSelectDropDown() {

      try {
         let locator = by.xpath('//p-multiselectitem//li[contains(@class,"ui-multiselect-item")]//div[contains(@class,"ui-chkbox-box")]');
         await browser.sleep(2000);
         await element.all(locator).each(async function (ele, Index) {
            await browser.actions().mouseMove(ele).perform().then(async function () {
               await action.Click(ele, "Option " + Index);
            });

         });

      }
      catch (err) {
         await action.ReportSubStep("clickMultiSelectAllOptions", "click select all from drop down", "Fail");
      }
   }

   /**
* desc: click Days Option Short Cut ActiveInactive
* @method clickDaysOptionShortCutActiveInactive
* @author: vikas
* @param :{}
* @return none
*/
   async clickDaysOptionShortCutActiveInactive(attrinuteName: string, activeInactive: string, optionName: string) {
      try {
         let loactor;
         if (activeInactive.toUpperCase() == "ACTIVE") {
            loactor = by.xpath('//xg-day-picker[contains(@attr-name,"' + attrinuteName + '") or contains(@name,"' + attrinuteName + '")]//span//a[contains(text(),"' + optionName + '")][contains(@class,"active")]');
         }
         if (activeInactive.toUpperCase() == "INACTIVE") {
            loactor = by.xpath('//xg-day-picker[contains(@attr-name,"' + attrinuteName + '") or contains(@name,"' + attrinuteName + '")]//span//a[contains(text(),"' + optionName + '")]');
         }



         await action.Click(loactor, optionName + " and selection " + activeInactive);

      }
      catch (err) {
         await action.ReportSubStep("clickDaysOptionShortCutActiveInactive", "click on option " + optionName, "Fail");
      }

   }

   /**
 * desc: click single select drop down
 * @method clickComponentPdropdown
 * @author: vikas
 * @param :{}
 * @return none
 */
   async clickComponentPdropdown(dropdownAttrName: string) {

      try {
         let locator = by.css('xg-dropdown[attr-name*="' + dropdownAttrName + '"] p-dropdown[class*="xg-dropdown"] label');
         await action.MouseMoveToElement(locator, dropdownAttrName);
         await action.Click(locator, dropdownAttrName + " DropDown");
      }
      catch (err) {
         await action.ReportSubStep("clickComponentPdropdown", "click on drop down " + dropdownAttrName, "Fail");
      }
   }

   /**
    * desc: click fileds like Markets,Channel(s) etc with multiselect option
    * @method clickFieldWithComboboxMultiselect
    * @author: vikas
    * @param :{}
    * @return none
    */
   async clickFieldWithComboboxMultiselect(attributeName: string) {
      try {
         await browser.sleep(2000)
         let Xpmutiselect = by.xpath('//xg-multi-select[contains(@attr-name,"' + attributeName + '")]//p-multiselect');
         await action.MouseMoveToElement(Xpmutiselect, attributeName);
         await action.Click(Xpmutiselect, attributeName);

      }
      catch (err) {
         await action.ReportSubStep("clickFieldWithComboboxMultiselect", "click multiselect drop down" + attributeName, "Fail");
      }

   }

   /**
          * desc: click Multi Select All Options
          * @method clickMultiSelectAllOptions
          * @author: vikas
          * @param :{}
          * @return none
          */
   async clickMultiSelectAllOptions() {

      try {
         let locator = by.xpath('//div[contains(@class,"ui-widget-header ui-corner-all")]//div[contains(@class,"ui-chkbox-box")]');
         await browser.sleep(2000);
         await action.MouseMoveJavaScript(locator, "All Option");
         await action.Click(locator, "All Option multi select");
      }
      catch (err) {
         await action.ReportSubStep("clickMultiSelectAllOptions", "click select all from drop down", "Fail");
      }
   }


   /**
* desc: click Close Button Multi Select List Dropdown
* @method clickCloseButtonMultiSelectListDropdown
* @author: vikas
* @param :{}
* @return none
*/
   async clickCloseButtonMultiSelectListDropdown() {
      try {
         let locator = by.css('a[class*="ui-multiselect-close"] span[class*="pi pi-times"]');
         await action.MouseMoveJavaScript(locator, "x Button Multiselect");
         await action.Click(locator, 'x Button Multiselect');
      }
      catch (err) {
         await action.ReportSubStep("clickCloseButtonMultiSelectListDropdown", "click on dropdown x button", "Fail");
      }

   }

   /**
              * desc: retrun Assign PAV Ratings Programs Records PAV Table
              * @method returnAssignPAVRatingsProgramsRecordsPAVTable
              * @author: vikas
              * @param :{}
              * @return none
              */
   async returnAssignPAVRatingsProgramsRecordsPAVTable(columnMainName: string, columnToVerifyName: string, textToverify: string) {
      try {
         let locTableRow = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")]');
         let locColumns = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//th[contains(@class,"ui-sortable-column")]');
         let columnIndexMain: number;
         let columnIndex: number;
         let clickColumnIndex: number;
         let AllData = {}


         await browser.sleep(2000);
         await verify.verifyProgressBarNotPresent();
         await browser.sleep(2000);
         await browser.waitForAngular();
         await element.all(locColumns).each(async function (ele, Index) {
            await browser.executeScript('arguments[0].scrollIntoView()', ele.getWebElement()).then(async function () {
               await ele.getText().then(function (_valuecol) {
                  if (_valuecol.trim() == columnMainName) {
                     columnIndexMain = Index + 1;
                  }
                  if (_valuecol.trim() == columnToVerifyName) {
                     columnIndex = Index + 1;
                  }
               });
            });
         }).then(async function () {
            console.log("col main " + columnIndexMain);
            console.log("col col veri" + columnIndex);

            await browser.sleep(2000);
            await element.all(locTableRow).each(async function (ele, Index) {
               let rowIndex = Index + 1
               let locColumn = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndexMain + ']');
               await browser.executeScript('arguments[0].scrollIntoView()', element(locColumn).getWebElement()).then(async function () {
                  await browser.sleep(500);
                  await element(locColumn).getText().then(async function (_text) {
                     console.log("click text ++ " + _text);

                     let locColumnReq = by.xpath('//xg-grid//div[contains(@class,"ui-table-frozen-view")]//tbody//tr[contains(@class,"ng-star-inserted")][' + rowIndex + ']//td[contains(@class,"ui-resizable-column")][' + columnIndex + ']');
                     await browser.actions().mouseMove(element(locColumnReq)).perform().then(async function () {

                        await element(locColumnReq).getText().then(function (_value) {
                           if (_value.trim().indexOf(textToverify) > -1) {
                              AllData[rowIndex] = _text.trim();
                           }

                        })
                     });

                  });
               });
            })
         });
         return AllData;
      }
      catch (err) {
         await action.ReportSubStep("returnAssignPAVRatingsProgramsRecordsPAVTable", "Retrun Data", "Fail");
      }

   }
}