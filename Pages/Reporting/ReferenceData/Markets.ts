import { browser, element, by, protractor, promise } from 'protractor';
import { ActionLib } from '../../../Libs/GeneralLibs/ActionLib';



let action = new ActionLib();

export class Markets {

    
    sourceDropDownSelectedValue = by.css('div[attr-name="xgc-source"] label[class*="ui-dropdown-label"]');
    tableHeaders = by.css('div[attr-name="xgc-market-reference"] th[preorderablecolumn]');
    tableRows = by.css('div[attr-name="xgc-market-reference"] div[class*="ui-table-unfrozen-view"] tr[ng-reflect-index="0"]');
    summaryMessage = by.css('div[class*="ui-table-summary"] div');
    summarySelected = by.css('div[class*="ui-table-summary"] i');
    buttonReset = by.css('button[attr-name="xgc-market-reset-grid"]');
    dateInput = by.css('xg-grid table input[ng-reflect-name="DateInput"]');
    SearchIcon = by.css('div[attr-name="xgc-market-reference"] th:nth-child(2) div[title="Equal To"] i');
    resetButton = by.css('button[attr-name="xgc-market-reset-grid"] i');
    SelectEqualTo = by.css('ul li div[title="Less Than"] i')
    notEqualTO = by.css('ul li div[title="Not Equal"] i')
    SendID = by.css(" tr th:nth-child(2)  xg-grid-column-filter >div > div > div > input");
    selectLessthan = by.css('[class*="ui-dropdown-items" ] li:nth-child(3)')
    selectNotEqaulTo = by.css('ul li div[title="Not Equal"] i')
    selectGreathan = by.css('ul li div[title="Greater Than"] i')
    cancelIcon = by.css('th:nth-child(2)  xg-grid-column-filter  div  div >div > i');
    mKRankIcon = by.css('div[attr-name="xgc-market-reference"] th:nth-child(2) div[title="Equal To"] i');
    mKRanksendID = by.css("tr th:nth-child(2)  xg-grid-column-filter >div > div > div > input");
    mKCancelIcon = by.css('th:nth-child(2)  xg-grid-column-filter  div  div >div > i')
    

    /**
      * desc: verify Total Records Message in the grid
      * @method verifyTotalRecordsMessage
      * @author: Priyanka
      * @param :{}
      * @return none
      */
    async verifyTotalRecordsMessage() {
        try {
            var returnedSummary;
            var returnedSelections;
            var countOfRows;

            returnedSummary = await action.GetText(this.summaryMessage, "");
            returnedSelections = await action.GetText(this.summarySelected, "");
            countOfRows = await action.getWebElementsCount(this.tableRows);
            let framedSummaryText = "There are " + countOfRows + " records in this dataset.";

            if (returnedSummary.trim() == framedSummaryText.trim() && returnedSelections.trim() == "0 records selected") {
                console.log("Text is As Expected");
            }
            else if (returnedSummary.trim() != framedSummaryText.trim() && returnedSelections.trim() != "0 records selected") {
                await action.ReportSubStep("verify Table Summary Text", "Table Summary Text received is " + returnedSummary + " " + returnedSelections, "Fail");
            }
        }
        catch (err) {
            await action.ReportSubStep("verify Table Summary Text", "Table Summary Text received is " + returnedSummary + " " + returnedSelections, "Fail");
        }
    }
    /**
       * desc: verify Total Records Message in the grid
       * @method mouseMoveToHeader
       * @author: Priyanka
       * @param :{}
       * @return none
       */
    async mouseMoveToHeader(Header: String) {
        try {

            let loc = by.xpath("//tr//th[@tabindex][contains(text(),'" + Header + "')]");
            await action.MouseMoveToElement(loc, "")

        }
        catch (err) {
            console.log("Unable to Mouse move");
        }
    }
    /**
      * desc: Enter data into Date Field
      * @method enterDateIntoDateField
      * @author: Priyanka
      * @param :{}
      * @return none
      */
    async enterDateIntoDateField(text) {
        try {
            await action.ClearText(this.dateInput, "");
            await action.SetText(this.dateInput, text, " Date Field");
        }
        catch (err) {
            console.log("Unable to Mouse move");
        }
    }
    /**
       * desc: verify data into Date Field
       * @method verifyDateInDateField
       * @author: Priyanka
       * @param :{}
       * @return none
       */
    async verifyDateInDateField(text) {
        try {
            let loc = by.css('xg-grid tr td:nth-child(10)');
            await action.MouseMoveToElement(loc, "");
            await action.GetText(loc, "").then(function (_text: string) {
                console.log("Retrieved Text:" + _text);
                if (_text.indexOf(text) <= -1) {
                    action.ReportSubStep("verify Last Updated", "Last Updated Received is  " + _text, "Fail");
                }
            });
        }
        catch (err) {
            console.log("Unable to Mouse move");
        }
    }

    async sendtext(data: any) {

        try {
            await browser.driver.sleep(2000);
            await action.SetText(this.SendID, data, "channeltext");
            await browser.driver.sleep(5000)
        } catch (err) {
            throw new Error('Error');
        }
    }

    async clickongreaterthan() {

        try {

            await browser.driver.sleep(2000);
            await action.Click(this.SearchIcon, "search clicked")
            await browser.sleep(500)
            //await action.Click(this.SelectAllIcons,"clicked")

            await action.Click(this.selectGreathan, "Select nelison")
            await browser.driver.sleep(500)

        } catch (err) {

            throw new Error('Error');
        }
    }
    async clickonmKgreaterthan() {

        try {

            await browser.driver.sleep(2000);
            await action.Click(this.mKRankIcon, "search clicked")
            await browser.sleep(500)
            //await action.Click(this.SelectAllIcons,"clicked")

            await action.Click(this.selectGreathan, "Select nelison")
            await browser.driver.sleep(500)

        } catch (err) {

            throw new Error('Error');
        }
    }
    async clickonMklessthan() {

        try {

            await browser.driver.sleep(2000);
            await action.Click(this.mKRankIcon, "search clicked")
            await browser.sleep(500)
            // await action.Click(this.SelectAllIcons,"clicked")

            await action.Click(this.selectLessthan, "Select nelison")
            await browser.driver.sleep(500)

        } catch (err) {

            throw new Error('Error');
        }
    }
    
    async verifyLessthenText() {

        try {
            await element.all(by.xpath("/tr//td[contains(@class,'ui-resizable-column noTopBottomBorder')][2]")).each(async function (elementx, index) {
                //console.log("index"+index);
                let ini = index + 1;
                await element(by.xpath("//tr[contains(@class,'noTopBottomBorder')][" + ini + "]//td[2]")).getText().then(function (value) {
                    console.log(value);
                    expect(parseInt(value, 10)).toBeLessThan(120)

                })
            })
            console.log('Filtered values are lessthan given values')
        } catch (err) {
            throw new Error('Error');
        }
    }
    async verifygreaterthenText() {

        try {
            await element.all(by.xpath("//tr//td[contains(@class,'ui-resizable-column noTopBottomBorder')][2]")).each(async function (elementx, index) {
                //console.log("index"+index);
                let ini = index + 1;
                await element(by.xpath("//tr[contains(@class,'noTopBottomBorder')][" + ini + "]//td[2]")).getText().then(function (value) {
                    console.log(value);
                    expect(parseInt(value, 10)).toBeGreaterThan(120)

                })

            })
            console.log('Filtered values are greaterthan given value')
        } catch (err) {
            throw new Error('Error');
        }
    }
    async verifyNOTthenText() {

        try {
            await element.all(by.xpath("//tr//td[contains(@class,'ui-resizable-column noTopBottomBorder')][2]")).each(async function (elementx, index) {
                //console.log("index"+index);
                let ini = index + 1;
                await element(by.xpath("//tr[contains(@class,'noTopBottomBorder')][" + ini + "]//td[2]")).getText().then(function (value) {
                    console.log(value);
                    expect(parseInt(value, 10)).toBeGreaterThan(123)

                })

            })
            console.log('Filtered values are greaterthan given value')
        } catch (err) {
            throw new Error('Error');
        }
    }
    async verifyeualtoText() {

        try {
            await element.all(by.xpath("//tbody//tr[contains(@class,'ui-selectable-row')]")).each(async function (elementx, index) {
                //console.log("index"+index);
                let ini = index + 1;
                await element(by.xpath("//tbody//tr[contains(@class,'ui-selectable-row')][" + ini + "]//td[2]")).getText().then(function (value) {
                    console.log(value);
                    expect(parseInt(value, 10)).toEqual(4001)

                })
                console.log('Filtered values are equalto given value')
            })
        } catch (err) {
            throw new Error('Error');
        }

    }
    async verifylessthaneualtoText() {

        try {
            await element.all(by.xpath("//tbody//tr[contains(@class,'ui-selectable-row')]")).each(async function (elementx, index) {
                //console.log("index"+index);
                let ini = index + 1;
                await element(by.xpath("//tbody//tr[contains(@class,'ui-selectable-row')][" + ini + "]//td[2]")).getText().then(function (value) {
                    console.log(value);
                    expect(parseInt(value, 10)).toBeLessThanOrEqual(4001)

                })
            })
            console.log('Filtered values are lessthan or eual to given value')
        } catch (err) {
            throw new Error('Error');
        }
    }


    async clickonequalto(){
    
        try{
       
        await browser.driver.sleep(2000);
        await action.Click(this.SearchIcon,"search clicked")
        await browser.sleep(500)
        //await action.Click(this.SelectAllIcons,"clicked")
       
        await action.Click(this.SelectEqualTo,"Select nelison")
        await browser.driver.sleep(500)
             
        } catch (err) {
             
        throw new Error('Error');
        }
    }

    async clickonNotequalto(){
    
        try{
       
        await browser.driver.sleep(2000);
        await action.Click(this.SearchIcon,"search clicked")
        await browser.sleep(500)
        //await action.Click(this.SelectAllIcons,"clicked")
       
        await action.Click(this.notEqualTO,"Select nelison")
        await browser.driver.sleep(500)
             
        } catch (err) {
             
        throw new Error('Error');
        }
    }
    async clickonMkNotequalto(){
    
        try{
       
        await browser.driver.sleep(2000);
        await action.Click(this.mKRankIcon,"search clicked")
        await browser.sleep(500)
        //await action.Click(this.SelectAllIcons,"clicked")
       
        await action.Click(this.notEqualTO,"Select nelison")
        await browser.driver.sleep(500)
             
        } catch (err) {
             
        throw new Error('Error');
        }
    }
    
    /**
    * desc: click single select drop down
    * @method clickComponentPdropdown
    * @author: vikas
    * @param :{}
    * @return none
    */
   async clickComponentPdropdown(lable: string) {

    try {
       let locator = by.css('xg-dropdown[label*="' + lable + '"] p-dropdown[class*="xg-dropdown"] label');
       await action.MouseMoveToElement(locator, lable);
       await action.Click(locator, lable + " DropDown");
    }
    catch (err) {
       await action.ReportSubStep("clickComponentPdropdown", "click on drop down " + lable, "Fail");
    }
 }
  /**
   * desc: select options from Component Pdropdown
   * @method selectOptionsFromComponentPdropdown
   * @author: vikas
   * @param :{}
   * @return none
   */
   async selectOptionsFromComponentPdropdown(optionText: string) {

      try {
         let locator = by.xpath('//ul[contains(@class,"ui-dropdown-items")]//li[contains(@class,"ui-dropdown-item")]//span[normalize-space(text())="' + optionText + '"]');
         await action.MouseMoveToElement(locator, optionText);
         await action.Click(locator, optionText + " DropDown");

      }
      catch (err) {
         await action.ReportSubStep("selectOptionsFromComponentPdropdown", "click on drop down option " + optionText, "Fail");
      }
   }
}
