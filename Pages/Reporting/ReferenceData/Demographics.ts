import { browser, element, by, protractor, promise } from 'protractor';
import { Gutils } from '../../../Utility/gutils';
import { ActionLib } from '../../../Libs/GeneralLibs/ActionLib';
import { VerifyLib } from '../../../Libs/GeneralLibs/VerificationLib';
import { Reporter } from '../../../Utility/htmlResult';

let gUtils = new Gutils();
let action = new ActionLib();
let verify = new VerifyLib();
let report = new Reporter();

export class Demographics {

   SearchIcon = by.css('th:nth-child(2) label[class*="ui-dropdown-label"]')
   filterTypeDropDown = "//table/thead/tr[contains(@class,'xg-search-filter')]/th[not(contains(@class,'checkbox'))][dynamic]/xg-grid-column-filter//p-dropdown//label/div/*";
   gridTableSearchInput = "//table/thead/tr[contains(@class,'xg-search-filter')]/th[@class='ng-star-inserted'][dynamic]//input[contains(@class,'text-box') or contains(@class,'inputtext')]";
   imgClear = by.xpath("//i[contains(@class,'circle ng-star-inserted')]");
   tableHeaders = by.xpath("//table/thead//th[@tabindex]");
   Startswith = by.css("div[title='Starts With']")
   Endswith = by.css("div[title='Ends With']")
   Contains = by.css("div[title='Contains']")
   Equals = by.css("div[title='Equals']")
   DoesnotEqual = by.css("div[title='Does not equal']")
   sourceDropdown = by.css('xg-dropdown[label*="Source"] label[class*="ui-dropdown-label"]')
   categorydropdown = by.css('xg-dropdown[label*="Category"] label[class*="ui-dropdown-label"]')
   marketdropdown = by.css('xg-dropdown[label*="Market"] label[class*="ui-dropdown-label"]')
   type = by.xpath("//th[5]/xg-grid-column-filter/div/div/div/input")


   /**
    * desc: To select Single Select DropDownOptions
    * @method selectSingleSelectDropDownOptions
    * @author: Dhana
    * @param :{Dropdown Locator, DropdownOptions Locator, Data: Option to be selected}
    * @return none
    **/
   async selectSingleSelectDropDownOptions(Dropdown, DropdownOptions, Data: string, log: string) {
      try {
         await browser.waitForAngular();
         await verify.verifyElement(Dropdown, log)
         await action.MouseMoveToElement(Dropdown, "Dropdown")
         await action.Click(Dropdown, log)
         await verify.verifyElement(DropdownOptions, "DropDown Options")
         let DropdownOptionsLocator = await action.makeDynamicLocatorContainsText(DropdownOptions, Data, "Equal")
         await action.Click(DropdownOptionsLocator, log)
      }
      catch (err) {
         action.ReportSubStep("selectSingleSelectDropDownOptions", "Requried dropdown not selected" + Data, "Fail");
         throw new Error(err);
      }
   }
   /**
   * desc: To Verify ColumnName Displayed In Grid
   * @method VerifyColumnNameDisplayedInGrid
   * @author: Dhana
   * @param :{ColumnName: Column Names to be Verified}
   * @return none
   **/
   async VerifyColumnNameDisplayedInGrid(ColumnName: Array<any>) {
      try {
         await browser.waitForAngular();
         for (let i = 0; i < ColumnName.length; i++) {
            let Component = by.xpath('//th[contains(text(),"' + ColumnName[i] + '")]');
            await browser.executeScript('arguments[0].scrollIntoView()', element(Component).getWebElement());
            await verify.verifyElementIsDisplayed(Component, ColumnName[i]);
            action.ReportSubStep("VerifyColumnNameDisplayedInGrid", "Displayed " + " " + ColumnName[i] + "", "Pass");
         }
      }
      catch (err) {
         action.ReportSubStep("VerifyColumnNameDisplayedInGrid", "Not Displayed " + ColumnName + "", "Fail");
         throw new Error(err);
      }

   }

   /**
  * desc:  to searchAndValidateValueInGridTable
  * @method searchAndValidateValueInGridTable
  * @author: Dhana
  * @param :{Header Name, Filter type}
  * @return none
  **/
   async searchAndValidateValueInGridTable(headerName: string, filterType: any, data: string, filter: string) {
      try {
         let index = await this.getTableColumnIndex(headerName);
         await action.Click(by.xpath(this.filterTypeDropDown.replace("dynamic", String(index))), "Click On " + headerName + " dropdown image in the grid");
         await action.Click(filterType, "")
         await this.enterValueInTextField(by.xpath(this.gridTableSearchInput.replace("dynamic", String(index))), headerName, data);
         await this.GridValidation(headerName.trim(), data, filter);
         await action.Click(this.imgClear, "Clear the filter");
      } catch (error) {
         throw new Error(error)
      }
   }

   /**
  * desc: To getTableColumnIndex
  * @method  getTableColumnIndex
  * @author: Dhana
  * @param :{ColumnName: Column Names to be Verified}
  * @return none
  **/
   async getTableColumnIndex(columnName: string) {
      try {

         let ElementFinder: Array<any> = await element.all(this.tableHeaders).getWebElements();
         let size = await ElementFinder.length;
         for (let index = 0; index < size; index++) {
            let actualColumnHeader = await ElementFinder[index].getText();
            if (actualColumnHeader.trim() == columnName.trim()) {
               return (Number(index) + 1);
            }
            else if (actualColumnHeader.trim() != columnName.trim() && index == (size - 1)) {
               console.log("There is no Column with name:" + columnName + " in the table");
               report.ReportStatus("Retrieve Column Index From the table", columnName.trim(), 'Fail', "Column should be present", columnName.trim() + " column is not available in the table");
            }
         }
      } catch (error) {
         console.log("Exception raised in getTableColumnIndex method:" + error);
         throw new Error(error);
      }
   }

   /**
   * desc: To enterValueInTextField
   * @method enterValueInTextField
   * @author: dhana
   * @param :{Element name, LabelName}
   * @return none
   **/

   async enterValueInTextField(ElementName, LabelName: string, data: any) {

      try {

         await browser.sleep(3000);
         await action.ClearText(ElementName, LabelName);
         await action.SetText(ElementName, data, LabelName);
      }
      catch (err) {
         throw new Error(err);
      }
   }

   /**
   * desc: To Verify GridValidation
   * @method GridValidation
   * @author: Dhana
   * @param :{ColumnName: Column Names to be Verified , Filter}
   * @return none
   **/
   async GridValidation(ColumnName: string, data: any, filter: string) {

      try {
         //await browser.waitForAngular();
         await browser.waitForAngular();
         let gridColumnIndex;
         let gridColumnHeaderLocator = by.css('xg-grid th[style*="display: table-cell"]')
         let paginator_next = by.css('a[class*="ui-paginator-next"]')
         let elementCount = await element.all(gridColumnHeaderLocator).count()
         let headerElements = await element.all(gridColumnHeaderLocator).getWebElements()
         let headerName;

         for (let index = 0; index < elementCount; index++) {
            headerName = await headerElements[index].getText();
            if (headerName.trim() == ColumnName.trim()) {
               gridColumnIndex = index + 2;
               break;
            }
            else if (headerName.trim() != ColumnName.trim() && index == (elementCount - 1)) {
               action.ReportSubStep("gridDataValidation", "Column Name not available in Grid", "Fail");
            }
         }
         await console.log("index of column is " + gridColumnIndex, "Column no is " + gridColumnIndex, "Name of column is " + headerName)
         await verify.verifyElement(by.css('xg-grid tr td:nth-child(' + gridColumnIndex + ')'), headerName)
         await action.MouseMoveToElement(by.css('xg-grid tr td:nth-child(' + gridColumnIndex + ')'), headerName)
         let gridDataCount = await element.all(by.css('xg-grid tr td:nth-child(' + gridColumnIndex + ')')).count()
         let gridDataElements = await element.all(by.css('xg-grid tr td:nth-child(' + gridColumnIndex + ')')).getWebElements()
         for (let colIndex = 0; colIndex < gridDataCount; colIndex++) {
            let text = await gridDataElements[colIndex].getText()


            // console.log(ColumnName + " Value " + text)
            if (filter.trim().replace(" ", "").replace(" ", "").toLowerCase() == "endswith") {
               if (text.toLowerCase().trim().includes(data.toLowerCase().trim())) {
                  action.ReportSubStep("GridValidations", "Data available " + text, "Pass");
               }
               else {
                  action.ReportSubStep("GridValidations", "Data Not available " + text, "Fail");
               }
            }
            else if (filter.trim().replace(" ", "").replace(" ", "").toLowerCase() == "startswith") {
               if (text.toLowerCase().trim().includes(data.toLowerCase().trim())) {
                  action.ReportSubStep("GridValidations", "Data available " + text, "Pass");
               }
               else {
                  action.ReportSubStep("GridValidations", "Data Not available " + text, "Fail");
               }
            }
            else if (filter.trim().replace(" ", "").replace(" ", "").toLowerCase() == "equals") {
               if (text.toLowerCase().trim() == (data.toLowerCase().trim())) {
                  action.ReportSubStep("GridValidations", "Data available " + text, "Pass");
               }
               else {
                  action.ReportSubStep("GridValidations", "Data Not available " + text, "Fail");
               }
            }
            else if (filter.trim().replace(" ", "").replace(" ", "").toLowerCase() == "contains") {
               if (text.toLowerCase().includes(data.toLowerCase())) {
                  action.ReportSubStep("GridValidations", "Data available " + text, "Pass");
               }
               else {
                  action.ReportSubStep("GridValidations", "Data Not available " + text, "Fail");
               }
            }
            else if (filter.trim().replace(" ", "").replace(" ", "").toLowerCase() == "doesnotcontain") {
               if (!text.toLowerCase().includes(data.toLowerCase())) {
                  action.ReportSubStep("GridValidations", "Data available " + text, "Pass");
               }
               else {
                  action.ReportSubStep("GridValidations", "Data Not available " + text, "Fail");
               }
            }
            else if (filter.trim().replace(" ", "").replace(" ", "").toLowerCase() == "doesnotequal") {
               if (text.toLowerCase().trim() != (data.toLowerCase().trim())) {
                  action.ReportSubStep("GridValidations", "Data available " + text, "Pass");
               }
               else {
                  action.ReportSubStep("GridValidations", "Data Not available " + text, "Fail");
               }

            }


         }
      }

      catch (err) {
         throw new Error('Error');
      }

   }
   /** 
        * desc: To Validate grid values in the view 
        * @method GridValidationNp
        * @author: Dhana
        * @param :{ColumnName:Column Name which is to be Validated,data:Testdata to be Entered}
        * @return none
     */
   async GridValidationsUpdated(ColumnName: string, data: string) {
      try {
         await browser.waitForAngular();
         await element.all(by.css('xg-grid th[style*="display: table-cell"]')).each(async function (value, index) {
            value.getText().then(async function (text) {
               if (text == ColumnName) {
                  console.log("index of column is " + index, "Name of column is " + text)
                  let ColNo = Number(index) + 2
                  console.log("Column no is" + ColNo)
                  await browser.sleep(1000)
                  await verify.verifyElement(by.css('xg-grid tr td:nth-child(' + ColNo + ')'), text)
                  await action.MouseMoveToElement(by.css('xg-grid tr td:nth-child(' + ColNo + ')'), text)
                  await element.all(by.css('xg-grid tr td:nth-child(' + ColNo + ')')).each(function (value) {
                     value.getText().then(function (text) {
                        console.log(ColumnName + " is " + text)
                        if (data.includes(text) || text.includes(data)) {
                           action.ReportSubStep("GridValidations", "Data available " + text, "Pass");
                        }
                        else {
                           action.ReportSubStep("GridValidations", "Data Not available " + text, "Fail");
                        }
                     })
                  })
               }

            })
         })

      }
      catch (err) {
         throw new Error('Error');
      }
   }






}  