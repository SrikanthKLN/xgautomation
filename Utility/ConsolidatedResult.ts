/**
 * Created by vsingh on 22-02-2016.
 */

import { globalvalues } from '../Utility/globalvalue';
let fs = require('fs');
export class Consolidatedclass {
    static writeStream= fs.createWriteStream("../../TestResult/CustomHtmlResult/Consolidated.html");
    static totalTestCaseExecuted=0;
    static totalTestPassed=0;
    static totalTestFailed=0;
    static totalTimeexecution=0;
    static totalTimeexecutionConverted;
    static DateHeader=new Date().toLocaleString();
    static TestCaseHolder={};
    
    static AddReport (TestCaseName,Description,Status,StratTime,EndTime,TestResultLink,Tduration,TimeRawDuration) {
        //Some action here
        Consolidatedclass.totalTestCaseExecuted=Consolidatedclass.totalTestCaseExecuted+1;
        console.log(Consolidatedclass.totalTestCaseExecuted);
        if(Status=="Fail")
        {
            Consolidatedclass.totalTestFailed=Consolidatedclass.totalTestFailed+1;
        }
        else
        {
            Consolidatedclass.totalTestPassed=Consolidatedclass.totalTestPassed+1;
        }
        
        this.totalTimeexecution = this.totalTimeexecution+parseFloat(TimeRawDuration);
        this.totalTimeexecutionConverted=globalvalues.timeConversion(this.totalTimeexecution);

        Consolidatedclass.TestCaseHolder[Consolidatedclass.totalTestCaseExecuted]=TestCaseName+"#"+Description+"#"+Status+"#"+StratTime+"#"+EndTime+"#"+TestResultLink+"#"+Tduration;

        //Html Start From Here
       
       

    }

    static genReport()
               {
                this.writeStream.write("<html>"+"\n");
                this.writeStream.write("<title> xG-Campaigns Automation Execution Summary Report </title>"+"\n");
                this.writeStream.write("<head>"+"\n");
                this.writeStream.write("<style>.MainHead { font-family: Arial; font-size: 12pt ; border-bottom: thin groove lightyellow; border-top: thin groove lightyellow; color: white; background:#C0504D;}</style>"+"\n");
                this.writeStream.write("<style> .SubHead { font-family: Arial; font-size: 10pt ; border-bottom: thin groove lightyellow; border-top: thin groove lightyellow; color: white; background:#3366FF; } </style>"+"\n");
                this.writeStream.write("<style> .ColumnHead {font-family: Arial; font-size: 10pt ; text-align: center; border-bottom: solid lightyellow 1.0pt; border-top: solid black 1.5pt; color: black; background:#BCEEDA;}</style>"+"\n");
                this.writeStream.write("<style> .FailStatus { font-family: Arial; font-size: 10pt ; border-bottom: thin groove lightyellow; border-top: thin groove lightyellow; color: Red; background:#D7D7D7; } </style>"+"\n");
                this.writeStream.write("<style> .PassStatus { font-family: Arial; font-size: 10pt ; border-bottom: thin groove lightyellow; border-top: thin groove lightyellow;color: green; background:#D7D7D7; } </style>"+"\n");
                this.writeStream.write("<style> .WarnedStatus { font-family: Arial; font-size: 10pt ; border-bottom: thin groove lightyellow; border-top: thin groove lightyellow;color: #7030A0; background:#D7D7D7; } </style>"+"\n");
                this.writeStream.write("<style> .GeneralHead { font-family: Arial; font-size: 10pt ; border-bottom: thin groove lightyellow; border-top: thin groove lightyellow;  color: black; background:#D7D7D7;} </style>"+"\n");
                this.writeStream.write("<style> .RequirementsHead { font-family: Arial; font-size: 10pt ; border-bottom: thin groove lightyellow; border-top: thin groove lightyellow;color: white; background:#4F81BD;} </style>"+"\n");
                this.writeStream.write("<style> .TestSummaryHead { font-family: Arial; font-size: 10pt ; border-bottom: thin groove lightyellow; border-top: thin groove lightyellow; color: white; background:#6388EF;} </style>"+"\n");
                this.writeStream.write("<style> .TestDetailsHead {font-family: Arial; font-size: 10pt ; border-bottom: thin groove lightyellow; border-top: thin groove lightyellow;color: white; background:#9494E0; } </style>"+"\n");
                this.writeStream.write("<style> .Hyperlink { font-family: Arial; font-size: 10pt ; border-bottom: thin groove lightyellow; border-top: thin groove lightyellow;  color: black; background:#D7D7D7;} </style>"+"\n");
                this.writeStream.write("</head>"+"\n");
                this.writeStream.write("<body>"+"\n");
                this.writeStream.write("<div style=\"background-color:#66B2FF;font-family:'Times New Roman', Times, serif;font-size:small\" >"+"\n");
                this.writeStream.write("<h1><center>xG-Campaigns - Automation Test Case Execution Summary</center></h1>"+"\n");
                this.writeStream.write("</div>"+"\n");
                this.writeStream.write("<div style=\"background-color:#66B2FF;font-family:'Times New Roman', Times, serif;font-size:medium\">"+"\n");
                this.writeStream.write("<h3><label><center>Report Generated On: " + this.DateHeader +" </h3>"+"\n");
                this.writeStream.write("</div>");
                this.writeStream.write("<table border='1' cellpadding='5' cellspacing='0' width='100%' >"+"\n");
                this.writeStream.write("<tr>"+"\n");
                this.writeStream.write("<td align=center Class=TestDetailsHead><font face=Arial size='3'><b><u>Test Case Execution Summary</u></b></font></td>"+"\n");
                this.writeStream.write("</tr>"+"\n");
                this.writeStream.write("</table>"+"\n");
                this.writeStream.write("<table border='2' cellpadding='0' cellspacing='0' width='100%'><tr>"+"\n");
        
                this.writeStream.write("<td class=ColumnHead width='28%'><b>URL</b></td>"+"\n");
                this.writeStream.write("<td class=ColumnHead width='8%'><b>Total Tests Executed</b></td>"+"\n");
                this.writeStream.write("<td class=ColumnHead width='8%'><b>Total Tests Passed</b></td>"+"\n");
                this.writeStream.write("<td class=ColumnHead width='8%'><b>Total Tests Failed</b></td>"+"\n");
                this.writeStream.write("<td class=ColumnHead width='16%'><b>Time Taken for execution (hh:mm:ss)</b></td>"+"\n");
               // writeStream.write("<td class=ColumnHead width='20%'><b>Tests Execution Date and Time</b></td></tr>"+"\n");
                var sModuleName=""+globalvalues.urlname+"";
               // var sTime="02:05:PM";
                this.writeStream.write("<tr class=GeneralHead><td class=Hyperlink   >"+sModuleName+"</td><td style='text-align:center;' <b>"+this.totalTestCaseExecuted+" </b></td><td style='text-align:center;' <b>"+this.totalTestPassed+"</b></td><td style='text-align:center;' <b>"+this.totalTestFailed+"</b></td><td style='text-align:center;' <b>" + this.totalTimeexecutionConverted +" </b></td></tr>"+"\n");
        
                this.writeStream.write("<table border='1' cellpadding='5' cellspacing='0' width='100%' style='padding-top: 20px;'>"+"\n");
                this.writeStream.write("<tr>"+"\n");
                this.writeStream.write("<td align=center Class=TestDetailsHead><font face=Arial size='3'><b><u>Detailed Test Case Execution Summary</u></b></font></td>"+"\n");
                this.writeStream.write("</tr>"+"\n");
                this.writeStream.write("</table>"+"\n");
        
                this.writeStream.write("<div style=\"background-color:#C1504C;font-family:'Times New Roman', Times, serif;font-size:medium\">"+"\n");
                this.writeStream.write("<table border='1' cellpadding='5' cellspacing='0' width='100%' style='padding-top: 20px;'>"+"\n");
                this.writeStream.write("<tr>"+"\n");
                this.writeStream.write("<td align=flex-start Class=TestSummaryHead ></td>"+"\n");
                this.writeStream.write("</tr>"+"\n");
                this.writeStream.write("</table>"+"\n");
                this.writeStream.write("</div>"+"\n");
        
                this.writeStream.write("<div style=\"background-color:#C1504C;font-family:'Times New Roman', Times, serif;font-size:medium\">"+"\n");
                this.writeStream.write("<table border='2' cellpadding='0' cellspacing='0' width='100%'><tr>"+"\n");
                this.writeStream.write("<td class=ColumnHead width='8%'><b>Test Case Name</b></td>"+"\n");
                this.writeStream.write("<td class=ColumnHead width='20%'><b>Description</b></td>"+"\n");
                this.writeStream.write("<td class=ColumnHead width='8%'><b>Start Time</b></td>"+"\n");
                this.writeStream.write("<td class=ColumnHead width='8%'><b>End Time</b></td>"+"\n");
                this.writeStream.write("<td class=ColumnHead width='16%'><b>Duration (hh:mm:ss)</b></td>"+"\n");
                this.writeStream.write("<td class=ColumnHead width='8%'><b>Status</b></td></tr>"+"\n");
               // let Duration="02:05:06";
                let indexHtmlCon:number;
                for(indexHtmlCon=1;indexHtmlCon<=this.totalTestCaseExecuted;indexHtmlCon++) {
                    var ResultString = this.TestCaseHolder[indexHtmlCon].split('#');
                    var _colourValue;
                    if (ResultString[2]=="Fail")
                    {
                        _colourValue = "red";
                    }
        
                    else if(ResultString[2]=="Pass")
                    {
                        _colourValue = "green";
                    }
                    else
                    {
                        _colourValue = "grey";  
                    }
                  
                    this.writeStream.write("<tr class=GeneralHead><td class=Hyperlink ><a href='" + ResultString[5] + "'>" + ResultString[0] + " </a></td><td <b>" + String(ResultString[1]).split(',').join(' ')+ "</b></td><td style='text-align:center;' <b>" + ResultString[3] + "</b></td><td style='text-align:center;' <b>" + ResultString[4] + "</b></td><td style='text-align:center;' <b>" + ResultString[6] + "</b></td><td style='text-align:center;background-color: " + _colourValue + ";' <b>" + ResultString[2] + "</b></td></tr>"+"\n");
               }
               this.writeStream.write("</div>"+"\n");
               this.writeStream.write("</body>"+"\n");
               this.writeStream.write("</html>"+"\n");
               } 
    constructor(){
       
    }

}