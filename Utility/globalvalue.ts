import { browser, by, element } from "protractor";
import { ActionLib } from '../Libs/GeneralLibs/ActionLib';
let action = new ActionLib();

export class globalvalues {

    static reportInstance;
    static folderTimeStamp;
    static loggerInstance;
    static rootfolder = "../";
    static QuitONFailed = true; // true if quit on failure
    //static urlname="http://qaxgstorybook.azurewebsites.net";
    // static urlname="http://qaxgcontrolstorybook.azurewebsites.net/";
    static urlname = "https://xgplatform-dev.myimagine.com/";
    // static urlname = "https://xgplatform-qa.myimagine.com/";
    static BuildNo = "2.0";
    static username = "xgautomation@imaginecommunications.com";
    static password = 'qs"$f]qP9%';


    // static ratingAPI = "http://rtgsapp01.eastus.cloudapp.azure.com/xgratings/api/"
    static ratingAPI = "https://67a55d8c.ngrok.io/xgratingsqa/api/"


    static timeConversion(millisec) {

        var seconds = (millisec / 1000).toFixed(1);
        var minutes = (millisec / (1000 * 60)).toFixed(1);
        var hours = (millisec / (1000 * 60 * 60)).toFixed(1);
        var days = (millisec / (1000 * 60 * 60 * 24)).toFixed(1);

        if (parseInt(seconds) < 60) {
            return seconds + " Sec";
        } else if (parseInt(minutes) < 60) {
            return minutes + " Min";
        } else if (parseInt(hours) < 24) {
            return hours + " Hrs";
        } else {
            return days + " Days"
        }
    }


    getcurrentDate() {
        let today = new Date();
        let year = today.getFullYear();
        let month = today.getMonth() + 1;
        let Days = today.getDate();
        let YearString: string = year.toString().substr(0, 2);
        return month + "/" + Days + "/" + YearString;
    }

    async LaunchStoryBook() {
        try {
            let username = by.css('input[name*="loginfmt"]');
            let nextLocator = by.css('input[type*="submit"]');
            // let password = by.css('input[name*="passwd"]');
            let btnYes = by.css('input[value*="Yes"]');
            let usernameImagine = by.css('input[name*="username"]');
            let passwordImagine = by.css('input[name*="password"]');


            await browser.driver.get(globalvalues.urlname);
            await browser.waitForAngularEnabled(false);
            await action.SetText(username, globalvalues.username, "Enter User Name");
            await action.Click(nextLocator, "next button");
            await action.SetText(usernameImagine, globalvalues.username.split('@')[0].trim(), "Enter username");
            await action.SetText(passwordImagine, globalvalues.password, "Enter password");
            await action.Click(nextLocator, "submit");
            await action.Click(btnYes, "Yes");
            await browser.waitForAngular();
            await browser.waitForAngularEnabled(false);
        }
        catch (err) { await action.ReportSubStep("LaunchStoryBook", "Error In application Launch and Login", "Fail") }
    }

}