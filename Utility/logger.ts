/*global document, window, alert, console, require, browser,jasmine,
 requirePage, requireData, requireConfig, requireLibrary,
 describe, it, beforeEach, forEach, by, expect, element*/
/**
 * Logger functionality, maintains log the testcase flow while automation.
 * @class logger
 */

 
 import log4js from 'log4js';
 
export class Logger {
    logger;
    logger_fail;
    
    Log(TestStepDesc) {
      
        this.logger.debug(TestStepDesc);
        //console.log(TestStepDesc);
       
    }
   LogFail(TestStepDesc) {
      
        this.logger_fail.error(TestStepDesc);
       
    } 
    constructor()
    {
        log4js.configure({
            appenders: { testcase: { type: 'file', filename: '../../TestResult/actionLog.log' } ,testcasefail: { type: 'file', filename: '../../TestResult/stepsfailLog.log' } },
            categories: { default: { appenders: ['testcase'], level: 'debug'} ,testcasefail: { appenders: ['testcasefail'], level: 'error'} }
            
          });
        
        this.logger = log4js.getLogger('testcase');  
        this.logger_fail = log4js.getLogger('testcasefail');

   
      
    }
    
}