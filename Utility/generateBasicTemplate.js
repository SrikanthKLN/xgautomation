var fs = require('fs');
var request = require("request");

var script_header1 = `/*
***********************************************************************************************************
* @Script Name :`
var script_header2 = '\n* @Description :  '
var script_header3 = `* @Page Object Name(s) : XXX\n* @Dependencies/Libs : \n* @Pre-Conditions : \n* @Creation Date : `
var today = new Date();
var today_date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear() + '\n';
var script_header4 = `* @Author : \n* @Modified By & Date:dd-mm-yyyy
*************************************************************************************************************
*/`
var TestCase_ID = process.argv[3];
var obj, story_title, TC_Title, script_header;

var options = {
    method: 'GET',
    url: 'https://imaginecommunications.atlassian.net/rest/api/latest/issue/' + TestCase_ID,
    headers: { Authorization: 'Basic a3NyaWthbnRoQGF1cmV1c3RlY2hzeXN0ZW1zLmNvbTpScFVnVnRYMTJjQ0J1V3h3YVhqSENEOTE=', 'Content-Type': 'application/json' }
};

async function initialize(opt) {
    return new Promise(function (resolve, reject) {
        request(opt, function (error, response, body) {
            response.setEncoding('utf8');
            if (error) { reject(err); }
            else { resolve(JSON.parse(body)); }
        });
    });
}

async function WriteToCSV() {

    writeStream = fs.createWriteStream("../Template/" + process.argv[3] + ".ts");
    writeStream.write(script_header);

    writeStream.write("\n\n //Import Statements\n");
    writeStream.write("import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';" + "\n");
    writeStream.write("import { globalvalues } from '../Utility/globalvalue';" + "\n");
    writeStream.write("import { Reporter } from '../Utility/htmlResult';" + "\n");
    writeStream.write("import { ActionLib } from '../Libs/GeneralLibs/ActionLib';" + "\n");
    writeStream.write("import { VerifyLib } from '../Libs/GeneralLibs/VerificationLib';" + "\n");
    writeStream.write("import { StoryBookComp } from '../Pages/SBCommonComponents/SBCommonElementsPage';" + "\n\n");

    writeStream.write("//Import Class Objects Instantiation\n");
    writeStream.write("let report = new Reporter();" + "\n");
    writeStream.write("let verify = new VerifyLib();" + "\n");
    writeStream.write("let action = new ActionLib();" + "\n");
    writeStream.write("let SBComm = new StoryBookComp();" + "\n\n");
    writeStream.write("//Variables Declaration\n");
    writeStream.write("let TestCase_ID = '" + process.argv[3] + "'" + "\n");
    writeStream.write("let TestCase_Title = '" + story_title + "'" + "\n");

    writeStream.write("//HTML Report generate intiation" + "\n");
    writeStream.write("report.InitializeSigleHtmlReport(TestCase_ID,TestCase_Title);" + "\n");
    writeStream.write("globalvalues.reportInstance = report;" + "\n\n");

    writeStream.write("//**********************************  TEST CASE Implementation ***************************\n");

    TC_Title = story_title.toUpperCase();
    writeStream.write("describe('" + TC_Title + "'" + ", () => {" + "\n\n");

    var lineRdr = require("readline").createInterface({
        input: fs.createReadStream('../TestCaseCSV/' + process.argv[2] + '.csv')
    }, function (err) {
        console.log("CSV file Not found");
    });

    lineRdr.on('line', function (line) {
        line_arr = line.split('|');

        try {

            if (line_arr[0] != "action") {

                let stepAction = line_arr[0].replace(/\"/g, "\'");
                let stepData = line_arr[1].replace(/\"/g, "\'");
                let stepExpResult = line_arr[2].replace(/\"/g, "\'");

                if (typeof (line_arr[2]) != "undefined") {
                    writeStream.write("// --------------Test Step------------\n");

                    writeStream.write("it(\"" + stepAction + "\", async () => {" + "\n\n");
                    writeStream.write("let stepAction = \"" + stepAction + "\";" + "\n");
                    writeStream.write("let stepData = \"" + stepData + "\";" + "\n");
                    writeStream.write("let stepExpResult = \"" + stepExpResult + "\";" + "\n");
                    writeStream.write("let stepActualResult = \"" + stepExpResult + "\";" + "\n\n");

                    writeStream.write("try {" + "\n\n");
                    writeStream.write("report.ReportStatus(stepAction,stepData,'Pass',stepExpResult ,stepActualResult);" + "\n");
                    writeStream.write(" }" + "\n\n");
                    writeStream.write("catch (err) {" + "\n");
                    writeStream.write("report.ReportStatus(stepAction,stepData,'Fail',stepExpResult ,err);" + "\n");
                    writeStream.write(" }" + "\n\n});\n\n\n");
                }
                else { writeStream.write(stepAction + "\n"); }
            }
        }
        catch{ console.log("******Error In reading File********** "); }
    }),
        lineRdr.on('close', function (line) {
            writeStream.write("});" + "\n");
        });
}

async function main() {
    obj = await initialize(options);
    story_title = obj.fields.summary;
    jira_id = obj.key;
    script_header = script_header1 + '  ' + jira_id + script_header2 + story_title + today_date + script_header3 + today_date + script_header4;
    await WriteToCSV();
}
main();

// console.log("******start reading File**********");
// console.log("action -- " + stepAction);
// console.log("data -- " + stepData);
// console.log("result-- " + stepExpResult);
// writeStream.write("it('"+ stepAction +"', async () => {" + "\n\n"); 
//writeStream.write("let stepAction = '',stepData='',stepExpResult='',stepActualResult"+ "\n\n");
// await doCall(options, function (resp) {console.log(resp);story_title = resp;});
// var script_header2 ='\n* @Description :'