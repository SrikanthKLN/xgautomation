import { browser, element, by, By, $, $$, ExpectedConditions, protractor } from 'protractor';

export class Gutils {

    // Function to bring element on screen
    mouseMove(locator) {
        return new Promise(function (resolve, reject) {
            browser.waitForAngular().then(function () {
                var EC = protractor.ExpectedConditions;
                browser.wait(EC.presenceOf(element(locator)), 120000).then(function () {
                }, function (err) {
                    console.log('Error while looking for presenceOf for element ' + locator);
                }).then(function () {
                    browser.actions().mouseMove(element(locator)).perform().then(function () {
                        resolve();
                    }, function (err) {
                        reject(err);

                    });
                });
            });
        });
    }

    //Explicitly waits for the give time
    Wait(interval: number) {
        return new Promise(function (resolve, reject) {
            let int = interval * 1000;
            browser.sleep(int).then(function () {
                resolve();
            });
        });
    }

    //Dynamically waits for maximum of xx seconds to load the object, if element loaded then jumps to next step.
    mediumDynamicWait() { return 180000; }
    shortDynamicWait() { return 120000; }
    veryShortDynamicWait() { return 45000; }

    // Get the current time stamp
    currentTimeStamp() {
        let today = new Date();
        let timeStamp = today.getMonth() + 1 + '-' + today.getDate() + '-' + today.getFullYear() + '-' + today.getHours() + 'H_' + today.getMinutes() + 'M_' + today.getSeconds() + 'S';
        return timeStamp;
    }

    // Create dynamic locator
    makeDynamicLocator(classname, locator) {
        return new Promise(function (resolve, reject) {
            let classInstancce = new classname();
            classInstancce.locator = classInstancce.locator.replace("tempvalue", locator);
        });
    }

    // Get Current Date
    getcurrentDate() {

        let today = new Date();
        let year = today.getFullYear();
        let month = today.getMonth() + 1;
        let Days = today.getDate();
        let monthS: string;
        let DaysS: string;
        let YearString: string = year.toString();

        if (Number(month) < 10) { monthS = "0" + String(month); }
        else { monthS = String(month); }

        if (Number(Days) < 10) { DaysS = "0" + String(Days); }
        else { DaysS = String(Days); }

        return monthS + "/" + DaysS + "/" + YearString;
    }

    // Get Date after given days
    getcurrentDateAfterSomeDays(afterdays: number) {

        let today = new Date();
        today.setDate(today.getDate() + afterdays);
        let year = today.getFullYear();
        let month = today.getMonth() + 1;
        let Days = today.getDate();
        let YearString: string = year.toString();
        let monthS: string;
        let DaysS: string;

        if (Number(month) < 10) { monthS = "0" + String(month); }
        else { monthS = String(month); }

        if (Number(Days) < 10) { DaysS = "0" + String(Days); }
        else { DaysS = String(Days); }

        return monthS + "/" + DaysS + "/" + YearString;
    }
}