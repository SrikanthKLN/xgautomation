/**
 * Created by vikas singh.
 */
import { browser, element, by, protractor, promise } from 'protractor';
import { globalvalues } from '../Utility/globalvalue';
import {Consolidatedclass } from '../Utility/ConsolidatedResult';
import {Logger } from '../Utility/logger';
let repoLogger=new Logger();
export class Reporter {

    TestCaseName = null;
    stepCountHTML: number = null;
    TotalStatusHTML = null;
    pass_count_HTML = null;
    Fail_count_HTML = null;
    skipped_count_HTML=null;
    TCStartTime = null;
    TCEndTime = null;
    screenShotDirectory = null;
    stepNumber: number = null;
    DicData = null;
    totalTime=null;
    totalTimeRaw=null;
    Storevalue = null;
    Discription = null;

    InitializeSigleHtmlReport(TestCaseName, Description) {
        this.TestCaseName = TestCaseName.trim();
        this.Discription = Description;
        this.stepCountHTML = 0;
        this.TotalStatusHTML = "In Complete";
        this.pass_count_HTML = 0;
        this.Fail_count_HTML = 0;
        this.skipped_count_HTML = 0;
        this.TCStartTime = new Date();
        this.TCEndTime = new Date();
       
        this.screenShotDirectory = "";
        this.stepNumber = 0;
        this.DicData = {};
        this.Storevalue = {};
        this.Storevalue["StepNo"] = 0;
        globalvalues.loggerInstance=TestCaseName;
    }
    ReportStatus(TCName, Description, Status,expectedResult, actualresult) {


        var currentTime = new Date().toTimeString().split(" ")[0];
        this.TCEndTime = new Date()//.toLocaleString();
        this.totalTimeRaw=Math.abs(this.TCEndTime.getTime() - this.TCStartTime.getTime());
        this.totalTime = globalvalues.timeConversion(this.totalTimeRaw);
        this.stepNumber = this.stepNumber + 1;
        this.stepCountHTML = this.stepCountHTML + 1;
        // this.Storevalue["StepNo"]=this.stepNumber;
        //console.log(this.Storevalue["StepNo"])
        //console.log(this.stepCountHTML)
        var fssummary = require('fs');

        if ((Status.toUpperCase()) == "FAIL") {
           
            var relativeImagePath = this.TakeScreenShot("../../TestResult/CustomHtmlResult/" + this.TestCaseName + this.stepNumber + ".png");
            relativeImagePath="./" + this.TestCaseName + this.stepNumber + ".png";
            this.TotalStatusHTML = "Fail";
            this.Fail_count_HTML = parseInt(this.Fail_count_HTML) + 1;
            this.DicData[this.stepNumber] = TCName + "$" + Description + "$" + Status + "$" + actualresult + "$" + expectedResult + "$" + relativeImagePath;
            this.Storevalue["FailCount"] = this.Fail_count_HTML;
            var dataF = [this.stepNumber, this.TestCaseName, TCName, Description, Status, actualresult, expectedResult, relativeImagePath] + '\n';
            var dataFCSV = [TCName, Description,expectedResult,actualresult,Status] + '\n';
            expect("fail").toBe("pass");
            console.log(TCName+" --- fail --- ");
            fssummary.appendFile('../../TestResult/ResultCSV/' + this.TestCaseName + '.csv', dataFCSV, function (err) {
                if (err)
                    throw err;
            });
            if(globalvalues.QuitONFailed==true)
              {
                  browser.close();
              }
        }
        else if ((Status.toUpperCase()) == "PASS"){
            
            this.pass_count_HTML = this.pass_count_HTML + 1;
            this.DicData[this.stepNumber] = TCName + "$" + Description + "$" + Status + "$" + actualresult + "$" + expectedResult;
            this.Storevalue["PassCount"] = this.pass_count_HTML;
            var dataP = [this.stepNumber, this.TestCaseName, TCName, Description, Status, actualresult, expectedResult] + '\n';
            var dataPCSV = [TCName, Description,expectedResult,actualresult,Status] + '\n';
            expect("pass").toBe("pass");
            //console.log(Description+" --- pass --- ");
            fssummary.appendFile('../../TestResult/ResultCSV/' + this.TestCaseName + '.csv', dataPCSV, function (err) {
                if (err)
                    throw err;
            });
        }
        else if ((Status.toUpperCase()) == "SKIP"){
            
            this.skipped_count_HTML = this.skipped_count_HTML + 1;
            this.DicData[this.stepNumber] = TCName + "$" + Description + "$" + Status + "$" + actualresult + "$" + expectedResult;
            this.Storevalue["SkippedCount"] = this.pass_count_HTML;
            var dataP = [this.stepNumber, this.TestCaseName, TCName, Description, Status, actualresult, expectedResult] + '\n';
            var dataPCSV = [TCName, Description,expectedResult,actualresult,Status] + '\n';
            expect("pass").toBe("pass");
            //console.log(Description+" --- pass --- ");
            fssummary.appendFile('../../TestResult/ResultCSV/' + this.TestCaseName + '.csv', dataPCSV, function (err) {
                if (err)
                    throw err;
            });
        }
       if(this.Fail_count_HTML ==0)
         {
           if(this.pass_count_HTML > 0)
             {
                this.TotalStatusHTML = "Pass";
             }
         } 




        let indexStepCount: number;

        for (indexStepCount = 1; indexStepCount < this.stepCountHTML; indexStepCount++) {

        }
        var fs = require("fs");
        let writeStream = fs.createWriteStream("../../TestResult/CustomHtmlResult/" + this.TestCaseName + ".html");
        writeStream.write("<html><title>Testcase Summary</title><head><style>.ColumnHead {font-family: Arial; font-size: 10pt ; text-align: center; border-bottom: solid lightyellow 1.0pt; border-top: solid black 1.5pt; color: black; background:#BCEEDA;}</style><style>.Summary_Table{background:#BCEEDA;margin-top:1%;border-bottom: thin groove lightyellow; border-top: thin groove lightyellow;}</style></head><body> <div id='page'><div id ='page_Header' style='width:100%; height:30px;background:#C0504D;border-bottom: thin groove lightyellow; border-top: thin groove lightyellow;'><b> <label style='color:#ffffff; font-size:20px;margin-left:40%;padding-top:25px;'>Automation Batch Summary</label> </b></div><div id ='Sub_Header' style='width:100%; height:30px;background:#6288E9;margin-top:1%;border-bottom: thin groove lightyellow; border-top: thin groove lightyellow;'><b> <font style='color:#ffffff; font-size:20px;margin-left:40%;'>TestCase Execution Details</font> </b>" + "\n")
        writeStream.write("<table border='2' cellpadding='0' cellspacing='0' width='100%' style='margin-top:1%'>" + "\n");
        writeStream.write("<tr><td class=ColumnHead style='width:16%'><b> TestCase Name </b></td><td class=ColumnHead style='width:8%'><b>Total Steps</b></td><td class=ColumnHead style='width:16%'><b>Status</b></td><td class=ColumnHead style='width:8%'><b>Passed</b></td><td class=ColumnHead style='width:8%'><b>Failed</b></td><td class=ColumnHead style='width:8%'><b>Skipped</b></td><td class=ColumnHead style='width:16%'><b>Start Time</b></td><td class=ColumnHead style='width:16%'><b>End Time</b></td></tr>" + "\n");
        writeStream.write("<tr><td class=ColumnHead style='width:16%'><b> " + this.TestCaseName + " </b></td><td class=ColumnHead style='width:16%'><b>" + indexStepCount + " </b></td><td class=ColumnHead style='width:16%'><b>" + this.TotalStatusHTML + "</b></td><td class=ColumnHead style='width:8%'><b>" + this.pass_count_HTML + "</b></td><td class=ColumnHead style='width:8%'><b>" + this.Fail_count_HTML + "</b></td><td class=ColumnHead style='width:8%'><b>" + this.skipped_count_HTML + "</b></td><td class=ColumnHead style='width:16%'><b>" + this.TCStartTime.toLocaleString() + "</b></td><td class=ColumnHead style='width:16%'><b>" + this.TCEndTime.toLocaleString() + "</b></td></tr></table></div>" + "\n");
        writeStream.write("<div id ='Summary_Header' style='width:100%; height:30px;background:#9494E0;margin-top:4%;border-bottom: thin groove lightyellow; border-top: thin groove lightyellow;'><b> <font style='color:#ffffff; font-size:20px;margin-left:40%;'>Individual TestCase Execution Details</font> </b></div><div id ='Summary_Table' style='width:100%; height:30px;background:#BCEEDA;margin-top:1%;border-bottom: thin groove lightyellow; border-top: thin groove lightyellow;'><table border='2' cellpadding='0' cellspacing='0' width='100%'>" + "\n");
        writeStream.write("<tr><td class=ColumnHead style='width:3%'><b> StepNo. </b></td><td class=ColumnHead style='width:29%'><b>action</b></td><td class=ColumnHead style='width:10%'><b>data</b></td><td class=ColumnHead style='width:5%'><b>Status</b></td><td class=ColumnHead style='width:23%'><b>expected result</b></td><td class=ColumnHead style='width:30%'><b>actual result</b></td></tr>" + "\n");
        var indexHtml;

        for (indexHtml = 1; indexHtml <= this.stepNumber; indexHtml++) {
            var ResultString = this.DicData[indexHtml].split('$');

            writeStream.write("<tr>");
            writeStream.write("<td class=ColumnHead style='width:3%;text-align: left'>" + indexHtml + "</td>");
            writeStream.write("<td class=ColumnHead style='width:29%;text-align: left'>" + ResultString[0].toString().split(',').join(' ') + "</td>");
            writeStream.write("<td class=ColumnHead style='width:10%;text-align: left'>" + ResultString[1].toString().split(',').join(' ') + "</td>");
            if ((ResultString[2].toString().toUpperCase()) == "FAIL") {
                writeStream.write("<td bgcolor='red' ;style='width:5%'><b><a href='" + ResultString[5] + "'>" + ResultString[2].toString().toUpperCase() + "</a></b></td>");

            }
            else if((ResultString[2].toString().toUpperCase()) == "PASS")  {

                writeStream.write("<td  bgcolor='green';style='width:23%'><b>" + ResultString[2].toString().toUpperCase() + "</b></td>");
            }
            else if((ResultString[2].toString().toUpperCase()) == "SKIP")  {

                writeStream.write("<td  bgcolor='grey';style='width:23%'><b>" + ResultString[2].toString().toUpperCase() + "</b></td>");
            }
            writeStream.write("<td class=ColumnHead style='width:10%;text-align: left'>" + ResultString[4].toString().split(',').join(' ') + "</td>");
            writeStream.write("<td class=ColumnHead style='width:30%;text-align: left'>" + ResultString[3].toString().split(',').join(' ').split('\n').join(' ') + "</td>");
            writeStream.write("</tr>");

        }
        //  writeStream.write("</table></div></div></body></html>");
        writeStream = null;
    }
    TakeScreenShot(filename) {
        var fs = require('fs');
        browser.takeScreenshot().then(function (png) {
            var stream = fs.createWriteStream(filename);

            stream.write(new Buffer(png, 'base64'));
            stream.end();

        });
        return filename;
    }
    finalize() {
        // var conReport=new  consolidated();
        // conReport.AddReport(this.TestCaseName,this.Discription,this.TotalStatusHTML,this.TCStartTime,this.TCEndTime,this.TestCaseName+".html");
        var fs = require('fs');
        let data = [this.TestCaseName, this.Discription, this.TotalStatusHTML, this.TCStartTime.toLocaleString(), this.TCEndTime.toLocaleString(), this.TestCaseName.trim() + ".html",this.totalTime,this.totalTimeRaw] + '\n';

        fs.appendFile('../../TestResult/CustomHtmlResult/consolidatedResult.csv', data, function (err) {
            if (err)
                throw err;
        });

    }
    consolidatedResult() {
        //var conReport = new Consolidatedclass();
        let TotalStatusHTML = 1;
        let startTime = "";
        let fs = require('fs');
        let lineRdr = require("readline").createInterface({
            input: fs.createReadStream('../../TestResult/CustomHtmlResult/consolidatedResult.csv')
        }, function (err) {
            console.log("CSV file Not found");
        });

        lineRdr.on('line', function (line) {
            let line_arr = line.split(',');
            let lineZero;
            let lineOne;
            let linetwo;
            let linefour;
            let linesix;
            let linesEight;
            let linesNine;
            let linesTen;

            try {
                if (typeof (line_arr[0]) != "undefined") {
                    lineZero = line_arr[0];
                    //lineZero=lineZero.toString();
                }

                if (typeof (line_arr[1]) != "undefined") {
                    lineOne = line_arr[1];
                    // lineOne=lineOne.toString();
                }

                if (typeof (line_arr[2]) != "undefined") {
                    linetwo = line_arr[2];
                    // linetwo=linetwo.toString();
                }

                if (typeof (line_arr[4]) != "undefined") {
                    linefour = line_arr[4];
                    //linefour=linefour.toString();
                }

                if (typeof (line_arr[6]) != "undefined") {
                    linesix = line_arr[6];
                    //linesix=linesix.toString();
                }
                if (typeof (line_arr[8]) != "undefined") {
                    linesEight = line_arr[8];
                    //linesix=linesix.toString();
                }
                if (typeof (line_arr[9]) != "undefined") {
                    linesNine = line_arr[9];
                    //linesix=linesix.toString();
                   // console.log("line 9" +line_arr[9])
                }
               

                console.log("******start reading File**********");
                Consolidatedclass.AddReport(lineZero, lineOne, linetwo, linefour, linesix, lineZero + ".html",linesEight,linesNine);

            }
            catch
            {
                console.log("******Error In reading File********** ");
                // var conReport=new  consolidated(); 
                //conReport.AddReport(line_arr[0],line_arr[1],line_arr[2],line_arr[4],line_arr[6],line_arr[0]+".html");
            }







        }),
            lineRdr.on('close', function (line) {
                Consolidatedclass.genReport();



            });
        // data = [this.TestCaseName, this.Discription, this.TotalStatusHTML, this.TCStartTime, this.TCEndTime, this.TestCaseName+".html"]+ '\n';

        // fs.appendFile('../TestResult/consolidatedResult.csv', data, function (err) {
        //     if (err)
        //        throw err;
        // });

    }
   

}

